文件说明:
initdata.sql		初始化数据文件
注意：新增语句不允许使用转义符，以及使用分号结尾（Oracle执行会报错），如面的语句
错误写法
INSERT INTO dev_system_param (`id`, `name`, `value`, `label`, `i`) VALUES ('40288126488d23e001488d49ad4b000d', 'system_shortcut_icon', '', '地址栏图标', 1);
正确写法
INSERT INTO dev_system_param (id, name, value, label, i) VALUES ('40288126488d23e001488d49ad4b000d', 'system_shortcut_icon', '', '地址栏图标', 1)

mysql.sql		建表语句文件（MySQL）
oracle.sql	建表语句文件（Oracle）
sqlserver.sql	建表语句文件（SqlServer）
注意：
1.字段名，表名必须大写，按要求用符号包起来(除MySQL使用`，Oracle与SqlServer使用"。)
	其源码会通过这些符号获取建表语句中的表名及字段名，来判断表以及字段是否存在来决定是否创建
	其从数据库获取的字段名默认是大写的，所以这里的字段名也必须大写不然equal的时候会默认该字段未被创建，然后造成字段重复创建，继而报错
2.sql 不能以分号；结尾（Oracle执行会报错）


总结
1.以后字段新增直接在在建表语句里面添加，如下（DEV_STATION_DIM加个字段NAME）
改前
CREATE TABLE `DEV_STATION_DIM`(`STATION_ID` VARCHAR(32),`DIM_TAG_ID` VARCHAR(32))
改后
CREATE TABLE `DEV_STATION_DIM`(`STATION_ID` VARCHAR(32),`DIM_TAG_ID` VARCHAR(32),`NAME` VARCHAR(32))
2.如果要修改某个字段的属性需要另写sql 此工具不支持
	请分别放置在以下3个文件中
	initdata.other.mysql.sql （MySQL）
	initdata.other.oracle.sql（Oracle）
	initdata.other.sqlserver.sql（SqlServer）
3.表删除,字段删除需要另写sql 此工具不支持
	请分别放置在以下3个文件中
	initdata.other.mysql.sql （MySQL）
	initdata.other.oracle.sql（Oracle）
	initdata.other.sqlserver.sql（SqlServer）

