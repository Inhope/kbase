--数据迁移，知识库数据初始化（详情请咨询 charley.qian）
update kb_dim_val set flag=flag|1 where exists (select * from ra_file__object rfo where rfo.object_id=kb_dim_val.id);

update kb_object set status=status|256 where exists 
 (select * from ra_file__object rfo where rfo.object_id=kb_object.object_id);
 
 update kb_object set status=status|128 where exists 
 (select * from kb_val v join kb_dim_val dv on dv.value_id=v.value_id 
 join ra_file__object rfo on rfo.object_id=dv.id 
 where v.object_id=kb_object.object_id);
 
 
 --获取表结构字段
 --MySQL
 select lower(replace(group_concat(a.column_name),",","、")) from (
        select column_name,table_name from information_schema.`columns` where table_schema='kbs' and table_name in ('kb_category_tag','kb_object','kb_val','kb_dim_val','kb_dim_val_tag')
) a group by a. table_name

 --SqlServer
 select x.table_name,lower(stuff(replace(replace(x.column_names,'<column_name>','、'),'</column_name>',''),1,1,'')) as column_names from (
        select a.table_name,
            (select column_name from information_schema.columns where table_name= a.table_name for xml path('')) as column_names
        from (
               select table_name,column_name from information_schema.columns where table_name in ('auth_address_restrict','auth_perm','auth_perm_cate','auth_perm_cate_to_perm','auth_role')
        ) a group by a.table_name
) as x

--积分管理数据清理
---------删除以日期结尾的DEV_P_LOG_*的表
drop table DEV_P_LOG_*;
---------清空相关表中的数据
delete from DEV_P_ACT;
delete from DEV_P_LOG;
delete from DEV_P_RULE;
delete from DEV_P_STRATEGY where id not in ('2c9082be4ddd0936014ddd0a96e90001');
delete from DEV_P_STRATEGY_CONFIG;
delete from DEV_P_STRATEGYEXT;
delete from DEV_P_USERCONFIG;
delete from DEV_P_USERSTRATEGY;

