<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>公告弹窗列表</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.datagrid-row{
				height: 30px;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$(function(){
				//grid 双击行
				$('#popupGrid').datagrid({
					onClickRow: function(index, row){
					    var _url = '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id='+row.id;
						try{
						// 单击使用IE新窗口替代TAB标签显示内容
//							opener.KbasePlugin.openNotice(_url);
// 							var _diaWidth = screen.width-300;
// 							var _diaHeight = screen.height-300;
// 							var _scrWidth = screen.width;
// 							var _scrHegiht = screen.height;
// 							var _diaLeft = (_scrWidth-_diaWidth)/2;
// 							var _diaTop = (_scrHegiht-_diaHeight)/2;
// 							var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=yes, location=yes, scrollbars=yes, toolbar=no, resizable=yes, status=no, fullscreen=no';
 							window.open(_url,'_blank');
						}catch(e){
							alert(e.message);
						}	 
						//已读后粗体改变成正常体
						//$('tr[datagrid-row-index="' + index + '"]').css({'font-weight': 'normal'});	
						$('tr[datagrid-row-index="' + index + '"]').css({'color' : '#000000','font-weight': 'normal'});
						///self.close(); 弹窗公告打开后，不关闭弹窗。
					}
				});
			});
		</script>
	</head>

	<body>
		<table id="popupGrid" style="overflow:hidden;height:540px;width: 446px;" 
		    url="${pageContext.request.contextPath }/app/notice/bulletin!popup.htm"
	        rownumbers="true" data-options="toolbar:'#tb-inbox', singleSelect:true, 
	        	rowStyler: function(index, row){
					return 'color:#0000FF;font-weight:bold;';
				}">
			<thead>
				<tr>
					<th field="title" width="55%">标题</th>
					<th field="edittime" width="25%">发布时间</th>
					<th field="createuser" width="20%">发布人</th>
				</tr>
			</thead>
		</table>
	</body>
</html>
