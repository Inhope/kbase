<%@page import="com.eastrobot.util.SystemKeys"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

Map<String, String> map = new HashMap<String, String>();
map.put("1", SystemKeys.getText("base.yes", request));
map.put("0", SystemKeys.getText("base.no", request));
request.setAttribute("map", map);
%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>My JSP 'ekoz.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery.cookie.js"></script>
		<script type="text/javascript">
			$(function(){
				//
				$('#btnAdd').click(function(){
					$.post($.fn.getRootPath() + '/app/util/ekoz!addCache.htm', function(data){
						alert(data);
					}, 'text');
				});
				
				//
				$('#btnGet').click(function(){
					$.post($.fn.getRootPath() + '/app/util/ekoz!getCache.htm', function(data){
						alert(data);
					}, 'text');
				});
				
				$('#btnSayHello').click(function(){
					alert($.i18n.prop('base.say_hello'));
					alert($.i18n.prop('base.say_hello', 'Super Man'));
				});
				
			});
			
			function changeLocale(key){
				$.changeLocale(key);
				location.href = "app/util/ekoz.htm?request_locale=" + key;
			}
		</script>
	</head>

	<body>
		<%--中英文切换 --%>
		<a href="javascript:void(0)" onclick="changeLocale('zh')"><s:text name="base.zh"/></a>
		<a href="javascript:void(0)" onclick="changeLocale('en')"><s:text name="base.en"/></a>
		<a href="app/util/ekoz.htm" target="_blank"><s:text name="base.openblank"/></a>
	
		<%-- 多语言基本写法 1 --%>
		<s:text name="base.today" />
		<s:text name="base.bulletin" />
		<%-- 多语言基本写法 2 --%>
		<s:i18n name="com.eastrobot.i18n.base.resource">
			<s:text name="base.today" />
		</s:i18n>
		<br>
		<%--按钮上的多语言 --%>
		<button id="btnSayHello"><s:text name="base.say_hello" /></button>
		
		<%--s标签上的多语言 --%>
		<s:textfield id="hello" value="%{getText('base.say_hello')}"></s:textfield>
		<input id="username"  name="username" type="text" value="<s:text name="base.say_hello" />"/>
		<s:submit onclick="return false;" value="%{getText('base.say_hello')}"/>
		
		<s:select id="visit_select" list="#request.map" name="visit" headerKey="" headerValue="-%{getText('base.plssel')}-" cssStyle="width: 70px;"></s:select>
		
		<%-- @TODO list中如何写入多语言？ --%>
		<s:select id="visit_select" list="#{'1': '是', '0': '否' }" name="visit" headerKey="" headerValue="-%{getText('base.plssel')}-" cssStyle="width: 70px;"></s:select>
		
		
		<%--自定义标签上的多语言 --%>
		<br>
		<kbs:input type="a" value="base.say_hello" key="userManageExportUser0" onclick="alert('WTF');"/>
		<br>
		<button id="btnAdd"><s:text name="base.setcache"/></button>
		<button id="btnGet"><s:text name="base.delcache"/></button>
	</body>
</html>
