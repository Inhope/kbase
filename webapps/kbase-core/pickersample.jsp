<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>选择器模板</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			table{
				border: 1px solid #838383;
				width: 100%;
			}
			th, td, input{
				border:1px dotted #838383;
			}
			input {
				margin:1px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#deptuserdesc').click(function(){
					$.kbase.picker.deptUser({returnField:"deptuserdesc|deptuserid"});
					/*
					window._kbs_picker_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/util/picker!deptuser.htm?returnField=deptuserdesc|deptuserid"}, 
						area:["540px", "480px"]
					});
					*/
				});
				
				$('#deptuserdescmulti').click(function(){
					$.kbase.picker.multiDeptUser({returnField:"deptuserdescmulti|deptuseridmulti"});
					/*
					window._kbs_picker_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/util/picker!deptusermulti.htm?returnField=deptuserdescmulti|deptuseridmulti"}, 
						area:["540px", "480px"]
					});*/
				});
				
				$('#userByDeptDesc').click(function(){
					$.kbase.picker.userByDept({returnField:"userByDeptDesc|userByDeptId"});
					/*
					window._kbs_picker_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/util/picker!userbydept.htm?returnField=userByDeptDesc|userByDeptId"}, 
						area:["540px", "480px"]
					});*/
				});
				/*
				按部门选择用户（单选）[带搜索]
				*/
				$('#userByDeptDescS').click(function(){
					$.kbase.picker.userByDeptS({returnField:"userByDeptDescS|userByDeptIdS"});
				});
				
				$('#userByDeptDescMulti').click(function(){
					$.kbase.picker.multiUserByDept({returnField:"userByDeptDescMulti|userByDeptIdMulti"});
					/*
					window._kbs_picker_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/util/picker!multiuserbydept.htm?returnField=userByDeptDescMulti|userByDeptIdMulti"}, 
						area:["540px", "480px"]
					});*/
				});
				
				/*
				按部门选择用户（多选）[带搜索]
				*/
				$('#userByDeptDescMultiS').click(function(){
					$.kbase.picker.multiUserByDeptS({returnField:"userByDeptDescMultiS|userByDeptIdMultiS"});
				});
				
				//选择部门（多选）
				$('#deptDescMulti').click(function(){
					$.kbase.picker.multiDept({returnField:"deptDescMulti|deptIdMulti"});
				});
				
				$('#deptDescMulti ~ button').click(function(){
					$('#deptDescMulti').val('');
					$('#deptIdMulti').val('');
				});
								
				$('#stationByDeptDesc').click(function(){
					$.kbase.picker.stationByDept({returnField:"stationByDeptDesc|stationByDeptId"});
				});
				
				$('#stationByDeptDescMulti').click(function(){
					$.kbase.picker.multiStationByDept({returnField:"stationByDeptDescMulti|stationByDeptIdMulti", title:"请选择岗位", diaHeight: 515});
				});
				
				$('#kbDesc, #btnKb').click(function(){
					$.kbase.picker.kbval({returnField:"kbDesc|kbId|kbPid"});
				});
				$('#btnKbS').click(function(){
					$.kbase.picker.kbvalS({returnField:"kbDesc|kbId|kbPid"});
				});
				/**
				 * 选择最小分类
				 */
				$('#btnOntoCate').click(function(){
					$.kbase.picker.ontoCate({returnField: 'ontoCateDesc|ontoCateId'});
				});
				$('#paperDesc').click(function(){
					window.__kbs_layer_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/task/pp-paper.htm?ispicker=1&returnField=paperDesc|paperId"}, 
						area:["800px", "520px"]
					});
				});
			});
		</script>
	</head>

	<body>
		<table cellpadding="1" cellspacing="1">
			<tr>
				<th width="20%">功能</th>
				<th width="20%">方法名</th>
				<th width="20%">说明</th>
				<th width="40%">调用样例(第一个字段接收描述，第二个字段接收id)</th>
			</tr>
			<tr>
				<td>选择部门</td>
				<td></td>
				<td></td>
				<td>
					<input id="stationdesc" onclick="choose_stations('stationid', 'stationdesc');"/>
					<input id="stationid">
				</td>
			</tr>
			<tr>
				<td>选择部门与用户（混合选择）</td>
				<td>$.kbase.picker.deptUser(opts);</td>
				<td></td>
				<td>
					<input id="deptuserdesc"/>
					<input id="deptuserid" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>选择部门与用户（混合选择，多选）</td>
				<td>$.kbase.picker.multiDeptUser(opts);</td>
				<td></td>
				<td>
					<input id="deptuserdescmulti"/>
					<input id="deptuseridmulti" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择用户（单选）</td>
				<td>$.kbase.picker.userByDept(opts);</td>
				<td></td>
				<td>
					<input id="userByDeptDesc"/>
					<input id="userByDeptId" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择用户（单选）[带搜索]</td>
				<td>$.kbase.picker.userByDeptS(opts);</td>
				<td></td>
				<td>
					<input id="userByDeptDescS"/>
					<input id="userByDeptIdS" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择用户（多选）</td>
				<td>$.kbase.picker.multiUserByDept(opts);</td>
				<td></td>
				<td>
					<input id="userByDeptDescMulti"/>
					<input id="userByDeptIdMulti" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择用户（多选）[带搜索]</td>
				<td>$.kbase.picker.multiUserByDeptS(opts);</td>
				<td></td>
				<td>
					<input id="userByDeptDescMultiS"/>
					<input id="userByDeptIdMultiS" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择岗位（单选）</td>
				<td>$.kbase.picker.stationByDept(opts);</td>
				<td></td>
				<td>
					<input id="stationByDeptDesc"/>
					<input id="stationByDeptId" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>按部门选择岗位（多选）</td>
				<td>$.kbase.picker.multiStationByDept(opts);</td>
				<td></td>
				<td>
					<input id="stationByDeptDescMulti"/>
					<input id="stationByDeptIdMulti" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>选择部门（单选）</td>
				<td>$.kbase.picker.dept(opts);</td>
				<td></td>
				<td>
					<input id="deptDesc"/>
					<input id="deptId" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td>选择部门（多选）</td>
				<td>$.kbase.picker.multiDept(opts);</td>
				<td></td>
				<td>
					<input id="deptDescMulti"/>
					<input id="deptIdMulti" style="width:300px;"/>
					<button class="button button-action button-rounded button-small" style="margin-left:2px;margin-right:2px;padding-left:2px;padding-right:2px;">清空</button>
				</td>
			</tr>
			<tr>
				<td>选择知识</td>
				<td>$.kbase.picker.kbval(opts);</td>
				<td>
					returnField的参数可以是2个，也可以是3个，如果是2个，则返回标准问的描述和id，如果是3个，则第三个参数对应的字段接收标准问的实例id（不去除重复值）
				</td>
				<td>
					<input id="kbDesc"/>
					<input id="kbId" style="width:300px;"/>
					<input id="kbPid" style="width:300px;"/>
					<br>
					<button id="btnKb" class="button button-action button-rounded button-small">选择知识</button>
					<button id="btnKbS" class="button button-action button-rounded button-small">选择知识（含搜索功能）</button>
				</td>
			</tr>
			<tr>
				<td>选择最小分类</td>
				<td>$.kbase.picker.ontoCate(opts);</td>
				<td>
					
				</td>
				<td>
					<input id="ontoCateDesc"/>
					<input id="ontoCateId" style="width:300px;"/>
					<br>
					<button id="btnOntoCate" class="button button-action button-rounded button-small">选择最小分类</button>
				</td>
			</tr>
			<tr>
				<td>选择试卷</td>
				<td></td>
				<td></td>
				<td>
					<input id="paperDesc"/>
					<input id="paperId" style="width:300px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<font color="0000FF">[依赖的js文件:]</font><br>
					1. /library/jquery/js/Jquery.js<br>
					2. /library/jquery/js/Jquery-ex.js<br>
					3. /library/layer/layer.min.js<br>
					4. /library/jquery/js/jquery.picker.js<br>
					<font color="0000FF">[属性说明:]</font><br>
					 * returnField	返回的字段，desc|id，两个字段采用竖线分割，第一个字段接收描述，第二个字段接收id<br>
					 * diaWidth 对话框宽度<br>
					 * diaHeight 对话框高度<br>
					 * title 对话框标题，默认为false<br>
					 * 建议只传入returnField值，不传入title界面更好看<br>
					 * 默认宽高：540 × 480<br>
					<font color="0000FF">[代码样例:]</font><br>
					$.kbase.picker.userByDept({returnField:"userByDeptDesc|userByDeptId"});<br>
					$.kbase.picker.multiStationByDept({returnField:"stationByDeptDescMulti|stationByDeptIdMulti", title:"请选择岗位", diaHeight:515});
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
