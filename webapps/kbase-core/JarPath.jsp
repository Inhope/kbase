<%@page pageEncoding="UTF-8"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.*"%>
<%!String getClassPath(String className) {
		StringBuffer sb = new StringBuffer("<b>className</b>:" + className
				+ "<br>");
		try {
			Class z = Class.forName(className);
			String path = z.getName().replace(".", "/");
			path = "/" + path + ".class";
			URL url = z.getResource(path);
			if (url != null) {
				sb.append("<b>class loader</b>:" + z.getClassLoader());
				sb.append("<br><b>class path</b>:" + url.getPath());
			} else {
				sb.append("class not loaded by any classLoader");
			}
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter writer = new StringWriter();
			PrintWriter pw = new PrintWriter(writer);
			e.printStackTrace(pw);
			pw.close();
			sb.append("error:" + e);
			sb.append("<br><pre>" + writer.toString() + "</pre>");
		}
		return sb.toString();
	}%>
<html>
	<head>
		<title>find class path</title>
		<style>
* {
	font: 14px;
}

b {
	font-weight: bold;
}
</style>
	</head>
	<body>
		<form action="<%=request.getRequestURI()%>">
			class full name:
			<input type=text name=className size=60 />
			<br>
			<input type=submit value=submit />
		</form>
		<hr>
		<%
			String className = request.getParameter("className");
			if (className != null && !"".equals(className)) {
				out.println(getClassPath(className));
			}
		%>
	</body>
</html>