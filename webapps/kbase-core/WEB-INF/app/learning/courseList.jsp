<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>我的学习</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<style type="text/css">
			.usboxdiv{height: 100%;float: left;line-height: 28px;width: 325px;}
			.whiteBack{background:white;padding:5px 20px;border:#e0e0e0 solid 1px;}
			.grayBack{background:#edecec;padding:5px 20px;border:#e0e0e0 solid 1px;}
			.grayBack2{background:#edecec;padding:3px 10px;border:#e0e0e0 solid 1px;}
			#courseStatus li{float: left;}
			#statType li{float: left;}
		</style>
		<script type="text/javascript">
		//展开图标点击事件
			function loadMoreCate(_this,id){
				//展开图标样式变换和自己分类隐藏显示
				if($(_this).hasClass("pc_plus")){
					$(_this).removeClass("pc_plus");
					$(_this).addClass("pc_minus");
					$(_this).parent().parent().find("ul").show();
				}else if($(_this).hasClass("pc_minus")){
					$(_this).removeClass("pc_minus");
					$(_this).addClass("pc_plus");
					$(_this).parent().parent().find("ul").hide();
				}
				//追加子级分类
				if($(_this).parent().parent().find("ul").length == 0){
					$.ajax({
						type: "POST",
						url: $.fn.getRootPath()+"/app/learning/learn!courseCateJson.htm",
						data: "id="+id,
						success: function(data) {
							data = jQuery.parseJSON(data);
							$(data).each(function(i, cate){
								$(_this).parent().parent().append($('#cateTemplate').html().fill(
									cate.id,
									cate.name
								));
							});
						}
					});
				}
			}
			$(function(){
				//重置所有参数
				_resetParam = function(){
					$("input[name='statType']").val("");
					$("input[name='courseStatus']").val("");
					$("input[name='cateId']").val("");
					$("#searchKey").val("");
					$("#courseStatus").find("li a").css("background","#e0e0e0");
					$("#statType").find("li a").css("background","#e0e0e0");
					$("#courseStatus").find("li:first a").css("background","white");
					$('#course_list').empty();
				}
				//点击全部课程
				$("#allCourse").on("click",function(){
					_resetParam();
					var _param = "pageNo=1"
					_loadData(_param);
				});
				//点击分类
				$("[class='px_b2_left'] [class='px_b_con'] ul li").on("click","font[name='cateName']",function(){
					_resetParam();
					var _param = "pageNo=1&cateId="+$(this).attr("_id");
					_loadData(_param);
				});
				//点击课程
				$('#course_list').on("click","li .px_xk",function(){
					var _courseId = $(this).attr("_value");
					parent.parent.parent.TABOBJECT.open({
						id : 'courseInfo',
						name : '课程学习',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+_courseId,
						isRefresh : true
					}, this);
				});
				//点击搜索
				$("#searchImg").on("click",function(){
					//_resetParam();
					var _param = "searchKey="+$("#searchKey").val();
					$('#course_list').empty();
					_loadData(_param);
				});
				//点击全部、正在开课、即将开课、已结束     热门、最新、评价
				$("#courseStatus,#statType").on("click","li a",function(){
					//改变背景
					$(this).parent().parent().find("a").css("background","#e0e0e0");
					$(this).css("background","white");
					var _courseStatus = "";
					var _statType = "";
					if($(this).parent().parent().attr("id") == 'courseStatus'){
						_courseStatus = $(this).attr("value");
						_statType = $("input[name='statType']").val();
					}else if($(this).parent().parent().attr("id") == 'statType'){
						_statType = $(this).attr("value");
						_courseStatus = $("input[name='courseStatus']").val();
					}
					var _param = "cateId="+$("input[name='cateId']").val();
					_param += "&courseStatus="+_courseStatus;
					_param += "&statType="+_statType;
					_param += "&pageNo=1";//重置参数pageNo
					_param += "&searchKey="+$("#searchKey").val();
					$('#course_list').empty();
					_loadData(_param);
				});
				//点击加载更多
				$("#moreCourse").on("click",function(){
					var _param = "cateId="+$("input[name='cateId']").val();
					_param += "&courseStatus="+$("input[name='courseStatus']").val();
					_param += "&statType="+$("input[name='statType']").val();
					_param += "&pageNo="+$("[name='pageNo']").val();//下一页的数据参数pageNo
					_param += "&searchKey="+$("#searchKey").val();
					_loadData(_param);
				});
				//默认加载一页数据
				$("#moreCourse").click();
			});
			
			function _loadData(_param){
				var _url = $.fn.getRootPath()+"/app/learning/learn!courseJson.htm?"+_param;
				$('#course_list').ajaxLoading('正在加载数据...');
				$.ajax({
					type: "POST",
					url: _url,
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						$(data.datas).each(function(i, course){
							var courseName = course.name.substring(0, 9);
							if(course.name.length > 9){
								courseName += "..."
							}
							var startTime = "";
							var endTime = "";
							if(course.startTime != ''){
								var startDateValue = new Date(course.startTime.time);
								startTime = startDateValue.getFullYear()+"-"+(startDateValue.getMonth()+1)+"-"+startDateValue.getDate();
							}
							if(course.endTime != ''){
								var endDateValue = new Date(course.endTime.time);
								endTime = endDateValue.getFullYear()+"-"+(endDateValue.getMonth()+1)+"-"+endDateValue.getDate();
							}
							var score = Math.round(course.score)==0?1:Math.round(course.score);
							var fileName = course.fileName;
							if(fileName == null){
								fileName = "default.jpg";
							}
							$('#course_list').append($('#template').html().fill(
								course.id,
								courseName,
								course.content,
								course.count,
								score,
								fileName,
								endTime==''?'永不过期':(startTime+' 至 '+endTime),
								course.createUserNameCN
							));
						});
						if(data.datas.length > 0){
							$("[name='cateId']").val(data.cateId);
							$("[name='statType']").val(data.statType);
							$("[name='courseStatus']").val(data.courseStatus);
							$("[name='pageNo']").val(parseInt(data.pageNo)+1);
						}
						$('#course_list').ajaxLoadEnd();
					}
				});
			}
		</script>
		
	</head>
<body style="">
	<!-- 隐藏字段 -->
	<input type="hidden" name="cateId" value="${cateId }">
	<input type="hidden" name="statType" value="">
	<input type="hidden" name="courseStatus" value="">
	<input type="hidden" name="pageNo" value="1">
	<div class="page_all" style="width: 100%;height:100%;">
		<div class="px_b_con" style="height: 100%;">
			<table>
				<tr>
				<td style="vertical-align: top;">
					<!-- 左边分类树 -->
					<jsp:include page="cateTree.jsp" flush="true">
						<jsp:param value="${cateJa }" name="cateJa"/>
					</jsp:include>
				</td>
				<td style="width: 100%;vertical-align: top;">
					<div class="px_b_right" style="width: 100%;">
						<div class="px_b_top">
							<div class="usbox">
								<div class="usboxdiv">
									<ul id="courseStatus">
										<li><a href="javascript:void(0);" value="1" class="whiteBack">全部</a></li>
										<li><a href="javascript:void(0);" value="2" class="grayBack">正在开课</a></li>
										<li><a href="javascript:void(0);" value="3" class="grayBack">即将开课</a></li>
										<li><a href="javascript:void(0);" value="4" class="grayBack">已结束</a></li>
									</ul>
								</div>
								<div class="usboxdiv">
									<ul style="margin-left: 50px;" id="statType">
										<li><a href="javascript:void(0);" value="hot" class="grayBack2">热门</a></li>
										<li><a href="javascript:void(0);" value="new" class="grayBack2">最新</a></li>
										<li><a href="javascript:void(0);" value="score" class="grayBack2">评价</a></li>
									</ul>
								</div>
								<div class="px_search" style="float: left;">
									<a href="javascript:void(0);">
										<div class="kbs-inputbox fl">
											<input id="searchKey" type="text" class="input-from" placeholder="全部" value="" >
											<img id="searchImg" src="${pageContext.request.contextPath}/resource/learning/images/search_03.png"  alt=""/>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="px_b_kc">
							<div class="px_b_box">
								<div class="usbox">
									<div class="px_b_d" style="margin-top:15px;height: 750px;width:100%;overflow-y: scroll;">
										<div class="px_hang">
											<ul id="course_list">
												<!-- 课程列表区域 -->
											</ul>
										</div>
									</div>
									<div style="height: 30px;width:100%;float: left;margin-top: 10px;">
										<input id="moreCourse" type="button" value="加载更多..." style="width: 100%;cursor: pointer;">
									</div>
									<!-- 展示模板 -->
									<div id="template" style="display:none">
										<li style="margin-top: 10px;">
											<div class="px_xk" _value="{0}" style="cursor: pointer;">
												<img src="${pageContext.request.contextPath }/doc/course/cover/{5}" />
												<div class="px_xk_con">
													<ul>
														<li class="fl" style="width: 100%;"><span style="width:55px;">课程名称:</span>{1}</li>
														<li class="fl"><span>创建人:</span>{7}</li>
														<li class="fl"><span style="width:55px;">学习人数:</span>{3}</li>
														<li style="width: 100%;">
															<span>期限:</span>
															{6}
														</li>
														<c:set var="scoreInt">
															<fmt:formatNumber type="number" value="${course.score }" maxFractionDigits="0"/>
														</c:set>
														<li class="fl" style="width: 100%;">
															<span>评分:</span>
															<div class="px_l_star{4}"></div>
														</li>
														<div style="clear:both;"></div>
													</ul>
												</div>
											</div>
										</li>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td>
				</tr>
			</table>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>
