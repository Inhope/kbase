<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 16px;
			}
			.inputflag{color:red;font-size: 14px;}
		</style>	
		<script type="text/javascript">
			
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" value="${exam.id }" name="exam_id"/>
			<input type="hidden" value="${examBatch.id }" name="examBatch.id"/>
			<table width="100%" cellspacing="0" cellpadding="0" class="box">
				<tr>
					<td>批次名称<span class="inputflag">*</span></td>
					<td><input type="text" style="width: 75%" readonly="readonly"  value="${examBatch.name }"/>
				</tr>
				<tr>
					<td width="18%">考试人员<span class="inputflag">*</span></td>
					<td><input type="text" style="width: 75%" readonly="readonly" value="${examBatch.scopenames }" id="scopenames" name="examBatch.scopenames" readonly="readonly"/>&nbsp;<input type="hidden" id="scopeids" name="examBatch.scopeids" value="${examBatch.scopeids }"/></td>
				</tr>
				<tr>
					<td>批次考试开始时间<span class="inputflag">*</span></td>
					<td><input type="text" style="width: 75%" readonly="readonly"  id="start_time" name="start_time"  class="Wdate" readonly="readonly" value='<s:date name="examBatch.start_time" format="yyyy-MM-dd HH:mm"/>'/>
				</tr>
				<tr>
					<td>批次考试结束时间<span class="inputflag">*</span></td>
					<td><input type="text" style="width: 75%" readonly="readonly"  id="end_time" name="end_time"  class="Wdate" readonly="readonly" value='<s:date  name="examBatch.end_time" format="yyyy-MM-dd HH:mm"/>'/></td>
				</tr>
				<tr>
					<td>考试评卷人:</td>
					<td><input type="text" style="width: 75%" readonly="readonly" value="${examBatch.marker_name }" id="marker_name" name="examBatch.marker_name" readonly="readonly"/>&nbsp;<input type="hidden" value="${examBatch.marker_id }" id="marker_id" name="examBatch.marker_id" /></td>
				</tr>
			</table>
		</form>
	</body>
</html>

