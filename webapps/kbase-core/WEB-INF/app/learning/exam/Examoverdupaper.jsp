<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<!-- layer -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<style type="text/css">
		.examtop_left ul{width:20%}
	</style>
</head>
<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top">
					<div class="usbox">
						<div><span style="font-weight:bold;margin-left:45%;font-size:14px">${map.examinee.paper.name }</span></div>
						<div class="examtop_left fl">
							<ul>
								<li>
									考生：
									<span>${map.examinee.user.userInfo.userChineseName }</span>
								</li>
							</ul>
							<ul>
								<li>
									部门：
									<span>${map.user.mainStation.dept.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总数：
									<span><s:property value='#request.map.totalques'/></span>
								</li>
							</ul>
							<ul>
								<li>
									交卷时限：
									<span>${map.examinee.exambatch.exam.finish_minutes}分钟</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总分：${map.examinee.paper.score}
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
								<s:set var="select_var" value="{'A','B','C','D','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									<s:if test="#request.map.singlequeslist.size>0">
										<div class="eaxm_title">
											单选题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.singlequeslist" var="coilques" status="ind1">
												<input type="hidden" id="signle_length" value="<s:property value='#request.map.singlequeslist.size()'/>" />
												<input type="hidden" id="signle_id_${ind1.index+1 }" value="${coilques.ques.id}" />
												<input type="hidden" id="signle_score_${ind1.index+1 }" value="${coilques.score}" />
												<div class="exam_a">
													<span>${ind1.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<s:iterator value="#request.coilques.ques.quesResults" var="resu" status="ind11">
															<li>
																<a href="javascript:void(0);"><img src="images/ks_06.jpg" alt="" />&nbsp;<input type="radio" id="signle_${ind1.index+1}_${ind11.index+1 }" name="signle_${ind1.index+1}" isright="${resu.isRight}" value="${resu.id}" />${select_var[ind11.index]}.${resu.content}</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.morequeslist.size>0">
										<div class="eaxm_title dx">
											多选题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.morequeslist" var="coilques" status="ind2">
												<input type="hidden" id="more_length" value="<s:property value='#request.map.morequeslist.size()'/>" />
												<input type="hidden" id="more_id_${ind2.index+1 }" value="${coilques.ques.id}" />
												<input type="hidden" id="more_score_${ind2.index+1 }" value="${coilques.score}" />
												<div class="exam_a">
													<span>${ind2.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<s:iterator value="#request.coilques.ques.quesResults" var="resu" status="ind22">
															<li>
																<a href="javascript:void(0);"><img src="images/ks_09.jpg" alt="" />&nbsp;<input type="checkbox" id="more_${ind2.index+1}_${ind22.index+1 }" name="more_${ind.index+1}_answer" isright="${resu.isRight}" value="${resu.id}" />${select_var[ind22.index]}.${resu.content}</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>		
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.judgequeslist.size>0">
										<div class="eaxm_title dx">
											判断题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.judgequeslist" var="coilques" status="ind3">
												<input type="hidden" id="judge_length" value="<s:property value='#request.map.judgequeslist.size()'/>" />
												<input type="hidden" id="judge_id_${ind3.index+1 }" value="${coilques.ques.id}" />
												<input type="hidden" id="judge_score_${ind3.index+1 }" value="${coilques.score}" />
												<div class="exam_b">
													<span>${ind3.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<li>
															<input id="judge_${ind3.index+1 }_1" name="judge_${ind3.index+1 }_answer" type="radio" value="1" isright="${coilques.ques.isRight}"/><a href="javascript:void(0);"><img src="images/ks_06.jpg" alt="" />&nbsp;对</a>
														</li>
														<li>
															<input id="judge_${ind3.index+1}_2" name="judge_${ind3.index+1 }_answer" type="radio" name="signleanswer" value="0" isright="${coilques.ques.isRight}"/><a href="javascript:void(0);"><img src="images/ks_03.jpg" alt="" />&nbsp;错</a>
														</li>
													</ul>
												</div>
											</s:iterator>	
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.blankqueslist.size>0">
										<div class="eaxm_title dx">
											填空
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.blankqueslist" var="coilques" status="ind4">
												<input type="hidden" id="blank_length" value="<s:property value='#request.map.blankqueslist.size()'/>" />
												<input type="hidden" id="blank_id_${ind4.index+1 }" value="${coilques.ques.id}" />
												<input type="hidden" id="blank_score_${ind4.index+1 }" value="${coilques.score}" />
												<div class="exam_b" id="blank_answer_${ind4.index+1}">
													<span>${ind4.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
												</div>
											</s:iterator>	
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.ansqueslist.size>0">
										<div class="eaxm_title dx">
											简答题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.ansqueslist" var="coilques" status="ind5">
												<input type="hidden" id="ans_length" value="<s:property value='#request.map.ansqueslist.size()'/>" />
												<input type="hidden" id="ans_id_${ind5.index+1 }" value="${coilques.ques.id}" />
												<input type="hidden" id="ans_score_${ind5.index+1 }" value="${coilques.score}" />
												<div class="exam_c">
													<span>${ind5.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" id="ans_${ind5.index+1}" cols="100%" class="text-exam"></textarea>
														</div>
													</div>
												</div>
											</s:iterator>
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</body>
</html>

