<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			
			
			function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/examinee!getExaminees.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			
			function seeinfo(id){
				parent.__kbs_layer_index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['查看试卷', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!findUserpaper.htm?id='+id},
					area: ['80%', '80%']
				});
				
			}
			
			function editscore(id){
				_markerscore =$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['编辑成绩', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!editscore.htm?id='+id},
					area: ['250px', '100px']
				});
			}
			
			$(function(){
					var deptTreeObject = {
						deptTypeEl : $('#dept_name'),
						ktreeEl : $('div.menuContent'),
						treeAttr : {
							view : {
								expandSpeed: ''
							},
							async : {
								enable : true,
								url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
								autoParam : ["id", "name=n", "level=lv", "bh"],
								otherParam : {}
							},
							callback : {
								onClick : function(event, treeId, treeNode) {
									deptTreeObject.deptTypeEl.val(treeNode.name);
									$('#dept_bh').val(treeNode.bh);
								}
							}
						},
						render : function() {
							var self = this;
							self.deptTypeEl.focus(function(e){
								self.ktreeEl.css({
									'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
									'left' : $(this).offset().left + 'px'
								});
								if(self.ktreeEl.is(':hidden'))
									self.ktreeEl.show();
							});
							
							$('*').bind('click', function(e){
								if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
									
								} else {
									if(!self.ktreeEl.is(':hidden'))
										self.ktreeEl.hide();
								}
							});
							
							self.deptTypeEl.bind('keydown', function(keyArg) {
								if(keyArg.keyCode == 8) {
									$(this).val('');
									$('#dept_id').val('');
								} else if(keyArg.keyCode == 13) {
									self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
								}
							});
							
							self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
								$(this).parent().parent().parent().hide();
							});
							
							$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
						}
					}
					
				
				deptTreeObject.render();
				$("#topublish").click(function(){
					var checkids = [];
					$("input:checkbox[name='examrecord_id']:checked").each(function(i, item){
						checkids.push($(this).val());
					});
					if(checkids.length<=0){
						layer.alert("请至少选择一条数据发布成绩!", -1);
						return false;
					}$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/examinee!publistscore.htm',
							data :{"id":checkids.join(',')},
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1, function(){
										window.location.reload();
									});
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
				});
				
			})	
		</script>
	</head>
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border: 0px;">
					<form id="form0" action="" method="post">
					  <div class="content_right_bottom">
						<div class="gonggao_titile">
							<div class="gonggao_titile_right">
									<a href="#" id="topublish">发布成绩</a>
								</div>
							</div>
							<div class="yonghu_titile">
								<ul>
									<li>
										<input type="hidden" value="${examineeinfo.exam_id }" name="examineeinfo.exam_id" />
										姓名：<input type="text"  name="examineeinfo.user_name" value="${examineeinfo.user_name }"/>
									</li>
									<li>
										部门：<input type="text" id="dept_name" name ="examineeinfo.dept_name" value="${examineeinfo.dept_name}" onclick="showMenu('menuContent','dept_name')"  style="width:150px"/>
											<input id="dept_bh"  type="hidden" name="examineeinfo.dept_bh" value="${examineeinfo.dept_bh }"/>
									</li>
									<li>
										成绩:
										<select name="examineeinfo.score_status">
											<option value="">--请选择--</option>
											<option value="0" <c:if test="${examineeinfo.score_status==0 }">selected="selected"</c:if>>合格</option>
											<option value="1" <c:if test="${examineeinfo.score_status==1 }">selected="selected"</c:if>>不合格</option>
										</select>
									</li>
									<li>
										试卷：<input type="text" name="examineeinfo.paper_name" value="${examineeinfo.paper_name }"/>
									</li>
									<li>
										成绩发布:
										<select name="examineeinfo.score_publish">
											<option value="">--请选择--</option>
											<option <c:if test="${examineeinfo.score_publish==1 }">selected="selected"</c:if> value="1">已发布</option>
											<option <c:if test="${examineeinfo.score_publish==0 }">selected="selected"</c:if> value="0">未发布</option>
										</select>
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" /> </a>
									</li>
								</ul>
							</div>
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="5%">
												<input type="checkbox" value="" onclick="checkAll()" id="" />
											</td>
											<td width="10%">
												姓名
											</td>
											<td width="10%">
												部门
											</td>
											<td width="10%">
												成绩
											</td>
											<td width="10%">
												参加考试时间
											</td>
											<td width="10%">
												考试结束时间
											</td>
											<td width="5%">
												考试时长
											</td>
											<td width="10%">
												成绩发布
											</td>
											<td width="10%"></td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td><input type="checkbox" name="examrecord_id" value="${va.examRecord.id  }"/></td>
												<td>${va.user.userInfo.userChineseName}</td>
												<td>${va.dept.name}</td>
												<td>${va.examRecord.score }</td>
												<td>${va.examRecord.start_time }</td>
												<td>${va.examRecord.end_time }</td>
												<td>${va.examRecord.exam_interval }</td>
												<td><s:if test="#va.examRecord.is_publish==0">未发布</s:if><s:if test="#va.examRecord.is_publish==1">已发布</s:if></td>
												<td><a href="#" onclick="seeinfo('${va.examRecord.id  }')">查看</a></td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="9">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
									<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
										<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

