<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<!-- layer -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	
	<script type="text/javascript"><!--
		  var detailids=[];
		  var scores=[];
		  $(function(){
		   var blanklength = $('#blank_length').val();
	  	   for(var i=1;i<=blanklength;i++){
	  	   		var answer = $('#blank_answer_'+i).val();
			 	if(answer!=''){
			 		$('#blank_score_'+i+' span input').each(function(index,element){
						$(this).val(answer.split('|')[index]);				 		
			 		});
			 	}
			 }  
			  $("#tosave").click(function(){
				  //填空题判断
				  var blanklength = $('#blank_length').val();
				  for(var i=1;i<=blanklength;i++){
				  		var sumscore = $('#blank_referscore_'+i+'').val();
				  		if($.trim($('#score_blank_'+i+'').val())!=''){
				  			if(isNaN($('#score_blank_'+i+'').val())){
				  				layer.alert("填空题的分数请填写数字!", -1);
								return false;
							}else if(sumscore-$('#score_blank_'+i+'').val()<0){
								alert(sumscore);alert($('#score_blank_'+i+'').val());
								layer.alert("填空题中存在分数高于该题的总分!", -1);
								return false;
							}else{
								detailids.push($('#blank_id_'+i+'').val());//记录问题答案ID
				 				scores.push($('#score_blank_'+i+'').val());
							}
				  			
				  		}
				  }
				  //简答题判断
				  var anslength = $('#ans_length').val();
				  for(var i=1;i<=anslength;i++){
				  	var scorecount = $('#ans_referscore_'+i+'').val();
				  	if($.trim($('#score_ans_'+i+'').val())!=''){
				  		if(isNaN($('#score_ans_'+i+'').val())){
							layer.alert("简答题中分数请填写数字!", -1);
							return false;
						}else if(scorecount-$('#score_ans_'+i+'').val()<0){
							layer.alert("简答题中存在分数高于该题的总分!", -1);
							return false;
						}else{
							detailids.push($('#ans_id_'+i+'').val());//记录问题答案ID
					 		scores.push($('#score_ans_'+i+'').val());
						}
					 	
				 	}
				  }
				  if(scores.checkids<=0){
					 layer.alert("请至少给一题打分后提交!", -1);
				  }else{
					  $.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/examinee!savemarkepaper.htm',
							data :{"record_id":'${record_id}',"comment":$('#comment').val(),"detailids":detailids.join(','),"scores":scores.join(',')},
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1, function(){
										parent.window.location.reload();
										parent.layer.close(parent._markerpaer);
									});
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}	
			  });
		});	 
		
		//题目显示或隐藏
		function toggleinfo(info_id,img_id){
			if($('#'+info_id+'').is(":hidden")){
				$('#'+info_id+'').show();
				$('#'+img_id+'').attr('src','${pageContext.request.contextPath}/resource/learning/images/f_05.jpg');
			}else{
				$('#'+info_id+'').hide();
				$('#'+img_id+'').attr('src','${pageContext.request.contextPath}/resource/learning/images/hover_03.gif');
			}
		} 	
	</script>	
</head>
<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top">
					<div class="usbox">
						<div class="examtop_left fl">

							<ul>
								<li>
									考生：
									<span>${map.examinee.user.userInfo.userChineseName }</span>
								</li>
								<li>
									部门：
									<span>${map.user.mainStation.dept.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									考卷名称：
									<span>${map.examinee.paper.name }</span>
								</li>
								<li></li>
							</ul>
							<ul>
								<li>
									考题总数：
									<span>${map.qasize}</span>
								</li>
								<li>
									交卷时限：
									<span>${map.examinee.exambatch.exam.finish_minutes}分钟</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总分：
									<span class="px_red">${map.examinee.paper.score}</span>
								</li>
								<li></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
								<s:set var="select_var" value="{'A','B','C','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									<s:if test="#request.map.singlequeslist.size>0">
										<div class="eaxm_title" style="margin-bottom: 15px;">
											<div class="fl">单选题 </div>
				                          	<div class="eaxm_title_r fr">
                             					<span>
                             						共
                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
                             							<s:property value="#request.map.singlequeslist.size"/>
                             						</font>
                             						题
													<a href="javascript:void(0);" onclick="toggleinfo('singleques','singleimg')">
														<img id="singleimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
														onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
													</a>
                             					</span>
				                          	</div>
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="singleques">
											<div class="usbox">
											<s:iterator value="#request.map.singlequeslist" var="detail" status="ind1">
												<input type="hidden" id="signle_length" value="<s:property value='#request.map.singlequeslist.size()'/>" />
												<input type="hidden" id="signle_id_${ind1.index+1 }" value="${detail.ques_id}" />
												<input type="hidden" id="signle_score_${ind1.index+1 }" value="${detail.referScore}" />
												<div class="exam_a">
													<span>
														${ind1.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore})
														<s:if test="#detail.score==#detail.referScore">
															<span class="incorrect">正确</span>
														</s:if>
														<s:else>
															<span class="correct">错误</span>
														</s:else>
													</span>
													<ul>
														<s:iterator value="#request.detail.quesinfo.answer" var="resu" status="ind11">
															<li>
																<a href="javascript:void(0);">&nbsp;
																	<s:if test="#request.detail.answer==#request.resu.id">
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg" alt="" />&nbsp;
																	</s:if>
																	<s:else>
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;
																	</s:else>
																	<span style="<s:if test="#request.resu.isright==1">color:green;</s:if>">
																		${select_var[ind11.index]}.${resu.name}
																	</span>
																</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.morequeslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl">多选题</div>
											<div class="eaxm_title_r fr">
                             					<span>
                             						共
                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
                             							<s:property value="#request.map.morequeslist.size"/>
                             						</font>
                             						题
													<a href="javascript:void(0);" onclick="toggleinfo('moreques','morequesimg')">
														<img id="morequesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
														onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
													</a>
                             					</span>
											</div>
											<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="moreques">
											<div class="usbox">
											<s:iterator value="#request.map.morequeslist" var="detail" status="ind2">
												<input type="hidden" id="more_length" value="<s:property value='#request.map.morequeslist.size()'/>" />
												<input type="hidden" id="more_id_${ind2.index+1 }" value="${detail.ques_id}" />
												<input type="hidden" id="more_score_${ind2.index+1 }" value="${detail.referScore}" />
												<div class="exam_a">
													<span>
														${ind2.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore})
														<s:if test="#detail.score==#detail.referScore">
															<span class="incorrect">正确</span>
														</s:if>
														<s:else>
															<span class="correct">错误</span>
														</s:else>
													</span>
													<ul>
														<s:iterator value="#request.detail.quesinfo.answer" var="resu" status="ind22">
															<li>
																<a href="javascript:void(0);">
																<c:choose>
																	<c:when test="${fn:contains(detail.answer,resu.id ) }">
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_09.jpg" alt="" />&nbsp;
																	</c:when>
																	<c:otherwise>
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_12.jpg" alt="" />&nbsp;
																	</c:otherwise>
																</c:choose>
																<span style="<s:if test="#request.resu.isright==1">color:green;</s:if>">
																	${select_var[ind22.index]}.${resu.name}
																</span>
																</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>		
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.judgequeslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl">判断题</div>
				                          	<div class="eaxm_title_r fr">
                             					<span>
                             						共
                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
                             							<s:property value="#request.map.judgequeslist.size"/>
                             						</font>
                             						题
													<a href="javascript:void(0);" onclick="toggleinfo('judgeques','judgequesimg')">
														<img id="judgequesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
														onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
													</a>
                             					</span>
				                          	</div>
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con"  id="judgeques">
											<div class="usbox">
											<s:iterator value="#request.map.judgequeslist" var="detail" status="ind3">
												<div class="exam_b">
													<span>
														${ind3.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore})
														<s:if test="#detail.score==#detail.referScore">
															<span class="incorrect">正确</span>
														</s:if>
														<s:else>
															<span class="correct">错误</span>
														</s:else>
													</span>
													<ul>
														<li>
															<a href="javascript:void(0);">
																<s:if test="#request.detail.answer==1">
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg"/>&nbsp;
																</s:if>
																<s:else>
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>&nbsp;
																</s:else>
																<span style="<s:if test="#request.detail.quesinfo.isright==1">color:green;</s:if>">正确</span>
															</a>
														</li>
														<li>
															<a href="javascript:void(0);"><s:if test="#request.detail.answer==0"><img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg" alt="" />&nbsp;</s:if><s:else><img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;</s:else><span <s:if test="#request.detail.quesinfo.isright==0">style="color:green;"</s:if>>错误</span></a>
														</li>
													</ul>
												</div>
											</s:iterator>	
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.blankqueslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl">填空</div>
				                          	<div class="eaxm_title_r fr">
                             					<span>
                             						共
                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
														<s:property value="#request.map.blankqueslist.size"/>
                             						</font>
                             						题
													<a href="javascript:void(0);" onclick="toggleinfo('blankques','blankquesimg')">
														<img id="blankquesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
														onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
													</a>
                             					</span>
				                          	</div>
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con"  id="blankques">
											<div class="usbox">
											<s:iterator value="#request.map.blankqueslist" var="detail" status="ind4">
												<input type="hidden" id="blank_length" value="<s:property value='#request.map.blankqueslist.size()'/>" />
												<input type="hidden" id="blank_id_${ind4.index+1 }" value="${detail.id}" />
												<input type="hidden" id="blank_answer_${ind4.index+1 }" value="${detail.answer}" />
												<input type="hidden" id="blank_referscore_${ind4.index+1 }" value="${detail.referScore}" />
												<div class="exam_b" id="blank_score_${ind4.index+1 }">
													<span>
														${ind4.index+1}.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore})
													</span>
													<ul>
														<li style="width: 100%;">
														<s:iterator value="#detail.quesinfo.answer" var="resu">
															<span>
																<span class="incorrect" style="display: inline;margin-right:10px;">${resu.name }</span>
															</span>
														</s:iterator>
														</li>
													</ul>
													<div class="point fr">
				                                     	分数 &nbsp;
				                                     	<input type="text" class="point_exam" id="score_blank_${ind5.index+1 }" value="${detail.score}"/>
				                                 	</div>
												</div>
												<div style="clear: both;"></div>
											</s:iterator>	
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.ansqueslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl">简答题</div>
				                          	<div class="eaxm_title_r fr">
                             					<span>
                             						共
                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
														<s:property value="#request.map.ansqueslist.size"/>
                             						</font>
                             						题
													<a href="javascript:void(0);" onclick="toggleinfo('ansques','ansquesimg')">
														<img id="ansquesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
														onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
													</a>
                             					</span>
				                          	</div>
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con"  id="ansques">
											<div class="usbox">
											<s:iterator value="#request.map.ansqueslist" var="detail" status="ind5">
												<input type="hidden" id="ans_length" value="<s:property value='#request.map.ansqueslist.size()'/>" />
												<input type="hidden" id="ans_id_${ind5.index+1 }" value="${detail.id}" />
												<input type="hidden" id="ans_referscore_${ind5.index+1 }" value="${detail.referScore}" />
												<div class="exam_c">
													<span>
														${ind5.index+1 }.${detail.quesinfo.name}&nbsp;&nbsp;&nbsp;(${detail.referScore})
													</span>
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" id="ans_${ind5.index+1}" cols="100%" class="text-exam">${detail.answer}</textarea>
														</div>
													</div>
													<div class="exam_b">
														<ul>
															<li style="width: 100%;">
															<s:iterator value="#detail.quesinfo.answer" var="resu">
																<span>
																	<span class="incorrect" style="display: inline;margin-right:10px;">${resu.name }</span>
																</span>
															</s:iterator>
															</li>
														</ul>
													</div>
													<div class="point fr">
				                                     	分数 &nbsp;
				                                     	<input type="text" class="point_exam" id="score_ans_${ind5.index+1}" value="${detail.score}"/>
				                                 	</div>
												</div>
												<div style="clear: both;"></div>
											</s:iterator>
											</div>
										</div>
									</s:if>
									
									<div class="eaxm_title dx">
											点评
										</div>
										<div class="exam_con">
											<div class="usbox">
												<div class="exam_c">
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" id="comment" cols="100%" class="text-exam">${map.examinee.examRecord.comment }</textarea>
														</div>
													</div>
												</div>
												<div style="clear: both;"></div>
											</div>
										</div>
								</div>
								<div class="btn_st" id="tosave">
		                           <a href="javascript:void(0);">提交试题 </a>
		                       </div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</body>
</html>

