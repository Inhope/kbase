<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<!-- layer -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<script type="text/javascript">	
	$(function(){
		$("#tosave").click(function(){
			var score = $('#score').val();	
			if(isNaN(score)){
		    	layer.alert("请填写数字", -1); 
		    	return false;
		    }if(score==''){
		    	layer.alert("分数不能为空", -1); 
		    	return false;
		    }if (!isNaN(score) && score < 0) {
			    layer.alert("分数不能为负！", -1);
			    return false;
			}
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/learning/examinee!savescore.htm',
				data :{'id':'${id}','score':score},
				async: true,
				dataType : 'json',
				success : function(data){
					if(data.result == true){
						parent.window.location.reload();
						parent.layer.close(parent._markerscore);
					}else{
						layer.alert("操作失败!", -1);
					}
				},
				error : function(msg){
					layer.alert("网络错误，请稍后再试!", -1);
				}
			});
		});
	});	
	</script>
</head>
<body>
	<table width="100%" cellspacing="0" cellpadding="0" class="box">
		<tr>
			<td>分数</td>
			<td><input type="text" value="" id="score" style="width:95%"/></td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" id="tosave" value="提交" />
			</td>
		</tr>
	</table>
</body>
</html>

