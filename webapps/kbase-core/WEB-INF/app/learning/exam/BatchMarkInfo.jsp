<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 16px;
			}
		</style>	
		<script type="text/javascript">
			var scores = [];
			var record_ids = [];
			var ques_ids = [];
			$(function(){
				$("#tosave").click(function(){
					var message="";
					$("input[id^='score_']").each(function(i,items){
						if($.trim($(this).val())!=''){
							if(isNaN($(this).val())){
								message="请正确填写数据!";
								return false;
							}else if($('#sumscore').val()<$(this).val()){
								message="分数不能高于该题的总分!";
								return false;
							}else{
								scores.push($(this).val());
								ques_ids.push($('#detail_id_'+i+'').val());
								record_ids.push($('#record_id_'+i+'').val());
							}
							
						}
					});
					if(message!=''){
						layer.alert(message, -1);
						return false;
					}else if(scores.checkids<=0){
						layer.alert("请至少给一人打分!", -1);
						return false;
					}else{
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/exam-record!savebatchexam.htm',
							data :{"ques_ids":ques_ids.join(','),"scores":scores.join(','),"record_ids":record_ids.join(',')},
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1, function(){
									    parent.window.location.reload();
										parent.$('#examinfo').attr("src","");
									});
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				});
			});	
		</script>
	</head>
	<body>
	  <div  style="min-height:450px;">
		<div style="margin-top:30px;">
			<input type="hidden" id="sumscore" value="${examRecord.examRecordDetail.quesinfo.score }" />
			题目:${examRecord.examRecordDetail.quesinfo.content }&nbsp;&nbsp;(${examRecord.examRecordDetail.quesinfo.score })
		</div>
		<table width="100%" cellspacing="0" cellpadding="0" class="box">
			<tr>
				<td align="center" width="10%">姓名</td>
				<td align="center" width="80%">答题内容</td>
				<td align="center" width="10%">得分</td>
			</tr>
			<s:iterator value="#request.recordlist" var="record" status="ind">
				<tr>
					<td align="center">${record.examinee.user.userInfo.userChineseName }</td>
					<td align="center">${record.examRecordDetail.answer }</td>
					<input type="hidden" id="detail_id_${ind.index }" value="${record.examRecordDetail.id }" />
					<input type="hidden" id="record_id_${ind.index }" value="${record.id}" />
					<td align="center"><input id="score_${ind.index }" type="text" /></td>
				</tr>
			</s:iterator>
			<tr>
				<td colspan="3" align="right">
					<input type="button" id="tosave" value="提交" />
				</td>
			</tr>
		</table>
	 </div>	
	</body>
</html>

