<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			function totest(id){
				parent.__kbs_layer_index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['在线考试', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath() + '/app/learning/examinee!findexampaper.htm?id='+id},
					area: ['100%', '100%']
				});
			}
			
			function seeinfo(id){
				parent.__kbs_layer_index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['查看试卷', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!findUserpaper.htm?id='+id},
					area: ['95%', '95%']
				});
				
			}
			
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/examinee.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
		</script>
	</head>
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border: 0px;">
					<form id="form0" action="" method="post">
						<div class="content_right_bottom">
							<div class="yonghu_titile">
								<ul>
									<li>考试名称:<input type="text" name="examineeinfo.exam_name" value="${examineeinfo.exam_name }" /></li>
									<li>
										状态:          
										<select name="examineeinfo.status">
											<option value="">--请选择--</option>
											<option value="0">待考试</option>
											<option value="1">已完成</option>
											<option value="2">未考已过期</option>
										</select>
									</li>
									<li>
										合格:
										<select name="examineeinfo.score_status">
											<option value="">--请选择--</option>
											<option value="1">合格</option>
											<option value="0">不合格</option>
										</select>
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置"/> </a>
										<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" /> </a>
									</li>
								</ul>
							</div>

							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="15%">
												考试名称
											</td>
											<td width="15%">
												考试开始时间
											</td>
											<td width="15%">
												考试结束时间
											</td>
											<td width="5%">
												状态
											</td>
											<td width="5%">
												分数
											</td>
											<td width="5%">
												合格
											</td>
											<td width="10%">
												操作
											</td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td>${va.exambatch.exam.name }</td>
												<td><fmt:formatDate  value="${va.exambatch.start_time }" pattern="yyyy-MM-dd HH:mm" /></td>
												<td><fmt:formatDate  value="${va.exambatch.end_time}" pattern="yyyy-MM-dd HH:mm" /></td>
												<td><s:if test="#va.status==0">待考试</s:if><s:elseif test="#va.status==1">已完成</s:elseif><s:else>未考试,已过期</s:else></td>
												<td><s:if test="#va.examRecord.is_publish==1">${va.examRecord.score }</s:if></td>
												<td>
													<s:if test="#va.examRecord.is_publish==1">
														<s:if test="#va.examRecord.score<#va.paper.scorePass">未合格</s:if><s:if test="#va.examRecord.score>=#va.paper.scorePass">合格</s:if></td>
													</s:if>
												<td><s:if test="#va.status==0"><a href="#" onclick="totest('${va.id }')">考试</a></s:if><s:else><a href="#" onclick="seeinfo('${va.examRecord.id}')">查看</a></s:else></td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="9">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

