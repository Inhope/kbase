<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<script type="text/javascript">
		function showexaminfo(ques_id){
			$('#examinfo').attr("src",$.fn.getRootPath() + '/app/learning/exam-record!findbatchquesinfo.htm?exam_id='+'${exam_id}'+'&ques_id='+ques_id);
		}
		//展开目录
		function loadMoreCate(_this,ques_id){
				//展开图标样式变换和自己分类隐藏显示
				if($("#"+_this+"").hasClass("pc_plus")){
					$("#"+_this+"").removeClass("pc_plus");
					$("#"+_this+"").addClass("pc_minus");
					$("#"+ques_id+"").show();
				}else if($("#"+_this+"").hasClass("pc_minus")){
					$("#"+_this+"").removeClass("pc_minus");
					$("#"+_this+"").addClass("pc_plus");
					$("#"+ques_id+"").hide();
				}
		}		
	</script>	
</head>
	<body>
		<div class="px_b_left">
			<div class="px_b2_left" style="min-height:490px;">
				<!-- 
					<div class="px_b_title">
						<span><img src="${pageContext.request.contextPath}/resource/learning/images/px_ks_03.jpg" />&nbsp;</span>
						<span id="allCourse" style="cursor: pointer;"></span>
					</div>
					 -->
				<div class="px_b_con">
					<ul class="pc_b_a">
						<li onclick="loadMoreCate('blank','blankques')">
							<a href="javascript:void(0);"> <span id="blank" class="pc_plus"
								></span> <font>填空题</font> </a>
						</li>
						<div id="blankques" style="display: none;">
							<ul style="background: white;">
								<s:iterator value="#request.blankqueslist" var="blankques">
									<li>
										<a href="javascript:void(0);" title="${blankques.ques_name }" onclick="showexaminfo('${blankques.ques_id }')"> <font><s:property value="#blankques.ques_name.substring(0,7)+'.....'"/>(${blankques.ques_count})</font> </a>
									</li>
								</s:iterator>
							</ul>
						</div>
						<li onclick="loadMoreCate('ans','ansques')">
							<a href="javascript:void(0);"> <span id="ans" class="pc_plus" ></span> <font>问答题</font> </a>
						</li>
						<div id="ansques" style="display: none;">
							<ul  style="background: white;">
								<s:iterator value="#request.askqueslist" var="askques">
									<li>
										<a href="javascript:void(0);" title="${askques.ques_name}" onclick="showexaminfo('${askques.ques_id }')"> <font><s:property value="#askques.ques_name.substring(0,7)+'....'"/>(${askques.ques_count})</font> </a>
									</li>
								</s:iterator>
							</ul>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div>
	<div class="px_b_right">
		<iframe src="" height="100%" width="100%"  style="min-height:490px;" id="examinfo"></iframe>
	</div>
	</body>
</html>