<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 16px;
			}
		</style>	
		<script type="text/javascript">
			$(function(){
				$('#cateNames_select').click(function(){
			       $.kbase.picker.singleExamCate({returnField:"cateNames_select|cateIds_select"});
			    });
				var ids = [];  
			  	$("#tosave").click(function(){
			  		$("input:checkbox[id='exam_id']").each(function(i, item){
						ids.push($(this).val());
					});
					$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + url,
							async: false,
							data:{"cata_id":$('#cateIds_select').val()},
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1);
									parent.layer.close(parent.__kbs_layer_index);
								}else{
									layer.alert("删除失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});	
			  	});
			});  
		</script>
	</head>
	<body>
		<div>
			<form name="form1" id="form1" method="post" action="">
				<table width="100%" cellspacing="0" cellpadding="0" class="box">
					<tr>
						<td>选择考试分类:</td>
						<td><input type="text" id="cateNames_select" value="${exam.examCate.name }" name="exam.examCate.name" style="width:92%"/>
						<input type="hidden" id="cateIds_select" value="${exam.examCate.id }" name="exam.examCate.id" />
						</td>
					</tr>
					<tr>
						<td>已选试卷:</td>
						<td>
							<s:iterator value="#request.examlist" var="exam" status="ind">
								<input type="hidden" id="exam_id" value="${exam.id }" />
								${ind.index+1}.${exam.name }</br>
							</s:iterator>
						</td>
					</tr>
					<tr>
						<td align="right" colspan="2">
							<input type="button" value="保存" id="btnsave">
						</td>
					</tr>
				</table>
			  </form>	
			</div>		
	</body>
</html>

