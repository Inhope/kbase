<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<style type="text/css">
	.rTab th {text-align: left}
	.rTab td {float: left;margin-left: 25px;}
	.rTab td input {line-height: 20px;}
	.fkui_aa2{width:67px;height:28px;line-height:28px;float:right;color:#FFF;background:url(${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/fankui/ggzs_bg2.jpg) no-repeat;border:none;cursor:pointer;}
	
	a:focus{outline:none;}
	.ul_Cate{}
	.ul_Cate>ul>li{text-align: left;margin-left: 5px;margin-top: 5px;}
</style>		

<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PaperCate2.js"></script>
<script type="text/javascript">
/*保存分类*/
TreeUtils.setting({
	treeId: 'treeDemo',/*树id*/
	rootNodeId: 'root',/*树根节点id*/
	treeURL: $.fn.getRootPath()+ '/app/learning/exam-cate!examcatetree.htm',/*树数据初始化请求地址*/
	shadeId: 'rContent',/*弹窗id*/
	beforeOpenShade: function(flag, node, treeObj){/*打开弹窗前操作*/
		$('#err_msg').text('');
		if(node){
			if(flag == 'edit'){
				$('#pname').val(node.pName);
				$('#pid').val(node.pId);
				$('#name').val(node.name);
				$('#name2').val(node.name);
				$('#id').val(node.id);
			} else if(flag == 'add'){
				$('#pname').val(node.name);
				$('#pid').val(node.id);
				$('#name').val('');
				$('#name2').val('');
				$('#id').val('');
			}
		}
		return true;
	},
	btns: [/*按钮*/
		{id: 'delBtn' , title: '删除', url: '/app/learning/paper-cate!delete.htm', fn: 'drop'},
		{id: 'updateBtn', title: '编辑', fn: 'edit', params:{height: 200}},
		{id: 'addBtn' , title: '新增', fn: 'add', params:{height: 200}}
	]
});

/*保存分类*/
function paperCateSave(){
	var treeObj = TreeUtils.getZTreeObj();
	if(treeObj){
		var id = $('#id').val(),
		name = $('#name').val().replace(/(^\s*)|(\s*$)/g,''),/*去掉首尾空格*/
		name2 = $('#name2').val(),
		pId = $('#pid').val();
		if(pId=='root') pId='';
		
		/*分类名称不应为空*/
		if(name == '') {
			$('#err_msg').text('分类名称不应为空!');
			return false;
		}
		/*节点名称发生改变，验证是否同级下重名*/
		var targetNode = treeObj.getNodeByParam("id", pId, null);/*目标节点*/
		var node1 = treeObj.getNodesByFilter(function(nd){
			if(nd.pId == pId && nd.name == name) return true;
			return false;
		}, true, targetNode);
		/*同名岗位与当前需要编辑的岗位不是同一个*/
		if(node1 && node1.id != id){
			$('#err_msg').text('同级分类下不应存在相同名称的分类!');
			return false;
		}
		/*发送请求，修改数据*/
		var params = {'id': id, 'name': name, 'pid': pId};
		var loadindex = layer.load('提交中…');
		$.post($.fn.getRootPath() + '/app/learning/exam-cate!addorupdate.htm', params, 
			function(jsonResult) {
				layer.close(loadindex);
				layer.alert(jsonResult.msg, -1, function(){
					if (jsonResult.rst) {
						var node = jsonResult.node;
						if(id){/*更新节点*/
							var node_ = treeObj.getNodeByParam("id", id, null);
							node_.name = node.name;
							node_.remark = node.remark;
							node_.icon = node.icon;
							treeObj.updateNode(node_);
						} else {/*新增节点*/
							treeObj.addNodes(targetNode, node);
						}
					}
					layer.closeAll();
				});
		}, "json");
	}
}

$(document).ready(function(){
	/*渲染树高度*/
	$('#treeDemo').css('height', $(window).height()-50);
});
</script>
<div class="ul_Cate" style="background-color: #f6f6f6;">
	<ul style="margin-top: -5px;">
		<li style="height: 2px;"></li>
		<li>
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="addBtn" >新增</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="updateBtn" >编辑</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="delBtn" >删除</a>  
		</li>
		<li>
			<ul id="treeDemo" class="ztree" style="width:150px; overflow: auto;"></ul>
		</li>
	</ul>
</div>
<div id="rContent" style="border-bottom: 1px solid #e3e3e3;display:none;z-index:198910151;">
	<table class="rTab" width="100%">
		<tr>
			<th>所属分类</th>
		</tr>
		<tr>
			<td>
				<input id="pname" type="text" readonly="readonly"/>
				<input id="pid" type="hidden" />
			</td>
		</tr>
		<tr>
			<th>当前分类</th>
		</tr>
		<tr>
			<td>
				<input id="name" type="text" />
				<input id="id" type="hidden" />
			</td>
		</tr>
		<tr>
			<td style="float: right;margin-right: 15px;margin-top: 5px;">
				<a href="###" id="addOrEdt"><span class="fkui_aa2" style="text-align: center;">提交</span></a>
			</td>
		</tr>
	</table>
</div>
