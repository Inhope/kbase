<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<style type="text/css">
	.rTab th {text-align: left}
	.rTab td {float: left;margin-left: 25px;}
	.rTab td input {line-height: 20px;}
	.fkui_aa2{width:67px;height:28px;line-height:28px;float:right;color:#FFF;background:url(${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/fankui/ggzs_bg2.jpg) no-repeat;border:none;cursor:pointer;}
	
	a:focus{outline:none;}
	.ul_Cate{}
	.ul_Cate>ul>li{text-align: left;margin-left: 5px;margin-top: 5px;}
	
	.ul_PaperCate{width: 100%;}
	.ul_PaperCate>ul{width: 100%;}
	.ul_PaperCate>ul>li{text-align: left;margin-left: 5px;margin-top: 5px;}
	.box th{width: 30%;text-align: right;}
	.box td{width: 70%;}
	.box td>input{width: 97%;}
</style>

<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript">
var setting = {
	edit: {
		enable: true,
		showRemoveBtn: false,
		showRenameBtn: false
	},
	async: {
		enable: true,
		url: "${pageContext.request.contextPath}/app/learning/exam-cate!getexamcatetree.htm",
		autoParam:["id"]
	},
	view: {
		dblClickExpand: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},callback: {			
		onRightClick: onRightClick,
		onClick: zTreeOnClick
	}				
};

//树右击事件
function onRightClick(event, treeId, treeNode) {
	
	
}
//树点击事件
function zTreeOnClick(e, treeId, treeNode){
	$("#selectReset").click();
	$("#examCate_bh").val(treeNode.bh);
	$("#pageNo").val(1);
	pageClick(1);
}
//新增或者编辑
function saveTreeNode(newNode,node,flag){
	if(newNode.name == ''){
		layer.alert("节点名称不为空!", -1);
		return false;
	}
	if(flag == "edit"){
		if(newNode.id==''){
			layer.alert("请选择要编辑的节点!", -1);
			return false;
		}
	}
	
	//判断同级分类是否有重复的分类
	var node_p = zTree.getNodeByParam("id", newNode.parent_id, null);
	var node_ = zTree.getNodesByFilter(function(node){
		if(node.pId ==  newNode.parent_id && node.name == newNode.name){
			return true;
		}else{
			return false;
		}
	}, true, node_p);
	if(node_ != null && node_.id != newNode.id){
		layer.alert("同级分类下不应存在相同分类!", -1);
		return false;
	}
	
	$.ajax({
		type : 'post',
		url : $.fn.getRootPath() + '/app/learning/exam-cate!addorupdate.htm', 
		data : newNode,
		async: true,
		dataType : 'json',
		success : function(data){
			if(data.result == true){
				newNode = data.content;
				if(flag == "add"){
					zTree.addNodes(node, newNode);
				}else if(flag == "edit"){
					var newNode_ = zTree.getNodeByParam("id", newNode.id, null);
					newNode_.name = newNode.name;
					newNode_.pId = newNode.pId;
					newNode_.icon = newNode.icon;
					//更新节点
					zTree.updateNode(newNode_);
				} 
				layer.close(window.__kbs_layer_index);
				layer.alert("操作成功!", -1);
			}else{
				layer.alert("操作失败!", -1);
			}
		},
		error : function(msg){
			newNode = null;
			layer.alert("网络错误，请稍后再试!", -1);
		}
	});
}

//获取弹窗数据
function getNode(){
	var id = $("#id").val();
	var name = $("#name").val();
	var pid = $("#pid").val();
	var pname = $("#pname").text();
	var remark = $("#remark").val();
	//if(pid=='root'){pid=''};
	return { 'id':id,'name':name,'parent_id':pid,'pname':pname,'remark':remark };
}

var zTree;
$(document).ready(function(){
	
	$.ajax({
		type: "POST",
		dataType : 'json',
		url: $.fn.getRootPath()+ '/app/learning/exam-cate!getexamcatetree.htm',
		async: false,
		success: function(data){
			zTree=$.fn.zTree.init($("#treeDemo"), setting, data);
		}
	});
	
	//新增
	$("#btnNewCate").click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			
			$('#pname').text(node.name);
			$('#pid').val(node.id);
			$('#name').val('');
			$('#id').val('');
			$('#remark').val('');
			
			//绑定新增事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				saveTreeNode(getNode(),node,"add");
			})
			
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: "新增",
			    area: ['auto', 'auto'],
				offset: ['100px','400px'],
			    border: [3, 0.3, '#000'], //去掉默认边框
			    shade: [0.5, '#000',true], //去掉遮罩
			    closeBtn: [0, true], //去掉默认关闭按钮
			    //shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	
	//编辑
	$('#btnEditCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			
			if (node.id=='root'){
				layer.alert('无法编辑根节点', -1);
				return false;
			}
			
			$('#pname').text(node.pName);
			$('#pid').val(node.pId);
			$('#name').val(node.name);
			$('#id').val(node.id);
			$('#remark').val(node.remark);
			
			//绑定编辑事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				saveTreeNode(getNode(),node,"edit");
			})
			
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: "修改",
			    area: ['auto', 'auto'],
				offset: ['100px','400px'],
			    border: [3, 0.3, '#000'], //去掉默认边框
			    shade: [0.3, '#000',true], //去掉遮罩
			    closeBtn: [0, true], //去掉默认关闭按钮
			    //shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	//删除
	$('#btnDelCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			if (node.id=='root'){
				layer.alert('无法删除根节点', -1);
				return false;
			}
			
			var msg = "确定需要删除当前节点？";
			if (node.children && node.children.length > 0) {
				msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
			} 
			layer.confirm(msg, function(index){
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/learning/exam-cate!delcategory.htm', 
					data : {'id': node.id},
					async: true,
					dataType : 'json',
					success : function(data){
						if(data.result == true){
							zTree.removeNode(node);
						}
						layer.alert(data.msg, -1);
						layer.close(index);
					},
					error : function(msg){
						newNode = null;
						layer.alert("网络错误，请稍后再试!", -1);
					}
				});
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	/*渲染树高度*/
	$('#treeDemo').css('height', $(window).height()-50);
});
</script>
<div class="ul_Cate" style="background-color: #f6f6f6;border-right: 1px solid #dfdfdf;border-bottom: 1px solid #dfdfdf;">
	<ul style="margin-top: -5px;">
		<li style="height: 2px;"></li>
		<li>
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnNewCate" >新增</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnEditCate" >修改</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnDelCate" >删除</a>  
		</li>
		<li>
			<ul id="treeDemo" class="ztree" style="width:160px; overflow: auto;"></ul>
		</li>
	</ul>
</div>
<div id="rContent" style="display: none;">
	<table cellspacing="0" class="box" style="width: 300px;border: 0;">
		<tr>
			<th>
				父节点:
			</th>
			<td>
				<span id="pname"></span>
				<input type="hidden" id="pid"/>
			</td>
		</tr>
		<tr>
			<th>
				<span style="color: red;">*</span>当前节点:
			</th>
			<td>
				<input type="text" id="name" style="width: 97%;"/>
				<input type="hidden" id="id"/>
			</td>
		</tr>
		<tr>
			<th>
				备注:
			</th>
			<td>
				<textarea id="remark" rows="1" cols="1" style="width: 95%;"></textarea>
			</td>
		</tr>
	</table>
	<br>
	<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
		<span id="err_msg" style="color: red;float: left; margin-left: 5px;margin-top:5px;"></span>
		<input type="button" id="addOrEdt" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
	</div>
</div>