<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<style type="text/css">
			.yonghu_titile li{padding-left:10px}
		</style>
		<script type="text/javascript">
			//全选反选
			function checkAll(){
				$("input:checkbox[name='examrecord_id']").each(function(){
					$(this).attr("checked",$(this).attr("checked")=='checked'?false:true);
				});
			}
			//重置
			function selectReset(){
				$("#user_name").val("");
				$("#dept_name").val("");
				$("#dept_bh").val("");
				$("#paper_name").val("");
				$("#status option:first").attr("selected","selected");
				$("#score_status option:first").attr("selected","selected");
				$("#score_publish option:first").attr("selected","selected");
				// 新增重置批次名和考试状态
				$("#batch_name option:first").attr("selected","selected");
				$("#exam_status option:first").attr("selected","selected");
			}
			
			function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/examinee!getExaminees.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			
			function seeinfo(id){
				if(id ==''){
					layer.alert("该考生未进行考试!", -1);
					return false;
				}
				parent.__kbs_layer_index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['查看试卷', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!findUserpaper.htm?id='+id},
					area: ['80%', '80%']
				});
				
			}
			
			function editscore(id,status){
				if(status != 2){
					layer.alert("该考生未进行考试或已过期!", -1);
					return false;
				}
				_markerscore =$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['编辑成绩', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!editscore.htm?id='+id},
					area: ['250px', '200px']
				});
			}
			
			$(function(){
					var deptTreeObject = {
						deptTypeEl : $('#dept_name'),
						ktreeEl : $('div.menuContent'),
						treeAttr : {
							view : {
								expandSpeed: ''
							},
							async : {
								enable : true,
								url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
								autoParam : ["id", "name=n", "level=lv", "bh"],
								otherParam : {}
							},
							callback : {
								onClick : function(event, treeId, treeNode) {
									deptTreeObject.deptTypeEl.val(treeNode.name);
									$('#dept_bh').val(treeNode.bh);
								}
							}
						},
						render : function() {
							var self = this;
							self.deptTypeEl.focus(function(e){
								self.ktreeEl.css({
									'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
									'left' : $(this).offset().left + 'px'
								});
								if(self.ktreeEl.is(':hidden'))
									self.ktreeEl.show();
							});
							
							$('*').bind('click', function(e){
								if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
									
								} else {
									if(!self.ktreeEl.is(':hidden'))
										self.ktreeEl.hide();
								}
							});
							
							self.deptTypeEl.bind('keydown', function(keyArg) {
								if(keyArg.keyCode == 8) {
									$(this).val('');
									$('#dept_bh').val('');
								} else if(keyArg.keyCode == 13) {
									self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
								}
							});
							
							self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
								$(this).parent().parent().parent().hide();
							});
							
							$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
						}
					}
					
				
				deptTreeObject.render();
				
				//发布成绩
				$("#topublish").click(function(){
					var checkids = [];
					$("input:checkbox[name='examrecord_id']:checked").each(function(i, item){
						//如果有权限
						if($(this).attr("status")=='0'||$(this).attr("status")=='3'){
							//如果没做过试题 
							if($(this).val()!='' && $(this).attr("publish")=='0'){
								checkids.push($(this).val());
							}
						}
					});
					if(checkids.length<=0){
						layer.alert("请至少选择一条有发布权限且考试完成且未发布的数据发布!", -1);
						return false;
					}$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/examinee!publistscore.htm',
							data :{"id":checkids.join(',')},
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1, function(){
										window.location.reload();
									});
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
				});
				//批阅试卷
				$("#tomark").click(function(){
					var checkids = [];
					$("input:checkbox[name='examrecord_id']:checked").each(function(i, item){
						if($(this).attr("status")=='1'||$(this).attr("status")=='3'){
							//如果做过试题 
							if($(this).val()!=''){
								checkids.push($(this).val());
							}
						}
					});
					if(checkids.length<=0 || checkids.length>1){
						layer.alert("请至少选择一条有发布权限且考试过数据批阅!", -1);
						return false;
					}
					_markerpaer =$.layer({
						type: 2,
						border: [2, 0.3, '#000'],
						title: ['评阅试卷', 'font-size:14px;font-weight:bold;'],
						closeBtn: [0, true],
						iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!findmarkerpaer.htm?id='+checkids},
						area: ['90%', '100%']
					});
				});
				//批量阅卷
				$("#batchmark").click(function(){
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/learning/exam-record!checkbatchpaper.htm',
						data :{"exam_id":'${examineeinfo.exam_id}'},
						async: true,
						dataType : 'json',
						success : function(data){
							if(data.result == true){
								_markerpaer =$.layer({
									type: 2,
									border: [2, 0.3, '#000'],
									title: ['批量阅卷', 'font-size:14px;font-weight:bold;'],
									closeBtn: [0, true],
									iframe: {src :$.fn.getRootPath()+'/app/learning/exam-record!batchmarkepaper.htm?exam_id='+'${examineeinfo.exam_id}'},
									area: ['90%', '100%']
								});
							}else{
								layer.alert("无批量阅卷题!", -1);
							}
						},
						error : function(msg){
							layer.alert("网络错误，请稍后再试!", -1);
						}
					});
				});
				//强制交卷
				$("#imposeExam").click(function(){
					var flag = true;
					var checkids = [];
					$("input:checkbox[name='examrecord_id']:checked").each(function(i, item){
						if($(this).attr("examStatus")!='1'){
							layer.alert("只能选择正在考试的数据!", -1);
							flag = false;
						}else{
							checkids.push($(this).val());
						}
					});
					if(flag){
						if(checkids.length<=0){
							layer.alert("请至少选择一条数据!", -1);
							return false;
						}
						_confirm_layer_index = layer.confirm("确认强制交卷?",function(){
							layer.close(_confirm_layer_index);
							_shade_layer_index = $.layer({
								type: 1,
							    title: ["是否保留分数", 'font-size:14px;font-weight:bold;'],
							    area: ['200', '150'],
							    border: [2, 0.3, '#000'],
							    shade: [0],
							    closeBtn: [0, true],
							    page: {
							        dom: '#imposeShade'
							    }
							});
						});
					}
				});
				$("#imposeExamDo").on("click",function(){
					layer.close(_shade_layer_index);
					var checkids = [];
					$("input:checkbox[name='examrecord_id']:checked").each(function(i, item){
						checkids.push($(this).attr("examineeId"));
					});
					var checkS = $("[name='saveScore']:checked");
					if(checkS){
						var saveScore = checkS.val();
						var param = {
							"examIds":checkids.join(','),
							"saveScore":saveScore
						};
						$.ajax({
							type: "POST",
							url: $.fn.getRootPath()+'/app/learning/examinee!imposeExam.htm',
							data: param,
							dataType:"json",
							success : function(data){
								layer.alert(data.msg, -1);
								window.location.reload();
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				});
				//加时-延长考试
				$("#extraTime").on("click",function(){
					var batchId = $("#batch_name option:selected").val();
					if(batchId == ''){
						layer.alert("请选择批次",-1);
						return false;
					}
					layer.prompt({
						title: '输入加时(分钟)，并确认',
						formType: 0
					}, function(text){
						if(isNaN(text)){
							layer.alert("请输入正确数字",-1);
							return false;
						}else{
						    if(text<0){
						    layer.alert("请输入正确的加时分钟！",-1);
                            return false;
						    }
							var param = {
								"batchId":batchId,
								"minute":text
							};
							$.ajax({
								type: "POST",
								url: $.fn.getRootPath()+'/app/learning/examinee!extraTime.htm',
								data: param,
								dataType:"json",
								success : function(data){
									layer.alert(data.msg, -1);
									window.location.reload();
								},
								error : function(msg){
									layer.alert("网络错误，请稍后再试!", -1);
								}
							});
						}
					});
				});
			})	
		</script>
	</head>
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border: 0px;">
					<form id="form0" action="" method="post">
					  <div class="content_right_bottom">
						<div class="gonggao_titile">
							<div class="gonggao_titile_right">
									<a href="#" id="batchmark">批量阅卷</a>
									<a href="#" id="tomark">阅卷</a>
									<a href="#" id="topublish">发布成绩</a>
									<a href="#" id="imposeExam">强制交卷</a>
									<a href="#" id="extraTime">加时</a>
								</div>
							</div>
							<div class="yonghu_titile">
								<ul>
									<li>
										我的角色:
										<select id="status" name="examineeinfo.status">
											<option value="">--请选择--</option>
											<option value="0" <c:if test="${examineeinfo.status==0 }">selected="selected"</c:if>>我的发布</option>
											<option value="1" <c:if test="${examineeinfo.status==1 }">selected="selected"</c:if>>我的批阅</option>
										</select>
									</li>
									<li>
										<input type="hidden" value="${examineeinfo.exam_id }" name="examineeinfo.exam_id" />
										姓名：<input type="text" id="user_name"  name="examineeinfo.user_name" value="${examineeinfo.user_name }"/>
									</li>
									<li>
										部门：<input type="text" id="dept_name" name ="examineeinfo.dept_name" value="${examineeinfo.dept_name}" onclick="showMenu('menuContent','dept_name')"  style="width:90px"/>
											<input id="dept_bh"  type="hidden" name="examineeinfo.dept_bh" value="${examineeinfo.dept_bh }"/>
									</li>
									<li>
										批次名:
										<select id="batch_name" name="examineeinfo.batch_id">
											<option value="">--请选择--</option>
											<s:iterator value="#request.batchlist" var="batch">
												<option value="${batch.batch_id }" <c:if test="${examineeinfo.batch_id==batch.batch_id }">selected="selected"</c:if>>${batch.batch_name }</option>
											</s:iterator>
										</select>
									</li>
									<li>
										成绩:
										<select id="score_status" name="examineeinfo.score_status">
											<option value="">--请选择--</option>
											<option value="0" <c:if test="${examineeinfo.score_status==0 }">selected="selected"</c:if>>合格</option>
											<option value="1" <c:if test="${examineeinfo.score_status==1 }">selected="selected"</c:if>>不合格</option>
										</select>
									</li>
									<li>
										考试状态:
										<select id="exam_status" name="examineeinfo.exam_status">
											<option value="">--请选择--</option>
											<option <c:if test="${examineeinfo.exam_status==0 }">selected="selected"</c:if> value="0">未开始</option>
											<option <c:if test="${examineeinfo.exam_status==1 }">selected="selected"</c:if> value="1">考试中</option>
											<option <c:if test="${examineeinfo.exam_status==2 }">selected="selected"</c:if> value="2">已交卷</option>
											<option <c:if test="${examineeinfo.exam_status==3}">selected="selected"</c:if> value="3">已过期</option>
										</select>
									</li>
									<li>
										成绩发布:
										<select id="score_publish" name="examineeinfo.score_publish">
											<option value="">--请选择--</option>
											<option <c:if test="${examineeinfo.score_publish==1 }">selected="selected"</c:if> value="1">已发布</option>
											<option <c:if test="${examineeinfo.score_publish==0 }">selected="selected"</c:if> value="0">未发布</option>
										</select>
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" /> </a>
									</li>
									
								</ul>
							</div>
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="5%">
												<input type="checkbox" value="" onclick="checkAll()" id="" />
											</td>
											<td width="5%">
												姓名
											</td>
											<td width="8%">
												部门
											</td>
											<td width="5%">
												成绩
											</td>
											<td width="10%">
												参加考试时间
											</td>
											<td width="10%">
												考试结束时间
											</td>
											<td width="5%">
												考试时长
											</td>
											<td width="8%">
												成绩发布
											</td>
											<td width="8%">
												考试状态
											</td>
											<td width="8%">
												批次
											</td>
											<td width="8%"></td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td><input type="checkbox" name="examrecord_id" examStatus="${va.status }" status="${va.role_status}" publish="${va.examRecord.is_publish }" value="${va.examRecord.id  }" examineeId="${va.id }"/></td>
												<td>${va.user.userInfo.userChineseName}</td>
												<td>
													${va.user.mainStation.dept.name}
												</td>
												<td>${va.examRecord.score }</td>
												<td>${va.examRecord.start_time }</td>
												<td>${va.examRecord.end_time }</td>
												<td>
													<s:if test="#va.examRecord.exam_interval!=null">
														<fmt:formatNumber type="number" value="${va.examRecord.exam_interval/60 }" maxFractionDigits="1"/>分钟
													</s:if>
												</td>
												<td><s:if test="#va.examRecord.is_publish==0">未发布</s:if><s:if test="#va.examRecord.is_publish==1">已发布</s:if></td>
												<td>
													<s:if test="#va.status==0 && #va.is_overdue==0">未开始</s:if>
													<s:elseif test="#va.status==0 && #va.is_overdue==1">未开始,已过期</s:elseif>
													<s:elseif test="#va.status==1 && #va.is_overdue==0">考试中</s:elseif>
													<s:elseif test="#va.status==1 && #va.is_overdue==1">考试中,已过期</s:elseif>
													<s:elseif test="#va.status==2 && #va.is_overdue==0">已交卷</s:elseif>
													<s:elseif test="#va.status==2 && #va.is_overdue==1">已交卷,已过期</s:elseif>
												</td>
												<td>${va.exambatch.name }</td>
												<td>
													<s:if test="#va.role_status==1 || #va.role_status==3">
														<a href="#" onclick="seeinfo('${va.examRecord.id  }')">查看</a>||<a href="#" onclick="editscore('${va.examRecord.id  }','${va.status }')">编辑成绩</a>
													</s:if>
													<s:else>
														<a href="#" onclick="seeinfo('${va.examRecord.id  }')">查看</a>
													</s:else>
												</td>
												
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
									<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
										<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div id="imposeShade" style="display:none;">
					<div style="margin-top: 15px;margin-left: 20px;width: 100%;float: left;">
						<input type="radio" name="saveScore" value="1" checked="checked"/>保留分数
					</div>
					<div style="margin-top: 15px;margin-left: 20px;width: 100%;float: left;">
						<input type="radio" name="saveScore" value="0"/>不保留分数
					</div>
					<div class="sub_btn" style="width: 99%; height: 40px;top: 10px;left: 2px;border: 0;float: right;">
						<a href="javascript:void(0);" id="imposeExamDo">确定</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

