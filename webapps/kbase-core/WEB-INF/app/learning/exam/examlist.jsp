<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="gonggao_con" style="margin-left: 0px;margin-right: 0px;">
	<div class="gonggao_con_nr_full">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tdbg">
				<td width="2%">
					<input type="checkbox" value="" onclick="checkAll()" id="" />
				</td>
				<td width="10%">
					考试名称
				</td>
				<td width="10%">
					试卷名称
				</td>
				<td width="12%">
					开始日期
				</td>
				<td width="12%">
					结束日期
				</td>
				<td width="8%">
					创建人
				</td>
				<td width="7%">
					考试人数
				</td>
				<td width="8%">
					考试完成率
				</td>
				<td width="8%">
					缺考人数
				</td>
				<td width="5%">
					合格率
				</td>
				<td width="8%">
					考试状态
				</td>
				<td width="10%">操作</td>
			</tr>
			<s:iterator value="page.result" var="va">
				<tr>
					<td><input type="checkbox" name="exam_id" status="${va.examinfo.status}" value="${va.id }"/></td>
					<td>${va.name }</td>
					<td><a href="#" onclick="showPaperInfo('${va.paper.id }','${va.paper.type }');">${va.paper.name }</a></td>
					<td>${va.start_time }</td>
					<td>${va.end_time }</td>
					<td>${va.createUserNameCN}</td>
					<td>${va.examinfo.examcount}</td>
					<td>${va.examinfo.completepercent}</td>
					<td>${va.examinfo.noexamcount}</td>
					<td>${va.examinfo.passpercent}</td>
					<td><s:if test="#va.examinfo.status ==0">考试未开始</s:if><s:if test="#va.examinfo.status ==1">考试进行中</s:if><s:if test="#va.examinfo.status ==2">考试结束</s:if></td>
					<td><a href="#" onclick="topreview('${va.id }')">查看</a>||<a href="#" onclick="showdetailinfo('${va.id }')">管理</a></td>
				</tr>
			</s:iterator>
			<tr class="trfen">
				<td colspan="12">
					<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
				</td>
			</tr>
		</table>
	</div>
</div>
