<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
				<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<style type="text/css">
			.inputflag{color:red;font-size: 14px;}
		</style>	
		<script type="text/javascript">
			//试卷选择器
			
			
			function delbatch(id,trid){
				parent.layer.confirm("确定要删除吗？", function(index){
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/learning/exam-batch!delbatch.htm', 
						data :{"id":id},
						async: true,
						dataType : 'json',
						success : function(data){
							if(data.result == true){
								layer.alert("删除成功!", -1, function(){
									window.location.reload();
								});
							}else{
								layer.alert("删除失败!", -1);
							}
						},
						error : function(msg){
							layer.alert("网络错误，请稍后再试!", -1);
						}
					});
					parent.layer.close(index);
				});
			}
			
			function modifybatch(id){
				_batchindex = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['批次管理', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/exam-batch!batchtoupdate.htm?id='+id},
					area: ['100%', '100%']
				});
			}
			
			
			$(function(){
				$('#cateNames_select').click(function(){
			       $.kbase.picker.singleExamCate({returnField:"cateNames_select|cateIds_select"});
			    });
				var release_type = $('input:radio[name="exam.release_type"]:checked').val();
				$('input:radio[name^="exam.release_"]').click(function(){
					 var id ="release_"+$(this).val();
					 if($(this).val()!=release_type){
					 	$("input[id^='release_']").each(function(){
						 	$(this).val("");
						 	$(this).css('background-color','white');
						 	$(this).attr('disabled','disabled');
						 });
					 }
					 release_type = $(this).val();
					 $('#'+id+'').removeAttr('disabled');
				});
				//选择发布人
				$("#userselect").click(function(){
					$.kbase.picker.deptselectuser({returnField:"release_3|release_userid"});
				});
				
				//选择试卷树
				$("#paperName").click(function(){
					$.kbase.picker.singlePaperSelect({returnField:"paperName|paperId"});
				});
				
				//新增批次
				$("#batchtoadd").click(function(){
					if($('#exam_id').val()==''){
						layer.alert("请先保存考试信息!", -1);
						return false;
					}
					_batchindex = $.layer({
						type: 2,
						border: [2, 0.3, '#000'],
						title: ['批次管理', 'font-size:14px;font-weight:bold;'],
						closeBtn: [0, true],
						iframe: {src :$.fn.getRootPath()+'/app/learning/exam-batch!toaddbatch.htm?id='+$('#exam_id').val()},
						area: ['100%', '100%']
					});			
				});
				
				//退出
				$("#btnquit").click(function(){
					$(parent.document).find('div.content_content').find('iframe').each(function(){
						if (!$(this).is(":hidden")){
							$(this).attr("src", $.fn.getRootPath()+'/app/learning/exam.htm'); 
							return false;
						}
					});
					parent.layer.close(parent.__kbs_layer_index);				
				});
				
				//保存考试信息
				$("#btnsave").click(function(){
					var t = $("#form1").serializeArray();
					var data = {};
				    $.each(t, function() {
				      data[this.name] = this.value;
				    });
					if(validate()){
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/exam!savexam.htm', 
							data :data,
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									$('#exam_id').val(data.id);
									layer.alert("操作成功!", -1);
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				});
				//点击折叠按钮
			    $("#showOrHideInfo,#showOrHideBatch").on("click",function(){
			    	var url = $.fn.getRootPath()+"/resource/learning/images/fold_up.png";
			    	var _this;
			    	if($(this).attr("id") == 'showOrHideInfo'){
			    		_this = $("#infoArea tr").slice(1,8);
			    	}else if($(this).attr("id") == 'showOrHideBatch'){
			    		_this = $("#batchArea");
			    	}
			    	if(_this.is(":hidden")){
			    		_this.show();
			    		url = $.fn.getRootPath()+"/resource/learning/images/fold_down.png";
			    	}else{
			    		_this.hide();
			    	}
			    	$(this).attr("src",url);
			    });
			});
			function validate(){
					var exam_name = $('#exam_name').val();
					var paper_name = $('#paperName').val();
					var examcate = $('#cateNames_select').val(); 
					var start_time = $('#start_time').val();
					var end_time = $('#end_time').val();
					var late_minutes = $('#late_minutes').val();
					var finish_minutes = $('#finish_minutes').val();
					var release_type = $('input:radio[name="exam.release_type"]:checked').val();
					if(exam_name==''){
						layer.alert("请填写考试名称!", -1);
						return false;
					}if(examcate==''){
						layer.alert("请选择考试分类!", -1);
						return false;
					}if(paper_name==''){
						layer.alert("请选择考试用卷!", -1);
						return false;
					}if(start_time==''){
						layer.alert("请填写考试开始时间!", -1);
						return false;
					}
					if(end_time==''){
						layer.alert("请填写考试结束时间!", -1);
						return false;
					}
					if(late_minutes==''){
						layer.alert("请选择考试时间限制!", -1);
						return false;
					}
					if( isNaN( late_minutes ) )
					   {
					    alert('请输入正确的考试时间限制！');
                        form1.late_minutes.select();
                        return false;
					}
					if(finish_minutes==''){
						layer.alert("请选择交卷时间限制!", -1);
						return false;
					}
					if( isNaN( finish_minutes ) )
                       {
                        alert('请输入正确的交卷时间限制！');
                        form1.finish_minutes.select();
                        return false;
                    }
					if(parseInt(late_minutes) < parseInt(finish_minutes)){
						layer.alert("交卷时间不能大于考试时间!", -1);
						return false;
					}
					if(typeof(release_type) == "undefined"){
						layer.alert("请选择考试发布类型!", -1);
						return false;
					}if(release_type == '2' && $("#release_2").val()==''){
						layer.alert("请选择定时发布时间!", -1);
						return false;
					}if(release_type == '3' && $("#release_3").val()==''){
						layer.alert("请选择手动发布人!", -1);
						return false;
					}if(new Date(start_time.replace(/-/g,"/")).getTime()-new Date(end_time.replace(/-/g,"/")).getTime()>0){
						layer.alert("考试开始时间不能大于考试结束时间!", -1);
						return false;
					}else{
						var flag =true;
						$("input[id='batchstart_time']").each(function(){
							if(new Date(start_time.replace(/-/g,"/")).getTime()-new Date($(this).val().replace(/-/g,"/")).getTime()>0){
								layer.alert("考试批次中存在开始时间小于考试开始时间", -1);
								flag = false;
								return false;
							}
						});
						$("input[id='batchend_time']").each(function(){
							if(new Date(end_time.replace(/-/g,"/")).getTime()-new Date($(this).val().replace(/-/g,"/")).getTime()<0){
								layer.alert("考试批次中存在结束时间大于考试结束时间", -1);
								flag = false;
								return false;
							}
						});
						return flag;
					}
				}
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" name="exam.id" id="exam_id"  value="${exam.id }">
			<table id="infoArea" width="100%" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td colspan="3" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
						<div class="background_theme">
						<font class="font_style">基本信息</font>
						<img id="showOrHideInfo" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
							src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
						</div>
					</td>
				</tr>
				<tr>
					<td>考试名称<span class="inputflag">*</span></td>
					<td><input type="text" id="exam_name" value="${exam.name }" name="exam.name" style="width:92%"/></td>
				</tr>
				<tr>
					<td>考试用卷<span class="inputflag">*</span></td>
					<td><input type="text" id="paperName" value="${exam.paper.name}" name="exam.paper.name" readonly="readonly" style="width:82%"/><input type="hidden" id="paperId" name="exam.paper.id" value="${exam.paper.id }" /></td>
				</tr>
				<tr>
					<td>考试时间<span class="inputflag">*</span></td>
					<td>
						<input type="text" name="exam.start_time" id="start_time" value="${exam.start_time}" placeholder="开始时间"
						onclick="javascript:WdatePicker({minDate:'%y-%M-%d',maxDate:'#F{$dp.$D(\'end_time\')}',dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" style="width:30%;" />
						<input type="text"  name="exam.end_time" value="${exam.end_time}" id="end_time" placeholder="结束时间"
						onclick="javascript:WdatePicker({minDate:'#F{$dp.$D(\'start_time\')}',dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" style="width:30%;" />
					</td>
				</tr>
				<tr>
					<td>考试分类<span class="inputflag">*</span></td>
					<td><input type="text" id="cateNames_select" value="${exam.examCate.name }" name="exam.examCate.name" style="width:92%"/>
					<input type="hidden" id="cateIds_select" value="${exam.examCate.id }" name="exam.examCate.id" />
					</td>
				</tr>
				<tr>
					<td>考时限制<span class="inputflag">*</span></td>
					<td>
						考试时间限制<input type="text" value="${exam.late_minutes }" id="late_minutes" name="exam.late_minutes" style="width:5%;" />分钟<br>
						交卷时间限制<input type="text" value="${exam.finish_minutes }" name="exam.finish_minutes" id="finish_minutes" style="width:5%;" />分钟
					</td>
				</tr>
				<tr>
					<td>发布设置<span class="inputflag">*</span></td>
					<td>
						<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==1">checked="checked"</s:if> value="1" 
						style="margin-right: 10px;margin-bottom: 10px;"/>自动发布<br>
						<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==2">checked="checked"</s:if> value="2" 
						style="margin-right: 10px;margin-bottom: 10px;"/>预约发布
						<input id="release_2" name="exam.release_time" value="${exam.release_time }" 
						onclick="javascript:WdatePicker({minDate:'#F{$dp.$D(\'start_time\')}',dateFmt:'yyyy-MM-dd HH:mm'})" 
						class="Wdate" readonly="readonly" type="text" /><br>
						<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==3">checked="checked"</s:if> value="3" 
						style="margin-right: 10px;"/>手动发布人
						<input type="text"  id="release_3" value="${exam.release_username}" name="exam.release_username" readonly="readonly"/>
						<input type="hidden" value="${exam.release_userid }" id="release_userid" name="exam.release_userid"/>
						<font>
							<a id="userselect" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;margin-top:3px;"></img>
								添加发布人
							</a>
						</font>
					</td>
				</tr>
				<tr>
					<td>是否显示答案</td>
					<td>
						<input type="radio" name="exam.isShowAnswer" ${exam.isShowAnswer==1?"checked='checked'":""} 
							value="1" style="margin-right: 10px;margin-bottom: 10px;"/>是<br>
						<input type="radio" name="exam.isShowAnswer" ${exam.isShowAnswer==1?"":"checked='checked'"} 
							value="0" style="margin-right: 10px;"/>否
					</td>
				</tr>
				<tr>
					<td>开卷</td>
					<td>
						<s:select name="exam.isOpenBook" key="exam.isOpenBook" value="#request.exam.isOpenBook" list="#{'1':'开卷'}" style="width: 50px;"></s:select>
					</td>
				</tr>
				<tr>
					<td>考试说明</td>
					<td><textarea id="remark" name="exam.remark" rows="3" style="width:92%;">${exam.remark }</textarea></td>
				</tr>
				<tr>
					<td colspan="3" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
						<div class="background_theme">
						<font class="font_style">考试批次</font>
						<img id="showOrHideBatch" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
							src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
						</div>
					</td>
				</tr>
			</table>
		</form>
		<table id="batchArea" width="100%" cellspacing="0" cellpadding="0" class="box" id="batchinfo" style="width:100%;border: 0px;">
			<tr>
				<td	align="center" style="width: 60px;"><b>批次</b></td>
				<td	align="center" style="width: 40px;"><b>人数</b></td>
				<td	align="center"><b>人员</b></td>
				<td	align="center" style="width: 260px;"><b>时间</b></td>
				<td align="center" style="width: 80px;"><b>操作</b></td>
			</tr>
			<s:iterator value="#request.batchlist" var="batch" status="ind">
				<input type="hidden" id="batchstart_time" value="<fmt:formatDate  value="${batch.start_time}" pattern="yyyy-MM-dd HH:mm" />" />
				<input type="hidden" id="batchend_time" value="<fmt:formatDate  value="${batch.end_time}" pattern="yyyy-MM-dd HH:mm" />" />
				<tr id="batchinfo_${ind.index+1 }">
					<td	align="center">${batch.batch_name }</td>
					<td	align="center">${batch.usercount }</td>
					<td	align="center">
						${batch.scopeNames }
					</td>
					<td	align="center">
						<fmt:formatDate  value="${batch.start_time}" pattern="yyyy-MM-dd HH:mm" />
						-
						<fmt:formatDate  value="${batch.end_time}" pattern="yyyy-MM-dd HH:mm" />
					</td>
					<td  align="center"><a href="#" onclick="delbatch('${batch.batch_id }','batchinfo_${ind.index+1 }')">删除</a>||<a href="#" onclick="modifybatch('${batch.batch_id }','batchinfo_${ind.index+1 }')">修改</a></td>
				</tr>
			</s:iterator>
			<tr>
				<td align="left" colspan="5">
					<font style="float: right;margin-right: 20px;">
						<a id="batchtoadd" href="javascript:void(0);" style="cursor: pointer;">
							<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
							添加批次
						</a>
					</font>
				</td>
			</tr>
		</table>
		<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
			<a id="btnquit" href="javascript:void(0);">退出</a>
			<a id="btnsave" href="javascript:void(0);">保存</a>
		</div>
	</body>
</html>

