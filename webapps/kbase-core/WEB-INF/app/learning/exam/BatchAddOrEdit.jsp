<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<style type="text/css">
			.inputflag{color:red;font-size: 14px;}
		</style>	
		<script type="text/javascript">
			function validate(){
				var scopenames =$('#scopenames').val();
				var start_time =$('#start_time').val();
				var end_time=$('#end_time').val();
				if(scopenames==''){
					layer.alert("请选择考试人员!", -1);
					return false;
				}else if(start_time==''){
					layer.alert("请选择考试开始时间!", -1);
					return false;
				}else if(end_time==''){
					layer.alert("请选择考试结束时间!", -1);
					return false;
				}else if(new Date(start_time.replace(/-/g,"/")).getTime()-new Date(end_time.replace(/-/g,"/")).getTime()>0){
					layer.alert("考试开始时间不能大于考试结束时间!", -1);
					return false;
				}else{
					return true;
				}
			};
			
			
			$(function(){
				//选取评卷人
				$("#userselect").click(function(){
					$.kbase.picker.deptselectuser({returnField:"marker_name|marker_id"});
				});
				
				//选择考试用户
				$("#examuserselect").click(function(){
					$.kbase.picker.deptusersearch({returnField:"scopenames|scopeids"});
				});
				
				//保存批次信息
				$("#batchtosave").click(function(){
					var t = $("#form1").serializeArray();
					var data = {};
				    $.each(t, function() {
				      data[this.name] = this.value;
				    });
					data['examBatch.exam.id']=$('#id').val();
					if(validate()){
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/exam-batch!savebatch.htm', 
							data :data,
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.result == true){
									layer.alert("操作成功!", -1, function(){
										parent.window.location.href=$.fn.getRootPath() + '/app/learning/exam!toaddorudate.htm?id='+'${exam.id }' ;
										parent.layer.close(parent._batchindex);
									});
								}else{
									layer.alert("操作失败!", -1);
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				})
			});
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" value="${exam.id }" name="exam_id"/>
			<input type="hidden" value="${examBatch.id }" name="examBatch.id"/>
			<input type="hidden" value="${examBatch.name }" name="examBatch.name"/>
			<table width="100%" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td width="18%">考试人员<span class="inputflag">*</span></td>
					<td>
						<input type="text" style="width: 80%" value="${examBatch.scopenames }" id="scopenames" name="examBatch.scopenames" readonly="readonly"/>
						<input type="hidden" id="scopeids" name="examBatch.scopeids" value="${examBatch.scopeids }"/>
						<font>
							<a id="examuserselect" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;margin-top:3px;"></img>
								添加
							</a>
						</font>
					</td>
				</tr>
				<tr>
					<td>批次考试开始时间<span class="inputflag">*</span></td>
					<td>
						<input type="text" style="width: 80%"  id="start_time" name="start_time" 
							onclick="javascript:WdatePicker({minDate:'${exam.start_time }',maxDate:'${exam.end_time }',dateFmt:'yyyy-MM-dd HH:mm'})" 
							class="Wdate" readonly="readonly" value='<s:date name="examBatch.start_time" format="yyyy-MM-dd HH:mm"/>'/>
					</td>
				</tr>
				<tr>
					<td>批次考试结束时间<span class="inputflag">*</span></td>
					<td>
						<input type="text" style="width: 80%"  id="end_time" name="end_time" 
							onclick="javascript:WdatePicker({minDate:'${exam.start_time }',maxDate:'${exam.end_time }',dateFmt:'yyyy-MM-dd HH:mm'})" 
							class="Wdate" readonly="readonly" value='<s:date  name="examBatch.end_time" format="yyyy-MM-dd HH:mm"/>'/>
					</td>
				</tr>
				<tr>
					<td>考试评卷人</td>
					<td>
						<input type="text" style="width: 80%" value="${examBatch.marker_name }" id="marker_name" name="examBatch.marker_name" 
							readonly="readonly"/>
						<input type="hidden" value="${examBatch.marker_id }" id="marker_id" name="examBatch.marker_id" />
						<font>
							<a id="userselect" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;margin-top:3px;"></img>
								添加
							</a>
						</font>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="2">
						<input type="button" value="保存" id="batchtosave">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

