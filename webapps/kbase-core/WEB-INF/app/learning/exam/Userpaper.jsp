<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<!-- layer -->
	<style type="text/css">
		.examtop_left ul{width:20%}
	</style>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<script type="text/javascript">		
		$(function(){
			var blanklength = $('#blank_length').val();
			for(var i=1;i<=blanklength;i++){
				var answer = $('#blank_answer_'+i).val();
				if(answer!=''){
					$('#blank_quescore_'+i+' span input').each(function(index,element){
						$(this).val(answer.split('|')[index]);				 		
					});
				}
			}
			//点击关联知识
			$("[name='kbvalue']").on("click",function(){
				var kbvalue = $(this).attr("_value");
				console.info(parent.parent.parent.parent);
				parent.parent.parent.parent.TABOBJECT.open({
					id : 'kbvalueInfo',
					name : "知识详情",
					hasClose : true,
					url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+kbvalue,
					isRefresh : true
				}, this);
			});
		});
		//题目显示或隐藏
		function toggleinfo(info_id,img_id){
			if($('#'+info_id+'').is(":hidden")){
				$('#'+info_id+'').show();
				$('#'+img_id+'').attr('src','${pageContext.request.contextPath}/resource/learning/images/f_05.jpg');
			}else{
				$('#'+info_id+'').hide();
				$('#'+img_id+'').attr('src','${pageContext.request.contextPath}/resource/learning/images/hover_03.gif');
			}
		} 
	</script>			 
</head>
<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top">
					<div class="usbox">
						<div><span style="font-weight:bold;margin-left:45%;font-size:14px">${map.examinee.paper.name }</span></div>
						<div class="examtop_left fl">
							<ul>
								<li>
									考生：
									<span>${map.examinee.user.userInfo.userChineseName }</span>
								</li>
							</ul>
							<ul>
								<li>
									部门：
									<span>${map.user.mainStation.dept.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总数：
									<span>${map.qasize}</span>
								</li>
							</ul>
							<ul>
								<li>
									交卷时限：
									<span>${map.examinee.exambatch.exam.finish_minutes}分钟</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总分${map.examinee.paper.score}
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
								<s:set var="select_var" value="{'A','B','C','D','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									<s:if test="#request.map.singlequeslist.size>0">
										<div class="eaxm_title" style="margin-bottom: 15px;">
											<div class="fl title_left">单选题 </div>
											<div class="fr title_right">
	                             				<div class="fl">
	                             					<span>
	                             						对题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.singlecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						错题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.singlequeslist.size-#request.map.singlecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						得分
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.singlescore"/>
	                             						</font>
	                             					</span>&nbsp;
	                             					<span>
	                             						共
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.singlequeslist.size"/>
	                             						</font>
	                             						题
														<a href="javascript:void(0);" onclick="toggleinfo('singleques','singleimg')">
															<img id="singleimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
															onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
														</a>
	                             					</span>
	                             				</div>
				                          	</div>
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="singleques">
											<div class="usbox">
											<s:iterator value="#request.map.singlequeslist" var="detail" status="ind1">
												<input type="hidden" id="signle_length" value="<s:property value='#request.map.singlequeslist.size()'/>" />
												<input type="hidden" id="signle_id_${ind1.index+1 }" value="${detail.ques_id}" />
												<input type="hidden" id="signle_score_${ind1.index+1 }" value="${detail.referScore}" />
												<div class="exam_a">
													<span>
														${ind1.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore}分)
														<s:if test="#request.map.showAnswer==1">
															<s:if test="#detail.score==#detail.referScore"><span class="incorrect">正确</span></s:if>
															<s:else><span class="correct">错误</span></s:else>
														</s:if>
													</span>
													<ul>
														<s:iterator value="#request.detail.quesinfo.answer" var="resu" status="ind11">
															<li>
																<a href="javascript:void(0);">&nbsp;
																	<s:if test="#request.detail.answer==#request.resu.id">
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg"/>&nbsp;
																	</s:if>
																	<s:else>
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>&nbsp;
																	</s:else>
																	<span style="<s:if test="#request.map.showAnswer==1&&#request.resu.isright==1">color:green;</s:if>">
																		${select_var[ind11.index]}.${resu.name}
																	</span>
																</a>
															</li>
														</s:iterator>
														<c:if test="${map.openBook == 1 && fn:trim(detail.quesinfo.kbvalue)!=''}">
															<c:set value="${fn:split(detail.quesinfo.kbvalue,',') }" var="kbValues"/>
															<c:forEach items="${kbValues}" var="value">
															<li>
																<span>
																	<span class="incorrect" style="display: inline;margin-right:10px;">
																		<font style="cursor:pointer;" name="kbvalue" _value="${value }">${value }</font>
																	</span>
																</span>
															</li>
															</c:forEach>
														</c:if>
													</ul>
												</div>
											</s:iterator>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.morequeslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl title_left">多选题</div>
											<div class="fr title_right">
	                             				<div class="fl">
	                             					<span>
	                             						对题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.morecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						错题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.morequeslist.size-#request.map.morecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						得分
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.morescore"/>
	                             						</font>
	                             					</span>&nbsp;
	                             					<span>
	                             						共
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.morequeslist.size"/>
	                             						</font>
	                             						题
														<a href="javascript:void(0);" onclick="toggleinfo('moreques','morequesimg')">
															<img id="morequesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
															onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
														</a>
	                             					</span>
	                             				</div>
					                        </div>	
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="moreques">
											<div class="usbox">
											<s:iterator value="#request.map.morequeslist" var="detail" status="ind2">
												<input type="hidden" id="more_length" value="<s:property value='#request.map.morequeslist.size()'/>" />
												<input type="hidden" id="more_id_${ind2.index+1 }" value="${detail.ques_id}" />
												<input type="hidden" id="more_score_${ind2.index+1 }" value="${detail.referScore}" />
												<div class="exam_a">
													<span>
														${ind2.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore}分)
														<s:if test="#request.map.showAnswer==1">
															<s:if test="#detail.score==#detail.referScore"><span class="incorrect">正确</span></s:if>
															<s:else><span class="correct">错误</span></s:else>
														</s:if>
													</span>
													<ul>
														<s:iterator value="#request.detail.quesinfo.answer" var="resu" status="ind22">
															<li>
																<a href="javascript:void(0);">
																	<c:choose>
																		<c:when test="${fn:contains(detail.answer,resu.id ) }"><img src="${pageContext.request.contextPath}/resource/learning/images/ks_09.jpg" alt="" />&nbsp;</c:when>
																		<c:otherwise><img src="${pageContext.request.contextPath}/resource/learning/images/ks_12.jpg" alt="" />&nbsp;</c:otherwise>
																	</c:choose>
																	<span style="<s:if test="#request.map.showAnswer==1&&#request.resu.isright==1">color:green;</s:if>">${select_var[ind22.index]}.${resu.name}</span></a>
															</li>
														</s:iterator>
														<c:if test="${map.openBook == 1 && fn:trim(detail.quesinfo.kbvalue)!=''}">
															<c:set value="${fn:split(detail.quesinfo.kbvalue,',') }" var="kbValues"/>
															<c:forEach items="${kbValues}" var="value">
															<li>
																<span>
																	<span class="incorrect" style="display: inline;margin-right:10px;">
																		<font style="cursor:pointer;" name="kbvalue" _value="${value }">${value }</font>
																	</span>
																</span>
															</li>
															</c:forEach>
														</c:if>
													</ul>
												</div>
											</s:iterator>		
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.judgequeslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
											<div class="fl title_left">判断题</div>
											<div class="fr title_right">
	                             				<div class="fl">
	                             					<span>
	                             						对题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.judgecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						错题
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.judgequeslist.size -#request.map.judgecorrsize"/>
	                             						</font>
	                             						道
	                             					</span>&nbsp;
	                             					<span>
	                             						得分
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.judgescore"/>
	                             						</font>
	                             					</span>&nbsp;
	                             					<span>
	                             						共
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.judgequeslist.size"/>
	                             						</font>
	                             						题
														<a href="javascript:void(0);" onclick="toggleinfo('judgeques','judgequesimg')">
															<img id="judgequesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
															onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
														</a>
	                             					</span>
	                             				</div>
					                        </div>  	
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="judgeques">
											<div class="usbox">
												<s:iterator value="#request.map.judgequeslist" var="detail" status="ind3">
												<div class="exam_a">
													<span>
														${ind3.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore}分)
														<s:if test="#request.map.showAnswer==1">
															<s:if test="#detail.score==#detail.referScore"><span class="incorrect">正确</span></s:if>
															<s:else><span class="correct">错误</span></s:else>
														</s:if>
													</span>
													<ul>
														<li>
															<a href="javascript:void(0);">
																<s:if test="#request.detail.answer==1">
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg"/>&nbsp;
																</s:if>
																<s:else>
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>&nbsp;
																</s:else>
																<span style="<s:if test="#request.map.showAnswer==1&&#request.detail.quesinfo.isright==1">color:green;</s:if>">正确</span>
															</a>
														</li>
														<li>
															<a href="javascript:void(0);">
																<s:if test="#request.detail.answer==0">
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg"/>&nbsp;
																</s:if>
																<s:else>
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>&nbsp;
																</s:else>
																<span style="<s:if test="#request.map.showAnswer==1&&#request.detail.quesinfo.isright==0">color:green;</s:if>">错误</span>
															</a>
														</li>
														<c:if test="${map.openBook == 1 && fn:trim(detail.quesinfo.kbvalue)!=''}">
															<c:set value="${fn:split(detail.quesinfo.kbvalue,',') }" var="kbValues"/>
															<c:forEach items="${kbValues}" var="value">
															<li>
																<span>
																	<span class="incorrect" style="display: inline;margin-right:10px;">
																		<font style="cursor:pointer;" name="kbvalue" _value="${value }">${value }</font>
																	</span>
																</span>
															</li>
															</c:forEach>
														</c:if>
													</ul>
												</div>
												</s:iterator>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.blankqueslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
				                          	<div class="fl title_left">填空题</div>
											<div class="fr title_right">
	                             				<div class="fl">
	                             					<span>
	                             						共
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.blankqueslist.size"/>
	                             						</font>
	                             						题
														<a href="javascript:void(0);" onclick="toggleinfo('blankques','blankquesimg')">
															<img id="blankquesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
															onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
														</a>
	                             					</span>
	                             				</div>
					                        </div>  	
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="blankques">
											<div class="usbox">
												<s:iterator value="#request.map.blankqueslist" var="detail" status="ind4">
													<input type="hidden" id="blank_length" value="<s:property value='#request.map.blankqueslist.size()'/>" />
													<input type="hidden" id="blank_id_${ind5.index+1 }" value="${detail.ques_id}" />
													<input type="hidden" id="blank_score_${ind5.index+1 }" value="${detail.referScore}" />
													<input type="hidden" id="blank_answer_${ind4.index+1 }" value="${detail.answer}" />
													<div class="exam_a" id="blank_quescore_${ind4.index+1 }">
														<span>${ind4.index+1}.${detail.quesinfo.content}(${detail.referScore}分)</span>
														<ul>
															<c:if test="${map.showAnswer == 1}">
																<li style="width: 100%;">
																<s:iterator value="#detail.quesinfo.answer" var="resu">
																	<span>
																		<span class="incorrect" style="display: inline;margin-right:10px;">${resu.name }</span>
																	</span>
																</s:iterator>
																</li>
															</c:if>
															<c:if test="${map.openBook == 1 && fn:trim(detail.quesinfo.kbvalue)!=''}">
																<c:set value="${fn:split(detail.quesinfo.kbvalue,',') }" var="kbValues"/>
																<c:forEach items="${kbValues}" var="value">
																<li>
																	<span>
																		<span class="incorrect" style="display: inline;margin-right:10px;">
																			<font style="cursor:pointer;" name="kbvalue" _value="${value }">${value }</font>
																		</span>
																	</span>
																</li>
																</c:forEach>
															</c:if>
														</ul>
														<div class="point fr">
					                                     	分数 &nbsp;<input type="text" disabled="disabled" class="point_exam" id="score_blank_${ind5.index+1 }" value="${detail.score}"/>
					                                 	</div>
													</div>
													<div style="clear: both;"></div>
												</s:iterator>	
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.ansqueslist.size>0">
										<div class="eaxm_title dx" style="margin-bottom: 15px;">
				                          	<div class="fl title_left">简答题</div>
											<div class="fr title_right">
	                             				<div class="fl">
	                             					<span>
	                             						共
	                             						<font style="background-color: white;font-size: 13px;text-align:center;padding:0px 2px 0px 2px;">
	                             							<s:property value="#request.map.ansqueslist.size"/>
	                             						</font>
	                             						题
														<a href="javascript:void(0);" onclick="toggleinfo('ansques','ansquesimg')">
															<img id="ansquesimg" src="${pageContext.request.contextPath}/resource/learning/images/f_05.jpg" 
															onmouseover="this.src='${pageContext.request.contextPath}/resource/learning/images/hover_05.jpg'" width="20"/>
														</a>
	                             					</span>
	                             				</div>
					                        </div>  	
				                          	<div style="clear:both"></div>
										</div>
										<div class="exam_con" id="ansques">
											<div class="usbox">
											<s:iterator value="#request.map.ansqueslist" var="detail" status="ind5">
												<input type="hidden" id="ans_length" value="<s:property value='#request.map.ansqueslist.size()'/>" />
												<input type="hidden" id="ans_id_${ind5.index+1 }" value="${detail.ques_id}" />
												<input type="hidden" id="ans_score_${ind5.index+1 }" value="${detail.referScore}" />
												<div class="exam_c">
													<span>${ind5.index+1 }.${detail.quesinfo.content}&nbsp;&nbsp;&nbsp;(${detail.referScore}分)</span>
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" id="ans_${ind5.index+1}" cols="100%" class="text-exam">${detail.answer}</textarea>
														</div>
													</div>
													<div class="exam_a">
														<ul>
															<c:if test="${map.showAnswer == 1}">
																<li style="width: 100%;">
																<s:iterator value="#detail.quesinfo.answer" var="resu">
																	<span>
																		<span class="incorrect" style="display: inline;margin-right:10px;">${resu.name }</span>
																	</span>
																</s:iterator>
																</li>
															</c:if>
															<c:if test="${map.openBook == 1 && fn:trim(detail.quesinfo.kbvalue)!=''}">
																<c:set value="${fn:split(detail.quesinfo.kbvalue,',') }" var="kbValues"/>
																<c:forEach items="${kbValues}" var="value">
																<li>
																	<span>
																		<span class="incorrect" style="display: inline;margin-right:10px;">
																			<font style="cursor:pointer;" name="kbvalue" _value="${value }">${value }</font>
																		</span>
																	</span>
																</li>
																</c:forEach>
															</c:if>
														</ul>
													</div>
													<div class="point fr">
				                                     	分数 &nbsp;<input type="text"  disabled="disabled" class="point_exam" id="score_ans_${ind5.index+1}" value="${detail.score}"/>
				                                 	</div>
				                                 	<div style="clear: both;"></div>
												</div>
											</s:iterator>
											</div>
										</div>
									</s:if>
									<div class="eaxm_title dx">
											点评
										</div>
										<div class="exam_con">
											<div class="usbox">
												<div class="exam_c">
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" readonly="readonly" id="comment" cols="100%" class="text-exam">${map.examinee.examRecord.comment }</textarea>
														</div>
													</div>
												</div>
												<div style="clear: both;"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</body>
</html>

