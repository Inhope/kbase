<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/exam.js"></script>
		<style type="text/css">
			td a{color:blue}
			.gonggao_con_nr_full{background:#FFF;width:100%;height:100%;}
			.gonggao_con_nr_full table{border-right:1px solid #edecec;}
			.gonggao_con_nr_full .tdbg{background:#f5f9ff;font-weight:bold;color:#333333;}
			.gonggao_con_nr_full .tdbg td{color:#333333;font-weight:bold;}
			.gonggao_con_nr_full td{word-break:break-all;border:1px solid #edecec;border-top:none;border-right:none;color:#666;line-height:33px;text-align:center;}
			.gonggao_con_nr_full .trfen{background:#f5f9ff;}
		</style>
	</head>
	<body>
		<div style="width: 100%;">
			<div style="width: 16%;float: left;">
				<jsp:include page="../exam/ExamCate.jsp"></jsp:include>
			</div>
			
				<div data-options="region:'center',title:''" style="width: 84%;float: left;">
					<div id="p" class="easyui-panel" data-options="fit:false"
						style="border: 0px;width: 100%;overflow: hidden;">
						<form id="form0" action="" method="post">
							<input type="hidden" name="pbTaskModel.ppType.id" id="ppTypeId" value="${pbTaskModel.ppType.id}" />
							<input type="hidden" name="pbTaskModel.ppType.path" id="ppTypeNamesInput" value="${pbTaskModel.ppType.path}" />
							<input type="hidden" name="exam.examCate.bh" id="examCate_bh" value="${exam.examCate.bh }" />
							<div class="content_right_bottom" style="width: 100%;">
								<div class="gonggao_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;">
									<div style="float: left;">
										<b id="ppTypeNames" style="">${pbTaskModel.ppType.path}</b>
									</div>
									<div class="gonggao_titile_right" style="margin-right: 10px;">
										<%-- <myTag:input type="a" value="批量导出" key="examManageExport" id="export"/> --%>
										<myTag:input type="a" value="删除" key="examManageDel" id="todelete"/>
										<myTag:input type="a" value="编辑" key="examManageEdit" id="toedit"/>
										<myTag:input type="a" value="新建" key="examManageAdd" id="toadd"/>
										<%-- <s:iterator value="#request.menuList" var="menu">
											<s:if test="#menu.key == 'quesManageMoveCate'">
							          			<a href="javascript:void(0);" id="tomove" style="float: left; margin-right: 0px;">移动分类</a>
												<input type="text" id="examCateNames_select" name="examCateNames" onclick="showCate('cateContent','examCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
													style="height: 26px;border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
							          			<input type="hidden" id="examCateIds_select" name="examCateIds">&nbsp;
											</s:if>
										</s:iterator> --%>
										<kbs:if key="examManageMoveCate">
											<a href="javascript:void(0);" id="tomove" style="float: left; margin-right: 0px;">移动分类</a>
											<input type="text" id="examCateNames_select" name="examCateNames" onclick="showCate('cateContent','examCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
												style="height: 26px;border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
						          			<input type="hidden" id="examCateIds_select" name="examCateIds">&nbsp;
										</kbs:if>
									</div>
								</div>
								<div class="yonghu_titile" style="margin-left: 0px;margin-right: 0px;width: 100%;float: left;height: auto;">
									<ul>
										<li>
											考试名称
											<input type="text" id="exam_name" name="exam.name" value="${exam.name}" style="width:120px"/>
										</li>
										<li>
											试卷名称
											<input type="text" id="paper_name" name="exam.paper.name" value="${exam.paper.name}" style="width:120px"/>
										</li>
										<li>
											开始日期
											<input type="text" id="start_time" name="exam.start_time" value="${exam.start_time }" onclick="javascript:WdatePicker({maxDate:'#F{$dp.$D(\'end_time\')}',dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" style="width:120px"/>
											-
											<input type="text" id="end_time" name="exam.end_time" value="${exam.end_time}" onclick="javascript:WdatePicker({minDate:'#F{$dp.$D(\'start_time\')}',dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" style="width:120px"/>
										</li>
										<li>
											考试状态
											<select id="status" name="exam.status">
												<option value="">--请选择--</option>
												<option value="0" <c:if test="${exam.status==0 }">selected="selected"</c:if>>考试未开始</option>
												<option value="1" <c:if test="${exam.status==1 }">selected="selected"</c:if>>考试进行中</option>
												<option value="2" <c:if test="${exam.status==2 }">selected="selected"</c:if>>考试已结束</option>
											</select>
										</li>
										<li class="anniu" style="float: right;margin-right: 10px;">
											<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
											<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="查询" onclick="javascript:pageClick('1');" /> </a>
										</li>
									</ul>
								</div>
								<div id="content" style="float: left;width: 100%;margin-right: 0px;margin-left: 0px;">
									<div class="gonggao_con" style="margin-left: 0px;margin-right: 0px;">
										<div class="gonggao_con_nr_full">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="tdbg">
													<td width="2%">
														<input type="checkbox" value="" id="" onclick="checkAll();"/>
													</td>
													<td width="10%">
														考试名称
													</td>
													<td width="10%">
														试卷名称
													</td>
													<td width="12%">
														开始日期
													</td>
													<td width="12%">
														结束日期
													</td>
													<td width="8%">
														创建人
													</td>
													<td width="7%">
														考试人数
													</td>
													<td width="8%">
														考试完成率
													</td>
													<td width="8%">
														缺考人数
													</td>
													<td width="5%">
														合格率
													</td>
													<td width="8%">
														考试状态
													</td>
													<td width="10%">操作</td>
												</tr>
												<s:iterator value="page.result" var="va">
													<tr>
														<td><input type="checkbox" name="exam_id" status="${va.examinfo.status}" value="${va.id }"/></td>
														<td>${va.name }</td>
														<td><a href="#" onclick="showPaperInfo('${va.paper.id }','${va.paper.type }');">${va.paper.name }</a></td>
														<td>${va.start_time }</td>
														<td>${va.end_time }</td>
														<td>${va.createUserNameCN}</td>
														<td>${va.examinfo.examcount}</td>
														<td>${va.examinfo.completepercent}</td>
														<td>${va.examinfo.noexamcount}</td>
														<td>${va.examinfo.passpercent}</td>
														<td><s:if test="#va.examinfo.status ==0">考试未开始</s:if><s:if test="#va.examinfo.status ==1">考试进行中</s:if><s:if test="#va.examinfo.status ==2">考试结束</s:if></td>
														<td><a href="#" onclick="topreview('${va.id }')">查看</a>||<a href="#" onclick="showdetailinfo('${va.id }')">管理</a></td>
													</tr>
												</s:iterator>
												<tr class="trfen">
													<td colspan="12">
														<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
													</td>
												</tr>
											</table>
										</div>
									</div>
								<div id="content">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div id="cateContent" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="examcatetree" class="ztree" style="margin-top:0; width:190px;"></ul>
		</div>
	</body>
</html>

