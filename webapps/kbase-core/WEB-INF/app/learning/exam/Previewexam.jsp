<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
				<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<style type="text/css">
			.inputflag{color:red;font-size: 14px;}
		</style>	
		<script type="text/javascript">
			$(function(){
				//点击折叠按钮
			    $("#showOrHideInfo,#showOrHideBatch").on("click",function(){
			    	var url = $.fn.getRootPath()+"/resource/learning/images/fold_up.png";
			    	var _this;
			    	if($(this).attr("id") == 'showOrHideInfo'){
			    		_this = $("#infoArea tr").slice(1,8);
			    	}else if($(this).attr("id") == 'showOrHideBatch'){
			    		_this = $("#batchinfo");
			    	}
			    	if(_this.is(":hidden")){
			    		_this.show();
			    		url = $.fn.getRootPath()+"/resource/learning/images/fold_down.png";
			    	}else{
			    		_this.hide();
			    	}
			    	$(this).attr("src",url);
				});
			});
			function previewbatch(id){
				_batchindex = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['查看批次', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :$.fn.getRootPath()+'/app/learning/exam-batch!previewbatch.htm?id='+id},
					area: ['100%', '60%']
				});
			};
		</script>
	</head>
	<body>
			<form name="form1" id="form1" method="post" action="">
				<input type="hidden" name="exam.id" id="exam_id"  value="${exam.id }">
				<table id="infoArea" width="100%" cellspacing="0" cellpadding="0" class="box" style="width: 100%;border: 0px;">
					<tr>
						<td colspan="2" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
							<div class="background_theme">
							<font class="font_style">基本信息</font>
							<img id="showOrHideInfo" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
								src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
							</div>
						</td>
					</tr>
					<tr>
						<td>考试名称<span class="inputflag">*</span></td>
						<td><input type="text" id="exam_name" readonly="readonly" value="${exam.name }" name="exam.name" style="width:92%"/></td>
					</tr>
					<tr>
						<td>考试用卷<span class="inputflag">*</span></td>
						<td><input type="text" id="paperName" readonly="readonly" value="${exam.paper.name}" name="exam.paper.name" readonly="readonly" style="width:82%"/><input type="hidden" id="paperId" name="exam.paper.id" value="${exam.paper.id }" /></td>
					</tr>
					<tr>
						<td>考试时间<span class="inputflag">*</span></td>
						<td>
							<input type="text" name="exam.start_time" readonly="readonly" id="start_time" value="${exam.start_time}" class="Wdate" style="width:30%;" />
							<input type="text"  name="exam.end_time" value="${exam.end_time}" id="end_time" class="Wdate" style="width:30%;" />
						</td>
					</tr>
					<tr>
						<td>考试分类<span class="inputflag">*</span></td>
						<td><input type="text" id="cateNames_select" readonly="readonly" value="${exam.examCate.name }" name="exam.examCate.name" style="width:92%"/>
						<input type="hidden" id="cateIds_select" value="${exam.examCate.id }" name="exam.examCate.id" />
						</td>
					</tr>
					<tr>
						<td>考时限制<span class="inputflag">*</span></td>
						<td>
							考试时间限制:<input type="text" value="${exam.late_minutes }" readonly="readonly" id="late_minutes" name="exam.late_minutes" style="width:5%;" />分钟<br>
							交卷时间限制:<input type="text" value="${exam.finish_minutes }" name="exam.finish_minutes" id="finish_minutes" style="width:5%;"  readonly="readonly"/>分钟
						</td>
					</tr>
					<tr>
						<td>发布设置<span class="inputflag">*</span></td>
						<td>
							<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==1">checked="checked"</s:if> value="1" disabled="disabled" style="margin-right: 10px;margin-bottom: 10px;"/>自动发布<br>
							<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==2">checked="checked"</s:if> value="2" disabled="disabled" style="margin-right: 10px;margin-bottom: 10px;"/>预约发布
							<input id="release_2" name="exam.release_time" value="${exam.release_time }"   class="Wdate" disabled="disabled" type="text"  readonly="readonly"/><br>
							<input type="radio" name="exam.release_type" <s:if test="#request.exam.release_type==3">checked="checked"</s:if> value="3" disabled="disabled" style="margin-right: 10px;"/>手动发布人
							<input type="text"  id="release_3" value="${exam.release_username}" name="exam.release_username" readonly="readonly"/>
							<input type="hidden" value="${exam.release_userid }" id="release_userid" name="exam.release_userid"/>&nbsp;
						</td>
					</tr>
					<tr>
						<td>是否显示答案</td>
						<td>
							<input type="radio" name="exam.isShowAnswer" ${exam.isShowAnswer==1?"checked='checked'":""} 
								value="1" style="margin-right: 10px;margin-bottom: 10px;" disabled="disabled"/>是<br>
							<input type="radio" name="exam.isShowAnswer" ${exam.isShowAnswer==1?"":"checked='checked'"} 
								value="0" style="margin-right: 10px;" disabled="disabled"/>否
						</td>
					</tr>
					<tr>
						<td>开卷/闭卷</td>
						<td>
							<s:select disabled="true" name="exam.isOpenBook" key="exam.isOpenBook" value="#request.exam.isOpenBook" list="#{'0':'闭卷','1':'开卷'}" style="width: 50px;"></s:select>
						</td>
					</tr>
					<tr>
					    <!-- 去除考试说明的必填状态 -->
						<td>考试说明</td>
						<td><textarea id="remark" name="exam.remark" readonly="readonly" rows="3" style="width:92%;">${exam.remark }</textarea></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
							<div class="background_theme">
							<font class="font_style">考试批次</font>
							<img id="showOrHideBatch" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
								src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
							</div>
						</td>
					</tr>
				</table>
			</form>
				<table width="100%" cellspacing="0" cellpadding="0" class="box" id="batchinfo" style="width: 100%;border: 0px;">
					<tr>
						<td	align="center" style="width: 60px;"><b>批次</b></td>
						<td	align="center" style="width: 40px;"><b>人数</b></td>
						<td	align="center"><b>人员</b></td>
						<td	align="center" style="width: 260px;"><b>时间</b></td>
						<td align="center" style="width: 80px;"><b>操作</b></td>
					</tr>
					<s:iterator value="#request.batchlist" var="batch" status="ind">
						<input type="hidden" id="batchstart_time" value="<fmt:formatDate  value="${batch.start_time}" pattern="yyyy-MM-dd HH:mm" />" />
						<input type="hidden" id="batchend_time" value="<fmt:formatDate  value="${batch.end_time}" pattern="yyyy-MM-dd HH:mm" />" />
						<tr id="batchinfo_${ind.index+1 }">
							<td	align="center">${batch.batch_name }</td>
							<td	align="center">${batch.usercount }</td>
							<td	align="center">
								${batch.scopeNames }
							</td>
							<td	align="center"><fmt:formatDate  value="${batch.create_date}" pattern="yyyy-MM-dd HH:mm" /></td>
							<td  align="center"><a href="#" onclick="previewbatch('${batch.batch_id }','batchinfo_${ind.index+1 }')">查看</a></td>
						</tr>
					</s:iterator>
				</table>
	</body>
</html>

