<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>考试管理</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<!-- layer -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<style type="text/css">
		.examtop_left ul{width:15%}
	</style>
	<script type="text/javascript">	
		$(function(){
			var usedTime = '${map.examinee.examRecord.exam_interval}';
			usedTime = usedTime==''?"0":usedTime;
			var intervalDate=parseInt(usedTime);//考试已用时间
			//初始化倒计时
			var examTime = parseInt('${map.examinee.exambatch.exam.late_minutes}')*60;/*计算考试时间（秒数）*/
			var stayTime = examTime - intervalDate;/*剩余考试时间（秒数）*/
			var hour = parseInt(stayTime/(60*60));
			var minute = parseInt(stayTime/60%60);
			var second = parseInt(stayTime - hour*60*60 - minute*60);
			if(examTime >= intervalDate){
				if(minute < 10) minute = '0' + minute;
				if(second < 10) second = '0' + second;
				$('#hours').html(hour);
				$('#minites').html(minute);
				$('#second').html(second);
			}else{
				$('#hours').html('00');
				$('#minites').html('00');
				$('#second').html('00');
			}
			//考试题目:关闭操作
			_closeOperate = function(){
				$("#px_b_kc_id").find("input[type='radio'],input[type='checkbox']").attr("disabled","disabled");
				$("#px_b_kc_id").find("input[type='text'],textarea").attr("readonly","readonly");
			}
			//考试题目:开启操作
			_openOperate = function(){
				$("#px_b_kc_id").find("input[type='radio'],input[type='checkbox']").removeAttr("disabled");
				$("#px_b_kc_id").find("input[type='text'],textarea").removeAttr("readonly");
			}
			//默认关闭操作，点击开始考试时放开操作
			_closeOperate();
			//点击开始考试
			$("#tochange").on('click',function(){
				$(this).find('a').html('交卷');
				$(this).find('a').css({'background-color': 'gray'});
				$(this).unbind('click');
				var examId = '${map.examinee.id}';
				//开始考试后更新考试状态
				//$('body').ajaxLoading('正在查询数据...');
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/learning/examinee!changeStatus.htm',
					data:{"examId":examId,"quesIds":'${quesIds}'},
					async: true,
					dataType : 'json',
					success : function(data){
						$('body').ajaxLoadEnd();/*去除遮罩*/
						if(data.result){/*正确修改考试任务状态才允许考试*/
							//放开考试题目操作
							_openOperate();
							//若是闭卷考试不能点击页面以外其他地方
							
							
							
							/*考试倒计时*/
							var ft = parseInt('${map.examinee.exambatch.exam.finish_minutes}')*60;/*计算交卷时限（秒数）*/
							var lt = parseInt('${map.examinee.exambatch.exam.late_minutes}')*60;/*计算考试时间（秒数）*/
							var st = new Date().getTime()-intervalDate*1000;
							var i = setInterval(function(){
								var et = new Date().getTime();
								var tt = intervalDate = parseInt((et - st)/1000);/*计算考试时间（已过，秒数）*/
								/*10分钟提示*/
								var ot = lt - tt;/*剩余考试时间（秒数）*/
								var h = parseInt(ot/(60*60));
								var m = parseInt(ot/60%60);
								var s = parseInt(ot - h*60*60 - m*60);
								if(m > 0 && m == 10*60) {
									layer.alert("离考试结束还有10分钟!", -1);
								}
								/*交卷时间限制*/
								if(tt > ft){
									$('#tochange').hide();
									$('#tosave').show();
								}
								/*考试倒计时*/
								if(lt >= tt){
									if(m < 10) m = '0' + m;
									if(s < 10) s = '0' + s;
									$('#hours').html(h);
									$('#minites').html(m);
									$('#second').html(s);
									/*考试时间到了自动提交*/
									if(lt == tt){
										$("#tosave").click();
									}
								} else {
									/*取消定时器*/
									clearInterval(i);
								}
							}, 1000);
							//每10秒记录一次耗费时间
							setInterval(function(){
								_updateRecord();
							}, 10*1000);
						} else {
							layer.alert("网络错误，请稍后再试!", -1);
						}
					},error : function(msg){
						layer.alert("网络错误，请稍后再试!", -1);
					}
				});
			});
			
			//保存考试耗费时间
			_updateRecord = function(){
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/learning/exam-record!updateRecord.htm',
					traditional: true,
					data :{"id":'${map.examinee.id}',"intervalDate":intervalDate},
					async: true,
					dataType : 'json',
					success : function(){},
					error : function(){}
				});
			}
	        
			//交卷
			$("#tosave").on('click',function(){
				var _layer_confirm = layer.confirm("确定要交卷吗？", function(){
					var examId = '${map.examinee.id}';
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/learning/examinee!saveExaminee.htm',
						traditional: true,//传数组(考虑数组join分割后台不好取)
						data :{"examId":examId,"intervalDate":intervalDate,"quesIds":'${quesIds}'},
						async: true,
						dataType : 'json',
						success : function(data){
							if(data.result == true){
								layer.alert(data.msg, -1, function(){
									parent.window.location.reload();
									parent.layer.close(parent.__kbs_layer_index);
								});
							}else{
								layer.alert(data.msg, -1);
							}
						},
						error : function(msg){
							layer.alert("网络错误，请稍后再试!", -1);
						}
					});
				});
			});
			
			//自动保存题目答案-操作选项时触发
			$("[name='singleQues'],[name='moreQues'],[name='judgeQues'],[name='blankQues'],[name='ansQues']").on("change",function(){
				var examId = '${map.examinee.id}';//考试id
				var quesId = $(this).find("#quesId").val();//题目id
				var referScore = $(this).find("#quesScore").val();//题目参考分数
				var quesScore = "0";//题目分数
				var answer = "";//答案选项id
				var quesType;
				var answerArr;//所有答案选项
				if($(this).attr("name")=="singleQues"){
					quesType = "0";//单选
					answerArr = $(this).find("[name^='singleCheckResult']");
				}else if($(this).attr("name")=="moreQues"){
					quesType = "1";//多选
					answerArr = $(this).find("[name='moreCheckResult']");
				}else if($(this).attr("name")=="judgeQues"){
					quesType = "2";//判断
					answerArr = $(this).find("[name^='judgeCheckResult']");
				}else if($(this).attr("name")=="blankQues"){
					quesType = "3";//填空
					answerArr = $(this).find("input[class='input-exam']");
				}else if($(this).attr("name")=="ansQues"){
					quesType = "4";//简答
					answerArr = $(this).find("textarea[class='text-exam']");
				}
				if(quesType == '0' || quesType == '1' || quesType == '2'){
					var flag = true;
					answerArr.each(function(){
						if($(this).is(":checked")){//选到了错误答案
							if(answer == ""){
								answer = $(this).val();
							}else{
								answer += "|"+$(this).val();
							}
							if($(this).attr("isright")==0){
								flag = false;
							}
						}else if(!$(this).is(":checked")){//未选到正确答案
							if($(this).attr("isright")==1){
								flag = false;
							}
						}
					});
					if(flag) quesScore = $(this).find("#quesScore").val();
				}else if(quesType == '3' || quesType == '4'){
					answerArr.each(function(){
						if(answer == ""){
							answer = $(this).val();
						}else{
							answer += "|"+$(this).val();
						}
					});
				}
				var param = {
					"examId":examId,
					"quesId":quesId,
					"referScore":referScore,
					"quesScore":quesScore,
					"answer":answer,
					"quesType":quesType,
					"intervalDate":intervalDate
				}
				_saveExamQues(param);
			});
			
			_saveExamQues = function(param){
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/learning/examinee!saveExamQues.htm',
					traditional: true,
					data :param,
					async: true,
					dataType : 'json',
					success : function(data){
						if(data.status == 0) console.info(data.msg);
						else console.info(data.msg);
					},
					error : function(msg){
						layer.alert("网络错误，请稍后再试!", -1);
					}
				});
			}
		});
		//填充答题数据:填空，问答
		$(function(){
			$("[name='blankQues']").each(function(){
				var detail = $(this).find("[name='details']").val();
				var details = detail.split("|");
				$(this).find("input[class='input-exam']").each(function(i,item){
					$(this).val(details[i]);
				});
			});
			$("[name='ansQues']").each(function(){
				var detail = $(this).find("[name='details']").val();
				var details = detail.split("|");
				$(this).find("textarea[class='text-exam']").each(function(i,item){
					$(this).val(details[i]);
				});
			});
		});
	</script>
</head>
<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top">
					<div class="usbox">
						<div class="examtop_right fr" id="tochange" style="margin-right:20px;">
							<a href="javascript:void(0);" >开始考试</a>
						</div>
						<div class="examtop_right fr" id="tosave" style="display: none;margin-right:20px;">
							<a href="javascript:void(0);">交卷</a>
						</div>
						<div style="width: 85%"><span style="font-weight:bold;margin-left:45%;font-size:14px">${map.examinee.paper.name }</span></div>
						<input type="hidden" id="openBook" value="${map.examinee.exambatch.exam.isOpenBook }"/>
						<div class="examtop_left fl" style="width:80%">
							<ul>
								<li>
									考生：<span>${map.examinee.user.userInfo.userChineseName }</span>
								</li>
							</ul>
							<ul>
								<li>
									部门：<span>${map.user.mainStation.dept.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总数：<span><s:property value='#request.map.totalques'/></span>
								</li>
							</ul>
							<ul>
								<li>
									考题限时：<span>${map.examinee.exambatch.exam.finish_minutes}分钟</span>
								</li>
							</ul>
							<ul>
								<li>
									考题总分：${map.examinee.paper.score}
								</li>
							</ul>
							<ul>
								<li>
									剩余时间：
									<input type="hidden" value="${map.examinee.exambatch.exam.finish_minutes}" id="finish_minutes" />
									<span id="hours">0</span>:<span id="minites">${map.examinee.exambatch.exam.late_minutes}</span>:<span id="second">00</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div id="bg"></div>
				<div class="px_b_kc" id="px_b_kc_id">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
								<s:set var="select_var" value="{'A','B','C','D','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									<s:if test="#request.map.singlequeslist.size>0">
										<div class="eaxm_title">
											单选题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.singlequeslist" var="coilques" status="ind1">
												<div class="exam_a" name="singleQues">
													<input type="hidden" id="quesId" value="${coilques.ques.id}" />
													<input type="hidden" id="quesScore" value="${coilques.score}" />
													<s:set value="#request.detailMap[#coilques.ques.id]" var="resultId"/>
													<span>${ind1.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<s:iterator value="#request.coilques.ques.quesResults" var="resu" status="ind11">
															<li>
																<a href="javascript:void(0);">
																	<input type="radio" name="singleCheckResult_${ind1.index }" isright="${resu.isRight}" value="${resu.id}"
																	${resu.id==resultId?"checked":""} />
																	${select_var[ind11.index]}.${resu.content}
																</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>
											<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.morequeslist.size>0">
										<div class="eaxm_title dx">
											多选题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.morequeslist" var="coilques" status="ind2">
												<div class="exam_a" name="moreQues">
													<input type="hidden" id="quesId" value="${coilques.ques.id}" />
													<input type="hidden" id="quesScore" value="${coilques.score}" />
													<s:set value="#request.detailMap[#coilques.ques.id]" var="resultId"/>
													<span>${ind2.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<s:iterator value="#request.coilques.ques.quesResults" var="resu" status="ind22">
															<li>
																<a href="javascript:void(0);">
																	<input type="checkbox" name="moreCheckResult" isright="${resu.isRight}" value="${resu.id}" 
																	${fn:contains(resultId,resu.id)?"checked":""}/>
																	${select_var[ind22.index]}.${resu.content}
																</a>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:iterator>
											<div style="clear: both;"></div>	
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.judgequeslist.size>0">
										<div class="eaxm_title dx">
											判断题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.judgequeslist" var="coilques" status="ind3">
												<div class="exam_b" name="judgeQues">
													<input type="hidden" id="quesId" value="${coilques.ques.id}" />
													<input type="hidden" id="quesScore" value="${coilques.score}" />
													<s:set value="#request.detailMap[#coilques.ques.id]" var="resultId"/>
													<span>${ind3.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<ul>
														<li>
															<input type="radio" name="judgeCheckResult_${ind3.index }" value="1" isright="${coilques.ques.isRight=='1'?1:0}"
															${resultId==1?"checked":"" }/>
															<a href="javascript:void(0);">对</a>
														</li>
														<li>
															<input type="radio" name="judgeCheckResult_${ind3.index }" value="0" isright="${coilques.ques.isRight=='0'?1:0}" 
															${resultId==0?"checked":"" }/>
															<a href="javascript:void(0);">错</a>
														</li>
													</ul>
												</div>
											</s:iterator>
											<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<s:if test="#request.map.blankqueslist.size>0">
										<div class="eaxm_title dx">
											填空
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.blankqueslist" var="coilques" status="ind4">
												<div class="exam_b" name="blankQues">
													<input type="hidden" id="quesId" value="${coilques.ques.id}" />
													<input type="hidden" id="quesScore" value="${coilques.score}" />
													<s:set value="#request.detailMap[#coilques.ques.id]" var="resultId"/>
													<input type="hidden" name="details" value="${resultId}" />
													<span>
														${ind4.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)
													</span>
												</div>
											</s:iterator>
											<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									<s:if test="#request.map.ansqueslist.size>0">
										<div class="eaxm_title dx">
											简答题
										</div>
										<div class="exam_con">
											<div class="usbox">
											<s:iterator value="#request.map.ansqueslist" var="coilques" status="ind5">
												<div class="exam_c" name="ansQues">
													<input type="hidden" id="quesId" value="${coilques.ques.id}" />
													<input type="hidden" id="quesScore" value="${coilques.score}" />
													<s:set value="#request.detailMap[#coilques.ques.id]" var="resultId"/>
													<input type="hidden" name="details" value="${resultId}" />
													<span>${ind5.index+1 }.${coilques.ques.content}&nbsp;&nbsp;(${coilques.score}分)</span>
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" cols="100%" class="text-exam"></textarea>
														</div>
													</div>
												</div>
											</s:iterator>
											<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
