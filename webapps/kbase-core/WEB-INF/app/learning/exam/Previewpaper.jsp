<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-预览</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/style.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">	
			
		</script>
	</head>
	<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top" style="height: 60px;">
					<div class="usbox" style="margin-top: 10px;">
						<div class="examtop_left fl" style="width: 90%;">
							<ul>
								<li>
									考卷名称：
									<span>${paper.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									<s:set var="amount" value="0"></s:set>
									<s:iterator value="#request.paper.mapRange" var="va">
										<s:set var="amount" value="#amount + #va.value.amount" ></s:set>
									</s:iterator>
									考题总数：
									<span><s:property value="#amount"/> </span>
								</li>
							</ul>
							<ul>
								<li style="margin-top: -8px;">
									考题总分：
									<span class="px_red">${paper.score}</span>
								</li>
							</ul>
							<ul>
								<li style="margin-top: -8px;">
									及格分数：
									<span class="px_red">${paper.scorePass}</span>
								</li>
							</ul>
						</div>
						<div class="examtop_right fr" style="width: 10%;">
							<%--
								<a href="javascript:void(0);" style="line-height: normal;height: auto;width: 80%;">导出</a>
							 --%>
		                </div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
									<!-- 标题名称  -->
									<s:set var="title_var" value="{'一','二','三','四','五','六','七','八','九','十'}"></s:set>
									<s:set var="ind_title_var" value="0"></s:set>
									<!-- 选项名称 -->
									<s:set var="select_var" value="{'A','B','C','D','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									
									<!-- 单选题 -->
									<s:set var="key" value="0 + ''"></s:set>
									<s:set var="data" value="#request.quesMap[#key]"></s:set>
									<s:if test="#data != null && #data.size() > 0">
										<div class="eaxm_title">
											${title_var[ind_title_var] }、${quesTypes[key] }
											<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
										</div>
										<div class="exam_con">
											<div class="usbox">
												<s:iterator value="#data" var="va" status="st">
													<!--正确答案  -->
													<s:set var="isRight" value="''"></s:set>
													<!--遍历答案选项  -->
													<div class="exam_a">
														<span>${st.index + 1 }.${va.content }</span>
														<ul>
															<s:iterator value="#va.quesResults" var="va1" status="st1">
																<li>
																	<a href="javascript:void(0);">
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;${select_var[st1.index] }.${va1.content }
																	</a>
																</li>
															</s:iterator>
														</ul>
													</div>
												</s:iterator>
											</div>
										</div>
									</s:if>

									
									<!-- 多选题 -->
									<s:set var="key" value="1 + ''"></s:set>
									<s:set var="data" value="#request.quesMap[#key]"></s:set>
									<s:if test="#data != null && #data.size() > 0">
										<div class="eaxm_title dx">
											${title_var[ind_title_var] }、${quesTypes[key] }
											<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
										</div>
										<div class="exam_con">
											<div class="usbox">
												<s:iterator value="#data" var="va" status="st">
													<!--正确答案  -->
													<s:set var="isRight" value="''"></s:set>
													<!--遍历答案选项  -->
													<div class="exam_a">
														<span>${st.index + 1 }.${va.content }</span>
														<ul>
															<s:iterator value="#va.quesResults" var="va1" status="st1">
																<li>
																	<a href="javascript:void(0);">
																		<img src="${pageContext.request.contextPath}/resource/learning/images/ks_12.jpg" alt="" />&nbsp;${select_var[st1.index] }.${va1.content }
																	</a>
																</li>
															</s:iterator>
														</ul>
													</div>
												</s:iterator>
											</div>
										</div>
									</s:if>

									
									<!-- 判断题 -->
									<s:set var="key" value="2 + ''"></s:set>
									<s:set var="data" value="#request.quesMap[#key]"></s:set>
									<s:if test="#data != null && #data.size() > 0">
										<div class="eaxm_title dx">
											${title_var[ind_title_var] }、${quesTypes[key] }
											<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
										</div>
										<div class="exam_con">
											<div class="usbox">
												<s:iterator value="#data" var="va" status="st">
													<!--遍历答案选项  -->
													<div class="exam_b">
														<span>${st.index + 1 }.${va.content }</span>
														<ul>
															<li>
																<a href="javascript:void(0);">
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;对
																</a>
															</li>
															<li>
																<a href="javascript:void(0);">
																	<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;错
																</a>
															</li>
														</ul>
													</div>
												</s:iterator>
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<!-- 填空题 -->
									<s:set var="key" value="3 + ''"></s:set>
									<s:set var="data" value="#request.quesMap[#key]"></s:set>
									<s:if test="#data != null && #data.size() > 0">
										<div class="eaxm_title dx">
											${title_var[ind_title_var] }、${quesTypes[key] }
											<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
										</div>
										<div class="exam_con">
											<div class="usbox">
												<s:iterator value="#data" var="va" status="st">
													<div class="exam_b">
														<span>${st.index + 1 }.${va.content }</span>
													</div>
												</s:iterator>
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>
									
									<!-- 简答题 -->
									<s:set var="key" value="4 + ''"></s:set>
									<s:set var="data" value="#request.quesMap[#key]"></s:set>
									<s:if test="#data != null && #data.size() > 0">
										<div class="eaxm_title dx">
											${title_var[ind_title_var] }、${quesTypes[key] }
											<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
										</div>
										<div class="exam_con">
											<div class="usbox">
												<s:iterator value="#data" var="va" status="st">
													<!--遍历答案选项  -->
													<div class="exam_a">
														<span>${st.index + 1 }.${va.content }</span>
														<div class="exam_ca">
															<div class="usbox">
																<textarea rows="10" cols="100%" class="text-exam"></textarea>
															</div>
														</div>
													</div>
												</s:iterator>
												<div style="clear: both;"></div>
											</div>
										</div>
									</s:if>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

</html>

