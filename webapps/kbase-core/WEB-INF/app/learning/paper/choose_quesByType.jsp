<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>试卷试题选择</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			body{
				margin: 3px;
			}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 398px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
			.treediv{font-size:14px;}
			.queschoose{display:block;white-space:nowrap; overflow:hidden; text-overflow:ellipsis;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			//点击显示和隐藏
			function shwoinfo(obj){
				if($(obj).is('.queschoose')){
					$(obj).removeClass();
					$(obj).after('<br>');
				}else{
					$(obj).addClass('queschoose');
					$(obj).next().remove();
				}
			}
			//题目类型
			var _key = '${param.key}';
			_chkStyle = '${param.chkStyle}';/*树单选radio、多选checkbox*/
			if(!_chkStyle) _chkStyle = 'checkbox';
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath() + '/app/learning/paper!getCateTree.htm',
					autoParam:['id'],
					otherParam:{'key': _key}
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "s", "N": "s" }
				},
				callback: {
					//onCheck: ztreeOnCheck,
					beforeCheck: ztreeOnBeforeCheck,
					onClick: zTreeOnClick,
					onAsyncSuccess:onAsync,
					onExpand: ztreeOnExpand
				}
			};
			var TreeUtils = {
				fn: {
					/*左侧区域数据删除*/
					rightHandle: function (objs, flag){
						if(!objs) return false;
						if(flag == 'Add'){/*添加*/
							if(_chkStyle == 'radio') /*单选应当移除所有已有项*/
								$('#sel option').remove();
							$(objs).each(function(i, node){
								if ($("#sel option[value='"+node.id+"']").length == 0){
									$('#sel').append('<option value="' + node.id + '">' + node.name + '</option>');
									$("#sel option[value='" + node.id + "']").data('content', node.content);
								}
							});
						} else if(flag == 'Del'){/*删除*/
							$(objs).each(function(i, node){
								if ($("#sel option[value='"+node.id+"']").length>0){
									$("#sel option[value='"+node.id+"']").remove();
								}
							});
						}
					},
					/*名称获取父页面对象*/
					getObjByName: function(name){
						var obj = parent;
						if(name.indexOf('.') > -1){
							var _that;
							$(name.split('.')).each(function(i, item){
								_that = obj;/*this对象应改为上一级*/
								obj = $(obj).attr(item);
							});
							return [_that, obj];
						} else {
							return $(obj).attr(name);
						}
					}
				}
			};
			//回显数据
			function onAsync(event, treeId, treeNode, msg){
					var _that = TreeUtils, /*回显数据*/
					initData = _that.fn.getObjByName('${param.initData}');
					/*数据回显*/
					if(initData.length > 1)/*回掉*/
						initData = initData[1].call(initData[0], _key);
					else initData = initData(_key);
					if(initData){
						if($(initData).hasOwnProperty('key')){
							if($.isArray(initData)){/*返回指定类型的数据*/
								var nodes = initData;
								if(nodes && nodes.length > 0){
									_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
									//_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
								}
							} else {/*返回所有的数据*/
								for(key in initData){
									var nodes = initData[key];
									if(nodes && nodes.length > 0){
										_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
										//_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
									}
								}
							}
						} else {
							var nodes = initData;
							if(!$.isArray(initData)) {
								nodes = new Array();
								nodes.push(initData);
							}
							if(nodes && nodes.length > 0){
								_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
								//_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
							}
						}
					}
			}
			//点击树节点时
			function zTreeOnClick(event, treeId, treeNode){
				var choose = new Array();
				$('#sel option').each(function(){
					choose.push($(this).val());	
				});
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/learning/paper!findCateQuesData.htm', 
					data : {"id":treeNode.id,"key":_key},
					async: true,
					dataType : 'json',
					success : function(data){
						$('#quescontent').html("");
						for(var i=0;i<data.length;i++){
							var name = typeof(data[i].name)=='undefined'?"":data[i].name;
							var content = $.trim(data[i].content);
							var subContent = '';
							if(content.length>20){
								subContent = (data[i].content.substring(0,16)+'····');
							}else{
								subContent = content;
							}
							var html = "";
							if($.inArray(data[i].id, choose)!=-1){
								html = '<span onclick="shwoinfo(this);" class="queschoose" title="'+content+'"><input type='+_chkStyle+' name="ques" checked="checked" id="'+data[i].id+'" onclick="ztreeOnCheck(\''+data[i].id+'\',\''+content+'\',\''+name+'\');" />'+subContent+'</span>';
							}else{
								html = '<span onclick="shwoinfo(this);" class="queschoose" title="'+content+'"><input type='+_chkStyle+' name="ques" id="'+data[i].id+'" onclick="ztreeOnCheck(\''+data[i].id+'\',\''+content+'\',\''+name+'\');" />'+subContent+'</span>';
							}  
							$('#quescontent').append(html);
						}
					},
					error : function(){
						layer.alert("网络错误，请稍后再试!", -1);
					}
				});
			};
			//勾选checkbox/radio数据时
			function ztreeOnCheck(id,content,name){
				var flag = $("#"+id).is(':checked');
				if (flag){
					if ($("#sel option[value='"+id+"']").length==0){
						if(_chkStyle=='radio'){
							$("#sel option").remove();
						}
						$("#sel").append('<option value="'+id+'">'+content+'</option>');
						$("#sel option[value='" + id + "']").data('content',name);
					}
				}else{
					if ($("#sel option[value='"+id+"']").length>0){
						$("#sel option[value='"+id+"']").remove();
					}
				}
				
			}
			
			function ztreeOnBeforeCheck(treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				return true;
			}
			
			function ztreeOnExpand(event, treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				if (treeNode.checked){
					var subNodes = ztreeObj.getNodesByParam("objtype", "KBVAL", treeNode);
					$(subNodes).each(function(i, item){
						ztreeObj.checkNode(item, treeNode.checked, true, true);
					});
				}
			}
			
			function ztreeOnBeforeExpand(treeId, treeNode){
				treeNode.nocheck = true;
			}
			
			$(function(){
				var ztreeObj = $.fn.zTree.init($("#treeDemo"), setting);
				//移除选中的值
				$("#btnMoveout").click(function(){
					var id = $('#sel option:selected').val();
					if (id!=undefined && id.length>0){
						if($("#"+id).is(':checked')){
							$("#"+id).attr("checked", false);
						}
					}
					$('#sel option:selected').remove();
				});
				//双击取消选中的select
				$("#sel").dblclick(function(){
					$("#btnMoveout").click();
				});
				
				//确定
				$("#btnOk").click(function(){
					callback = TreeUtils.fn.getObjByName('${param.callback}');
					var data = new Array();
					if ($("#sel option").length>0){
						$("#sel option").each(function(i, item){
							data.push({
								id: $(item).val(), 
								name: $(item).text(),
								content: $(item).data('content')
							});
						});
						if(callback.length > 1) /*回掉*/
							callback[1].call(callback[0], _key, data);
						else callback(_key, data);
					}
					$("#btnClose").click();
				});
				//取消
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="55%">
					<div style="width: 30%;float: left;" class="ztree" id="treeDemo"></div>
					<div class="treediv" style="width: 67%;float: left;" id="quescontent"></div>
				</td>
				<td width="6%">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="39%">
					<select id="sel" size="28" style="width:100%;height:398px;border:1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<button class="button button-primary button-rounded button-small" id="btnOk">确定</button>
					<button class="button button-primary button-rounded button-small" id="btnClose">取消</button>
				</td>
			</tr>
		</table>
	</body>
</html>
