<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<style>
	.ul_PaperCate{width: 100%;}
	.ul_PaperCate>ul{width: 100%;}
	.ul_PaperCate>ul>li{width: 100%;text-align: left;margin-left: 5px;margin-top: 5px;}
	.box th{width: 30%;text-align: right;}
	.box td{width: 70%;}
	.box td>input{width: 97%;}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PaperCate2.js"></script>
<script type="text/javascript">
	/*保存分类*/
	TreeUtils.setting({
		treeId: 'paperCateTree',/*树id*/
		rootNodeId: '${rootNodeId}',/*树根节点id*/
		treeURL: $.fn.getRootPath() + '/app/learning/paper-cate!initTree.htm',/*树数据初始化请求地址*/
 		btns: [/*按钮*/
 			{id: 'searchBtn', keywords:'keywords', title: '检索', fn: 'search'}
 		]
	});
</script>
<div class="ul_PaperCate">
	<ul>
		<li>
			<input type="text" id="keywords" style="border: 1px solid #cccccc;height: 20px;">
			<img id="searchBtn" title="搜分类名称"
				src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png"
				style="cursor: pointer;margin-top: 2px;margin-left: 2px;" />
		</li>
		<li>
			<ul id="paperCateTree" class="ztree" style="width:170px; overflow: auto;"></ul>
		</li>
	</ul>
</div>
