<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<style>
	.ul_PaperCate{width: 100%;}
	.ul_PaperCate>ul{width: 100%;}
	.ul_PaperCate>ul>li{text-align: left;margin-left: 5px;margin-top: 5px;}
	.box th{width: 30%;text-align: right;}
	.box td{width: 70%;}
	.box td>input{width: 97%;}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PaperCate2.js"></script>
<script type="text/javascript">
	/*保存分类*/
	TreeUtils.setting({
		treeId: 'paperCateTree',/*树id*/
		rootNodeId: '${rootNodeId}',/*树根节点id*/
		treeURL: $.fn.getRootPath() + '/app/learning/paper-cate!initTree.htm',/*树数据初始化请求地址*/
		dragURL: $.fn.getRootPath() + '/app/learning/paper-cate!drag.htm',/*树节点拖动请求地址*/
		shadeId: 'addOrUpdateWin',/*弹窗id*/
		beforeOpenShade: function(flag, node, treeObj){/*打开弹窗前操作*/
			$('#err_msg').text('');
			var id = '', name = '', pId = '', pName = '', remark = '';
			if(node){
				if(flag == 'edit'){
					id = node.id, name = node.name, remark = node.remark,
					pId = node.pId, pName = node.pName;
				} else if(flag == 'add'){
					pId = node.id, pName = node.name;
				}
			}
			$('#cid').val(id),
			$('#cname').val(name), $('#cname2').val(name),
			$('#cpId').val(pId),
			$('#cpName').text(pName),
			$('#cremark').val(remark);
			return true;
		},
 		btns: [/*按钮*/
 			{id: 'searchBtn', keywords:'keywords', title: '检索', fn: 'search'},
 			{id: 'delBtn' , title: '删除', url: '/app/learning/paper-cate!delete.htm', fn: 'drop'},
 			{id: 'updateBtn', title: '编辑', fn: 'edit', params:{height: 200}},
 			{id: 'addBtn' , title: '新增', fn: 'add', params:{height: 200}}
 		]
	});
	
	/*保存分类*/
	function paperCateSave(){
		var treeObj = TreeUtils.getZTreeObj();
		if(treeObj){
			var id = $('#cid').val(),
			name = $('#cname').val().replace(/(^\s*)|(\s*$)/g,''),/*去掉首尾空格*/
			name2 = $('#cname2').val(),
			pId = $('#cpId').val(),
			remark = $('#cremark').val();
			
			/*分类名称不应为空*/
			if(name == '') {
				//$('#err_msg').text('分类名称不应为空!');
				layer.alert('分类名称不应为空!', -1);
				return false;
			}
			/*节点名称发生改变，验证是否同级下重名*/
			var targetNode = treeObj.getNodeByParam("id", pId, null);/*目标节点*/
			var node1 = treeObj.getNodesByFilter(function(nd){
				if(nd.pId == pId && nd.name == name) return true;
				return false;
			}, true, targetNode);
			/*同名岗位与当前需要编辑的岗位不是同一个*/
			if(node1 && node1.id != id){
				//$('#err_msg').text('同级分类下不应存在相同名称的分类!');
				layer.alert('同级分类下不应存在相同名称的分类!', -1);
				return false;
			}
			/*发送请求，修改数据*/
			var params = {'id': id, 'name': name, 'remark': remark, 'pId': pId};
			var loadindex = layer.load('提交中…');
			$.post($.fn.getRootPath() + '/app/learning/paper-cate!addOrEdit.htm', params, 
				function(jsonResult) {
					layer.close(loadindex);
					layer.alert(jsonResult.msg, -1, function(){
						if (jsonResult.rst) {
							var node = jsonResult.node;
							if(id){/*更新节点*/
								var node_ = treeObj.getNodeByParam("id", id, null);
								node_.name = node.name;
								node_.remark = node.remark;
								node_.icon = node.icon;
								treeObj.updateNode(node_);
							} else {/*新增节点*/
								treeObj.addNodes(targetNode, node);
							}
						}
						layer.closeAll();
					});
			}, "json");
		}
	}
	
	/*初始化*/
	$(function(){
		/*渲染搜索框宽度*/
		var w = $('#keywords').parent().width();
		$('#keywords').css({width: (w-30)+'px'});
		/*渲染树高度*/
		$('#paperCateTree').css('height', $(window).height()-60);
	});
</script>
<div class="ul_PaperCate" style="background-color: #f6f6f6;border-right: 2px solid #dfdfdf;border-bottom: 1px solid #dfdfdf;margin-left: -2px;">
	<ul style="margin-top: -5px;">
		<li style="height: 2px;"></li>
		<li>
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="addBtn" >添加</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="updateBtn" >修改</a>  
			<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="delBtn" >删除</a>  
		</li>
		<li>
			<input type="text" id="keywords" style="border: 1px solid #cccccc;height: 20px;">
			<img id="searchBtn" title="搜分类名称"
				src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png"
				style="cursor: pointer;margin-top: 2px;margin-left: 2px;" />
		</li>
		<li>
			<ul id="paperCateTree" class="ztree" style="width:150px; overflow: auto;"></ul>
		</li>
	</ul>
</div>
<div id="addOrUpdateWin" style="display: none;">
	<table cellspacing="0" class="box" style="width: 300px;">
		<tr>
			<th>
				父节点:
			</th>
			<td>
				<span id="cpName"></span>
				<input type="hidden" id="cpId"/>
			</td>
		</tr>
		<tr>
			<th>
				<span style="color: red;">*</span>当前节点:
			</th>
			<td>
				<input type="text" id="cname" style="width: 97%;"/>
				<input type="hidden" id="cid"/>
				<input type="hidden" id="cname2"/><%--用来验证是否重名 --%>
			</td>
		</tr>
		<tr>
			<th>
				备注:
			</th>
			<td>
				<textarea id="cremark" rows="1" cols="1" style="width: 95%;"></textarea>
			</td>
		</tr>
	</table>
	<br>
	<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
		<!-- <span id="err_msg" style="color: red;float: left; margin-left: 5px;margin-top:5px;"></span> -->
		<input type="button" onclick="javascript:paperCateSave();" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
	</div>
</div>