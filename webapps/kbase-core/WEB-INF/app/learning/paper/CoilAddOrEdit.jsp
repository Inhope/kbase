<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考卷管理-编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;}
			.box th{width: 20%;}
			.box td{width: 30%;}
			.box td>input[type=text]{width: 97%;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript">
			CoilAddOrEdit = {
				fn: {
					ajax: function(opts){
						var url = opts.url,
							params = opts.params,
							callback = opts.callback,
							dataType = opts.dataType;
						if(!dataType) dataType = 'json';
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + url,
							data : params,
							async: false,
							dataType : dataType,
							success : function(jsonResult){
								callback(opts, jsonResult);
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				},
				/*选择器*/
				choose: {
					/*回显数据方法名*/
					initData: function(){
						var data = new Array(), 
							paperId = $('#paperId').val(), /*试卷id*/
							paperName = $('#paperName').val(), /*试卷名称*/
							pname = $('#pname').text(),/*分类名称*/
							plevel = $('#plevel').text(),/*难易程度*/
							pscore = $('#pscore').text(),/*试卷分数*/
							pscorepass = $('#pscorepass').text();/*合格分数*/
						if(paperId && paperName)
							data.push({'id': paperId, 'name': paperName, 
								params:{'pname': pname, 'plevel':plevel, 'pscore':pscore, 'pscorepass':pscorepass}});
						return data;
					},
					/*弹窗回掉方法名*/
					callback: function(data){
						var _that = this;
						if(data){
							data = data[0];
							var paperId = data.id, /*试卷id*/
							paperName = data.name, /*试卷名称*/
							params = eval("(" + data.params + ")"),/*相关数据*/
							pname = params.pname,/*分类名称*/
							plevel = params.plevel,/*难易程度*/
							pscore = params.pscore,/*试卷分数*/
							pscorepass = params.pscorepass;/*合格分数*/
							if(paperId && paperName){
								$('#paperId').val(paperId);
								$('#paperName').val(paperName);
								$('#pname').text(pname);
								$('#plevel').text(plevel);
								$('#pscore').text(pscore);
								$('#pscorepass').text(pscorepass);
							}
						}
					},
					/*初始化*/
					init: function(){
						/*关联试卷绑定事件*/
						var h = $("body").height(), w = $("body").width();
						$('#paperName').click(function(){
							$.kbase.picker.paperByCate({
								initData: 'CoilAddOrEdit.choose.initData',/*回显数据方法名*/
								callback: 'CoilAddOrEdit.choose.callback',/*弹窗回掉方法名*/
								diaWidth:w*0.9, diaHeight:h*0.9
							});
						});
					}
				},
				
				/*表单*/
				from: {
					/*表单内容验证*/
					check: function(){
						$(form_coil).validate({
							rules : {
								"coil.name" : {
									required : [ "考卷名称" ]
								},
								"paperName" : {
									required : [ "关联试卷" ]
								}
							},
							// 验证通过时的处理
							success : function() {
								
							},
							errorPlacement : function(error, element) {
								error.appendTo(element.parent());
							},
							focusInvalid : false,
							onkeyup : false
						});
						return $(form_coil).validate().form();
					},
					/*试卷验证*/
					paperCheck: function(callback){
						var _that = this, flag = false,
							fn = CoilAddOrEdit.fn;
						/*发送请求*/
						fn.ajax({
							url: '/app/learning/coil!beforeCheck.htm',
							params: {'paperId': $('#paperId').val()},
							dataType: 'html',
							callback: callback
						});
					},
					/*同名验证*/
					checkCoilIsExsit: function (){
						var _that = this, flag = false,
							fn = CoilAddOrEdit.fn, 
							opts = {
								url: '/app/learning/coil!checkCoilIsExsit.htm',
								params: {'name':$('#name').val(), 'name2':$('#name2').val()},
								callback: function(opts, jsonResult){
									if(jsonResult.rst){
										if(jsonResult.data == 0) flag = true;
										else layer.alert('此考卷名称已被使用，请换一个！', -1)
									}
								}
							};
						/*发送请求*/
						fn.ajax(opts);
						return flag;
					},
					submit: function(){
						var _that = this;
						/*表单内容验证*/
						if(!_that.check()) return false;
						/*同名验证*/
						if(!_that.checkCoilIsExsit()) return false;
						/*提交*/
						var load_index = layer.load('提交中…');
						var params = $("#form_coil").serialize();
						$.post($.fn.getRootPath() + "/app/learning/coil!addOrEditDo.htm", params,
							function(jsonResult){
								layer.close(load_index);
								load_index = layer.alert(jsonResult.msg, -1, function(){
									layer.close(load_index);
									if(jsonResult.rst){/*操作成功*/
										parent.pageClick();/*刷新当前页面*/
										parent.layer.closeAll();/*关闭弹窗*/
									}
								});
						}, 'json');
					},
					init: function(){
						var _that = this;
						/*绑定初始化事件*/
						$('#saveBtn').click(function(){
							_that.submit();
						});
					}
				},
				/*初始化*/
				init: function(){
					var _that = this; 
					_that.from.init();
					_that.choose.init();
				}
			}
			
			/*初始化*/
			$(function(){
				/*初始化*/
				CoilAddOrEdit.init();
			});
			
		</script>
	</head>
	<body>
		<form id="form_coil" action="" method="post">
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th>
							<span style="color: red;">*</span>考卷名称：
						</th>
						<td>
							<input type="text" id="name" name="coil.name" value="${coil.name }" style="width: 97%;"/>
							<input type="hidden" name="coil.id" value="${coil.id }" />
							<input type="hidden" id="name2" name="name2" value="${coil.name }"/>
						</td>
						<th>
							<span style="color: red;">*</span>关联试卷：
						</th>
						<td>
							<input type="text" id="paperName" name="paperName" value="${coil.paper.name }" style="width: 97%;"/>
							<input type="hidden" id="paperId" name="coil.paper.id" value="${coil.paper.id }" />
						</td>
					</tr>
					<tr>
						<th>
							考卷分类：
						</th>
						<td>
							<span id="pname">${coil.paper.paperCate.name }</span>
						</td>
						<th>
							考卷难易：
						</th>
						<td>
							<span id="plevel">
								<s:iterator value="#request.paperLevel" var="pLevel">
									<s:if test="#request.coil.paper.level == #pLevel.level">${pLevel.name }</s:if>
								</s:iterator>
							</span>
						</td>
					</tr>
					<tr>
						<th>
							考卷总分：
						</th>
						<td>
							<span id="pscore">${coil.paper.score }</span>
						</td>
						<th>
							合格分数：
						</th>
						<td>
							<span id="pscorepass">${coil.paper.scorePass }</span>
						</td>
					</tr>
					<tr>
						<th>
							<span style="color: red;">*</span>是否显示分数：
						</th>
						<td colspan="3">
							否<input type="radio" name="coil.showScore" value="0" 
								<s:if test="#request.coil == null || !#request.coil.showScore">checked="checked"</s:if>>
							是<input type="radio" name="coil.showScore" value="1" 
								<s:if test="#request.coil.showScore">checked="checked"</s:if>>
						</td>
					</tr>
					<tr>
						<th>
							有效期(开始)：
						</th>
						<td>
							<input name="coil.startTime" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 00:00:00' })" readonly="readonly" class="Wdate"
								value='<s:date  name="#request.coil.startTime" format="yyyy-MM-dd HH:mm:ss"/>' 
								type="text"  />
						</td>
						<th>
							有效期(结束)：
						</th>
						<td>
							<input name="coil.endTime" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 23:59:59' })" readonly="readonly" class="Wdate"
								value='<s:date  name="#request.coil.endTime" format="yyyy-MM-dd HH:mm:ss"/>' 
								type="text"  />
						</td>
					</tr>
					<tr>
						<th>
							备注：
						</th>
						<td colspan="3">
							<textarea name="coil.remark" rows="1" cols="60">${coil.remark }</textarea>
						</td>
					</tr>
				</table>
				<br>
				<span id="err_msg"></span>
				<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="saveBtn" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
