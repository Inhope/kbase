<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!-- 单选题 -->
<s:set var="key" value="0 + ''"></s:set>
<s:set var="data" value="#request.quesMap[#key]"></s:set>
<s:if test="#data != null && #data.size() > 0">
	<div class="eaxm_title">
		${title_var[ind_title_var] }、${quesTypes[key] }
		<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
	</div>
	<div class="exam_con">
		<div class="usbox">
			<s:iterator value="#data" var="va" status="st">
				<!--正确答案  -->
				<s:set var="isRight" value="''"></s:set>
				<s:iterator value="#va.quesResults" var="va1" status="st1">
					<s:if test="#va1.isRight == 1">
						<s:if test="#isRight == null ||  #isRight == ''">
							<s:set var="isRight" value="#select_var[#st1.index]"></s:set>
						</s:if>
						<s:else>
							<s:set var="isRight" value="#isRight + ', ' + #select_var[#st1.index]"></s:set>
						</s:else>
					</s:if>
				</s:iterator>
				<!--遍历答案选项  -->
				<div class="exam_a">
					<span>${st.index + 1 }.${va.content }&nbsp; 
						<span class="incorrect">正确答案：${isRight}</span>
					</span>
					<ul>
						<s:iterator value="#va.quesResults" var="va1" status="st1">
							<li>
								<a href="javascript:void(0);">
									<s:if test="#va1.isRight == 1">
										<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg" alt="" />&nbsp;
									</s:if>
									<s:else>
										<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />&nbsp;
									</s:else>
									${select_var[st1.index] }.${va1.content }
								</a>
							</li>
						</s:iterator>
					</ul>
				</div>
			</s:iterator>
		</div>
	</div>
</s:if>
