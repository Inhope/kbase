<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!-- 判断题 -->
<s:set var="key" value="2 + ''"></s:set>
<s:set var="data" value="#request.quesMap[#key]"></s:set>
<s:if test="#data != null && #data.size() > 0">
	<div class="eaxm_title dx">
		${title_var[ind_title_var] }、${quesTypes[key] }
		<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
	</div>
	<div class="exam_con">
		<div class="usbox">
			<s:iterator value="#data" var="va" status="st">
				<!--遍历答案选项  -->
				<div class="exam_b">
					<span>${st.index + 1 }.${va.content }</span>
					<ul>
						<li>
							<a href="javascript:void(0);">
								<s:if test="#va.isRight == 1">
									<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg" alt="" />
								</s:if>
								<s:else>
									<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />
								</s:else>
								&nbsp;对
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<s:if test="#va.isRight == 0">
									<img src="${pageContext.request.contextPath}/resource/learning/images/ks_03.jpg" alt="" />
								</s:if>
								<s:else>
									<img src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg" alt="" />
								</s:else>
								&nbsp;错
							</a>
						</li>
					</ul>
				</div>
			</s:iterator>
			<div style="clear: both;"></div>
		</div>
	</div>
</s:if>
