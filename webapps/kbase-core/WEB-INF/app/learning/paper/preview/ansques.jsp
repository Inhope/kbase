<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!-- 简单题 -->
<s:set var="key" value="4 + ''"></s:set>
<s:set var="data" value="#request.quesMap[#key]"></s:set>
<s:if test="#data != null && #data.size() > 0">
	<div class="eaxm_title dx">
		${title_var[ind_title_var] }、${quesTypes[key] }
		<s:set var="ind_title_var" value="#ind_title_var + 1"></s:set>
	</div>
	<div class="exam_con">
		<div class="usbox">
			<s:iterator value="#data" var="va" status="st">
				<!--遍历答案选项  -->
				<div class="exam_a">
					<span>${st.index + 1 }.${va.content }</span>
					<div class="exam_ca">
						<div class="usbox">
							<textarea rows="10" cols="100%" class="text-exam"></textarea>
						</div>
					</div>
				</div>
			</s:iterator>
			<div style="clear: both;"></div>
		</div>
	</div>
</s:if>
