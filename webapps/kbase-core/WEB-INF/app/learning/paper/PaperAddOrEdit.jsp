<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.box th{width: 20%;}
			.box td{width: 30%;}
			.box td>input[type=text]{width: 97%;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript">
			var PaperAddOrEdit = {
				fn: {
					save: function(){
						$(form_paper).validate({
							rules : {
								"paper.name" : {
									required : [ "试卷名称" ]
								},
								"paper.paperCate.name" : {
									required : [ "试卷分类" ]
								},
								"paper.level" : {
									required : [ "试卷难易" ]
								},
								"paper.score" : {
									required : [ "试卷总分" ],
									digits : [ "试卷总分" ],
									number:true,
									min : [ 1 ]
								},
								"paper.scorePass" : {
									required : [ "合格分数" ],
									digits : [ "合格分数" ],
									number:true,
									min : [ 1 ]
								},
								"paper.showScore" : {
									required : [ "是否显示分数" ]
								}
							},
							// 验证通过时的处理
							success : function() {
								
							},
							errorPlacement : function(error, element) {
								error.appendTo(element.parent());
							},
							focusInvalid : false,
							onkeyup : false
						});
						
						var flag =false;
						if(parseInt($('#paper_score').val())-parseInt($('#paper_scorepass').val())<0){
							parent.layer.alert("合格分数不能大于总分",-1);
							return false;
						}else{
							if($(form_paper).validate().form()){
								flag = true;
							}
						}
						if(flag){
							$.getJSON($.fn.getRootPath() + "/app/learning/paper!checkPaperIsExsit.htm",
		       	    			{"name":$("#name").val(), 'name2':$("#name2").val()},
		       	    			function(jsonResult){
		       	    				flag = false;/*默认不提交*/
									if(jsonResult.rst){
										if(jsonResult.data == 0) flag = true;
										else $("#err_msg").html("<font color='red'>此试卷名称已被使用，请换一个！</font>");
									}else{
										$("#err_msg").html("");
									}
									
									if(flag){
										var load_index = layer.load('提交中…');
										var type =$("input[name='paper.type']:checked").val();
										var title = "";
										var url ='';
										
										//测试负载服务地址
										var params = $("#form_paper").serialize();
										$.post($.fn.getRootPath() + "/app/learning/paper!addOrEditDo.htm", params,
											function(jsonResult){
												layer.close(load_index);
												if(jsonResult.rst){/*操作成功*/
													if(type=='0'){
														title="手工";
														url=$.fn.getRootPath()+"/app/learning/paper!relatedQues.htm?paperId="+jsonResult.paper_id+"&type="+type
													}else{
														title="随机";
														url=$.fn.getRootPath()+"/app/learning/paper!randomQues.htm?paperId="+jsonResult.paper_id+"&type="+type
													}
													parent.__kbs_layer_index_two = parent.$.layer({
														type: 2,
														border: [2, 0.3, '#000'],
														title: [title, 'font-size:14px;font-weight:bold;'],
														closeBtn: [0, true],
														iframe: {src : url},
														area: ['1000px', '100%'],
														end :function(){
															parent.layer.close(parent.__kbs_layer_index_one);
														}
													});
												}else{
													parent.layer.alert("操作失败",-1);
												}
										}, 'json');
									}
								});
			        	 }
					},
					/*分类选择事件*/
					choosePaperCate: function(){
						var h = $('body').height(), 
							w = $('body').width()
						$.kbase.picker.paperCate({
							returnField:"pcname|pcid", 
							diaWidth:w*0.5, diaHeight:h*0.9,
							chkStyle: 'radio'/*单选（默认复选）*/
						});
					}
				},
				
				init: function(){
					var _that = this; 
					/*分类选择绑定事件*/
					$('#pcname').click(function(){
						_that.fn.choosePaperCate();
					});
					
					$('#paperSave').click(function(){
						_that.fn.save();
					});
				}
			}
			
			/*初始化*/
			$(function(){
				PaperAddOrEdit.init();
			});
			
		</script>
	</head>
	<body>
		<form id="form_paper" action="" method="post">
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th>
							<span style="color: red;">*</span>试卷名称：
						</th>
						<td>
							<input type="text" id="name" name="paper.name" value="${paper.name }" style="width: 97%;"/>
							<input type="hidden" name="paper.id" value="${paper.id }" />
							<input type="hidden" id="name2" name="name2" value="${paper.name }"/>
						</td>
						<th>
							<span style="color: red;">*</span>试卷分类：
						</th>
						<td>
							<s:if test="#request.paper.paperCate.delFlag==0">
								<input type="text" id="pcname" name="paper.paperCate.name" value="${paper.paperCate.name }" readonly="readonly" />
								<input type="hidden" id="pcid" name="paper.paperCate.id" value="${paper.paperCate.id }" />
							</s:if>
							<s:else>
								<input type="text" id="pcname" name="paper.paperCate.name" value="" readonly="readonly" />
								<input type="hidden" id="pcid" name="paper.paperCate.id" value="" />
							</s:else>
						</td>
					</tr>
					<tr>
						<th>
							<span style="color: red;">*</span>试卷总分：
						</th>
						<td>
							<input type="text" name="paper.score" id="paper_score" value="${paper.score }" />
						</td>
						<th>
							<span style="color: red;">*</span>合格分数：
						</th>
						<td>
							<input type="text" name="paper.scorePass" id="paper_scorepass" value="${paper.scorePass }" />
						</td>
					</tr>
					<tr>
						<th>
							<span style="color: red;">*</span>试卷难易：
						</th>
						<td>
							<s:select name="paper.level" value="#request.paper.level" listKey="level" listValue="name" list="#request.paperLevel"></s:select>
						</td>
						<th>
							试卷类型：
						</th>
						<td colspan="3">
							<s:if test="#request.paper == null">
								<%--新增 --%>
								手工<input type="radio" name="paper.type" value="0" 
									<s:if test="#request.paper == null || #request.paper.type != 1">checked="checked"</s:if>>
								随机<input type="radio" name="paper.type" value="1" 
									<s:if test="#request.paper.type == 1">checked="checked"</s:if>>
							</s:if>
							<s:else>
								<%--编辑 --%>
								<input type="radio" name="paper.type" value="0" <s:if test="#request.paper.type == 0">checked="checked"</s:if> disabled="disabled"/>手工
								<input type="radio" name="paper.type" value="1" <s:if test="#request.paper.type == 1">checked="checked"</s:if> disabled="disabled"/>随机
							</s:else>
						</td>
					</tr>
					
					<tr>
						<th>
							有效期(开始)：
						</th>
						<td>
							<input name="paper.startTime" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 00:00:00' })" readonly="readonly" class="Wdate"
								value='<s:date  name="#request.paper.startTime" format="yyyy-MM-dd HH:mm:ss"/>' 
								type="text"  />
						</td>
						<th>
							有效期(结束)：
						</th>
						<td>
							<input name="paper.endTime" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 23:59:59' })" readonly="readonly" class="Wdate"
								value='<s:date  name="#request.paper.endTime" format="yyyy-MM-dd HH:mm:ss"/>' 
								type="text"  />
						</td>
					</tr>
					<tr>
						<th>
							备注：
						</th>
						<td colspan="3">
							<textarea name="paper.remark" rows="1" cols="60">${paper.remark }</textarea>
						</td>
					</tr>
				</table>
				<br>
				<span id="err_msg"></span>
				<!-- 
				<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="paperSave" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
				 -->
				<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
					<a href="javascript:void(0);"  id="paperSave">下一步</a>
				</div>
			</div>
		</form>
	</body>
</html>
