<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-手工题目</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.box th{width: 20%;}
			.box td{width: 30%;}
			.box td>input{width: 97%;}
			
			.choose_ques{}
			.choose_ques table tr{height: 24px;}
			.choose_ques input[type='text']{width: 30px; text-align: right;}
			.choose_ques td, .choose_ques th{text-align: center;}
			
			.left_title{width: 30%; float: left;}
			.right_content{width: 70%; float: left; overflow-y: auto;}
			
			.btns{}
			.btns>input{height: 21px; line-height:12px; font-size: 12px;}
			.btns>span{float: left;}
			.btns>input[type='button']{float: right; width: 60px; vertical-align:middle;}
			
			.left_title table tr:not(:first-child){cursor:pointer }
			.right_content table tr:not(:first-child)>td:nth-child(1){width: 10%;}
			.right_content table tr:not(:first-child)>td:nth-child(2){width: 70%;}
			.right_content table tr:not(:first-child)>td:nth-child(3){width: 20%;}
			
			.sel{background-color: orange;}
			
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PaperRelationQues.js"></script>
		<script type="text/javascript">
			/*试卷id, 试卷总分*/
			var _paperId = '${paper.id }', 
				_Score = '${paper.score }';
		</script>
	</head>
	
	<body>
		<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th>
						试卷名称：
					</th>
					<td>
						${paper.name }
					</td>
					<th>
						试卷状态：
					</th>
					<td>
						<s:iterator value="#request.paperStatus" var="va">
							<s:if test="#request.paper.status == #va.status">${va.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				<tr>
					<th>
						试卷总分：
					</th>
					<td>
						${paper.score }
					</td>
					<th>
						试卷难易：
					</th>
					<td>
						<s:iterator value="#request.paperLevel" var="pLevel">
							<s:if test="#request.paper.level == #pLevel.level">${pLevel.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				<tr>
					<td colspan="4"  class="choose_ques">
						<!-- 左侧 -->
						<div class="left_title">
							<table cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<th>题型</th>
									<th>题目数</th>
									<th>总分</th>
								</tr>
								<s:set var="amount" value="0"></s:set>
								<s:set var="score" value="0"></s:set>
								<s:iterator value="#request.paper.quesTypes" var="va">
									<tr key="${va.key }">
										<td id="${va.key }_name">${va.name }</td>
										<td id="${va.key }_amount">${va.amount }</td>
										<td id="${va.key }_score">${va.score }</td>
										<s:set var="amount" value="#amount + #va.amount"></s:set>
										<s:set var="score" value="#score + #va.score"></s:set>
									</tr>
								</s:iterator>
								<tr style="cursor: text;">
									<th>总计</th>
									<th id="t_amount">${amount }</th>
									<th id="t_score">${score }</th>
								</tr>
							</table>
						</div>
						<!-- 右侧 -->
						<s:iterator value="#request.paper.quesTypes" var="va" status="st">
							<div class="right_content" style="display: none;">
								<table key="${va.key }" cellspacing="0" class="box" style="width: 100%;">
									<tr>
										<td colspan="3" width="85%" class="btns">
											<!-- 
											<span>分值&nbsp;&nbsp;<input type="text" name="defScore" value="0">
												<span style="color: red;">(${va.name }默认分值)</span></span>
											 -->	
											<input type="button" name="downBtn" value="下移">
											<input type="button" name="upBtn" value="上移">
											<input type="button" name="delBtn" value="删除题目">
											<input type="button" name="addBtn" value="添加题目">
											<div style="float:right">
												<span style="color: red;">${va.name }默认分值</span><input type="text" name="defScore" value="0">
											</div>
											<s:if test="#request.paper.type==0">
												<input type="checkbox" name="isRandom" value="0" 
													onchange="javascript:changeval(this);"
													${paper.isRandom==1?"checked":"" }
													style="float: right;vertical-align: middle;width: 20px;margin-right: 10px;">
												<font style="float: right;margin-top: 2px;">随机排序</font>
											</s:if>
										</td>
									</tr>
									<s:iterator value="#va.list_Ques" var="va1">
										<tr>
											<td style="text-align: center;">
												<input type="checkbox" name="${va.key }_quesId" value="${va1.ques.id }" />
											</td>
											<td>
												${va1.ques.content }
											</td>
											<td><span>分值<input type="text" name="${va.key }_score" value="${va1.score }" ></span></td>
										</tr>
									</s:iterator>
								</table>
							</div>
						</s:iterator>
					</td>
				</tr>
			</table>
			<br>
			<span id="err_msg"></span>
			<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
				<input type="button" id="saveRelationQues" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
			</div>
		</div>
	</body>
</html>