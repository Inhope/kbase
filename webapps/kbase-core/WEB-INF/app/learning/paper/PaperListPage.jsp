<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<s:iterator value="page.result" var="va">
		<tr>
			<td>
				<input name="paperId_list" type='checkbox' value="${va.id }" 
					score="${va.score }" scoreReal="${va.scoreReal }" />
			</td>
			<td>
				<a href="javascript:void(0);" style="color: blue;" 
					name="showDetail"  params="{paperId: '${va.id }', ids: '${va.id }'}">${va.name }</a>
				(<s:if test="#va.valid">有效</s:if><s:else>无效</s:else>)
			</td>
			<td>
				<s:if test="#va.paperCate.delFlag==0">
					${va.paperCate.name }
				</s:if>&nbsp;
			</td>
			<td>
				<s:date  name="#va.startTime" format="yyyy-MM-dd HH:mm"/>&nbsp;
			</td>
			<td>
				<s:date  name="#va.endTime" format="yyyy-MM-dd HH:mm"/>&nbsp;
			</td>
			<td>
				${va.score }
			</td>
			<td>
				<s:iterator value="#request.paperLevel" var="pLevel">
					<s:if test="#va.level == #pLevel.level">${pLevel.name }</s:if>
				</s:iterator>
				&nbsp;
			</td>
			<td>
				<s:iterator value="#request.paperStatus" var="pStatus">
					<s:if test="#va.status == #pStatus.status">${pStatus.name }</s:if>
				</s:iterator>
			</td>
			<td>
				<s:if test="#va.type == 0">
					<a href="javascript:void(0);" style="color: blue;" 
						name="relatedQues" params="{paperId: '${va.id }', type: '${va.type }'}">手工</a>
				</s:if>
				<s:elseif test="#va.type == 1">
					<a href="javascript:void(0);" style="color: blue;" 
						name="randomQues" params="{paperId: '${va.id }', type: '${va.type }'}">随机</a>
				</s:elseif>
				<s:if test="#va.valid">
					<a href="javascript:void(0);" style="color: blue;" 
						name="checkPaper" params="{paperId: '${va.id }', type: '${va.type }'}">验证</a>
				</s:if>
			</td>
		</tr>
	</s:iterator>
	<tr class="trfen">
		<td colspan="10">
			<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
		</td>
	</tr>
</table>
