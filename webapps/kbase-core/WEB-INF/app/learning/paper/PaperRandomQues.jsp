<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-随机题目</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.box>tr>th{width: 20%;}
			.box>tr>td{width: 30%;}
			.box>tr>td>input{width: 97%;}
			
			
			/*试卷管理-随机规则*/
			.set_title, .sum_title, .req_title, .rule_title{background-color: #d5d5d5;}
			.set_con, .sum_con, .req_con, .rule_con{}
			/*总体（标题）*/
			.set_title>span, .sum_title>span, .req_title>span, .rule_title>span{float: left;font-size: 14px;font-weight:bold;margin-left: 5px;}
			.set_title>input[type='button'], .req_title>input[type='button'], .rule_title>input[type='button']
				{height: 21px; line-height:12px; font-size: 12px; float: right; width: 60px; vertical-align:middle;}
			/*总体（内容）*/
			.set_con table tr, .sum_con table tr, .req_con table tr, .rule_con table tr{height: 24px;}
			.set_con td, .sum_con td, .req_con td, .rule_con td{text-align: center;}
			.set_con .left_title table tr:first-child>td, .set_con .right_con table tr:first-child>td, 
				.sum_con table tr:first-child>td, .req_con table tr:first-child>td, .rule_con table tr:first-child>td{font-weight: bold;}
			.req_con input[type='text'], .rule_con input[type='text']{width: 99%; text-align: right;}
			.req_con select, .rule_con select{width: 99%;}
			
			/*题目选择（内容左侧）*/
			.set_con .left_title{width: 15%; float: left;}
			.set_con .left_title table tr:not(:first-child){cursor:pointer }
			/*题目选择（标题右侧）*/
			.set_con .right_con{width: 100%; float: left;}
			.set_con .right_con table tr>td:nth-child(1){width: 20%;}
			.set_con .right_con table tr>td:nth-child(2){width: 20%;}
			.set_con .right_con table tr>td:nth-child(3){width: 15%;}
			.set_con .right_con table tr>td:nth-child(4){width: 15%;}
			.set_con .right_con table tr>td:nth-child(5){width: 15%;}
			.set_con .right_con table tr>td:nth-child(6){width: 15%;}
			.set_con input[type='text']{width: 30px; text-align: right;}
			/*题目选择（标题右侧-选中效果）*/
			.sel{background-color: orange;}
			.err{background-color: red;}
			.err2{border: 1px solid red;}
			
			/*必选题*/
			.req_con table tr>td:nth-child(1){width: 15%;}
			.req_con table tr>td:nth-child(2){width: 15%;}
			.req_con table tr>td:nth-child(3){width: 30%;}
			.req_con table tr>td:nth-child(4){width: 40%;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PaperRandomQues.js"></script>
		<script type="text/javascript">
			var _QuesTypes = ${quesTypes}, /*试卷管理-题目类型*/
				_Levels = ${levels}, /*试卷管理-题目难易*/
				_PaperScore = ${paper.score },
				_PaperId = '${paper.id }';
		</script>
	</head>
	<body>
		<div class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th>
						试卷名称：
					</th>
					<td>
						${paper.name }
					</td>
					<th>
						试卷状态：
					</th>
					<td>
						<s:iterator value="#request.paperStatus" var="va">
							<s:if test="#request.paper.status == #va.status">${va.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				<tr>
					<th>
						试卷总分：
					</th>
					<td>
						${paper.score }
					</td>
					<th>
						试卷难易：
					</th>
					<td>
						<s:iterator value="#request.paperLevel" var="pLevel">
							<s:if test="#request.paper.level == #pLevel.level">${pLevel.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				
				<!-- 题目设置（开始） -->
				<tr flag="prev">
					<td colspan="4" class="set_title">
						<span>题目选择</span>
						<input type="button" name="delBtn" value="删除">
						<input type="button" name="downBtn" value="下移">
						<input type="button" name="upBtn" value="上移">
					</td>
				</tr>
				<tr flag="prev">
					<td colspan="4" class="set_con">
						<!-- 左侧 -->
						<!-- 
						<div class="left_title">
							<table cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<td>题型</td>
								</tr>
								<s:iterator value="#request.quesTypes" var="va">
									<tr key="${va.key }">
										<td id="${va.key }_name">${va.value }</td>
									</tr>
								</s:iterator>
							</table>
						</div>
						 -->
						<!-- 右侧 -->
						<div class="right_con">
							<table flag="set" cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<td>
										<input type="checkbox" name="" />
									</td>
									<td>题型</td>
									<td>每题分数</td>
									<td>计划题数</td>
								</tr>
								<s:iterator value="#request.range" var="va">
									<tr>
										<td><input type="checkbox" name="ck" value="${va.key }"/></td>
										<td name="key_name">
											<s:property value="#request.quesTypes[#va.key]"/>
										</td>
										<td><input type="text" name="key_score" value="${va.value.score }" ></td>
										<td><input type="text" name="key_amount" value="${va.value.amount }" ></td>
									</tr>
								</s:iterator>
							</table>
						</div>
					</td>
				</tr>
				<!-- 题目设置（结束） -->
				<tr flag="next">
					<td colspan="4" height="10px"></td>
				</tr>
				<!-- 概述表（开始） -->
				<tr flag="next">
					<td colspan="4" class="sum_title">
						<span>出题计划</span>
					</td>
				</tr>
				<tr flag="next">
					<td colspan="4" class="sum_con">
						<table flag="sum" cellspacing="0" class="box_" style="width: 100%;">
							<tr>
								<td width="20%">题型</td>
								<td width="20%">每题分数</td>
								<td width="20%">计划题数</td>
								<td width="20%">实际题数</td>
								<td width="20%">总分</td>
							</tr>
							<s:iterator value="#request.paper.mapRange" var="va">
								<tr>
									<td>
										<s:property value="#request.quesTypes[#va.key]"/>
									</td>
									<td>${va.value.score }</td>
									<td>${va.value.amount }</td>
									<td></td>
									<td></td>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<!-- 概述表（结束） -->
				<tr flag="next">
					<td colspan="4" height="20px"></td>
				</tr>
				<!-- 必选题（开始） -->
				<tr flag="next">
					<td colspan="4" class="req_title">
						<span>必选题</span>
						<input type="button" name="delBtn" value="删除">
						<input type="button" name="addBtn" value="新建">
					</td>
				</tr>
				<tr flag="next">
					<td colspan="4" class="req_con">
						<table flag="req" cellspacing="0" class="box_" style="width: 100%;">
							<tr>
								<td><input type="checkbox" name="" /></td>
								<td>题目类型</td>
								<td>试题内容</td>
								<td>试题备注</td>
							</tr>
							<s:iterator value="#request.paper.validPaperQuess" var="va" status="st">
								<tr>
									<td><input type="checkbox" name="ck" value="${va.id }"/></td>
									<td>
										<s:property value="#request.quesTypes[#va.ques.quesType.key]"/>
										<input type="hidden" name="key" value="${va.ques.quesType.key }">
									</td>
									<td name="content">
										<textarea id="kd_${st.index }" readonly="readonly" style="display: none;">
											${va.ques.content }
										</textarea>
									</td>
									<td>
										${va.ques.name }
										<input type="hidden" name="quesName" value="${va.ques.name }">
										<input type="hidden" name="quesId" value="${va.ques.id }">
									</td>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<!-- 必选题（结束） -->
				<tr flag="next">
					<td colspan="4" height="20px"></td>
				</tr>
				<!-- 随机题规则（开始） -->
				<tr flag="next">
					<td colspan="4" class="rule_title">
						<span>随机题规则</span>
						<input type="button" name="delBtn" value="删除">
						<input type="button" name="addBtn" value="新建">
					</td>
				</tr>
				<tr flag="next">
					<td colspan="4" class="rule_con">
						<table flag="rule" cellspacing="0" class="box_" style="width: 100%;">
							<tr>
								<td>
									<input type="checkbox" name="" />
								</td>
								<td>题目类型</td>
								<td>题目分类</td>
								<td>题目数量</td>
								<s:iterator value="#request.paperLevel" var="pLevel">
									<td>${pLevel.name }</td>
								</s:iterator>
							</tr>
							<s:iterator value="#request.paper.paperRules" var="va">
								<tr>
									<td>
										<input type="checkbox" name="ck" value="${va.id }"/>
									</td>
									<td>
										<s:property value="#request.quesTypes[#va.key]"/>
										<input type="hidden" name="key" value="${va.key }">
									</td>
									<td>
										${va.quesCate.name }
										<input type="hidden" name="quesCateId" value="${va.quesCate.id }">
										<input type="hidden" name="quesCateName" value="${va.quesCate.name }">
									</td>
									<td>
										<input type="text" name="amount" value="${va.amount }">
									</td>
									<s:iterator value="#request.paperLevel" var="pLevel">
										<td>
											<s:set var="k" value="#pLevel.level + ''"></s:set>
											<s:if test="#va.mapContent[#k] != null">
												<input type="text" name="level${k }" value="${va.mapContent[k].amount }">
											</s:if>
											<s:else>
												<%-- 如果新增了新的难度va.mapContent[k]应该为空 --%>
												<input type="text" name="level${k }" value="0">
											</s:else>
										</td>
									</s:iterator>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<!-- 随机题规则（结束） -->
			</table>
		</div>
		<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
			<a href="javascript:void(0);" flag="next" id="saveBtn">保存</a>
			<a href="javascript:void(0);" flag="next" id="prevBtn">上一步</a>
			<a href="javascript:void(0);" flag="prev" id="nextBtn">下一步</a>
		</div>
	</body>
</html>