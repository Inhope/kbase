<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-预览</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/style.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">	
			
		</script>
	</head>
	<body>
		<div class="page_all">
			<div class="exam">
				<div class="exam_top" style="height: 60px;">
					<div class="usbox" style="margin-top: 10px;">
						<div class="examtop_left fl" style="width: 90%;">
							<ul>
								<li>
									考卷名称：
									<span>${paper.name}</span>
								</li>
							</ul>
							<ul>
								<li>
									<s:set var="amount" value="0"></s:set>
									<s:iterator value="#request.paper.mapRange" var="va">
										<s:set var="amount" value="#amount + #va.value.amount" ></s:set>
									</s:iterator>
									考题总数：
									<span><s:property value="#amount"/> </span>
								</li>
							</ul>
							<ul>
								<li style="margin-top: -8px;">
									考题总分：
									<span class="px_red">${paper.score}</span>
								</li>
							</ul>
							<ul>
								<li style="margin-top: -8px;">
									及格分数：
									<span class="px_red">${paper.scorePass}</span>
								</li>
							</ul>
						</div>
						<div class="examtop_right fr" style="width: 10%;">
							<%--
								<a href="javascript:void(0);" style="line-height: normal;height: auto;width: 80%;">导出</a>
							 --%>
		                </div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
								<div class="usbox">
									<!-- 标题名称  -->
									<s:set var="title_var" value="{'一','二','三','四','五','六','七','八','九','十'}"></s:set>
									<s:set var="ind_title_var" value="0"></s:set>
									<!-- 选项名称 -->
									<s:set var="select_var" value="{'A','B','C','D','E','F','F','G','H','I','J','Q','L','M','N'}"></s:set>
									
									<!-- 单选题 -->
									<jsp:include page="preview/singleques.jsp"></jsp:include>
									
									<!-- 多选题 -->
									<jsp:include page="preview/moreques.jsp"></jsp:include>
									
									<!-- 判断题 -->
									<jsp:include page="preview/judgeques.jsp"></jsp:include>
									
									<!-- 填空题 -->
									<jsp:include page="preview/blankques.jsp"></jsp:include>
									
									<!-- 简答题 -->
									<jsp:include page="preview/ansques.jsp"></jsp:include>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

</html>

