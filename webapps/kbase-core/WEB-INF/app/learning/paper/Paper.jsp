<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.yonghu_titile li{padding-left:8px;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PageUtils.js"></script>
		<script type="text/javascript">
			$(function(){
				PageUtils.init({
					/*列表勾选标示*/
					ckAllId: 'paperId_list_all',
			 		ckName: 'paperId_list',
			 		/*功能按钮*/
			 		btns: [
			 			{id: 'btnStop',  title: '停用', url: '/app/learning/paper!statusUpdate.htm', fn: 'fn2', 
			 				params: {status: 1}
			 			},
			 			{id: 'btnStrt',  title: '启用', url: '/app/learning/paper!statusUpdate.htm', fn: 'fn2', 
			 				params: {status: 0}, 
			 				before: function(){
			 					var _that = this, bl = true;
								$('input:checkbox[name="' + _that.ckName + '"]:checked').each(
									function(i, item){
										var score = $(item).attr('score'),/*规定分数*/
											scoreReal = $(item).attr('scoreReal');/*实际分数*/
										if(score != scoreReal){/*规定分数应与实际分数相等*/
											bl = false; 
											return bl;
										}
								});
								if(!bl) /*数据不符合要求不应提交过去*/
									layer.alert('总分应与实际分数相等!', -1);
			 					return bl;
			 				}
			 			},
			 			{id: 'btnDel' ,  title: '删除', url: '/app/learning/paper!delete.htm', fn: 'fn2'},
			 			{id: 'btnEdit',  title: '编辑', url: '/app/learning/paper!addOrEditTo.htm', fn: 'fn1' },
			 			{id: 'btnAdd' ,  title: '新增试卷', url: '/app/learning/paper!addOrEditTo.htm', fn: 'fn0'},
			 			{id: 'btnReset', title: '重置', fn: 'reset'},
			 			{id: 'btnSubm',  title: '提交', fn: 'pageInit'},
			 			{id: 'isShowOff',  title: '提交', fn: 'pageInit'}
			 		],
			 		/*分页跳转*/
			 		page: function(pageNo){
				 		var loadindex = layer.load('提交中…');
						$("#pageNo").val(pageNo);
						$('#form0').form('submit', {
					        url: $.fn.getRootPath() + '/app/learning/paper!listPage.htm',  
					        onSubmit:function(params){
					        	if($('#cate_select').val() == '1') {
					        		var nodes = $.fn.zTree.getZTreeObj(
					        			TreeUtils.options.treeId).getSelectedNodes();
									if(nodes){
										var ids = new Array();
										$(nodes).each(function(i, obj){ids.push(obj.id);});
										params.cateIds = ids.join(',');
									}
					        	}
								return $(this).form('validate');
					        },
					        success:function(htmlResult){
					        	var tab = $('div[class="gonggao_con_nr"] table:eq(0)');/*目标表*/
					        	$(tab).find("tr:not(:first)").empty();/*移除非标题行数据*/
					        	$(tab).find("tr:first").after($(htmlResult).find('tr'));/*渲染数据*/
					        	layer.close(loadindex);/*关闭加载条*/
					        	/*渲染列事件*/
					        	var h = $("body").height(), w = $("body").width();
					        	PageUtils.setting({
							 		btns: [
							 			{name: 'relatedQues', title: '手工', url: '/app/learning/paper!relatedQues.htm', fn: 'fn0', height: h, width: w*0.99},
							 			{name: 'randomQues',  title: '随机', url: '/app/learning/paper!randomQues.htm',  fn: 'fn0', height: h, width: w*0.99},
							 			{name: 'checkPaper',  title: '验证', url: '/app/learning/paper!checkPaper.htm',  fn: 'fn0', height: h, width: w*0.99},
							 			{name: 'showDetail',  title: '编辑', url: '/app/learning/paper!addOrEditTo.htm', fn: 'fn0', height: h, width: w*0.99}
							 		]
							 	});
					        }
					    });
			 		}
			 	});
			});
			
			/*分页跳转*/
			function pageClick(pageNo) {
				if(!pageNo) /*获取当前页页码*/
					pageNo = $("#pageNo").val();
				PageUtils.fn.page(pageNo);
			}
			
		 	/*初始化*/
		 	$(function(){
		 		/*列表数据*/
		 		pageClick(1);
		 	});
		 	
		 	function showCate(cateContent, inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+cateContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", function(event){
					onBodyDown(event, cateContent)
				});
			}
			
			function onBodyDown(event, cateContent) {
				if (!(event.target.id == cateContent || $(event.target).parents("#"+cateContent).length>0)) {
					hideCate(cateContent);
				}
			}
			
			function hideCate(cateContent) {
				$("#"+cateContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event, cateContent)
				});
			}
			
			
			$(function(){
				//分类选择树
				var cateTreeObject = {
					cateEl : $('#paperCateNames_select'),
					ktreeEl : $('div.cateContent'),
					treeAttr : {
						view : {
							expandSpeed: ''
						},
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/learning/paper!getPaperCateTree.htm",
							autoParam : ["id"]
						},
						callback : {
							onClick : function(event, treeId, treeNode) {
								cateTreeObject.cateEl.val(treeNode.name);
								$('#paperCateIds_select').val(treeNode.id);
							}
						}
					},
					render : function() {
						var self = this;
						self.cateEl.val('');
						self.cateEl.focus(function(e){
							self.ktreeEl.css({
								'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
								'left' : $(this).offset().left + 'px'
							});
							if(self.ktreeEl.is(':hidden'))
								self.ktreeEl.show();
						});
						
						$('*').bind('click', function(e){
							if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.cateEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
											
							} else {
								if(!self.ktreeEl.is(':hidden'))
									self.ktreeEl.hide();
							}
						});
						self.cateEl.bind('keydown', function(keyArg) {
							if(keyArg.keyCode == 8) {
								$(this).val('');
								$('#paperCateIds_select').val('');
							} else if(keyArg.keyCode == 13) {
								self.cateEl.is(':hidden') || self.cateEl.hide();
							}
						});
						$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
					}
				}
				
				var Page = {
					/*初始化*/
					init: function(){
						/*渲染页面高度*/
						$('body').css('height', $(window).height());
					}
				}
				cateTreeObject.render();
				/*初始化*/
				Page.init();
				
				$('#paperCateMove').click(function(){
					var ids = PageUtils.fn.getMultiChecked();
					if($('#paperCateIds_select').val()==''){
						layer.alert("请选择分类!", -1);
						return false;
					}
					if(ids){
						layer.confirm("确定要选中的数据移动至该分类吗？", function(){
							var loadindex = layer.load('提交中…');
							$.ajax({
								type : 'post',
								url : $.fn.getRootPath() + '/app/learning/paper!moveToCate.htm', 
								data :{"ids":ids.join(','),"cate_id":$('#paperCateIds_select').val()},
								async: true,
								dataType : 'json',
								success : function(data){
									layer.close(loadindex);
									if(data.rst){
										layer.alert("操作成功!", -1);
										pageClick(1);
									}else{
										layer.alert("操作失败!", -1);
									}
								},
								error : function(msg){
									layer.alert("网络错误，请稍后再试!", -1);
								}
							});
						});	
					}
				});
				
			});
		</script>
	</head>
	<body>
		<div style="width: 100%;">
			<div style="width: 16%;float: left;">
				<jsp:include page="PaperCate.jsp"></jsp:include>
			</div>
			<div id="cateContent" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
				<ul id="treeDemo" class="ztree" style="margin-top:0; width:190px;"></ul>
			</div>
			<div style="width: 84%;float: left;">
				<!--******************************************************试卷管理初始化页面*********************************************************************************  -->
				<form id="form0" action="" method="post">
					<input type="hidden" name="paperModel.paperCate.bh" id="paperCate_bh" value="${paperModel.paperCate.bh }" />
					<div class="content_right_bottom" style="width: 100%;">
						<div class="gonggao_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;">
							<div class="gonggao_titile_right" style="margin-right: 10px;">
								<kbs:input type="a" value="停用" key="paperManageStop" id="btnStop"/>
								<kbs:input type="a" value="启用" key="paperManageStart" id="btnStrt"/>
								<kbs:input type="a" value="删除" key="paperManageDel" id="btnDel"/>
								<kbs:input type="a" value="编辑" key="paperManageEdit" id="btnEdit"/>
								<kbs:input type="a" value="新增" key="paperManageAdd" id="btnAdd"/>
								<kbs:if key="paperManageMoveCate">
									<a href="javascript:void(0);" id="paperCateMove" style="float: left; margin-right: 0px;">移动分类</a>
									<input type="text" id="paperCateNames_select" name="paperNames" onclick="showCate('cateContent','paperCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
										style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
				          			<input type="hidden" id="paperCateIds_select" name="paperIds">&nbsp;
								</kbs:if>
							</div>
						</div>
						<div class="yonghu_titile" style="margin-right: 0px;margin-left: 0px;float: left;width: 100%;height: auto;">
							<ul>
								<li>
									试卷名称
									<input type="text" id="name_select" name="paperModel.name" value="${paperModel.name }" style="width:80px"/>
									<input type="hidden" id="quesContent" name="quesContent" value="${quesContent }" style="width:80px"/>
								</li>
								<li>
									开始日期
									<input type="text" id="starttime_select" onclick="javascript:WdatePicker({maxDate:'#F{$dp.$D(\'endtime_select\')}',dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate" name="paperModel.startTime" value='<s:date  name="paperModel.startTime" format="yyyy-MM-dd"/>' style="width:80px"/>
								</li>
								<li>
									结束日期
									<input type="text" id="endtime_select" name="paperModel.endTime" onclick="javascript:WdatePicker({minDate:'#F{$dp.$D(\'starttime_select\')}',dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate" value='<s:date  name="paperModel.endTime" format="yyyy-MM-dd"/>' style="width:80px"/>
								</li>
								<li>
									试卷状态
									<s:select id="status_select" name="paperModel.status" value="#request.paperModel.status" 
										headerKey="" headerValue="-请选择-" listKey="status" listValue="name"  list="#request.paperStatus"></s:select>
								</li>
								<li>
									难易度
									<s:select id="level_select" name="paperModel.level" value="#request.paperModel.level" 
										headerKey="" headerValue="-请选择-" listKey="level" listValue="name"  list="#request.paperLevel"></s:select>
								</li>
								<!-- 
								<li>
									试卷分类
									<s:select id="cate_select" list="#{'0':'不指定', '1':'指定', '2':'未分类'}" name="cate" cssStyle="width: 70px;"></s:select>
								</li>
								 -->
								<li>
									<span style="float:left;font-size:12px">屏蔽过期</span>
									<input style="float: left;width: 17px;margin-top: 9px;" id="isShowOff" name="paperModel.isOverdue" value="1" type="checkbox"/>
								</li>
								<li class="anniu" style="float: right;margin-right: 10px;">
									<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" id="btnReset" /> </a>
									<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="查询" id="btnSubm" /> </a>
								</li>
							</ul>
						</div>
		
						<div class="gonggao_con" style="margin-right: 0px;margin-left: 0px;float: left;width: 100%;">
							<div class="gonggao_con_nr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr class="tdbg">
										<td width="4%">
											<input id="paperId_list_all" type='checkbox'/>
										</td>
										<td width="23%">
											试卷名称
										</td>
										<td width="8%">
											分类
										</td>
										<td width="15%">
											开始日期
										</td>
										<td width="15%">
											结束日期
										</td>
										<td width="7%">
											总分
										</td>
										
										<td width="7%">
											难易度
										</td>
										<td width="7%">
											状态
										</td>
										<td width="14%">
											操作
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
