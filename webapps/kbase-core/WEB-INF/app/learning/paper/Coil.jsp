<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考卷管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/PageUtils.js"></script>
		<script type="text/javascript">
			PageUtils.init({
				/*列表勾选标示*/
		 		ckName: 'coilId_list',
		 		/*功能按钮*/
		 		btns: [
		 			{id: 'btnExpt', title: '导出', url: '/app/learning/coil!export.htm', 
		 				fn: function(item, obj){
		 					console.log(this);
		 				}
		 			},
		 			{id: 'btnStop',  title: '停用', url: '/app/learning/coil!statusUpdate.htm', fn: 'fn2', 
		 				params: {status: 1}
		 			},
		 			{id: 'btnStrt',  title: '启用', url: '/app/learning/coil!statusUpdate.htm', fn: 'fn2', 
		 				params: {status: 0}
		 			},
		 			{id: 'btnDel' ,  title: '删除', url: '/app/learning/coil!delete.htm', fn: 'fn2'},
		 			{id: 'btnEdit',  title: '编辑', url: '/app/learning/coil!addOrEditTo.htm', fn: 'fn1', 
			 			before: function(options){
			 				var h = $('body').height(), w = $('body').width();
			 				options.height = h*0.95;
			 				options.width = w*0.95;
			 				return true;
			 			}
			 		},
		 			{id: 'btnAdd' ,  title: '新增', url: '/app/learning/coil!addOrEditTo.htm', fn: 'fn0', 
		 				before: function(options){
			 				var h = $('body').height(), w = $('body').width();
			 				options.height = h*0.95;
			 				options.width = w*0.95;
			 				return true;
			 			}
			 		},
		 			{id: 'btnReset', title: '重置', fn: 'reset'},
		 			{id: 'btnSubm',  title: '提交', fn: 'pageInit'}
		 		],
		 		/*分页跳转*/
		 		page: function(pageNo){
			 		var loadindex = layer.load('提交中…');
					$("#pageNo").val(pageNo);
					$('#form0').form('submit', {
				        url: $.fn.getRootPath() + '/app/learning/coil!listPage.htm',  
				        onSubmit:function(params){
				        	if($('#cate_select').val() == '1') {
				        		var nodes = $.fn.zTree.getZTreeObj(
				        			TreeUtils.options.treeId).getSelectedNodes();
								if(nodes){
									var ids = new Array();
									$(nodes).each(function(i, obj){ids.push(obj.id);});
									params.cateIds = ids.join(',');
								}
				        	}
							return $(this).form('validate');
				        },
				        success:function(htmlResult){
				        	var tab = $('div[class="gonggao_con_nr"] table:eq(0)');/*目标表*/
				        	$(tab).find("tr:not(:first)").empty();/*移除非标题行数据*/
				        	$(tab).find("tr:first").after($(htmlResult).find('tr'));/*渲染数据*/
				        	layer.close(loadindex);/*关闭加载条*/
				        }
				    });
		 		}
		 	});
		 	
			/*分页跳转*/
			function pageClick(pageNo) {
				if(!pageNo) /*获取当前页页码*/
					pageNo = $("#pageNo").val();
				PageUtils.fn.page(pageNo);
			}
			
		 	/*初始化*/
		 	$(function(){
		 		/*列表数据*/
		 		pageClick(1);
		 	});
		</script>
	</head>
	<body>
		<div style="width: 100%;">
			<div style="width: 15%;float: left;">
				<jsp:include page="CoilCate.jsp"></jsp:include>
			</div>
			<div style="width: 85%;float: left;">
				<!--******************************************************考卷管理初始化页面*********************************************************************************  -->
				<form id="form0" action="" method="post">
					<div class="content_right_bottom" style="min-width: 860px;">
						<div class="gonggao_titile">
							<div class="gonggao_titile_right">
								<a href="javascript:void(0);" id="btnExpt">导出</a>
								<a href="javascript:void(0);" id="btnStop">停用</a>
								<a href="javascript:void(0);" id="btnStrt">启用</a>
								<a href="javascript:void(0);" id="btnDel">删除</a>
								<a href="javascript:void(0);" id="btnEdit">编辑</a>
								<a href="javascript:void(0);" id="btnAdd">新增</a>
							</div>
						</div>
						<div class="yonghu_titile">
							<ul>
								<li>
									考卷名称：
									<input type="text" id="name_select" name="coilModel.name" value="${coilModel.name }" />
								</li>
								<li>
									考卷状态：
									<s:select id="status_select" name="coilModel.status" value="#request.coilModel.status" 
										headerKey="" headerValue="-请选择-" listKey="status" listValue="name"  list="#request.paperStatus"></s:select>
								</li>
								<li>
									考卷难易：
									<s:select id="level_select" name="coilModel.level" value="#request.coilModel.level" 
										headerKey="" headerValue="-请选择-" listKey="level" listValue="name"  list="#request.paperLevel"></s:select>
								</li>
								<li>
									考卷分类：
									<s:select id="cate_select" list="#{'0':'不指定', '1':'指定', '2':'未分类'}" name="cate" cssStyle="width: 70px;"></s:select>
								</li>
								<li class="anniu">
									<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" id="btnReset" /> </a>
									<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" id="btnSubm" /> </a>
								</li>
							</ul>
						</div>
		
						<div class="gonggao_con">
							<div class="gonggao_con_nr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr class="tdbg">
										<td width="4%">
											<input id="coilId_list_all" type='checkbox' />
										</td>
										<td width="25%">
											考卷名称
										</td>
										<td width="17%">
											分类
										</td>
										<td width="5%">
											总分
										</td>
										<td width="7%">
											及格分数
										</td>
										<td width="7%">
											使用次数
										</td>
										<td width="5%">
											难易度
										</td>
										<td width="5%">
											状态
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
