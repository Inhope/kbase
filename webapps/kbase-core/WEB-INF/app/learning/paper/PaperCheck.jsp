<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理-试卷验证</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.box>tr>th{width: 20%;}
			.box>tr>td{width: 30%;}
			.box>tr>td>input{width: 97%;}
			
			/*试卷管理-随机规则*/
			.set_title, .sum_title, .req_title, .rule_title{background-color: #d5d5d5;}
			.set_con, .sum_con, .req_con, .rule_con{}
			/*总体（标题）*/
			.set_title>span, .sum_title>span, .req_title>span, .rule_title>span{float: left;font-size: 14px;font-weight:bold;margin-left: 5px;}
			/*总体（内容）*/
			.set_con table tr, .sum_con table tr, .req_con table tr, .rule_con table tr{height: 24px;}
			.set_con td, .sum_con td, .req_con td, .rule_con td{text-align: center;}
			.set_con .left_title table tr:first-child>td, .set_con .right_con table tr:first-child>td, 
				.sum_con table tr:first-child>td, .req_con table tr:first-child>td, .rule_con table tr:first-child>td{font-weight: bold;}
			.req_con select, .rule_con select{width: 99%;}
			
			/*题目选择（内容左侧）*/
			.set_con .left_title{width: 15%; float: left;}
			.set_con .left_title table tr:not(:first-child){cursor:pointer }
			/*题目选择（标题右侧）*/
			.set_con .right_con{width: 85%; float: left;}
			.set_con .right_con table tr>td:nth-child(1){width: 20%;}
			.set_con .right_con table tr>td:nth-child(2){width: 20%;}
			.set_con .right_con table tr>td:nth-child(3){width: 15%;}
			.set_con .right_con table tr>td:nth-child(4){width: 15%;}
			.set_con .right_con table tr>td:nth-child(5){width: 15%;}
			.set_con .right_con table tr>td:nth-child(6){width: 15%;}
			/*题目选择（标题右侧-选中效果）*/
			.sel{background-color: orange;}
			
			/*必选题*/
			.req_con table tr>td:nth-child(1){width: 15%;}
			.req_con table tr>td:nth-child(2){width: 15%;}
			.req_con table tr>td:nth-child(3){width: 30%;}
			.req_con table tr>td:nth-child(4){width: 40%;}
			
			/*规则是否通过*/
			.suc{
				background-image:url(${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/black/images/tree_icons.png);
				background-repeat:no-repeat; background-position:-255px 0px; height:18px; width:18px; margin:0 auto;
			}
			.err{
				background-image:url(${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/black/images/tree_icons.png);
				background-repeat:no-repeat; background-position:-255px -18px ; height:18px; width:18px; margin:0 auto;
			}
			.err1{background-color: red;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
		<script type="text/javascript">
			var _QuesTypes = ${quesTypes};
			var PaperCheck = {
				/*富文本框*/
				editor: {
					init: function(){
						/*初始化相关富文本*/
						$('textarea[id^="kd_"]').each(function(ind, item){
							var id = $(item).attr('id');
							KindEditor.create('textarea[id="' + id + '"]', 
								{width: '99%', readonlyMode : true,height: '60px', minHeight: '50px', items: []});
						});
					}
				},
				/*表单提交*/
				from: {
					submit:function(){
						var ckResult = ${ckResult.rst };
						if(ckResult){
							window.open($.fn.getRootPath() 
								+ '/app/learning/paper!preview.htm?paperId=${paper.id }');
						} else {
							layer.alert('数据不全，无法生成考卷！', -1);
						}
					},
					init: function(){
						var _that = this;
						$('#previewBtn').click(function(){
							_that.submit();
						});
					}
				},
				/*初始化*/
				init:function(){
					var _that = this;
					_that.from.init();
					_that.editor.init();
				}
			}
			
			$(function(){
				PaperCheck.init();
			});
		</script>
	</head>
	<body>
		<div class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;float: left;">
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th>
						试卷名称：
					</th>
					<td>
						${paper.name }
					</td>
					<th>
						试卷状态：
					</th>
					<td>
						<s:iterator value="#request.paperStatus" var="va">
							<s:if test="#request.paper.status == #va.status">${va.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				<tr>
					<th>
						试卷总分：
					</th>
					<td>
						${paper.score }
					</td>
					<th>
						试卷难易：
					</th>
					<td>
						<s:iterator value="#request.paperLevel" var="pLevel">
							<s:if test="#request.paper.level == #pLevel.level">${pLevel.name }</s:if>
						</s:iterator>
					</td>
				</tr>
				
				<!-- 概述表（开始） -->
				<tr flag="next">
					<td colspan="4" class="sum_title">
						<span>题目选择</span>
					</td>
				</tr>
				<tr flag="next">
					<td colspan="4" class="sum_con">
						<table flag="sum" cellspacing="0" class="box_" style="width: 100%;">
							<tr>
								<td width="20%">题型</td>
								<td width="20%">
									<s:if test="#request.paper.type==1">每题分数</s:if>
									<s:else>计划分数</s:else>
								</td>
								<td width="20%">计划题数</td>
								<td width="20%">实际题数</td>
								<td width="20%">实际分数</td>
							</tr>
							<s:set var="datas" value="#request.ckResult.datas" ></s:set>
							<s:iterator value="#request.paper.mapRange" var="va">
								<s:set var="data" value="" ></s:set>
								<s:iterator value="#datas" var="va1">
									<s:if test="#va1.key == #va.key">
										<s:set var="data" value="#va1.value"></s:set>
									</s:if>
								</s:iterator>
								<tr>
									<td>
										<s:property value="#request.quesTypes[#va.key]"/>
									</td>
									<td>${va.value.score }</td>
									<td>${va.value.amount }</td>
									<td <s:if test="!#data.rst">class="err1"</s:if>>
										<s:property value="#data.amount"/>
									</td>
									<td <s:if test="!#data.rst">class="err1"</s:if>>
										<s:property value="#data.score"/>
									</td>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<!-- 概述表（结束） -->
				
				<!-- 必选题（开始） -->
				<tr flag="next">
					<td colspan="4" class="req_title">
						<span>必选题</span>
					</td>
				</tr>
				<tr flag="next">
					<td colspan="4" class="req_con">
						<table flag="req" cellspacing="0" class="box_" style="width: 100%;">
							<tr>
								<td>验证结果</td>
								<td>题目类型</td>
								<td>试题内容</td>
								<td>试题备注</td>
							</tr>
							<s:set var="quess" value="#request.ckResult.quess" ></s:set>
							<s:iterator value="#request.paper.paperQuess" var="va" status="st">
								<s:set var="ques" value=""></s:set>
								<s:iterator value="#quess" var="va1">
									<s:if test="#va1.id == #va.id">
										<s:set var="ques" value="#va1"></s:set>
									</s:if>
								</s:iterator>
								<tr>
									<td>
										<div <s:if test="#ques.rst">class="suc"</s:if>
											<s:else>class="err"</s:else>></div>
									</td>
									<td>
										<s:property value="#request.quesTypes[#va.ques.quesType.key]"/>
									</td>
									<td>
										<textarea id="kd_${st.index }" readonly="readonly" style="display: none;">
											${va.ques.content }
										</textarea>
									</td>
									<td <s:if test="!#ques.rst">class="err1"</s:if>>
										${va.ques.name }
									</td>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<!-- 必选题（结束） -->
				
				<s:if test="#request.paper.type==1">
					<!-- 随机题规则（开始） -->
					<tr flag="next">
						<td colspan="4" class="rule_title">
							<span>随机题规则</span>
						</td>
					</tr>
					<tr flag="next">
						<td colspan="4" class="rule_con">
							<table flag="rule" cellspacing="0" class="box_" style="width: 100%;">
								<tr>
									<td>
										验证结果
									</td>
									<td>题目类型</td>
									<td>题目分类</td>
									<td>随机难度</td>
									<s:iterator value="#request.paperLevel" var="pLevel">
										<td>${pLevel.name }</td>
									</s:iterator>
								</tr>
								<s:set var="rules" value="#request.ckResult.rules" ></s:set>
								<s:iterator value="#request.paper.paperRules" var="va" status="st">
									<s:set var="rule" value=""></s:set>
									<s:iterator value="#rules" var="va1">
										<s:if test="#va1.id == #va.id">
											<s:set var="rule" value="#va1"></s:set>
										</s:if>
									</s:iterator>
									<tr>
										<td>
											<div <s:if test="#rule.rst">class="suc"</s:if>
												<s:else>class="err"</s:else>></div>
										</td>
										<td>
											<s:property value="#request.quesTypes[#va.key]"/>
										</td>
										<td <s:if test="!#rule.crst">class="err1"</s:if>>
											${va.quesCate.name }
										</td>
										<td <s:if test="!#rule.arst">class="err1"</s:if>>
											${va.amount }（${rule.amount }）
										</td>
										<s:set var="levs" value="#rule.levs"></s:set>
										<s:iterator value="#request.paperLevel" var="pLevel">
											<s:set var="k" value="#pLevel.level + ''"></s:set>
											<s:set var="lev" value=""></s:set>
											<s:iterator value="#levs" var="va1">
												<s:if test="#va1.lk == #k">
													<s:set var="lev" value="#va1"></s:set>
												</s:if>
											</s:iterator>
											<s:if test="#lev != null">
												<td <s:if test="!#lev.rst">class="err1"</s:if>>
													${va.mapContent[k].amount }（${lev.amount }）
												</td>
											</s:if>
											<s:else>
												<%-- 如果新增了新的难度lev应该为空 --%>
												<td>0（0）</td>
											</s:else>
										</s:iterator>
									</tr>
								</s:iterator>
							</table>
						</td>
					</tr>
					<!-- 随机题规则（结束） -->
					
				</s:if>
			</table>
		</div>
		<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
			<a href="javascript:void(0);" flag="next" id="previewBtn">预览</a>
		</div>
	</body>
</html>