<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<table cellspacing="0" class="box" style="width: 100%;">
	<!-- 概述表（开始） -->
	<tr flag="next">
		<td colspan="4" class="sum_title">
			<span>题目选择</span>
		</td>
	</tr>
	<tr flag="next">
		<td colspan="4" class="sum_con">
			<table flag="sum" cellspacing="0" class="box_" style="width: 100%;">
				<tr>
					<td width="20%">题型</td>
					<td width="20%">每题分数</td>
					<td width="20%">计划题数</td>
					<td width="20%">实际题数</td>
					<td width="20%">总分</td>
				</tr>
				<s:iterator value="#request.paper.mapRange" var="va">
					<tr>
						<td>
							<s:iterator value="#request.quesTypes" var="va1">
								<s:if test="#va1.key == #va.key">${va1.value }</s:if>
							</s:iterator>
						</td>
						<td>${va.value.score }</td>
						<td>${va.value.amount }</td>
						<td></td>
						<td></td>
					</tr>
				</s:iterator>
			</table>
		</td>
	</tr>
	<!-- 概述表（结束） -->
	
	<!-- 必选题（开始） -->
	<tr flag="next">
		<td colspan="4" class="req_title">
			<span>必选题</span>
		</td>
	</tr>
	<tr flag="next">
		<td colspan="4" class="req_con">
			<table flag="req" cellspacing="0" class="box_" style="width: 100%;">
				<tr>
					<td>序号</td>
					<td>题目类型</td>
					<td>试题名称</td>
					<td>试题内容</td>
				</tr>
				<s:iterator value="#request.paper.validPaperQuess" var="va" status="st">
					<tr>
						<td>${st.index+1 }</td>
						<td>
							<s:iterator value="#request.quesTypes" var="va1">
								<s:if test="#va1.key == #va.ques.quesType.key">
									${va1.value }
									<input type="hidden" name="key" value="${va1.key }">
								</s:if>
							</s:iterator>
						</td>
						<td>
							${va.ques.name }
							<input type="hidden" name="quesName" value="${va.ques.name }">
							<input type="hidden" name="quesId" value="${va.ques.id }">
						</td>
						<td>
							............
						</td>
					</tr>
				</s:iterator>
			</table>
		</td>
	</tr>
	<!-- 必选题（结束） -->
	
	<!-- 随机题规则（开始） -->
	<tr flag="next">
		<td colspan="4" class="rule_title">
			<span>随机题规则</span>
		</td>
	</tr>
	<tr flag="next">
		<td colspan="4" class="rule_con">
			<table flag="rule" cellspacing="0" class="box_" style="width: 100%;">
				<tr>
					<td>
						序号
					</td>
					<td>题目类型</td>
					<td>题目分类</td>
					<td>随机难度</td>
					<s:iterator value="#request.paperLevel" var="pLevel">
						<td>${pLevel.name }</td>
					</s:iterator>
				</tr>
				<s:iterator value="#request.paper.paperRules" var="va">
					<tr>
						<td>
							${st.index+1 }
						</td>
						<td>
							<s:iterator value="#request.quesTypes" var="va1">
								<s:if test="#va1.key == #va.key">${va1.value }</s:if>
							</s:iterator>
							<input type="hidden" name="key" value="${va.key }">
						</td>
						<td>
							${va.quesCate.name }
							<input type="hidden" name="quesCateId" value="${va.quesCate.id }">
						</td>
						<td>
							<input type="text" name="amount" value="${va.amount }">
						</td>
						<s:iterator value="#request.paperLevel" var="pLevel">
							<td>
								<s:set var="k" value="#pLevel.level + ''"></s:set>
								<input type="text" name="level${k }" value="${va.mapContent[k].amount }">
							</td>
						</s:iterator>
					</tr>
				</s:iterator>
			</table>
		</td>
	</tr>
	<!-- 随机题规则（结束） -->
</table>
