<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<s:iterator value="page.result" var="va">
		<tr>
			<td>
				<input name="coilId_list" type='checkbox' value="${va.id }" />
			</td>
			<td>
				<a href="javascript:void(0);" style="color: blue;" 
					name="showDetail" params="{coilId: '${va.id }'}">${va.name }</a>&nbsp;
			</td>
			<td>
				<s:if test="#va.paperCate.delFlag==0">
					${va.paperCate.name }
				</s:if>&nbsp;
			</td>
			<td>
				${va.score }
			</td>
			<td>
				${va.scorePass }
			</td>
			<td>
				
			</td>
			<td>
				<s:iterator value="#request.paperLevel" var="pLevel">
					<s:if test="#va.level == #pLevel.level">${pLevel.name }</s:if>
				</s:iterator>
			</td>
			<td>
				<s:iterator value="#request.paperStatus" var="pStatus">
					<s:if test="#va.status == #pStatus.status">${pStatus.name }</s:if>
				</s:iterator>
			</td>
		</tr>
	</s:iterator>
	<tr class="trfen">
		<td colspan="9">
			<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
		</td>
	</tr>
</table>
