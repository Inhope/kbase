<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>

	<div class="px_b_left">
		<div class="px_b2_left">
			<div class="px_b_title">
				<span><img src="${pageContext.request.contextPath}/resource/learning/images/px_ks_03.jpg" />&nbsp;</span>
				<span id="allCourse" style="cursor: pointer;">全部课程</span>
			</div>
			<div class="px_b_con">
				<ul class="pc_b_a">
					<c:forEach items="${cateJa}" var="cate">
						<li>
							<a href="javascript:void(0);">
								<span class="pc_plus" onclick="loadMoreCate(this,'${cate.id }');"></span>
								<font name="cateName" _id="${cate.id }">${cate.name }</font>
							</a>
						</li>
					</c:forEach>
				</ul>
			</div>
			<div style="display: none;" id="cateTemplate">
				<ul class="pc_b_b">
					<li>
						<a href="javascript:void(0);">
							<span class="pc_plus" onclick="loadMoreCate(this,'{0}');"></span>
							<font name="cateName" _id="{0}">{1}</font>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
