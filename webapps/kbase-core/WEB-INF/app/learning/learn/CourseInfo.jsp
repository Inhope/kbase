<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程学习</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		<style type="text/css">
			
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		
		<script type="text/javascript">
			$(function(){
				$('.course_con_title li a').each(function(){
					var id = $(this).attr("id");
					$(this).click(function(){
						$('.course_item').find('div').remove();
						$('#course_comment_frame').css("display", "none");
						
						$(this).parent().addClass('ts').siblings().removeClass('ts');
						
						if(id == 'cont_tab'){//课件内容
							$('.course_item').append($('#course_content').html());
							
						}else if (id == 'attach_tab'){//附件目录
							$('.course_item').append($('#course_file').html());
							
						}else if (id == 'related_ques_tab'){//相关知识
							$('.course_item').append($('#course_ques').html());
							
						}else if (id == 'comment_tab'){//评论内容
							//$('.course_item').append($('#course_comment').html());
							
							$('#course_comment_frame').css("display", "block");
							var url = $.fn.getRootPath() + '/app/learning/course-comment!courseComments.htm?courseId=' + $('#courseId').val();
							$('#course_comment_frame').attr("src", url);
							
						}
					
					});
				});	
				//页面初始化时显示课件内容Tab
				$('#cont_tab').click();
				
				$('.course_item').on('click', '.related-ques-detail', function(){
					var question = $(this).attr('_question');
					parent.TABOBJECT.open({
						id : 'courseFileInfo',
						name : "知识详情",
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+question,
						isRefresh : true
					}, this);
				});
				
				//学习
				$('#learn-xx-btn').click(function(){
					var _learnstatus = $(this).attr('_learnstatus');
					var courseId = $('#courseId').val();
					var courseName = $('#courseName').val();
					
					if(_learnstatus == '0'){
						//点击未学习
						$.post($.fn.getRootPath() + '/app/learning/learn!addLearnCount.htm', 
							{'courseId': courseId, 'courseName': courseName},
							function(data){
								if(data.rst == '1'){
									$('#learn-xx-btn').text('立即学习');
									$('#learn-xx-btn').attr('_learnstatus', '1');
									$('#learn-xx-btn').css('background-color', '#34a519');
									
									$('#learn_status').text('立即学习');
									$('#learn_status').css('color', '#34a519');
									$('.learn_num').text(parseInt($('.learn_num').text())+1);
								}
								
						}, "json");
					}else if(_learnstatus == '1' || _learnstatus == '2'){
						//点击立即学习
						parent.TABOBJECT.open({
							id : "立即学习",
							name : "立即学习",
							title : "立即学习",
							hasClose : true,
							url : $.fn.getRootPath()+"/app/learning/learn!learnNow.htm?courseId="+courseId,
							isRefresh : true
						}, this);
						
					}
					
				});
				
				
				
				
				//推荐
				$('#recommendbtn').click(function(){
					var courseId = $('#courseId').val();
					window.__kbs_layer_recommend = $.layer({
						type : 2,
						border : [ 2, 0.3, '#000' ],
						title : [ "课程推荐", 'font-size:14px;font-weight:bold;' ],
						shade: [0.3, '#000'],
						closeBtn : [ 0, true ],
						iframe : {
							src : $.fn.getRootPath() + "/app/learning/learn!toRecommend.htm?courseId="+courseId, 
						},
						area: ['530px', '490px']
					});
				});
				
				
				
				
				//收藏
				$('.learn_collect_title').on('click', function(){
					var courseId = $('#courseId').val();
					$.post($.fn.getRootPath() + "/app/learning/learn!collect.htm", {'courseId': courseId}, 
						function(data){
							if (data.rst == "1"){
								parent.layer.alert("课程收藏成功", -1);
								$('.learn_collect_num').text(parseInt($('.learn_collect_num').text())+1);
							}else if (data.rst == "2") {
								parent.layer.alert("课程已经收藏过", -1);
							}else{
								parent.layer.alert(data.msg, -1);
							}
						}, 'json');
				});
				
				
				
				
			});
				
				
				
		</script>
	</head>
	<body>
		<input type="hidden" id="courseId" value="${course.id}">
		<input type="hidden" id="courseName" value="${course.name}">
		
		<div class="page_all">
	        <div class="class_top">
	            <div class="class_left fl">
	                  <s:if test="#request.course.fileName==null">
							<img src="${pageContext.request.contextPath}/resource/learning/images/pxks_03.jpg" alt=""/> 
						</s:if>
						<s:else>
							<img src="${pageContext.request.contextPath }/doc/course/cover/${course.fileName}" alt="" width="320px" height="200px"/> 
						</s:else>
	            </div>
	            <div class="class_right fl">
	               <h1>${course.name }</h1>
	               <h2>
	               		学习人数  <span class="orange learn_num">${course.count}</span> ｜ 综合评分  <span class="orange">${course.score}</span> &nbsp;  
	               
	               		<a href="javascript:void(0);" class="learn_collect_title">收藏 </a>
		               	<span class="learn_collect_num orange">
							${course.collectCount}
						</span>
	               </h2>
	               <h3>
	               		<s:if test="#request.course.courseLearn==null || #request.course.courseLearn.size==0">
							<span id="learn_status" style="color: rgb(210, 0, 0);">未学习</span>
						</s:if>
						<s:else>
							<s:iterator value="#request.course.courseLearn" var="va">
								<s:if test="#va.status == 1">
									<span id="learn_status" style="color: #34a519;">立即学习</span>
								</s:if>
								<s:if test="#va.status == 2">
									<span id="learn_status" style="color: #34a519;">已学习</span>
								</s:if>
							</s:iterator>		
						</s:else>
	               </h3>
	               <h4>
		               <ul>
		                  <li class="btn_std">
		                  	<s:if test="#request.course.isValid">
		                  	<s:if test="#request.course.courseLearn==null || #request.course.courseLearn.size==0">
								<a href='javascript:void(0);' id="learn-xx-btn" _learnstatus="0" class='learn-btn'>学习</a>
							</s:if>
							<s:else>
								<s:iterator value="#request.course.courseLearn" var="va">
									<s:if test="#va.status == 1">
										<a href='javascript:void(0);' id="learn-xx-btn" _learnstatus="1" class='learn-btn' style="background-color:#34a519;">立即学习</a>
									</s:if>
									<s:if test="#va.status == 2">
										<a href='javascript:void(0);' id="learn-xx-btn" _learnstatus="2" class='learn-btn' style="background-color:#34a519;">已学习</a>
									</s:if>
								</s:iterator>		
							</s:else>
							</s:if>
							<s:else>
								<a href='javascript:void(0);' style="background-color:gray;">已结束</a>
							</s:else>
		                  </li>
	                 	
	                 	  <li class="btn_int">
	                 	  		<a href='javascript:void(0);' id="recommendbtn" class='learn-btn'>推荐</a>
	                 	  </li>
		                 
		               </ul>
	               </h4>
	            </div><div style="clear:both;"></div>
	        </div>
	        
	        
	         
	        <div class="px_b_kc">
	           <div class="px_b_box">
	               <div class="class_title">
						<ul class="course_con_title">
							<li class="ts">
								<a href="javascript:void(0);" id="cont_tab">课程内容</a>
							</li>
							<li>
								<a href="javascript:void(0);" id="attach_tab">附件目录</a>
							</li>
							<li>
								<a href="javascript:void(0);" id="related_ques_tab">相关知识</a>
							</li>
							<li>
								<a href="javascript:void(0);" id="comment_tab">评论内容</a>
							</li>
						</ul>
	               </div>

					<div class="class_conn">
						<div class="usbox course_item">
							<!-- iframe仅评论用tab -->
							<iframe id="course_comment_frame" src=""  frameborder="0" scrolling="no" style="width: 100%; min-height: 1150px; display: none;"/></iframe>
						</div>
					</div>
	               
	               
	               <div style="clear:both;"></div>
	           </div>
	        </div>        
	                      
	    </div>
	    
	    
	    <!-- --------------课程内容start-------------- -->
		<div id="course_content" style="display: none;">
			<div class="kc_ml" style="margin: 0px; line-height: 15px;">
               <!-- ${contentStr } -->
               ${course.content }
            </div>
		</div>
		<!-- 课程内容end -->
		
		
		<!-- --------------课程附件start-------------- -->
		<div id="course_file" style="display: none;">
			<div class="kc_ml" style="margin: 0px; line-height: 15px;">
				<ul class="kc_mu">
					<s:if test="#request.course.courseFile!=null && #request.course.courseFile.size>0">
						<s:iterator value="course.courseFile" var="va" status="status">
							<li>
								<a href="#">
									<span>第${va.order+1 }节&emsp;${va.realName }</span>
								</a>
								<div class="mu_con">
									${va.remark }
								</div>
							</li>
						</s:iterator>									
					</s:if> 
				</ul>
             </div>
		</div>
		<!-- 课程附件end -->
		
		
		
		<!-- --------------课程相关知识start-------------- -->
		<div id="course_ques" style="display: none;">
			<div style="padding: 5px;">
				<ul>
					<s:if test="#request.course.relatedQueses!=null && #request.course.relatedQueses.size>0">
						<s:iterator value="course.relatedQueses" var="va">
							<li style="list-style:none;line-height:25px;">
								${va.question }
								<span>
									<a class="related-ques-detail" href="javascript:void(0);" _question="${va.question}" style="text-decoration:none;">
										<font color="#CC0000">[详情]</font>
									</a>
								</span>
							</li>
						</s:iterator>									
					</s:if>
				</ul>
			</div>
		</div>
		<!-- 课程相关知识end -->
		
		
		<!-- --------------课程评论start-------------- -->
		<div id="course_comment" style="display: none;">
			<div style="padding: 5px;">
				评论评论评论评论评论评论评论..............
			</div>
		</div>
		<!-- 课程评论end -->
		
    
    
	</body>
</html>
