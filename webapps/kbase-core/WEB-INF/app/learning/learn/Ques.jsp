<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程自测</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		<style type="text/css">
			html{overflow-y:scroll;-moz-user-select:none;}
			a{padding:4px;color:#333333;text-decoration:none;line-height:30px;}
			.selected{}
			.select{}
			.input-exam{text-align: center;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/course.js"></script>
		<script type="text/javascript">
			var $checked = false; //是否已经提交试卷；
			//单选，选择操作
			function s_select(_this){
				if (!$checked){
					//换选中图标并更改选中状态
					$(_this).parents("ul").find("img").attr("src",$.fn.getRootPath()+'/resource/learning/images/ks_06.jpg')
					.attr("checked",false);
					$(_this).find("img").attr("src",$.fn.getRootPath()+'/resource/learning/images/ks_03.jpg')
					.attr("checked",true);
				}
			}
			//多选，选择操作
			function m_select(_this){
				if (!$checked){
					if($(_this).find("img").attr("checked")){
						$(_this).find("img").attr("src",$.fn.getRootPath()+'/resource/learning/images/ks_12.jpg')
						.attr("checked",false);
					}else{
						$(_this).find("img").attr("src",$.fn.getRootPath()+'/resource/learning/images/ks_09.jpg')
						.attr("checked",true);
					}
				}
			}
			
			//判断试题正误（用户选择的答案与标准答案做比较）
			function check(obj){
				var ques = $(obj);
				var flag = true;
				ques.find("ul img").each(function(){
					if($(this).attr("isRight")==1){
						if(!$(this).attr("checked")){
							flag = false;
						}
					}else{
						if($(this).attr("checked")){
							flag = false;
						}
					}
				});
				if(flag){
					ques.find("#title").append('<span class="incorrect">正确</span>');
				}else{
					ques.find("#title").append('<span class="correct">错误</span>');
				}
				//显示参考答案
				$('[name="referDiv"]').css("display","");
			}
			
			//开始考试
			var begintime = 60*${course.limitMinute };//自测限时
			var counttime = 0;//考试耗时
			
			function examTimer(){
				if('${course.limitMinute}'>0){
					var minute="0";
					var second="0";
					begintime = parseInt(begintime)-1;
					minute = parseInt(begintime/60);
					second = begintime%60;
					if(second < 0){
						//超过考试规定时间时，停止计时，强制提交试卷
						stopExam();
						return false;
					}else{
						if(minute == "0"){
						$('#timeShow').html(second + "秒");
					}else{
						$('#timeShow').html(minute + "分" + second + "秒");
					}
					timer = window.setTimeout("examTimer()", 1000);
					counttime = counttime + 1;
					}
				}
			}
			
			//开始考试
			function startExam(){
				$(".exam-cover").css("display", "none");//隐藏	
				$('#start_exam').css("display", "none");
				$('#stop_exam').css("display", "block");
				examTimer();
			}
			
			//停止考试
			function stopExam(){
				if(!$checked){
					clearTimeout(window.timer);
					$('#timeShow').html($('#timeShow').html() + "<label style='margin-left:100px;'>耗时：<span>" + counttime + "秒</span></label>");
					$('.ques-div').each(function(index, item){
						check(item);
					});
					$checked = true;
					//自测记录
					$.post($.fn.getRootPath() + '/app/learning/learn!ques.htm', 
						{'courseId': $("#courseId").val()}, function(data){
						}, "json");
					$("#stop_exam").css("background-color","#ECECEC");
				}
			}
		</script>
	</head>
	<body>
		<input type="hidden" id="courseId" value="${course.id}">
		<div class="page_all">
			<div class="exam">
				<div class="exam_top">
					<div class="usbox">
						<div class="examtop_left fl" style="width: 60%;">
							<ul style="width: 75%;">
								<li>
									课程名称：
									<span>${course.name}</span>
								</li>
								<li>
									考题限时：<span id="timeShow">${course.limitMinute }分钟</span>
								</li>
							</ul>
						</div>
						<div class="examtop_right fr">
							<a href='javascript:void(0);' id="stop_exam" onclick="stopExam()" style="display:none;">提交测试</a>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<div class="exam_dx">
							<div class="usbox">
							<s:set var="answItem_no" value="{'A','B','C','D','E','F','G','H','I','J','K','L','M','N'}"></s:set>
							<s:iterator value="#request.list" var="va">
								<s:if test="#va.quesTypeKey == 0 && #va.quesList.size > 0">
									<div class="eaxm_title" style="margin-bottom: 15px;">
			                        	<span>
											${va.quesTypeName}	<!-- 单选题 -->
										</span>
			                        </div>
			                        <div class="exam_con">
			                        	<div class="usbox">
			                            	<s:iterator value="#va.quesList" var="ques" status="index">
												<div id="singleQues" class="ques-div exam_a"><!-- 单选题 -->
					                            	<span id="title">${index.index+1 }.&nbsp;${ques.quesContent}</span>
													<ul>
						                                <s:iterator value="#ques.quesResults" var="quesResult" status="indexR">
						                                	<li>
							                                	<a href="#">
							                                		<span class='ques-answer' style="display: inline;" 
							                                			onclick="javascript:s_select(this)">
							                                			<img class='select' isRight="${quesResult.isRight}" src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>
																		${answItem_no[indexR.index]}.&emsp;${quesResult.content}
																	</span>
							                                	</a>
						                                	</li>
														</s:iterator>
					                                	<li style="width: 100%;float: left;margin: 10px 0px 20px 0px;display: none;" name="referDiv">
						                                	<a href="#">
					                                			<font color="green" style="border: 1px dashed green;padding: 2px 10px 2px 10px;">
					                                				参考答案:
					                                				<s:iterator value="#ques.quesResults" var="quesResult" status="indexR">
					                                					<s:if test="#quesResult.isRight==1">
					                                						<font style="margin-right: 5px;">
					                                						${answItem_no[indexR.index]}
				                                							</font>
					                                					</s:if>
					                                				</s:iterator>
					                                			</font>
						                                	</a>
					                                	</li>
													</ul>
					                        	</div>
			                            		<div style="clear:both;"></div>
			                            	</s:iterator>
			                        	</div>
			                        </div>
								</s:if>
							</s:iterator>
							<s:iterator value="#request.list" var="va">
								<s:if test="#va.quesTypeKey == 1 && #va.quesList.size > 0">
									<div class="eaxm_title dx" style="margin-bottom: 15px;">
			                          	${va.quesTypeName}	<!-- 多选题 -->
			                        </div>
			                        <div class="exam_con">
			                        	<div class="usbox">
			                            	<s:iterator value="#va.quesList" var="ques" status="index">
						                        <div class="ques-div exam_a"><!-- 多选题  -->
					                            	<span id="title">${index.index+1 }.&nbsp;${ques.quesContent}</span>
					                            	<ul>
						                                <s:iterator value="#ques.quesResults" var="quesResult" status="indexR">
						                                	<li>
							                                	<a href="#">
							                                		<span class='ques-answer' style="display: inline;" 
							                                			onclick="m_select(this)">
							                                			<img class='select' isRight="${quesResult.isRight}" src="${pageContext.request.contextPath}/resource/learning/images/ks_12.jpg"/>
																		${answItem_no[indexR.index]}.&emsp;${quesResult.content}
																	</span>
							                                	</a>
						                                	</li>
														</s:iterator>
					                                	<li style="width: 100%;float: left;margin: 10px 0px 20px 0px;display: none;" name="referDiv">
						                                	<a href="#">
					                                			<font color="green" style="border: 1px dashed green;padding: 2px 10px 2px 10px;">
					                                				参考答案:
					                                				<s:iterator value="#ques.quesResults" var="quesResult" status="indexR">
					                                					<s:if test="#quesResult.isRight==1">
					                                						<font style="margin-right: 5px;">
					                                						${answItem_no[indexR.index]}
				                                							</font>
					                                					</s:if>
					                                				</s:iterator>
					                                			</font>
						                                	</a>
					                                	</li>
					                            	</ul>
					                        	</div>
			                            		<div style="clear:both;"></div>
			                            	</s:iterator>
			                        	</div>
			                        </div>
								</s:if>
							</s:iterator>
							<s:iterator value="#request.list" var="va">
								<s:if test="#va.quesTypeKey == 2 && #va.quesList.size > 0">
									<div class="eaxm_title dx" style="margin-bottom: 15px;">
			                          	${va.quesTypeName}	<!-- 判断题 -->
			                        </div>
			                        <div class="exam_con">
			                        	<div class="usbox">
			                            	<s:iterator value="#va.quesList" var="ques" status="index">
						                        <div class="ques-div exam_b"><!-- 判断题  -->
					                            	<span id="title">${index.index+1 }.&nbsp;${ques.quesContent}</span>
					                                <ul>
					                                	<li>
						                                	<a href="#">
						                                		<span class='ques-answer' style="display: inline;" 
						                                			onclick="javascript:s_select(this)">
						                                			<img class='select' isRight="${quesResult.isRight==1?1:0}" src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>
																	${answItem_no[0]}.&emsp;对
																</span>
						                                	</a>
					                                	</li>
					                                	<li>
					                                		<a href="#">
					                                			<span class='ques-answer' style="display: inline;" 
					                                				onclick="javascript:s_select(this)">
					                                				<img class='select' isRight="${quesResult.isRight==0?1:0}" src="${pageContext.request.contextPath}/resource/learning/images/ks_06.jpg"/>
																	${answItem_no[1]}.&emsp;错
																</span>
					                                		</a>
					                                	</li>
					                                	<li style="width: 100%;float: left;margin: 10px 0px 20px 0px;display: none;" name="referDiv">
						                                	<a href="#">
					                                			<font color="green" style="border: 1px dashed green;padding: 2px 10px 2px 10px;">
					                                				参考答案:
				                                						<font style="margin-right: 5px;">
				                                							${quesResult.isRight==0?'错':'对'}
			                                							</font>
					                                			</font>
						                                	</a>
					                                	</li>
					                                </ul>
					                        	</div>
			                            		<div style="clear:both;"></div>
			                            	</s:iterator>
			                        	</div>
			                        </div>
								</s:if>
							</s:iterator>
							<s:iterator value="#request.list" var="va">
								<s:if test="#va.quesTypeKey == 3 && #va.quesList.size > 0">
									<div class="eaxm_title dx" style="margin-bottom: 15px;">
			                        	${va.quesTypeName}	<!-- 填空 -->
			                        </div>
			                        <div class="exam_con">
			                        	<div class="usbox">
			                            	<s:iterator value="#va.quesList" var="ques" status="index">
						                        <div class="exam_b" id='${ques.quesId}'><!-- 填空  -->
					                            	<span>${index.index+1 }.&nbsp;${ques.quesContent}</span>
					                        	</div>
			                                	<div style="width: 100%;float: left;margin: 10px 0px 20px 0px;display: none;" name="referDiv">
		                                			<font color="green" style="border: 1px dashed green;padding: 2px 10px 2px 10px;">
		                                				参考答案:
		                                				<s:iterator value="#ques.quesResults" var="quesResult">
	                                						<font style="margin-right: 5px;">
	                                							${quesResult.content}
	                               							</font>
		                                				</s:iterator>
		                                			</font>
			                                	</div>
			                            		<div style="clear:both;"></div>
			                            	</s:iterator>
			                        	</div>
			                        </div>
								</s:if>
							</s:iterator>
							<s:iterator value="#request.list" var="va">
								<s:if test="#va.quesTypeKey == 4 && #va.quesList.size > 0">
									<div class="eaxm_title dx" style="margin-bottom: 15px;">
			                        	${va.quesTypeName}	<!-- 问答 -->
			                        </div>
			                        <div class="exam_con">
			                        	<div class="usbox">
			                            	<s:iterator value="#va.quesList" var="ques" status="index">
					                        	<div class="exam_c">
													<span>${ind5.index+1 }.${ques.quesContent}</span>
													<div class="exam_ca">
														<div class="usbox">
															<textarea rows="10" cols="100%" class="text-exam"></textarea>
														</div>
													</div>
												</div>
			                                	<div style="width: 100%;float: left;margin: 10px 0px 20px 0px;display: none;" name="referDiv">
		                                			<font color="green" style="border: 1px dashed green;padding: 2px 10px 2px 10px;">
		                                				参考答案:
                                						<font style="margin-right: 5px;">
                                							${ques.resultRefer}
                               							</font>
		                                			</font>
			                                	</div>
			                            		<div style="clear:both;"></div>
			                            	</s:iterator>
			                        	</div>
			                        </div>
								</s:if>
							</s:iterator>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="exam-cover" class="exam-cover">
			<!-- 遮罩层 -->
			<div class="examtop_right flc">
				<a href='javascript:void(0);' id="start_exam" onclick="startExam()">开始测试</a>
			</div>
		</div>
	</body>
</html>
