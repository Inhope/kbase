<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程学习</title>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/course.css" type="text/css"></link>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		
		
		<style type="text/css">
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/course.js"></script>
		
		
		<script type="text/javascript">
			var swfAddress='${swfAddress}';
			
			$(function(){
				
				//显示确认学习按钮
				$('.m-chapterList').on('mouseover', '.section', function(){
					if($(this).find("div:eq(1)").attr('_filestatus')=='0'){
						$(this).find(".ksjbtn").css("display", "block");
						$(this).find(".ksstatus").css("display", "none");
					}
				});
				
				//隐藏确认学习按钮
				$('.m-chapterList').on('mouseout', '.section', function(){
					$(this).css("background", "");
					$(this).find(".ksjbtn").css("display", "none");
					$(this).find(".ksstatus").css("display", "block");
				});
				
				
				//点击课程附件章节名称开始学习
				$('.m-chapterList .section').each(function(){
					$(this).find(".course-title").click(function(){
						var _filename = $(this).attr("_filename");
						var _realname = $(this).attr("_realname");
						/*
						var fileId = $(this).attr("_fileid");
						var fileType = $(this).attr("_filetype");
						var videoType = ["mp4", "flv", "swf", "mov", "m4v", "f4v"];
						
						if(videoType.contains(fileType)){
							$.layer({
								type : 2,
								border : [ 2, 0.3, '#000' ],
								title : [ _realname, 'font-size:14px;font-weight:bold;' ],
								shade: [0.3, '#000'],
								closeBtn : [ 0, true ],
								offset : ["5px", "25%"],
								iframe : {
									src : $.fn.getRootPath() + '/app/learning/learn!learnLesson.htm?attachId='+fileId
								},
								area: ['870px', '630px']
							});
							
						}else{
						    $.layer({
								    type: 2,
								    border: [2, 0.3, '#000'],
								    title: [ _realname, 'font-size:14px;font-weight:bold;' ],
								    closeBtn: [0, true],
								    offset : ["5px", "15%"],
								    iframe: {src : swfAddress + '/attachment/preview.do?fileName='+_filename},
								    area: ['1000px', '600px']
								});
							
						}
						*/
						
						$.layer({
						    type: 2,
						    border: [2, 0.3, '#000'],
						    title: [ _realname, 'font-size:14px;font-weight:bold;' ],
						    closeBtn: [0, true],
						    //offset : ["5px", "15%"],
						    iframe: {src : swfAddress + '/attachment/preview.do?fileName='+_filename},
						    area: ['100%', '100%']
						});
								
					});
				});	
				
				//点击确认学习按钮，表明该章节已经学习过了
				$('.m-chapterList .sureLearn').each(function(index, item){
					$(this).click(function(){
						var _item = $(this).next();
						if($(_item).attr("_filestatus")=="0"){
							var fileId = $(this).attr("_fileid");
							var courseId = $('#courseId').val();
							
							$.post($.fn.getRootPath() + '/app/learning/learn!sureLearned.htm', 
								{'courseId': courseId, 'attachId': fileId},
								function(data){
									if(data.rst == '1'){
										$(_item).attr("_filestatus", "1");
										$(_item).html("已学习");
										$(_item).removeClass('mu_wwc');
										$(_item).addClass('mu_ww mu_yx');
										
										$('.class_progress ul li:eq('+index+')').removeClass('progress_b');
										$('.class_progress ul li:eq('+index+')').addClass('progress_a');
										$('.learn-rate').text(data.learnedCount+"/"+data.fileTotal);
									}
							}, "json");
							
						}else{
							parent.layer.alert("已经学习过啦！！", -1);
						}
						
					});
				});	
				
				
				
				
				
				//课件评论
				$('.course_comment').click(function(){
					//课程评论前先检查是否学习完该课程
					var courseId = $('#courseId').val();
					$.post($.fn.getRootPath() + '/app/learning/learn!checkLearnedOver.htm', 
						{'courseId': courseId},
						function(data){
							if(data.rst == '1'){
								if(data.status){
									//检验是否评论过该课程
									checkComment();
								}else{
									parent.layer.alert("您还未完成该课程的学习，不能发表评论", -1);
								}
							}else{
								parent.layer.alert("系统异常，请稍后再试", -1);
							}
					}, "json");
					
				});	
				
				//根据星星的数量改变综合分数
				$(".score").change(function() {
					var selectedValue = $('input:radio[name="score"]:checked').val();
					$("#comment_score").text(selectedValue+".0");
				});
				
				//课件评论保存
				$("#saveComment").click(function(){
					var courseId = $('#courseId').val();
					var score = $('input:radio[name="score"]:checked').val();//星数、分数，分别用1-5表示
					var content = $('#comment_txt').val();
					
					if($.trim(content) == ''){
						parent.layer.alert("请填写评论内容", -1);
					}else{
						$.post($.fn.getRootPath() + '/app/learning/learn!comment.htm', 
							{'courseId': courseId, 'score': score, 'content': content},
							function(data){
								if(data.rst == '1'){
									parent.layer.alert("评论成功", -1);
									layer.close(__kbs_layer_comment);
								}else if(data.rst == '2'){
									parent.layer.alert(data.msg, -1);
								}else{
									parent.layer.alert("系统异常，请稍后再试", -1);
								}
						}, "json");
					}
					
				});
				
				//点击课程自测题
				$('#course_ques_btn').click(function(){
					var courseId = $('#courseId').val();
					var courseName = $('#courseName').val();
					
					$.post($.fn.getRootPath() + '/app/learning/learn!checkCourseQues.htm', 
						{'courseId': courseId},
						function(data){
							if(data.rst == '1'){
								if(data.status){
									parent.TABOBJECT.open({
										id : "courseExam",
										name : "课后自测",
										title : "课后自测",
										hasClose : true,
										url : $.fn.getRootPath()+"/app/learning/learn!toQues.htm?courseId="+courseId+"&courseName="+courseName,
										isRefresh : true
									}, this);
								}else{
									parent.layer.alert("该课程没有课后自测试题", -1);
								}
							}else{
								parent.layer.alert("系统异常，请稍后再试", -1);
							}
					}, "json");
					
				});	
				
				
			});
			
			
			
			//课程评论过，就不能再评论了
			function checkComment(){
				var courseId = $('#courseId').val();
				$.post($.fn.getRootPath() + '/app/learning/learn!checkComment.htm', 
					{'courseId': courseId},
					function(data){
						if(data.rst == '1'){
							if(!data.status){
								window.__kbs_layer_comment = $.layer({
									type: 1,
									title: '课程评论',
								    area: ['600px', '450px'],
								    border: [2, 0.3, '#000'], //去掉默认边框
								    shade: [0.5, '#666'], //去掉遮罩
								    shift: '2', 
								    page: {
								        dom: '#comment_cont'
								    }
								});
							}else{
								parent.layer.alert("您已经评论过了，不能重复评论", -1);
							}
						}else{
							parent.layer.alert("系统异常，请稍后再试", -1);
						}
				}, "json");
			}
			
			
			
				
				
				
		</script>
	</head>
	<body>
		<input type="hidden" id="courseId" value="${course.id}">
		<input type="hidden" id="courseName" value="${course.name}">
		
		
		<div class="page_all">
	        <div class="class_top">
	            <div class="class_left_a fl">
	                 <s:if test="#request.course.fileName==null">
						<img src="${pageContext.request.contextPath}/resource/learning/images/pxks_03.jpg" alt=""/> 
					</s:if>
					<s:else>
						<img src="${pageContext.request.contextPath }/doc/course/cover/${course.fileName}" alt="" width="320px" height="200px"/> 
					</s:else>
	            </div>
	            <div class="class_right fl">
	               <h1>${course.name }</h1>
	               <div class="class_comment">
		           		<a href="javascript:void(0);" class="course_comment"><img src="${pageContext.request.contextPath}/resource/learning/images/kcxx_03.jpg" alt=""/> 评论</a>
		           		
						<div class="class_progress">
							<ul>
								<s:if test="#request.course.courseFile!=null && #request.course.courseFile.size>0">
									<s:iterator value="course.courseFile" var="va" begin="0" end="#request.course.courseFile.size">
									
										<s:set var="learnedFileArr" value="#request.courseLearn.learnedFile"></s:set>
										<s:if test="#learnedFileArr.indexOf(#va.id)>-1">
											<li class="progress_a">
												<span></span>
											</li>
										</s:if>
										<s:else>
											<li class="progress_b">
												<span></span>
											</li>
										</s:else>	
										
									</s:iterator>									
								</s:if> 
								<!-- 
								<li class="learn-rate">
									${learnedCount}/${fileTotal}
								</li>
								 -->
							</ul>
							<br/><br/><br/>
							<div class="progress_zi">
								您当前的学习进度
								<span class="learn-rate">
									<s:if test="#request.fileTotal == 0">
										<span style="font-size:2em;">100%</span>
									</s:if>
									<s:else>
										${learnedCount}/${fileTotal}
									</s:else>
								</span>
							</div>
						</div>
	
				   </div>
		        </div>
		        <div style="clear:both;"></div>
		         
		        <div class="px_b_kc">
		           <div class="px_b_box">
		               
		           </div>
		        </div>        
	    	</div>
	    	
	        <div class="px_b_kc">
	           <div class="px_b_box">
	              <div class="kc_ml">
		               <div class="kc_red"> 课程简介</div>
		               <!-- ${contentStr } -->
               			${course.content }<br><br>
		
		               <div class="kc_red"> 目录</div>
						<ul class="kc_mu m-chapterList">
							<s:if test="#request.course.courseFile!=null && #request.course.courseFile.size>0">
								<s:iterator value="course.courseFile" var="va" status="status">
								
									<li class="section">
										<a href="#">
											<div class="mu_wwc fl ksjbtn sureLearn" _fileid="${va.id }" style="display: none; color: #eaeaea; background-color:red;">确认学习</div>
															
											<s:set var="learnedFileArr" value="#request.courseLearn.learnedFile"></s:set>
											<s:if test="#learnedFileArr.indexOf(#va.id)>-1">
												<div class="mu_ww fl mu_yx ksstatus" _filestatus="1">已学习</div>
											</s:if>
											<s:else>
												<div class="mu_wwc fl ksstatus" _filestatus="0">未学习</div>
											</s:else>	
											
											<span class="course-title" _fileid="${va.id }" _filetype="${va.fileType }" _realname="${va.realName }" _filename="${va.fileName }">第${va.order+1 }节&emsp;${va.realName }</span>
											
										</a>
										<div class="mu_con">
											${va.remark }
										</div>
									</li>
									
								</s:iterator>									
							</s:if> 
							<s:else>
								 没有附件<br><br>
							</s:else>
							
						</ul>
						
						 <div style="width:256px; margin:0 auto; padding-bottom:20px;">
						 	<a href="javascript:void(0);" id="course_ques_btn">
						 		<img src="${pageContext.request.contextPath}/resource/learning/images/kcxx_10.jpg"  alt=""/>
						 	</a> 
						 </div>
	              </div>
	           </div>
	        </div>
	        
	  </div>
	  
	  
			
			<!-- -----------评论start----------- -->
			<div id="comment_cont" class="page_com" style="display: none;">
				<div class="com_usbox">
					<div class="class_contitle" style="border: none;">
						<div class="fl class_l">
							<h1 id="comment_score">
								1.0
							</h1>
							<h2>
								评分
							</h2>
						</div>
						<div class="fl class_r">
							<div id="starBg" class="star_bg">  
							    <input type="radio" id="starScore1" class="score score_1" value="1" name="score" checked="checked">
							    <a href="#starScore1" class="star star_1" title="1分"><label for="starScore1">1分</label></a>
							    <input type="radio" id="starScore2" class="score score_2" value="2" name="score">
							    <a href="#starScore2" class="star star_2" title="2分"><label for="starScore2">2分</label></a>
							    <input type="radio" id="starScore3" class="score score_3" value="3" name="score">
							    <a href="#starScore3" class="star star_3" title="3分"><label for="starScore3">3分</label></a>
							    <input type="radio" id="starScore4" class="score score_4" value="4" name="score">
							    <a href="#starScore4" class="star star_4" title="4分"><label for="starScore4">4分</label></a>
							    <input type="radio" id="starScore5" class="score score_5" value="5" name="score">
							    <a href="#5" class="star star_5" title="5分"><label for="starScore5">5分</label></a>
							</div>
						</div>
					</div>
					<div style="clear: both;"></div>
					<div class="com_text">
						<textarea id="comment_txt" rows="5" cols="100%" style="width: 523px; height: 230px;" class="text-exam"></textarea>
					</div>
					<div class="btn_com">
						<ul>
							<li class="ts">
								<a id="saveComment" href="javascript:void(0);">提交评论</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="javascript:layer.close(__kbs_layer_comment);">取消</a>
							</li>
						</ul>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<!-- -----------评论end----------- -->





	</body>
</html>
