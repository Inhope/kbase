<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>我收藏的课程</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		
		
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/course-collect!myCollectCourses.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}	
			
			//查询条件初始化
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
				});
			}			
			
			
			$(function(){	
			    
				//取消收藏
			    $('[name="course_collect"]').click(function(){
					var id = $(this).attr('_id');
			       
			        var url = $.fn.getRootPath() + '/app/learning/course-collect!cancelCollect.htm';
					
					layer.confirm("确定要取消收藏此课程吗？", function(){
						var loadindex = layer.load('提交中…');
						$.post(url, {"id" : id},
							function(data){
								layer.alert(data.msg, -1, function(){
									layer.close(loadindex);
									if (data.rst) pageClick('1');
								});
						}, "json");
					});
			       
			    });
			    
			    
			    
			    //点击课程名称
			    $('.course-nmae').each(function(){
					$(this).click(function(){
						var courseId = $(this).attr("_id");
						parent.parent.TABOBJECT.open({
							id : 'courseInfo',
							name : '课程学习',
							hasClose : true,
							url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+courseId,
							isRefresh : true
						}, this);
					});
				});
			    
			    
			});
			
			
		</script>
	</head>
		<body>
		
			<form id="form0" action="" method="post">
				<div class="content_right_bottom" style="min-width:860px;">
				
					<div class="yonghu_titile">
						<ul>
							<li>
								课程名称：
								<input type="text" id="name_select" name="courseName" value="${courseName }" style="width: 350px;"/>
							</li>
							<li>
								课程创建人：
								<input id="createUserNameCN_select"  name="createUserNameCN" type="text" value="${createUserNameCN }"/>
							</li>
							
							<li class="anniu">
								<a href="javascript:void(0)"><input type="button"
									class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
								<a href="javascript:void(0)"><input type="button"
									class="youghu_aa2" value="查询" onclick="javascript:pageClick('1');" />
								</a>
							</li>
						</ul>
					</div>
				
					
					<div class="gonggao_con">
						<div class="gonggao_con_nr">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr class="tdbg">
									<td width="75%">
										课程名称
									</td>
									<td width="15%">
										课程创建人
									</td>
									<td width="10%">
										操作
									</td>
								</tr>
								
								<s:iterator value="page.result" var="va">
									<tr>
										<td>
											<span _id="${va.course.id}" class="course-nmae" style="cursor:pointer; color:#434343;">${va.course.name}</span>
										</td>
										<td>
											${va.course.createUserNameCN}
										</td>
										<td>
											<span _id="${va.id}" name="course_collect" style="cursor:pointer; color:blue;">取消收藏</span>
										</td>
									</tr>
								</s:iterator>
								<tr class="trfen">
									<td colspan="3">
										<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</form>
	
			
		
			
	</body>
</html>

