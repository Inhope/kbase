<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程推荐</title>
		<style type="text/css">
			
			.learn-btn{ 
			 	line-height:30px;
				height:30px;
				width:80px;
				color:#ffffff;
				background-color:#db2c06;
				font-size:20px;
				font-weight:bold;
				font-family:SimHei;
				border:1px solid #dcdcdc;
				-webkit-border-top-left-radius:3px;
				-moz-border-radius-topleft:3px;
				border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-moz-border-radius-topright:3px;
				border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-moz-border-radius-bottomleft:3px;
				border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;
				-moz-border-radius-bottomright:3px;
				border-bottom-right-radius:3px;
				-moz-box-shadow: inset 0px 0px 0px 0px #ffffff;
				-webkit-box-shadow: inset 0px 0px 0px 0px #ffffff;
				box-shadow: inset 0px 0px 0px 0px #ffffff;
				text-align:center;
				display:inline-block;
				text-decoration:none;
				margin-left: 370px;
			}
			.learn-btn:hover {
				background-color:#34a519;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				
				/*
				推荐给哪些人---按部门选择用户（多选）[带搜索]
				*/
				$('#userByDeptNames_select').click(function(){
					$.kbase.picker.multiUserByDeptS({returnField:"userByDeptNames_select|userByDeptIds_select"});
				});
				
				//清空推荐范围
				$('#btnClearUser').click(function(){
					$('#userByDeptIds_select').val('');
					$('#userByDeptNames_select').val('');
				});
				
				
				
				$('#recommendbtn').click(function(){
					var courseId = $('#courseId').val();
					var presenteeId = $('#userByDeptIds_select').val();
					
					if($.trim(presenteeId) == ''){
						parent.layer.alert("请选择推荐范围", -1);
					}
					$.post($.fn.getRootPath() + '/app/learning/learn!recommend.htm', 
							{'courseId': courseId, 'presenteeId': presenteeId},
							function(data){
								if(data.rst == '1'){
									layer.alert(data.msg, -1, function(){
										parent.layer.close(parent.__kbs_layer_recommend);
									});
								}else{
									parent.layer.alert("推荐出错", -1);
								}
						}, "json");
					
				});
				
				
			});
				
				
				
		</script>
	</head>
	<body>
		<div style="1px solid #c0c0c0; margin: 20px 20px;">
			<input type="hidden" id="courseId" value="${courseId}">
		
			<input type="hidden" id="userByDeptIds_select" name="userByDeptIds"/>&nbsp;
			推荐范围：<input type="text" id="userByDeptNames_select" name="userByDeptNames" readonly="readonly" style="1px solid #c0c0c0; width: 300px;"/>&nbsp;
			<input type="button" value="清空" id="btnClearUser">
			
			<br/><br/>
			<a href='javascript:void(0);' id="recommendbtn" class='learn-btn'>推荐</a>
			
		
		</div>
		
	</body>
</html>
