<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>我的课程</title>
		<style type="text/css">
			.course_con_title li{list-style-type:none; float:left;} 
			.course_con_title{ position:relative; z-index:1; height:auto;}
			.course_con_title li a{_width:80px; padding:0px 50px; height:35px; background-color:#f5f5f5; display:block; line-height:35px; border:#dddddd solid 1px; border-left:none; color: #080808; text-decoration: none;}
			.course_con_title li:first-child a { border-left:#dddddd solid 1px; }
			.course_con_title li a:hover{ background-color:#FFFFFF; border-bottom:#ffffff solid 1px;}
			.course_con_title .focus a{background-color:#FFFFFF; border-bottom:#ffffff solid 1px;color:#525252;}
			
			
			.course_item{width: 97%; height: auto; min-height:500px; float: left; margin-left: 40px; margin-top: -2px; border: 1px solid #dddddd; overflow-y: scroll;}
			
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		
		<script type="text/javascript">
			$(function(){
				
				$('.course_con_title li a').click(function(){
					var _type = $(this).attr("_type");
					var isSelected = $(this).parent().attr('class');
					
					if (isSelected == 'focus') {
						return false;//已被选择的tab，重复点击不再发送请求
					}else{
						var url = '';
						if(_type == '1'){
							url = $.fn.getRootPath() + '/app/learning/course-learn!myLearnedCourses.htm';
						}else if(_type == '2'){
							url = $.fn.getRootPath() + '/app/learning/course-learn!myLearningCourses.htm';
						}else if(_type == '3'){
							url = $.fn.getRootPath() + '/app/learning/course-collect!myCollectCourses.htm';
						}
						$('#course_frame').attr("src", url);
						
						$('.course_con_title li').removeClass('focus');
						$(this).parent().attr('class', "focus");
					}
					
				});	
				
				//页面初始化时显示完成课程Tab
				$('#learned_tab').click();
				
			});
			
			
				
				
				
		</script>
	</head>
	<body>
		<input type="hidden" id="courseId" value="${course.id}">
		
		<div class="main_course" style="width: 99%; height: 100%;">
			
					<ul class="course_con_title">
						<li>
							<a href="javascript:void(0);" id="learned_tab" _type="1">完成课程</a>
						</li>
						<li>
							<a href="javascript:void(0);" id="learning_tab" _type="2">学习课程</a>
						</li>
						<li>
							<a href="javascript:void(0);" id="collect_tab" _type="3">收藏课程</a>
						</li>
					</ul>
					
					<div id="course_item" class="course_item">
						<!-- tab内容显示区 -->
						<iframe id="course_frame" src=""  frameborder="0" scrolling="no" style="width: 100%; height: 600px;" /></iframe>
					</div>
					
				
				
		
			</div>
		
	</body>
</html>
