<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程学习</title>
		
		<style type="text/css">
			.fpa{display: block; width: 852px; height: 575px;}
			
		
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/flowplayer/flowplayer-3.2.11.min.js"></script>
		
		<script type="text/javascript">
			$(function(){
				$f("player", "${pageContext.request.contextPath }/library/flowplayer/flowplayer-3.2.12.swf", {
				    clip:  { 
				        autoPlay: false,       //是否自动播放，默认true
				        autoBuffering: true     //是否自动缓冲视频，默认true
				    },
				    onLoad: function() {    // 当播放器载入完成时调用
				        this.setVolume(30);    // 设置音量0-100，默认50
				    }  
				}); 
				
			});
				
		</script>
	</head>
	<body>
		<!-- 
		<a id="player" class="fpa" href="${pageContext.request.contextPath }/doc/course/${courseFile.fileName}"></a>
		 -->
		<a id="player" class="fpa" href="http://172.16.8.165:8080/kbase-converter/DATAS/swf/${courseFile.fileName}"></a>
	</body>
	
	
	
	
	
	
	
	
	<!-- jplyer
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>课程学习</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jPlayer-2.9.1/skin/pink.flag/jplayer.pink.flag.css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jPlayer-2.9.1/dist/jplayer/jquery.jplayer.min.js"></script>
		
		
		<script type="text/javascript">
			$(function(){
				
				$("#jquery_jplayer_1").jPlayer({
					ready: function () {
						$(this).jPlayer("setMedia", {
							m4v: "${pageContext.request.contextPath }/doc/course/${courseFile.fileName}",
							webmv: "${pageContext.request.contextPath }/doc/course/${courseFile.fileName}",
							flv: "${pageContext.request.contextPath }/doc/course/${courseFile.fileName}",
							poster: "${pageContext.request.contextPath}/resource/learning/images/pxks_03.jpg"
						});
					},
					swfPath: "../../dist/jplayer",
					supplied: "m4v, webmv, flv",
					size: {
						width: "640px",
						height: "360px",
						cssClass: "jp-video-360p"
					},
					useStateClassSkin: true,
					autoBlur: false,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
				});
				
			});
				
				
		</script>
	</head>
	<body>

		<div id="jp_container_1" class="jp-video jp-video-360p" role="application" aria-label="media player" style="margin-left: 100px;">
			<div class="jp-type-single">
				<div id="jquery_jplayer_1" class="jp-jplayer"></div>
				<div class="jp-gui">
					<div class="jp-video-play">
						<button class="jp-video-play-icon" role="button" tabindex="0">
							play
						</button>
					</div>
					<div class="jp-interface">
						<div class="jp-progress">
							<div class="jp-seek-bar">
								<div class="jp-play-bar"></div>
							</div>
						</div>
						<div class="jp-current-time" role="timer" aria-label="time">
							&nbsp;
						</div>
						<div class="jp-duration" role="timer" aria-label="duration">
							&nbsp;
						</div>
						<div class="jp-controls-holder">
							<div class="jp-controls">
								<button class="jp-play" role="button" tabindex="0">
									play
								</button>
								<button class="jp-stop" role="button" tabindex="0">
									stop
								</button>
							</div>
							<div class="jp-volume-controls">
								<button class="jp-mute" role="button" tabindex="0">
									mute
								</button>
								<button class="jp-volume-max" role="button" tabindex="0">
									max volume
								</button>
								<div class="jp-volume-bar">
									<div class="jp-volume-bar-value"></div>
								</div>
							</div>
							<div class="jp-toggles">
								<button class="jp-repeat" role="button" tabindex="0">
									repeat
								</button>
								<button class="jp-full-screen" role="button" tabindex="0">
									full screen
								</button>
							</div>
						</div>
						<div class="jp-details">
							<div class="jp-title" aria-label="title">
								&nbsp;
							</div>
						</div>
					</div>
				</div>
				<div class="jp-no-solution">
					<span>Update Required</span> 
					请更新您的浏览器到最新版或更新您的<a href="http://get.adobe.com/flashplayer/" target="_blank">Flash 插件</a>.
				</div>
			</div>
		</div>


	</body>
	 -->
	
</html>
