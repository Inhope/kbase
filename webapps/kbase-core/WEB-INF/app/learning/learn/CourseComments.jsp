<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>课程评论</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		
		
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/course-comment!courseComments.htm?courseId="+$('#courseId').val());
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}	
			
		</script>
	</head>
		<body>
		
			<form id="form0" action="" method="post">
				<input type="hidden" id="courseId" value="${course.id}">

				<div class="class_conn">
					<div class="usbox">
						<div class="class_contitle">
							<div class="fl class_l">
								<h1>
									${course.score}
								</h1>
								<h2>
									综合评分
								</h2>
							</div>
							<div class="fl class_r">
							 	<s:set var="avgScore" value="course.score"></s:set>
								<s:if test="#avgScore >= 5">
									<div class="class_star5"></div>
								</s:if>
								<s:elseif test="#avgScore >= 4 && #avgScore < 5">
									<div class="class_star4"></div>
								</s:elseif>
								<s:elseif test="#avgScore >= 3 && #avgScore < 4">
									<div class="class_star3"></div>
								</s:elseif>
								<s:elseif test="#avgScore >= 2 && #avgScore < 3">
									<div class="class_star2"></div>
								</s:elseif>
								<s:else>
									<div class="class_star1"></div>
								</s:else>
								
							</div>
						</div>
						<div style="clear: both;"></div>
						<div class="class_comment">
							<s:iterator value="page.result" var="va">
								<div class="comment_list">
									<ul>
										<li>
											<span>${va.commentUserNameCN}</span>
											<s:if test="#va.score <= 1">
												<div class="px_l_star1"></div>
											</s:if>
											<s:else>
												<div class="px_l_star${va.score}"></div>
											</s:else>
											
										</li>
										<li>
											${va.content}
										</li>
										<li class="comment_time">
											<span> <s:date name="#va.createDate" format="yyyy-MM-dd HH:mm:ss" /> </span>
										</li>
									</ul>
								</div>
							</s:iterator>
							
						</div>
					</div>
				</div>


				<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
								
			</form>
	
			
		
			
	</body>
</html>

