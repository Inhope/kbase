<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>培训管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/course.js"></script>
		
		<script type="text/javascript">
			parent.$('body').ajaxLoadEnd();
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				parent.$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/train!listPage.htm");
				if(pageNo != undefined){
					$("#pageNo").val(pageNo);
				}
				$("#form0").submit();
			}				
			
			//查询条件初始化
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
				});
				$("#publishType").val('');
				$("#exprieTrain_select").removeAttr("checked");
			}
			
			//新增或修改培训
			function addOrEditTrain(idAttr){
		        var url, bl = false, title;
				if (idAttr == "trainAdd") {
					url = "/app/learning/train!addOrEditTo.htm";
					title = "新增培训";
				} else if (idAttr == "trainEdit") {
					url = "/app/learning/train!addOrEditTo.htm";
					title = "编辑培训";
					bl = true;
				} else {
					return;
				}
			
				if (bl) {
					var trainId_list = $("input:checkbox[name='trainId_list']:checked")
					if (trainId_list.length == 0) {
						layer.alert("请选择要操作的数据!", -1);
						return;
					} else if (trainId_list.length > 1) {
						layer.alert("不能同时操作多条数据!", -1);
						return;
					} else {
						var trainId = $(trainId_list[0]).val();
						url += '?trainId=' + trainId;
					}
				}
				
				parent.__kbs_layer_index = parent.$.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ title, 'font-size:14px;font-weight:bold;' ],
					shade: [0.3, '#000'],
					closeBtn : [ 0, true ],
					offset : ["0px", "200px"],
					iframe : {
						src : $.fn.getRootPath() + url
					},
					area: ['720px', '400px'],
					end:function(){
						pageClick();
					}
				});
		       
			}
			
			//培训删除、启用、停用
			function updateStatus(idAttr){
		        var url, bl = false, title, parameters = {}, msgExt = '';
				if (idAttr == "trainDel") {
					url = "/app/learning/train!delTrain.htm";
					title = "删除";
				} else if (idAttr == "editStatus0") {
					url = "/app/learning/train!statusSet.htm";
					title = "启用";
					parameters.status = 0;
				} else if (idAttr == "editStatus1") {
					url = "/app/learning/train!statusSet.htm";
					title = "停用";
					parameters.status = 1;
				} else {
					return false;
				}
				
				var trainIds = "";
				$("input:checkbox[name='trainId_list']:checked").each(function(){
					bl = true;
					trainIds += "," + $(this).val();
				});
				if(!bl){
					layer.alert("请至少选择一条数据!", -1);
					return;
				}
				parameters.trainIds = trainIds.substring(1);
				
				if (bl){
						parent.layer_confirm = parent.layer.confirm("确定要" + title + "选中的数据吗？", function(){
							parent.layer.close(parent.layer_confirm);
							//var loadindex = layer.load('提交中…');
							$.post($.fn.getRootPath() + url, parameters,
								function(data){
									parent.layer_confirm = parent.layer.alert(data.msg, -1, function(){
										parent.layer.close(parent.layer_confirm);
										if (data.rst) pageClick();
									});
							}, "json");
						});
					}
			}
			
			$(function(){	
			    //移动培训到选择的分类
			    $('#trainCateMove').click(function(){
			       var _trainCateName = $("#trainCateNames_select").val();
			       if($.trim(_trainCateName)==''){
			       		parent.layer.alert("请选择分类!", -1);
						return;
			       }else{
			       		var url, bl = false, title, parameters = {}, msgExt = '';
						var trainIds = "";
						$("input:checkbox[name='trainId_list']:checked").each(function(){
							bl = true;
							trainIds += "," + $(this).val();
						});
						if(!bl){
							parent.layer.alert("请至少选择一条数据!", -1);
							return;
						}
						trainIds = trainIds.substring(1);
					
						url = "/app/learning/train!updateCate.htm";
						parameters.trainIds = trainIds;
						parameters.cateId = $("#trainCateIds_select").val();
						
						if (bl){
							layer.confirm("确定要改变选中培训的分类吗？", function(){
								var loadindex = layer.load('提交中…');
								$.post($.fn.getRootPath() + url, parameters,
									function(data){
										layer.alert(data.msg, -1, function(){
											layer.close(loadindex);
											if (data.rst) pageClick();
										});
								}, "json");
							});
						}
			       }
			    });
			    
			    $("#publishType").on("change",function(){
			    	parent.$('body').ajaxLoading('正在查询数据...');
					$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/train!listPage.htm");
					$("#pageNo").val(1);
					$("#form0").submit();
			    });
			    
			    //屏蔽过期（勾选时屏蔽已过期）
			    $('#exprieTrain_select').click(function(){
			    	var exprieTrain = $("input:checkbox[name='exprieTrain']:checked");
			    	if (exprieTrain.length > 0) {
			    		$("input:checkbox[name='exprieTrain']").val("1");
					}else{
						$("input:checkbox[name='exprieTrain']").val("0");
					}
			    	parent.$('body').ajaxLoading('正在查询数据...');
					$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/train!listPage.htm");
					$("#pageNo").val(1);
					$("#form0").submit();
			    });
			    
			    //课程导出
			    $('#trainExp').click(function(){
			    	$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/train!exportTrain.htm");
					$("#form0").submit();
			    });
			    
			    //培训导入step1
				$("#trainImp").on("click",function(){
					
					window.__kbs_layer_trainImp = $.layer({
						type: 1,
						title: '培训导入',
					    area: ['400', '300'],
					    border: [2, 0.3, '#000'], //去掉默认边框
					    shade: [0.5, '#666'], //去掉遮罩
					    shift: '2', 
					    page: {
					        dom: '#trainImportShade'
					    }
					});
					
				});
			    
			    //培训导入step2
			    $("#importTrain").click(function(){
					if($.trim($("#trainFile").val()) == ''){
						parent.layer.alert("请选择需要上传的文件!", -1);
						return false;
					}
					//数据导入
					$.ajaxFileUpload({
						url: $.fn.getRootPath()+"/app/learning/train!importTrain.htm",
						type: 'post',
						secureuri: false, //一般设置为false
						fileElementId: 'trainFile', // 上传文件的id、name属性名
						dataType: 'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){  
							if (data.rst == "1") {
								parent.layer.alert("导入成功", -1);
								$('body').ajaxLoadEnd();
					   			location.reload();
							} else {
								alert(data.msg);
							}
						},
						error: function(data, status, e){ 
							parent.layer.alert("网络错误，请稍后再试!", -1);
						}
					});
					
				});
			
			  //培训分类div中的tree
			  var cateTreeObject = {
					cateEl : $('#trainCateNames_select'),
					ktreeEl : $('div.cateContent'),
					treeAttr : {
						view : {
							expandSpeed: ''
						},
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/learning/train-cate!cateJson.htm",
							autoParam : ["id"]
						},
						callback : {
							onClick : function(event, treeId, treeNode) {
								cateTreeObject.cateEl.val(treeNode.name);
								$('#trainCateIds_select').val(treeNode.id);
							}
						}
					},
					render : function() {
						var self = this;
						self.cateEl.val('');
						self.cateEl.focus(function(e){
							self.ktreeEl.css({
								'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
								'left' : $(this).offset().left + 'px'
							});
							if(self.ktreeEl.is(':hidden'))
								self.ktreeEl.show();
						});
						
						$('*').bind('click', function(e){
							if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.cateEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
								
							} else {
								if(!self.ktreeEl.is(':hidden'))
									self.ktreeEl.hide();
							}
						});
						self.cateEl.bind('keydown', function(keyArg) {
							if(keyArg.keyCode == 8) {
								$(this).val('');
								$("#trainCateIds_select").val('');
							} else if(keyArg.keyCode == 13) {
								self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
							}
						});
						$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
					}
				}
			    cateTreeObject.render();
			    //点击培训率显示详情
			    $("body .gonggao_con_nr").on("click","#trainRate",function(){
			    	var trainId = $(this).attr("_id");
			    	var trainName = $(this).attr("_name");
			    	var url = "/app/learning/train!trainRateUI.htm"+"?id="+trainId;
					parent.$.layer({
						type : 2,
						border : [ 2, 0.3, '#000' ],
						title : [ trainName, 'font-size:14px;font-weight:bold;' ],
						shade: [0.3, '#000'],
						closeBtn : [ 0, true ],
						offset : ["100px", "400px"],
						iframe : {
							src : $.fn.getRootPath() + url
						},
						area: ['350px', '250px']
					});
			    });
			});
			
		</script>
	</head>
		<body>
		<div style="margin-top:0; width:100%;">
			
					<!--******************************************************培训管理初始化页面*********************************************************************************  -->
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="width:100%;">
							<div class="gonggao_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;">
								<div class="gonggao_titile_right" style="margin-right: 10px;">
									<%-- <myTag:input type="a" value="导出" key="trainManageExport" id="trainExp"/>
									<myTag:input type="a" value="导入" key="trainManageImport" id="trainImp"/> --%>
									<myTag:input type="a" value="停用" key="trainManageStop" id="editStatus1" onclick="updateStatus('editStatus1')"/>
									<myTag:input type="a" value="启用" key="trainManageStart" id="editStatus0" onclick="updateStatus('editStatus0')"/>
									<myTag:input type="a" value="删除" key="trainManageDel" id="trainDel" onclick="updateStatus('trainDel')"/>
									<myTag:input type="a" value="编辑" key="trainManageEdit" id="trainEdit" onclick="addOrEditTrain('trainEdit')"/>
									<myTag:input type="a" value="新增" key="trainManageAdd" id="trainAdd" onclick="addOrEditTrain('trainAdd')"/>
									<%-- <s:iterator value="#request.menuList" var="menu">
									<s:if test="#menu.key == 'quesManageMoveCate'">
					          			<a href="javascript:void(0);" id="trainCateMove" style="float: left; margin-right: 0px;">移动分类</a>
										<input type="text" id="trainCateNames_select" name="trainCateNames" onclick="showCate('cateContent','trainCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
											style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
					          			<input type="hidden" id="trainCateIds_select" name="trainCateIds">&nbsp;
									</s:if>
									</s:iterator> --%>
									<kbs:if key="trainManageMoveCate">
					          			<a href="javascript:void(0);" id="trainCateMove" style="float: left; margin-right: 0px;">移动分类</a>
										<input type="text" id="trainCateNames_select" name="trainCateNames" onclick="showCate('cateContent','trainCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
											style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
					          			<input type="hidden" id="trainCateIds_select" name="trainCateIds">&nbsp;
									</kbs:if>
								</div>
							</div>
							<div class="yonghu_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
								<ul>
									<li>
										发布类型
									    <s:select id="publishType" name="trainModel.publishType" value="#request.trainModel.publishType" list="#{null:'-全部-', 0:'手动发布', 1:'自动发布'}"></s:select>
									</li>
									<li>
										培训名称
										<input type="text" id="name_select" name="trainModel.name" value="${trainModel.name }" />
									</li>
									<!-- <li>
										创建人
										<input id="createUserNameCN_select"  name="trainModel.createUserNameCN" type="text" value="${trainModel.createUserNameCN }"/>
									</li>
									<li>
										创建时间
										<input type="text" id="createDate1_select"  name="createDate1" value="${createDate1 }" placeholder="开始日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'createDate2_select\')||\'new Date()\'}'})"/>
										-
										<input type="text" id="createDate2_select"  name="createDate2" value="${createDate2 }" placeholder="结束日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'createDate1_select\')}',maxDate:new Date()})"/>
									</li>
									 -->
									<li>
										培训周期
										<input type="text" id="startTime_select"  name="trainModel.startTime" value="<s:date name='trainModel.startTime' format='yyyy-MM-dd' />" placeholder="开始日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endTime_select\')}'})"/>
										-
										<input type="text" id="endTime_select"  name="trainModel.endTime" value="<s:date name='trainModel.endTime' format='yyyy-MM-dd' />" placeholder="结束日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'startTime_select\')}'})"/>
									</li>
									<li>
										培训状态
									    <s:select id="status_select" name="trainModel.status" value="#request.trainModel.status" headerKey="" headerValue="-全部-" list="#{0:'启用', 1:'停用'}"></s:select>
									</li>
									<li>
										<span style="float:left;">屏蔽过期</span>
										<input type='checkbox' id="exprieTrain_select" name="exprieTrain" value="${exprieTrain }" <s:if test="#request.exprieTrain == 1">checked</s:if> style="width: 20px;float: left;margin-top: 9px;"/>
								 	</li>
									<li class="anniu" style="float: right;margin-right: 10px;">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="查询" onclick="javascript:pageClick('1');" />
										</a>
										<input type="hidden" id="cateIds_select" name="cateIds" value="${cateIds }">&nbsp;
									</li>
								</ul>
							</div>
							
							<div class="gonggao_con" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="3%">
												<input type='checkbox' id="checkAll"/>
											</td>
											<td width="23%">
												培训名称
											</td>
											<td width="7%">
												开始日期
											</td>
											<td width="7%">
												结束日期
											</td>
											<td width="7%">
												培训人数
											</td>
											<td width="8%">
												培训完成率
											</td>
											<td width="5%">
												创建人
											</td>
											<td width="13%">
												培训分类
											</td>
											<td width="7%">
												培训状态
											</td>
											<td width="7%">
												是否过期
											</td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td>
													<input type='checkbox' name="trainId_list" value="${va.id }" />
												</td>
												<td>
													${va.name}
												</td>
												<td>
													<s:date name="#va.startTime" format="yyyy/MM/dd" />&nbsp;
												</td>
												<td>
													<s:date name="#va.endTime" format="yyyy/MM/dd" />&nbsp;
												</td>
												<td>
													${va.totalPerson}
												</td>
												<td>
													<s:if test="#va.totalPerson==0">
														${va.trainRate}%
													</s:if>
													<s:else>
													<span style="cursor:pointer;color:darkblue;" id="trainRate" _id="${va.id }" _name="${va.name}">
														${va.trainRate}%
													</span>
													</s:else>
												</td>
												<td>
													${va.createUserNameCN}
												</td>
												<td>
													${va.trainCate.name}
												</td>
												<td>
													<s:if test="#va.status == 0">启用</s:if>
													<s:if test="#va.status == 1">停用</s:if>
												</td>
												<td>
													<s:if test="#va.expire">已过期</s:if>
													<s:else>未过期</s:else>
												</td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="10">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
	
			
		</div>
		
		
		
		<div id="cateContent" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:190px;"></ul>
		</div>
		
		
		<div id="trainImportShade" style="display:none;">
			<ul>
				<li id="scBefore" style="padding-left: 30px;">
					<p>
						<br />
						注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/learning/train!downloadTrainTemplete.htm?fileName=trainImportMould.xls" style="color: blue">点击下载</a>)填写相关数据
						<br />
						特别提示：
						<br />
						1、请及时下载最新模板
						<br />
						2、上传内容请严格按照模板要求来填写
						<br />
						<br />
					</p>
				</li>
				<li id="importHandle">	
					<p style="float: left;">
						<input type="file" id="trainFile" name="trainFile" />
					</p>
					<span style="float: left;">
						<input id="importTrain" value="上传" type="button">
					</span>
				</li>
			</ul>						
		</div>
		
			
	</body>
</html>

