<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<style>
	.box th{width: 30%;text-align: right;}
	.box td{width: 70%;}
	.box td>input{width: 97%;}
</style>
		
<script type="text/javascript">	
		//去掉前后空格
		String.prototype.trim = function() {return this.replace(/(^\s*)|(\s*$)/g,'');}
			//左侧菜单树
			var setting = {
				view: {
					dblClickExpand: false
				},
				edit: {
					enable: true,
					showRemoveBtn: false,
					showRenameBtn: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},callback: {			
					onClick: zTreeOnClick,
					//onRightClick: zTreeOnRightClick,
					beforeDrop: zTreeBeforeDrop,
					onDrop: zTreeOnDrop
				}				
			};
			/*拖拽前判断重名和不能移成顶级节点*/
			function  zTreeBeforeDrop(treeId, treeNode, targetNode, moveType){
				if(moveType != "inner"){
					layer.alert("不能移动节点跟顶级节点同级!", -1);
					return false;
				}else{
					var parentId = "";
					if(targetNode){
						parentId = targetNode.id;
					}
					var name_cate = treeNode[0].name;
					var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
					if(name_cate != ''){
						var node_ = treeObj.getNodesByFilter(function(node){
							if(node.pId == parentId && node.name == name_cate)return true;
							return false;
						}, true, targetNode[0]);
						if(node_ != null && node_.id != treeNode[0].id){
							layer.alert("同级分类下不应存在相同分类!", -1);
							return false;
						}
					}
				}
			}	
			
			/************节点拖拽操作结束的事件********
			targetNode: 成为 treeNodes 拖拽结束的目标节点 JSON 数据对象。
			moveType: "inner"：成为子节点，"prev"：成为同级前一个节点，"next"：成为同级后一个节点,如果 moveType = null，表明拖拽无效
			**********/
			function  zTreeOnDrop(event, treeId, treeNode, targetNode, moveType){
				//如果拖拽改变了父子级关系，则需要修改数据库数据
				if(moveType === "inner"){
					var parentId = "";
					if(targetNode){
						parentId = targetNode.id;
					}
					$.post($.fn.getRootPath()+"/app/learning/train-cate!dropCate.htm",
						{'id_cate':treeNode[0].id, parentId: parentId},
						function(data){
							if(data.rst != '1'){
								layer.alert("移动失败!", -1);	
							}
						},"json");
				}
			}	
			//为了保证请求刷新列表后树的节点保持展开状态，这里改将培训列表的数据放在iframe中显示
			function zTreeOnClick(event, treeId, treeNode){	
				$('body').ajaxLoading('正在查询数据...');
				//$("#trainpage_frame").attr("src", $.fn.getRootPath()+"/app/learning/train!listPage.htm");
				$(window.frames['trainpage_frame'].document).find("#pageNo").val(1);
				$(window.frames['trainpage_frame'].document).find("#cateIds_select").val(treeNode.id);
				$(window.frames['trainpage_frame'].document).find("#form0").attr("action", $.fn.getRootPath()+"/app/learning/train!listPage.htm");
				$(window.frames['trainpage_frame'].document).find("#form0").submit();
				$('body').ajaxLoadEnd();
			}
			
			//分类重置
			function resetCate(status, node){
				$("input[id$='_cate']").each(function (){
					$(this).attr("value","");
				});				
				$("textarea[id$='_cate']").each(function (){
					$(this).attr("value","");
				});			
				$("#name_pcate").unbind("click");
				if(status=="edit"){
					//编辑赋值
					if(node.pId!=null){
						//分类名称被修改后，子分类的父分类显示的还是以前的名称
						var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
						var pNode = treeObj.getNodeByParam("id",node.pId,null);
						$("#id_pcate").attr("value",pNode.id);	
						$("#name_pcate").text(pNode.name);
					}else{
						$("#id_pcate").attr("value","");
						$("#name_pcate").text("");
					}
					//绑定点击事件
					$("#name_pcate").click(function (){ showMenu1();});	
					$("#id_cate").attr("value",node.id!=null?node.id:'');
					$("#name_cate").attr("value",node.name!=null?node.name:'');
					$("#remark_cate").attr("value",node.remark!=null?node.remark:'');						
					//标题显示
					$("#title0").html("<b>分类编辑</b>");
					//显示删除按钮
					$("delCate").css("display","block");
				}else{
					//新建赋值
					$("#id_pcate").attr("value",node.id!=null?node.id:'');
					$("#name_pcate").text(node.name!=null?node.name:'');
					//标题显示
					$("#title0").html("<b>分类新增</b>");	
					//隐藏删除按钮
					$("delCate").css("display","none");
				}		
			}
			//分类保存
			function saveCate(status,node){
				var id_cate = $("#id_cate").val();
				var name_cate = $("#name_cate").val().trim();
				var id_pcate = $("#id_pcate").val();
				//判断同级分类是否有重复的分类
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				if(name_cate != ''){
					var node_p = treeObj.getNodeByParam("id", id_pcate, null);
					var node_ = treeObj.getNodesByFilter(function(node){
						if(node.pId == id_pcate && node.name == name_cate)return true;
						return false;
					}, true, node_p);
					//同名分类与当前需要编辑的分类不是同一个
					if(node_ != null && node_.id != id_cate){
						layer.alert("同级分类下不应存在相同分类!", -1);
						return false;
					}
				}else{
					layer.alert("分类名称不应为空!", -1);
					return false;
				}
				//判断是否是顶级父节点
				id_pcate = id_pcate!='0'?id_pcate:'';
				//保存按钮不可用
				document.getElementById("saveCate").disabled = true;			
				$.post($.fn.getRootPath()+"/app/learning/train-cate!addOrEditCate.htm",
					{
						'id_cate':id_cate,
						'name_cate':name_cate,
						'id_pcate':id_pcate,
						'name_pcate':$("#name_pcate").text(),
						'remark_cate':$("#remark_cate").val()
					},
					function(data){					
						if(data!=null&&data!=''&&data.rst!='0'){
							layer.alert("操作成功!", -1);
							if(status=="edit"){
								var pId = node.pId;
								node.name=data.result.name;							
								node.remark = data.result.remark;
								node.pId = data.result.pId;
								node.pName = data.result.pName;							
								if(pId!=data.result.pId){//父节点发生变化
									//移动节点
									treeObj.moveNode(treeObj.getNodeByParam("id",data.result.pId,null),node, "inner");
								}
								//更新节点
								treeObj.updateNode(node);
							}else{
								//节点新增						
								treeObj.addNodes(treeObj.getNodeByParam("id",data.result.pId,null),data.result);						
							}	
							try{
								layer.close(window.__kbs_layer_index);
							}catch(e){}
						}else{
							layer.alert("保存失败!", -1);
						}
						//保存按钮可用
						document.getElementById("saveCate").disabled = false;	
					},
					"json"
				);
			}
			
			//分类编辑、新建
			function addOrEditCate(status){
				var title;
				if(status == 'add'){
					title = "新增";
				}else if(status == 'edit'){
					title = "修改";
				}	
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var node = treeObj.getSelectedNodes()[0];//当前选中节点				
				if(node==null||node==''){					
					layer.alert("请选择分类节点！", -1);
					return;
				}else{
					//$("#content0").css("display","block");
					window.__kbs_layer_index = $.layer({
						type: 1,
					    title: title,
					    area: ['auto', 'auto'],
						offset: ['100px','400px'],
					    border: [3, 0.3, '#000'], //去掉默认边框
					    shade: [0.3, '#000',true], //去掉遮罩
					    closeBtn: [0, true], //去掉默认关闭按钮
					    //shift: '2', //从左动画弹出
					    page: {
					        dom: '#content0'
					    }
					});
					//信息初始化
					resetCate(status, node);
					//绑定信息重置
					$("#resetCate").unbind("click");
					$("#resetCate").bind("click",function (){
						resetCate(status, node);		
					}); 
					//绑定保存
					$("#saveCate").unbind("click");
					$("#saveCate").bind("click",function (){
						saveCate(status, node);
					});
				}
			}			
			//分类删除
			function delCate(){			
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var node = treeObj.getSelectedNodes()[0];		
				if(node==null){
					layer.alert("请选择分类节点!", -1);
					return;
				}else{
					if(node.id == 0){
						layer.alert("顶级分类节点不可删除!", -1);
						return;
					}else{
						layer.confirm("确定删除"+node.name+"吗？", function(index){
							$.ajax({
								type: "POST",
								url: $.fn.getRootPath()+"/app/learning/train-cate!checkHaveTrain.htm",
								data: "id="+node.id,
								dataType:"json",
								success: function(data) {
									layer.close(index);
									if(data.status == 0){//分类下无培训
										$.post($.fn.getRootPath()+"/app/learning/train-cate!delCate.htm",
										{'id_cate':node.id},
										function(data){
											if(data.rst=='1'){
												layer.alert("删除成功!", -1);
												treeObj.removeNode(node);
											}else{
												layer.alert("删除失败!", -1);														
											}	
											hideRMenu();			
										},"json");
									}else if(data.status == 1){//分类下有培训
										layer.alert(data.message+"，不能删除!", -1);
									}
								}
							});
						});
					}
				}
			}		
			//页面初始化
			$(document).ready(function(){
				$(function(){
					$('#treeDemo').css('height', $(window).height()-60);
				});
				//分类树初始化
				$.post($.fn.getRootPath()+"/app/learning/train-cate!initCateTree.htm",
					function(data){
						//分类树初始化
						$.fn.zTree.init($("#treeDemo"), setting, data);
					},"json");
			});	
		</script>
				<div style="background-color: #f6f6f6;height: 504px;border-bottom: 1px solid #dfdfdf;border-right: 2px solid #dfdfdf;">
					<div id="catePanel" class="easyui-panel" title="" style="padding:5px;border:0;background:#f6f6f6; ">
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnNewCate" onclick="addOrEditCate('add')" >添加</a>  
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnEditCate" onclick="addOrEditCate('edit')" >修改</a>  
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnDelCate" onclick="delCate('edit')" >删除</a>  
					</div>
					<div id="treeDemo" class="ztree" style="margin-top:0; width:260px;"></div>
				</div>
				<div id="content0" style="display: none;">
					<table cellspacing="0" class="box" style="width: 300px;">
						<tr>
							<th>
								父节点:
							</th>
							<td>
								<span id="name_pcate"></span>
								<input type="hidden" id="id_pcate"/>
							</td>
						</tr>
						<tr>
							<th>
								<span style="color: red;">*</span>当前节点:
							</th>
							<td>
								<input type="text" id="name_cate" style="width: 97%;"/>
								<input type="hidden" id="id_cate"/>
							</td>
						</tr>
						<tr>
							<th>
								备注:
							</th>
							<td>
								<textarea id="remark_cate" rows="1" cols="1" style="width: 95%;"></textarea>
							</td>
						</tr>
					</table>
					<br>
					<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
						<span id="err_msg" style="color: red;float: left; margin-left: 5px;margin-top:5px;"></span>
						<input type="button" id="saveCate" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
					</div>
				</div>
