<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>新增或编辑培训</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        
        <style type="text/css">
			.inputflag{
			    color:red;
			    font-size:14px;
			}
		</style>		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript">
			$(function(){
				//课程内容富文本插件
				var editor;			
				KindEditor.ready(function(K) {
					editor = K.create('textarea[id="content"]', {
						width:"99%",height:"150px",
			            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist'] 
					});
				});
				$('.btnSave').click(function(){
					var _this = this;
					var publishType = $("#publishType").val();//发布类型:0-手动，1-自动
					var status = $(_this).attr('_status');//0=保存生效;1=保存不生效
					var trainId = $('#trainId').val();
					var name = $('#name').val();
					var trainType = $('#trainType').val();//培训类型
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					var content = editor.html();
					var cateIds = $('#cateIds_select').val();//培训分类
					var courseIds = $('#courseIds_select').val();//绑定的课程
					var releaseType;
					var scopeIds;
					var scopeNames;
					var publishDays = $("#publishDays").val();
					var totalPerson;
					if(publishType == '0'){
						releaseType = $('#releaseType').val();//发布范围：0-全员，1-指定
						if(releaseType == '0'){
						}else if(releaseType == '1'){
							scopeIds = $('#scopeids').val();//发布对象ids
							scopeNames = $('#scopenames').val();//发布对象names
							totalPerson = $('#totalPerson').val();//发布对象总人数
						}
					}else if(publishType == '1'){
						releaseType = $('#releaseTypeAuto').val();//发布范围：2-部门，3-角色
						if(releaseType == '2'){
							scopeIds = $('#deptIdMulti').val();//发布对象ids
							scopeNames = $('#deptDescMulti').val();//发布对象names
						}else if(releaseType == '3'){
							scopeIds = $('#roleIdSelect').val();
							scopeNames = $('#roleIdSelect').find("option:selected").text();
						}
					}
					if($.trim(name)==''){
						layer.alert('请填写培训名称',-1);
						return;
					}else if ($.trim(trainType)==''){
						layer.alert('请选择培训类型',-1);
						return;
					}else if ($.trim(cateIds)==''){
						layer.alert('请选择培训分类',-1);
						return;
					}else if($.trim(releaseType)==''){
						layer.alert('请选择发布范围',-1);
						return;
					}else if($.trim(releaseType)=='1' && scopeIds==''){
						layer.alert('请选择发布对象',-1);
						return;
					}else if($.trim(releaseType)=='2' && scopeIds==''){
						layer.alert('请选择部门',-1);
						return;
					}else if($.trim(releaseType)=='3' && scopeIds==''){
						layer.alert('请选择角色',-1);
						return;
					}else if(isNaN($('#publishDays').val())){
						layer.alert('培训天数请输入正确数字',-1);
						return;
					}else if(courseIds==''){
						layer.alert('请绑定课程',-1);
						return;
					}else if($.trim(content)==''){
						layer.alert('请填写培训内容',-1);
						return;
					}else{
						var params = {
							id : trainId,
							publishType : publishType,
							status : status,
							name : name,
				   			startTime : startTime,
				   			endTime : endTime,
				   			trainType : trainType,
				   			cateIds : cateIds,
				   			content : content,
				   			courseIds : courseIds,
				   			releaseType : releaseType,
				   			scopeIds : scopeIds,
				   			scopeNames : scopeNames,
				   			totalPerson : totalPerson,
				   			publishDays : publishDays
						};
						var url = $.fn.getRootPath()+"/app/learning/train!addOrEditDo.htm";
						if($.trim(trainId) != ''){
							addOrUpdate(url, params);
						}else{
							$.post($.fn.getRootPath() + '/app/learning/train!checkSameName.htm', 
								{'name': name},
								function(data){
									if(data.rst == '1'){
										if(data.status){
											layer.alert('已经存在同名的培训',-1);
											return;
										}else{
											addOrUpdate(url, params);
										}
									}else{
										alert("系统异常，请稍后再试");
									}
							}, "json");
						}
					} 
				});
				
				//培训名称字数提示信息
				$("#name").keyup(function(){
				    var name = $(this).val().replace(/[\r\n]/g,"");
				    var length = name.length;
				    if(length > 50){
				    	layer.alert('培训名称最多不能超过50个字符',-1);
				        $(this).val(title.substring(0,50))
				        length = 50;
				    }
				    $("#title_lenght_show").text("50/"+length);
				});
				
				//清空开放对象
				$('#btnClearScope').click(function(){
					var _layer_confirm = layer.confirm("确定要清空开放对象吗？", function(){
						$('#scopenames').val('');
						$('#scopeids').val('');
						$('#scopeshow').val('');
						$('#totalPerson').val('');
						layer.close(_layer_confirm);
					});
				});
				
				//清空开放部门对象
				$('#btnClearDeptScope').click(function(){
					var _layer_confirm = layer.confirm("确定要清空开放对象吗？", function(){
						$('#deptDescMulti').val('');
						$('#deptIdMulti').val('');
						layer.close(_layer_confirm);
					});
				});
				
				//培训分类选择(单选)
			    $('#cateNames_select').click(function(){
			       $.kbase.picker.singleTrainCate({returnField:"cateNames_select|cateIds_select"});
			    });
			    
			    //课程绑定选择(单选)
			    $('#courseNames_btn').click(function(){
			       $.kbase.picker.singleCourse({returnField:"courseNames_select|courseIds_select"});
			    });
			    
			    //选择部门（多选）
			    $('#deptDescMulti').click(function(){
					$.kbase.picker.multiDept({returnField:"deptDescMulti|deptIdMulti"});
				});
				
			    //选择考试用户
				$("#scopeshow").click(function(){
					$.kbase.picker.multiDeptusersearch({returnField:"scopenames|scopeids"});
				});
				
			    //发布范围判断
				$("#releaseType").on("change",function(){
					_shadeScopen();
				});
				_shadeScopen = function(){
					if($("#releaseType").val() == '1'){
						$("#releaseScope").show();
					}else if($("#releaseType").val() == '0'){
						$("#releaseScope").hide();
					}
				}
				_shadeScopen();
				
			    //角色、部门判断
				$("#releaseTypeAuto").on("change",function(){
					_shadeDeptRole();
				});
				_shadeDeptRole = function(){
					if($("#releaseTypeAuto").val() == '2'){
						$("#deptSelect").show();
						$("#roleSelect").hide();
					}else if($("#releaseTypeAuto").val() == '3'){
						$("#deptSelect").hide();
						$("#roleSelect").show();
					}
				}
				_shadeDeptRole();
				
			    //发布类型判断
				$("#publishType").on("change",function(){
					_shadeType();
				});
				_shadeType = function(){
					if($("#publishType").val() == '0'){
						$("#releaseTypeArea").show();
						$("#releaseTypeAutoArea").hide();
						$("#deptSelect").hide();
						$("#roleSelect").hide();
						$("#publishDaysAuto").hide();
						_shadeScopen();
					}else{
						$("#releaseTypeAutoArea").show();
						$("#releaseTypeArea").hide();
						$("#releaseScope").hide();
						$("#publishDaysAuto").show();
						_shadeDeptRole();
					}
				}
				_shadeType();
				//点击折叠按钮
			    $("#showOrHideInfo").on("click",function(){
			    	var url = $.fn.getRootPath()+"/resource/learning/images/fold_up.png";
			    	var _this = $("#infoArea");
			    	if(_this.is(":hidden")){
			    		_this.show();
			    		url = $.fn.getRootPath()+"/resource/learning/images/fold_down.png";
			    	}else{
			    		_this.hide();
			    	}
			    	$(this).attr("src",url);
			    });
			});
			
			function addOrUpdate(url, params){
				//提交数据
				$('body').ajaxLoading('正在提交数据...');
				$.ajax({
			   		type: "POST",
			   		url: url,
			   		data: params,
			   		success: function(data){
			   			$('body').ajaxLoadEnd();
			   			data = jQuery.parseJSON(data);
			   			layer.alert(data.msg,-1,function(){
			   				parent.layer.close(parent.__kbs_layer_index);
						});
			   			//parent.location.reload();
			   		},
			   		error: function(msg){
			   			layer.alert(msg,-1);
			   			$('body').ajaxLoadEnd();
			   		}
				});
			}
		</script>
	</head>
	<body>
		<div id="form_main">
		<form id="form1" action="" method="post">
			<input type="hidden" id="trainId" name="trainModel.id" value="${trainModel.id }" />
			<table cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td colspan="3" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
						<div class="background_theme">
						<font class="font_style">基本信息</font>
						<img id="showOrHideInfo" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
							src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
						</div>
					</td>
				</tr>
			</table>
			<table id="infoArea" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td>发布类型<span class="inputflag">*</span></td>
					<td colspan="2">
					    <s:select id="publishType" name="trainModel.publishType" 
					    	list="#{0:'手动发布', 1:'自动发布'}" value="#request.trainModel.publishType"></s:select>
					</td>
				</tr>
				<tr>
					<td width="15%">培训名称<span class="inputflag">*</span></td>
					<td width="75%" style="border-right-style:hidden">
						<input type="text" id="name" name="trainModel.name" value="${trainModel.name }" style="width:550px;" />
					</td>
					<td width="10%" style="border-left-style:hidden" valign="bottom"><span id="title_lenght_show">50/0</span></td>
				</tr>
				<tr>
					<td>开始日期</td>
					<td colspan="2">
						<input type="text" id="startTime" name="trainModel.startTime" readonly="readonly" value="${trainModel.startTime}" style="width:150px;" class="Wdate" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})" /> 
						结束日期
						<input type="text" id="endTime" name="trainModel.endTime" readonly="readonly" value="${trainModel.endTime}" style="width:150px;" class="Wdate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})"/>
						<span style="margin-left:5px;"><font color="red">不填写为永不过期</font></span>
					</td>
				</tr>
				<tr>
					<td>培训类型<span class="inputflag">*</span></td>
					<td colspan="2">
						<s:select id="trainType" name="trainModel.trainType" list="#{1:'必修', 2:'选修'}" key="1"
							listKey="key" listValue="value" value="#request.trainModel.trainType"></s:select>
					</td>
				</tr>
				<tr>
					<td>培训分类<span class="inputflag">*</span></td>
					<td colspan="2">
						<input type="text" id="cateNames_select" name="cateNames" value="${trainModel.trainCate.name}" readOnly="readOnly">
				        <input type="hidden" id="cateIds_select" name="trainModel.trainCate.id" value="${trainModel.trainCate.id}">&nbsp;
					</td>
				</tr>
				<tr id="releaseTypeArea" style="display: none;">
					<td>发布范围<span class="inputflag">*</span></td>
					<td colspan="2">
						<s:select id="releaseType" name="trainModel.releaseType" list="#{0:'全员', 1:'指定'}" headerKey="" headerValue="-请选择-"
							listKey="key" listValue="value" value="#request.trainModel.releaseType"></s:select>
						<div id="releaseScope" style="display: none;float: right;width: 85%;">
							<s:if test="#request.trainModel.scopeNames.length() > 25">
								<s:set var="scopeshow" value="trainModel.scopeNames.substring(0,25)+'...'"></s:set>
							</s:if>
							<s:else>
								<s:set var="scopeshow" value="trainModel.scopeNames"></s:set>
							</s:else>
							<input type="text" id="scopeshow" value="${trainModel.releaseType==1?scopeshow:''}" style="width: 65%" readonly="readonly" placeholder="选择发布对象"/>
							<input type="hidden" id="totalPerson" name="trainModel.totalPerson" value="${trainModel.totalPerson}" style="width: 5%;" readonly="readonly"/>
							<input type="hidden" id="scopenames" name="trainModel.scopeNames" value="${trainModel.releaseType==1?trainModel.scopeNames:'' }"/>
							<input type="hidden" id="scopeids" name="trainModel.scopeIds" value="${trainModel.releaseType==1?trainModel.scopeIds:'' }"/>
							<!-- <input type="button" value="清空" id="btnClearScope"> -->
							<font id="btnClearScope" style="margin-left: 10px;">
								<a href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;margin-top: 2px;"></img>
								</a>
							</font>
						</div>
					</td>
				</tr>
				<tr style="display: none;">
					<td>发布对象<span class="inputflag">*</span></td>
					<td colspan="2">
						
					</td>
				</tr>
				<tr id="releaseTypeAutoArea" style="display: none;">
					<td>发布范围<span class="inputflag">*</span></td>
					<td colspan="2">
						<s:select id="releaseTypeAuto" name="trainModel.releaseType" headerKey="" headerValue="-请选择-" listKey="key" listValue="value"
							list="#{2:'部门', 3:'角色'}" value="#request.trainModel.releaseType"></s:select>
					</td>
				</tr>
				<tr id="deptSelect" style="display: none;">
					<td>部门选择<span class="inputflag">*</span></td>
					<td colspan="2">
						<input id="deptDescMulti" style="width:80%;" value="${trainModel.releaseType==2?trainModel.scopeNames:'' }"/>
						<input type="hidden" id="deptIdMulti" value="${trainModel.releaseType==2?trainModel.scopeIds:'' }"/>
						<font id="btnClearDeptScope" style="margin-left: 10px;">
							<a href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;margin-top: 2px;"></img>
							</a>
						</font>
					</td>
				</tr>
				<tr id="roleSelect" style="display: none;">
					<td>角色选择<span class="inputflag">*</span></td>
					<td colspan="2">
						<s:set value="#request.trainModel.scopeIds" var="scope"></s:set>
						<s:select id="roleIdSelect" list="#request.list_Role" name="roleId" headerKey="" headerValue="-请选择-" 
								listKey="id" listValue="name" value="scope"></s:select>
					</td>
				</tr>
				<tr id="publishDaysAuto">
					<td>培训天数<span class="inputflag">*</span></td>
					<td colspan="2">
						<input type="text" id="publishDays" name="trainModel.publishDays" value="${trainModel.publishDays }" style="width:50px;" />天
					</td>
				</tr>
				<tr>
					<td>课程绑定<span class="inputflag">*</span></td>
					<td colspan="2">
						<!-- <input id="courseNames_btn" type="button" name="courseNamesBtn" readonly="readonly" value="绑定课程"/>&nbsp; -->
						<s:if test="#request.trainModel.courses!=null && #request.trainModel.courses.size>0">
							<s:iterator value="trainModel.courses" var="va">
								<s:set var="course" value="va"></s:set>
							</s:iterator>
						</s:if>
						<input type="hidden" id="courseNames_select" name="courseNames" value="${course.name }">&nbsp;
				        <input type="hidden" id="courseIds_select" name="courseIds" value="${course.id }">&nbsp;
				        <ul id="courseView" style="width: 200px;float: left;">
				        	<s:if test="#request.trainModel.courses!=null && #request.trainModel.courses.size>0">
								<s:iterator value="trainModel.courses" var="va">
									<li _courseid="${va.id }" style="list-style:none;line-height:20px;background-color: #ECECEC;">${va.name }</li>
								</s:iterator>
								<s:set var="haveCourse" value="true"></s:set>
							</s:if>
							<s:else>
								<li _courseid="" style="list-style:none;line-height:20px;background-color: #ECECEC;">&nbsp;</li>
							</s:else>
						</ul>
				        <ul style="width: 200px;float: left;margin-left: 20px;">
							<li style="list-style:none;line-height:20px;">
								<a id="courseNames_btn" href="javascript:void(0);"><font color="blue">${haveCourse==true?'更新':'绑定' }课程</font></a>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>培训内容<span class="inputflag">*</span></td>
					<td valign="bottom" style="border-right-style:hidden" colspan="2">
						<textarea id="content" name="trainModel.content" style="width:99%;height:150px;">${trainModel.content }</textarea>
					</td>
				</tr>
			</table>
			<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
				<a href="javascript:void(0);" onclick="parent.layer.close(parent.layer.getFrameIndex(window.name));">退出</a>
				<a href="javascript:void(0);" class="btnSave" _status='1'>保存不生效</a>
				<a href="javascript:void(0);" class="btnSave" _status='0'>保存生效</a>
			</div>
		</form>
		</div>
	</body>
</html>
