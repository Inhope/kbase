<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />

<div class="gonggao_con_nr" style="height: 100%;">
	<table style="width: 50%;float: left;" border="0" cellspacing="0" cellpadding="0">
		<tr class="tdbg">
			<td width="45%">完成人员</td>
		</tr>
		<c:forEach items="${userList }" var="user">
		<c:if test="${user.status == 2 }">
			<tr>
				<td width="45%">
					${user.name }
				</td>
			</tr>
		</c:if>
		</c:forEach>
	</table>
	<table style="width: 50%;float: left;" border="0" cellspacing="0" cellpadding="0">
		<tr class="tdbg">
			<td width="45%">未完成人员</td>
		</tr>
		<c:forEach items="${userList }" var="user">
		<c:if test="${user.status == 1 }">
			<tr>
				<td width="45%">
					${user.name }
				</td>
			</tr>
		</c:if>
		</c:forEach>
	</table>
</div>
