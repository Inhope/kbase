<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>我的学习</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$(function(){
				//tab页切换
				$("div[class='px_top_meau']").on("click","ul li a",function(){
					//tab样式转换
					$(this).parent().parent().find("li").removeClass("ts");
					$(this).parent().addClass("ts");
					//加载iframe
					var _value = $(this).attr("_value");
					if(_value == 'index'){
						$('#learnContent').attr('src', $.fn.getRootPath() + '/app/learning/learn!home.htm');
					}else if(_value == 'train'){
						$('#learnContent').attr('src', $.fn.getRootPath() + '/app/learning/learn!train.htm');
					}else if(_value == 'exam'){
						$('#learnContent').attr('src', $.fn.getRootPath() + '/app/learning/examinee.htm');
					}
				});
				//默认首页
				$("div[class='px_top_meau'] ul li a[_value='index']").click();
			});
		</script>
	</head>
<body>
	<div class="easyui-layout" data-options="fit:false" id="cc" style="height:700px;">
		<div data-options="region:'north'" style="overflow: hidden;/* background:#edecec; */">
			<div class="px_top_meau">
				<ul> 
					<li class="ts"><a href="javascript:void(0);" _value="index">首页</a></li>
					<li><a href="javascript:void(0);" _value="train">培训</a></li>
					<li><a href="javascript:void(0);" _value="exam">考试</a></li>
				</ul>
			</div>
		</div>
		<div data-options="region:'center'" style="overflow: hidden;">
			<iframe id="learnContent" src="" scrolling="yes" frameborder="0" style="height:95%;width:100%;"></iframe>
		</div>
	</div>
</body>
</html>
