<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>编辑题目</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<jsp:include page="/resource/learning/header.jsp"></jsp:include>
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<style type="text/css">
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			#knowledgeView a{
				cursor: pointer;
			}
		</style>
		<script type="text/javascript">
			//富文本编辑器
			var editor;
			$(function(){
				KindEditor.ready(function(K) {
					editor = K.create('textarea[id="content"]', {
						width:"90%",height:"150px",pasteType : 1,
			            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist'] 
					});
				});
				//保存
				$("#doUnSave,#doSave").on("click",function(){
					var _id = $("#id").val();
					var _status = $(this).attr("value");
					var _name = $("#name").val();
					var _startDate = $("#startDate").val();
					var _endDate = $("#endDate").val();
					var _level = $("#level").val();
					var _score = $("#score").is(":checked")==true?1:0;
					var _cateId = $("#quesCateId_select").val();
					var _content = editor.html();
					var _resultRefer = $("[name='resultRefer']").val();
					if(_resultRefer == undefined){
						_resultRefer = "";
					}
					var _isRight = $("[name='isRight']:checked").val();
					if(_isRight == undefined){
						_isRight = "";
					}
					
					var key = $("#type option:selected").attr("key");
					var strResult = "";//答案选项内容字符串
					var isCheck = false;//是否有选中的答案选项
					var _result = [];
					$("[name='resultCheck']").each(function(i){
						var order;
						if($(this).parent().find("[id='order']").length > 0){
							order = $(this).parent().find("[id='order']").attr("value")
						}else{
							order = i+1;
						}
						if($(this).val() != ''){
							if($(this).is(":checked")){
								_result.push('{"isRight":"1","content":"'+$(this).val()+'","id":"'+$(this).attr("_id")+'","order":"'+order+'"}');
								isCheck = true;
							}else{
								_result.push('{"isRight":"0","content":"'+$(this).val()+'","id":"'+$(this).attr("_id")+'","order":"'+order+'"}');
							}
						}
						strResult += $(this).val();
					});
					_result = "["+_result+"]";
					var _kbValue = $('#queses').val();
					if(_cateId == ''){
						layer.alert('请选择题目分类',-1);
						return false;
					}
					if(editor.isEmpty()){
						layer.alert('请填写题目内容',-1);
						return false;
					}
					if(key == 0 || key == 1 || key == 3){
						if(strResult == ''){
							layer.alert('请添加答案选项并输入内容',-1);
							return false;
						}else if((key == 0 || key == 1) && !isCheck){
							layer.alert('请选择正确的答案选项',-1);
							return false;
						}
					}
					
					var _param = {
						id:_id,
						status:_status,
						name:_name,
						startDate:_startDate,
						endDate:_endDate,
						level:_level,
						score:_score,
						content:_content,
						resultRefer:_resultRefer,
						result:_result,
						isRight:_isRight,
						kbValue:_kbValue,
						cateId:_cateId
					}
					
					//判断是否被试卷引用过
					var _quesIds = [];
					_quesIds.push(_id);
					$.ajax({
						type: "POST",
						url: $.fn.getRootPath()+"/app/learning/ques!checkQuesUsed.htm",
						data: "ids="+_quesIds,
						dataType:"json",
						success: function(data) {
							if(data.status == 0){//无试卷引用该题目
								doUpdate(_param);
							}else if(data.status == 1){//试卷引用该题目
								layer.confirm(data.message+"，此操作有可能对在线考试造成影响，确定修改吗？",function(){
									doUpdate(_param);
								});
							}else if(data.status == 2){//系统出错
								layer.alert(data.message,-1);
							}
						}
					});
				});
				//选择分类
				$("#quesCateName_select").on("click",function(){
					$.kbase.picker.singleQuesCate({returnField:"quesCateName_select|quesCateId_select",diaWidth:400,diaHeight:360});
				});
				//增加填空区域
				$("#addText").on("click",function(){
					var order = $("#textList").find("input[name='resultCheck']").length + 1;//序号
					KindEditor.insertHtml("#content","("+order+")");
					$("#textTemplate").find("[id='order']").attr("value",order).text(order+":");
					$("#textList").append($("#textTemplate").html());
				});
				//增加radio
				$("#addRadio").on("click",function(){
					$("#radioList").append($("#radioTemplate").html());
				});
				$("#radioList").on("click","input:radio[name='resultCheck']",function(){
					_checkResult();
				});
				//增加checkBox
				$("#addCheckBox").on("click",function(){
					$("#checkBoxList").append($("#checkBoxTemplate").html());
				});
				//多选-显示正确答案提示
				$("#checkBoxList").on("click","input:checkBox[name='resultCheck']",function(){
					_checkResult();
				});
				//显示正确答案提示
				_checkResult = function(checkType){
					$("input[name='resultCheck']").each(function(i){
						if($(this).is(":checked")){
							$(this).parent().find("font").show();
						}else{
							$(this).parent().find("font").hide();
						}
					});
				}
				_checkResult();
				
				//知识来源(多选)
			    $('#quesNames_btn').click(function(){
			       $.kbase.picker.multiRelatedQues({returnField:"quesNames_select|quesIds_select"});
			    });
			    //删除选中知识
			    $('#knowledge_btn').click(function(){
			    	if($("input:checkbox[name='knowledge_list']:checked").length > 0){
			    		var _layer_confirm = layer.confirm("确定要删除选择的答案吗？", function(){
							var quesIds = $('#quesIds_select').val();
					    	var quesNames = $('#quesNames_select').val();
					    	var queses = $('#queses').val();//key串
					    	$("input:checkbox[name='knowledge_list']:checked").each(function(index, item){
								//删除时，同时删除隐藏域中保存的对应的id、名称和类型的key，才能保证再次添加时回显列表正确
					    		var _quesIds = $(this).next('a').attr('_quesIds');
					    		var _quesNames = $(item).next('a').attr('_quesNames');
					    		var _queses = _quesIds + "||" + _quesNames;
					    		quesIds = delLi(quesIds, _quesIds, ",");
								quesNames = delLi(quesNames, _quesNames, ",");
								queses = delLi(queses, _queses, "##");
					    		$('#quesIds_select').val(quesIds);
					    		$('#quesNames_select').val(quesNames);
					    		$('#queses').val(queses);
					    		$(this).parent().remove()
							});
							layer.close(_layer_confirm);
							layer.alert("操作成功",-1);
						});
			    	}else{
			    		layer.alert("请选择要删除的知识",-1);
			    	}
			    });
			    delLi = function(originalVal, delVal, regex){
					var _originalVal = originalVal.split(regex);
					_originalVal.splice(_originalVal.indexOf(delVal), 1);
					originalVal = _originalVal.join(regex)
		    		return originalVal;
			    }
			    //点击知识可进入详情
			    $("#knowledgeView").on("click","a",function(){
					var question = $(this).attr('_quesNames');
					parent.parent.TABOBJECT.open({
						id : 'courseFileInfo',
						name : "知识详情",
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+question,
						isRefresh : true
					}, this);
			    });
			    //点击折叠按钮
			    $("#showOrHideInfo,#showOrHidePro").on("click",function(){
			    	var url = $.fn.getRootPath()+"/resource/learning/images/fold_up.png";
			    	var _this;
			    	if($(this).attr("id") == 'showOrHideInfo'){
			    		_this = $("#infoArea tr").slice(1,4);
			    	}else if($(this).attr("id") == 'showOrHidePro'){
			    		_this = $("#proArea");
			    	}
			    	if(_this.is(":hidden")){
			    		_this.show();
			    		url = $.fn.getRootPath()+"/resource/learning/images/fold_down.png";
			    	}else{
			    		_this.hide();
			    	}
			    	$(this).attr("src",url);
			    });
			});
			function doUpdate(_param){
				$.ajax({
					type: "POST",
					url: "${pageContext.request.contextPath}/app/learning/ques!update.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						if(data.status == 1){
							layer.alert(data.message,-1,function(){
								parent.layer.close(parent.__kbs_layer_index_update);
							});
						}else{
							layer.alert(data.message,-1);
						}
					},
					error: function(msg){
						layer.alert("系统错误，请稍候重试！", -1);
					}
				});
			}
			
			//删除填空项
			function delText(_this){
				var _layer_confirm = layer.confirm("确定要删除选择的答案吗？", function(){
					//要删除的选项的序号-移除该序号在内容的标识
					var thisValue = $(_this).parent().find("[id='order']").attr("value");
					editor.html(editor.html().replace("("+thisValue+")",""));
					//大于该序号的选项的序号全部依次修改
					$("[name='resultCheck']").each(function(){
						var nextValue = $(this).parent().find("[id='order']").attr("value");
						if(nextValue > thisValue){
							$(this).parent().find("[id='order']").attr("value",nextValue-1).text(nextValue-1+":");
							//修改选项在内容的标识
							editor.html(editor.html().replace("("+nextValue+")","("+(nextValue-1)+")"));
						}
					});
					//根据选项id删除
					var _id = $(_this).parent().find("input[name='resultCheck']").attr("_id");
					if(_id != ''){
						delResultById(_id);
					}
					$(_this).parent().remove();
					layer.close(_layer_confirm);
				});
			}
			//给单选框(多选框)赋值
			function setValue(_this){
				$(_this).parent().find("[name='resultCheck']").val($(_this).val());
			}
			//删除选项
			function delResult(_this){
				var _layer_confirm = layer.confirm("确定要删除选择的答案吗？", function(){
					var _id = $(_this).parent().find("input[name='resultCheck']").attr("_id");
					if(_id != ''){
						delResultById(_id);
					}
					$(_this).parent().remove();
					layer.close(_layer_confirm);
				});
			}
			//根据id删除result
			function delResultById(_id){
				$.ajax({
					type: "POST",
					url: "${pageContext.request.contextPath}/app/learning/ques!delResult.htm",
					data: "id="+_id,
					success: function(data) {
					},
					error: function(msg){
						alert(msg);
					}
				});
			}
		</script>
	</head>
	<body>
		<input type="hidden" id="id" value="${ques.id }">
		<table id="infoArea" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
			<tr>
				<td colspan="2" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
					<div class="background_theme">
					<font class="font_style">基本信息</font>
					<img id="showOrHideInfo" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
						src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
					</div>
				</td>
			</tr>
			<tr>
				<td width="70">题目分类<span class="inputflag">*</span></td>
				<td>
					<input value="${ques.quesCate.name }" type="text" id="quesCateName_select" name="quesCateName_select" readOnly="readOnly" placeholder="选择分类"
						style="float: left;vertical-align: middle;margin: 8px 10px auto 0px;height: 25px;width: 60px;">
					<input value="${ques.quesCate.id }" type="hidden" id="quesCateId_select" name="quesCateId_select">
				</td>
			</tr>
			<tr>
				<td>难易度<span class="inputflag">*</span></td>
				<td>
					<select id="level" name="level">
						<c:forEach items="${levelList }" var="type">
						<option value="${type.key }" ${ques.level==type.key?"selected":"" }>${type.name }</option>
						</c:forEach>
					</select>
					<!-- <input type="checkbox" id="score" name="score" style="margin-left: 60px;" ${ques.score==1?"checked":""}>
					区间评分 -->
				</td>
			</tr>
			<tr>
				<td>开始日期</td>
				<td>
					<input id="startDate" name="startDate" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})"
					readonly="readonly" class="Wdate" type="text"
					value='<s:date name="ques.startDate" format="yyyy-MM-dd"/>' style="width: 100px;"/>
					结束日期
					<input id="endDate" name="endDate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd'})"
					readonly="readonly" class="Wdate" type="text"
					value='<s:date name="ques.endDate" format="yyyy-MM-dd"/>' style="width: 100px;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
					<div class="background_theme">
					<font class="font_style">题目设计</font>
					<img id="showOrHidePro" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
						src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
					</div>
				</td>
			</tr>
		</table>
		<table id="proArea" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
			<tr>
				<td width="70">题目类型<span class="inputflag">*</span></td>
				<td>
					<select name="type" id="type" disabled="disabled">
						<c:forEach items="${typeList }" var="type">
						<option value="${type.id }" ${type.id==ques.quesType.id?"selected":""} key="${type.key }">${type.name }</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>题目内容<span class="inputflag">*</span></td>
				<td>
					<textarea id="content" name="content" rows="3" cols="35" maxlength="500">${ques.contentValue}</textarea>
				</td>
			</tr>
			<!-- 单选 -->
			<c:if test="${ques.quesType.key==0 }">
			<tr>
				<td>
					答案选项
				</td>
				<td>
					<div id="radioList">
					<c:forEach items="${ques.quesResults }" var="result">
						<c:if test="${result.delFlag == 0 }">
						<div style="margin-bottom: 5px;">
							<input name="resultCheck" type="radio" _id="${result.id }" value="${result.content }" ${result.isRight==1?"checked='checked'":""}>
							<input name="result" type="text" value="${result.content }" onchange="setValue(this);" style="width: 300px;margin-top:5px;height: 23px;">
							<img onclick="delResult(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
								src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
							<font color="red" style="display: none;">正确答案</font>
						</div>
						</c:if>
					</c:forEach>
					</div>
					<div>
						<font style="float:left;">
							<a id="addRadio" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
								添加选项
							</a>
						</font>
					</div>
					<div id="radioTemplate" style="display: none;">
						<div style="margin-bottom: 5px;">
							<input name="resultCheck" type="radio" value="">
							<input name="result" type="text" onchange="setValue(this);" style="width: 300px;margin-top:5px;height: 23px;">
							<img onclick="delResult(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
								src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
							<font color="red" style="display: none;">正确答案</font>
						</div>
					</div>
				</td>
			</tr>
			</c:if>
			<!-- 多选 -->
			<c:if test="${ques.quesType.key==1 }">
				<tr>
					<td>
						答案选项
					</td>
					<td>
						<div id="checkBoxList">
							<c:forEach items="${ques.quesResults }" var="result">
								<c:if test="${result.delFlag == 0 }">
								<div style="margin-bottom: 5px;">
									<input name="resultCheck" type="checkbox" _id="${result.id }" value="${result.content }" ${result.isRight==1?"checked='checked'":""}>
									<input name="result" type="text" value="${result.content }" onchange="setValue(this);" style="width: 300px;margin-top:5px;height: 23px;">
									<img onclick="delResult(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
										src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
									<font color="red" style="display: none;">正确答案</font>
								</div>
								</c:if>
							</c:forEach>
						</div>
						<div>
							<font style="float:left;">
								<a id="addCheckBox" href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
									添加选项
								</a>
							</font>
						</div>
						<div id="checkBoxTemplate" style="display: none;">
							<div style="margin-bottom: 5px;">
								<input name="resultCheck" type="checkbox" value="">
								<input name="result" type="text" onchange="setValue(this);" style="width: 300px;margin-top:5px;height: 23px;">
								<img onclick="delResult(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
									src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
								<font color="red" style="display: none;">正确答案</font>
							</div>
						</div>
					</td>
				</tr>
			</c:if>
			<!-- 判断 -->
			<c:if test="${ques.quesType.key==2 }">
			<tr>
				<td>
					答案选项
				</td>
				<td>
					<input type="radio" value="1" name="isRight" ${ques.isRight==1?"checked='checked'":""} style="margin-bottom: 10px;">正确<br>
					<input type="radio" value="0" name="isRight" ${ques.isRight==0?"checked='checked'":""}>错误
				</td>
			</tr>
			</c:if>
			<!-- 填空 -->
			<c:if test="${ques.quesType.key==3}">
			<tr>
				<td>
					答案选项
				</td>
				<td>
					<div id="textList">
						<c:forEach items="${ques.quesResults }" var="result">
							<div style="margin-bottom: 5px;">
								<span id="order" value="${result.order }">${result.order }:</span>
								<input name="resultCheck" type="text" _id="${result.id }" value="${result.content }" style="width: 300px;margin-top:5px;height: 23px;">
								<img onclick="delText(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
									src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
							</div>
						</c:forEach>
					</div>
					<div>
						<font style="float:left;">
							<a id="addText" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
								添加填空区
							</a>
						</font>
					</div>
					<div id="textTemplate" style="display: none;">
						<div style="margin-bottom: 5px;">
							<span id="order" value=""></span>
							<input name="resultCheck" type="text" _id="" value="" style="width: 300px;margin-top:5px;height: 23px;">
							<img onclick="delText(this);" style="margin-top: 10px;margin-left: 10px;width: 15px;cursor: pointer;" 
								src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png">
						</div>
					</div>
				</td>
			</tr>
			</c:if>
			<!-- 问答 -->
			<c:if test="${ques.quesType.key==4 }">
			<tr>
				<td>
					参考答案
				</td>
				<td>
					<textarea name="resultRefer" rows="3" cols="35" maxlength="500" style="width: 497px;max-width: 497px;">${ques.resultRefer}</textarea>
				</td>
			</tr>
			</c:if>
			<tr>
				<td>
					知识来源
				</td>
				<td>
					<input id="quesIds_select" type="hidden" name="quesIds" value="${ques.kbValueIds }"/>
					<input id="quesNames_select" type="hidden" name="quesNames" value="${ques.kbValueNames }"/>
					<input id="queses" type="hidden" name="queses" value="${ques.kbValue }"/>
					<div>
						<ul id="knowledgeView" style="margin-top: 7px;">
							<c:set value="${fn:split(ques.kbValueIds, ',')}" var="ids" />
							<c:forEach items="${ques.kbValueNames }" var="kbValue" varStatus="status">
								<li style="list-style:none;line-height:25px;">
									<input type="checkbox" name="knowledge_list">
									<a _quesIds="${ids[status.index]}" _quesNames="${kbValue }">
										${kbValue}
									</a>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div>
						<font style="float:left;">
							<a id="quesNames_btn" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
								关联知识
							</a>
						</font>
						<font style="float:left;margin-left: 20px;">
							<a id="knowledge_btn" href="javascript:void(0);" style="cursor: pointer;">
								<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;"></img>
								删除
							</a>
						</font>
					</div>
				</td>
			</tr>
			<tr>
				<td>备注</td>
				<td>
					<textarea id="name" rows="2" cols="35" maxlength="500" style="width: 497px;max-width: 497px;">${ques.name}</textarea>
				</td>
			</tr>
		</table>
		<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
			<a href="javascript:void(0);" flag="next" onclick="parent.layer.close(parent.__kbs_layer_index_update);">退出</a>
			<a href="javascript:void(0);" flag="next" id="doUnSave" value="1">保存不生效</a>
			<a href="javascript:void(0);" flag="next" id="doSave" value="0">保存生效</a>
		</div>
	</body>
</html>

