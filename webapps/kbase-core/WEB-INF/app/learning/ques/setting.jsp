<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>设置</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<jsp:include page="/resource/learning/header.jsp"></jsp:include>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<style type="text/css">
			.levelTemplate{float: left; margin-right: 10px;margin-top: 7px;}
			.fl{ border:none; width:50%;line-height:30px;}
			.kbs-conn{ border-radius:5px; margin-top:2px;}
			.kbs-conn li { float:left; }
			.kbs-conn li a{ padding:5px 20px;background-color:#e0e0e0; color:#000;border:#e0e0e0 solid 1px; }
			.kbs-conn .kbs-border-a a{ border-radius:5px 0px 0px 5px;}
			.kbs-conn .kbs-border-b a{ border-radius:0px 5px 5px 0px;}
			.fkui_aa2{width:67px;height:28px;line-height:28px;float:right;color:#FFF;background:url(${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/fankui/ggzs_bg2.jpg) no-repeat;border:none;cursor:pointer;}
		</style>
		<script type="text/javascript">
			$(function(){
				//相似度检查点击事件
				$("#checksame").on("click","li a",function(){
					var _this = $(this);
					var _checksame = _this.attr("_value");
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/app/learning/ques!saveCheckSame.htm",
						data: "checksame="+_checksame,
						success: function(data) {
							data = jQuery.parseJSON(data);
							if(data.status == 1){
								$("#checksame").find("a").removeAttr("_checked");
								$("#checksame").find("a").css("background-color","");
								_this.attr("_checked","checked");
								_this.css("background-color","red");
							}else{
								layer.alert(data.message, -1);
							}
						}
					});
				});
				//添加题目难易度
				$("#addLevel").on("click",function(){
					var _levelName = $("#levelName").val();
					if(_levelName == ''){
						layer.alert("请输入题目难度内容", -1);
						return false;
					}
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/app/learning/ques!addQuesLevel.htm",
						data: "name="+_levelName+"&type=2",
						success: function(data) {
							data = jQuery.parseJSON(data);
							var _html = "<div class='levelTemplate'>"+$("#levelTemplate").html()+"<div>"
							$("#levelArea").append(_html.fill(
								data.id,
								data.name,
								data.key
							));
						}
					});
				});
				
				//双击可修改难易度名称
				$("#levelArea input").on("dblclick",function(){
					var _id = $(this).attr("id");
					var _value = $(this).attr("value");
					$("#leval_id").val(_id);
					$("#level_name").val(_value);
					//显示修改页面
					parent._levalContent_index = $.layer({
						type: 1,
					    title: false,
					    area: ['auto', 'auto'],
					    border: [5, 0.3, '#000'],
					    shade: [0],
					    shift: 'left',
					    page: {
					        dom: '#levelContent'
					    }
					});
				});
				
				//难易度名称提交修改
				$("#submitBtn").on("click",function(){
					var _id = $("#leval_id").val();
					var _name = $("#level_name").val();
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/app/learning/ques!updateLevel.htm",
						data: "id="+_id+"&name="+_name,
						success: function(data) {
							data = jQuery.parseJSON(data);
							if(data.status == 1){
								$("#"+_id).val(_name);
								layer.alert(data.message, -1);
								layer.close(parent._levalContent_index);
							}else{
								layer.alert(data.message, -1);
							}
						}
					});
				});
			});
			//删除题目类型-难易度
			function delQuesType(_this){
				var _id = $(_this).parent().find("input").attr("id");
				var _key = $(_this).parent().find("input").attr("key");
				$.ajax({
					type: "POST",
					url: "${pageContext.request.contextPath}/app/learning/ques!checkLevelUsed.htm",
					data: "key="+_key,
					success: function(data) {
						data = jQuery.parseJSON(data);
						if(data.status == 1){
							$.ajax({
								type: "POST",
								url: "${pageContext.request.contextPath}/app/learning/ques!delType.htm",
								data: "id="+_id,
								success: function(data) {
									data = jQuery.parseJSON(data);
									if(data.status == 1){
										$(_this).parent().remove();
									}else{
										layer.alert(data.message, -1);
									}
								}
							});
						}else{
							layer.alert("有题目引用该难度，不能删除", -1);
						}
					}
				});
			}
		</script>
	</head>
	<body>
		<div id="levelContent" style="border-bottom: 1px solid #e3e3e3;display:none;">
        	<input type="text" style="display: none;" id="leval_id">
			<table width="100%">
				<tr><th>难易度名称</th></tr>
				<tr><td><input id="level_name" type="text"/></td></tr>
				<tr>
					<td style="float: right;margin-right: 15px;margin-top: 5px;">
						<a href="javascript:void(0);" id="submitBtn"><span class="fkui_aa2" style="text-align: center;">提交</span></a>
					</td>
				</tr>
			</table>
		</div>
		<table width="100%" cellspacing="0" cellpadding="0" class="box">
			<tr>
				<td width="50px;">题目难易</td>
				<td>
					<input type="text" id="levelName" 
					style="border-left: 0;border-right: 0;border-top: 0;border-bottom: 1px solid #C9C9C9;">
					<img style="width: 15px;margin-left: -3px;" id="addLevel"
						src="${pageContext.request.contextPath}/resource/learning/images/addicon.jpg"></img>
				</td>
			</tr>
			<tr>
				<!-- 难易度自定义区域 -->
				<td colspan="2" id="levelArea">
					<c:forEach items="${levelList }" var="level">
						<div class="levelTemplate">
							<input type="button" id="${level.id }" key="${level.key }" value="${level.name }" style="min-width: 70px;">
							<%-- <img style="width: 15px;margin-left: -20px;margin-top:-5px;cursor: pointer;" onclick="delQuesType(this);"
								src="${pageContext.request.contextPath}/resource/learning/images/redDelIcon.jpg">
							</img> --%>
						</div>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div>
						题目相似度检查
					</div>
					<div>
						<ul class="fl kbs-conn" id="checksame">
							<li class="kbs-border-a">
								<a href="javascript:void(0);" _value="true" ${checksame?"_checked='checked' style='background-color:red;'":"" }>启动</a>
							</li>
							<li class="kbs-border-b">
								<a href="javascript:void(0);" _value="false" ${checksame?"":"_checked='checked' style='background-color:red;'" }>关闭</a>
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</table>
		<div id="levelTemplate" style="display: none;">
			<input type="button" id="{0}" value="{1}" key="{2}" style="min-width: 70px;">
			<%-- <img style="width: 15px;margin-left: -20px;margin-top:-5px;cursor: pointer;" onclick="delQuesType(this);"
				src="${pageContext.request.contextPath}/resource/learning/images/redDelIcon.jpg">
			</img> --%>
		</div>
	</body>
</html>
