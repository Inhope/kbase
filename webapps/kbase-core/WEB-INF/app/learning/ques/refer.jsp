<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>题目管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<jsp:include page="/resource/learning/header.jsp"></jsp:include>
		<script type="text/javascript">
			//分页查询
			function pageClick(pageNo){
				$("#pageNo").val(pageNo);
				$("#searchBtn").click();
			}
			$(function(){
				//月份点击事件
				$("#month").bind('click',function(){
					$("#startTime").val("");
					$("#endTime").val("");
					WdatePicker({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false });
				});
				//鼠标是否悬停在div上
				var isOutDept = true;
				outDeptArea = function(){
					isOutDept = true;
				}
				overDeptArea = function(){
					isOutDept = false;
				}
				var deptTreeObject = {
					deptTypeEl : $('#dept'),
					ktreeEl : $('#deptArea'),
					treeAttr : {
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
							autoParam : ["id", "name=n", "level=lv", "bh"]
						},
						callback : {
							onClick : function(event, treeId, treeNode) {
								deptTreeObject.deptTypeEl.val(treeNode.name);
								deptTreeObject.deptTypeEl.attr('bh', treeNode.bh);
							}
						}
					},
					render : function() {
						var self = this;
						var cityObj = self.deptTypeEl;
						var cityOffset = $(cityObj).offset();
						self.deptTypeEl.val('');
						self.deptTypeEl.focus(function(e){
							self.ktreeEl.css({
								'top' : cityOffset.top + cityObj.outerHeight() + "px",
								'left' : cityOffset.left + "px"
							});
							if(self.ktreeEl.is(':hidden')){
								self.ktreeEl.show();
							}
						});
						$("*").on("click",function(){
							if($("#dept").is(":focus")){
								
							}else{
								if(isOutDept){
									self.ktreeEl.hide();
								}
							}
						});
						self.deptTypeEl.bind('keydown', function(keyArg) {
							if(keyArg.keyCode == 8) {
								$(this).val('');
								$(this).attr('bh','');
							} else if(keyArg.keyCode == 13) {
								self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
							}
						});
						$.fn.zTree.init($('ul#deptTree'), this.treeAttr );
					}
				}
				
				//鼠标是否悬停在div上
				var isOutCategory = true;
				outCategoryArea = function(){
					isOutCategory = true;
				}
				overCategoryArea = function(){
					isOutCategory = false;
				}
				var categoryTreeObject = {
					categoryEl : $('#category'),
					ktreeEl : $('#categoryArea'),
					treeAttr : {
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/main/ontology-category!list.htm"
						},
						callback : {
							onClick : function(event, treeId, treeNode) {
								categoryTreeObject.categoryEl.val(treeNode.name);
								categoryTreeObject.categoryEl.attr('bh', treeNode.bh);
								categoryTreeObject.categoryEl.attr('_categoryId', treeNode.id);
							}
						}
					},
					render : function() {
						var self = this;
						var cityObj = self.categoryEl;
						var cityOffset = $(cityObj).offset();
						self.categoryEl.val('');
						self.categoryEl.focus(function(e){
							self.ktreeEl.css({
								'top' : cityOffset.top + cityObj.outerHeight() + "px",
								'left' :cityOffset.left + "px"
							});
							if(self.ktreeEl.is(':hidden')){
								self.ktreeEl.show();
							}
						});
						$("*").on("click",function(){
							if($("#category").is(":focus")){
								
							}else{
								if(isOutCategory){
									self.ktreeEl.hide();
								}
							}
						});
						self.categoryEl.bind('keydown', function(keyArg) {
							if(keyArg.keyCode == 8) {
								//清除知识目录
								$(this).val('');
								$(this).attr("bh","");
								$(this).attr("_categoryid","");
							} else if(keyArg.keyCode == 13) {
								self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
							}
						});
						$.fn.zTree.init($('ul#categoryTree'), this.treeAttr );
					}
				}
				deptTreeObject.render();
				categoryTreeObject.render();
				//选择分类
				$("#quesCateName_select").on("click",function(){
					$.kbase.picker.singleQuesCate({returnField:"quesCateName_select|quesCateId_select",diaWidth:400,diaHeight:360});
				});
				//新增题目
				$("#dataList").on("click","[name='referQuesAdd']",function(){
					var kbValue = $(this).attr("kbValue");
					var url = "/app/learning/ques!addUI.htm?kbValue="+kbValue;
					var title = "新增题目";
					__kbs_layer_index_one = $.layer({
						type: 2,
						border: [2, 0.3, '#000'],
						title: [title, 'font-size:14px;font-weight:bold;'],
						closeBtn: [0, true],
						iframe: {src : $.fn.getRootPath()+url},
						area: ['650px', '100%'],
						end:function(){
							loadForm();
						}
					});
				});
				//获取参数
				handleParam = function(){
					var _stat = $("[name='stat']").val();
					var pageNo = $("#pageNo").val();
					if(pageNo == undefined){
						pageNo = "1";
					}
					var _param = "pageNo="+pageNo;
					if(_stat == 'searchMost' || _stat == 'clickMost'){
						var _month = $("#month").val();
						if(_month == ''){
							layer.alert("请选择月份",-1);
							return false;
						}
						var _startTime = $("#startTime").val();
						var _endTime = $("#endTime").val();
						var deptBh = $("#dept").attr("bh");
						var deptName = $("#dept").val();
						var categoryBh = $("#category").attr("bh");
						//var _categoryId = $("#category").attr("_categoryId");
						_param += "&stat="+_stat+"&month="+_month+"&startTime="+_startTime+"&endTime="+_endTime
							+"&deptBh="+deptBh+"&deptName="+deptName+"&categoryBh="+categoryBh;//+"&categoryId="+_categoryId;
					}else if(_stat == 'userAsk'){
						var _month = $("#month").val();
						_param += "&stat="+_stat+"&month="+_month;
					}else if(_stat == 'latestUpdate'){
						_param += "&stat="+_stat;
					}
					return _param
				}
				//请求地址
				getLoadUrl = function(stat){
					var url = $.fn.getRootPath()+"/app/learning/ques!searchMost.htm";
					if(stat=='searchMost'){
					}else if(stat=='clickMost'){
						url = $.fn.getRootPath()+"/app/learning/ques!clickMost.htm";
					}else if(stat=='latestUpdate'){
						url = $.fn.getRootPath()+"/app/learning/ques!latestUpdate.htm";
					}else if(stat=='userAsk'){
						url = $.fn.getRootPath()+"/app/learning/ques!userAsk.htm";
					}
					return url;
				}
				//加载数据
				loadData = function(url){
					var param = handleParam();
					$.ajax({
						type: "POST",
						url: url,
						data: param,
						success: function(htmlResult) {
							var tab = $('#dataList');
				        	$(tab).find("tr").empty();
				        	$(tab).append($(htmlResult).find('tr'));
				        	$('body').ajaxLoadEnd();
						}
					});
				}
				//左边tab点击事件
				$("#statBtn").on("click","a",function(){
					var stat = $(this).attr("name");
					$("[name='stat']").val(stat);
					var url = getLoadUrl(stat);
					loadData(url);
					//背景色变换
					$(this).parent().find("a").css('background','none');
					$(this).css('background','#ECECEC');
					//查询条件变换
					if(stat == 'searchMost' || stat == 'clickMost'){
						$("#referMonth,#referCycle,#referDept,#referCate,#referSearch").show();
					}else if(stat == 'userAsk'){
						$("#referMonth,#referCycle,#referSearch").show();
						$("#referDept,#referCate").hide();
					}else{
						$("#referMonth,#referCycle,#referDept,#referCate,#referSearch").hide();
					}
				});
				//搜索
				$("#searchBtn").on("click",function(){
					var url = getLoadUrl($("[name='stat']").val());
					loadData(url);
				});
				//重置
				$("#resetBtn").on("click",function(){
					var date = new Date();
				    var month = date.getMonth()+1;
				    if(month < 10){
				    	month = "0"+month;
				    }
					$("#month").val(date.getFullYear()+"-"+month);
					$("#startTime").val("");
					$("#endTime").val("");
					$("#dept").val("");
					$("#dept").attr("bh","");
					$("#category").val("");
					$("#category").attr("bh","");
					$("#category").attr("_categoryid","");
				});
				//默认显示搜索最多数据
				$("#statBtn a[name='searchMost']").click();
			});
		</script>
	</head>
<body>
	<div style="width:5%;border: 0px;float: left;" id="statBtn">
		<a name="searchMost" href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;border-top: 0px;">
			搜<br>索<br>最<br>多
		</a>
		<a name="clickMost" href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;border-top: 0px;">
			点<br>选<br>最<br>多
		</a>
		<a name="latestUpdate" href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;border-top: 0px;">
			最<br>新<br>更<br>新
		</a>
		<a name="userAsk" href="javascript:void(0)" class="easyui-linkbutton" style="width:100%;border-top: 0px;">
			用<br>户<br>提<br>问
		</a>
	</div>
	<div style="border: 1px;float: left;width: 95%;overflow: hidden;">
		<form id="form0" action="" method="post">
			<input type="hidden" value="${stat }" name="stat">
			<div class="content_right_bottom" style="width:100%;min-width:860px;margin-bottom: 0px;">
				<div class="yonghu_titile" style="height: 40px;margin-left: 0px;margin-right: 0px;">
					<ul>
						<li><b>出题参考</b></li>
						<li id="referMonth">月份<input value="${month }" id="month" type="text" readonly="readonly"/></li>
						<li id="referCycle">周期
							<input id="startTime" onclick="javascript:WdatePicker({dateFmt:'yyyy-M-d'})" readonly="readonly" class="Wdate"
								name="startTime" value='<fmt:formatDate value="${startTime }"/>' type="text"  style="width:100px"/>
							至
							<input id="endTime" onclick="javascript:WdatePicker({dateFmt:'yyyy-M-d'})" readonly="readonly" class="Wdate"
								name="endTime" value='<fmt:formatDate value="${endTime }"/>' type="text"  style="width:100px" />
						</li>
						<li id="referDept">部门
							<input id="dept" readonly="readonly" name="" type="text" bh="${deptBh}" value="${deptName}"/>
						</li>
						<li id="referCate">知识目录
							<input id="category" readonly="readonly" name="" type="text" bh="${categoryBh }" _categoryId=""/>
						</li>
						<li id="referSearch" class="anniu">
							<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="查询" id="searchBtn"/></a>
						</li>
						<li id="referSearch" class="anniu">
							<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="重置" id="resetBtn"/></a>
						</li>
					</ul>
				</div>
				<div class="gonggao_con_nr" style="overflow-x:hidden;width:100%;">
					<table id="dataList" width="100%" cellspacing="0" cellpadding="0">
					</table>
				</div>
			</div>
		</form>
	</div>
	<div onmouseover="overDeptArea();" onmouseout="outDeptArea();" id="deptArea" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 400px;">
		<ul id="deptTree" class="ztree" style="margin-top:0; width:190px;"></ul>
	</div>
	<div onmouseover="overCategoryArea();" onmouseout="outCategoryArea();" id="categoryArea" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 400px;">
		<ul id=categoryTree class="ztree" style="margin-top:0; width:190px;"></ul>
	</div>
	<jsp:include page="import.jsp"></jsp:include>
</body>
</html>
