<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript">
		function checkBlank(){
			var _key = '${ques.quesType.key}';
			if(_key == 3){
				var results = jQuery.parseJSON('${jsonArr}');
				$(jQuery.parseJSON('${jsonArr}')).each(function(i,item){
					$("#blankQues").find("input").each(function(){
						if($(this).attr("order") == item.order){
							$(this).val(item.content);
						}
					});
				});
			}
		}
	</script>
</head>
<body style="overflow-x: hidden;" onload="javscript:checkBlank();">
	<div class="px_b_box" style="height: 348px;">
		<div class="usbox">
			<c:if test="${ques.quesType.key == 0 }">
				<div class="eaxm_title">
					单选题
				</div>
				<div class="exam_con">
					<div class="usbox" style="height: 100%;">
						<div class="exam_a">
							<span>${ques.content }</span>
							<ul>
								<c:forEach items="${ques.quesResults }" var="result">
									<li>
										<a href="javascript:void(0);">
											<table>
												<tr>
													<td valign="top">
														<img style="margin-right: 5px;" 
															src="${pageContext.request.contextPath}/resource/learning/images/${result.isRight==1?'ks_03.jpg':'ks_06.jpg' }"/>
													</td>
													<td>
														${result.content }
													</td>
												</tr>
											</table>
										</a>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${ques.quesType.key == 1 }">
				<div class="eaxm_title dx">
					多选题
				</div>
				<div class="exam_con">
					<div class="usbox">
						<div class="exam_a">
							<span>${ques.content }</span>
							<ul>
								<c:forEach items="${ques.quesResults }" var="result">
								<li>
									<a href="#">
										<img src="${pageContext.request.contextPath}/resource/learning/images/${result.isRight==1?'ks_09.jpg':'ks_12.jpg' }"/>
										&nbsp;${result.content }
									</a>
								</li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${ques.quesType.key == 2 }">
				<div class="eaxm_title dx">
					判断题
				</div>
				<div class="exam_con">
					<div class="usbox">
						<div class="exam_b">
							<span>${ques.content }</span>
							<ul>
								<li><a href="#"><img src="${pageContext.request.contextPath}/resource/learning/images/${ques.isRight==1?'ks_03.jpg':'ks_06.jpg' }"  alt=""/>&nbsp;对</a></li>
								<li><a href="#"><img src="${pageContext.request.contextPath}/resource/learning/images/${ques.isRight==1?'ks_06.jpg':'ks_03.jpg' }"  alt=""/>&nbsp;错</a></li>
							</ul>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</c:if>
			<c:if test="${ques.quesType.key == 3 }">
				<div class="eaxm_title dx">
					填空
				</div>
				<div class="exam_con">
					<div class="usbox">
						<div class="exam_b">
							<span id="blankQues">${ques.content }</span>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</c:if>
			<c:if test="${ques.quesType.key == 4 }">
				<div class="eaxm_title dx">
					简答题
				</div>
				<div class="exam_con">
					<div class="usbox">
						<div class="exam_c">  
							<span>${ques.content }</span> 
							<div class="exam_ca">
								<div class="usbox">
									<textarea rows="8" cols="100%" class="text-exam" readonly="readonly">${ques.resultRefer }</textarea>
								</div>
							</div>    
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
</body>
</html>