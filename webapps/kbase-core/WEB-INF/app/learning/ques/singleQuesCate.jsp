<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>题目分类-单选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/> --%>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			body{
				margin: 3px;
			}
			/* .ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			} */
			div {
				width: 500px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 375px;
				overflow: auto;
			}
			input {
				border: 1px solid #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/learning/ques!childCateJson.htm",
					autoParam:["id"]
				},
				check: {
					enable: true,
					chkStyle: "radio",
					radioType: "all"
				},
				callback: {
					onCheck: ztreeClick
				}
			};
			
			function ztreeClick(event, treeId, treeNode, clickFlag){
				$("#id").val(treeNode.id);
				$("#desc").val(treeNode.name);
			}
			
			$(function(){
				$.fn.zTree.init($("#treeDemo"), setting);
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
					}
					//回显值
					$("#desc").val(parent.$("#"+descField).val());
					$("#id").val(parent.$("#"+idField).val());
				}
				$("#btnOk").click(function(){
					var id = $("#id").val();
					var desc = $("#desc").val();
					if (descField!=null){
						parent.$("#"+descField).val(desc);
						parent.$("#"+idField).val(id);
					}
					$("#btnClose").click();
				});
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
			});
		</script>
	</head>
	<body>
		<div class="ztree" id="treeDemo" style="width: 380px;height: 280px;"></div>
		<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: right;">
			<a href="javascript:void(0);" class="btnSave" id="btnClose">取消</a>
			<a href="javascript:void(0);" class="btnSave" id="btnOk">确定</a>
			<input id="desc" style="width:200px;float: right;margin-right: 20px;margin-top: 10px;">
			<input type="hidden" id="id">
		</div>
		<br>
	</body>
</html>
