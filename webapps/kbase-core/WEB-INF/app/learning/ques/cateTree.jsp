<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
<style>
	.ul_PaperCate{width: 100%;}
	.ul_PaperCate>ul{width: 100%;}
	.ul_PaperCate>ul>li{text-align: left;margin-left: 5px;margin-top: 5px;}
	.box th{width: 30%;text-align: right;}
	.box td{width: 70%;}
	.box td>input{width: 97%;}
</style>
<script type="text/javascript">
	$(function(){
		$('#treeDemo').css('height', $(window).height()-60);
	});
</script>
<div style="background: #f6f6f6;border-bottom: 1px solid #dfdfdf;border-right: 2px solid #dfdfdf;min-height: 524px;">
	<div style="padding:5px;border:0;background:#f6f6f6; ">
		<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnNewCate" >添加</a>  
		<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnEditCate" >修改</a>  
		<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnDelCate" >删除</a>  
	</div>
	<div id="treeDemo" class="ztree" style="overflow-y: auto; overflow-x: auto;"></div>
</div>
<div id="rContent" style="display: none;">
	<table cellspacing="0" class="box" style="width: 300px;border: 0;">
		<tr>
			<th>
				父节点:
			</th>
			<td>
				<span id="pName"></span>
				<input type="hidden" id="pId"/>
			</td>
		</tr>
		<tr>
			<th>
				<span style="color: red;">*</span>当前节点:
			</th>
			<td>
				<input type="text" id="name" style="width: 97%;"/>
				<input type="hidden" id="id"/>
			</td>
		</tr>
		<tr>
			<th>
				备注:
			</th>
			<td>
				<textarea id="remark" rows="1" cols="1" style="width: 95%;"></textarea>
			</td>
		</tr>
	</table>
	<br>
	<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
		<span id="err_msg" style="color: red;float: left; margin-left: 5px;margin-top:5px;"></span>
		<input type="button" id="addOrEdt" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
	</div>
</div>