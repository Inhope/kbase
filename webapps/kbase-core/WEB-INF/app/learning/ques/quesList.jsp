<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<c:forEach items="${quesList }" var="ques">
	<tr id="dataListTr">
		<td name="${ques.isValid }"><input type='checkbox' name="quesId" value="${ques.id }"/></td>
		<%-- <td>${ques.name}</td> --%>
		<td style="text-align: left;padding-left: 10px;padding-right: 10px;">${ques.content}</td>
		<td>${ques.quesCate.name }</td>
		<td>${ques.quesType.name }</td>
		<td>${ques.kbValueNames }&nbsp;</td>
		<%-- <td>${ques.count }</td> --%>
		<td>
			<c:forEach items="${levelList }" var="level">
				<c:if test="${level.key==ques.level }">
					${level.name }
				</c:if>
			</c:forEach>
		</td>
		<td>
			<c:if test="${ques.status==0 }">启用</c:if>
			<c:if test="${ques.status==1 }">停用</c:if>
		</td>
		<td><fmt:formatDate value="${ques.startDate }" pattern="yyyy/M/d"/>&nbsp;</td>
		<td><fmt:formatDate value="${ques.endDate }" pattern="yyyy/M/d"/>&nbsp;</td>
		<td>
			<span style="cursor:pointer;color:darkblue;" name="quesInfo" _id="${ques.id }">预览</span>||
			<span style="cursor:pointer;color:darkblue;" name="quesQuote" _id="${ques.id }" _value="${ques.contentDesc }">引用</span>
		</td>
	</tr>
	</c:forEach>
	<tr class="trfen">
		<td colspan="12">
			<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
		</td>
	</tr>
</table>
