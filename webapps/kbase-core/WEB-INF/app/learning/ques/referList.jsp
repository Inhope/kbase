<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr class="tdbg">
		<c:if test="${stat == 'searchMost' || stat == 'clickMost' || stat == 'userAsk'}">
			<td width="45%">名称</td>
			<td width="45%">次数</td>
		</c:if>
		<c:if test="${stat == 'latestUpdate'}">
			<td width="45%">名称</td>
			<td width="45%">创建日期</td>
		</c:if>
		<td width="10%">&nbsp;</td>
	</tr>
	<c:if test="${stat == 'searchMost'}">
		<c:forEach items="${list }" var="v">
		<tr>
			<td>${v.content }</td>
			<td>${v.count }</td>
			<td>
				<span style="cursor:pointer;color:darkblue;" name="referQuesAdd" kbValue="${v.content }">新建题目</span>
			</td>
		</tr>
		</c:forEach>
	</c:if>
	<c:if test="${stat == 'clickMost'}">
		<c:forEach items="${list }" var="v">
		<tr>
			<td>${v.question }</td>
			<td>${v.count }</td>
			<td>
				<span style="cursor:pointer;color:darkblue;" name="referQuesAdd" kbValue="${v.question }">新建题目</span>
			</td>
		</tr>
		</c:forEach>
	</c:if>
	<c:if test="${stat == 'latestUpdate'}">
		<c:forEach items="${list }" var="v">
		<tr>
			<td>${v.ontologyValue.question }</td>
			<td><fmt:formatDate value="${v.ontologyValue.editTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			<td>
				<span style="cursor:pointer;color:darkblue;" name="referQuesAdd" kbValue="${v.ontologyValue.question }">新建题目</span>
			</td>
		</tr>
		</c:forEach>
	</c:if>
	<c:if test="${stat == 'userAsk'}">
		<c:forEach items="${list }" var="v">
		<tr>
			<td>${v.CONTENT }</td>
			<td>${v.COUNT }</td>
			<td>
				<span style="cursor:pointer;color:darkblue;" name="referQuesAdd">新建题目</span>
			</td>
		</tr>
		</c:forEach>
	</c:if>
	<c:if test="${fn:length(list) > 0}">
	<tr class="trfen">
		<td colspan="11">
			<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
		</td>
	</tr>
	</c:if>
</table>
