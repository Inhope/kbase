<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>题目管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<jsp:include page="/resource/learning/header.jsp"></jsp:include>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		<style type="text/css">
			.yonghu_titile div label { padding-left:15px; color: #666;}
			.yonghu_titile div label input {
				border: 1px solid #cccccc;
			  	height: 22px;
			  	line-height: 22px;
			  	padding-left: 2px;
			  	width: 86px;
			}
			/*数据列表input框样式*/
			.input-exam{ border-bottom:#e7e7e7 solid 1px; width:70px;border-left: 0px;border-top: 0px;border-right: 0px;}
		</style>
		<script type="text/javascript">
			$(function(){
			//移动分类-分类树
			var cateTreeObject = {
				cateEl : $('#quesCateName_select'),
				ktreeEl : $('#cateContent'),
				treeAttr : {
					view : {
						expandSpeed: ''
					},
					async : {
						enable : true,
						url : $.fn.getRootPath() + "/app/learning/ques!childCateJson.htm",
						autoParam : ["id"]
					},
					callback : {
						onClick : function(event, treeId, treeNode) {
							cateTreeObject.cateEl.val(treeNode.name);
							$('#quesCateId_select').val(treeNode.id);
						}
					}
				},
				render : function() {
					var self = this;
					self.cateEl.val('');
					self.cateEl.focus(function(e){
						self.ktreeEl.css({
							'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
							'left' : $(this).offset().left + 'px'
						});
						if(self.ktreeEl.is(':hidden'))
							self.ktreeEl.show();
					});
					$('*').bind('click', function(e){
						if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.cateEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
							
						} else {
							if(!self.ktreeEl.is(':hidden'))
								self.ktreeEl.hide();
						}
					});
					self.cateEl.bind('keydown', function(keyArg) {
						if(keyArg.keyCode == 8) {
							$(this).val('');
							$('#quesCateId_select').val('');
						} else if(keyArg.keyCode == 13) {
							self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
						}
					});
					$.fn.zTree.init($('ul#cateTreeDemo'), this.treeAttr );
				}
			}
			
			$("#quesCateName_select").on("click",function(){
				$('ul#cateTreeDemo').empty();
				cateTreeObject.render();
			});
			
			//查看引用题目的试卷
			$("[class='gonggao_con_nr']").on("click","[name='quesQuote']",function(){
				var _this = this;
				var _quesIds = [];
				_quesIds.push($(_this).attr("_id"));
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/ques!checkQuesUsed.htm",
					data: "ids="+_quesIds,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){//无试卷引用该题目
							layer.alert(data.message,-1);
						}else if(data.status == 1){//试卷引用该题目
							var pagerManagementId = "pagerManagement";
							//判断试卷管理的iframeId是否存在，不存在即赋值
							var pMp = $(parent.document.body).find("[class='juli'] li a[url='/kbase-core/app/learning/paper.htm']").parent();
							if(pMp.attr('frameId')) {
								pagerManagementId = pMp.attr('frameId');
							}else{
								pMp.attr('frameId', pagerManagementId);
							}
							parent.TABOBJECT.open({
								id : pagerManagementId,
								name : '试卷管理',
								hasClose : true,
								url : $.fn.getRootPath() + '/app/learning/paper.htm?quesContent='+$(_this).attr("_value"),
								isRefresh : true
							}, this);
						}else if(data.status == 2){//系统出错
							layer.alert(data.message,-1);
						}
					}
				});
			});
			
			});
		</script>
	</head>
	<body>
		<div style="width: 100%;">
			<div style="width: 16%;float: left;">
				<jsp:include page="cateTree.jsp"></jsp:include>
			</div>
			<div style="width: 84%;float: left;">
				<form id="form0" action="" method="post">
					<div class="content_right_bottom" style="width: 100%;">
						<input type="hidden" id="quesCateId" name="quesCateId">
						<div class="gonggao_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
							<div class="gonggao_titile_right" style="margin-right: 10px;">
								<myTag:input type="a" value="批量导出" key="quesManageExport" id="quesExport"/>
								<myTag:input type="a" value="批量导入" key="quesManageImport" id="quesImport"/>
								<myTag:input type="a" value="停用" key="quesManageStop" id="quesStop"/>
								<myTag:input type="a" value="启用" key="quesManageStart" id="quesStart"/>
								<myTag:input type="a" value="删除" key="quesManageDel" id="quesDel"/>
								<myTag:input type="a" value="编辑" key="quesManageEdit" id="quesEdit"/>
								<myTag:input type="a" value="新增" key="quesManageAdd" id="quesAdd"/>
								<myTag:input type="a" value="设置" key="quesManageSetting" id="quesSetting"/>
			          			<myTag:input type="a" value="出题参考" key="quesManageRefer" id="quesRefer"/>
								<%-- <s:iterator value="#request.menuList" var="menu">
								<s:if test="#menu.key == 'quesManageMoveCate'">
								<div style="float: right;">
									<a href="javascript:void(0);" id="moveToCate" style="float: left; margin-right: 0px;">移动分类</a>
									<input type="text" id="quesCateName_select" name="quesCateName_select" readOnly="readOnly" placeholder="选择分类" 
										style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
			          				<input type="hidden" id="quesCateId_select" name="quesCateId_select">&nbsp;
								</div>
								</s:if>
								</s:iterator> --%>
								<kbs:if key="quesManageMoveCate">
									<a href="javascript:void(0);" id="moveToCate" style="float: left; margin-right: 0px;">移动分类</a>
									<input type="text" id="quesCateName_select" name="quesCateName_select" readOnly="readOnly" placeholder="选择分类" 
										style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
			          				<input type="hidden" id="quesCateId_select" name="quesCateId_select">&nbsp;
								</kbs:if>
							</div>
						</div>
						<div class="yonghu_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
								<label style="float: left;">
									内容
									<input type="text" id="quesContent" name="ques.content" value="${ques.content }"/>
								</label>
								<label style="float: left;">
									有效期
									<input id="startDate" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
										name="ques.startDate" value='<s:date  name="ques.startDate" format="yyyy-MM-dd"/>' type="text"  style="width:80px"/>
									-
									<input id="endDate" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
										name="ques.endDate" value='<s:date  name="ques.endDate" format="yyyy-MM-dd"/>' type="text"  style="width:80px" />
								</label>
								<label style="float: left;">
									类型
									<select id="type" name="quesTypeId">
										<option value=""></option>
										<c:forEach items="${typeList }" var="type">
										<option value="${type.id }" ${ques.quesType.id==type.id?"selected":"" }>${type.name }</option>
										</c:forEach>
									</select>
								</label>
								<label style="float: left;">
									难易度
									<select id="level" name="ques.level" style="width: 50px;">
										<option value=""></option>
										<c:forEach items="${levelList }" var="type">
										<option value="${type.key }" ${ques.level==type.key?"selected":"" }>${type.name }</option>
										</c:forEach>
									</select>
								</label>
								<label style="float: left;">
									题目状态
									<s:select id="status" name="ques.status" key="ques.status" list="#{null:'','0':'启用','1':'停用'}" style="width: 50px;"></s:select>
								</label>
								<%-- <label style="float: left;">
									关联知识
									<input type="text" id="kbValue" name="ques.kbValue" value="${ques.kbValue }"/>
								</label> --%>
								<label style="float: left;">
									<span style="margin-left: 10px;">屏蔽过期</span>
									<span>
										<input style="margin-left: 5px;" id="isShowOff" name="isShowOff" value="" type="checkbox"/>
									</span>
								</label>
								<div class="anniu" style="float: right;width: 140px;">
									<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" id="selectReset"/></a>
									<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="查询" id="submitForm"/></a>
								</div>
						</div>
						<div class="gonggao_con" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;">
							<div class="gonggao_con_nr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" id="quesList">
									<tr class="tdbg">
										<td width="4%"><input type='checkbox' id="checkAll"/></td>
										<!-- <td width="15%">名称</td> -->
										<!-- <td width="8%">创建人</td> -->
										<td width="32%">内容</td>
										<td width="10%">分类</td>
										<td width="5%">类型</td>
										<td width="17%">关联知识</td>
										<!-- <td width="4%">入选数</td> -->
										<td width="5%">难易度</td>
										<td width="5%">状态</td>
										<td width="7%">开始日期</td>
										<td width="7%">结束日期</td>
										<!-- <td width="12%">创建时间</td> -->
										<td width="8%">&nbsp;</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="cateContent" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="cateTreeDemo" class="ztree" style="margin-top:0; width:190px;"></ul>
		</div>
		<jsp:include page="import.jsp"></jsp:include>
	</body>
</html>
