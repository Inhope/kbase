<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>课程管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/course.js"></script>
		
		<script type="text/javascript">
			parent.$('body').ajaxLoadEnd();
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				parent.$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/course!listPage.htm");
				if(pageNo != undefined){
					$("#pageNo").val(pageNo);
				}
				$("#form0").submit();
			}				
			
			//查询条件初始化
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
				});
				$("#exprieCourse_select").removeAttr("checked");
			}
			
			
			// 弹出框打开
			function openShade(id) {
				var url, bl = false, title;
				if (id == "courseAdd") {
					url = "/app/learning/course!addOrEditTo.htm";
					title = "新增课程";
				} else if (id == "courseEdit") {
					url = "/app/learning/course!addOrEditTo.htm";
					title = "编辑课程";
					bl = true;
				} else {
					return;
				}
			
				if (bl) {
					var courseId_list = $("input:checkbox[name='courseId_list']:checked")
					if (courseId_list.length == 0) {
						layer.alert("请选择要操作的数据!", -1);
						return;
					} else if (courseId_list.length > 1) {
						layer.alert("不能同时操作多条数据!", -1);
						return;
					} else {
						var courseId = $(courseId_list[0]).val();
						url += '?courseId=' + courseId;
					}
				}
				
				parent.__kbs_layer_index = parent.$.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ title, 'font-size:14px;font-weight:bold;' ],
					shade: [0.3, '#000'],
					closeBtn : [ 0, true ],
					offset : ["0px", "200px"],
					iframe : {
						src : $.fn.getRootPath() + url
					},
					area: ['720px', '400px'],
					end:function(){
						pageClick();
					}
				});
			}
			
			function noShade(idAttr) {
				var url, bl = false, title, parameters = {}, msgExt = '';
				if (idAttr == "courseDel") {
					url = "/app/learning/course!delCourse.htm";
					title = "删除";
				} else if (idAttr == "editStatus0") {
					url = "/app/learning/course!statusSet.htm";
					title = "启用";
					parameters.status = 0;
				} else if (idAttr == "editStatus1") {
					url = "/app/learning/course!statusSet.htm";
					title = "停用";
					parameters.status = 1;
				} else {
					return false;
				}
				
				var courseIds = "";
				$("input:checkbox[name='courseId_list']:checked").each(function(){
					bl = true;
					courseIds += "," + $(this).val();
				});
				if(!bl){
					parent.layer.alert("请至少选择一条数据!", -1);
					return;
				}
				parameters.courseIds = courseIds.substring(1);
				
				if (bl){
						parent.layer_confirm = parent.layer.confirm("确定要" + title + "选中的数据吗？", function(){
							parent.layer.close(parent.layer_confirm);
							$.post($.fn.getRootPath() + url, parameters,
								function(data){
									parent.layer_confirm = parent.layer.alert(data.msg, -1, function(){
										parent.layer.close(parent.layer_confirm);
										if (data.rst) pageClick();
									});
							}, "json");
						});
					}
			}
			
			
			$(function(){	
				//课程分类选择(单选)
				/*
			    $('#courseCateNames_select').click(function(){
			       $.kbase.picker.singleCourseCate({returnField:"courseCateNames_select|courseCateIds_select"});
			    });
			    */
			    
			    //移动课程到选择的分类
			    $('#courseCateMove').click(function(){
			       var _courseCateName = $("#courseCateNames_select").val();
			       if($.trim(_courseCateName)==''){
			       		parent.layer.alert("请选择分类!", -1);
						return;
			       }else{
			       		var url, bl = false, title, parameters = {}, msgExt = '';
						var courseIds = "";
						$("input:checkbox[name='courseId_list']:checked").each(function(){
							bl = true;
							courseIds += "," + $(this).val();
						});
						if(!bl){
							parent.layer.alert("请至少选择一条数据!", -1);
							return;
						}
						courseIds = courseIds.substring(1);
					
						url = "/app/learning/course!updateCate.htm";
						parameters.courseIds = courseIds;
						parameters.cateId = $("#courseCateIds_select").val();
						
						if (bl){
							layer.confirm("确定要改变选中课程的分类吗？", function(){
									var loadindex = layer.load('提交中…');
									$.post($.fn.getRootPath() + url, parameters,
										function(data){
											layer.alert(data.msg, -1, function(){
												layer.close(loadindex);
												if (data.rst) pageClick();
											});
									}, "json");
								});
							}
			       }
					
			    });
			    
			    
			    
			    
			    //课程导出
			    $('#courseExp').click(function(){
			    	$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/course!exportCourse.htm");
					$("#form0").submit();
			    });
			    
			    
			    //课程导入step1
				$("#courseImp").on("click",function(){
					
					window.__kbs_layer_courseImp = $.layer({
						type: 1,
						title: '课程导入',
					    area: ['400', '300'],
					    border: [2, 0.3, '#000'], //去掉默认边框
					    shade: [0.5, '#666'], //去掉遮罩
					    shift: '2', 
					    page: {
					        dom: '#courseImportShade'
					    }
					});
					
					
					
					
				});
			    
			    
			    //课程导入step2
			    $("#importCourse").click(function(){
					if($.trim($("#courseFile").val()) == ''){
						parent.layer.alert("请选择需要上传的文件!", -1);
						return false;
					}
					//数据导入
					$.ajaxFileUpload({
						url: $.fn.getRootPath()+"/app/learning/course!importCourse.htm",
						type: 'post',
						secureuri: false, //一般设置为false
						fileElementId: 'courseFile', // 上传文件的id、name属性名
						dataType: 'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){  
							if (data.rst == "1") {
								parent.layer.alert("导入成功", -1);
								$('body').ajaxLoadEnd();
					   			location.reload();
							} else {
								alert(data.msg);
							}
						
						},
						error: function(data, status, e){ 
							parent.layer.alert("网络错误，请稍后再试!", -1);
						}
					});
					
				
				});
				
				
				
				
				//课程分类div中的tree
			  var cateTreeObject = {
					cateEl : $('#courseCateNames_select'),
					ktreeEl : $('div.cateContent'),
					treeAttr : {
						view : {
							expandSpeed: ''
						},
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/learning/course-cate!cateJson.htm",
							autoParam : ["id"]
						},
						callback : {
							onClick : function(event, treeId, treeNode) {
								cateTreeObject.cateEl.val(treeNode.name);
								$('#courseCateIds_select').val(treeNode.id);
							}
						}
					},
					render : function() {
						var self = this;
						self.cateEl.val('');
						self.cateEl.focus(function(e){
							self.ktreeEl.css({
								'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
								'left' : $(this).offset().left + 'px'
							});
							if(self.ktreeEl.is(':hidden'))
								self.ktreeEl.show();
						});
						
						$('*').bind('click', function(e){
							if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.cateEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
								
							} else {
								if(!self.ktreeEl.is(':hidden'))
									self.ktreeEl.hide();
							}
						});
						self.cateEl.bind('keydown', function(keyArg) {
							if(keyArg.keyCode == 8) {
								$(this).val('');
								$("#courseCateIds_select").val('');
							} else if(keyArg.keyCode == 13) {
								self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
							}
						});
						$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
					}
				}
			  cateTreeObject.render();
				//预览
				$('table').on("click","[name='courseInfo']",function(){
					var _courseId = $(this).attr("_id");
					parent.parent.parent.TABOBJECT.open({
						id : 'courseInfo',
						name : '课程学习',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+_courseId,
						isRefresh : true
					}, this);
				});
				
			    //屏蔽过期（勾选时屏蔽已过期）
			    $('#exprieCourse_select').click(function(){
			    	var exprieCourse = $("input:checkbox[name='exprieCourse']:checked");
			    	if (exprieCourse.length > 0) {
			    		$("input:checkbox[name='exprieCourse']").val("1");
					}else{
						$("input:checkbox[name='exprieCourse']").val("0");
					}
			    	parent.$('body').ajaxLoading('正在查询数据...');
					$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/course!listPage.htm");
					$("#pageNo").val(1);
					$("#form0").submit();
			    });
			});
			
			
			
		</script>
	</head>
		<body>
		
		
		<div style="margin-top:0; width:100%;">
			
			
					<!--******************************************************课件管理初始化页面*********************************************************************************  -->
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="width:100%;">
							<div class="gonggao_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
								<div class="gonggao_titile_right" style="margin-right: 10px;">
									<%-- <myTag:input type="a" value="导出" key="courseManageExport" id="courseExp"/>
									<myTag:input type="a" value="导入" key="courseManageImport" id="courseImp"/> --%>
									<myTag:input type="a" value="停用" key="courseManageStop" id="editStatus1" onclick="noShade(this.id)"/>
									<myTag:input type="a" value="启用" key="courseManageStart" id="editStatus0" onclick="noShade(this.id)"/>
									<myTag:input type="a" value="删除" key="courseManageDel" id="courseDel" onclick="noShade(this.id)"/>
									<myTag:input type="a" value="编辑" key="courseManageEdit" id="courseEdit" onclick="openShade(this.id)"/>
									<myTag:input type="a" value="新增" key="courseManageAdd" id="courseAdd" onclick="openShade(this.id)"/>
									<%-- <s:iterator value="#request.menuList" var="menu">
									<s:if test="#menu.key == 'quesManageMoveCate'">
					          			<a href="javascript:void(0);" id="courseCateMove" style="float: left; margin-right: 0px;">移动分类</a>
										<input type="text" id="courseCateNames_select" name="courseNames" onclick="showCate('cateContent','courseCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
											style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
					          			<input type="hidden" id="courseCateIds_select" name="courseIds">&nbsp;
									</s:if>
									</s:iterator> --%>
									<kbs:if key="courseManageMoveCate">
					          			<a href="javascript:void(0);" id="courseCateMove" style="float: left; margin-right: 0px;">移动分类</a>
										<input type="text" id="courseCateNames_select" name="courseNames" onclick="showCate('cateContent','courseCateNames_select')" readOnly="readOnly" placeholder="选择分类" 
											style="height: 26px; border: 1px solid #cccccc;margin-left:-2px;margin-top:-2px;vertical-align: middle;width: 120px;">
					          			<input type="hidden" id="courseCateIds_select" name="courseIds">&nbsp;
									</kbs:if>
								</div>
							</div>
							<div class="yonghu_titile" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;height: auto;">
								<ul>
									<li>
										课程名称
										<input type="text" id="name_select" name="courseModel.name" value="${courseModel.name }" />
									</li>
									<!-- <li>
										创建人
										<input id="createUserNameCN_select"  name="courseModel.createUserNameCN" type="text" value="${courseModel.createUserNameCN }"/>
									</li>
									<li>
										创建时间
										<input type="text" id="createDate1_select"  name="createDate1" value="${createDate1 }" placeholder="开始日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'createDate2_select\')||\'new Date()\'}'})"/>
										-
										<input type="text" id="createDate2_select"  name="createDate2" value="${createDate2 }" placeholder="结束日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'createDate1_select\')}',maxDate:new Date()})"/>
									</li>
									 -->
									<li>
										有效期
										<input type="text" id="startTime_select"  name="courseModel.startTime" value="<s:date name='courseModel.startTime' format='yyyy-MM-dd' />" placeholder="开始日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endTime_select\')}'})"/>
										-
										<input type="text" id="endTime_select"  name="courseModel.endTime" value="<s:date name='courseModel.endTime' format='yyyy-MM-dd' />" placeholder="结束日期" readonly="readonly" class="Wdate" style="width:80px" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'startTime_select\')}'})"/>
									</li>
									<li>
										课程状态
									    <s:select id="status_select" name="courseModel.status" value="#request.courseModel.status" headerKey="" headerValue="-全部-" list="#{0:'启用', 1:'停用'}"></s:select>
									</li>
									<li>
										难易度
										<select id="level_select" name="courseModel.level">
											<option value="">-全部-</option>
											<s:iterator value="#request.levelList" var="queslevel">
												<option value="${queslevel.key }">${queslevel.name }</option>
											</s:iterator>
										</select>
									</li>
									<li>
										<span style="float:left;">屏蔽过期</span>
										<input type='checkbox' id="exprieCourse_select" name="exprieCourse" value="${exprieCourse }" <s:if test="#request.exprieCourse == 1">checked</s:if> style="width: 20px;float: left;margin-top: 9px;"/>
								 	</li>
									<li class="anniu" style="float: right;margin-right: 10px;">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="查询" onclick="javascript:pageClick('1');" />
										</a>
										<input type="hidden" id="cateIds_select" name="cateIds" value="${cateIds }">&nbsp;
									</li>
								</ul>
							</div>
							
							<div class="gonggao_con" style="margin-right: 0px;margin-left: 0px;width: 100%;float: left;">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="3%">
												<input type='checkbox' id="checkAll"/>
											</td>
											<td width="23%">
												课程名称
											</td>
											<td width="7%">
												开始日期
											</td>
											<td width="7%">
												结束日期
											</td>
											<td width="7%">
												学习次数
											</td>
											<td width="5%">
												难易度
											</td>
											<td width="13%">
												课程分类
											</td>
											<td width="5%">
												创建人
											</td>
											<td width="7%">
												课程状态
											</td>
											<td width="7%">
												是否过期
											</td>
											<td width="4%">
												&nbsp;
											</td>
										</tr>
										<s:iterator value="page.result" var="va">
											<s:if test="#va != null">
												<tr>
												<td>
													<input type='checkbox' name="courseId_list" value="${va.id }" />
												</td>
												<td>
													${va.name} 
												</td>
												<td>
													<s:date name="#va.startTime" format="yyyy/MM/dd" />&nbsp;
												</td>
												<td>
													<s:date name="#va.endTime" format="yyyy/MM/dd" />&nbsp;
												</td>
												<td>
													${va.count}
												</td>
												<td>
													<s:iterator value="#request.levelList" var="levelLs">
														<s:if test="#va.level == #levelLs.key">${levelLs.name }</s:if>
													</s:iterator>
												</td>
												<td>
													${va.courseCate.name}
												</td>
												<td>
													${va.createUserNameCN}
												</td>
												<td>
													<s:if test="#va.status == 0">启用</s:if>
													<s:if test="#va.status == 1">停用</s:if>
												</td>
												<td>
													<s:if test="#va.expire">已过期</s:if>
													<s:else>未过期</s:else>
												</td>
												<td>
													<span style="cursor:pointer;color:darkblue;" name="courseInfo" _id="${va.id }">预览</span>
												</td>
											</tr>
											</s:if>
										    <div >
												
											</div>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
	
			
		</div>
		
		
		
		<div id="cateContent" class="cateContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:190px;"></ul>
		</div>
		
		
		
		<div id="courseImportShade" style="display:none;">
			<ul>
				<li id="scBefore" style="padding-left: 30px;">
					<p>
						<br />
						注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/learning/course!downloadCourseTemplete.htm?fileName=courseImportMould.xls" style="color: blue">点击下载</a>)填写相关数据
						<br />
						特别提示：
						<br />
						1、请及时下载最新模板
						<br />
						2、上传内容请严格按照模板要求来填写
						<br />
						<br />
					</p>
				</li>
				<li id="importHandle">	
					<p style="float: left;">
						<input type="file" id="courseFile" name="courseFile" />
					</p>
					<span style="float: left;">
						<input id="importCourse" value="上传" type="button">
					</span>
				</li>
			</ul>						
		</div>
		
		
			
	</body>
</html>

