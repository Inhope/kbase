<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>新增或编辑课程</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <style type="text/css">
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			#knowledgeView a{
				cursor: pointer;
			}
		</style>		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/course.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/learning/js/learning.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript">
			$(function(){
				$('.btnSave').click(function(){
					var _this = this;
					var status = $(_this).attr('_status');
					var courseId = $('#courseId').val();
					var name = $('#name').val();
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					var level = $('#level').val();
					var content = editor.html(); //$('#content').val();
					//课程封面
					var coverFileName = $('#coverFileName').val();
					var coverRealName = $('#coverRealName').val();
					//课程分类
					var courseCateIds = $('#cateIds_select').val();
					var releaseType = $('#releaseType').val();//开放范围
					var scopeids = $('#scopeids').val();//开放对象ids
					var scopenames = $('#scopenames').val();//开放对象names
					//知识来源
					var queses = $('#queses').val();
					//自测限时
					var limitMinute = $("#limitMinute").val();
					//课后自测试题
					var arr = new Array();
					$('#quesCourse').find('li').each(function(index, item){
						var courseQuesParam = {};
						var $input = $(item).find('input');
						courseQuesParam.quesId = $input.attr('_quesid');
						courseQuesParam.quesName = $input.attr('_quesname');
						courseQuesParam.ordernum = index;
						var patten = new RegExp($input.attr('_quesid')+'_*');
	    				_questype = $input.attr('_questype').replace(patten, '');
						courseQuesParam.quesType = _questype;
						arr.push(courseQuesParam);
					});
					//附件
					var attachArr = new Array();
					$('#courseAttachView').find('li').each(function(index, item){
						var courseAttachParam = {};
						var $name = $(item).find('div:eq(1)');
						courseAttachParam.attachId = $name.attr('_attachId');
						courseAttachParam.attachFileName = $name.attr('_attachfilename');
						courseAttachParam.attachRealName = $name.text();
						courseAttachParam.attachType = $name.attr('_attachtype');
						courseAttachParam.attachRemark = $(item).find('[name="remark"]').val();
						courseAttachParam.ordernum = index;
						attachArr.push(courseAttachParam);
					});
					if(name == null || $.trim(name)==''){
						layer.alert('课程名称不能为空', -1);
						return;
					}
					/*else if( startTime==null || $.trim(startTime)=='' || endTime==null || $.trim(endTime)==''){
						layer.alert('开始时间和结束时间不能为空', -1);
						return;
					}*/
					else if (level == null || $.trim(level)==''){
						layer.alert('请选择难易度', -1);
						return;
					}else if (courseCateIds == null || $.trim(courseCateIds)==''){
						layer.alert('请选择分类', -1);
						return;
					}else if(releaseType==null || $.trim(releaseType)==''){
						layer.alert('开放范围不能为空', -1);
						return;
					}else if($.trim(releaseType)=='1' && scopeids==''){
						layer.alert('请选择发布对象',-1);
						return;
					}else if(content==null || $.trim(content)==''){
						layer.alert('内容不能为空', -1);
						return;
					}else if(attachArr.length == 0){
						layer.alert('请上传课件', -1);
						return;
					}else if(limitMinute != '' && isNaN(limitMinute)){
						layer.alert('自测限时请填入数字', -1);
						return;
					}else{
						var params = {
							id : courseId,
							status : status,
							name : name,
				   			startTime : startTime,
				   			endTime : endTime,
				   			level : level,
				   			courseCateIds : courseCateIds,
				   			releaseType : releaseType,
				   			scopeids : scopeids,
				   			scopenames : scopenames,
				   			content : content,
				   			coverFileName : coverFileName,
				   			coverRealName : coverRealName,
				   			queses : queses,
				   			quesParam : JSON.stringify(arr),
				   			attachParam : JSON.stringify(attachArr),
				   			limitMinute : limitMinute
						};
						var url = $.fn.getRootPath()+"/app/learning/course!addOrEditDo.htm";
						if($.trim(courseId) != ''){
							addOrUpdate(url, params);
						}else{
							$.post($.fn.getRootPath() + '/app/learning/course!checkSameName.htm', 
								{'name': name},
								function(data){
									if(data.rst == '1'){
										if(data.status){
											layer.alert('已经存在同名的课程', -1);
											return;
										}else{
											addOrUpdate(url, params);
										}
									}else{
										parent.layer.alert("系统异常，请稍后再试", -1);
									}
							}, "json");
						}
					} 
				});
				
				//课程名称字数提示信息
				$("#name").keyup(function(){
				    var name = $(this).val().replace(/[\r\n]/g,"");
				    var length = name.length;
				    if(length > 50){
						layer.alert('课程名称最多不能超过50个字符', -1);
				        $(this).val(title.substring(0,50))
				        length = 50;
				    }
				    $("#title_lenght_show").text("50/"+length);
				});
				
				//课程知识来源(多选)
			    $('#quesNames_btn').click(function(){
			       $.kbase.picker.multiRelatedQues({returnField:"quesNames_select|quesIds_select"});
			    });
			    
			    //课程分类选择(单选)
			    $('#cateNames_select').click(function(){
			       $.kbase.picker.singleCourseCate({returnField:"cateNames_select|cateIds_select"});
			    });
			    
			    //选择考试用户
				$("#scopenames").click(function(){
					$.kbase.picker.multiDeptusersearch({returnField:"scopenames|scopeids"});
				});
			    
			    //------------------------课后自测试题操作start------------------------
			    //添加试题
				$('#quesAdd').click(function(){
			       $.kbase.picker.multiCourseQues({returnField:"quesCourseNames_select|quesCourseIds_select|quesTypes_select"});
			    });
			    
			    //删除课后自测试题
			    $('#quesDel').click(function(){
			    	if($("input:checkbox[name='quesid_list']:checked").length > 0){
			    		var _layer_confirm = layer.confirm("确定要删除选择的题目吗？", function(){
					    	var quesCourseIds = $('#quesCourseIds_select').val();
					    	var quesCourseNames = $('#quesCourseNames_select').val();
					    	var quesTypes = $('#quesTypes_select').val();//题目的key串
					    	$("input:checkbox[name='quesid_list']:checked").each(function(){
					    		var _quesId = $(this).attr('_quesid');
					    		var _questype = $(this).attr('_questype');
								//删除题目时，同时删除隐藏域中保存的对应的题目id、名称和题目类型的key，才能保证再次添加题目时回显列表正确
								quesCourseIds = delLi(quesCourseIds, _quesId, ",");
								quesCourseNames = delLi(quesCourseNames, $(this).attr('_quesname'), ",");
								quesTypes = delLi(quesTypes, _questype, ",");
					    		$('#quesCourseIds_select').val(quesCourseIds);
					    		$('#quesCourseNames_select').val(quesCourseNames);
					    		$('#quesTypes_select').val(quesTypes);
					    		//删除选中的题目
								var quesLi = $(this).parent();
								$(quesLi).parent().find(quesLi).remove();
								//该题目对应题型数量减1
								var patten = new RegExp(_quesId+'_*');
			    				_questype = _questype.replace(patten, '');
								var cur_num = $("#num_"+_questype).text();
								$("#num_"+_questype).text(parseInt(cur_num)-1);
							});
							layer.close(_layer_confirm);
			    		});
			    	}else{
			    		layer.alert("请选择要删除的题目",-1);
			    	}
			    });
			    
			    //课后自测试题上移、下移
			    $('.quesMove').click(function(){
					var type = $(this).attr('_type');
			    	var quesId_list = $("input:checkbox[name='quesid_list']:checked");
					if (quesId_list.length == 0 || quesId_list.length > 1) {
						layer.alert("请选择一条数据!", -1);
						return;
					} else {
						var quesCourseIds = $('#quesCourseIds_select').val();
				    	var quesCourseNames = $('#quesCourseNames_select').val();
				    	var quesTypes = $('#quesTypes_select').val();//试题的key串
						var quesId = $(quesId_list).attr('_quesid');
						var quesName = $(quesId_list).attr('_quesname');
						var quesType = $(quesId_list).attr('_questype');
						if(type == 'up'){
							var quesLi = $(quesId_list).parent();
							quesLi.prev().before(quesLi);
							//上移试题时，同时修改隐藏域中保存的试题id和名称，才能保证再次添加试题时回显列表正确	
							$('#quesCourseIds_select').val(moveLi(quesCourseIds, quesId, 'up'));
							$('#quesCourseNames_select').val(moveLi(quesCourseNames, quesName, 'up'));
							$('#quesTypes_select').val(moveLi(quesTypes, quesType, 'up'));
						}else{
							var quesLi = $(quesId_list).parent();
							quesLi.next().after(quesLi);
							//下移试题时，同时修改隐藏域中保存的试题id和名称，才能保证再次添加试题时回显列表正确	
					    	$('#quesCourseIds_select').val(moveLi(quesCourseIds, quesId, 'down'));
							$('#quesCourseNames_select').val(moveLi(quesCourseNames, quesName, 'down'));
							$('#quesTypes_select').val(moveLi(quesTypes, quesType, 'down'));
						}
					}
			    });
			    
			    //点击自测题类型，给对应类型的试题添加背景颜色，方便查看
			    $('#quesTypeView li').each(function(){
					$(this).click(function(){
						$(this).css('background-color', '#FFEC8B').siblings().css('background-color', '');
						var _item = $(this).find('span:eq(0)');
						var typeSpanId = $(_item).attr("id");
						var p = new RegExp('[0-9]+');
						var _key = typeSpanId.match(p);
						p = new RegExp('\S*_'+_key);
						$('#quesCourse li').each(function(index, item){
							var _item2 = $(item).find('input:eq(0)');
							var _questypeId = $(_item2).attr("_questype");
							if(p.test(_questypeId)){
								$(item).css('background-color', '#FFEC8B');
							}else{
								$(item).css('background-color', '');	
							}
						});	
					});
				});	
			    //------------------------课后自测试题操作end------------------------
			    
			    //附件上移、下移
			    $('.attachMove').click(function(){
					var type = $(this).attr('_type');
			    	var attach_list = $("input:checkbox[name='attach_list']:checked");
			    	if (attach_list.length == 0 || attach_list.length > 1) {
						layer.alert("请选择一条数据!", -1);
						return;
					} else {
						var fileName = $(attach_list).siblings("div:eq(-2)").attr("_attachFileName");
						var realName = $(attach_list).siblings("div:eq(-2)").text();
						if(type == 'up'){
							var attachLi = $(attach_list).parent();
							attachLi.prev().before(attachLi);
						}else{
							var attachLi = $(attach_list).parent();
							attachLi.next().after(attachLi);
						}
						//重置附件序号
						$("#courseAttachView").find("li").each(function(index, item){
							var $ordernum = $(item).find('div:eq(0)');
							$(item).find('div:eq(0)').text(index+1);
						});
					}
			    });
			    
			    //删除课程的附件
			    $('#delAttach').click(function(){
			    	var attachs = $("input:checkbox[name='attach_list']:checked");
			    	if(attachs.length > 0){
			    		var _layer_confirm = layer.confirm("确定要删除选择的课件吗？", function(){
					    	$(attachs).each(function(){
					    		//删除选中的附件
								var attachLi = $(this).parent();
								$(attachLi).parent().find(attachLi).remove();
								var attachId = $(this).siblings("div:eq(-2)").attr("_attachId");
					    		if($.trim(attachId) != ''){
					    			$.post($.fn.getRootPath() + "/app/learning/course!delCourseFile.htm", {"attachId" : attachId},
										function(data){
											if (data.rst == "1") {
											}else{
												alert(data.msg);
											}
									}, "json");
					    		}
					    		//重置附件序号
								$("#courseAttachView").find("li").each(function(index, item){
									var $ordernum = $(item).find('div:eq(0)');
									$(item).find('div:eq(0)').text(index+1);
								});
							});
							layer.close(_layer_confirm);
							layer.alert("操作成功",-1);
						});
			    	}else{
			    		layer.alert("请选择要删除的课件",-1);
			    	}
			    });
			    //------------------------附件操作end------------------------
			    
			    //删除选中的关联知识
			    $('#knowledge_btn').click(function(){
			    	var knowledges = $("input:checkbox[name='knowledge_list']:checked");
			    	if(knowledges.length > 0){
			    		var _layer_confirm = layer.confirm("确定要删除选择的知识吗？", function(){
					    	var quesIds = $('#quesIds_select').val();
					    	var quesNames = $('#quesNames_select').val();
					    	var queses = $('#queses').val();//试题的key串
					    	$(knowledges).each(function(index, item){
					    		var _quesIds = $(this).next('a').attr('_quesIds');
					    		var _quesNames = $(item).next('a').attr('_quesNames');
					    		var _queses = _quesIds + "||" + _quesNames;
								//删除试题时，同时删除隐藏域中保存的对应的试题id、名称和试题类型的key，才能保证再次添加试题时回显列表正确
								quesIds = delLi(quesIds, _quesIds, ",");
								quesNames = delLi(quesNames, _quesNames, ",");
								queses = delLi(queses, _queses, "##");
					    		$('#quesIds_select').val(quesIds);
					    		$('#quesNames_select').val(quesNames);
					    		$('#queses').val(queses);
					    		//删除选中的试题
								var quesLi = $(this).parent();
								$(quesLi).parent().find(quesLi).remove();
								//编辑的时候从数据库中删除该条知识
								var _rqId = $(this).next('a').attr('_rqId');
								if(_rqId != "undefined" && $.trim(_rqId) != ""){
									$.post($.fn.getRootPath() + "/app/learning/course!delRelatedQues.htm", {"id" : _rqId},
										function(data){
											if (data.rst == "1") {
												
											}else{
												alert(data.msg);
											}
									}, "json");
								}
							});
							layer.close(_layer_confirm);
							layer.alert("操作成功",-1);
						});
			    	}else{
			    		layer.alert("请选择要删除的知识",-1);
			    	}
			    });
			    
				//清空开放对象
				$('#btnClearScope').click(function(){
		    		var _layer_confirm = layer.confirm("确定要清空开放对象吗？", function(){
						$('#scopenames').val('');
						$('#scopeids').val('');
						layer.close(_layer_confirm);
					});
				});
				
				//开放范围为全员时，选择框不可操作
				$("#releaseType").on("change",function(){
					_shadeScopen();
				});
				_shadeScopen = function(){
					if($("#releaseType").val() == '0'){
						$("#releaseScope").hide();
					}else{
						$("#releaseScope").show();
					}
				}
				_shadeScopen();
				//点击知识可进入详情
			    $("#knowledgeView").on("click","a",function(){
					var question = $(this).attr('_quesNames');
					parent.parent.parent.TABOBJECT.open({
						id : 'courseFileInfo',
						name : "知识详情",
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+question,
						isRefresh : true
					}, this);
			    });
			    //点击折叠按钮
			    $("#showOrHideInfo,#showOrHideTest").on("click",function(){
			    	var url = $.fn.getRootPath()+"/resource/learning/images/fold_up.png";
			    	var _this;
			    	if($(this).attr("id") == 'showOrHideInfo'){
			    		_this = $("#infoArea tr").slice(1,10);
			    	}else if($(this).attr("id") == 'showOrHideTest'){
			    		_this = $("#testArea");
			    	}
			    	if(_this.is(":hidden")){
			    		_this.show();
			    		url = $.fn.getRootPath()+"/resource/learning/images/fold_down.png";
			    	}else{
			    		_this.hide();
			    	}
			    	$(this).attr("src",url);
			    });
			    
			});			
			//课程内容富文本插件
			var editor;			
			KindEditor.ready(function(K) {
				editor = K.create('textarea[id="content"]', {
					width:"99%",height:"150px",
		            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist'] 
				});
			});
			
			function addOrUpdate(url, params){
				//提交数据
				$('body').ajaxLoading('正在提交数据...');
				//var params = $("#form1").serialize();
				$.ajax({
			   		type: "POST",
			   		url: url,
			   		data: params,
			   		success: function(data){
			   			$('body').ajaxLoadEnd();
			   			data = jQuery.parseJSON(data);
			   			layer.alert(data.msg,-1,function(){
							parent.layer.close(parent.__kbs_layer_index);
						});
			   			//parent.location.reload();
			   		},
			   		error: function(msg){
			   			alert(msg);
			   			$('body').ajaxLoadEnd();
			   		}
				});
			}
			
			//上传封面图片
			function uploadImg() {
				$.ajaxFileUpload({
					url: '${pageContext.request.contextPath }/app/learning/course!uploadImg.htm', 
					type: 'post',
					secureuri: false, //一般设置为false
					fileElementId: 'coverPic', // 上传文件的id、name属性名
					dataType: 'json', //返回值类型，一般设置为json、application/json
					success: function(data, status){  
						if (data.rst == "1") {
							$('#addCover').attr("src",'${pageContext.request.contextPath }/doc/course/cover/' + data.coverFileName + '');
							//$('#coverPic').next('#imgView').remove();
							$("[name='coverFileName']").val(data.coverFileName);
							$("[name='coverRealName']").val(data.coverRealName);
							/*$('#coverPic').after('<div id="imgView" style="float: left;"><img src="${pageContext.request.contextPath }/doc/course/cover/' + data.coverFileName + '" style="width: 86px; height: 70px; padding-bottom: 2px; margin-right: 3px;"></div>'); */
						} else {
							layer.alert(data.msg, -1);
						}
					},
					error: function(data, status, e){ 
						alert(e);
					}
				});
			}
			
			//删除试题时，同时删除隐藏域中保存的对应的试题id和名称，才能保证再次添加试题时回显列表正确
			/*
			 * originalVal 源字符串
			 * delVal 欲移除的字符串
			 * regex 分隔符
			 */
			function delLi(originalVal, delVal, regex){
				var _originalVal = originalVal.split(regex);
				_originalVal.splice(_originalVal.indexOf(delVal), 1);
				originalVal = _originalVal.join(regex)
	    		return originalVal;
			}
			
			//上移、下移试题时，同时修改隐藏域中保存的试题id和名称，才能保证再次添加试题时回显列表正确	
			function moveLi(originalVal, curVal, type){
				var _originalVal = originalVal.split(",");
				if(type=="up"){
					upMove(_originalVal, _originalVal.indexOf(curVal));
				}else{
					downMove(_originalVal, _originalVal.indexOf(curVal));
				}
		    	return _originalVal.join(",");
			}
		 
			// 上移
			var upMove = function(arr, i) {
				if(i == 0) {
					return;
				}
				swapItems(arr, i, i - 1);
			};
		 
			// 下移
			var downMove = function(arr, i) {
				if(i == arr.length -1) {
					return;
				}
				swapItems(arr, i, i + 1);
			};
			
			// 交换数组元素
			var swapItems = function(arr, index1, index2) {
				arr[index1] = arr.splice(index2, 1, arr[index1])[0];
				return arr;
			};
			
			//上传课件
			function uploadFiles() {
				var upload_idx = layer.load('正在上传，请稍后……');
				$.ajaxFileUpload({
					url: '${pageContext.request.contextPath }/app/util/file-upload!upload.htm', 
					type: 'post',
					secureuri: false, //一般设置为false
					fileElementId: 'attachments', // 上传文件的id、name属性名
					data: { dir: 'course'}, 
					dataType: 'json', //返回值类型，一般设置为json、application/json
					success: function(data, status){  
						if (data.rst == "1") {
							var attachHtml = '';
							attachHtml += '<li style="list-style:none;line-height:30px;">';
							attachHtml += '<input type="checkbox" value="" name="attach_list">&nbsp;';
							attachHtml += '<div style="display: inline; margin-right: 10px;">' + ($("#courseAttachView").find("li").length+1) + '</div>&nbsp;';
							attachHtml += '<div style="display: inline; margin-right: 10px;" _attachId="" _attachFileName="' + data.attachFileName + '" _attachType="' + data.attachType + '">' + data.attachRealName + '</div>';
							attachHtml += '<div style="display: inline;padding-left: 10px;"><input value="" placeholder="添加说明" name="remark"></div>';//附件介绍
							attachHtml += '</li>';
							$("#courseAttachView").append(attachHtml);
						} else {
							alert(data.msg);
						}
					},
					error: function(data, status, e){ 
						alert(e);
						layer.close(upload_idx);
					},
					complete: function () {
						layer.close(upload_idx);
        			}
				});
			}
		</script>
	</head>
	<body>
		<div id="form_main">
		<form id="form1" action="" method="post">
			<input type="hidden" id="courseId" name="courseModel.id" value="${courseModel.id }" />
			<input type="hidden" id="coverFileName" name="coverFileName" value="">
			<input type="hidden" id="coverRealName" name="coverRealName" value="">
			<table id="infoArea" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td colspan="3" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
						<div class="background_theme">
						<font class="font_style">基本信息</font>
						<img id="showOrHideInfo" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
							src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
						</div>
					</td>
				</tr>
				<tr>
					<td width="15%">课程名称<span class="inputflag">*</span></td>
					<td width="75%" style="border-right-style:hidden">
						<input type="text" id="name" name="courseModel.name" value="${courseModel.name }" style="width:550px;" />
					</td>
					<td width="10%" style="border-left-style:hidden" valign="bottom"><span id="title_lenght_show">50/0</span></td>
				</tr>
				<tr>
					<td>开始日期<span class="inputflag"></span></td>
					<td colspan="2">
						<input type="text" id="startTime" name="courseModel.startTime" readonly="readonly" value="${courseModel.startTime}" style="width:150px;" class="Wdate" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})" />
						结束日期
						<input type="text" id="endTime" name="courseModel.endTime" readonly="readonly" value="${courseModel.endTime}" style="width:150px;" class="Wdate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})"/>
						<span style="margin-left:5px;"><font color="red">不填写为永不过期</font></span>
					</td>
				</tr>
				<tr>
					<td>课程难易<span class="inputflag">*</span></td>
					<td colspan="2">
						<s:select id="level" name="courseModel.level" value="#request.courseModel.level" headerKey="" headerValue="-全部-" list="#request.levelList" listKey="key" listValue="name"></s:select>
					</td>
				</tr>
				<tr>
					<td>课程分类<span class="inputflag">*</span></td>
					<td colspan="2">
						<input type="text" id="cateNames_select" name="cateNames" value="${courseModel.courseCate.name}" readOnly="readOnly">
				        <input type="hidden" id="cateIds_select" name="courseModel.courseCate.id" value="${courseModel.courseCate.id}">&nbsp;
					</td>
				</tr>
				<tr>
					<td>开放范围<span class="inputflag">*</span></td>
					<td colspan="2">
						<div style="float: left;">
							<s:select id="releaseType" name="courseModel.releaseType" list="#{0:'全员', 1:'指定'}" headerKey="" headerValue="-请选择-"
								listKey="key" listValue="value" value="#request.courseModel.releaseType"></s:select>
						</div>
						<div id="releaseScope" style="float: left;margin-left: 20px;width: 80%;">
							<input placeholder="选择开放对象" style="width: 80%;line-height: 20px;" type="text" id="scopenames" name="courseModel.scopenames" value="${courseModel.scopenames }" readonly="readonly"/>
							<input type="hidden" id="scopeids" name="courseModel.scopeids" value="${courseModel.scopeids }"/>
							<!-- <a id="btnClearScope" href="javascript:void(0);"><font color="blue">清空</font></a> -->
							<font style="margin-left: 10px;">
								<a id="btnClearScope" href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;margin-top: 2px;"></img>
								</a>
							</font>
						</div>
					</td>
				</tr>
				<tr>
					<td>课程内容<span class="inputflag">*</span></td>
					<td valign="bottom" style="border-right-style:hidden" colspan="2">
						<textarea id="content" name="courseModel.content" style="width:100%;height:150px;visibility:hidden;">${courseModel.content }</textarea>
					</td>
				</tr>
				<tr>
					<td>课程封面<%-- <span class="inputflag">*</span> --%></td>
					<td colspan="2">
						<!-- 设置文件选择框透明度，用预览图覆盖：可兼容ie -->
						<div style="float: left;margin-left: 10px;">
							<s:if test="#request.courseModel.fileName!=null && #request.courseModel.fileName!=''">
								<img id="addCover" src="${pageContext.request.contextPath}/doc/course/cover/${courseModel.fileName }" style="width: 45px;height:45px;">
							</s:if>
							<s:else>
								<img id="addCover" src="${pageContext.request.contextPath}/resource/learning/images/upload_img.png" style="width: 45px;height:45px;">
							</s:else>
						</div>
						<div style="float:left;margin-left: -45px;">
							<input type="file" id="coverPic" name="coverPic" accept="image/*" 
								style="float: left;width: 45px;height: 45px;filter:alpha(Opacity=0);-moz-opacity:0;opacity: 0;cursor: pointer;" 
								onchange="if($(this).val() != ''){uploadImg()}"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>上传课件<span class="inputflag">*</span></td>
					<td colspan="2">
						<!-- <font color="red" style="float: left;margin-right: 10px;margin-top: 5px;margin-left: 10px;">附件不能大于4M</font> -->
						<ul id="courseAttachView" style="margin-top: 10px;">
							<s:if test="#request.courseModel.courseFile!=null && #request.courseModel.courseFile.size>0">
								<s:iterator value="courseModel.courseFile" var="va">
									<li style="list-style:none;line-height:30px;">
										<input type="checkbox" value="" name="attach_list">
										<div style="display: inline; margin-right: 10px;">${va.order+1 }:</div>
										<div style="display: inline; margin-right: 10px;" _attachId="${va.id }" _attachFileName="${va.fileName }" _attachType="${va.fileType }">${va.realName }</div>
										<div style="display: inline;padding-left: 10px;"><input value="${va.remark }" placeholder="添加说明" name="remark"></div>
									</li>
								</s:iterator>
							</s:if>
						</ul>
						<div>
							<font style="float:left;">
								<a href="javascript:void(0);">
									<div style="float: left;">
										<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;height: 15px;"></img>
										添加课件
									</div>
									<div style="float:left;margin-left: -72px;">
										<input type="file" id="attachments" name="attachments" 
											style="float: left;width: 72px;height: 15px;filter:alpha(Opacity=0);-moz-opacity:0;opacity: 0;cursor: pointer;" 
											onchange="if($(this).val() != ''){uploadFiles()}"/>
									</div>
								</a>
							</font>
							<font style="float:left;margin-left: 20px;">
								<a id="delAttach" href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;"></img>
									删除
								</a>
							</font>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						知识来源
					</td>
					<td colspan="2">
						<input id="quesIds_select" type="hidden" name="quesIds" value="${quesIds }"/>
						<input id="quesNames_select" type="hidden" name="quesNames" value="${quesNames }"/>
						<input id="queses" type="hidden" name="queses" value="${queses }"/>
						<div>
							<ul id="knowledgeView" style="margin-top: 7px;">
								<s:if test="#request.courseModel.relatedQueses!=null && #request.courseModel.relatedQueses.size>0">
									<s:iterator value="courseModel.relatedQueses" var="va">
										<li style="list-style:none;line-height:25px;">
											<input type="checkbox" value="" name="knowledge_list">
											<a name="qaLink" _rqId="${va.id }" _quesIds="${va.valueId }" _quesNames="${va.question }">
												${va.question }
											</a>
										</li>
									</s:iterator>									
								</s:if>
							</ul>
						</div>
						<div>
							<font style="float:left;">
								<a id="quesNames_btn" href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/add_icon.png" style="width: 15px;"></img>
									关联知识
								</a>
							</font>
							<font style="float:left;margin-left: 20px;">
								<a id="knowledge_btn" href="javascript:void(0);" style="cursor: pointer;">
									<img src="${pageContext.request.contextPath}/resource/learning/images/del_icon.png" style="width: 15px;"></img>
									删除
								</a>
							</font>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;">
						<div class="background_theme">
						<font class="font_style">课后自测</font>
						<img id="showOrHideTest" style="margin-top: 5px;margin-right:5px;float: right;" width="15px" 
							src="${pageContext.request.contextPath}/resource/learning/images/fold_down.png">
						</div>
					</td>
				</tr>
			</table>
			<table id="testArea" cellspacing="0" cellpadding="0" class="box" style="width:100%;border: 0px;">
				<tr>
					<td width="25%" style="text-align: center;">
						自测限时(<input id="limitMinute" value="${courseModel.limitMinute }" style="width: 40px;">分钟)
					</td>
					<td width="75%">
						<div class="gonggao_titile_right">
							<a href="javascript:void(0);" id="quesMoveDown" class="quesMove" _type="down">下移</a>
							<a href="javascript:void(0);" id="quesMoveUp" class="quesMove" _type="up">上移</a>
							<a href="javascript:void(0);" id="quesDel">删除题目</a>
							<a href="javascript:void(0);" id="quesAdd">添加题目</a>
							<input id="quesCourseNames_select" type="hidden" name="quesCourseNames" value="${quesCourseContents }"/>
							<input id="quesCourseIds_select" type="hidden" name="quesCourseIds" value="${quesCourseIds }"/>
							<input id="quesTypes_select" type="hidden" name="quesTypes" value="${quesTypes }"/><!-- 试题类型的key -->
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div style="margin-top: -70px;">
							<ul id="quesTypeView" style="margin-top: 7px;">
							 	<s:if test="#request.courseQuesTypeJa!=null && #request.courseQuesTypeJa.size>0">
									<s:iterator value="#request.courseQuesTypeJa" var="va">
										<li _questypeid=${va.id } style="list-style:none;line-height:20px; cursor: pointer;">
											${va.name }(<span id="num_${va.key }" class="inputflag"><s:if test="#va.quesNum != null">${va.quesNum }</s:if><s:elseif test="#va.quesNum == null">0</s:elseif></span>)
										</li>
									</s:iterator>									
								</s:if>
							</ul>
						</div>
					</td>
					<td>
						<div style="height: 200px; overflow-y: scroll;">
							<ul id="quesCourse" style="padding: 5px;">
								<s:if test="#request.courseQuesJa!=null && #request.courseQuesJa.size>0">
									<s:iterator value="#request.courseQuesJa" var="va">
										<li style="list-style:none;line-height:20px;">
											<input type="checkbox" value="" _quesname="${va.quesContent }" _questype="${va.quesId }_${va.quesTypeKey }" _quesid="${va.quesId }" name="quesid_list">${va.quesContent }
										</li>
									</s:iterator>									
								</s:if>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		 	<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: left;">
				<a href="javascript:void(0);" onclick="parent.layer.close(parent.layer.getFrameIndex(window.name));">退出</a>
				<a href="javascript:void(0);" class="btnSave" _status='1'>保存不生效</a>
				<a href="javascript:void(0);" class="btnSave" _status='0'>保存生效</a>
			</div>
		</form>
		</div>
	</body>
</html>
