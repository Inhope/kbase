<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>课程关联知识-多选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/> --%>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			body{
				margin: 3px;
			}
			/* .ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			} */
			div {
				width: 240px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 358px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
			input[type="text"]{
				border: 1px solid #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
		
			var _type = '${type}';
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/learning/course!relatedQuesJson.htm",
					autoParam: ["id", "name", "objtype"],
					otherParam:{"nocheck":"true", "mode": _type}
				},
				callback: {
					onClick: ztreeOnCheck
				}
			};
			
			function ztreeOnCheck(event, treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				if (treeNode.objtype==_type){
					if ($("#sel option[value='"+treeNode.id+"']").length==0){
						$("#sel").append('<option value="'+treeNode.id+'">'+treeNode.name+'</option>');
					}
				}
			}
			
			
			var cancelRelatedQuesIds;
			$.extend({
				pushArray: function(array, value){
					if ($.inArray(value, array) == -1) 
						array.push(value);
				}
			});
			$(function(){
				/*取消的节点*/
				var cancelObj = parent.$('#cancelRelatedQuesIds');
				cancelRelatedQuesIds = new Array();
				if(cancelObj.length > 0) 
					if($(cancelObj).val())
						cancelRelatedQuesIds = $(cancelObj).val().split(',');
				
				var ztreeObj = $.fn.zTree.init($("#treeDemo"), setting);
				
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
					}
					
					//回显值
					window.setTimeout(function(){
						if (parent.$("#"+descField).val().length>0){
							var descArr = parent.$("#"+descField).val().split(",");
							var idArr = parent.$("#"+idField).val().split(",");
							
							$(descArr).each(function(i, item){
								//TODO 回显取消勾选树有存在bug，只能回显顶级，而且如果选择项太多会对页面性能有影响
								var ztreeNodes = ztreeObj.getCheckedNodes(false);
								$(ztreeNodes).each(function(j, node){
									if (node.id==$.trim(idArr[i])){
										ztreeObj.checkNode(node, true);
									}
								});
								$("#sel").append('<option value="'+$.trim(idArr[i])+'">'+$.trim(item)+'</option');
							});
						}
					}, 100);
				}
				
				//移除选中的值
				$("#btnMoveout").click(function(){
					var id = $('#sel option:selected').val();
					if (id!=undefined && id.length>0){
						var ztreeNodes = ztreeObj.getCheckedNodes();
						$(ztreeNodes).each(function(i, item){
							if (item.id==id){
								ztreeObj.checkNode(item, false);
							}
						});
					}
					$('#sel option:selected').remove();
				});
				//双击取消选中的select
				$("#sel").dblclick(function(){
					$("#btnMoveout").click();
				});
				
				//确定
				$("#btnOk").click(function(){
					/**
					 * 允许清空选择的值
					 * @author Gassol.Bi
					 */
					 
					 
					var descArr = [];
					var idArr = [];
					//
					var html = '';
					var kbVal = '';
					
					$("#sel option").each(function(i, item){
						descArr.push($(item).text());
						idArr.push($(item).val());
						
						var _id = $(item).val();
						var cont = $(item).text();
						html += '<li style="list-style:none;line-height:25px;">';
						html += '<input type="checkbox" value="" name="knowledge_list">';
						html += ' <a name="qaLink" _quesIds="' + _id + '" _quesNames="' + cont + '">';
						html += cont;
						html += '</a></li>';
						kbVal += '##' + _id + '||' + cont;
						
					});
					
					$(parent.document).find('#knowledgeView').find('li').remove();
					$(parent.document).find('#knowledgeView').append(html); 
					if($.trim(kbVal) != ''){
						$(parent.document).find('#queses').val(kbVal.substring(2));
					}else{
						$(parent.document).find('#queses').val('');
					}
					
					
					if (descField!=null){
						parent.$("#"+descField).val(descArr.join(","));
						parent.$("#"+idField).val(idArr.join(","));
						
						//追加取消选中的节点
						if(cancelObj.length > 0)
							$(cancelObj).val(cancelRelatedQuesIds.join(','));
						else 
							parent.$("#"+descField).after('<input type="hidden" id="cancelRelatedQuesIds" name="cancelRelatedQuesIds" value="' + cancelRelatedQuesIds.join(',') + '" / >');
					}
					
					
					
					$("#btnClose").click();
				});
				
				//取消
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
				
				
				//搜索
				$('#keywords').on('keyup', function(event){
					if (event.keyCode==13){
						$('#btnSearch').click();
					}
				});
				
				$('#btnSearch').on('click', function(){
					var _keyword = $('#keywords').val();
					if (_keyword.length>0){
						$.post('${pageContext.request.contextPath}/app/learning/course!searchRelatedQuesJson.htm', {mode: _type, keyword: _keyword}, function(data){
							if (data!=null){
								var searchNode = ztreeObj.getNodesByParam('id', 'SearchResultNode', null);
								if (searchNode!=''){
									ztreeObj.removeNode(searchNode[0]);
								}
								ztreeObj.addNodes(null, [{id:'SearchResultNode', name:'搜索结果', isParent: 'true', nocheck:'true', open:'true', children: data}]);
								//点击搜索以后滚动条直接跳的底部
								var resH = $("#treeDemo")[0].scrollHeight;
								$("#treeDemo").scrollTop(resH);
								
							}
						}, 'json');
					}
				});
				
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<input type="text" id="keywords">
					<img id="btnSearch" title="搜知识" src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png" style="cursor: pointer;"/>
				</td>
				
				<td width="6%" rowspan="2">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%" rowspan="2">
					<select id="sel" size="28" style="width:230px;height:400px;border:1px solid #F3F3F3;"></select>
				</td>
			
			</tr>
			<tr>
				<td>
					<div class="ztree" id="treeDemo"></div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: right;">
						<a href="javascript:void(0);" class="btnSave" id="btnClose">取消</a>
						<a href="javascript:void(0);" class="btnSave" id="btnOk">确定</a>
					</div>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
