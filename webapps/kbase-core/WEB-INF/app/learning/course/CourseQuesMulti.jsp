<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>添加试题-多选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/> --%>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/learning/css/paper.css" />
		<style type="text/css">
			body{
				margin: 3px;
			}
			
			div {
				width: 240px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 358px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
			input[type="text"]{
				border: 1px solid #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/learning/ques!courseQuesJson.htm",
					autoParam:["id"],
					otherParam:{"nocheck":"true"},
					dataFilter: ztreeAsyncDataFilter
				},
				check: {
					enable: true,
					chkStyle: "checkbox"
				},
				callback: {
					onCheck: ztreeOnCheck
				}
			};
			
			function ztreeOnCheck(event, treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				if (!treeNode.open){
					ztreeObj.expandNode(treeNode);
				}
				
				if (treeNode.checked){
					_appendOption(treeNode);
					//处理所有子节点
					$(_getAllChildren(treeNode)).each(function(i, item){
						_appendOption(item);
					});
					
				}else{
					_removeOption(treeNode);
					//处理所有子节点
					$(_getAllChildren(treeNode)).each(function(i, item){
						_removeOption(item);
					});
					
					/*记录取消选中的节点id*/
					$.pushArray(cancelCateIds, treeNode.id);
				}
			}
			
			function ztreeAsyncDataFilter(treeId, parentNode, responseData) {
				if (parentNode && parentNode.checked) {
					for(var i=0; i<responseData.length; i++) {
						var treeNode = responseData[i];
						treeNode.checked = true;
						//
						_appendOption(treeNode);
					}
				}
				return responseData;
			}
			
			//判断当前treeNode是否是父节点
			function _isParent(treeNode){
				if (treeNode.isParent=='true' || treeNode.isParent==true){
					return true;
				}else{
					return false;
				}
			}
			//select标签增加option
			function _appendOption(treeNode){
				if ($("#sel option[value='"+treeNode.id+"']").length==0){
					//试题类型的值用  （试题id加下划线加类型key ）表示
					$("#sel").append('<option value="'+treeNode.id+'" questype="'+treeNode.id+'_'+treeNode.quesType+'">'+treeNode.name+'</option>');
				}
			}
			//select标签移除option
			function _removeOption(treeNode){
				if ($("#sel option[value='"+treeNode.id+"']").length>0){
					$("#sel option[value='"+treeNode.id+"']").remove();
				}
			}
			
			/**
			 * 递归
			 * 获取节点下所有已展开的子节点
			 */
			function _getAllChildren(treeNode){
				var nodes = [];
				if (treeNode.children && treeNode.children.length>0){
					nodes = $.merge(nodes, treeNode.children);
					
					$(treeNode.children).each(function(i, item){
						nodes = $.merge(nodes, _getAllChildren(item));
					});
				}
				return nodes;
			}
			
			
			var cancelCateIds;
			$.extend({
				pushArray: function(array, value){
					if ($.inArray(value, array) == -1) 
						array.push(value);
				}
			});
			$(function(){
				/*取消的节点*/
				var cancelObj = parent.$('#cancelCateIds');
				cancelCateIds = new Array();
				if(cancelObj.length > 0) 
					if($(cancelObj).val())
						cancelCateIds = $(cancelObj).val().split(',');
				
				var ztreeObj = $.fn.zTree.init($("#treeDemo"), setting);
				
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				var quesTypeField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
						quesTypeField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
						quesTypeField = paramArr[2];
					}
					
					//回显值
					window.setTimeout(function(){
						if (parent.$("#"+descField).val().length>0){
							var descArr = parent.$("#"+descField).val().split(",");
							var idArr = parent.$("#"+idField).val().split(",");
							var queryTypeArr = parent.$("#"+quesTypeField).val().split(",");

							$(descArr).each(function(i, item){
								//TODO 回显取消勾选树有存在bug，只能回显顶级，而且如果选择项太多会对页面性能有影响
								var ztreeNodes = ztreeObj.getCheckedNodes(false);
								$(ztreeNodes).each(function(j, node){
									if (node.id==$.trim(idArr[i])){
										ztreeObj.checkNode(node, true);
									}
								});
								$("#sel").append('<option value="'+$.trim(idArr[i])+'" questype="'+$.trim(queryTypeArr[i])+'">'+$.trim(item)+'</option');
							});
						}
					}, 100);
				}
				
				//移除选中的值
				$("#btnMoveout").click(function(){
					var id = $('#sel option:selected').val();
					if (id!=undefined && id.length>0){
						var ztreeNodes = ztreeObj.getCheckedNodes();
						$(ztreeNodes).each(function(i, item){
							if (item.id==id){
								ztreeObj.checkNode(item, false);
							}
						});
					}
					$('#sel option:selected').remove();
				});
				//双击取消选中的select
				$("#sel").dblclick(function(){
					$("#btnMoveout").click();
				});
				
				//确定
				$("#btnOk").click(function(){
					/**
					 * 允许清空选择的值
					 * @author Gassol.Bi
					 */
					var descArr = [];
					var idArr = [];
					var queryTypeArr = [];
					var descHtml = '';
					
					
					//保存之前先将以前的试题类型数量置0
					$(parent.document).find("span[id^=num_]").text(0);
					
					$("#sel option").each(function(i, item){
						descArr.push($(item).text());
						idArr.push($(item).val());
						queryTypeArr.push($(item).attr("questype"));
						
						var _id = $(item).val();
						var cont = $(item).text();
						var _questype = $(item).attr("questype");
						descHtml += '<li style="list-style:none;line-height:20px;">';
						descHtml += '<input type="checkbox" name="quesid_list" _quesid="' + _id + '" _questype="' + _questype + '" _quesname="' + cont + '" value="" />';
						descHtml += cont;
						descHtml += '</li>';
						
						///////////////////////////////////////////
						//该试题对应题型数量加1
						var patten = new RegExp(_id+'_*');
	    				var _qt = _questype.replace(patten, '');
						var cur_num = $(parent.document).find("#num_"+_qt).text();
						$(parent.document).find("#num_"+_qt).text(parseInt(cur_num)+1);
						
						
					});
					
					if (descField!=null){
						$(parent.document).find('#quesCourse').find('li').remove();
						$(parent.document).find('#quesCourse').append(descHtml); 
						
						parent.$("#"+descField).val(descArr.join(","));
						parent.$("#"+idField).val(idArr.join(","));
						parent.$("#"+quesTypeField).val(queryTypeArr.join(","));
						
						
						//追加取消选中的节点
						if(cancelObj.length > 0)
							$(cancelObj).val(cancelCateIds.join(','));
						else 
							parent.$("#"+descField).after('<input type="hidden" id="cancelCateIds" name="cancelCateIds" value="' + cancelCateIds.join(',') + '" / >');
					}
					
					$("#btnClose").click();
				});
				//取消
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
				
				
				//点击搜索按钮，搜索分类
				$('#btnSearch').click(function(){
					var keywords = $('#keywords').val();
					if($.trim(keywords) == ''){
						return;
					}else{
						$.post('${pageContext.request.contextPath}/app/learning/ques!searchCourseQuesJson.htm', {'keywords': keywords}, function(data){
							if (data!=null){
								var searchNode = ztreeObj.getNodesByParam('id', 'SearchResultNode', null);
								if (searchNode!=''){
									ztreeObj.removeNode(searchNode[0]);
								}
								ztreeObj.addNodes(null, [{id:'SearchResultNode', name:'搜索结果', isParent: 'true', nocheck:'true', open:'true', children: data}]);
								//点击搜索以后滚动条直接跳的底部
								var resH = $("#treeDemo")[0].scrollHeight;
								$("#treeDemo").scrollTop(resH);
								
							}
						}, 'json');
					}
				});
				//回车，搜索分类
				$('#keywords').bind('keydown', function(event) {
					if (event.keyCode=="13") {
						$('#btnSearch').click();
					}
				});
				
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<input type="text" id="keywords">
					<img id="btnSearch" title="搜分类" src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png" style="cursor: pointer;"/>
				</td>
				
				<td width="6%" rowspan="2">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%" rowspan="2">
					<select id="sel" size="28" style="width:230px;height:400px;border:1px solid #F3F3F3;"></select>
				</td>
			
			</tr>
			<tr>
				<td>
					<div class="ztree" id="treeDemo"></div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="sub_btn" style="width: 99%; height: 40px;top: 2px;left: 2px;border: 0;float: right;">
						<a href="javascript:void(0);" class="btnSave" id="btnClose">取消</a>
						<a href="javascript:void(0);" class="btnSave" id="btnOk">确定</a>
					</div>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
