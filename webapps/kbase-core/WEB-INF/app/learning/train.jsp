<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>我的学习</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript">
			$(function(){
				//点击全部课程
				$("#allCourse").on("click",function(){
					parent.parent.parent.TABOBJECT.open({
						id : 'courseList',
						name : '课程目录',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!list.htm',
						isRefresh : true
					}, this);
				});
				//点击课程
				$("[class='px_hang'] [class='px_xk']").on("click",function(){
					var _courseId = $(this).attr("_value");
					parent.parent.parent.TABOBJECT.open({
						id : 'courseInfo',
						name : '课程学习',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+_courseId,
						isRefresh : true
					}, this);
				});
				//点击分类
				$("[class='px_b2_left'] [class='px_b_con'] ul li").on("click","font[name='cateName']",function(){
					var _cateId = $(this).attr("_id");
					console.info(_cateId);
					parent.parent.parent.TABOBJECT.open({
						id : 'courseList',
						name : '课程目录',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!list.htm?cateId='+_cateId,
						isRefresh : true
					}, this);
				});
				//点击我的课程
				$("a[name='myCourse']").on("click",function(){
					parent.parent.parent.TABOBJECT.open({
						id : 'myCourse',
						name : '我的课程',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!myCourse.htm?type=1',
						isRefresh : true
					}, this);
				});
				//我的培训计划-折叠
				$("ul li[class='px_kc_03']").on("click",function(){
					if($("#showPlan").is(":hidden")){
						$("#showPlan").show();
						$("#foldsBtn").attr("src",$.fn.getRootPath() + '/resource/learning/images/px_06.png');
					}else{
						$("#showPlan").hide();
						$("#foldsBtn").attr("src",$.fn.getRootPath() + '/resource/learning/images/px_05.png');
					}
				});
				//点击我的培训安排标签
				$("#statCycle").on("click","li",function(){
					$(this).parent().find("li a").css("background","#edecec");
					$(this).find("a").css("background","white");
					var statCycle = $(this).find("a").attr("value");
					loadTrainForm(statCycle);
				});
				
				loadTrainForm = function(statCycle){
					var _param = "statCycle="+statCycle;
					//异步加载培训任务数据
					$.ajax({
						type: "POST",
						url: $.fn.getRootPath()+"/app/learning/learn!trainTask.htm",
						data: _param,
						success: function(data) {
							data = jQuery.parseJSON(data);
				        	//$('#train_list').find("tr:not(:last)").parent().remove();暂不要"更多"
				        	$('#train_list').empty();
							if(data.trainList.length > 0){
								$(data.trainList).each(function(i, train){
									//$('#train_list').find("tr:last").before($('#trainTemplate').html().fill(暂不要"更多"
									$('#train_list').append($('#trainTemplate').html().fill(
										train.id,
										train.name,
										train.startTime,
										train.endTime==''?'永久有效':train.endTime,
										train.courseId
									));
								});
							}
						}
					});
				}
				loadTrainForm("day");//默认显示今日培训安排
				$("#train_list").on("click","[name='courseInfo']",function(){
					parent.parent.parent.TABOBJECT.open({
						id : 'courseInfo',
						name : '课程学习',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+$(this).attr("_courseId"),
						isRefresh : true
					}, this);
				});
			});
			
			//展开图标点击事件
			function loadMoreCate(_this,id){
				//展开图标样式变换和自己分类隐藏显示
				if($(_this).hasClass("pc_plus")){
					$(_this).removeClass("pc_plus");
					$(_this).addClass("pc_minus");
					$(_this).parent().parent().find("ul").show();
				}else if($(_this).hasClass("pc_minus")){
					$(_this).removeClass("pc_minus");
					$(_this).addClass("pc_plus");
					$(_this).parent().parent().find("ul").hide();
				}
				//追加子级分类
				if($(_this).parent().parent().find("ul").length == 0){
					$.ajax({
						type: "POST",
						url: $.fn.getRootPath()+"/app/learning/learn!courseCateJson.htm",
						data: "id="+id,
						success: function(data) {
							data = jQuery.parseJSON(data);
							$(data).each(function(i, cate){
								$(_this).parent().parent().append($('#cateTemplate').html().fill(
									cate.id,
									cate.name
								));
							});
						}
					});
				}
			}
		</script>
	</head>
<body>
	<div class="page_all">
		<div class="px_b_con">
			<!-- 左边分类树 -->
			<jsp:include page="cateTree.jsp" flush="true">
				<jsp:param value="${cateJa }" name="cateJa"/>
			</jsp:include>
			<div class="px_b_right" style="margin-bottom: 30px;">
				<div class="px_b_top">
					<div class="usbox">
						<ul>
							<li class="px_kc_01"><a href="javascript:void(0);" name="myCourse">我的课程</a></li>
							<li class="px_kc_03" style="float:right;">
								<a href="#">
									<img src="${pageContext.request.contextPath}/resource/learning/images/px_03.png" style="margin-right: 7px;"/>
									我的培训安排
									<img id="foldsBtn" src="${pageContext.request.contextPath}/resource/learning/images/px_05.png" style="margin-left: 7px;"/>
								</a>
							</li>
						</ul>
					</div>
					<div id="showPlan" style="display: none;position: absolute;z-index: 999999;background: white;
						right: 0px;margin-top:7px;width: 250px;height: 170px;border: 1px solid #CDCDCD;">
						<div style="height: 100%;float: left;width: 100%;">
							<div style="float: left;width: 100%;line-height: 30px;margin-left: 5px;margin-top:5px;">
								<ul id="statCycle">
									<li style="float: left;margin-top: -5px;">
										<a value="day" href="javascript:void(0);" style="background:white;padding:3px 10px;border:#e0e0e0 solid 1px;">今日</a>
									</li>
									<li style="float: left;margin-top: -5px;">
										<a value="week" href="javascript:void(0);" style="background:#edecec;padding:3px 10px;border:#e0e0e0 solid 1px;">本周</a>
									</li>
									<li style="float: left;margin-top: -5px;">
										<a value="month" href="javascript:void(0);" style="background:#edecec;padding:3px 10px;border:#e0e0e0 solid 1px;">本月</a>
									</li>
								</ul>
							</div>
							<div style="border: 1px solid #edecec;float: left;height: 82%;width: 95%;margin-left: 5px;margin-top:-5px;">
								<table style="width: 100%;">
									<tr>
										<td style="width: 60%;"><b>培训名称</b></td>
										<td style="width: 40%;"><b>结束日期</b></td>
									</tr>
								</table>
								<table style="width: 100%;" id="train_list">
									<!-- <tr>
										<td colspan="2" style="text-align: right;">更多</td>
									</tr> -->
								</table>
								<table id="trainTemplate" style="display: none;">
									<tr _id="{0}">
										<td name="courseInfo" _courseId="{4}" style="width: 60%;"><font style="cursor: pointer;">{1}</font></td>
										<!-- <td style="width: 120px;">2015-11-12 15:12:32</td> -->
										<td style="width: 40%;">{3}</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<div class="usbox">
							<c:if test="${fn:length(newOpenList) > 0}">
							<div class="px_b_d">
								<div class="px_title">
									<span>NEWS</span> &nbsp; 新开课程
								</div>
								<div class="px_hang">
									<ul>
										<c:forEach items="${newOpenList }" var="course">
										<li>
											<div class="px_xk" _value="${course.id}" style="cursor: pointer;">
												<%-- <div class="layer_a blue">
													<div class="px_xk_zi">
														${course.name}
													</div>
												</div> --%>
												<c:if test="${course.fileName==null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/default.jpg" />
												</c:if>
												<c:if test="${course.fileName!=null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/${course.fileName}" />
												</c:if>
												<div class="px_xk_con">
													<ul>
														<li class="fl" style="width: 100%;"><span style="width:55px;">课程名称:</span>${course.name }</li>
														<li class="fl"><span>创建人:</span>${course.createUserNameCN }</li>
														<li class="fl"><span style="width:55px;">学习人数:</span>${course.count }</li>
														<li style="width: 100%;">
															<span>期限:</span>
															<c:if test="${course.endTime == null}">
																永不过期
															</c:if>
															<c:if test="${course.endTime != null}">
																<fmt:formatDate value="${course.startTime }"/> 至 
																<fmt:formatDate value="${course.endTime }"/>
															</c:if>
														</li>
														<c:set var="scoreInt">
															<fmt:formatNumber type="number" value="${course.score }" maxFractionDigits="0"/>
														</c:set>
														<li class="fl" style="width: 100%;">
															<span>评分:</span>
															<c:if test="${scoreInt==0}">
																<div class="px_l_star1"></div>
															</c:if>
															<c:if test="${scoreInt>0}">
																<div class="px_l_star${scoreInt}"></div>
															</c:if>
														</li>
														<div style="clear:both;"></div>
													</ul>
												</div>
											</div>
										</li>
										</c:forEach>
									</ul>
								</div>
							</div>
							</c:if>
							<c:if test="${fn:length(hotList) > 0}">
							<div class="px_b_d" style="margin-top:15px;">
								<div class="px_title">
									<span>HOT</span> &nbsp; 热门选课
								</div>
								<div class="px_hang">
									<ul>
										<c:forEach items="${hotList }" var="course">
										<li>
											<div class="px_xk" _value="${course.id}" style="cursor: pointer;">
												<%-- <div class="layer_a green">
													<div class="px_xk_zi">
														${course.name }
													</div>
												</div> --%>
												<c:if test="${course.fileName==null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/default.jpg" />
												</c:if>
												<c:if test="${course.fileName!=null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/${course.fileName}" />
												</c:if>
												<div class="px_xk_con">
													<ul>
														<li class="fl" style="width: 100%;"><span style="width:55px;">课程名称:</span>${course.name }</li>
														<li class="fl"><span>创建人:</span>${course.createUserNameCN }</li>
														<li class="fl"><span style="width:55px;">学习人数:</span>${course.count }</li>
														<li style="width: 100%;">
															<span>期限:</span>
															<c:if test="${course.endTime == null}">
																永不过期
															</c:if>
															<c:if test="${course.endTime != null}">
																<fmt:formatDate value="${course.startTime }"/> 至 
																<fmt:formatDate value="${course.endTime }"/>
															</c:if>
														</li>
														<c:set var="scoreInt">
															<fmt:formatNumber type="number" value="${course.score }" maxFractionDigits="0"/>
														</c:set>
														<li class="fl" style="width: 100%;">
															<span>评分:</span>
															<c:if test="${scoreInt==0}">
																<div class="px_l_star1"></div>
															</c:if>
															<c:if test="${scoreInt>0}">
																<div class="px_l_star${scoreInt}"></div>
															</c:if>
														</li>
														<div style="clear:both;"></div>
													</ul>
												</div>
											</div>
										</li>
										</c:forEach>
									</ul>
								</div>
							</div>
							</c:if>
							<c:if test="${fn:length(recommendList) > 0}">
							<div class="px_b_d" style="margin-top:15px;">
								<div class="px_title">
									<span>RECOMMEND</span> &nbsp; 推荐课程
								</div>
								<div class="px_hang">
									<ul>
										<c:forEach items="${recommendList }" var="course">
										<li>
											<div class="px_xk" _value="${course.id}" style="cursor: pointer;">
												<%-- <div class="layer_a orange_r">
													<div class="px_xk_zi">
														${course.name }
													</div>
												</div> --%>
												<c:if test="${course.fileName==null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/default.jpg" />
												</c:if>
												<c:if test="${course.fileName!=null}">
													<img src="${pageContext.request.contextPath }/doc/course/cover/${course.fileName}" />
												</c:if>
												<div class="px_xk_con">
													<ul>
														<li class="fl" style="width: 100%;"><span style="width:55px;">课程名称:</span>${course.name }</li>
														<li class="fl"><span>创建人:</span>${course.createUserNameCN }</li>
														<li class="fl"><span style="width:55px;">学习人数:</span>${course.count }</li>
														<li style="width: 100%;">
															<span>期限:</span>
															<c:if test="${course.endTime == null}">
																永不过期
															</c:if>
															<c:if test="${course.endTime != null}">
																<fmt:formatDate value="${course.startTime }"/> 至 
																<fmt:formatDate value="${course.endTime }"/>
															</c:if>
														</li>
														<c:set var="scoreInt">
															<fmt:formatNumber type="number" value="${course.score }" maxFractionDigits="0"/>
														</c:set>
														<li class="fl" style="width: 100%;">
															<span>评分:</span>
															<c:if test="${scoreInt==0}">
																<div class="px_l_star1"></div>
															</c:if>
															<c:if test="${scoreInt>0}">
																<div class="px_l_star${scoreInt}"></div>
															</c:if>
														</li>
														<div style="clear:both;"></div>
													</ul>
												</div>
											</div>
										</li>
										</c:forEach>
									</ul>
								</div>
							</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>
