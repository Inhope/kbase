<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>我的学习</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/Highcharts-4.1.8/js/highcharts.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript">
			//绘制折线图
			function drawLearnTrend(xData,yData){
				$("#drawLearnTrend").highcharts({
					title: {text: ''},
					xAxis: {
						title: {
							text: ''
						},
						categories: xData,
						labels:{
							step:1,//刻度间隔
							formatter:function(){
								if($("[name='lineMonth']").find("option:selected").val()==''){
									return this.value+'月';
								}else{
									return this.value+'周';
								}
							}
						}
					},
					yAxis: {
						title: {
							enabled:false
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}],
			            min:0,
			            allowDecimals:false
					},
					tooltip: {
						formatter:function(){
							if($("[name='lineMonth']").find("option:selected").val()==''){
								return '第'+this.x+'月'+this.series.name+'：'+this.y;
							}else{
								return '第'+this.x+'周'+this.series.name+'：'+this.y;
							}
						}
					},
					legend: {
						enabled:false
					},
					series: [{
						name: '学习数',
						data: yData
					}],
					credits:{
						enabled:false
					}
				});
			}
			//绘制饼图
			function drawErrorType(pieData){
				$('#drawErrorType').highcharts({
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {text: ''},
					tooltip: {
						enabled:false
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false
							},
							showInLegend: true
						}
					},
					series: [{
						type: 'pie',
						data: pieData
					}],
					credits:{
						enabled:false
					},
					legend:{
						layout:'vertical',
						verticalAlign:'middle',
						align:'right',
						itemMarginBottom:5,
						symbolPadding:5,//默认是5px
						labelFormatter:function(){
							return this.name;
						}
					},
					tooltip: {
						headerFormat:'',
						pointFormat: '<b>{point.y}</b>个,占比:'+'<b>{point.percentage:.1f}%</b>'
					}
				});
			}
			//绘制柱状图
			function drawTestTrend(examData){
				$('#drawTestTrend').highcharts({
					chart: {
						type: 'column',
						margin: [ 8, 10, 37, 50]
					},
					title: {text: ''},
					xAxis: {
						categories: [],
						labels: {
							step:1,//刻度间隔
							rotation: -0,//旋转度
							align: 'center',
							style: {
								fontSize: '10px'
							},
							formatter:function(){
								return this.value+1;
							}
						},
						maxStaggerLines:1//轴标签最多显示行数
					},
					yAxis: {
						min: 0,
						max: 100,
						title: {
							text: ''
						},
						tickInterval:20
					},
					tooltip: {
						formatter:function(){
							return '第'+(this.x+1)+'次考试<br>成绩：<b>'+this.y+' 分</b>';
						}
					},
					legend: {
						enabled: false
					},
					series: [{
						data: examData
					}],
					credits:{
						enabled:false
					}
				});
			}
			
			function initLearnTrend(){
				var lineYear = $("[name='lineYear']").find("option:selected").val();
				var lineMonth = $("[name='lineMonth']").find("option:selected").val();
				var _param = "lineYear="+lineYear+"&lineMonth="+lineMonth;
				//异步加载折线图数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!learnTrend.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						drawLearnTrend(data.xDataList,data.yDataList);
					}
				});
			}
			function initErrorType(){
				var pieYear = $("[name='pieYear']").find("option:selected").val();
				var pieMonth = $("[name='pieMonth']").find("option:selected").val();
				var _param = "pieYear="+pieYear+"&pieMonth="+pieMonth;
				//异步加载饼图数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!errorType.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						drawErrorType(data.pieList);
					}
				});
			}
			function initTestTrend(){
				var columnYear = $("[name='columnYear']").find("option:selected").val();
				var columnHalf = $("[name='columnHalf']").find("option:selected").val();
				var _param = "columnYear="+columnYear+"&columnHalf="+columnHalf;
				//异步加载柱图数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!testTrend.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						drawTestTrend(data.examDatas);
					}
				});
			}
			
			function initExamTask(){
				var examMonth = $("[name='examMonth']").find("option:selected").val();
				var _param = "examMonth="+examMonth;
				//异步加载考试任务数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!examTask.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						$('#exam_list').find("ul[name='data']").remove();
						if(data.examList.length > 0){
							$(data.examList).each(function(i, exam){
								var _isOverDate;
								if(exam.isOverDate==1){
									_isOverDate = '<li class="pers_10 gray">已过期</li>';
								}else if(exam.isOverDate==0){
									_isOverDate = '<li class="pers_10 orange">未过期</li>';
								}
								var createUserNameCN = exam.createUserNameCN.substring(0,4);
								var titleName = exam.createUserNameCN;
								if(exam.createUserNameCN.length > 5){
									createUserNameCN += "...";
								}
								$('#exam_list').append($('#examTemplate').html().fill(
									exam.id,
									exam.name,
									exam.type,
									exam.startTime.substring(0,10),
									exam.endTime.substring(0,10),
									createUserNameCN,
									_isOverDate,
									titleName
								));
							});
						}else{
							//layer.alert("该月无数据",-1);
							return false;
						}
					}
				});
			}
			
			function initTrainTask(){
				var trainMonth = $("[name='trainMonth']").find("option:selected").val();
				var _param = "trainMonth="+trainMonth;
				//异步加载培训任务数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!trainTask.htm",
					data: _param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						$('#train_list').find("ul[name='data']").remove();
						if(data.trainList.length > 0){
							$(data.trainList).each(function(i, train){
								var typeName;
								if(train.type==1){
									typeName = '必修';
								}else if(train.type==2){
									typeName = '选修';
								}
								var _isOverDate;
								if(train.isOverDate==1){
									_isOverDate = '<li class="pers_10 gray">已过期</li>';
								}else if(train.isOverDate==0){
									_isOverDate = '<li class="pers_10 orange">未过期</li>';
								}
								var startTime = train.startTime;
								var endTime = train.endTime;
								if(startTime == ''){
									startTime = "&nbsp;";
								}
								if(endTime == ''){
									endTime = "&nbsp;";
								}
								var userName = train.userName.substring(0,4);
								var titleName = train.userName;
								if(train.userName.length > 5){
									userName += "...";
								}
								$('#train_list').append($('#trainTemplate').html().fill(
									train.id,
									train.name,
									typeName,
									userName,
									startTime,
									endTime,
									_isOverDate,
									train.courseId,
									titleName
								));
							});
						}
					}
				});
			}
			
			$(function () {
				initLearnTrend();//初始化学习趋势
				initErrorType();//初始化错误题型
				initTestTrend();//初始化成绩趋势
				initExamTask();//初始化考试任务
				initTrainTask();//初始化培训任务
				//年月change事件
				$("[name='lineYear'],[name='lineMonth']").on("change",function(){
					initLearnTrend();
				});
				$("[name='pieYear'],[name='pieMonth']").on("change",function(){
					initErrorType();
				});
				$("[name='columnYear'],[name='columnHalf']").on("change",function(){
					initTestTrend();
				});
				$("[name='examMonth']").on("change",function(){
					initExamTask();
				});
				$("[name='trainMonth']").on("change",function(){
					initTrainTask();
				});
				//点击培训任务，定位到课程详情
				$("#train_list").on("click","ul[class='px_lb_c'] [name='courseInfo']",function(){
					parent.parent.parent.TABOBJECT.open({
						id : 'courseInfo',
						name : '课程学习',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+$(this).attr("_courseId"),
						isRefresh : true
					}, this);
				});
			});
		</script>
	</head>
<body>
	<div class="page_all">
		<div class="usbox" style="width: 100%;">
			<div class="px_mystudy">
				<ul>
					<li>题目<br><span>${countJson.quesCount }</span></li>
					<li>试卷<br><span>${countJson.paperCount }</span></li>
					<li>课程<br><span>${countJson.courseCount }</span></li>
					<li>知识<br><span>${countJson.kbCount }</span></li>
					<div style="clear:both"></div>
				</ul>
			</div>
		</div> 
		<div class="px_usbox">
			<div class="px_box">
				<div class="usbox">
					<ul class="px_from">
						<li style="height: 250px;">
							<div class="px_xx_title px_01">
							学习趋势图
							</div>
							<div class="px_select" style="width: 165px;">
								<div class="px_s fl" style="margin-right: 10px;">
									<select name="lineYear">
										<c:forEach items="${yearList }" var="year">
											<option value="${year }">${year }年</option>
										</c:forEach>
									</select>
								</div>
								<div class="px_s fl">
									<select name="lineMonth">
										<option value="">全部</option>
										<c:forEach items="${monthList }" var="month">
											<option value="${month }">${month }月</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="px_box_from">
								<div id="drawLearnTrend" style="height: 87%;border: 1px solid transparent;"></div>
							</div>
						</li>
						<li style="height: 250px;">
							<div class="px_xx_title px_03">
							错题型分布
							</div>
							<div class="px_select" style="width: 165px;">
								<div class="px_s fl" style="margin-right: 10px;">
									<select name="pieYear">
										<c:forEach items="${yearList }" var="year">
											<option value="${year }">${year }年</option>
										</c:forEach>
									</select>
								</div>
								<div class="px_s fl">
									<select name="pieMonth">
										<c:forEach items="${monthList }" var="month">
											<option value="${month }" ${month==nowMonth?"selected":"" }>${month }月</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="px_box_from">
								<div id="drawErrorType" style="height:87%;border: 1px solid transparent;"></div>
							</div>
						</li>
						<div style="clear:both;"></div>
					</ul>
					<ul class="px_from_b">
						<li style="height: 250px;">
							<div class="px_xx_title px_02">
							个人成绩趋势图
							</div>
							<div class="px_select" style="width: 165px;">
								<div class="px_s fl" style="margin-right: 10px;">
									<select name="columnYear">
										<c:forEach items="${yearList }" var="year">
											<option value="${year }">${year }年</option>
										</c:forEach>
									</select>
								</div>
								<div class="px_s fl">
									<select name="columnHalf">
										<option value="3">全部</option>
										<option value="1">上半年 </option>
										<option value="2">下半年 </option>
									</select>
								</div>
							</div>
							<div class="px_box_from">
								<div id="drawTestTrend" style="height:87%;border: 1px solid transparent;"></div>
							</div>
						</li>
						<div style="clear:both;"></div>
					</ul>
					<div class="px_lb">
						<div class="px_lba">
							<div class="px_lb_title">
								<div class="px_ks fl">考试任务</div>
								<div class="px_s fr">
									<select name="examMonth">
										<option value="">全部</option>
										<c:forEach items="${monthList }" var="month">
											<option value="${month }">${month }月 </option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="px_lb_con" id="exam_list">
								<ul clazs="px_lb_c first_ul">
									<li class="pers_35">考试名称</li>
									<!-- <li class="pers_20">类型</li> -->
									<li class="pers_15">出卷人</li>
									<li class="pers_20">开始日期</li>
									<li class="pers_20">结束日期</li>
									<li class="pers_10">状态</li>
								</ul>
							</div>
							<div style="display: none;" id="examTemplate">
								<ul class="px_lb_c" id="{0}" name="data">
									<li class="pers_35">{1}</li>
									<!-- <li class="pers_20">{2}</li> -->
									<li class="pers_15" title="{7}">{5}</li>
									<li class="pers_20">{3}</li>
									<li class="pers_20">{4}</li>
									{6}
								</ul>
							</div>
							<div style="clear:both;"></div>
						</div> 
						<div class="px_lba">
							<div class="px_lb_title">
								<div class="px_ks fl">培训任务</div>
								<div class="px_s fr">
									<select name="trainMonth">
										<option value="">全部</option>
										<c:forEach items="${monthList }" var="month">
											<option value="${month }">${month }月 </option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="px_lb_con" id="train_list">
								<ul clazs="px_lb_c first_ul">
									<li class="pers_35">培训任务</li>
									<li class="pers_10">类型</li>
									<li class="pers_15">创建人</li>
									<li class="pers_20">开始时间</li>
									<li class="pers_10">状态</li>
								</ul>
								<div style="clear:both;"></div>
							</div>
							<div style="display: none;" id="trainTemplate">
								<ul class="px_lb_c" id="{0}" name="data">
									<li class="pers_35" _courseId="{7}" name="courseInfo"><font style="cursor: pointer;">{1}</font></li>
									<li class="pers_10">{2}</li>
									<li class="pers_15" title="{8}">{3}</li>
									<li class="pers_20">{4}</li>
									<li class="pers_10">{6}</li>
								</ul>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
