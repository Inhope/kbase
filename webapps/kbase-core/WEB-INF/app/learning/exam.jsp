 <%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>我的学习</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/learning/css/learnCenter.css" type="text/css"></link>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
	<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
	<script type="text/javascript">
		function toExam(id,isOpenBook){
			parent.__kbs_layer_index = $.layer({
				type: 2,
				border: [2, 0.3, '#000'],
				title: ['在线考试', 'font-size:14px;font-weight:bold;'],
				closeBtn: [0, true],
				iframe: {src :$.fn.getRootPath() + '/app/learning/examinee!findexampaper.htm?id='+id},
				area: ['100%', '100%'],
				shade: [0.3 , '#000' , true],
				moveOut: false,
				close:function(){
					if(isOpenBook==0){
						//判断是否在考试中，是则保存考试记录
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/learning/examinee!findExaminee.htm',
							traditional: true,
							data :{"id":id},
							async: true,
							dataType : 'json',
							success : function(data){
								if(data.status == 1){//考试中
									$.ajax({
										type : 'post',
										url : $.fn.getRootPath() + '/app/learning/examinee!saveExaminee.htm',
										traditional: true,
										data :{"examId":id,"intervalDate":data.intervalDate},
										async: true,
										dataType : 'json',
										success : function(data){
											window.location.reload();
										},
										error : function(msg){
											layer.alert("网络错误，请稍后再试!", -1);
										}
									});
								}
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					}
				}
			});
		}
		
		function seeinfo(id,examinee_id){
			parent.__kbs_layer_index = $.layer({
				type: 2,
				border: [2, 0.3, '#000'],
				title: ['查看试卷', 'font-size:14px;font-weight:bold;'],
				closeBtn: [0, true],
				iframe: {src :$.fn.getRootPath()+'/app/learning/examinee!findUserpaper.htm?id='+id+"&examinee_id="+examinee_id},
				area: ['100%', '100%']
			});
		}
		
		function pageClick(pageNo){
			$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/examinee.htm");
			$("#pageNo").val(pageNo);
			$("#form0").submit();
		}
		
		$(function(){
			$("#day,#week,#month").on("click",function(){
				$(this).parent().find("li").removeClass("ts");
				$(this).addClass("ts");
				var param = {
					status:0,//0:待考试,1:已完成,2:过期未考试
					statCycle:$(this).attr("id")
				};
				//异步加载待考试数据
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/learn!examTask.htm",
					data: param,
					success: function(data) {
						data = jQuery.parseJSON(data);
						$('[class="ks_time_box"]').empty();
						if(data.examList.length > 0){
							$(data.examList).each(function(i, exam){
								$('#exam_list').append($('#examTemplate').html().fill(
									exam.id,
									exam.name,
									exam.startTime.substring(0,10),
									exam.startTime.substring(10,16)
								));
							});
						}else{
							return false;
						}
					}
				});
			});
			$("#day").click();
		});
	</script>
</head>
<body>
	<div class="page_all" style="width: 100%;">
		<div class="px_b_con">
			<div class="px_ks_left" style="width: 22%;">
				<div class="px_ks2_left" style="width: 100%;">
					<div class="px_b_title">
						<img src="${pageContext.request.contextPath}/resource/learning/images/ks_time_03.jpg" style="margin-top: 7px;margin-right:5px;">
						我的考试安排
					</div> 
					<div class="ks_time">
						<ul class="ks_title">
							<li class="ts" id="day"><a href="javascript:void(0);">今日</a></li>
							<li id="week"><a href="javascript:void(0);">本周</a></li>
							<li id="month"><a href="javascript:void(0);">本月</a></li>
						</ul>
						<ul class="ks_time_box" id="exam_list">
							<!-- 考试安排列表 -->
						</ul>
						<ul id="examTemplate" style="display: none;">
							<li>
								<ul>
									<li class="p1">{1}</li>
									<li class="p2">{2}</li>
									<li class="p3">{3}</li>
								</ul>
							</li>
						</ul>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
			<form id="form0" action="" method="post">
			<div class="px_ks_right" style="width: 77%;float: left;">
				<div class="px_ks_top">
					<div class="usbox">
						<div class="ps_right" style="width:100%; float:right;">
							<div class="px_ks_zt">
								<div class="fl">状态：</div>
								<div class="px_s fl">
									<select name="examineeinfo.status">
										<option value="">--请选择--</option>
										<option value="0" ${examineeinfo.status==0?"selected":""}>待考试</option>
										<option value="1" ${examineeinfo.status==1?"selected":""}>考试中</option>
										<option value="2" ${examineeinfo.status==2?"selected":""}>已完成</option>
										<option value="3" ${examineeinfo.status==3?"selected":""}>未考已过期</option>
									</select>
								</div>
							</div>
							<div class="px_ks_zt">
								<div class="fl">&nbsp;&nbsp;合格：</div>
								<div class="px_s fl">
									<select name="examineeinfo.score_status">
										<option value="">--请选择--</option>
										<option value="1" ${examineeinfo.score_status==1?"selected":""}>合格</option>
										<option value="0" ${examineeinfo.score_status==0?"selected":""}>未合格</option>
									</select>
								</div>
							</div>
							<div class="px_ks_zt">
								<!-- <a href="#"> -->
									<div class="kbs-inputbox fl">
										<input name="examineeinfo.exam_name" value="${examineeinfo.exam_name }" type="text" class="input-from" placeholder="输入考试名称">
										<img src="${pageContext.request.contextPath}/resource/learning/images/search_03.png"
										 onclick="javascript:pageClick('1');"/>
									</div>
								<!-- </a> -->
							</div>
						</div>
					</div>
				</div>
				<div class="px_b_kc">
					<div class="px_b_box">
						<ul class="px_ks_table"> 
							<li style="width:23%;">考试名称</li>
							<li style="width:19%;">考试时间</li>
							<li style="width:18%;">结束时间</li>
							<li style="width:14%;">状态</li>
							<li style="width:10%;">分数</li>
							<li style="width:10%;">合格</li>
							<li style="width:6%;">操作</li>
						</ul>
						<s:iterator value="page.result" var="va" status="index">
							<ul class="${index.index%2==0?'px_ks_white':'px_ks_blue'}">
								<li style="width:23%;">${va.exambatch.exam.name }</li>
								<li style="width:19%;"><fmt:formatDate  value="${va.exambatch.start_time }" pattern="yyyy-MM-dd HH:mm" /></li>
								<li style="width:18%;"><fmt:formatDate  value="${va.exambatch.end_time}" pattern="yyyy-MM-dd HH:mm" /></li>
								<li style="width:14%;">
									<s:if test="#va.status==0">待考试</s:if>
									<s:elseif test="#va.status==1">考试中</s:elseif>
									<s:elseif test="#va.status==2">已完成</s:elseif>
									<s:elseif test="#va.status==3">未考试,已过期</s:elseif>
								<li style="width:10%;"><s:if test="#va.examRecord.is_publish==1">${va.examRecord.score }</s:if></li>
								<li style="width:10%;">
									<s:if test="#va.examRecord.is_publish==1">
										<s:if test="#va.examRecord.score<#va.paper.scorePass">未合格</s:if><s:if test="#va.examRecord.score>=#va.paper.scorePass">合格</s:if></td>
									</s:if>
								</li>
								<s:if test="#va.status<2"><!-- 0未考试;1考试中;2考试结束;3已过期 -->
									<li class="exam_btn" style="width:6%;"><a href="#" onclick="toExam('${va.id }','${va.exambatch.exam.isOpenBook }')">考试</a></li>
								</s:if>
								<s:else>
									<li class="ck_btn" style="width:6%;"><a href="#" onclick="seeinfo('${va.examRecord.id}','${va.id }')">查看</a></li>
								</s:else>
							</ul>
						</s:iterator>
						<ul class="px_ks_table">
							<jsp:include page="../util/part_fenye.jsp"></jsp:include>
						</ul>
						<!-- <ul class="px_ks_white">
							<li style="width:23%;">第3周考试第3周考试第3周考试</li>
							<li style="width:19%;">2015-11-18 13:00</li>
							<li style="width:18%;">2016-02-03 12:00</li>
							<li style="width:14%;">待考试</li>
							<li style="width:10%;"></li>
							<li style="width:10%;"></li>
							<li class="ck_btn" style="width:6%;"><a href="#">查看</a></li>
						</ul>
						<ul class="px_ks_blue">
							<li style="width:23%;">2015年第3周考试</li>
							<li style="width:19%;">2015-11-18 13:00</li>
							<li style="width:18%;">2016-02-03 12:00</li>
							<li style="width:14%;">待考试</li>
							<li style="width:10%;"></li>
							<li style="width:10%;"></li>
							<li class="exam_btn" style="width:6%;"><a href="#">考试</a></li>
						</ul> -->
					</div>
				</div>
			</div>
			</form>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>