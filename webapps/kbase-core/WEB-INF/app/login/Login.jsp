<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<c:choose>
			<c:when test="${not empty system_shortcut_icon}">
				<link href="${pageContext.request.contextPath}/theme/${system_shortcut_icon }?t=20150201" type="image/x-icon" rel="shortcut icon">
			</c:when>
			<c:otherwise>
				<link href="${pageContext.request.contextPath}/resource/login/images/logo.ico" type="image/x-icon" rel="shortcut icon">
			</c:otherwise>
		</c:choose>
		<title>${system_title} -- 登录</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/login/css/login.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jQuery.md5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/login/js/Login.js"></script>
		<script type="text/javascript">
			/*
			 * 整个父页面都指向登录界面，内嵌后会导致系统出错 hidden by eko.zhan at 2016-01-08 09:10
			if(window.top != window.self) {
				window.parent.location.href = '${pageContext.request.contextPath}/main.htm';
			}*/
		</script>
	</head>

	<body>
		<div class="top">
			<div class="top_logo">
				<c:choose>
					<c:when test="${empty system_login_title and empty system_login_logo}"><b>${system_title}</b></c:when>
					<c:when test="${not empty system_login_title}"><b>${system_login_title}</b></c:when>
					<c:otherwise><img src="${pageContext.request.contextPath}/theme/${system_login_logo }?t=20150201" style="margin:3px;margin-top:12px;"/></c:otherwise>
				</c:choose>
			</div>
		</div>

		<div class="login">
			<div class="login_left">
				<img src="${pageContext.request.contextPath}/resource/login/images/login_tu1.jpg" width="500" height="356" />
			</div>
			<div class="login_right">
				<form action="${pageContext.request.contextPath}/app/login/login!doLogin.htm" method="post">
					<%-- add by eko.zhan at 2016-06-15 11:50 超时登录后直接转向之前一次的页面，而不是转向main --%>
					<input type="hidden" value="${requestScope.lastUrl }" name="lastUrl" />
					<ul>
						<li class="wz">
							用户名
						</li>
						<li class="kuang">
							<input class="kuang1" name="username" value="" type="text" maxlength="16"/>
							<img src="${pageContext.request.contextPath}/resource/login/images/login_ico1.jpg" width="32" height="32" />
						</li>
						<li class="wz">
							密码
						</li>
						<li class="kuang">
							<input class="kuang2" id="password" value="" type="password" maxlength="32"/>
							<input name="password" type="hidden"/>
							<img src="${pageContext.request.contextPath}/resource/login/images/login_ico2.jpg" width="32" height="32" />
						</li>
						<s:if test="#request.captchaOpen">
						<li class="wz">
							验证码
						</li>
						<li>
							<input class="yan" name="captcha" type="text" maxlength="4"/>
							<img id="captcha" src="${pageContext.request.contextPath}/app/login/login!captcha.htm"
								style="margin: 1px 0 0 3px; height: 32px; width: 58px; cursor: pointer;">
							</img>
						</li>
						</s:if>
						<li>
							<div id="tip" style="margin-top: 15px; color: red; height: 14px;">
								${userError}
							</div>
						</li>
						<li>
							<a href="javascript:void(0);" class="tijiao"><input type="button" id="submit1" value="提交"/>
							</a>
						</li>
					</ul>
				</form>
			</div>
		</div>

	</body>
</html>
