<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>虚拟部门管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css"/>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			$(function(){
			    
				$('#submit').click(function(){
					var scopetype=[];
					$("input:checkbox[id='scopetype']:checked").each(function(i, item){
							scopetype.push($(this).val());
					});
					if($('#virtualname').val()==''){
						layer.alert('虚拟部门名称不能为空', -1);
						return false;
					}if($('#deptnameset').val()==''){
						layer.alert('虚拟部门不能为空', -1);
						return false;
					}if(scopetype.length<=0){
						layer.alert('请至少勾选一个范围', -1);
						return false;
					}
					$.ajax({
					    url:$.fn.getRootPath()+'/app/custom/dh/virtual-dept!savevirtuladept.htm',
						data:{
							"dept.scopetype":scopetype.join(","),
							"dept.virtualname":$("#virtualname").val(),
							"dept.deptnameset":$("#deptnameset").val(),
							"dept.deptbhset":$("#deptbhset").val(),
							"dept.id":$("#deptid").val()
						},
						type:"post",
						dataType:"json",
						success:function(data){
							if(data=="success"){
								layer.alert('操作成功', -1, function(index){
						   			$(parent.document).find('div.content_content').find('iframe').each(function(){
										if (!$(this).is(":hidden")){
											$(this).attr("src", $.fn.getRootPath()+'/app/custom/dh/virtual-dept.htm'); 
											return false;
										}
									});	
						   			parent.layer.close(parent.__kbs_layer_index);
						   		});
							}
					   }
			  　　　　 });
				})
				
				$('#deptnameset').click(function(){
					$.kbase.picker.deptusersearch({returnField:"deptnameset|deptbhset"});
				})
			});
		</script>
	</head>
	<body>
		<div style="margin-top: 30px">
			<form name="form1" id="form1" method="post" action="">
				<input type="hidden" id="deptid" name="dept.id" value="${dept.id }" />
				<table class="box" align="center" style="width:100%">
					<tr>
						<td>虚拟名称</td>
						<td><input type="text" id="virtualname" name="dept.virtualname" value="${dept.virtualname }" style="width:90%;"/></td>
					</tr>
					<tr>
						<td>虚拟部门</td>
						<td><input type="text"  id="deptnameset" name="dept.deptnameset" readonly="readonly" style="width:90%;" value="${dept.deptnameset }"/><input type="hidden"  id="deptbhset" value="${dept.deptbhset }" name="dept.deptbhset"/></td>
					</tr>
					<tr>
						<td>适用范围</td>
						<td><input type="checkbox" <s:if test="#request.dept.scopetype.contains('推荐')">checked="checked"</s:if> id="scopetype"  value="推荐"/>推荐&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" <s:if test="#request.dept.scopetype.contains('公告管理')">checked="checked"</s:if> id="scopetype" value="公告管理"/>公告管理&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"  id="scopetype" <s:if test="#request.dept.scopetype.contains('任务考试')">checked="checked"</s:if> value="任务考试"/>任务考试</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<input type="button" id="submit" class="userShade_aa2" value="提交" />
						</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>

