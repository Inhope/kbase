<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>虚拟部门管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		
		<style type="text/css">
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
		</style>
		
		<script type="text/javascript">
			$(function(){
				  //新建虚拟部门
				  $('#toadd').click(function(){
					  	parent.__kbs_layer_index = parent.$.layer({
								type: 2,
								border: [2, 0.3, '#000'],
								title: ['新建虚拟部门', 'font-size:14px;font-weight:bold;'],
								closeBtn: [0, true],
								iframe: {src : $.fn.getRootPath()+'/app/custom/dh/virtual-dept!toaddvirtualdept.htm'},
								area: ['700px', '600px']
						});
				  })
				  
				  //修改虚拟部门
				  $('#tomodify').click(function(){
				  		var idlength = $("input:checkbox[id='deptid']:checked").length;
				  		var deptid = $("input:checkbox[id='deptid']:checked").val();
						if(idlength<=0 ||　idlength>1){
							parent.layer.alert("请选择一条数据修改", -1);
						}else{
							parent.__kbs_layer_index = parent.$.layer({
								type: 2,
								border: [2, 0.3, '#000'],
								title: ['修改虚拟部门', 'font-size:14px;font-weight:bold;'],
								closeBtn: [0, true],
								iframe: {src : $.fn.getRootPath()+'/app/custom/dh/virtual-dept!toaddvirtualdept.htm?id='+deptid},
								area: ['700px', '600px']
							});
						}
				  })
				  
				  $('#deletedept').click(function(){
				  		var deptids = [];
				  		$("input:checkbox[id='deptid']:checked").each(function(i, item){
							deptids.push($(this).val());
						});
						if(deptids.length<=0){
							parent.layer.alert("请至少选择一条数据", -1);
						}else{
							parent.layer.confirm("确定要删除选中的数据吗?", function(index){
								$.ajax({
								    url:$.fn.getRootPath()+'/app/custom/dh/virtual-dept!deletedept.htm',
									data:{"deptids":deptids.join(",")},
									type:"post",
									dataType:"json",
									success:function(data){
										if(data=='success'){
											layer.alert("操作成功!", -1, function(){
												location.reload();
											});
										}else{
											layer.alert("操作失败!", -1);
										}
								   	}
						  　　　　 });
						  		parent.layer.close(index);
							});
						}
				  });
			});
		</script>
	</head>
		<body>
			<form id="form0" action="" method="post">
				<div class="content_right_bottom" style="min-width:860px;">
					<div class="gonggao_titile">
						<div class="gonggao_titile_right">
						    <a href="javascript:void(0);" id="deletedept">删除虚拟部</a>
						    <a href="javascript:void(0);" id="tomodify">修改虚拟部</a>
						    <a href="javascript:void(0);" id="toadd">新建虚拟部</a>
						</div>
					</div>
					
					<div class="gonggao_con">
						<div class="gonggao_con_nr" style="overflow: auto;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="classinfo">
								<tr>
									<td width="3%">&nbsp;</td>
									<td	align="center" width="20%">虚拟名称</td>
									<td	align="center" width="47%">虚拟部门</td>
									<td	align="center" width="15">适用范围</td>
									<td	align="center" width="15">创建日期</td>
								</tr>
								<s:iterator value="#request.deptlist" var="dept">
									<tr>
										<td><input type="checkbox" id="deptid" value="${dept.id }"/></td>
										<td	align="center">${dept.virtualname }</td>
										<td	align="center" title="${dept.deptnameset }">
											<s:if test="#dept.deptnameset.length()>50">
												<s:property value="#dept.deptnameset.substring(0,50)+'...'" />
											</s:if>
											<s:else>
												${dept.deptnameset}
											</s:else>
										</td>
										<td	align="center">${dept.scopetype }</td>
										<td	align="center">${dept.createDate }</td>
									</tr>
								</s:iterator>
							</table>
						</div>
					</div>
				</div>
			</form>
			<div id="taskinfo"style="position: absolute;z-index:100010;display:none;">
				
			</div>
	</body>
</html>

