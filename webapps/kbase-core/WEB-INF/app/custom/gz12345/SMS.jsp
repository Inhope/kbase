<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>知识库-发短信(广州12345)</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css?2015123001" rel="stylesheet" type="text/css" />
		<style type="text/css">
			table.box {
				font-family: "Courier New", Courier;
				font-size: 12px;
				border: 1px solid #C0C0C0;
				padding: 0px;
			}
			
			.box .th {
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: right;
				width: 15%;
			}
			
			.box .td {
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: left;
				width: 35%;
			}
			
			.box .td input {
				border: 1px solid #C0C0C0;
			}
			
			.box .td input[type="button"],.box .td button {
				cursor: pointer;
				margin-right: 5px;
				font-weight: bolder;
				height: 25px;
			}
			
			.box_duanxin th {
				text-align: center;
				background-color: #C0C0C0;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
			
			.box_duanxin td {
				text-align: center;
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript">
			$(function () {
				var answer = parent.Gz12345.sms.answer;
				$('#answer').text(parent.Gz12345.fn.tagHandle(answer));/*过滤特殊字符*/
				$('table[class="box_duanxin"]').find('tr:eq(0)').
					children('td:eq(1)').html(answer);
			});
			/*短息发送-父页面调用*/
			function sendSMS(){
				PageUtils.submit('#from0', {
					url: '/app/custom/gz12345/gz12345!sendSMS.htm',
					after:function(data){
						$.messager.alert('信息', data.msg, 'info', function(){
				   			if (data.rst)/*发送成功关闭窗口*/
				        	 	parent.$('#dg-dialog').dialog('close'); 
				   		});
				    }
				});
			}
		</script>
	</head>
	<body>
		<form id="from0" action="" method="post">
			<!-- 短信内容 -->
			<textarea rows="1" cols="1" id="answer" name="answer"
				style="display: none;"></textarea>
			<input type="hidden" name="question" value="${param.question }">
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th class="th">
						短信号码：
					</th>
					<td class="td">
						<input name="mobile" class="easyui-validatebox" data-options="required:true,validType:'mobile'" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellspacing="0" width="100%" class="box_duanxin">
							<tr>
								<th width="30%">
									标题
								</th>
								<th width="70%">
									内容
								</th>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div style="width: 100%; max-height: 400px; overflow: auto;">
							<table cellspacing="0" width="100%" class="box_duanxin">
								<tr>
									<td width="30%">
										${param.question }
									</td>
									<td width="70%" style="text-align: left;">
										
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>