<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息-新增|编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/InfoDataEdit.js"></script>
	</head>
	<body class="easyui-layout">
		<form id="form1" method="post" action="" enctype="multipart/form-data">
			<!-- 信息id -->
			<input type="hidden" id="infoDataId" name="id" value="${infoData.id }"/>
			<!-- 模板id -->
			<input type="hidden" name="mouldId" value="${infoMould.id }"/>
			<!-- 为了删除历史文件 -->
			<input type="hidden" id="mapKey" name="mapKey"/>
	    	<div data-options="region:'center'">
	    		<table class="box" cellspacing="0" style="width: 100%; height: auto;">
	    			<s:iterator value="#request.infoMould.columns" var="va">
	    				<s:if test="#va.show">
	    					<tr>
			                    <th width="25%">${va.name }:</th>
			                    <td width="75%">
			                    	<s:set var="prop" value="'col' + #va.index"></s:set>
			                    	<textarea style="display: none;" rows="0" cols="0" prop="${prop }" 
			                    		type="${va.type }">${datas[prop] }</textarea>
			                    </td>
			                </tr>
	    				</s:if>
	    			</s:iterator>
	            </table>
			</div>
		</form>
		
		<!-- 业务选择 -->
		<div id="choose_category-dialog"></div>
	</body>
</html>
