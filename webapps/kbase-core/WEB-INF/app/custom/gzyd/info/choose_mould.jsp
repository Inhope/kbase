<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>选择模板</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:99%;height:99%;}
		</style>
		<style type="text/css">
			body{margin: 3px;}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div { width: 240px;}
			.ztree {border: 1px solid #F3F3F3; height: 358px; overflow: auto;}
			table { border: 1px solid #F3F3F3; }
			td {border:1px dotted #F3F3F3;}
			input[type="text"]{border: 1px solid #F3F3F3;}
			
			.searchNode{
				background-color: #ffe6b0;
			    border: 1px solid #ffb951;
			    color: black;
			    height: 16px;
			    opacity: 0.8;
			    padding-top: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript">
			$(function(){
				var _chkStyle = '${param.chkStyle}';/*树单选radio、多选checkbox*/
				if(!_chkStyle) _chkStyle = 'checkbox';
				var TreeUtils = {
					tree:{
						treeId: 'treeDemo',
						treeObj: null,
						setting:  {
							async: {
								enable: true,
								url: $.fn.getRootPath() + '/app/custom/gzyd/mould-type!syncDatas.htm',
								autoParam:['id'],
								otherParam:{},
								dataFilter: function(treeId, parentNode, childNodes){
									if(childNodes) $(childNodes).each(function(i, o){
										if(o.type == 'type')
											o.nocheck = true;
									});
									return childNodes;
								}
							},
							view: {
								dblClickExpand: false,
								selectedMulti: true
							},
							data: {
								simpleData: {
									enable: true
								}
							},
							check: {
								enable: true,
								radioType: 'all',
								chkStyle: _chkStyle,
								chkboxType: { "Y": "s", "N": "s" }
							},
							callback: {
								onAsyncSuccess: function(event, treeId, treeNode, msg){
									var _that = TreeUtils, /*回显数据*/
										initData = _that.fn.getObjByName('${param.initData}');
									/*数据回显*/
									if(initData.length > 1)/*回掉*/
										initData = initData[1].call(initData[0]);
									else initData = initData();
								},
								beforeClick: function(treeId, treeNode, clickFlag){ 
									var treeObj = $.fn.zTree.getZTreeObj(treeId);
									if(treeNode.type == 'type'){
										if(!treeNode.open)/*关闭分类节点直接展开*/
											treeObj.expandNode(treeNode, true, true, true);
									} else {/*模板节点直接选中*/
										treeObj.checkNode(treeNode, true);
									}
									return true; 
								}
							}
						},
						/*获取树对象*/
						getZTreeObj: function(){
							return $.fn.zTree.getZTreeObj(this.treeId);
						},
						/*树初始化*/
						init: function(){
							/*初始化页面高度*/
							$("#" + this.treeId).css("height", $(window).height()- 85 + 26);
							/*初始化树*/
							return this.treeObj = $.fn.zTree.init($("#" + this.treeId), this.setting);
						}
					},
					fn: {
						searchLabelObj: function (){
							var keywords = $('#keywords').val(), i = 0;/*搜索结果*/
							if(keywords){
								var ztreeObj = TreeUtils.tree.getZTreeObj();
								ztreeObj.getNodesByFilter(function(node){
									var pattern = new RegExp('.*' + keywords + '.*', 'g');
									if(node.type == 'ques' && pattern.test(node.name)){
										ztreeObj.selectNode(node, true);
										i++;
									} else {
										ztreeObj.cancelSelectedNode(node);
									}
									return null;
								}); 
							}
							$('#keywords')[0].focus();
							if(i == 0) parent.layer.alert('未查询到相关题目!', -1);
						},
						/*名称获取父页面对象*/
						getObjByName: function(name){
							var obj = parent;
							if(name.indexOf('.') > -1){
								var _that;
								$(name.split('.')).each(function(i, item){
									_that = obj;/*this对象应改为上一级*/
									obj = $(obj).attr(item);
								});
								return [_that, obj];
							} else {
								return $(obj).attr(name);
							}
						}
					},
					/*初始化*/
					init: function(){
						/*树初始化*/
						var _that = this, ztreeObj = _that.tree.init(), 
						 	callback = _that.fn.getObjByName('${param.callback}');/*回掉函数*/
						
						/*确定*/
						$("#btnOk").click(function(){
							/*返回结果*/
							var data = ztreeObj.getCheckedNodes(true);
							/*回掉*/
							if(callback.length > 1)
								callback[1].call(callback[0], data);
							else callback(data);
							/*关闭窗口*/
							$("#btnClose").click();
						});
						
						/*取消*/
						$("#btnClose").click(function(){
							parent.PageUtils.dialog.close('${param.dialogId }');
						});
						
						/*点击搜索按钮，搜索用户*/
						$('#searchLabelObj').click(function(){
							_that.fn.searchLabelObj();
						});
						
						/*回车，搜索用户*/
						$('#keywords').bind('keydown', function(event) {
							if (event.keyCode=="13") {
								_that.fn.searchLabelObj();
							}
						});
					}
				}
				TreeUtils.init();
			})
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr style="display: none;">
				<td>
					<input type="text" id="keywords" placeholder="请输入题目名称" style="width: 300px;">
					<img id="searchLabelObj" title="搜题目名称"
						src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png"
						style="cursor: pointer;" />
				</td>
			</tr>
			<tr>
				<td>
					<div class="ztree" id="treeDemo" style="width: 99%;"></div>
				</td>
			</tr>
			<tr>
				<td align="right">
					<input type="button" value="确定" id="btnOk"  >
					<input type="button" value="取消" id="btnClose" >
				</td>
			</tr>
		</table>
	</body>
</html>
