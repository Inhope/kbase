<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
			ul.def{width: 100%; list-style:none; margin-left: -24px;}
			ul.def li {
				line-height: 24px;
			}
			
			.datagrid-header-row td{
				background-color:#888888;
				color:#000000;
				font-weight:bold;
			}
    		.datagrid-header td, .datagrid-body td, .datagrid-footer td {
			    border-style: solid;
			    border-width: 0 1px 1px 0;
			    margin: 0;
			    padding: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/ExportUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/InfoData.js"></script>
	</head>
	<body class="easyui-layout">
		<%-- 获取中文地市字段 --%>
	 	<s:set var="locationName" value="''"></s:set>
		<%-- 列表标头数据 --%>
		<s:set var="title" value="#request.infoMould.name"></s:set>
		<%-- anEdit是否允许进行编辑操作（true允许，false不允许）——获取当前地市信息--%>
		<s:if test="!#parameters.anEdit">
			<s:if test='#session.dimTags !=null && #session.dimTags.size()>0'>
				<s:iterator value="#session.dimTags" var="va">
					<s:if test="#va.tag==#session.location">
						<s:set var="locationName" value="#va.name"></s:set>
					</s:if>						    
			   </s:iterator>
			</s:if>
			<%-- 列表标头数据 --%>
			<s:set var="title" value="#title + '  地市：' + #request.locationName"></s:set>
		</s:if>
		
		<%-- 列表开始 --%>
    	<div data-options="region:'center',title:'${title }',iconCls:'icon-ok',split:true">
    		<%-- 默认“生效(发布)时间”、“失效时间”、“地市”的数据索引位置为1、2、3 --%>
    		<s:set var="defProps" value="','"></s:set>
			<s:iterator value="#request.infoMould.columns" var="va">
				<s:if test="#va.show">
					<s:set var="defProps" value="#defProps + #va.index + ',' "></s:set>
				</s:if>
 			</s:iterator>
			<!--导航栏  -->
			<div id="now-toolbar" style="padding:0 5px 5px;height:auto;">
				<div>
					<table width="100%" style="border-bottom: 1px solid #c0c0c0;">
						<tr>
							<td style="width: 400px;">
								<ul class="def">
									<li>
										<s:if test="#defProps.indexOf(',1,') > -1">
											时间区间： 
											<input type="radio" name="period" value="3">最近3天
											<input type="radio" name="period" value="7">最近7天
											<input type="radio" name="period" value="30">最近30天
										</s:if>
									</li>
									<li>
										<s:if test="#defProps.indexOf(',2,') > -1">
											是否过期：
											<input type="radio" name="valid" value="">全部&nbsp;
											<input type="radio" name="valid" value="true">已过期&nbsp;
											<input type="radio" name="valid" value="false">未过期
										</s:if>
									</li>
									<li>
										关键字：<input class="easyui-textbox" id="searCon" style="width:150px;">&nbsp;&nbsp;
										<a href="javascript:void(0);" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
										<a href="javascript:void(0);" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
									</li>
								</ul>
							</td>
							<%-- anEdit是否允许进行编辑操作（true允许，false不允许）——是否显示所有地市 --%>
							<s:if test="#parameters.anEdit">
								<td <s:if test="#defProps.indexOf(',3,') > -1"> 
									style="border-left: 1px solid #c0c0c0;" </s:if>>
									<!-- 地市 -->
									<s:if test="#defProps.indexOf(',3,') > -1">
										<input type="hidden" id="locals">
									</s:if>
								</td>
							</s:if>
						</tr>
					</table>
				</div>
				<%--anEdit是否允许进行编辑操作（true允许，false不允许）——是否显示操作按钮 --%>
				<s:if test="#parameters.anEdit">
					<div style="padding-top:5px;">
						<%--用以判断是否需要显示按钮分界符 --%>
						<s:set var="sum" value="0"></s:set>
						<%--权限按钮-新增、编辑、删除 --%>
						<kbs:if key="gzyd_infoData_add">
							<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" plain="true" id="btn-add" 
								mouldId="${infoMould.id }">新增</a>
							<s:set var="sum" value="#sum+1"></s:set>
						</kbs:if>
						<kbs:if key="gzyd_infoData_edit">
							<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" plain="true" id="btn-edit">编辑</a>
							<s:set var="sum" value="#sum+1"></s:set>
						</kbs:if>
						<kbs:if key="gzyd_infoData_del">
							<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" plain="true" id="btn-del">删除</a>
							<s:set var="sum" value="#sum+1"></s:set>
						</kbs:if>
						<%--按钮分界符 --%>
						<s:if test="#sum>0">
							<span class="datagrid-btn-separator" style="vertical-align: middle; height: 15px;display:inline-block;float:none"></span>
						</s:if>
						<%--权限按钮-导入、导出 --%>
						<kbs:if key="gzyd_infoData_imp">
							<a href="javascript:void(0);" title="导入" class="easyui-linkbutton"  iconCls="icon-undo" plain="true" id="btn-imp">导入</a>
						</kbs:if>
						<kbs:if key="gzyd_infoData_exp">
							<a href="javascript:void(0);" title="导出" class="easyui-linkbutton"  iconCls="icon-redo" plain="true" id="btn-exp">导出</a>
						</kbs:if>
				    </div>
				</s:if>
			</div>
			<!-- 列表 -->
			<%--anEdit是否允许进行编辑操作（true允许，false不允许）——是否显示全选 --%>
			<s:set var="singleSelect" value="true"></s:set>
			<s:if test="#parameters.anEdit">
				<s:set var="singleSelect" value="false"></s:set>
			</s:if>
			<table id="now-grid" class="easyui-datagrid" data-options="toolbar:'#now-toolbar',
				url:'${pageContext.request.contextPath }/app/custom/gzyd/info-data!loadData.htm?mouldId=' + _MouldId,
				queryParams: PageUtils.datagrid.getQueryParams(),
				method:'POST',border:false,rownumbers:true,nowrap:false,pagination:true,fit:true,singleSelect:${singleSelect },
				rowStyler:function(index,row){
					if (index%2!=0){
						return 'background-color:#DDDDDD;';
					}
				},
				onCheck:function(rowIndex,rowData){
					$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#0066FF');
				},
				onUncheck:function(rowIndex,rowData){
					if (rowIndex%2 == 0){
						$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#fff');
					} else {
						$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#DDDDDD');
					}
				},
				onSelectAll:function(rows){
					$('.datagrid-btable tr').css('background-color','#0066FF');
				},
				onUnselectAll:function(rows){
					$('.datagrid-btable tr:even').css('background-color','#fff');
					$('.datagrid-btable tr:odd').css('background-color','#DDDDDD');
				}">
				<thead>
					<tr>
						<th data-options="field:'id',checkbox:true"></th>
						<s:iterator value="#request.infoMould.columns" var="va">
	    					<s:if test="#va.show && (#va.type.key == 'String' || #va.type.key == 'Text' || #va.type.key == 'Date' 
	    						|| #va.type.key == 'Location' || #va.type.key == 'Img' || #va.type.key == 'File' 
	    						|| #va.type.key == 'Category')">
	    						
	    						<%-- 列渲染 --%>
	    						<s:if test="#va.type.key == 'Location'">
	    							<th data-options="field:'col${va.index }',width:${va.width },align:'left',
	    								formatter: function(value, row, index){
	    									return value?value.replace(/\|/g, '、'):value;
	    							}">${va.name }</th>
	    						</s:if>
	    						<s:elseif test="#va.type.key == 'Img'">
	    							<th data-options="field:'col${va.index }',width:${width },align:'left',
	    								formatter: function(value, row, index){
	    										   return InfoData.fn.fmtImg(value, row, index);
	    							}">${va.name }</th>
	    						</s:elseif>
	    						<s:elseif test="#va.type.key == 'File'">
	    							<th data-options="field:'col${va.index }',width:${width },align:'left',
	    								formatter: function(value,row,index){
	    									return InfoData.fn.fmtFile(value, row, index);
	    							}">${va.name }</th>
	    						</s:elseif>
	    						<s:elseif test="#va.type.key == 'Category'">
	    							<th data-options="field:'col${va.index }',width:${width },align:'left',
	    								formatter: function(value, row, index){
	    									if(value && value.indexOf('\|\&\|')>-1){
												var str = value.split('\|\&\|'), 
													ids = str[0].split(','), 
													cates = str[1].split(',');
												/*渲染结果*/
												var htm = '';
												$(ids).each(function(i, o){
													var a = $(document.createElement('a'))[0];
		    										$(a).attr('href', 'javascript:void(0);');
		    										$(a).attr('onclick', 'javascript:InfoData.fn.openCate(\'' + o + '\', \'' + cates[i] + '\', ' 
															+ '\'/app/category/category.htm?type=1&categoryId=' + o + '&categoryName=' + cates[i] + '\')');
		    										$(a).text(cates[i]);
		    										htm += '，' + a.outerHTML;
												});
		    									return htm.replace(/^，|，$/g, '');
											}
	    									return '';
	    							}">${va.name }</th>
	    						</s:elseif>
	    						<s:else>
	    							<th data-options="field:'col${va.index }',width:${width },align:'left'">${va.name }</th>
	    						</s:else>
		    				</s:if>
		    			</s:iterator>
		    			<th data-options="field:'createUser',width:140,align:'left'">创建人</th>
					</tr>
				</thead>
			</table>
		</div> 
		
		<!-- 信息编辑、新增 -->
		<div id="infoData-dialog"></div>
		<!-- 数据导入 -->
		<div id="infoData-import"></div>
		<!-- 信息编辑、新增-图片预览 -->
		<div id="picture-dialog"></div>
		<!-- 列表初始化 -->
		<script type="text/javascript">
			/*模板id*/
			var _MouldId = '${infoMould.id }', 
				_AnEdit = '${param.anEdit }';/*anEdit=true来自管理页面，反之来自分类页面*/;
			/*管理页面默认选中“全部”，分类页面默认选中“未过期”*/
			/*管理页面默认选中“全部”，分类页面默认选中“未过期”*/
			if(_AnEdit == 'true') {
				$('input[type="radio"][name="valid"][value=""]:eq(0)').attr('checked', 'checked');/*ie8*/
				$('input[type="radio"][name="valid"][value=""]:eq(0)').prop('defaultChecked', true);/*ie7*/
			} else {
				$('input[type="radio"][name="valid"][value="false"]:eq(0)').attr('checked', 'checked');/*ie8*/
				$('input[type="radio"][name="valid"][value="false"]:eq(0)').prop('defaultChecked', true);/*ie7*/
			}
			/*列表初始化-公共*/
			PageUtils.init({
				datagrid: $('#now-grid'), /*datagrid对象*/
				getQueryParams: function(){ /*datagrid查询条件*/
					/*检索内容、模板id*/
					var rst = {
						searCon: $('#searCon').textbox('getText')
					};
					/*时间段*/
					if($('#period'))
						rst.period = $('input[name="period"]:checked').val();
					/*是否有效*/
					if($('#valid'))
						rst.valid = $('input[name="valid"]:checked').val();
					/*地市*/
					if(_AnEdit == 'true') {
						var td = $('#locals');
						if(td){
							var locals = new Array();
				            $('input[type="checkbox"][name="locals"]:checked').each(function(){ 
				                locals.push($(this).val());
				            })
				            rst.locals = locals;
						}
					} else {
						 rst.locals = '${locationName }';
					}
					return rst;
				}
			});
			/*功能初始化*/
			$(function(){
				/*回车自动搜索*/
				$('#searCon').textbox('textbox').keydown(function (e) {
           			if (e.keyCode == 13) {
                		$('#now-search').click();
            		}
                });
			});
		</script>
	</body>
</html>
