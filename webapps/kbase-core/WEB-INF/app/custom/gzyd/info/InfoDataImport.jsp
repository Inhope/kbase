<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息-导入</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			/*模板id*/
			var _MouldId = '${param.mouldId }';
			$(function(){
				var that = window.InfoDataImport = {}, 
					fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				fn.extend({
					check: function(obj){
						var fileName = obj.value;
						var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
						if (suffix != ('.xls') && suffix != ('.xlsx')) {
							$.messager.alert('信息', '请用模板文件做为上传文件!');
							obj.parentElement.innerHTML += '';
						}
					}
				});
				that.extend({
					getMould: function(){
						window.open($.fn.getRootPath() 
							+ '/app/custom/gzyd/info-data!getMould.htm?mouldId=' + _MouldId);
					},
					submit: function(){
						$('body').ajaxLoading('正在导入数据...');
						$('#form1').form('submit', {
						    url: $.fn.getRootPath() + '/app/custom/gzyd/info-data!importSave.htm', 
						    onSubmit: function(params){
						    	/*模板id*/
						    	params.mouldId = _MouldId;
						    	return $(this).form('validate');
						    },    
						    success:function(jsonResult){
						    	$('body').ajaxLoadEnd();
						    	jsonResult = eval('(' + jsonResult + ')');
						    	$.messager.alert('信息', jsonResult.msg, 'info', function(){
						   			if (jsonResult.path) {/*导入失败,下载错误日志文件*/
						   				window.open($.fn.getRootPath() + jsonResult.path);
						   			} else {
						   				parent.PageUtils.dialog.close('${param.dialogId }');
										parent.PageUtils.datagrid.reload(1);
						   			}
						   		});
						    }
						});
					}
				});
			});
			
		</script>
	</head>
	<body class="easyui-layout">
		<form id="form1" action="" method="post" enctype="multipart/form-data">
			<div style="width: 360px;margin: 10px;">
				<input type="file" name="file" style="float: left;" 
					onchange="javascript:InfoDataImport.fn.check(this);">
				<br />
				<p style="font-size: 12px;font-weight: bold;">
					注：请严格按照模板(<a href="javascript:void(0);" onclick="InfoDataImport.getMould()" 
						style="color: blue">点击下载</a>)填写相关数据
					<br />
					特别提示：
					<br />
					1、支持的EXCEL为2003及以上
					<br />
					2、上传需要通过修改模板内容来完成
					<br />
					3、上传内容请严格按照模板要求来填写
					<br />
				</p>
			</div>
		</form>
	</body>
</html>
