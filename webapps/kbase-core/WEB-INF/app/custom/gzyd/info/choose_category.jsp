<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>选择目录</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:99%;height:99%;}
		</style>
		<style type="text/css">
			body{margin: 3px;}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div { width: 240px;}
			.ztree {border: 1px solid #F3F3F3; height: 358px; overflow: auto;}
			table { border: 1px solid #F3F3F3; }
			td {border:1px dotted #F3F3F3;}
			input[type="text"]{border: 1px solid #F3F3F3;}
			
			.searchNode{
				background-color: #ffe6b0;
			    border: 1px solid #ffb951;
			    color: black;
			    height: 16px;
			    opacity: 0.8;
			    padding-top: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript">
			$(function(){
				var _chkStyle = '${param.chkStyle}';/*树单选radio、多选checkbox*/
				if(!_chkStyle) _chkStyle = 'checkbox';
				var TreeUtils = {
					tree:{
						treeId: 'treeDemo',
						treeObj: null,
						setting:  {
							async: {
								enable: true,
								url: $.fn.getRootPath() + '/app/points/p-strategy-ext!asyncCategory.htm',
								autoParam:['id'],
								otherParam:{},
								dataFilter: function(treeId, parentNode, childNodes){
									if(childNodes) $(childNodes).each(function(i, o){
										if(o.bh.indexOf('L') == -1) {
											o.nocheck = true;
											o.isParent = true;
										}
									});
									return childNodes;
								}
							},
							view: {
								dblClickExpand: false,
								selectedMulti: true
							},
							data: {
								simpleData: {
									enable: true
								}
							},
							check: {
								enable: true,
								chkStyle: _chkStyle,
								chkboxType: { "Y": "s", "N": "s" }
							},
							callback: {
								onCheck: function(event, treeId, treeNode){
									var _that = TreeUtils.fn;
									/*选中的节点*/
									var nodes = new Array();
									nodes.push(treeNode);
									/*右侧区域回显*/
									if (treeNode.checked) _that.rightHandle(nodes, 'Add');
									else _that.rightHandle(nodes, 'Del');
								},
								onAsyncSuccess: function(event, treeId, treeNode, msg){
									var _that = TreeUtils, /*回显数据*/
										initData = _that.fn.getObjByName('${param.initData}');
									/*数据回显*/
									if(initData.length > 1)/*回掉*/
										initData = initData[1].call(initData[0], '${param.target}');
									else initData = initData('${param.target}');
									if(initData){
										var nodes = initData;
										if(!$.isArray(initData)) {
											nodes = new Array();
											nodes.push(initData);
										}
										if(nodes && nodes.length > 0){
											_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
											_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
										}
									}
								},
								beforeClick: function(treeId, treeNode, clickFlag){ 
									var treeObj = $.fn.zTree.getZTreeObj(treeId);
									if(treeNode.bh.indexOf('L') == -1){
										if(!treeNode.open)/*关闭分类节点直接展开*/
											treeObj.expandNode(treeNode, true, true, true);
									} else {/*模板节点直接选中*/
										treeObj.checkNode(treeNode, true, false, true);
									}
									return true; 
								}
							}
						},
						/*获取树对象*/
						getZTreeObj: function(){
							return $.fn.zTree.getZTreeObj(this.treeId);
						},
						/*树初始化*/
						init: function(){
							/*初始化页面高度*/
							$("#" + this.treeId).css("height", $(window).height()-85);
							$('#sel').css("height", $(window).height()-55);
							/*初始化树*/
							return this.treeObj = $.fn.zTree.init($("#" + this.treeId), this.setting);
						}
					},
					fn: {
						/*右侧区域数据处理*/
						leftHandle: function (objs, flag){
							if(!objs) return false;
							var ztreeObj = TreeUtils.tree.getZTreeObj();
							if(flag == 'Add'){//添加
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) ztreeObj.checkNode(node, true, null);
								});
							} else if(flag == 'Del'){//删除
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) ztreeObj.checkNode(node, false);
								});
							}
						},
						/*左侧区域数据删除*/
						rightHandle: function (objs, flag){
							if(!objs) return false;
							if(flag == 'Add'){/*添加*/
								if(_chkStyle == 'radio') /*单选应当移除所有已有项*/
									$('#sel option').remove();
								$(objs).each(function(i, node){
									if ($("#sel option[value='"+node.id+"']").length == 0){
										$('#sel').append('<option value="' + node.id + '" title="' + node.name + '">' + node.name + '</option>');
										$("#sel option[value='" + node.id + "']").data('content', node.content);
									}
								});
							} else if(flag == 'Del'){/*删除*/
								$(objs).each(function(i, node){
									if ($("#sel option[value='"+node.id+"']").length>0){
										$("#sel option[value='"+node.id+"']").remove();
									}
								});
							}
						},
						/*名称获取父页面对象*/
						getObjByName: function(name){
							var obj = parent;
							if(name.indexOf('.') > -1){
								var _that;
								$(name.split('.')).each(function(i, item){
									_that = obj;/*this对象应改为上一级*/
									obj = $(obj).attr(item);
								});
								return [_that, obj];
							} else {
								return $(obj).attr(name);
							}
						}
					},
					/*初始化*/
					init: function(){
						/*树初始化*/
						var _that = this, ztreeObj = _that.tree.init(), 
						 	callback = _that.fn.getObjByName('${param.callback}');/*回掉函数*/
						
						/*确定*/
						$("#btnOk").click(function(){
							var data = new Array();
							if ($("#sel option").length > 0){
								$("#sel option").each(function(i, item){
									data.push({
										id: $(item).val(), 
										name: $(item).text(),
										content: $(item).data('content')
									});
								});
							}
							if(callback.length > 1) /*回掉*/
								callback[1].call(callback[0], '${param.target}', data);
							else callback('${param.target}', data);
							$("#btnClose").click();
						});
						
						/*取消*/
						$("#btnClose").click(function(){
							parent.PageUtils.dialog.close('${param.dialogId }');
						});
						
						/*移除选中的值*/
						$("#btnMoveout").click(function(){
							var id = $('#sel option:selected').val();
							if (id){
								var nodes = new Array();//回显节点
								nodes.push({'id':id});
								_that.fn.rightHandle(nodes, 'Del');//回显操作（右侧）
								_that.fn.leftHandle(nodes, 'Del');//回显操作（左侧）
							}
						});
						
						/*双击取消选中的select*/
						$("#sel").dblclick(function(){
							$("#btnMoveout").click();
						});
						
						/*移入选中的值*/
						$('#btnMovein').click(function(){
							var nodes = ztreeObj.getSelectedNodes();
							_that.fn.rightHandle(nodes, 'Add');//回显操作（右侧）
							_that.fn.leftHandle(nodes, 'Add');//回显操作（左侧）
						});
					}
				};
				TreeUtils.init();
			})
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<input class="easyui-searchbox" style="width:100%; max-width: ${param.width-20}px;" 
						data-options="prompt:'请输入检索内容!',searcher:function(value, name){
							if(value && !/\s+/.test(value)){
								$('body').ajaxLoading('正在查询数据...');
								$.ajax({
									type : 'post',
									url : $.fn.getRootPath() + '/app/search/diretory-search!search.htm', 
									data : { dirName: value },
									async: false,
									dataType : 'json',
									success : function(jsonResult){
										if(jsonResult.rst){
											var treeObj = $.fn.zTree.getZTreeObj('treeDemo');
											/*查看检索结果节点是否存在*/ 
											var srRst = treeObj.getNodeByParam('id', 'srRst', null);
											if(srRst) treeObj.removeChildNodes(srRst);
											else {
												/*通过tid获取第一个节点*/
												var node = treeObj.getNodeByTId('tree_1');
												treeObj.addNodes(null, {id:'srRst', name:'检索结果', nocheck:true});
												srRst = treeObj.getNodeByParam('id', 'srRst', null);
												treeObj.moveNode(node, srRst, 'prev');
											}
											/*数据处理*/
											var data = jsonResult.data;
											if(data && data.length > 0){
												$(data).each(function(i, o){
													/*不是最终子节点*/
													if(o.bh.indexOf('L') == -1) {
														o.nocheck = true;
														o.isParent = true;
													}
												});
											}
											/*拼接结果*/
											srRst = treeObj.getNodeByParam('id', 'srRst', null);
											treeObj.addNodes(srRst, jsonResult.data);
										} else {
											alert(jsonResult.msg);
										}
									},
									error : function(msg){
										alert('网络错误，请稍后再试!');
									}
								});
								/*延时关闭*/
								var init = setTimeout(function (){
									clearTimeout(init);/*取消定时器*/
									$('body').ajaxLoadEnd();
								}, 500);
							}
						}"></input>
				</td>

				<td width="6%" rowspan="2">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png"
						alt="移入" style="cursor: pointer;" id="btnMovein">
					<br>
					<br>
					<br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png"
						alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%" rowspan="2">
					<select id="sel" size="28" style="width: 100%; height: 400px; border: 1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td>
					<div class="ztree" id="treeDemo"></div>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<input type="button" value="确定" id="btnOk"  >
					<input type="button" value="取消" id="btnClose" >
				</td>
			</tr>
		</table>
	</body>
</html>
