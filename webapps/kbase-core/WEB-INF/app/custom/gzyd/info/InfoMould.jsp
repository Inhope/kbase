<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息模板管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
			
			.datagrid-header-row td{
				background-color:#888888;
				color:#000000;
				font-weight:bold;
			}
			.datagrid-header td, .datagrid-body td, .datagrid-footer td {
			    border-style: solid;
			    border-width: 0 1px 1px 0;
			    margin: 0;
			    padding: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/InfoMould.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/MouldType.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid'), /*datagrid对象*/
					getQueryParams: function(){ /*datagrid查询条件*/
						/*获取当前选中的分类*/
						var type = InfoMould.fn.getSelectedType(),
							typeId = type ? type.id : '';
						return {
							code: $('#code').textbox('getText'),
							name: $('#name').textbox('getText'),
							type: typeId
						};
					}
				});
			});
		</script>
	</head>
	<body class="easyui-layout">
    	<div data-options="region:'west',title:'分类',split:true" style="width:21%;">
    		<div style="margin:10px 10px; text-align: right;">
    			<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" id="btnAddType">新增</a>
    			<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" id="btnEditType">编辑</a>
    			<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" id="btnDelType">删除</a>
			</div>
    	
    		<ul id="type-tree" class="easyui-tree" 
    			data-options="animate:true, url:'${pageContext.request.contextPath}/app/custom/gzyd/mould-type!syncData.htm', 
    				loadFilter: function(data, parent){
    					/*首次加载,加获取的数据添加到指定根节点下*/
    					if(parent == null && data){
    						$(data).each(function(i, o){
    							o.text = o.name;
    							o.state = 'open';
    						});
    						data = [{text:'模板分类', state: 'open', 
    							children: PageUtils.fn.treeData(data)}];
    					}
    					return data;
    				},
    				onClick: function(node){
    					/*刷新列表*/
    					$('#now-search').click();
					}
				">
    		</ul>
    	</div>
    	<div data-options="region:'center',title:'列表',iconCls:'icon-ok',split:true" style="width:79%;">
			<!--导航栏  -->
			<div id="now-toolbar" style="padding:5px;height:auto;">
				<div>
					查找代码：<input class="easyui-textbox" id="code" style="width:150px;">&nbsp;&nbsp;
					名称：	<input class="easyui-textbox" id="name" style="width:150px;">&nbsp;&nbsp;
					<a href="javascript:void(0);" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
				</div>
				<div>
					<%--权限按钮 --%>
					<kbs:if key="gzyd_infoMould_add">
						<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" plain="true" id="btn-add">新增</a>
					</kbs:if>
					<kbs:if key="gzyd_infoMould_edit">
						<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" plain="true" id="btn-edit">编辑</a>
					</kbs:if>
					<kbs:if key="gzyd_infoMould_del">
						<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" plain="true" id="btn-del">删除</a>
					</kbs:if>
			    </div>
			</div>
			<!-- 列表 -->
			<table id="now-grid" class="easyui-datagrid" data-options="toolbar: '#now-toolbar',
				url:'${pageContext.request.contextPath }/app/custom/gzyd/info-mould!loadData.htm', method: 'POST', 
				border:false,rownumbers:true,nowrap:false,pagination:true,fit:true,fitColumns:true,singleSelect:true,
				rowStyler:function(index,row){
					if (index%2!=0){
						return 'background-color:#DDDDDD;color:#000000;';
					}
				},
				onCheck:function(rowIndex,rowData){
					$('.datagrid-btable tr:even').css('background-color','#fff');
					$('.datagrid-btable tr:odd').css('background-color','#DDDDDD');
					$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#0066FF');
				}">
				<thead>
					<tr>
						<th data-options="field:'id',checkbox:true"></th>
						<th data-options="field:'code',width:100,align:'left'">查询代码</th>
						<th data-options="field:'name',width:200,align:'left'">名称</th>
						<th data-options="field:'type',width:100,align:'left'">分类</th>
						<th data-options="field:'createUser',width:80,align:'left'">创建人</th>
						<th data-options="field:'createDate',width:120,align:'left'">创建日期</th>
						<th data-options="field:'total',width:50,align:'right'">关联</th>
						<th data-options="field:'aaa',width:80,align:'center',
							formatter: function(value, row, index){
								var html = '';
			            		var a = $(document.createElement('a'))[0];
								$(a).attr('href', 'javascript:void(0);');
								$(a).attr('onclick', 'javascript:InfoMould.fn.searchData(\'' + row.id + '\', \'' + row.name + '\', ' 
									+ '\'/app/custom/gzyd/info-data.htm?mouldId=' + row.id + '&anEdit=true\')');
								$(a).text('检索');
								html = a.outerHTML
								
			            		return html;
							}">操作</th>
					</tr>
				</thead>
			</table>
		</div> 
		
		<!-- 模板编辑、新增  -->
		<div id="infoMould-dialog"></div>
		<!-- 分类编辑、新增  -->
		<div id="mouldType-dialog"></div>
	</body>
</html>
