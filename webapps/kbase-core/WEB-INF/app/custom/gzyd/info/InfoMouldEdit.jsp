<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息模板管理-新增|编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
			.datagrid-row-selected { 
				background: #c8d47b;
			  	color: #fff;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/datagrid-cellediting.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/InfoMouldEdit.js"></script>
		<script type="text/javascript">
			/*列表数据*/
			var rows = ${rows};
			/*列格式数据*/
			var columnTypes = ${columnTypes}, 
				mapTypes = {};
			$(columnTypes).each(function(i, o){
				mapTypes[o.key] = o.name;
			});
		</script>
	</head>
	<body class="easyui-layout">
		<form id="form1" method="post" >
			<div data-options="region:'north',split:true" style="height:60px">
	    		<table class="box" cellspacing="0" style="width: 100%; height: auto;">
	                <tr>
	                    <th>模板名称:</th>
	                    <td style="border-right:none;">
	                    	<input name="name" class="easyui-textbox" style="width: 100%;" 
	                    		data-options="required:true" value="${infoMould.name }"/>
	                    	<input type="hidden" name="id" value="${infoMould.id }"/>
	                    </td>
	                    <th style="border-right:none;"></th>
	                    <td style="border-right:none;">
	                    </td>
	                </tr>
	                <tr>
	                    <th style="width: 12%;">查询代码:</th>
	                    <td style="width: 38%;">
	                    	<input name="code" class="easyui-textbox" style="width: 100%;" 
	                    		data-options="required:true" value="${infoMould.code }"/>
	                    </td>
	                    <th style="width: 12%;">模板分类:</th>
	                    <td style="width: 38%;">
	                    	<ul class="easyui-combotree" id="ct" name="type" style="width: 100%;"
								data-options="url:'${pageContext.request.contextPath}/app/custom/gzyd/mould-type!syncData.htm', 
								cascadeCheck:false,required:true, value:'${infoMould.type.id }',
				    			loadFilter:function(data, parent){
					   				if(data){
					   					$(data).each(function(i, o){
					   						o.text = o.name;
						   				});
					   				}
					   				return PageUtils.fn.treeData(data);
					   			},
					   			onLoadSuccess:function(node, data){
					   				var t = $('#ct').combotree('tree');
					   				if(t){
					   					var node = t.tree('find', '${infoMould.type.id }');
					   					if(node) {
					   						t.tree('expandTo', node.target);
					   						t.tree('scrollTo', node.target);
					   					}
					   				}
					   			}"></ul>
	                    </td>
	                </tr>
	            </table>
	    	</div>
	    	<div data-options="region:'center',split:true">
	    		<!--导航栏  -->
				<div id="now-toolbar" style="padding:5px;height:auto;">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" 
						onclick="DatagridEdit.append()">添加</a>
	       			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" 
	       				onclick="DatagridEdit.remove()">删除</a>
	       			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-goup',plain:true" 
						onclick="DatagridEdit.up()">上移</a>
	       			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-godown',plain:true" 
	       				onclick="DatagridEdit.down()">下移</a>
	       			<%-- 
	       			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" 
	        			onclick="DatagridEdit.accept()">接受</a>
	        		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true" 
	        			onclick="DatagridEdit.reject()">撤销</a>
	       			--%>
				</div>
				<!-- 列表 -->
				<table id="now-grid" class="easyui-datagrid" data-options="border:false,
					rownumbers:true,nowrap:false,fit:true,fitColumns:true,singleSelect:true,
					toolbar: '#now-toolbar'">
					<thead>
						<tr>
							<th data-options="field:'name',width:150,
								editor:{ type:'text', options:{ required:true }
								}">名称</th>
							<th data-options="field:'type',width:100,
		                        editor:{
		                            type:'combobox',
		                            options:{
		                                valueField:'key',
		                                textField:'name',
		                                required:true,
		                                data: columnTypes
		                            }
		                        },
								formatter:function(value,row,index){
		                            return mapTypes[row.type];
		                        }">要素</th>
							<th data-options="field:'query',width:40,align:'center',
								editor:{type:'checkbox',options:{on:'是',off:'否'}},
								formatter:function(value, row){
									if(typeof(value) == 'undefined') 
										row.query = '是';
		                            return row.query;
		                        }">搜索</th>
							<th data-options="field:'show', width:40,align:'center',
								editor:{type:'checkbox',options:{on:'是',off:'否'}},
								formatter:function(value, row){
									if(typeof(value) == 'undefined') 
										row.show = '是';
		                            return row.show;
		                        }">显示</th>
							<th data-options="field:'width',width:60,
								editor:'numberbox',
								formatter:function(value, row){
									if(typeof(value) == 'undefined') 
										row.width = 0;
		                            return row.width;
		                        }">显示宽度</th>
						</tr>
					</thead>
				</table>
			</div> 
		</form>
	</body>
	<script type="text/javascript">
		$(function(){
			var dg = $('#now-grid').datagrid({
                data: rows,
                onBeforeCellEdit: function(index, field){
                	/*当前行数据*/
                	var row = rows[index];
                	/*默认“生效时间”、“失效时间”、“地市”的数据索引位置为1、2、3*/
                	/*允许修“生效时间”、“发布时间下拉”*/
               		/*不允许对“结束时间”、“地市”的名称及类型进行修改*/
               		/*不允许对“生效时间，结束时间”的名称及类型进行修改*/
               		if((field == 'name' || field == 'type') && row.index 
               			&& (row.index == 2 || row.index == 3)) {
               			$(this).datagrid('gotoCell', {index: index, field: field});
               			return false;
               		}
               		if((field == 'type') && row.index && row.index == 1){
               			$(this).datagrid('gotoCell', {index: index, field: field});
               			return false;
               		}
               		return true;
				}
            });
			dg.datagrid('enableCellEditing');
		});
	</script>
</html>
