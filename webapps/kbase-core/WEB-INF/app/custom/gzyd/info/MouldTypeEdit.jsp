<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>信息模板分类-新增|编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/MouldTypeEdit.js"></script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'center', border:false" style="width:100%;margin: 2px 1px;">
	        <form id="form1" method="post">
	        	<table class="box" cellspacing="0" style="width: 100%; height: auto;">
	                <tr>
	                    <th style="width: 25%;">父分类:</th>
	                    <td style="width: 75%;">
	                    	<ul class="easyui-combotree" name="pId" style="width: 100%;"
								data-options="lines:true,value:'${mouldType.parent.id }',
	                    			url:'${pageContext.request.contextPath}/app/custom/gzyd/mould-type!syncData.htm',
	                    			loadFilter: function(data, parent){
				    					/*首次加载,加获取的数据添加到指定根节点下*/
				    					if(parent == null && data){
				    						var typeId = '${mouldType.id }';
				    						var data_ = new Array();
				    						$(data).each(function(i, o){
				    							/*移除当前节点及其所有子节点*/
				    							if(!(o.bh.indexOf(typeId) > -1) 
				    								|| typeId == ''){
					    								o.text = o.name;
					    								o.state = 'open';
					    								data_.push(o);
				    							}
				    						});
				    						data = [{text:'模板分类', state: 'open', id:'',
				    							children: PageUtils.fn.treeData(data_)}];
				    					}
				    					return data;
				    				}"></ul>
	                    </td>
	                </tr>
	                <tr>
	                    <th>当前分类:</th>
	                    <td>
	                    	<input name="name" class="easyui-textbox" style="width: 100%;" 
	                    		data-options="required:true,missingMessage:'不为空'" value="${mouldType.name }"/>
	                    	<input type="hidden" name="id" value="${mouldType.id }"/>
	                    </td>
	                </tr>
	                <tr>
	                	<th>备注:</th>
	                    <td>
	                    	<input name="desc" class="easyui-textbox" style="width: 100%;height:60px" 
	                    		data-options="multiline:true,missingMessage:'不为空'" value="${mouldType.desc }"/>
	                    </td>
	                </tr>
	            </table>
	            <div style="height: 28px; width: 99%; padding: 10px 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="btn-save" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
	        </form>
		</div>
	</body>
</html>
