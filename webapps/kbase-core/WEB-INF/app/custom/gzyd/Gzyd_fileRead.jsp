<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>文件读写</title>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript">
		/*
			打开Internet Explorer “工具”-->“选项”-->“安全”-->“可信站点”-->“自定义级别”：
			1.将页面地址加入可信站点地址中。
			2.“对没有标记为安全的activex控件进行初始化和脚本运行”设置 成“启用”。
			
			
			一、语法
			FileSystemObject.OpenTextFile(fname,mode,create,format)
			参数说明：
			fname：必须的。要打开的文件的名字。
			mode：可选的。以什么方式打开。1=ForReading（以只读方式打开），2=ForWriting （以写方式打开），8=ForAppending（以添加方式打开，写入的内容将添加到文件末尾）。
			create：可选的。设置如果所打开的文件不存在是否创建该文件。True为是，False为否。默认是False。
			format：可选的。文件的格式。0=TristateFalse（以ASCII格式打开，这是默认的），-1=TristateTrue（以Unicode格式打开），-2=TristateUseDefault （以系统默认方式打开） 
		*/
		
		var SMSUtils = {
			_basePath: 'C:/Users/Gassol.Bi/Desktop/',
			_obtainToday: function(){
				var date = new Date();
				date.setDate(date.getDate());
				var y = date.getFullYear() + '';
				var m = (date.getMonth() + 1) + '';
				var d = date.getDate() + '';
				if(m < 10) m = '0' + m;
				if(d < 10) d = '0' + d;
				return (y + m + d);
			},
			_obtainData: function(src){
				var ts;
				try {
					if(!src) src = this._basePath + this._obtainToday() + '.txt';
					var fso = new ActiveXObject("Scripting.FileSystemObject");
					ts = fso.OpenTextFile(src, 1);/*1表示只读*/
					var arr = ts.ReadAll();ts.Close();
					if(arr.indexOf('\r\n') > -1){
						arr = arr.split('\r\n');
						return arr[arr.length-2];/*获取最后一条数据*/
					} else {
						return arr;
					}
				} catch (err){
					alert(err);
				} finally {
					ts.Close();
				}
				return null;
			},
			getData: function (){
				var ngccObj = {};/*返回结果*/
				var data  = this._obtainData();/*获取处理数据*/
				if(data && data.indexOf('|') > -1){
					data = data.split('|');
					ngccObj.SERIALNO = data[0];
					ngccObj.CONTACTID = data[1];
					ngccObj.SUBSNUMBER = data[2];
					ngccObj.SUBSCITYID = data[3];
					ngccObj.SUBSBRANDID = data[4];
					ngccObj.DATATIME = data[5];
				}
				return ngccObj;
			}
		}
		
		function ReadText(){
			var data = SMSUtils.getData();
			alert(data.DATATIME);
		}
	</script>
	</head>

	<body>
		<input type=button value="Read" onclick="ReadText()">
	</body>
</html>
