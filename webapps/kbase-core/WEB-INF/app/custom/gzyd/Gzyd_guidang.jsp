<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>知识库-归档</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css?2015123001" rel="stylesheet" type="text/css" />
		<style type="text/css">
			table.box {
				font-family: "Courier New", Courier;
				font-size: 12px;
				border: 1px solid #C0C0C0;
				padding: 0px;
			}
			
			.box .th {
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: right;
				width: 30%;
			}
			
			.box .td {
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: left;
				width: 70%;
			}
			
			.box .td input {
				border: 1px solid #C0C0C0;
			}
			
			.box .td input[type="button"],.box .td button {
				cursor: pointer;
				margin-right: 5px;
				font-weight: bolder;
				height: 25px;
			}
			
			.box_duanxin th {
				text-align: center;
				background-color: #C0C0C0;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
			
			.box_duanxin td {
				text-align: center;
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">
			$(function () {
				var filing = {}; 
					filing.extend = $.extend;
				/*归档*/
				filing.extend({
					guidang : function (){
						var ngccObj = this.ngccObj;
						var SERIALNO = ngccObj.SERIALNO;
						var CONTACTID = ngccObj.CONTACTID;
						var SUBSNUMBER = ngccObj.SUBSNUMBER;
						if(typeof(SERIALNO) == "undefined" ||　SERIALNO == ''){
							alert("接触流水号不应为空!");
							return false;
						}
						if(typeof(CONTACTID) == "undefined" ||　CONTACTID == ''){
							alert("接触编号不应为空!");
							return false;
						}
						if(typeof(SUBSNUMBER) == "undefined" ||　SUBSNUMBER == ''){
							alert("受理编号不应为空!");
							return false;
						}
						
						var SERVICETYPEID =  $("#level_guidang option:selected").val();
						var SERVICEFULLNAME = $("#level_guidang option:selected").attr("fname");
						
						if(SERVICETYPEID != null && SERVICETYPEID != ''){
							if(confirm("确认是否归档？")) {
								$('body').ajaxLoading('正在归档中...');
								$.ajax({
									type : 'post',
									url :  '${pageContext.request.contextPath}/app/custom/gzyd/gzyd-mappings!guidang.htm',
									data : {'SERIALNO':SERIALNO,
										'CONTACTID':CONTACTID,
										'SUBSNUMBER':SUBSNUMBER,
										'SERVICETYPEID':SERVICETYPEID,
										'SERVICEFULLNAME':SERVICEFULLNAME},
									async: false,
									dataType : 'json',
									success : function(data){
										$('body').ajaxLoadEnd();
										if(data==0){
											alert('归档成功!');
										}else if(data==1){
											alert('归档失败!');
										}else if(data==-100){
											alert('数据异常!');
										}else{
											alert('操作异常，请于华为归档接口提供人联系!');
										}
										window.close();//发送完成，关闭当前归档窗口 add by heart.cao 2016-04-28
									},
									error:function(XMLHttpRequest, textStatus, errorThrown){
										alert("网络错误，请稍后再试!");
										$('body').ajaxLoadEnd();
									}
								});
							}
							
						}else{
							alert("业务类型不应为空!\n如果业务类型下拉框为空，请先关联相关知识分类!");
						}
					},
					/*初始化*/
					ngccObj: null,
					init: function(){
						/*获取相关数据*/
						var ngccObj = this.ngccObj = window.dialogArguments.ngccObj;
						try {
							$("#jclsh_guidang").text(ngccObj.SERIALNO);//接触流水号
							$("#jcbh_guidang").text(ngccObj.CONTACTID);//接触编号
							$("#slbh_guidang").text(ngccObj.SUBSNUMBER);//受理编号
						} catch(e) {
							$("#jclsh_guidang").text('');//接触流水号
							$("#jcbh_guidang").text('');//接触编号
							$("#slbh_guidang").text('');//受理编号
						}
						/*业务映射数据*/
						var treeSrtype = window.dialogArguments.treeSrtype;
						if(treeSrtype.length>0){
							var array_filter = ",办理,咨询,查询,取消,投诉,";
							$("#level_guidang").empty();
							$("#level_guidang").append("<option fname='' value=''>--请选择--</option>");
							$.each(treeSrtype, function(index, item){
								if(array_filter.indexOf(","+$(this).attr("name")+",")>-1){
					        		$("#level_guidang").append("<option fname='" + $(this).attr("fname") 
					        			+ "' value='"+$(this).attr("id")+"'>"+$(this).attr("name")+"</option>");
					    		}
							});
						}
						
						//绑定归档保存
						$("#save_guidang").unbind("click");
						$("#save_guidang").bind("click",function (){
							filing.guidang();
						});
					}
				});
				/*初始化*/
				filing.init();
			});
		</script>
	</head>
	<body>
		<!--******************弹出框 归档***********************-->
		<div id="guidang" style="width: 99%;">
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th class="th">
						接触流水号：
					</th>
					<td class="td">
						<span id="jclsh_guidang"></span>&nbsp;
					</td>
				</tr>
				<tr>
					<th class="th">
						接触编号：
					</th>
					<td class="td">
						<span id="jcbh_guidang"></span>&nbsp;
					</td>
				</tr>
				<tr>
					<th class="th">
						受理号码：
					</th>
					<td class="td">
						<span id="slbh_guidang"></span>&nbsp;
					</td>
				</tr>
				<tr>
					<th class="th">
						处理类型：
					</th>
					<td class="td">
						<select id="level_guidang" style="width:80px">
							<option value="">--请选择--</option>
						</select>
					</td>
				</tr>
			</table>
			<br>
			<div style="height: 28px; width: 100%; padding: 0 0 7px 0; float: none;" 
				class="unsolve_xinxi_dan_con">
				<input type="button" id="save_guidang"
					style="float: right; margin-right: 10px;"
					class="unsolve_xinxi_an1">
			</div>
		</div>
		<!--******************弹出框 归档***********************-->
	</body>
</html>
