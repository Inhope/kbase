<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>业务类型管理</title>
		<link rel="stylesheet" type="text/css"  href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.xinxi_con_nr_right .zuzhi{
				width: 400px;
			}
			.xinxi_con_nr_right .zuzhi ul li span{
				width: 100px;
				font-weight:bold;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript">	
			/************左侧菜单树******************/
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+'/app/main/ontology-category!list.htm',
					autoParam: ["id"]
				},				
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},callback: {			
					onClick: zTreeOnClick
				}
			};			
			
			/************左击事件******************/
			function zTreeOnClick(event, treeId, treeNode){			
				//显示编辑区
				$(".xinxi_con_nr_right").css("display","none");//显示主区
				$("#content0").css("display","block");
				
				//信息初始化
				$("#cIds").val(treeNode.id);
			   	$("#cNames").val(treeNode.name);
				$.ajax({
			   		type: "POST",
			   		url : $.fn.getRootPath() + "/app/custom/gzyd/gzyd-mappings!addOrEditTo.htm",
			   		data : {
			   			'categoryid':treeNode.id
			   		},
			   		async: false,
			   		dataType : 'json',
			   		success: function(mappings){
			   			if(mappings != null){
			   				$("#sIds").val(mappings.srtypeid);
			   				$("#sNames").val(mappings.srtypename);
			   				$("#sFIds").val(mappings.srtypefid);
			   				$("#sFNames").val(mappings.srtypefname);
			   				$("#desc").val(mappings.desc);
			   				
			   				/*信息模板数据*/ 
							var infomould = mappings.infomould, 
								mouldId = '', mouldName = '';
							if(infomould){
								var str = infomould.split('\|\&\|');
								mouldId = str[0];
								mouldName = str[1];
							}
							$('#mouldId').val(mouldId);
							$('#mouldName').val(mouldName);
							/*信息模板叶子分类不关联*/
							if(treeNode.bh.indexOf('L') > -1)
								$('#mould').css({display: 'none'});
							else $('#mould').css({display: ''});
							
			   			}
			   		}
			   　});
			}
			
			//保存映射
			function saveMappings(){
				var params=$("#form1").serialize();
				$.ajax({
			   		type: "POST",
			   		url : $.fn.getRootPath() + "/app/custom/gzyd/gzyd-mappings!addOrEditDo.htm?"+params,
			   		async: false,
			   		dataType : 'text',
			   		success: function(msg){
			   			parent.layer.alert(msg, -1);
			   		}
			   　});
			}
					
					
			var treeObj;
			/*****************页面初始化************************/
			$(document).ready(function(){
				//业务类型树初始化
				$.fn.zTree.init($("#treeDemo"), setting);
				treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				
				//绑定保存
				$("#saveMappings").unbind("click");
				$("#saveMappings").bind("click",function (){
					saveMappings();
				});
				
				/*信息模板*/
				window.GzydMappings = {
					mould: {
						choose: function(){
							PageUtils.dialog.open('#choose_mould-dialog', {
					    		url: '/app/custom/gzyd/mould-type!mould.htm',
					    		params: {
					    			chkStyle: 'radio',
					    			initData: 'GzydMappings.mould.initData',/*回显数据方法名*/
									callback: 'GzydMappings.mould.callback'/*弹窗回掉方法名*/
								},
					    		title: '业务选择', width: 400, height: $(window).height()*0.98
					    	});
						},
						initData:function(){
							return {
								id: $('#mouldId').val(),
								name: $('#mouldName').val()
							}	
						},
						callback:function(data){
							if(data && data.length > 0){
								var node = data[0];
								$('#mouldId').val(node.id);
								$('#mouldName').val(node.name);
							} else {
								$('#mouldId').val('');
								$('#mouldName').val('');
							}
						}
					}
				}
			});	
		</script>
		
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xinxi_con">
				<div class="xinxi_con_nr">
					<div class="xinxi_con_nr_left" style="width: auto;">
						<div id="menuContent" class="menuContent">
							<ul id="treeDemo" class="ztree" style="margin-top:0; width:480px;"></ul>
						</div>
					</div>
					<form action="" id="form1" name="form1" method="post">
						<div class="xinxi_con_nr_right" style="display: none;margin-left: 490px;" id="content0">
							<div class="zuzhi" style="height: 310px;">
								<ul>
									<li>
										<span>实例分类:&nbsp;&nbsp;</span>
										<p>
											<input type="text"   id="cNames" name='categoryname'/>
											<input type="hidden" id="cIds"   name='categoryid'/>
										</p>
									</li>
									<li>
										<span>业务类型:&nbsp;&nbsp;</span>
										<p>
											<input type="text"   id="sNames" name='srtypename' onclick="javascript:choose_srtype('sIds','sNames','sFIds','sFNames');" size="63" readonly="readonly"/>
											<input type="hidden" id="sIds"   name='srtypeid'/>
										</p>
									</li>
									<li>
										<span>业务类型(全称):&nbsp;&nbsp;</span>
										<p>
											<input type="text"   id="sFNames" name='srtypefname' size="63" readonly="readonly" />
											<input type="hidden" id="sFIds"   name='srtypefid'/>
										</p>
									</li>
									<li style="display: none;" id="mould">
										<span>信息模板:&nbsp;&nbsp;</span>
										<p>
											<input type="text"   id="mouldName" name='mouldName' size="63" readonly="readonly" 
												onclick="javascript:GzydMappings.mould.choose();"/>
											<input type="hidden" id="mouldId"   name='mouldId'/>
										</p>
									</li>
									<li>
										<span>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注:&nbsp;&nbsp;</span>
										<p>
											<textarea id="desc" name="desc" cols="" rows=""></textarea>
										</p>
									</li>
									<li class="anniu">									
										<a href="#"><input type="button" class="fkui_aa2" value="保存" id="saveMappings"/></a>
									</li>
								</ul>
							</div>
						</div>
					</form>					
				</div>
			</div>
		</div>
		
		<!-- 信息编辑、新增-图片预览 -->
		<div id="choose_mould-dialog"></div>
	</body>
</html>
