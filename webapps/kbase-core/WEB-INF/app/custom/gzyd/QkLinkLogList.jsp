<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>快速链接(广州移动)-原子化知识库</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/datagrid-detailview.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">	
			$(function(){
				var that = window.FaultTreeEx = {}, fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				
				fn.extend({
					getyyyyMMdd: function(){
						var date = new Date();
						date.setDate(date.getDate());
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						if(m < 10) m = '0' + m;
						if(d < 10) d = '0' + d;
						return y + '-' + m + '-' + d;
					}
				});
				
				that.extend({
					init: function(){
						/*查询条件初始化*/
						$('#createDate').datebox('setValue', fn.getyyyyMMdd());
						
						/*刷新列表*/
						var init = setTimeout(function (){
							clearTimeout(init);/*取消定时器*/
							$('#now-tab').datagrid({
								url: $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!qkLinkLogListData.htm',
								queryParams: {'createDate': $('#createDate').datebox('getValue')},
				                view: detailview,
				                detailFormatter:function(index,row){
				                    return '<div class="ddv" style="padding:5px 0"></div>';
				                },
				                onExpandRow: function(index, row){
				                   	var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
				                    ddv.datagrid({
				                        url: $.fn.getRootPath() 
				                        	+ '/app/custom/gzyd/gzyd-mappings!qkLinkLogDetailData.htm',
				                        queryParams: {
				                        	'createDate': $('#createDate').datebox('getValue'), 
				                        	'userId': row.userid
				                        },
				                        width:600,
				                        fitColumns:true,
				                        singleSelect:true,
				                        rownumbers:true,
				                        loadMsg:'加载中......',
				                        height:'auto',
				                        columns:[[
				                            {field:'username',title:'用户',width:200},
				                            {field:'createdate',title:'时间',width:200}
				                        ]],
				                        onResize:function(){
				                            $('#now-tab').datagrid('fixDetailRowHeight',index);
				                        },
				                        onLoadSuccess:function(){
				                            setTimeout(function(){
				                                $('#now-tab').datagrid('fixDetailRowHeight',index);
				                            },0);
				                        }
				                    });
				                    $('#now-tab').datagrid('fixDetailRowHeight',index);
				                }
				            });
						}, 500);
						
						/*查询按钮*/
						$('#now-search').click(function(){
							var queryParams = $("#now-tab").datagrid('options').queryParams;
					        queryParams.createDate = $('#createDate').datebox('getValue');
							queryParams.userName = $('#userName').textbox('getValue');
							queryParams.deptId = $('#deptTree').combotree('getValue');
					        $("#now-tab").datagrid('reload');
						});
						
						/*重置按钮*/
						$('#now-clear').click(function(){
							$('#createDate').datebox('setValue', '');
							$('#userName').textbox('setValue', '');
							$('#userName').textbox('setText', '');
							$('#deptTree').combotree('setValue', '')
						});
					}
				});
				/*初始化*/
				that.init();
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto">
			<div>
				日期：<input class="easyui-datebox" id="createDate" style="width:100px;">&nbsp;&nbsp;
				账号：<input class="easyui-textbox" id="userName" style="width:150px;">&nbsp;&nbsp;
				部门：<input class="easyui-combotree" id="deptTree" style="width:150px;" 
						data-options="url:'${pageContext.request.contextPath}/app/auther/dept!asyncData.htm',
                     	 	loadFilter: function(data, parent){
                     	 		var data_ = new Array();
                     	 		if(data) {
                     	 			$(data).each(function(i, item){
                     	 				if(item.type == 'dept') 
                     	 					data_.push({
		                     	 				id: item.id,
		                     	 				text: item.name,
		                     	 				state: 'closed'
		                     	 			});
	                     	 		});
                     	 		}
                     	 		return data_;
                     	 	}">
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
			</div>
		</div>
		<!-- 列表 -->
		<table id="now-tab" title="快速链接(广州移动)" 
			data-options="url: '', method: 'POST', 
				rownumbers: true, nowrap: false, 
				pagination:true,
				toolbar: '#now-toolbar',
		        fit: true,  fitColumns:true,
		        singleSelect:true,
		        checkOnSelect:false,
		        selectOnCheck:false">
			<thead>
				<tr>
					<th data-options="field:'username',width:100,align:'right'">点击人</th>
					<th data-options="field:'amount',width:80,align:'right'">点击次数</th>
					<th data-options="field:'dname',width:80,align:'right'">部门</th>
					<th data-options="field:'sname',width:80,align:'right'">岗位</th>
				</tr>
			</thead>
		</table>
	</body>
</html>
