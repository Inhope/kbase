<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>知识库-发短信</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css?2015123001" rel="stylesheet" type="text/css" />
		
		<style type="text/css">
			table.box {
				font-family: "Courier New", Courier;
				font-size: 12px;
				border: 1px solid #C0C0C0;
				padding: 0px;
			}
			
			.box .th {
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: right;
				width: 15%;
			}
			
			.box .td {
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
				text-align: left;
				width: 35%;
			}
			
			.box .td input {
				border: 1px solid #C0C0C0;
			}
			
			.box .td input[type="button"],.box .td button {
				cursor: pointer;
				margin-right: 5px;
				font-weight: bolder;
				height: 25px;
			}
			
			.box_duanxin th {
				text-align: center;
				background-color: #C0C0C0;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
			
			.box_duanxin td {
				text-align: center;
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>		
		<script type="text/javascript">
			$(function () {
			    var _submitted = 0;//是否已提交	1-是 0-否	
				var sms = {}; 
				sms.extend = $.extend;
				sms.extend({
					/*短信发送*/
					send: function (){
						var SERIALNO = $("#jclsh_duanxin").val();/*接触流水号*/
						var CONTACTID = $("#jcbh_duanxin").val();/*接触编号*/
						var SUBSNUMBER = $("#slbh_duanxin").val();/*接收短信号码*/

						if(typeof(SERIALNO) == "undefined" ||　SERIALNO == ''){
							alert("接触流水号不应为空!");
							return false;
						}
						if(typeof(CONTACTID) == "undefined" ||　CONTACTID == ''){
							alert("接触编号不应为空!");
							return false;
						}
						if(typeof(SUBSNUMBER) == "undefined"　||　SUBSNUMBER == ''){
							alert("接收短信号码不应为空!");
							return false;
						}else{
							if(!(/^1[3|4|5|7|8][0-9]\d{4,8}$/.test(SUBSNUMBER))){
								alert("接收短信号码应为手机号码!");
								return false;
							}
						}
						//如果已经提交，停止执行，防止重复提交  modify by heart.cao 2016-10-31
						if(_submitted == 1){
						   return false;
						}						
						if(confirm("确认发送短信？")) {
							_submitted = 1;		
							window.__kbs_layer_index = layer.load('正在发送短信，请稍候...');															
							$('#gzyd_from').form('submit', {
							    url: '${pageContext.request.contextPath}/app/custom/gzyd/gzyd-mappings!duanxin.htm',
							    onSubmit: function(params){
							    	/*获取短信标题及短信内容*/
							    	var qas = sms.getCkQas();
									if(!qas) {
										alert('请勾选需要发送短信的行!');
										return false;
									}
							    	$('#gzyd_qas').text(qas);
							    	return true;
							    },    
							    success:function(data){
							        layer.close(window.__kbs_layer_index);	
							        _submitted = 0;							    
							    	var json = eval("(" + data + ")"); 
							    	var result = json.result;
									var content = json.content;
									if(typeof(result) == "undefined" && content){
										var m = "";
									 	for(var i=0; i<content.length; i++){
									 		var q = content[i].q;
									 		var r = content[i].r;
									 		m += "知识点：'"+q+"',"
									 		if(r==0) m += '短信发送成功!'; 
									 		else if(r==1) m += '短信发送失败!'; 
									 		else if(r==-100) m += '短信发送异常!'; 
									 		else m += '操作异常，请于华为短信接口提供人联系!';
											m += "\n";
								  	 	}
								  	 	
								  	 	if(json.gd_rst == '0'){
								  	 		m += '归档结果：自动归档成功!';
								  	 	} else {
								  	 		m += '归档结果：' + json.gd_msg;
								  	 	}
								  	 	// alert(m);
								  	 	//提示窗设置自动关闭 modify  by heart.cao 2016-10-31
								  	 	layer.msg(m, 1, -1);
								  	 	window.close();//发送完成，关闭当前短信发送窗口 add by heart.cao 2016-04-28
									} else {
										alert('网络异常，请稍后再试!');
									}
							    }
							});
						}
					},
					/*获取需要发送短信的标准问*/
					getCkQas: function(){
						var qas = this.qas;
						if(qas && qas.length > 0) {
							var tab = $("table .box_duanxin").eq(1), 
								trs = $(tab).find('tr').has('td:first input[type="checkbox"][name="gzyd_duanxin_ck"]:checked'), 
								qas_ = '';
							$(trs).each(function(){
								var qa = qas[$(this).index()];
								// qas_ += '{qid:"'+qa.qid+'",q:"' + qa.q + '",a:"' + qa.a + '"},';
								qas_ += '{qid:"'+qa.qid+'",q:"' + sms.tagHandle(qa.q) + '",a:"' + sms.tagHandle(qa.a) + '"},';
							});
							qas_ = qas_.replace(/^,|,$/,'');
							if(qas_) qas_ = '[' + qas_ + ']';
							return qas_;
						}
					},
					/*初始化*/
					qas: null,
					init: function(){
						$('#gzyd_duanxin_ck_all').click(function(){
							var checked = $(this).attr('checked');
							if(checked == 'checked')
								$('input[type="checkbox"][name="gzyd_duanxin_ck"]').attr('checked', checked);
							else $('input[type="checkbox"][name="gzyd_duanxin_ck"]').removeAttr('checked', checked);
						});
						
						//********************************初始化短信页面数据(结束)***************************************
						/*相关数据*/
						var args, isDialog;
						if(window.dialogArguments){
							/*showModalDialog打开方式，数据获取方式*/
							isDialog = true;
							args = window.dialogArguments;
						} else {
							/*针对非showModalDialog打开方式，数据获取方式*/
							isDialog = false;
							args = window.opener.GzydMappings.
								fn.sms.getData1(${param.isRClick});
						}
						
						/*获取短信标题及短信内容*/
						var qas = this.qas = args.qas;
						var tab = $("table .box_duanxin").eq(1);
						$(tab).empty();//移除历史数据
						for(var i = 0, len = qas.length; i<len; i++){
							//添加新数据
							$(tab).append('<tr>' + 
											'<td width=\"10%\"><input type="checkbox" name="gzyd_duanxin_ck" checked="checked"></td>' + 
											//'<td width=\"10%\"><input type="checkbox" name="q" value="'+qas[i].q+'" checked="checked" /></td>' + 
											'<td width=\"20%\">'+qas[i].q+'</td>' + 
											'<td width=\"70%\">'+qas[i].a+'</td>' + 
										'</tr>');
						}
						/*获取相关数据*/
						var ngccObj = args.ngccObj;
						$("#jclsh_duanxin").val(ngccObj.SERIALNO);/*接触流水号*/
						$("#jcbh_duanxin").val(ngccObj.CONTACTID);/*接触编号*/
						$("#slbh_duanxin").val(ngccObj.SUBSNUMBER);/*受理编号*/
						$("#send_duanxin").text("10086");//发送短信号码
						$("#jrsj_duanxin").text(ngccObj.DATATIME);//发送短信号码						
						
						/*默认全选*/
						$('#gzyd_duanxin_ck_all').attr('checked', 'checked');
						/*分类id*/
						var categoryId = args.categoryId;
						$("#categoryId").val(categoryId);
						//*********************************初始化短信页面数据(结束)**************************************************************
						/*短信发送*/
						$('#save_duanxin').click(function(){
							sms.send();
						});
					},
					/*
					 * 标准问、标准回复内容处理
					 */
					tagHandle: function(con){
						con = con.replace(/<[^>]+>/g, "");//html标签处理
						con = con.replace(/(^\s*)|(\s*$)/g, "");//去除首尾空白
						con = con.replace(/\"/g, "\\\"");//转义双引号
						return con;
					}
				});
				/*初始化*/
				sms.init();
			});
		</script>
	</head>
	<body>
		<!--******************弹出框 短信***********************-->
		<div id="duanxin" title="短信">
			<form id="gzyd_from" action="" method="post">
				<!-- 发送短信号码 -->
				<input type="hidden" name="SENDNUMBER" value="10086" />
				<input type="hidden" name="SERVICETYPEID" value="SERVICETYPEID" />
				<input type="hidden" name="SERVICEFULLNAME" value="SERVICEFULLNAME" />
				<input type="hidden" id="categoryId" name="categoryId"/>
				<!-- 短信内容 -->
				<textarea rows="1" cols="1" id="gzyd_qas" name="qas"
					style="display: none;"></textarea>
				<table cellspacing="0" class="box" style="width: 100%;">
					<!-- 
						<tr>
							<th class="th">接触流水号：</th>
							<td class="td"><span id="jclsh_duanxin"></span>&nbsp;</td>
							<th class="th">接触编号：</th>
							<td class="td"><span id="jcbh_duanxin"></span>&nbsp;</td>
						</tr>
					 -->
					<tr>
						<th class="th">
							接触流水号：
						</th>
						<td class="td">
							<input type="text" id="jclsh_duanxin" name="SERIALNO" />
							<input type="hidden" id="jcbh_duanxin" name="CONTACTID" />
						</td>
						<th class="th">
							接入时间：
						</th>
						<td class="td">
							<span id="jrsj_duanxin"></span>&nbsp;
						</td>
					</tr>
					<tr>
						<th class="th">
							接收短信号码：
						</th>
						<td class="td">
							<input type="text" id="slbh_duanxin" name="SUBSNUMBER" />
							&nbsp;
						</td>
						<th class="th">
							发送短信号码：
						</th>
						<td class="td">
							<span id="send_duanxin"></span>&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<table cellspacing="0" width="100%" class="box_duanxin">
								<tr>
									<th width="10%">
										<input type="checkbox" id="gzyd_duanxin_ck_all"
											checked="checked">
									</th>
									<th width="20%">
										标题
									</th>
									<th width="70%">
										内容
									</th>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div style="width: 100%; max-height: 400px; overflow: auto;">
								<table cellspacing="0" width="100%" class="box_duanxin">
									<tr>
										<td>
											<input type="checkbox" name="gzyd_duanxin_ck" value=""
												checked="checked" />
										</td>
										<td>
											1
										</td>
										<td>
											1
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
				<br>
				<div style="height: 28px; width: 100%; padding: 0 0 7px 0; float: none;" 
					class="unsolve_xinxi_dan_con">
					<input type="button" id="save_duanxin"
						style="float: right; margin-right: 10px;"
						class="unsolve_xinxi_an1">
				</div>
			</form>
		</div>
		<!--******************弹出框 短信***********************-->
	</body>
</html>