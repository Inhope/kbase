<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>知识库</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinxi.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		
		<script type="text/javascript">
		//所有大栏目
		var allTitleA = null;
		//标签点击
		function aClick(obj,type,method){
			if(!getURL(type,method)){
				alert("请求异常!");
				return;
			}
			//标签染色
			$("ul.xia li a").css("background-color","");
			$(obj).css("background-color","#E6E6E6");			
			
			$('.content_right_bottom').ajaxLoading('正在查询数据...');
			$.ajax({
				  url:$.fn.getRootPath()+getURL(type,method),
				  type:"post",
				  data:{"pageNo":$("#pageNo").val()},
				  timeout:5000,
				  async:false,
				  dataType:"html",
				  success:function(data){
				    if (data!=null&&data!="") {
				       $('.xinxi_con_nr_right').html(data);
				       if(type=='knowledgeCreate'&&method=='waitAuditing'){
				       		var sumKW = $('#sum_knowledgeCreate_waitAuditing').val();
				       		$('#sumKW').text(sumKW==-1?0:sumKW);
				       }
				    } else {
				       alert("网络错误，请稍后再试");
				    }
				    $('.content_right_bottom').ajaxLoadEnd();
				  },
				  error:function(){
				  	alert("网络错误，请稍后再试");
				  	$('.content_right_bottom').ajaxLoadEnd();
				  }
			});
		}
		
		//大栏目点击
		function titleAClick(obj){
			obj = $(obj).parent("li");
			//obj当前大栏目			
			var ulCurrent = $(obj).children('ul');
			if($(ulCurrent).css("display")=="none"){
				$(ulCurrent).slideDown();			
			}else{
				$(ulCurrent).slideUp();
			}
			$(obj).attr('class','xinxi_biaoti1');
			//其他栏目
			$(allTitleA).parent("li").not(obj).children('ul').slideUp();
			$(allTitleA).parent("li").not(obj).attr("class","xinxi_biaoti");
		}
//********************************************************************************************************		
		
		//弹出框打开
		function openShade(shadeId){
			var w = ($('body').width() - $('#'+shadeId).width())/3;
			$('#'+shadeId).css('left',w+'px');
			$('body').showShade();
			$('#'+shadeId).show();
		}
			
		//弹出框关闭
		function closeShade(shadeId){
			$('#'+shadeId).hide();
			$('body').hideShade();	
		}
		
		//获取url
		function getURL(type,method){
			var moduleId = "&moduleId=402882e94338dab001433c4a24580013";
			if(method=="waitAuditing"){//待我审核
				return '/app/auther/work-flow!waitAuditing.htm?type='+type+'&method='+method+moduleId;
			}else if(method=="myCreate"){//我发起未归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method+moduleId;
			}else if(method=="myCreate1"){//我发起已归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method+moduleId;
			}else if(method=="concluded"){//我处理未归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method+moduleId;
			}else if(method=="concluded1"){//我处理已归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method+moduleId;
			}else if(method=="knowledgelook"&&type=="knowledgeCreate"){
				return '/app/auther/work-flow!knowledgeCreateLook.htm?type='+type;
			}else if(method=="knowledgelook"&&type=="knowledgeFeedback"){
				return '/app/auther/work-flow!knowledgeFeedbackLook.htm?type='+type;
			}else if(type== "remmendknowledge" && method == "remmend"){
				return '/app/recommend/recommend.htm?flag=owner';
			}else if(type== "corrections" && method == "hascorrections"){
				return '/app/corrections/corrections!getcorrections.htm?status=1';
			}else if(type== "corrections" && method == "corrections"){
				return '/app/corrections/corrections!getcorrections.htm?status=0';
			}
			
			return false;
		}
		
//********************************************************************************************************		
		//初始化
		$(document).ready(function (){
			allTitleA = $('div.xinxi_con_nr_left').children('ul').children('li').children('a');			
			//大栏目添加点击事件
			$(allTitleA).each(function (){
				$(this).click(function (){
				 	titleAClick(this);
				});
			});
								
			//点击第一个大栏目
			$(allTitleA[0]).click();					
			//点击第一个大栏目下第一个菜单
			$(allTitleA[0]).parent("li").children('ul').children('li:eq(0)').children('a').click();				
		});	
		</script>
	</head>

	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xinxi_con">
				<div class="xinxi_con_nr">
					<div class="xinxi_con_nr_left">
						<ul>
							<li class="xinxi_biaoti">
								<a href="javascript:void(0)" onfocus="this.blur();">合理化建议</a>
								<ul class="xia">
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeCreate','waitAuditing')" id="knowledgeCreatewaitAuditing" >待我审核(<span id="sumKW" style="color:red">0</span>)</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeCreate','myCreate')" id="knowledgeCreatemyCreate">我发起未归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeCreate','myCreate1')" id="knowledgeCreatemyCreate1">我发起已归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeCreate','concluded')" id="knowledgeCreateconcluded">我处理未归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeCreate','concluded1')" id="knowledgeCreateconcluded1">我处理已归档</a>
									</li>									
								</ul>
							</li>
							<li class="xinxi_biaoti" style="display: none;">
								<a href="javascript:void(0)" onfocus="this.blur();">知识反馈</a>
								<ul class="xia">
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeFeedback','waitAuditing')" id="knowledgeFeedbackwaitAuditing" >待我审核</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeFeedback','myCreate')" id="knowledgeFeedbackmyCreate" >我发起未归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeFeedback','myCreate1')" id="knowledgeFeedbackmyCreate1" >我发起已归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeFeedback','concluded')" id="knowledgeFeedbackconcluded" >我处理未归档</a>
									</li>
									<li>
										<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'knowledgeFeedback','concluded1')" id="knowledgeFeedbackconcluded1">我处理已归档</a>
									</li>
								</ul>
							</li>
							<li class="xinxi_biaoti">
								<a href="javascript:void(0)" onfocus="this.blur();">推荐知识</a>
								<ul class="xia">
									<li>
									<a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'remmendknowledge','remmend')" id="remmendknowledge">我的推荐</a>
									</li>
								</ul>
							</li>
							<li class="xinxi_biaoti">
								<a href="javascript:void(0)" onfocus="this.blur();">纠错管理
								<s:if test="#request.correctionscount>0"><span id="flags">(<span id="totalcounts" style="color:red">${correctionscount}</span>)</span></s:if>
								</a>
								<ul class="xia">
									<li>
									 <a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'corrections','hascorrections')" id="corrections">已处理<s:if test="#request.processcount>0"><span id="processflag">(<span id="processcount" style="color:red">${processcount}</span>)</span></s:if></a>
									</li>
									<li>
									 <a href="javascript:void(0)" onfocus="this.blur();" onclick="aClick(this,'corrections','corrections')" id="corrections">未处理<s:if test="%{#request.correctionscount-#request.processcount>0}"><span id="flag">(<span id="totalcount" style="color:red">${correctionscount-processcount}</span>)</span></s:if></a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="xinxi_con_nr_right">
						<a href="javascript:void(0)" onfocus="this.blur();">知识反馈</a>
					</div>
				</div>
			</div>
		</div>	
		<!--流程查看审核  -->		
		<div id="workFlowShadeCotent"></div>
	</body>
</html>

