<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinxi.css" rel="stylesheet" type="text/css" />
<style>
.biaoge{width:100%;heihgt:auto;}
.biaoge tr{width:100%;}
.biaoge td{font-size:12px;color:#666;text-align: center;border-left:1px solid #DDD;border-top:1px solid #DDD;}

.zoomIn{
	background:url(../../theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/buttons.jpg) -175px -125px;
    height:30px;
	width:30px;
    display:block;
}

.zoomOut{
	background:url(../../theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/buttons.jpg) -135px -125px;
    height:30px;
	width:30px;
    display:block;
}

</style>
<script type="text/javascript"> 
	mxBasePath = '${pageContext.request.contextPath}'+'/resource/mxGraph/';
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/mxGraph/js/mxClient.js"></script>
<script type="text/javascript">
		var nodeid_current = "";
		function mainGraph(container){
			if (!mxClient.isBrowserSupported()){
				mxUtils.error('Browser is not supported!', 200, false);
			}else{
				var graph = new mxGraph(container);
				//是否已图形的中心进行放大，否则以元素为中心放大  
				graph.centerZoom = false;
				//是否可修改
				graph.setEnabled(false);
				//工具提示
				graph.setTooltips(false);			
				
				//突出节点
				new mxCellTracker(graph,'#00FF00',function(me){
					if(me.getCell().isEdge())return;
					return me.getCell();
				});
				
				//监听点击事件
				graph.addListener(mxEvent.CLICK, function(sender, evt){
					var cell = evt.getProperty('cell');
					//当前选择节点不为空，过滤重复点击	
					if (cell != null && nodeid_current != cell.id){
						//如果选中线
						if(cell.isEdge())return;
						//当前选择的节点
						nodeid_current = cell.id;
						//修改节点审核人员信息	
						$.post($.fn.getRootPath()+'/app/auther/work-flow!getWorkFlowNodeData.htm',
							{"nodeid":nodeid_current,"requestid":'${requestid}'},
							function(data){
								if(data!=null&&data!=""){
									$("#data0").html(data.data0);
									$("#data1").html(data.data1);
									$("#data2").html(data.data2);
								}
						},"json");
					}
				});
				
				
				var parent = graph.getDefaultParent();
								
				graph.getModel().beginUpdate();
				try{					
					var xmlDoc = mxUtils.load($.fn.getRootPath()+'/app/auther/work-flow!getWorkflowGraphXml.htm?requestid=${requestid}').getXml();
					var node = xmlDoc.documentElement;
					var dec = new mxCodec(node.ownerDocument);
					dec.decode(node, graph.getModel());
					
					var stylesheet = graph.getStylesheet();	
					var vertexStyle = stylesheet.getDefaultVertexStyle();
					vertexStyle[mxConstants.STYLE_FILLCOLOR] = "#FFFF00";
					//alert(vertexStyle[mxConstants.STYLE_FILLCOLOR]);

				}finally{
					graph.getModel().endUpdate();
					
					//缩小
					$("#zoomOut").click(function(){
						graph.zoomOut();
					});
					//放大
					$("#zoomIn").click(function(){
						graph.zoomIn();
					});
					
					$("#workFlowApprove").appendTo($(container));
					$("#workFlowApprove").css("display","block");
					var  minHeight = $(container).find("svg:eq(0)").css("min-height");
					$(container).find("svg:eq(0)").css("height",minHeight);
					
				}
			}
		};
	

</script>

<div id="workFlowView" class="xinxi_d"
	style="display: none; position: fixed; z-index: 10001; _position: absolute;height:auto;width:800px;">
	<div class="xinxi_dan" style="height:auto;display:inline-block;clear:both;width:800px;">
		<div class="xinxi_dan_title" style="width:100%;">
			<b>流程图</b>
			<a href="#" onclick="closeShade('workFlowView')"> <img
					src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
					width="12" height="12" /></a>
		</div>
		<div class="xinxi_dan_con" style="height:400px;width: 95%;overflow:auto;">
			<ul>
				<li style="display:block;width:100%;height:30px;line-height:30px;margin-bottom:10px;">
					<a href="#" id="zoomIn" class="zoomIn" title="放大" style="font-weight: bold;"></a>
					<a href="#" id="zoomOut" class="zoomOut" title="缩小" style="font-weight: bold;"></a>		
				</li>
				<li>					
					<div id="graphContainer" style="width:100%;height: 90%;"></div>
				</li>
			</ul>
		</div>
	</div>
</div>

<ul id="workFlowApprove" style="display: none;">
	<li>
		<b>未操作：</b>&nbsp;<span style="color: #669CEE;line-height: 30px;" id="data0" >${data0 }</span>
	</li>
	<li>
		<b>已操作：</b>&nbsp;<span style="color: #669CEE;line-height: 30px;" id="data1" >${data1 }</span>
	</li>
	<li>
		<b>已查看：</b>&nbsp;<span style="color: #669CEE;line-height: 30px;" id="data2" >${data2 }</span>
	</li>
	<s:if test="#request.list_WorkFlowApprove!=null&&#request.list_WorkFlowApprove.size>0">
		<li>
			<b>处理记录</b>
		</li>
		<li>
			<table class="biaoge" cellspacing="0" cellpadding="0" border="0"
				align="center" style="border-bottom: 1px solid #ddd; border-right: 1px solid #ddd;">
				<tr style="font-weight: bold;">
					<td width="30px;">
						序号
					</td>
					<td width="50px;">
						处理人
					</td>
					<td width="120px;">
						处理时间
					</td>
					<td width="80px;">
						处理结果
					</td>
					<td>
						处理意见
					</td>
				</tr>
				<s:iterator value="#request.list_WorkFlowApprove" var="va" status="statu">
					<tr>
						<td>
							${statu.index+1 }&nbsp;
						</td>
						<td>
							${va.operator }&nbsp;
						</td>
						<td>
							${va.optdate } ${va.opttime }&nbsp;
						</td>
						<td>
							${va.optertype }&nbsp;
						</td>
						<td>
							${va.remark }&nbsp;
						</td>
					</tr>
				</s:iterator>
			</table>
		</li>
	</s:if>
</ul>