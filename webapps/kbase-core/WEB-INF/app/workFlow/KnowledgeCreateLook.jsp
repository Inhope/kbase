<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinxi.css" rel="stylesheet" type="text/css" />
<style>
.biaoge{width:100%;heihgt:auto;}
.biaoge tr{width:100%;}
.biaoge td{font-size:12px;color:#666;text-align: center;border-left:1px solid #DDD;border-top:1px solid #DDD;}

</style>
<script type="text/javascript">
	function resetFromLook(){
		$("#handleResult")[0].selectedIndex = 0;
		$("#handlereason").attr("value","");
	}
	
	//用户新增、编辑		
	function submitWorkFlow(type,method,requestid,flag){
		if(!getURL(type,method)){
			alert("请求异常!");
			return;
		}
		
		$('.xinxi_an1').attr("disabled",true);
		$('.xinxi_an1').attr("value","提交中……");	
		$('.content_right_bottom').ajaxLoading('正在提交数据...');
				
		var requrl = $.fn.getRootPath()+'/app/auther/work-flow!submitWork.htm';		
		if(flag=='delete'){
			requrl = $.fn.getRootPath()+'/app/auther/work-flow!deleteWorkFlow.htm?type='+type;
		}
		
		$.ajax({
			url:requrl,
			type:"post",
			data:{
					"requestid":requestid,
					"handleResult":$("#handleResult").val(),
					"handlereason":$("#handlereason").val(),
					"maintable":$("#maintable").val()
			},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
				if (data=="1") {
				 	alert("操作成功!");
				} else {
				   	alert("操作失败!");
				}
				
				closeShade('workFlowShade');
				$('.content_right_bottom').ajaxLoadEnd();
				aClick($("#"+type+method),type,method);
			},
			error:function(){
				 alert("网络错误,请稍后再试");
			 }
		});
	}
</script>
<div id="workFlowShade" class="xinxi_d"
	style="display: none; position: fixed; z-index: 10001; _position: absolute;height:auto;width:500px;">
	<div class="xinxi_dan" style="height:auto;display:inline-block;clear:both;width:500px;">
		<textarea rows="" cols="" id="maintable" style="display: none">${maintable }</textarea>
		<div class="xinxi_dan_title" style="width:500px;">
			<b>知识新建</b>
			<a href="#" onclick="closeShade('workFlowShade')"> <img
					src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
					width="12" height="12" /> </a>
		</div>
		<div class="xinxi_dan_con" style="height:auto;margin-bottom: 15px;width: auto;">
			<ul>
				<li>
					<b>知识标题</b>
				</li>
				<li>
					<input type="text" value="${workFlowKnowledgeCreate.title }" readonly="readonly" style="width:455px;" />
				</li>
				<li>
					<b>知识内容</b>
				</li>
				<li>
					<textarea rows="" cols="" readonly="readonly" style="width:455px;">${workFlowKnowledgeCreate.content }</textarea>
				</li>
				<li>
					<b>发布原因</b>
				</li>
				<li>
					<input type="text" value="${workFlowKnowledgeCreate.reason }" readonly="readonly" style="width:455px;" />
				</li>
				<s:if test="workFlowKnowledgeCreate.list_WorkFlowApprove!=null&&workFlowKnowledgeCreate.list_WorkFlowApprove.size>0">
					<li>
						<b>处理记录</b>
					</li>
					<li>
						<table class="biaoge" cellspacing="0" cellpadding="0" border="0" align="center" style="border-bottom:1px solid #ddd;border-right:1px solid #ddd;">
							<tr style="font-weight:bold;">
								<td width="30px;">序号</td>
								<td width="50px;">处理人</td>
								<td width="120px;">处理时间</td>
								<td width="80px;">处理结果</td>
								<td>处理意见</td>
							</tr>
							<s:iterator value="workFlowKnowledgeCreate.list_WorkFlowApprove" var="va" status="statu">
								<tr>
									<td>${statu.index+1 }&nbsp;</td>
									<td>${va.operator }&nbsp;</td>
									<td>${va.optdate } ${va.opttime }&nbsp;</td>
									<td>${va.optertype }&nbsp;</td>
									<td>${va.remark }&nbsp;</td>
								</tr>
							</s:iterator>				
						</table>										
					</li>					
				</s:if>				
				<s:if test='#request.method=="waitAuditing"'>
					<li>
						<b>处理结果</b>
					</li>
					<li>
						<s:if test="#request.workFlowKnowledgeCreate.nodetype==1">
							<s:select id="handleResult" list="#{'1':'提交'}"></s:select>
						</s:if>
						<s:else>
							<s:select id="handleResult" list="#{'1':'通过','2':'驳回','end':'归档'}"></s:select>
						</s:else>
					</li>
					<li>
						<b>处理原因</b>
					</li>
					<li>
						<input type="text" id="handlereason" />
					</li>
					<li>
						<a href="#"><input class="xinxi_an1" type="button" onclick="submitWorkFlow('${type }','${method }','${workFlowKnowledgeCreate.requestid }','submit')" /></a>
						<a href="#"><input class="xinxi_an2" type="button" onclick="resetFromLook()" /></a>
						<s:if test="#request.workFlowKnowledgeCreate.nodetype==1">
							<a href="#"><input class="xinxi_an3" type="button" onclick="submitWorkFlow('${type }','${method }','${workFlowKnowledgeCreate.requestid }','delete')" /></a>
						</s:if>
					</li>
				</s:if>
			</ul>
		</div>
	</div>
</div>
