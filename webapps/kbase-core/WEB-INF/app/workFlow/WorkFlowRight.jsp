<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/My97DatePicker/skin/WdatePicker.css" />
<script type="text/javascript">
	//add by eko.zhan at 2015-04-21
	var SystemKeys = parent.SystemKeys;
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/mxGraph/js/mxClient.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
<style type= "text/css" >  
	td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
	li{cursor:pointer;}
</style>
<script type="text/javascript">
    
   function condition_submit(){
      var method=$("#condition_method").val();
      var type=$("#condition_type").val();
      var params=$("#forms").serialize();
      /**
      $.ajax({
			url : $.fn.getRootPath()+getURL(type,method)+params,
			type : 'POST',
			timeout : 15000,
			sync : false,
			dataType : 'html',
			success : function(data) {
				$('body').html(data);
				$(".conditions").css("display","block");
			}
		}); 
		**/
		
		var _index = layer.load("请稍候...");
      $('form').attr('action',$.fn.getRootPath()+getURL(type,method));
      $('form').submit();
   };
   
   
   //重置
   function cancel(){
     $('.conditions').hide();
   }
   
  function showflow(id,name){
    $("#workflowTypeId_select").val(id);
	$("#workflowTypeNames_select").val(name);
	$("#workflowTypeContent").hide();
  } 
   
  function showMenu(workflowTypeContent,inputName){
		var cityObj = $("#"+inputName);
		var cityOffset = $(cityObj).offset();
		$("#"+workflowTypeContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	    $("#workflowTypeContent").show();
	} 
   
   
  $(document).ready(function (){
        $.ajax({
			url : $.fn.getRootPath() + '/app/individual/individual!knowledgeCreate.htm',
			type : 'POST',
			data:{moduleId:$("#moduleId").val()},
			success : function(data) {
			    var nodes=eval("("+data+")");
			    for(var i=0;i<nodes.length;i++){
			      $("#workflowTypeTree").append('<li onclick=\'showflow("'+nodes[i].id+'","'+nodes[i].name+'")\'><a href="#">'+nodes[i].name+'</a></li>');
			    }
			},
			error : function(data, textStatus, errorThrown) {
				alert('失败!');
			}
		});
		$('body').click (function (e){
	       e = e || window.event;
	      if(e.target.id!="workflowTypeNames_select"){
	         $("#workflowTypeContent").hide();
	      }
	    })
	});
  
  
   
  /** 
    var setting = {
		view: {
			expandSpeed: "fast"//空不显示动画
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback : {
			onClick : onClick
		}
	};
	
	function onClick(event, treeId, treeNode){
		$("#workflowTypeId_select").val(treeNode.id);
		$("#workflowTypeNames_select").val(treeNode.name);
		$("#workflowTypeContent").hide();
	}
	
	function showMenu(workflowTypeContent,inputName){
	    
		var cityObj = $("#"+inputName);
		var cityOffset = $(cityObj).offset();
		$("#"+workflowTypeContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		
		$("body").bind("mousedown", function(event){
			onBodyDown(event,workflowTypeContent)
		});
	}
	function hideMenu(workflowTypeContent) {
		$("#"+workflowTypeContent).fadeOut("fast");
		$("body").unbind("mousedown",function(event){
			onBodyDown(event,workflowTypeContent)
		});
	}
	function onBodyDown(event,workflowTypeContent) {
		if (!(event.target.id == workflowTypeContent || $(event.target).parents("#"+workflowTypeContent).length>0)) {
			hideMenu(workflowTypeContent);
		}
	}
	 
    $(document).ready(function (){
        $.ajax({
			url : $.fn.getRootPath() + '/app/individual/individual!knowledgeCreate.htm',
			type : 'POST',
			data:{moduleId:$("#moduleId").val()},
			success : function(data) {
			    var znodes=eval("("+data+")");
				$.fn.zTree.init($("#workflowTypeTree"), setting, znodes);	
			},
			error : function(data, textStatus, errorThrown) {
				alert('失败!');
			}
		});
		
	});
  **/  
    
    
    
    
    function select(){
      $('.conditions').toggle();
    }
    
    function getURL(type,method){
			if(method=="waitAuditing"){//待我审核
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method;
			}else if(method=="myCreate"){//我发起未归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method;
			}else if(method=="myCreate1"){//我发起已归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method;
			}else if(method=="concluded"){//我处理未归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method;
			}else if(method=="concluded1"){//我处理已归档
				return '/app/auther/work-flow!workFlow.htm?type='+type+'&method='+method;
			}else if(method=="knowledgelook"&&type=="knowledgeCreate"){
				return '/app/auther/work-flow!knowledgeCreateLook.htm?type='+type;
			}else if(method=="knowledgelook"&&type=="knowledgeFeedback"){
				return '/app/auther/work-flow!knowledgeFeedbackLook.htm?type='+type;
			}else if(type== "remmendknowledge" && method == "remmend"){
				return '/app/recommend/recommend.htm?flag=owner';
			}else if(type== "corrections" && method == "hascorrections"){
				return '/app/corrections/corrections!getcorrections.htm?status=1';
			}else if(type== "corrections" && method == "corrections"){
				return '/app/corrections/corrections!getcorrections.htm?status=0';
			}
			
			return false;
		}
		
		
	function openShade(shadeId){
			var w = ($('body').width() - $('#'+shadeId).width())/3;
			$('#'+shadeId).css('left',w+'px');
			$('body').showShade();
			$('#'+shadeId).show();
	}
	
	function closeShade(shadeId){
			$('#'+shadeId).hide();
			$('body').hideShade();	
	}
	
	//分页跳转
	function pageClick(pageNo){
		var _index = layer.load('正在查询数据...');
		var type = $("#type").val();
		var method = $("#method").val();
		if(!getURL(type,method)){
			alert("请求异常!");
			return false;
		}
		
		var params=$('form').serialize();
		location.href = $.fn.getRootPath() + getURL(type,method) + "&" + params + "&pageNo=" + pageNo;
		/* modify by eko.zhan at 2015-02-04
		$.ajax({
			url:$.fn.getRootPath()+getURL(type,method)+"&"+params,
			type:"post",
			data:{"pageNo":pageNo},
			//timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
				//parent.layer.close(_index);
				if (data!=null&&data!="") {
					$('body').html(data);
				} else {
					alert("网络错误，请稍后再试");
				}
			},
				error:function(){
					alert("网络错误，请稍后再试");
				  	//$('.content_right_bottom').ajaxLoadEnd();
				}
		});	
		*/	
	}

	function workFlowLook(id, type, method){
		//mode 流程引擎启用版本(1新版本,默认就版本)
		if($.workflow.isUsable()){
			//var workflow_path  = '<%=PropertiesUtil.value("kbase.workflow.context_path")%>';
			//var knowledgeCreate  = '<%=PropertiesUtil.value("knowledgeCreate")%>';
			
			//modify by eko.zhan at 2015-04-21 15:00
			//var targeturl = '/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid=' + id;
			//$.workflow.open($.workflow.format(targeturl), parent.parent.$);
			$.workflow.editDocument(id, parent.parent.$);
			/*
			var _url = workflow_path + "/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl=" + workflow_path + "/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid=" + id;
			//var _url = workflow_path + "/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl=" + workflow_path + "/ekoz.jsp";
			var _index = parent.parent.$.layer({
			    type: 2,
			    border: [10, 0.3, '#000'],
			    title: "合理化建议",
			    closeBtn: [0, true],
			    //shade: [0],
			    iframe: {src : _url},
			    area: ['1000px', '560px']
			});*/
		}else{
			if(!getURL(type,"knowledgelook")){
				alert("请求异常!");
				return;
			}
			
			$.ajax({
				url:$.fn.getRootPath()+getURL(type,"knowledgelook")+"&method="+method,
				type:"post",
				data:{"id":id},
				timeout:5000,
				async:false,
				dataType:"html",
				success:function(data){
					if (data!=null&&data!="") {
					    $("#workFlowShadeCotent").html(data);
					    openShade("workFlowShade");
					 } else {
					    alert("网络错误，请稍后再试");
					 }
				},
				error:function(){
					 alert("网络错误,请稍后再试");
				}
			});	
		}
		
		
		//prompt("", workflow_path+"/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl=" + workflow_path+"/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid="+id);
		/*
		window.open(workflow_path+"/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl="
			+workflow_path+"/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid="+id,
			knowledgeCreate,"height=600,width=700,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no,alwaysRaised");
		*/
	}
	
	
	function workFlowView(id,type){		
		$.ajax({
			url:$.fn.getRootPath()+'/app/auther/work-flow!getWorkFlowView.htm?type='+type,
			type:"post",
			data:{"id":id},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
				if (data!=null&&data!="") {
				    $("#workFlowShadeCotent").html(data);
				    openShade("workFlowView");
				    mainGraph(document.getElementById('graphContainer'));
				 } else {
				    alert("网络错误，请稍后再试");
				 }
			},
			error:function(){
				 alert("网络错误,请稍后再试");
			}
		});			
	}
	
	var nodeid_current = "";
		function mainGraph(container){
			if (!mxClient.isBrowserSupported()){
				mxUtils.error('Browser is not supported!', 200, false);
			}else{
				var graph = new mxGraph(container);
				//是否已图形的中心进行放大，否则以元素为中心放大  
				graph.centerZoom = false;
				//是否可修改
				graph.setEnabled(false);
				//工具提示
				graph.setTooltips(false);			
				
				//突出节点
				new mxCellTracker(graph,'#00FF00',function(me){
					if(me.getCell().isEdge())return;
					return me.getCell();
				});
				
				//监听点击事件
				graph.addListener(mxEvent.CLICK, function(sender, evt){
					var cell = evt.getProperty('cell');
					//当前选择节点不为空，过滤重复点击	
					if (cell != null && nodeid_current != cell.id){
						//如果选中线
						if(cell.isEdge())return;
						//当前选择的节点
						nodeid_current = cell.id;
						//修改节点审核人员信息	
						$.post($.fn.getRootPath()+'/app/auther/work-flow!getWorkFlowNodeData.htm',
							{"nodeid":nodeid_current,"requestid":'${requestid}'},
							function(data){
								if(data!=null&&data!=""){
									$("#data0").html(data.data0);
									$("#data1").html(data.data1);
									$("#data2").html(data.data2);
								}
						},"json");
					}
				});
				
				
				var parent = graph.getDefaultParent();
								
				graph.getModel().beginUpdate();
				try{					
					var xmlDoc = mxUtils.load($.fn.getRootPath()+'/app/auther/work-flow!getWorkflowGraphXml.htm?requestid=${requestid}').getXml();
					var node = xmlDoc.documentElement;
					var dec = new mxCodec(node.ownerDocument);
					dec.decode(node, graph.getModel());
					
					var stylesheet = graph.getStylesheet();	
					var vertexStyle = stylesheet.getDefaultVertexStyle();
					vertexStyle[mxConstants.STYLE_FILLCOLOR] = "#FFFF00";
					//alert(vertexStyle[mxConstants.STYLE_FILLCOLOR]);

				}finally{
					graph.getModel().endUpdate();
					
					//缩小
					$("#zoomOut").click(function(){
						graph.zoomOut();
					});
					//放大
					$("#zoomIn").click(function(){
						graph.zoomIn();
					});
					
					$("#workFlowApprove").appendTo($(container));
					$("#workFlowApprove").css("display","block");
					var  minHeight = $(container).find("svg:eq(0)").css("min-height");
					$(container).find("svg:eq(0)").css("height",minHeight);
					
				}
			}
		};		
	
</script>


<div id="tagContent">
     <div class="tagContent selectTag" id="tagContent0">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="individual_more">
			<tr>
				<td class="individual_tdbg pl10 fw individual_tdbg1" width="25%">
				     知识标题
				</td>
				<td class="individual_tdbg fw individual_tdbg2" width="25%">
					流程类型
				</td>
				<td class="individual_tdbg fw individual_tdbg2" width="15%">
					当前结点
				</td>
				<td class="individual_tdbg fw individual_tdbg3" width="15%">
					开始时间
				</td>
				
				<td class="individual_tdbg fw individual_tdbg4" width="10%">
					创建者
				</td>
				<td class="individual_tdbg fw individual_tdmax individual_tdbg5" width="15%"><a href="#" class="screening_btn" onclick="select()">筛选</a>
				</td>
			</tr>
			<s:iterator value="page.result" var="va">
				<tr class="individual_moretr">
					<td class="pl10 individual_40" style="width: 25%">
					  <div class="w100">
						<a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')">
						   <p>	
							<s:if test="#va.workFlowForm!=null&&#va.workFlowForm.title!=null&&#va.workFlowForm.title!=''">${va.workFlowForm.title}</s:if><s:else>${va.requestname }</s:else>
						  </p>
						</a>
					  </div>
					</td>
					<td class="individual_15" title="${va.workflowname }" style="width: 35%">
					    <s:if test="#va.workflowname.length() > 12">
							<s:property value="#va.workflowname.substring(0, 12) + '...'"/>
						</s:if>
						<s:else>
							${va.workflowname }
						</s:else>
					</td>
					<td class="individual_15" style="width: 15%">
						${va.currentnodename }
					</td>
					<td class="individual_15" style="width: 15%">
						${va.createdate }
					</td>
					<td class="individual_15"  style="width: 10%">
						${va.createrName }
					</td>
					<!-- 
					<td class="individual_15">
						<a href="#" onclick="workFlowView('${va.id }','${type }')" title="查看">${va.currentnodename }</a>					
					</td>
					 -->
					<td class="individual_10"  style="width: 15%">&nbsp;</td>
				</tr>
			</s:iterator>
			
			<tr class="trfen">
				<td colspan="6">
					<input type="hidden" id="method" value="${method }" />
					<input type="hidden" id="type" value="${type }" />
					<s:if test="#request.type == 'knowledgeCreate' && #request.method == 'waitAuditing'">
						<!--合理化建议 待我审核  -->
						<input type="hidden" id="sum_knowledgeCreate_waitAuditing" value="${page.totalCount }" />
					</s:if>
					<jsp:include page="../util/part_fenye.jsp" flush="true"></jsp:include>
				</td>
		  </tr>
			
		</table>
	</div>
	
</div>

<div class="conditions" style="display: none">
  <div class="conditions_title"><font class="pl10">筛选条件</font></div>
  <div class="conditions_con">
    <form action="" method="post" id="forms">
      <input type="hidden" id="condition_method" value="${method }" />
	  <input type="hidden" id="condition_type" value="${type }" />
	  <input type="hidden" id="moduleId" name="moduleId" value="${moduleId }" />
      <ul>
        <li>开始时间: <input class="Wdate" id="create_time" name="create_time" value="${create_time}" onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly" type="text"/>
        </li>
        <li>结束时间: <input class="Wdate" id="end_time" name="end_time" value="${end_time}" onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly" type="text"/>
        </li>
        <li>创&nbsp;&nbsp;建&nbsp;&nbsp;者: <input id="user_name" value="${user_name}" name="user_name" type="text" />
        </li>
        <li>流程类型: <input name="workflowTypeNames_select" id="workflowTypeNames_select" value="${workflowTypeNames_select}" type="text"　 readonly="readonly" onfocus="showMenu('workflowTypeContent','workflowTypeNames_select')"/>
		 <input id="workflowTypeId_select" name="workflowtype_id" type="hidden" value="${workflowtype_id}"/>
        <li><font>&nbsp;</font><a href="#" class="conditions_btn" onclick="condition_submit()">确定</a><a href="#" onclick="cancel();" class="conditions_btn ml10">取消</a></li>
      </ul>
      
    </form>
  </div>
  <div id="workflowTypeContent" class="workflowTypeContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:155px;">
			<ul id="workflowTypeTree" class="ztree" style="margin-top:0;"></ul>
  </div>
</div>