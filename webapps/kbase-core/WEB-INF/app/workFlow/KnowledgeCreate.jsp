<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>知识库</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_xinjian.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_index.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript">
	var setting = {
		view: {
			expandSpeed: "fast"//空不显示动画
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback : {
			onClick : onClick
		}
	};
	
	var zNodes = ${json_workflowType};
	
	function onClick(event, treeId, treeNode){
		$("#workflowTypeId_select").val(treeNode.id);
		$("#workflowTypeNames_select").val(treeNode.name);
	}
	
	function showMenu(workflowTypeContent,inputName){
		var cityObj = $("#"+inputName);
		var cityOffset = $(cityObj).offset();
		$("#"+workflowTypeContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		
		$("body").bind("mousedown", function(event){
			onBodyDown(event,workflowTypeContent)
		});
	}
	function hideMenu(workflowTypeContent) {
		$("#"+workflowTypeContent).fadeOut("fast");
		$("body").unbind("mousedown",function(event){
			onBodyDown(event,workflowTypeContent)
		});
	}
	function onBodyDown(event,workflowTypeContent) {
		if (!(event.target.id == workflowTypeContent || $(event.target).parents("#"+workflowTypeContent).length>0)) {
			hideMenu(workflowTypeContent);
		}
	}
	
	function submitForm(){
		var workflow_path  = '<%=PropertiesUtil.value("kbase.workflow.context_path")%>';
		
		var workflowTypeId = $("#workflowTypeId_select").val();
		var workflowTypeNames = $("#workflowTypeNames_select").val();
		if(workflowTypeId==""||workflowTypeNames==""){
			parent.layer.alert("流程类型不能为空!", -1);
			return false;
		}
		var _parameters = "&paramName=com.ibot.kbs.cate.path|com.ibot.kbs.cate.id&paramValue="+encodeURI(encodeURI(workflowTypeNames))+"|"+workflowTypeId;
		/*
		window.open(workflow_path+"/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl="
			+workflow_path+"/workflow/request/workflow.jsp?reqtype=kbase&workflowid="+knowledgeCreate+_parameters,
			knowledgeCreate,"height=600,width=700,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no");
			*/
		var _targetUrl = workflow_path + "workflow/request/workflow.jsp?reqtype=kbase&workflowid=" + workflowTypeId;
		var _url = workflow_path + "j_acegi_security_check?j_username=${sessionScope.session_user_key.username}&j_password=1&targeturl=" + _targetUrl ;//+_parameters;
		parent.$.layer({
		    type: 2,
		    border: [10, 0.3, '#000'],
		    title: "合理化建议",
		    closeBtn: [0, true],
		    iframe: {src : _url},
		    area: ['1000px', '600px']
		});
		//关闭当前层
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	}
	
	$(document).ready(function (){
		$.fn.zTree.init($("#workflowTypeTree"), setting, zNodes);
	    
		$("#submitForm").unbind("click");
		$("#submitForm").click(function(){
			submitForm();
		});
	});
	</script>
</head>
<body>
		<div class="content_right_bottom">	
			<!-- 
			<div class="xin_titile">
				<b>合理化建议与问题反馈提交界面</b>
			</div>
			 -->
			<div class="xin_con">
				<ul>
					<li>
						<span><b>请选择流程类型</b><b style="color: red;">*</b></span>
						<input id="workflowTypeNames_select" style="width: 300px;" type="text" readonly="readonly" onfocus="showMenu('workflowTypeContent','workflowTypeNames_select')"/>
						<a href="#"><input class="xin_an1" id="submitForm" type="button" /></a>
						<input id="workflowTypeId_select" type="hidden" />
					</li>
					<li>
						
					</li>
				</ul>
			</div>
		</div>
		<!--******************知识分类***********************-->
		<div id="workflowTypeContent" class="workflowTypeContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:298px;">
			<ul id="workflowTypeTree" class="ztree" style="margin-top:0;"></ul>
		</div>
</body>
</html>
