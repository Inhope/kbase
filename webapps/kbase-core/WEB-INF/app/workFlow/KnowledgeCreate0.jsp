<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_xinjian.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_index.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript">
function resetForm(){
	$("input[id$='_form']").each(function(){
		$(this).attr("value","");
	});	
	
	$("textarea[id$='_form']").each(function(){
		$(this).attr("value","");
	});
	
	$("select[id$='_form']").each(function(){
		$(this)[0].selectedIndex = '';
	});	
}

function submitForm(){
	var title = $("#title_form").val();
	var content = $("#content_form").val();
	var reason = $("#reason_form").val();
	if(title==""){
		alert("问题/知识标题不能为空!");
		return false;
	}
	if(content==""){
		alert("内容不能为空!");
		return false;
	}
	if(reason==""){
		alert("发布原因不能为空!");
		return false;
	}
	
	$('body').ajaxLoading('正在保存数据...');
	$.ajax({
		type : 'post',
		url : $.fn.getRootPath() + "/app/auther/work-flow!knowledgeCreateDo.htm", 
		data : {
			"title" : title,
			"content" : content,
			"reason" : reason
		},
		async: true,
		dataType : 'html',
		success : function(data){
			if(data == '1'){
				alert("保存成功!");
				resetForm();
			}else{
				alert("网络错误，请稍后再试!");
			}
			$('body').ajaxLoadEnd();
		},
		error : function(msg){
			alert("网络错误，请稍后再试!");
			$('body').ajaxLoadEnd();
		}
	});
}

$(document).ready(function (){
	$("#resetForm").unbind("click");
	$("#resetForm").click(function (){
		resetForm();
	});
	
	$("#submitForm").unbind("click");
	$("#submitForm").click(function(){
		submitForm();
	});
});

</script>

</head>

<body>
<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xin_titile">
				<b>合理化建议与问题反馈提交界面</b>
			</div>
			<div class="xin_con">
				<ul>
					<li>
						<span><b>问题/知识标题</b><b style="color: red;">*</b></span>
						<input id="title_form" type="text" />
					</li>
					<!--<li>
						<span>所属目录</span>
						<select id="list_form" size="1">
							<option>
								达美乐1
							</option>
							<option>
								达美乐2
							</option>
							<option>
								达美乐3
							</option>
							<option>
								时间的4
							</option>
						</select>
					</li>
					-->
					<li>
						<span><b>内容</b><b style="color: red;">*</b></span>
						<textarea id="content_form" cols="" rows=""></textarea>
					</li>
					<li>
						<span><b>备注</b></span>
						<!--<textarea id="reason_form" cols="" rows=""></textarea>
					-->
						<input id="reason_form" type="text" />
					</li>
					<li>
						<a href="#"><input class="xin_an1" id="submitForm" type="button" /></a>
						<a href="#"><input class="xin_an2" id="resetForm" type="button" /></a>
					</li>
				</ul>
			</div>
		</div>
		<!--******************内容结束***********************-->
</body>
</html>
