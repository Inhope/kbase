<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinxi.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript">
function resetForm(){
	$("input[id$='_form']").each(function(){
		$(this).attr("value","");
	});	
	
	$("textarea[id$='_form']").each(function(){
		$(this).attr("value","");
	});
	
	$("select[id$='_form']").each(function(){
		$(this)[0].selectedIndex = '';
	});	
}

function submitForm(){
	var title = $("#title_form").val();
	var objId = $("#objId").val();
	var objName = $("#objName").val();
	var content = $("#content_form").val();
	if(objId=="")return false;
	if(title==""){
		alert("反馈标题不能为空!");
		return false;
	}
	if(content==""){
		alert("反馈内容不能为空!");
		return false;
	}
	
	$('body').ajaxLoading('正在保存数据...');
	$.ajax({
		type : 'post',
		url : $.fn.getRootPath() + "/app/auther/work-flow!knowledgeFeedbackDo.htm", 
		data : {
			"title" : title,
			"objId" : objId,
			"objName" : objName,
			"content" : content
		},
		async: true,
		dataType : 'html',
		success : function(data){
			if(data == '1'){
				alert("保存成功!");
				resetForm();
			}else{
				alert("网络错误，请稍后再试!");
			}
			closeShade('workFlowShade');
			$('body').ajaxLoadEnd();
		},
		error : function(msg){
			alert("网络错误，请稍后再试!");
			$('body').ajaxLoadEnd();
		}
	});
}

$(document).ready(function (){
	$("#resetForm").unbind("click");
	$("#resetForm").click(function (){
		resetForm();
	});
	
	$("#submitForm").unbind("click");
	$("#submitForm").click(function(){
		submitForm();
	});
});

</script>


<div id="workFlowShade" class="xinxi_d"
	style="display: none; position: fixed; z-index: 10001; _position: absolute;height: 300px;">
	<div class="xinxi_dan" style="height: 300px;">
		<div class="xinxi_dan_title">
			<b>知识反馈</b>
			<a href="#" onclick="closeShade('workFlowShade')"> <img
					src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
					width="12" height="12" /> </a>
		</div>
		<div class="xinxi_dan_con" style="height: 260px;">
			<ul>
				<li>
					<b>反馈标题</b>
				</li>
				<li>
					<input type="text" id="title_form" />
				</li>
				<li>
					<b>知识实例</b>
				</li>
				<li>
					<input type="text" id="objName" value="${objName }" readonly="readonly" />
					<input type="hidden" id="objId" value="${objId }" readonly="readonly" />
				</li>
				<li>
					<b>反馈内容</b>
				</li>
				<li>
					<textarea id="content_form" cols="" rows=""></textarea>
				</li>			
				<li>
					<a href="#"><input class="xinxi_an1" type="button" id="submitForm" /></a>
					<a href="#"><input class="xinxi_an2" type="button" id="resetForm" /></a>
				</li>
			</ul>
		</div>
	</div>
</div>

<!-- 
<div class="content_right_bottom">
	<div class="xin_titile">
		<b>知识反馈</b>
	</div>
	<div class="xin_con">
		<ul>
			<li>
				<span>反馈标题</span>
				<input type="text" id="title_form" />
			</li>
			<li>
				<span>知识实体</span>
				<input type="text" id="objName" value="${objName }"
					readonly="readonly" />
				<input type="hidden" id="objId" value="${objId }"
					readonly="readonly" />
			</li>
			<li>
				<span>反馈内容</span>
				<textarea id="content_form" cols="" rows=""></textarea>
			</li>
			<li>
				<a href="#"><input class="xin_an1" id="submitForm" type="button" />
				</a>
				<a href="#"><input class="xin_an2" id="resetForm" type="button" />
				</a>
			</li>
		</ul>
	</div>
</div>

 -->


