<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>公告管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
				overflow: hidden;
			}
			.kbs-input{
				border: 1px solid #cdcdcd;
				width: 150px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			var pageContext = {
				addTab: function(args){
					var opts = $.extend({
						forceOpen: false
					}, args);
					if (opts.forceOpen){	//是否强制打开，如果强制打开则不判断该页面是否存在
						$('#tabs').tabs('add', {
							id: opts.id,
							title: opts.title,
						    content: '<iframe src="" scrolling="auto" frameborder="0" style="width:100%;height:580px;overflow-y:auto;border:0;"></iframe>',
						    closable: opts.closable==undefined?true:opts.closable,
						    width: '100%',
						    height: '98%'
						});
						
						//modify by eko.zhan at 2016-07-11 10:40 jeasyui tab iframe 在低版本浏览器上会加载两次，尽管第一次请求是 aborted，但后台日志依然记录，导致都是两次
						var tab = $('#tabs').tabs('getSelected');
						tab.find('iframe').attr('src', opts.url);
					}else if ($('#tabs').tabs('exists', opts.title)){
						//已存在相同标题页签
						$('#tabs').tabs('select', opts.title);
					}else{
						$('#tabs').tabs('add', {
							id: opts.id,
							title: opts.title,
						    content: '<iframe src="" scrolling="auto" frameborder="0" style="width:100%;height:580px;overflow-y:auto;border:0;"></iframe>',
						    closable: opts.closable==undefined?true:opts.closable,
						    width: '100%',
						    height: '98%'
						});
						
						//modify by eko.zhan at 2016-07-11 10:40 jeasyui tab iframe 在低版本浏览器上会加载两次，尽管第一次请求是 aborted，但后台日志依然记录，导致都是两次
						var tab = $('#tabs').tabs('getSelected');
						tab.find('iframe').attr('src', opts.url);
					}
				}
			};
		
			$(function(){
				$('#btnGoback').on('click', function(){
					try{
					/*
						opener.TABOBJECT.open({
							id : 'notice',
							name : '公告管理',
							hasClose : true,
							url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isVirgin=1&isNote=0',
							isRefresh : true
						}, opener.TABOBJECT);
						*/
						opener.KbasePlugin.guideNotice();
					}catch(e){
						alert(e.message);
					}
					
					//关闭当前窗口
					window.close();
				});
				
				//tabs右键菜单
				$('#menuCloseAll').on('click', function(e){
					var tabs = $('#tabs').tabs('tabs');
					$(tabs).each(function(i, item){
						var opts = $(item).panel('options');
						if (opts.closable){
							$('#tabs').tabs('close', opts.title);
						}
					});
				});
				//关闭其他菜单
				$('#menuCloseOther').on('click', function(){
					var selectedTab = $('#tabs').tabs('getSelected');
					var title = $(selectedTab).panel('options').title;
					var tabs = $('#tabs').tabs('tabs');
					$(tabs).each(function(i, item){
						var opts = $(item).panel('options');
						if (opts.closable && opts.title!=title){
							$('#tabs').tabs('close', opts.title);
						}
					});
				});
			
				$('#tabs').tabs({
					onSelect: function(title, index){
						if (title=='收件箱'){
							if ($('#inbox[_isload="0"]').length>0){
								$('#inbox').attr({
									'_isload': 1,
									'src': '${pageContext.request.contextPath }/app/notice/bulletin!inbox.htm'
								});
							}
						}else if (title=='发件箱'){
							if ($('#outbox[_isload="0"]').length>0){
								$('#outbox').attr({
									'_isload': 1,
									'src': '${pageContext.request.contextPath }/app/notice/bulletin!outbox.htm'
								});
							}
						}
					},
					onContextMenu: function(e, title, index){
						e.preventDefault();
						
						$('#tabs').tabs('select', title);
						
						$('#mm').menu('show', {
	                        left: e.pageX,
	                        top: e.pageY
	                    });
					}
				});
			});
		</script>
	</head>

	<body>
		<div id="mm" class="easyui-menu" style="width:120px;">
        	<div id="menuCloseOther">关闭其他标签</div>
        	<div id="menuCloseAll">关闭所有标签</div>
        </div>
		<!-- <div id="tab-tools">
	        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true, iconCls:'icon-add'" id="btnGoback">返回旧版</a>
	    </div> -->
		<div id="tabs" data-options="fit:true,tools:'#tab-tools'">
			<div title="收件箱" style="overflow:hidden;">
	        	<iframe id="inbox" _isload="0" src="" scrolling="no" frameborder="0" style="width:100%;height:98%;"></iframe>
	        </div>
	        <c:if test="${requestScope.isBulletinAdmin}">
			    <c:choose>
			       <c:when test="${param.seltype == 1}">		
			        	<div title="发件箱">
			        		<iframe id="outbox" _isload="0" src="" scrolling="no" frameborder="0" style="width:100%;height:98%;"></iframe>
			        	</div>			       	        	       
			       </c:when>
			       <c:otherwise>
			        	<div title="发件箱" data-options="selected:true" >
			        		<iframe id="outbox" _isload="0" src="" scrolling="no" frameborder="0" style="width:100%;height:98%;"></iframe>
			        	</div>
			       </c:otherwise>
			    </c:choose>	        
	        </c:if>		    
	    </div>
	</body>
</html>
