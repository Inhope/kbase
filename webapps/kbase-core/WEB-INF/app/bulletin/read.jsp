<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>公告详情</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
				overflow-x:no;
				overflow-y:auto;
			}
			.kbs-title{
				font-size:24px;
				font-weight:bolder;
			}
			.kbs-subtitle{
				font-size:12px;
			}
		</style>
		
		
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
					
			$(function(){
				$('a[_kbs_onto_cate=1]').on('click', function(){
					var _id = $(this).attr('_id');
					var _text = $(this).text();
					//http://172.16.8.137:8080/kbase-core/app/category/category.htm?type=1&categoryId=72774d6098264ed49fb6041dbb827a2b&categoryName=10086%E7%94%B5%E8%AF%9D%E6%94%AF%E4%BB%98%E5%85%85%E5%80%BC%E4%B8%9A%E5%8A%A1
					var _url = '${pageContext.request.contextPath}/app/category/category.htm?&key=BULLETIN&type=1&categoryId=' + _id + '&categoryName=' + _text;
					if ('${requestScope.enableCateTag}'=='false'){
						_url = '${pageContext.request.contextPath}/app/search/search.htm?searchMode=3&askContent=' + _text;
					}
					if (parent.pageContext && parent.pageContext.addTab){
						parent.pageContext.addTab({
							id: _id,
							title: _text,
							url: _url
						});
					}else{
						//
						parent.TABOBJECT.open({
							id : 'values',
							name : _text,
							hasClose : true,
							url : _url,
							isRefresh : true
						});
					}
				});
				
				//点击查看人员名单
				$('#btnView').on('click', function(){
					if ($('#readUserPanel:hidden').length==1){
						$('#btnView').text('点击隐藏名单');
						$('#readUserPanel').show();
					}else{
						$('#btnView').text('点击查看名单');
						$('#readUserPanel').hide();
					}
				});
				
				//编辑最新版本的公告
				$("#btnEdit").on('click', function(){
	            	var bulletinId = $('#id').val();
	            	location.href = '${pageContext.request.contextPath}/app/notice/bulletin!create.htm?id=' + bulletinId;
	            });
	             
	             //公告收藏
				$('#btnBulletinCollect').click(function(){
					var param = [];
					var locations = ''; 
					if(${empty bulletin.bizLocation }){
						locations = '全部地市';
					}else{
						locations = '${bulletin.bizLocation }';
					}
					window.__kbs_fav_params = param;
					//这里传入很多为空的参数是防止在IE7下解析数组出错，共用一个方法
					param.push({
						id: $('#id').val(),
						name: '${bulletin.title }',
						locations: locations,
						question: '',
						title: '',
						content: '',
						answer: ''
					});
					
					var url = '${pageContext.request.contextPath}/app/fav/fav-detail.htm?key=BULLETIN';
					window._open(url, '请选择收藏夹', 800, 400);
					return false;
				});
				
	            //点击历史公告链接，查看历史公告详情（另开标签页）
	             $(".openversion").on('click', function(){
             		var _hisid = $(this).attr('_hisid');
             		var _title = $(this).attr('_title');
	                var _url = $.fn.getRootPath()+'/app/notice/bulletin!openVersion.htm?hisId=' + _hisid;
	                //modify by heart.cao 解决首页打开公告，无法查看历史版本信息的BUG 2016-04-27
	                if (parent.pageContext && parent.pageContext.addTab){
	                	parent.pageContext.addTab({
							id: _hisid,
							title: _title,
							url: _url
						});
	                }else{
	                    parent.TABOBJECT.open({
							id : _hisid,
							name : _title,
							hasClose : true,
							url : _url,
							isRefresh : true
						});
	                }  
             	});
             	//modify  by heart.cao  添加当前用户为当前公告的已阅人 2016-05-31
                window.setTimeout(function(){
	                var _url = '${pageContext.request.contextPath}/app/notice/bulletin!appendReader.htm'; 
                    $.post(_url
	                ,{
	                  'id' : $('#bulletinId').val()
	                },function(data){
	                   if(data && data.rst == '0'){
	                       alert(data.msg);
	                   }
	                },'json');                
                },3*1000)                
			});
			
		</script>
		
	</head>

	<body>
		<br/><br/>
		<table cellspacing="0" cellpadding="0" style="width:92%" align="center">
			<tr>
				<td align="center" valign="middle" style="line-height: 30px;">
					<span class="kbs-title">${bulletin.title }</span><br>
					<span class="kbs-subtitle">
						${bulletin.createUserName } 
						<fmt:formatDate value="${bulletin.editTime }" pattern="yyyy-MM-dd HH:mm"/>
						(
							<c:choose>
								<c:when test="${empty bulletin.bizLocation }">全部地市</c:when>
								<c:otherwise>${bulletin.bizLocation }</c:otherwise>
							</c:choose>
						/ 
							<c:choose>
								<c:when test="${empty bulletin.bizType }">全部类型</c:when>
								<c:otherwise>${bulletin.bizType }</c:otherwise>
							</c:choose>
						)
					</span>
				</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" class="box" style="width:92%" align="center">
			<tr>
				<td style="line-height: 30px;">${bulletin.content }</td>
			</tr>
			<c:if test="${not empty bulletin.ontoValueId}">
				<tr>
					<td style="line-height: 26px;">
						<c:set value="${fn:split(bulletin.ontoValueName, ',')}" var="ontoValueNames" />
						<c:set value="${fn:split(bulletin.ontoValueId, ',')}" var="ontoValueIds" />
						<c:forEach items="${ontoValueIds }" var="ontoValueId" varStatus="status">
							<a href="javascript:void(0)" _id="${ontoValueId }" _kbs_onto_cate=1>${ontoValueNames[status.index] }</a><br>
						</c:forEach>
					</td>
				</tr>
			</c:if>
			
			<!-- modify by alan.zhang at 2016-03-07 10:20:45 -->
			<%--  --%>
			<c:if test="${not empty bulletin.fileSet}">
				<tr>
					<td style="line-height: 30px;">
						<c:forEach items="${bulletin.fileSet }" var="attach" varStatus="status">
							${status.index+1 }.&nbsp;<a href="${swfAddress }/convert/download.do?dir=bulletin&filename=${attach.fileName }&realname=${attach.realName }">${attach.realName }</a><br>
						</c:forEach>
					</td>
				</tr>
			</c:if>
			
			
			<tr>
				<td style="line-height: 26px;">
					<c:set var="readUserList" value="${fn:split(bulletin.readUser, ',')}"></c:set>
					已阅 ${fn:length(readUserList)} 人，<span id="btnView" style="cursor:pointer;color:#0000FF;">点击查看名单</span><br/>
					<span id="readUserPanel" style="display:none">${bulletin.readUser }</span>
				</td>
			</tr>
			
			
		    <c:if test="${not empty bulletin.bulletinVersionSet}">
		    	<tr>
	              <td>
	                  <b>历史版本</b>
	              </td>
	            </tr>
		        
		    	<c:set var="idx" value="${fn:length(bulletin.bulletinVersionSet)}"></c:set>
				<tr>
					<td style="line-height: 30px;">
						<c:forEach items="${bulletin.bulletinVersionSet }" var="btinVer" varStatus="status">
							${idx}.&nbsp;
							<a class="openversion" href="javascript:void(0)" _hisid="${btinVer.hisId }" _title="${btinVer.title }">
								${btinVer.createUserName }(<fmt:formatDate value="${btinVer.editTime }" pattern="yyyy-MM-dd HH:mm:ss"/>)${btinVer.title }
							</a><br>
							<c:set var="idx" value="${idx-1}"></c:set>
						</c:forEach>
					</td>
				</tr>
			</c:if>

	           <c:if test="${requestScope.isBulletinAdmin}">
	             <tr>
	                <td align="right">
	                  <input type="hidden" id="id" name="id" value="${bulletin.id }"/>
	                  <!-- <button id="btnEdit">编辑</button> -->
	                  <c:if test="${bulletionStr=='BULLETION'}">
	                  	<input type="button" value="收藏" id="btnBulletinCollect">
	                  </c:if>
	                  <input type="button" value="编辑" id="btnEdit">
	                </td>
	              </tr>
	           </c:if>
			
			
		</table>
		<div style="display:none">
		   <input type="hidden" id="bulletinId" name="bulletinId" value="${bulletin.id }"/>
		</div>
		
	</body>
</html>