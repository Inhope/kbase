<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String checkLocations = ",深圳,";
String defaultlocation = (String)session.getAttribute("defaultlocation");
defaultlocation = (defaultlocation!=null && checkLocations.indexOf(","+defaultlocation.trim()+",")!=-1)?defaultlocation.trim():"";
request.setAttribute("defaultlocation",defaultlocation);
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>收件箱</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.kbs-input{
				border: 1px solid #cdcdcd;
				width: 150px;
			}
			.datagrid-row{
				height: 30px;
			}
			
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//按回车查询
				$('#tb-inbox input[name="title"]').keyup(function(e){
					if (e.keyCode==13){
						$('a[name="btnQuery"]').click();
					}
				});
				//收件箱查询
				$('a[name="btnQuery"]').on('click', function(){
					var title = $('#tb-inbox input[name="title"]').val();
					var startTime = $('#tb-inbox input[name="startTime"]').val();
					var endTime = $('#tb-inbox input[name="endTime"]').val();
					var createUser = $('#tb-inbox input[name="createUser"]').val();
					
					var bizLocationArr = [];
					$('#tb-inbox input[name="bizLocation"]:checked').each(function(i, item){
						bizLocationArr.push(item.value);
					});
					var bizTypeArr = [];
					$('#tb-inbox input[name="bizType"]:checked').each(function(i, item){
						bizTypeArr.push(item.value);
					});		
					var filterRead = 0;
					if($('#tb-inbox input[name="filterRead"]')[1].checked){
					    filterRead = 1;
					}else if($('#tb-inbox input[name="filterRead"]')[2].checked){
					    filterRead = 2;
					}			
					var _opts = $('#inboxGrid').datagrid("options");
				    _opts.url = '${pageContext.request.contextPath }/app/notice/bulletin!inboxList.htm';
					$('#inboxGrid').datagrid('load', {
						'bulletin.title': title,
						'bulletin.startTime': startTime,
						'bulletin.endTime': endTime,
						'bulletin.createUser': createUser,
						'bulletin.bizLocation': bizLocationArr.join(','),
						'bulletin.bizType': bizTypeArr.join(','),
						'bulletin.filterRead': filterRead
					});
					
				});
				//刷新
				$('a[name="btnRefresh"]').on('click', function(){
					//modify by eko.zhan at 2016-08-09 10:55 根据需求刷新和查询区分
					$('a[name="btnReset"]').click();
					$('a[name="btnQuery"]').click();
					// $('#inboxGrid').datagrid('reload');  //modify by heart.cao 2016-04-15 刷新执行查询操作
					//$('a[name="btnQuery"]').click();
				});
				
				//公告收藏夹
				$('a[name="btnBulletin"]').on('click', function(){
					//这里URL bulletinManage 为1 说明是走公告管理进入 收藏夹
					parent.pageContext.addTab({
						id: 'btnBulletin',
						title: '公告收藏夹',
						url: '${pageContext.request.contextPath}/app/fav/fav-clip-object.htm?fid=BULLETIN&bulletinManage=1'
					});
				});
				
				//收件箱重置
				$('a[name="btnReset"]').on('click', function(){
					$('#tb-inbox input[name="title"]').val('');
					$('#tb-inbox input[name="startTime"]').val('');
					$('#tb-inbox input[name="endTime"]').val('');
					$('#tb-inbox input[name="createUser"]').val('');
					$('#tb-inbox input[name="bizLocation"]:checked').removeAttr('checked');
					$('#tb-inbox input[name="bizType"]:checked').removeAttr('checked');
					$('#tb-inbox input[name="filterRead"]')[0].checked = true;
				});
				
				//grid 双击行
				$('#inboxGrid').datagrid({
					onDblClickRow: function(index, row){
						parent.pageContext.addTab({
							id: row.id,
							title: row.title + '-公告',
							url: '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + row.id
						});
						//未读行已读后粗体改变成正常体
						if (!row.isread){
							$('tr[datagrid-row-index="' + index + '"]').css({'color' : '#000000','font-weight': 'normal'});
						}
					},
					onClickRow: function(index, row){
						parent.pageContext.addTab({
							id: row.id,
							title: row.title + '-公告',
							url: '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + row.id
						});
						//未读行已读后粗体改变成正常体
						if (!row.isread){
							$('tr[datagrid-row-index="' + index + '"]').css({'color' : '#000000','font-weight': 'normal'});
						}
					},
					onLoadSuccess: function(){
						//隐藏分页条中每页显示的记录条数select控件
						$('.pagination-page-list').hide();
					}
				});
				$('a[name="btnQuery"]').click();
			});
		</script>
	</head>

	<body>
		<div id="tb-inbox" style="padding:5px;display:none;">
			标题/内容：<input type="text" name="title" class="kbs-input">
			创建日期：<input type="text" name="startTime" class="kbs-input" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly"> ~ <input type="text" name="endTime" class="kbs-input" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly">
			发布人：<input type="text" name="createUser" class="kbs-input">
			<!-- <select name="filterRead">
			      <option value="0" selected>全部</option>
			      <option value="1" >未读</option>
			      <option value="2" >已读</option>
			</select> -->
			<input type="radio" name="filterRead" value="0" checked>全部
			<input type="radio" name="filterRead" value="1">未读
			<input type="radio" name="filterRead" value="2">已读
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnQuery">查询</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnReset">重置</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnRefresh">刷新</a>
			<c:if test="${bulletionStr=='BULLETION'}">
				<a href="javascript:void(0)" class="easyui-linkbutton" name="btnBulletin">公告收藏夹</a>
			</c:if>
			<br/>
			<c:if test="${fn:length(requestScope.bizLocationList)>0}">
				地市：
					<c:forEach items="${requestScope.bizLocationList}" var="item">
						<c:choose>
						     <c:when test="${item.name==requestScope.defaultlocation}">
						          <input type="checkbox" name="bizLocation" value="${item.name }" checked="checked">${item.name}
						     </c:when>
						     <c:otherwise>
						          <input type="checkbox" name="bizLocation" value="${item.name }" >${item.name}
						     </c:otherwise>
						</c:choose>
					</c:forEach>
				<br/>
			</c:if>
			<c:if test="${fn:length(requestScope.bizTypeArr)>0}">
				类型：
					<c:forEach items="${requestScope.bizTypeArr}" var="item">
				    	<input type="checkbox" name="bizType" value="${item }" />${item }
				    </c:forEach>
			</c:if>
		</div>
		<table id="inboxGrid" style="overflow:hidden;height:540px;" 
		    url="${pageContext.request.contextPath }/app/notice/bulletin!inboxList.htm?bulletin.bizLocation=${requestScope.defaultlocation }"
	        rownumbers="true" pagination="true"
	        data-options="toolbar:'#tb-inbox', singleSelect:true, 
	        	rowStyler: function(index, row){
					if (!row.isread){
						return 'color:#0000FF;font-weight:bold;';
					}
				}">
			<thead>
				<tr>
					<th field="title" width="45%" data-options="formatter: function(value, row, index){
					    var _title = row.title;
						if (row.istop==1){
						    _title = '<font color=\'#FF0000\'>【置顶】</font> ' + _title;
						}
						if(row.hasfile=='1'){
					       _title =  _title + '&nbsp;<img style=\'width:16px;height:16px;\' src=\'${pageContext.request.contextPath}/theme/notice-attachment.png\'  />';
					    }
						return _title;
					}">标题</th>
					<!-- <th field="createtime" width="10%">创建时间</th> -->
					<th field="bizlocation" width="17%">地市</th>
					<th field="biztype" width="18%">类型</th>
					<th field="edittime" width="10%">发布时间</th>
					<th field="createuser" width="10%">发布人</th>
					<!-- <th field="endtime" width="10%">结束时间</th> -->
					<!-- <th field="scope" width="30%">发布范围</th> -->
				</tr>
			</thead>
			
			
			
		</table>
	</body>
</html>
