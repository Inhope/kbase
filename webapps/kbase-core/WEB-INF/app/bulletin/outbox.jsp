<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>发件箱</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.kbs-input{
				border: 1px solid #cdcdcd;
				width: 150px;
			}
			.datagrid-row{
				height: 30px;
			}
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//按回车查询
				$('#tb-outbox input[name="title"]').keyup(function(e){
					if (e.keyCode==13){
						$('a[name="btnQuery"]').click();
					}
				});
				//新增
				$('a[name="btnNew"]').on('click', function(){
					parent.pageContext.addTab({
						id: 'navBulletin',
						title: '新增公告',
						url: '${pageContext.request.contextPath}/app/notice/bulletin!create.htm'
					});
				});
				//删除
				$('a[name="btnDel"]').on('click', function(){
					var selRows = $('#outboxGrid').datagrid('getChecked');
					if (selRows.length == 0){
						alert('请选择待删除的数据');
						return false;
					}
					if (window.confirm("确定确定删除吗？")){
						var ids = '';
				  		for(var i = 0; i < selRows.length; i++){
				  			if(i==0){
				  				ids = selRows[i].id;
				  			}else{
				  				ids += "," + selRows[i].id;
				  			}
				  		}
				  		$.ajax({
					   		type: "POST",
					   		url: "${pageContext.request.contextPath}/app/notice/bulletin!delete.htm",
					   		data: {ids:ids},
					   		dataType:'json',
					   		success: function(data){
					   			if (data.rst == "1") {
									alert(data.msg);
									$('a[name="btnRefresh"]').click();
								} else {
									alert(data.msg);
								}
					   		},
					   		error: function(){
					   			alert("删除失败");
					   		}
						});
					}
				
				
				});
				
				//过期
				$('a[name="btnExpire"]').on('click', function(){
					$.post('${pageContext.request.contextPath}/app/notice/bulletin!expired.htm');
				});
				//刷新
				$('a[name="btnRefresh"]').on('click', function(){
					//modify by eko.zhan at 2016-08-09 10:55 根据需求刷新和查询区分
					$('a[name="btnReset"]').click();
					$('a[name="btnQuery"]').click();
					//$('#outboxGrid').datagrid('reload');
				});
				//置顶=1
				$('a[name="btnTop"]').on('click', function(){
					topBulletin(1);
				});
				//取消置顶=0
				$('a[name="btnCancelTop"]').on('click', function(){
					topBulletin(0);
				});
				
				//公告收藏夹
				$('a[name="btnBulletin"]').on('click', function(){
					//这里URL bulletinManage 为1 说明是走公告管理进入 收藏夹
					parent.pageContext.addTab({
						id: 'btnBulletin',
						title: '公告收藏夹',
						url: '${pageContext.request.contextPath}/app/fav/fav-clip-object.htm?fid=BULLETIN&bulletinManage=1'
					});
				});
				
				//发件箱查询
				$('a[name="btnQuery"]').on('click', function(){
					var title = $('#tb-outbox input[name="title"]').val();
					var startTime = $('#tb-outbox input[name="startTime"]').val();
					var endTime = $('#tb-outbox input[name="endTime"]').val();
					var createUser = $('#tb-outbox input[name="createUser"]').val();
					
					var bizLocationArr = [];
					$('#tb-outbox input[name="bizLocation"]:checked').each(function(i, item){
						bizLocationArr.push(item.value);
					});
					var bizTypeArr = [];
					$('#tb-outbox input[name="bizType"]:checked').each(function(i, item){
						bizTypeArr.push(item.value);
					});
					
					$('#outboxGrid').datagrid('load', {
						'bulletin.title': title,
						'bulletin.startTime': startTime,
						'bulletin.endTime': endTime,
						'bulletin.createUser': createUser,
						'bulletin.bizLocation': bizLocationArr.join(','),
						'bulletin.bizType': bizTypeArr.join(',')
					});
					
				});
				//发件箱重置
				$('a[name="btnReset"]').on('click', function(){
					$('#tb-outbox input[name="title"]').val('');
					$('#tb-outbox input[name="startTime"]').val('');
					$('#tb-outbox input[name="endTime"]').val('');
					$('#tb-outbox input[name="createUser"]').val('');
					$('#tb-outbox input[name="bizLocation"]:checked').removeAttr('checked');
					$('#tb-outbox input[name="bizType"]:checked').removeAttr('checked');
				});
				
				//grid 双击行
				$('#outboxGrid').datagrid({
					onDblClickRow: function(index, row){
						parent.pageContext.addTab({
							id: row.id,
							title: row.title + '-公告',
							url: '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + row.id
						});
					},
					onClickRow: function(index, row){
						parent.pageContext.addTab({
							id: row.id,
							title: row.title + '-公告',
							url: '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + row.id
						});
					},
					onLoadSuccess: function(){
						//隐藏分页条中每页显示的记录条数select控件
						$('.pagination-page-list').hide();
					}
				});
			});
			
			
			function topBulletin(top){
				var cancelTop = '';
				if(top == 0){
					cancelTop = '取消';
				}
				var selRows = $('#outboxGrid').datagrid('getChecked');
				if (selRows.length == 0){
					alert('请选择待' + cancelTop + '置顶的数据');
					return false;
				}
				if (confirm("确定"+cancelTop+"置顶 "+selRows.length+" 条数据？")){
					var ids = '';
			  		for(var i = 0; i < selRows.length; i++){
			  			if(i==0){
			  				ids = selRows[i].id;
			  			}else{
			  				ids += "," + selRows[i].id;
			  			}
			  		}
			  		$.ajax({
				   		type: "POST",
				   		url: "${pageContext.request.contextPath}/app/notice/bulletin!top.htm",
				   		data: {ids:ids, top:top},
				   		dataType:'json',
				   		success: function(data){
				   			if (data.rst == "1") {
								alert(data.msg);
								$('a[name="btnRefresh"]').click();
							} else {
								alert(data.msg);
							}
				   		},
				   		error: function(){
				   			alert(cancelTop+"置顶失败");
				   		}
					});
				}
			    
			}
			
			
		</script>
	</head>

	<body>
		<div id="tb-outbox" style="padding:5px;display:none;">
			标题/内容：<input type="text" name="title" class="kbs-input">
			创建日期：<input type="text" name="startTime" class="kbs-input" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly"> ~ <input type="text" name="endTime" class="kbs-input" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly">
			发布人：<input type="text" name="createUser" class="kbs-input">
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnQuery">查询</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnReset">重置</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnDel">召回</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnExpire" style="display:none;">过期</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnNew">新增</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnRefresh">刷新</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnTop">置顶</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnCancelTop">取消置顶</a>
			<c:if test="${bulletionStr=='BULLETION'}">
				<a href="javascript:void(0)" class="easyui-linkbutton" name="btnBulletin">公告收藏夹</a>
			</c:if>
			<br/>
			<c:if test="${fn:length(requestScope.bizLocationList)>0}">
				地市：
					<c:forEach items="${requestScope.bizLocationList}" var="item">
						<input type="checkbox" name="bizLocation" value="${item.name }">${item.name }
					</c:forEach>
				<br/>
			</c:if>
			<c:if test="${fn:length(requestScope.bizTypeArr)>0}">
				类型：
				<c:forEach items="${requestScope.bizTypeArr}" var="item">
			    	<input type="checkbox" name="bizType" value="${item }" />${item }
			    </c:forEach>
			</c:if>
		</div>
		<table id="outboxGrid" style="width:100%;height:540px;overflow:hidden;"
	        url="${pageContext.request.contextPath }/app/notice/bulletin!outboxList.htm"
	        rownumbers="true" pagination="true"
	        data-options="toolbar:'#tb-outbox',singleSelect:false">
			<thead>
				<tr>
	            	<th data-options="field:'id',width:'5%',checkbox:true"></th>
					<th field="title" width="20%" data-options="formatter: function(value, row, index){
					    var _title = row.title;
						if (row.istop==1){
						    _title = '<font color=\'#FF0000\'>【置顶】</font> ' + _title;
						}
						if(row.hasfile=='1'){
					       _title =  _title + '&nbsp;<img style=\'width:16px;height:16px;\' src=\'${pageContext.request.contextPath}/theme/notice-attachment.png\'  />';
					    }
						return _title;
					}">标题</th>
					<!--<th field="createtime" width="10%">创建时间</th>  -->
					<th field="bizlocation" width="7%">地市</th>
					<th field="biztype" width="8%">类型</th>					
					<th field="edittime" width="10%">发布时间</th>
					<th field="createuser" width="10%">发布人</th>
					<th field="endtime" width="10%">结束时间</th>
					<th field="scope" width="30%">发布范围</th>
				</tr>
			</thead>
						
		</table>
	</body>
</html>
