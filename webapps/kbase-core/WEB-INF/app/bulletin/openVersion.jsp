<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>公告详情</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-title{
				font-size:24px;
				font-weight:bolder;
			}
			.kbs-subtitle{
				font-size:12px;
			}
		</style>
		
		
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
					
			$(function(){
				$('a[_kbs_onto_cate=1]').on('click', function(){
					var _id = $(this).attr('_id');
					var _text = $(this).text();
					//http://172.16.8.137:8080/kbase-core/app/category/category.htm?type=1&categoryId=72774d6098264ed49fb6041dbb827a2b&categoryName=10086%E7%94%B5%E8%AF%9D%E6%94%AF%E4%BB%98%E5%85%85%E5%80%BC%E4%B8%9A%E5%8A%A1
					var _url = '${pageContext.request.contextPath}/app/category/category.htm?&key=BULLETIN&type=1&categoryId=' + _id + '&categoryName=' + _text;
					if (parent.pageContext && parent.pageContext.addTab){
						parent.pageContext.addTab({
							id: _id,
							title: _text,
							url: _url
						});
					}else{
						//
						parent.TABOBJECT.open({
							id : 'values',
							name : _text,
							hasClose : true,
							url : _url,
							isRefresh : true
						});
					}
				});
				
				//点击查看人员名单
				$('#btnView').on('click', function(){
					if ($('#readUserPanel:hidden').length==1){
						$('#btnView').text('点击隐藏名单');
						$('#readUserPanel').show();
					}else{
						$('#btnView').text('点击查看名单');
						$('#readUserPanel').hide();
					}
				});
			});
			
		</script>
		
	</head>

	<body>
		<br/><br/>
		<table cellspacing="0" cellpadding="0" style="width:92%" align="center">
			<tr>
				<td align="center" valign="middle" style="line-height: 30px;">
					<span class="kbs-title">${bulletin.title }</span><br>
					<span class="kbs-subtitle">
						<fmt:formatDate value="${bulletin.editTime }" pattern="yyyy-MM-dd HH:mm"/>
						(
							<c:choose>
								<c:when test="${empty bulletin.bizLocation }">全部地市</c:when>
								<c:otherwise>${bulletin.bizLocation }</c:otherwise>
							</c:choose>
						/ 
							<c:choose>
								<c:when test="${empty bulletin.bizType }">全部类型</c:when>
								<c:otherwise>${bulletin.bizType }</c:otherwise>
							</c:choose>
						)
					</span>
				</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" class="box" style="width:92%" align="center">
			<tr>
				<td style="line-height: 30px;">${bulletin.content }</td>
			</tr>
			<c:if test="${not empty bulletin.ontoValueId}">
				<tr>
					<td style="line-height: 26px;">
						<c:set value="${fn:split(bulletin.ontoValueName, ',')}" var="ontoValueNames" />
						<c:set value="${fn:split(bulletin.ontoValueId, ',')}" var="ontoValueIds" />
						<c:forEach items="${ontoValueIds }" var="ontoValueId" varStatus="status">
							<a href="javascript:void(0)" _id="${ontoValueId }" _kbs_onto_cate=1>${ontoValueNames[status.index] }</a><br>
						</c:forEach>
					</td>
				</tr>
			</c:if>
			
			<%--  历史附件--%>
			<c:if test="${not empty bulletin.fileVersionSet}">
				<tr>
					<td style="line-height: 30px;">
						<c:forEach items="${bulletin.fileVersionSet }" var="attach" varStatus="status">
							${status.index+1 }.&nbsp;<a href="${swfAddress }/convert/download.do?dir=bulletin&filename=${attach.fileName }&realname=${attach.realName }">${attach.realName }</a><br>
						</c:forEach>
					</td>
				</tr>
			</c:if>
			
			<%-- add by eko.zhan at 2016-08-08 16:55 移动深圳-黄陆权提出历史公告需要显示查阅人员 --%>
			<tr>
				<td style="line-height: 26px;">
					<c:set var="readUserList" value="${fn:split(bulletin.readUser, ',')}"></c:set>
					已阅 ${fn:length(readUserList)} 人，<span id="btnView" style="cursor:pointer;color:#0000FF;">点击查看名单</span><br/>
					<span id="readUserPanel" style="display:none">${bulletin.readUser }</span>
				</td>
			</tr>
			
		</table>
		
		
	</body>
</html>