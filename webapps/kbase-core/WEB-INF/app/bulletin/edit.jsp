<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>新增公告</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
	    
	    <!-- ckeditor -->
	    <script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
	    
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-important{
			    color:red;
			    margin-left: 3px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js?20160630"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript">
			var _toTopCount = "5"; //用户最多可置顶公告数量
			var _topCount = "5"; //当前用户已置顶公告数量
			var _categoryTag = "11"
					
			$(function(){
			
				//发布选择部门
				$('#deptDesc').click(function(){
					$.kbase.picker.multiDeptForNotice({returnField: 'deptDesc|deptId'});
// 					群组功能
// 					$.kbase.picker.multiDeptGroup({returnField: 'deptDesc|deptId'});
				});
				$('#btnClearDept').click(function(){
					$('#deptDesc').val('');
					$('#deptId').val('');
				});
				
				//最小分类选择
				$('#kbValueName').click(function(){
					if ('${requestScope.enableCateTag}'=='true'){
						$.kbase.picker.ontoCate({returnField: 'kbValueName|kbValueId'});
					}else{
						$.kbase.picker.kbval({returnField: 'kbValueName|kbValueId'});
					}
				});
				
				//最小分类选择
				$('#btnClearKbValue').click(function(){
					$('#kbValueName').val('');
					$('#kbValueValue').val('');
				});
				
				//保存公告
				$('#btnSave').click(function(){
					var id = $.trim($('#id').val());
					var title = $.trim($('#title').val());
					var content = BulletinEdit.editor.getValue();
					var kbValueName = $('#kbValueName').val();
					var kbValueId = $('#kbValueId').val();
					var deptDesc = $('#deptDesc').val();
					var deptId = $('#deptId').val();
					
					var bizLocationArr = [];
					$('input[name="bizLocation"]:checked').each(function(i, item){
						bizLocationArr.push($(item).val());
					});
					var bizLocation = bizLocationArr.join(',');
					//alert(bizLocation);
					var bizTypeArr = [];
					$('input[name="bizType"]:checked').each(function(i, item){
						bizTypeArr.push($(item).val());
					});
					var bizType = bizTypeArr.join(',');
					//alert(bizType);
					
					var isMarquee = $('#isMarquee:checked').val()==undefined?0:1;
					var isPopup = $('#isPopup:checked').val()==undefined?0:1;
					//alert(isMarquee);
					//alert(isPopup);
					
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					
					var isTop = $('#isTop:checked').val()==undefined?0:1;
					
					var topStartTime = $('#topStartTime').val();
					var topEndTime = $('#topEndTime').val();
					
					
					
					//附件
					//modify by alan.zhang at 2016-03-07 10:20:45
					var attachArr = new Array();
					$('#attachmentView').find('li').each(function(index, item){
						var attachParam = {};
						
						var $name = $(item).find('div:eq(1)');
						attachParam.fileId = $name.attr('_fileid');
						attachParam.fileName = $name.attr('_filename');
						attachParam.realName = $name.text();
						
						attachArr.push(attachParam);
					});
					
					
					
					if (title==''){
						alert('标题不能为空');
						return false;
					}
					if (content==''){
						alert('内容不能为空');
						return false;
					}
					if (deptDesc==''){
						alert('发布范围不能为空');
						return false;
					}
					if ($('input[name="bizLocation"]').length>0 && bizLocation==''){
						alert('公告地市不能为空');
						return false;
					}
					if ($('input[name="bizType"]').length>0 && bizType==''){
						alert('公告分类不能为空');
						return false;
					}
					if (startTime==''){
						alert('公告周期不能为空');
						return false;
					}
					if (endTime==''){
						alert('公告周期不能为空');
						return false;
					}
					if(isTop==1){
						if (topEndTime==''){
							alert('公告置顶结束时间不能为空');
							return false;
						}
					}
					
					var params = {
						'bulletin.id': id,
						'bulletin.title': title,
						'bulletin.content': content,
						'bulletin.ontoValueName': kbValueName,
						'bulletin.ontoValueId': kbValueId,
						'bulletin.releaseScope': deptId,
						'bulletin.releaseScopeName': deptDesc,
						'bulletin.bizLocation': bizLocation,
						'bulletin.bizType': bizType,
						'bulletin.isMarquee': isMarquee,
						'bulletin.isPopup': isPopup,
						'bulletin.startTime': startTime,
						'bulletin.endTime': endTime,
						'bulletin.isTop': isTop,
						'bulletin.topEndTime': topEndTime,						
						'attachParams' : JSON.stringify(attachArr)
					}
					
					window.__kbs_layer_index = layer.load('请稍候...');
					$.post('${pageContext.request.contextPath}/app/notice/bulletin!save.htm', params, function(data){
						layer.close(window.__kbs_layer_index);
						if (data.success){
							var url = '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + data.message;
							/*
							parent.pageContext.updateCurrent({
								title: data.title,
								url: url
							});
							*/
							
							$(parent.document).find('.tabs-selected .tabs-title').text(title);
							location.href = '${pageContext.request.contextPath}/app/notice/bulletin!read.htm?id=' + data.message;
						}else{
							alert('发布失败');
						}
					}, 'json');
					
				});
				
				//上传附件
				//modify by alan.zhang at 2016-03-07 10:20:45
				$('#attachments').parent('td').on('change', '#attachments', function(){
				    if($(this).val() != ''){
						uploadFiles();
					}
				});
				
				//删除附件
				//modify by alan.zhang at 2016-03-07 10:20:45
				$('#attachmentView').on('click', '.delAttach', function(){
					var _this = this;
					/*
					layer.confirm('您确定要删除该附件吗？', function(idx){ 
						//确定
					    $(_this).parent().remove();
						//重置附件序号
						$("#attachmentView").find("li").each(function(index, item){
							var $ordernum = $(item).find('div:eq(0)');
							$(item).find('div:eq(0)').text(index+1);
						});
						layer.close(idx);
					    
					});
					*/
					if(confirm('您确定要删除该附件吗？')){
						$(_this).parent().remove();
						//重置附件序号
						$("#attachmentView").find("li").each(function(index, item){
							var $ordernum = $(item).find('div:eq(0)');
							$(item).find('div:eq(0)').text(index+1);
						});
					
					}
					
					
				});
				
				
				$("#isTop").click(function(){
				    var isCk = $(this).attr("checked");
				    if(isCk=="checked"){
				       $("#top_cycle_tr").show();
				    }else{
				       $("#top_cycle_tr").hide();
				    }
				});
				
// 				全选功能
				$('#checkAll').click(function() {
				    if ($(this).attr('checked')) {
				        $('input[name="bizLocation"]').attr('checked', 'true');
				    } else {
				        $('input[name="bizLocation"]').removeAttr('checked');
				    }
				});
				
// 				反选功能
				$('#checkInvert').click(function() {
				    if ($(this).attr('checked')) {
				        $('input[name="bizLocation"]').each(function() {
				            if ($(this).attr("checked")) {
				                $(this).removeAttr("checked");
				            } else {
				                $(this).attr('checked', 'true');
				            }
				        });
				    } else {
				        $('input[name="bizLocation"]').each(function() {
				            if ($(this).attr("checked")) {
				                $(this).removeAttr("checked");
				            } else {
				                $(this).attr('checked', 'true');
				            }
				        });
				    }
				});
				
			});
			
			//上传附件（不转换格式）
			//modify by alan.zhang at 2016-03-07 10:20:45
			function uploadFiles() {
				var upload_idx = layer.load('正在上传，请稍后……');
			
				$.ajaxFileUpload({
					//url: '${pageContext.request.contextPath}/app/notice/bulletin!uploadFiles.htm', 
					url: '${pageContext.request.contextPath}/app/util/file-upload!upload.htm', 
					type: 'post',
					data: { dir: 'bulletin'}, 
					secureuri: false, //一般设置为false
					fileElementId: 'attachments', // 上传文件的id、name属性名
					dataType: 'json', //返回值类型，一般设置为json、application/json
					success: function(data, status){  
						if (data.rst == "1") {
							
							var attachHtml = '';
							attachHtml += '<li style="list-style:none;line-height:30px;">';
							attachHtml += '<div style="display: inline; margin-right: 10px;">' + ($("#attachmentView").find("li").length+1) + '.</div>&nbsp;';
							attachHtml += '<div style="display: inline; margin-right: 10px;" _fileid="" _filename="' + data.attachFileName + '">' + data.attachRealName + '</div>';
							attachHtml += '<div style="display: inline; margin-right: 10px;" class="delAttach"><a href="javascript:void(0)" style="text-decoration:none;">删除</a></div>';
							attachHtml += '</li>';
							
							$("#attachmentView").append(attachHtml);
						
						} else {
							alert(data.msg);
						}
					},
					error: function(data, status, e){ 
						alert(e);
					},
					complete: function () {
						layer.close(upload_idx);
        			}
				});
			}
			
			
			
			
			
		</script>
		
	</head>

	<body>
		<input type="hidden" id="id" value="${bulletin.id }" />
		<input type="hidden" id="kbValueId" value="${bulletin.ontoValueId }" />
		<input type="hidden" id="deptId" value="${bulletin.releaseScope }" />
		
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">标题<span class="kbs-important">*</span></td>
				<td width="75%" style="border-right-style:hidden">
					<textarea id="title" name="bulletin.title" rows="2" style="width:100%;border:1px solid #cdcdcd;">${bulletin.title }</textarea>
				</td>
				<td width="10%" style="border-left-style:hidden" valign="bottom"><span id="title_lenght_show"></span></td>
			</tr>
			<tr>
				<td>内容<span class="kbs-important">*</span></td>
				<td valign="bottom" style="border-right-style:hidden">
				<textarea id="content" name="bulletin.content" style="width:100%;height:150px;visibility:hidden;">${bulletin.content }</textarea>
				</td>
			    <td valign="bottom" style="border-left-style:hidden"><span id="content_lenght_show" style="float:left;"></span></td>
			</tr>			
			<tr>
				<td>加载知识</td>
				<td colspan="2">
					<input id="kbValueName" type="text" style="width:550px;" value="${bulletin.ontoValueName }"/>
					<input type="button" value="清空" id="btnClearKbValue">
				</td>
			</tr>
			<tr>
				<td>发布范围<span class="kbs-important">*</span></td>
				<td colspan="2">
					<input id="deptDesc" type="text" style="width:550px;" value="${bulletin.releaseScopeName }">
					<input type="button" value="清空" id="btnClearDept">
				</td>
			</tr>
			<c:if test="${fn:length(requestScope.bizLocationList)>0}">
				<tr>
					<td>公告地市<span class="kbs-important">*</span>
					<span>
							<input id="checkAll" name="" type="checkbox" value="" />全选
							<input id="checkInvert" name="" type="checkbox" value="" />反选
							</span>
					</td>
					<td colspan="2">
						<c:set var="_bizLocation" value=",${bulletin.bizLocation},"></c:set>
						<c:forEach items="${requestScope.bizLocationList}" var="item">
					     	<c:set var="_item" value=",${item.name },"></c:set>
							<input type="checkbox" name="bizLocation" value="${item.name }" <c:if test="${fn:indexOf(_bizLocation, _item) != -1}"> checked </c:if>/>${item.name }
						</c:forEach>
					</td>
				</tr>
			</c:if>
			<c:if test="${fn:length(requestScope.bizTypeArr)>0}">
				<tr>
					<td>公告分类<span class="kbs-important">*</span></td>
					<td colspan="2">
						<c:set var="_bizType" value=",${bulletin.bizType},"></c:set>
					    <c:forEach items="${requestScope.bizTypeArr}" var="item">
					     	<c:set var="_item" value=",${item },"></c:set>
					    	<input type="checkbox" name="bizType" value="${item }" <c:if test="${fn:indexOf(_bizType, _item) != -1}"> checked </c:if>/>${item }
					    </c:forEach>
					</td>
				</tr>
			</c:if>
			<tr>
				<td>显示类型</td>
				<td colspan="2">
					<input type="checkbox" id="isMarquee" value="1" <c:if test="${bulletin.isMarquee == 1}"> checked </c:if>/>跑马灯
					<input type="checkbox" id="isPopup" value="1" />弹屏
				</td>
			</tr>
			<tr>
				<td>公告周期<span class="kbs-important">*</span></td>
				<td colspan="2">
					<input type="text" id="startTime" name="bulletin.startTime" readonly="readonly" 
						value="<c:if test="${bulletin.startTime != null}">${bulletin.startTime }</c:if><c:if test="${bulletin.startTime == null}">${startTime }</c:if>" 
						style="width:140px;" onFocus="WdatePicker({startDate:'%y-%M-%d 00:00:00',maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})" /> ～ 
					<input type="text" id="endTime" name="bulletin.endTime" readonly="readonly" 
						value="<c:if test="${bulletin.endTime != null}">${bulletin.endTime }</c:if><c:if test="${bulletin.endTime == null}">${endTime }</c:if>" 
						style="width:140px;" onclick="WdatePicker({startDate:'%y-%M-%d 23:59:59',minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})"/>
				</td>
			</tr>
			<tr>
				<td>是否置顶</td>
				<td colspan="2"><input type="checkbox" id="isTop" value="1" <c:if test="${requestScope.topCount>=5 }">disabled="disabled"</c:if> <c:if test="${bulletin.isTop == 1}"> checked </c:if>/>置顶</td>
			</tr>
			<tr id="top_cycle_tr" style="display:<c:if test="${bulletin.isTop != 1}">none</c:if>">
				<td>置顶结束时间</td>
				<td colspan="2">
					<input type="text" id="topEndTime"  readonly="readonly" value="<c:if test="${bulletin.topEndTime != null}">${bulletin.topEndTime}</c:if><c:if test="${bulletin.topEndTime == null}">${topEndTime}</c:if>" 
						style="width:140px;" onclick="WdatePicker({startDate:'%y-%M-%d',minDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd',alwaysUseStartDate:true})"/>
				</td>
			</tr>
			<%-- --%>
			<tr>
				<td>附件</td>
				<td colspan="2">
					<input type="file" id="attachments" name="attachments" style="width: 222px;"/>
					<ul id="attachmentView" style="margin-top: 10px;">
				     	
				     	<c:if test="${not empty bulletin.fileSet}">
							<c:forEach items="${bulletin.fileSet }" var="attach" varStatus="status">
								<li style="list-style:none;line-height:30px;">
						     		<div style="display: inline; margin-right: 10px;">${status.index+1 }.</div>
						     		<div style="display: inline; margin-right: 10px;" _fileid="" _filename="${attach.fileName }">${attach.realName }</div>
						     		<div style="display: inline; margin-right: 10px;" class="delAttach">
						     			<a href="javascript:void(0)" style="text-decoration:none;">删除</a>
						     		</div>
						     	</li>
							</c:forEach>
						</c:if>
				     	
					</ul>
				</td>
			</tr>
			 
			<tr>
				<td align="right" colspan="3">
					<input type="button" value="发送" id="btnSave">
				</td>
			</tr>
		</table>
		
		<%-- 图片插入 --%>
		<jsp:include page="../util/ckEdit-myImage.jsp"></jsp:include>
		<script type="text/javascript">
			$(function(){
				var that = window.BulletinEdit = {}, fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				/*公共方法*/
				fn.extend({
					getBrowserVersion: function(){
						var userAgent = navigator.userAgent; /*取得浏览器的userAgent字符串*/
					    var isOpera = userAgent.indexOf('Opera') > -1; /*判断是否Opera浏览器*/
					    var isIE = userAgent.indexOf('compatible') > -1 && 
					    	userAgent.indexOf('MSIE') > -1 && !isOpera; /*判断是否IE浏览器*/
					    var isFF = userAgent.indexOf('Firefox') > -1; /*判断是否Firefox浏览器*/
					    var isSafari = userAgent.indexOf('Safari') > -1; /*判断是否Safari浏览器*/
					    if (isIE) {
					        var IE5 = IE55 = IE6 = IE7 = IE8 = false;
					        var reIE = new RegExp('MSIE (\\d+\\.\\d+);');
					        reIE.test(userAgent);
					        var fIEVersion = parseFloat(RegExp['$1']);
					        IE55 = fIEVersion == 5.5;
					        IE6 = fIEVersion == 6.0;
					        IE7 = fIEVersion == 7.0;
					        IE8 = fIEVersion == 8.0;
					        if (IE55) return 'IE55';
					        if (IE6) return 'IE6';
					        if (IE7) return 'IE7';
					        if (IE8) return 'IE8';
					    }
					    if(isFF) return 'FF';
					    if(isOpera) return 'Opera';
					},
					isIE7: function(){/*是否是ie7*/
						return this.getBrowserVersion() == 'IE7';
					},
					kindEditor: function (){
						/*
						 * add by eko.zhan at 2016-01-06 19:35 复写KindEditor的预览事件
						 */
						KindEditor.plugin('preview', function(K) {
							var self = this, name = 'preview', undefined;
							self.clickToolbar(name, function() {
								var lang = self.lang(name + '.'),
									html = '<div style="padding:10px 20px;">' +
										'<iframe class="ke-textarea" frameborder="0" style="width:630px;height:380px;"></iframe>' +
										'</div>',
									dialog = self.createDialog({
										name : name,
										width : 700,
										title : self.lang(name),
										body : html
									}),
									iframe = K('iframe', dialog.div),
									doc = K.iframeDoc(iframe);
								doc.open();
								doc.write(self.fullHtml());
								doc.close();
								K(doc.body).css('background-color', '#FFF');
								iframe[0].contentWindow.focus();
							});
						});
						
						/////////////////////////////////////////////////
						///////////////公告内容富文本插件////////////////////
						KindEditor.ready(function(K) {
							that.editor.editor = K.create('textarea[id="content"]', {
					            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
					            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist', 'table', '|', 'preview'] 
							});
						});
						/////////////////////////////////////////////////
					},
					/*获取editor对象*/
					ckEditor: function (){
						return CkEditImage.create.replace('bulletin.content',{
							height: 150
						});
					}
				});
				/*功能方法*/
				that.extend({
					editor: {
						editor: null,
						getValue: function(){},
						init: function (){}
					},
					init: function(){
						if(fn.isIE7()) {/*ie7使用kindEditor*/
							that.editor = {
								editor: null,
								getValue: function(){
									return $.trim(this.editor.html());
								},
								init:function(){/*kindEditor异步初始化*/
									fn.kindEditor();
								}
							}
						} else {/*其余使用ckEditor*/
							that.editor = {
								editor: null,
								getValue: function(){
									return $.trim(this.editor.getData());
								},
								init:function(){
									this.editor = fn.ckEditor();;
								}
							}
						}
						/*编辑器初始化*/
						that.editor.init();
					}
				});
				BulletinEdit.init();
			});
		</script>
	</body>
</html>