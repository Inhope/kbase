<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/task/task-menu.js"></script>
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		<script type="text/javascript">
			function getselectinfo(){
				var selectvalues=[];
				$("input:checkbox[name='article_id']:checked").each(function(){
					selectvalues.push($(this).val());
				})
				return selectvalues;
			}
			
			function tomodifyinfo(){
				var selectvalue = getselectinfo();
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}if(selectvalue.length>1){
					parent.layer.alert("不能选择多条数据!", -1);
					return false;
				}
				parent._index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改文章信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/article!toupdateinfo.htm?id="+selectvalue},
					area: ['90%', '100%']
				});
			}
			
			
			function delarticleinfo(){
				var selectvalue = getselectinfo();
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}
				layer.confirm("确定要删除选中的数据吗？", function(index){
					$.ajax({
					    url:$.fn.getRootPath()+'/app/micro/article!delarticleinfo.htm',
					    data:{"ids":selectvalue.join(',')},
						type:"post",
						dataType:"json",
						success:function(data){
						   parent.$(document).hint("删除成功");
					       layer.close(index);
					       window.location.href=$.fn.getRootPath()+'/app/micro/article.htm?id=${article.classification.id}';
					    }
			  　　　　 });
				})
			}
			
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/micro/article.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
			}
			
			function imageinfo(id,url){
				$("#image"+id+"").show();
				$("#imgdetailinfo"+id+"").attr("src",$.fn.getRootPath()+url);
			}
			
			function moveinfo(id){
				$("#"+id+"").hide();
			}
			
			function toimport(id){
				$.ajax({
					url:$.fn.getRootPath()+"/app/micro/article!processdetaildata.htm?",
					type:"post",
					data:{"id":id},
					timeout:5000,
					async:false,
					dataType:"json",
					success:function(data){
			            if(data.success) {
							var iframe = document.createElement("iframe");
				            iframe.src = $.fn.getRootPath()+"/app/micro/article!downLoad.htm?fileName=" + data.message;
				            iframe.style.display = "none";
				            document.body.appendChild(iframe);
						}
			        }
			  }); 
			}
			
			
			function toaddarticle(){
				parent._index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改文章信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/article!toaddinfo.htm?id=${article.classification.id }"},
					area: ['90%', '90%']
				});
			}
		</script>
	</head>
		<body>
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="min-width:860px;">
							<div class="gonggao_titile">
								<div class="gonggao_titile_right">
									<a href="javascript:void(0);" onclick="delarticleinfo()" id="">删除</a>
									<a href="javascript:void(0);" onclick="tomodifyinfo()" id="">修改</a>
									<a href="javascript:void(0);" onclick="toaddarticle()" id="" >添加</a>
								</div>
							</div>
							
							<div class="yonghu_titile">
								<ul>
									<input type="hidden" value="${article.classification.id }" name="article.classification.id" />
									<li>
										类别名称:
										<input type="text" id="title_select" value="${article.title}" name="article.title" />
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" />
										</a>
									</li>
								</ul>
							</div>
							
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="2%">&nbsp;</td>
											<td width="10%">
												文章标题
											</td>
											<td width="14%">
												微页面地址
											</td>
											
											<td width="10%">
												评论开启
											</td>
											
											<td width="10%">
												文章概要
											</td>
											<td width="10%">
												文章序列
											</td>
											<td width="10%">
												图片
											</td>
											<td width="10%">
												创建时间
											</td>
											<td width="10%">
												所属分类
											</td>
											<td width="10%">
												操作
											</td>
										
										</tr>
										<s:iterator value="#request.page.result" var="va" status="ind">
											<tr>
												<td>
													<input  name="article_id" type='checkbox' value="${va.id }" />
												</td>
												<td>
													${va.title }
												</td>
												<td>
													${pageContext.request.contextPath }/app/micro/article!articleinfo.htm?id=${va.id }
												</td>
												<td>
													<s:if test="#va.isenable==1">启用</s:if><s:if test="#va.isenable==0">停用</s:if>
												</td>
												<td>
													${va.content }
												</td>
												<td>
													${va.orderno }
												</td>
												<td>
													<a href="#" onmouseover="imageinfo('${ind.index+1 }','${va.imageurl}')" onmouseout="moveinfo('image${ind.index+1 }');">查看信息</a>
													<div id="image${ind.index+1}" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: visible; width:170px;margin-left:10px">
														 <img id="imgdetailinfo${ind.index+1}" />
												    </div>
												</td>
												<td>
													<s:date name="#va.createDate" format="yyyy-MM-dd" />
												</td>
												<td>
													${va.classification.title }
												</td>
												<td>
													<a href="#" onclick="toimport('${va.id }')">导出评论</a>
												</td>
												
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
	</body>
</html>

