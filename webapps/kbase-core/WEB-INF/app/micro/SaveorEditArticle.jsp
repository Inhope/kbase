<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<!-- kindeditor -->
		<script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor.js"></script>
		<script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
		<script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>
		
		<script type="text/javascript">
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[id="content"]', {
					cssPath : 'kindeditor/plugins/code/prettify.css',
					uploadJson : $.fn.getRootPath() + '/app/micro/articlefilemanager!toupload.htm',
					fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',
					allowFileManager : true,
					width : "80%", 
	   				height : "600px",
				});
			});
			
			function imginfo(){
				var imgName =  $("#imgload").val();
				var  path1 = imgName.lastIndexOf("\\");
			    var  name = imgName.substring(path1+1);
			    var  type=name.substring(name.lastIndexOf("."));
				if (name==""){
				    parent.$(document).hint("请选择文件");
					return false;
				}else{
					if(name.toLocaleLowerCase().match("^.+\\.(jpg|png|bmp|ico|jpeg|gif)$")==null) {
						parent.$(document).hint("目前只支持jpg|png|bmp|ico|jpeg|gif等格式的图片");
						return false;
					}
				}
			    var _url = $.fn.getRootPath() + '/app/micro/article!toupload.htm';
				$.ajaxFileUpload({
					url : _url,
					secureuri : false,
					fileElementId : "imgload",
					data : {"imgFieldName": "imgload", "imgName": name},
					dataType : 'json',
					timeout : 5000,
					success : function(data, status) {
						$("#imageurl").val(data);
						if($("#imageurl").val()==''){
							parent.layer.alert("上传图片失败",-1);
						}
					}
				});
			 
			}
			
			
			
			function cancel(){
				parent.layer.close(parent.window._index);
			}
			function confirm(){
				$("#content").val(editor.html());
				var message = '保存成功';
				if('${article.id}'!=''){
					message = '更新成功'
				}
				if($("#imageurl").val()==''){
					parent.layer.alert("请上传图片或上传图片失败请重新上传",-1);
					return false;
				}
				var params=$("#form1").serialize();
				$.ajax({
					url : $.fn.getRootPath()+ "/app/micro/article!savearticle.htm?"+params,
					data:{"article_id":'${article.id}'},
					type : "post",
					dataType : "json",
					success : function(data) {
						parent.$(document).hint(message);
						$(parent.document).find('div.content_content').find('iframe').each(function(){
								if (!$(this).is(":hidden")){
									$(this).attr("src", $.fn.getRootPath()+"/app/micro/article.htm?id=${article.classification.id}"); 
									return false;
								}
							});
						parent.layer.close(parent._index);
					}
				});
			}
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<table width="100%" cellspacing="0" cellpadding="0" class="box">
				<tr>
					<td>文章标题:</td>
					<input type="hidden" name="article.classification.id" value="${article.classification.id }" />
					<td><input type="text" 	name="article.title" value="${article.title}" style="width:80%"/></td>
				</tr>
				<tr>
					<td>文章序列:</td>
					<td><input type="text" 	name="article.orderno" value="${article.orderno}" style="width:80%"/></td>
				</tr>
				<tr>
					<td>是否启用:</td>
					<td>
						<select	name="article.isenable" style="width:80%">
							<option value="1" <s:if test="#request.article.isenable==1">selected="selected"</s:if>>启用</option>
							<option value="0" <s:if test="#request.article.isenable==0">selected="selected"</s:if>>停用</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>文章概要:</td>
					<td><textarea name="article.content" style="width:80%">${article.content}</textarea></td>
				</tr>
				<tr>
					<td>上传图片:</td>
					<td><input type="file"  id="imgload" name="imgload"  onchange="imginfo()" style="width:70%"/></td>
					<input type="hidden" name="article.imageurl" id="imageurl" value="${article.imageurl }" />
				</tr>
				 
				<tr>
					<td>文章内容:</td>
					<td><textarea name="article.outline" id="content">${article.outline}</textarea></td>
				</tr>
				
				<tr>
					<td	colspan="2" align="right">
						<input type="button" onclick="confirm();" value="确定"/>&nbsp;<input type="button" onclick="cancel();" value="取消"/>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

