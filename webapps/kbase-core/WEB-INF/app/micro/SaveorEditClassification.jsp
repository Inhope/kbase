<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>微页面管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			function imginfo(){
				var imgName =  $("#imgload").val();
				var  path1 = imgName.lastIndexOf("\\");
			    var  name = imgName.substring(path1+1);
			    var  type=name.substring(name.lastIndexOf("."));
				if (name==""){
				    parent.$(document).hint("请选择文件");
					return false;
				}else{
					if(name.toLocaleLowerCase().match("^.+\\.(jpg|png|bmp|ico|jpeg|gif)$")==null) {
						parent.$(document).hint("目前只支持jpg|png|bmp|ico|jpeg|gif等格式的图片");
						return false;
					}
				}
			    var _url = $.fn.getRootPath() + '/app/micro/classification!toupload.htm';
				$.ajaxFileUpload({
					url : _url,
					secureuri : false,
					fileElementId : "imgload",
					data : {"imgFieldName": "imgload", "imgName": name},
					dataType : 'json',
					timeout : 5000,
					success : function(data, status) {
						$("#imageurl").val(data);
						if($("#imageurl").val()==''){
							parent.layer.alert("上传图片失败",-1);
						}
					}
				});
			 
			}
			
			
			
			function cancel(){
				parent.layer.close(parent.window._index);
			}
			function confirm(){
				var params=$("#form1").serialize();
				var message = "保存成功";
				if('${classification.id }'!=''){
					message = "更新成功";
				}
				if($("#imageurl").val()==''){
					parent.layer.alert("请上传图片或上传图片失败请重新上传",-1);
					return false;
				}
				$.ajax({
					url : $.fn.getRootPath()+ "/app/micro/classification!saveClassification.htm?"+params,
					data:{"class_id":'${classification.id }'},
					type : "post",
					dataType : "json",
					success : function(data) {
						parent.$(document).hint(message);
						parent.window.location.href=$.fn.getRootPath()+"/app/micro/classification.htm?id=${classification.activity.id }";
						parent.layer.close(parent.window._index);
					}
				});
			}
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<table width="100%" cellspacing="0" cellpadding="0" class="box">
				<tr>
					<td>活动名称:</td>
					<td><input type="text" value="${classification.activity.title }" readonly="readonly" style="width:70%;color: grey"/></td>
					<input type="hidden" name="classification.activity.id" value="${classification.activity.id }"/>
				</tr>
				<tr>
					<td>类别名称:</td>
					<td><input type="text"	name="classification.title" value="${classification.title}" style="width:70%"/></td>
				</tr>
				<tr>
					<td>显示序号:</td>
					<td><input type="text"	name="classification.orderno" value="${classification.orderno}" style="width:70%"/></td>
				</tr>
				<tr>
					<td>上传图片:</td>
					<td><input type="file"  id="imgload" name="imgload"  onchange="imginfo()" style="width:70%"/></td>
					<input type="hidden" name="classification.imageurl" id="imageurl" value="${classification.imageurl}" />
				</tr>
				
				<tr>
					<td	colspan="2" align="right">
						<input type="button" onclick="confirm();" value="确定"/>&nbsp;<input type="button" onclick="cancel();" value="取消"/>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

