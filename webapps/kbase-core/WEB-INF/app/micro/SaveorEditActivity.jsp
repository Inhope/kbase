<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			function cancel(){
				parent.layer.close(parent.window._index);
			}
			function confirm(){
				var params=$("#form1").serialize();
				var message="保存成功";
				var url= $.fn.getRootPath()+ "/app/micro/activity!saveactivity.htm?"+params
				if('${activity.id}'!=''){
					message = "更新成功";
					url= $.fn.getRootPath()+ "/app/micro/activity!updateactivity.htm?"+params+"&activity.id=${activity.id}"
				}
				$.ajax({
					url : url,
					type : "post",
					dataType : "json",
					success : function(data) {
						parent.$(document).hint(message);
					    parent.window.location.href=$.fn.getRootPath()+"/app/micro/activity.htm";
						parent.layer.close(parent.window._index);
					}
				});
			}
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<table width="100%" cellspacing="0" cellpadding="0" class="box">
				<tr>
					<td>活动名称:</td>
					<td><input type="text"	name="activity.title" value="${activity.title }" style="width:70%"/></td>
				</tr>
				<tr>
					<td>是否启用:</td>
					<td>
						<select	name="activity.isenable" style="width:70%">
							<option value="1" <s:if test="#request.activity.isenable==1">selected="selected"</s:if>>启用</option>
							<option value="0" <s:if test="#request.activity.isenable==0">selected="selected"</s:if>>停用</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>当前模板:</td>
					<td>
						<select	name="activity.module" style="width:70%">
							<s:iterator	value="#request.modual" var="va">
								<option value="${va.key }" <s:if test="#request.activity.module==#va.key">selected="selected"</s:if>>${va.label }</option>
							</s:iterator>	
						</select>
					</td>
				</tr>
				<tr>
					<td	colspan="2" align="right">
						<input type="button" onclick="confirm();" value="确定"/>&nbsp;<input type="button" onclick="cancel();" value="取消"/>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

