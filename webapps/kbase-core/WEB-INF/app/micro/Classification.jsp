<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>123</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/task/task-menu.js"></script>
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		<script type="text/javascript">
			
			function tomodifyinfo(){
				var selectvalue=[];
				var flag=false;
				$("input:checkbox[name='classfication_id']:checked").each(function(){
					if($(this).attr("isopera")=='1'){
						flag = true;
					}else{
						selectvalue.push($(this).val());
					}
					return false;
					
				})
				if(flag){
					parent.layer.alert("不能修改默认分类", -1);
					return false;
				}
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}if(selectvalue.length>1){
					parent.layer.alert("不能选择多条数据!", -1);
					return false;
				}
				window._index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改分类信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/classification!toupdateinfo.htm?id="+selectvalue},
					area: ['50%', '42%']
				});
			}
			
			function delclassfication(){
				var selectvalue=[];
				var flag=false;
				$("input:checkbox[name='classfication_id']:checked").each(function(){
					if($(this).attr("isopera")=='1'){
						flag = true;
						return false;
					}else{
						selectvalue.push($(this).val());
					}
				})
				if(flag){
					parent.layer.alert("不能删除默认分类", -1);
					return false;
				}
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}
				layer.confirm("确定要删除选中的数据吗？", function(index){
					$.ajax({
					    url:$.fn.getRootPath()+'/app/micro/classification!delClassification.htm',
					    data:{"ids":selectvalue.join(',')},
						type:"post",
						dataType:"json",
						success:function(data){
						   parent.$(document).hint("删除成功");
					       layer.close(index);
					       window.location.href=$.fn.getRootPath()+'/app/micro/classification.htm?classification.activity.id=${activity.id}';
					    }
			  　　　　 });
				})
			}
			
			
			function imageinfo(id,url){
				$("#image"+id+"").show();
				$("#imgdetailinfo"+id+"").attr("src",$.fn.getRootPath()+url);
			}
			
			function moveinfo(id){
				$("#"+id+"").hide();
			}
			
			function toaddclassfication(){
				window._index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改分类信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/classification!toaddinfo.htm?id=${classification.activity.id}"},
					area: ['30%', '42%']
				});
			}
			
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/micro/classification.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
			}
			
			function showdetail(name,id){
			   parent.parent.TABOBJECT.open({
					id : 'article',
					name : '文章列表-'+name,
					hasClose : true,
					url : $.fn.getRootPath() + '/app/micro/article.htm?id=' + id,
					isRefresh : true
				}, this);
			}
		</script>
	</head>
		<body>
					<form id="form0" action="" method="post">
						
						<div class="content_right_bottom" style="min-width:860px;">
							<div class="gonggao_titile">
								<div class="gonggao_titile_right">
									<a href="javascript:void(0);" onclick="delclassfication()" id="">删除</a>
									<a href="javascript:void(0);" onclick="tomodifyinfo()" id="">修改</a>
									<a href="javascript:void(0);" onclick="toaddclassfication()" id="" >添加</a>
								</div>
							</div>
							
							<div class="yonghu_titile">
								<ul>
									<input type="hidden" value="${classification.activity.id}" name="classification.activity.id" />
									<li>
										类别名称:
										<input type="text" id="title_select" value="${classification.title}" name="classification.title" />
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" />
										</a>
									</li>
								</ul>
							</div>
						 	
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="2%">&nbsp;</td>
											<td width="35%">
												类别名称
											</td>
											<td width="18%">
												显示序号
											</td>
											
											<td width="15%">
												活动名称
											</td>
											<td width="10%">
												图片
											</td>
											
											<td width="10%">
												查看文章
											</td>
										
										</tr>
										<s:iterator value="#request.page.result" var="va" status="ind">
											<tr>
												<td><input type="checkbox" isopera="${va.operateflag }" name="classfication_id" value="${va.id }"	/></td>
												<td>${va.title }</td>
												<td>${va.orderno}</td>
												<td>${va.activity.title}</td>
												<td>
													<s:if test="#va.operateflag==1">无</s:if>
													<s:else>
													<a href="#" onmouseover="imageinfo('${ind.index+1 }','${va.imageurl}')" onmouseout="moveinfo('image${ind.index+1 }');">查看信息</a>
													<div id="image${ind.index+1}" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: visible; width:170px;margin-left:10px">
														 <img id="imgdetailinfo${ind.index+1}" />
												    </div>
												    </s:else>
												</td>
												<td><a href="#" onclick="showdetail('${va.title }','${va.id }')">文章类别</a></td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
	</body>
</html>

