<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <base href="<%=basePath%>">
   	<title id = "title">
   		${classification.title}
   	</title>
   	
   	<meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
   	
   	<link href="${pageContext.request.contextPath}/resource/micro/article/css/news.css" rel="stylesheet" type="text/css" />
   	<script src="${pageContext.request.contextPath}/resource/micro/article/js/iscroll.js" type="text/javascript"></script>
   	<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/klass.min.js"></script>
   	<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/jquery-1.9.1.min.js"></script>
   	<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/main.js"></script>
   	<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/code.photoswipe.jquery-3.0.4.min.js"></script>
	
	<script type="text/javascript">
		window.onload = function () {
	        var oWin = document.getElementById("win");
	        var oLay = document.getElementById("overlay");
	        var oBtn = document.getElementById("popmenu");
	        var oClose = document.getElementById("close");
	        oBtn.onclick = function () {
	            oLay.style.display = "block";
	            oWin.style.display = "block"
	        };
	        oLay.onclick = function () {
	            oLay.style.display = "none";
	            oWin.style.display = "none"
	        };
	    };
	</script>
	<script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38506186-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
  </head>
  
  <body id="listhome1">
  	<div id="ui-header">
        <div class="fixed">
            <a class="ui-title" id="popmenu">选择分类</a>
            <!-- <a class="ui-btn-left_pre" id="goBack" href="javascript:history.go(-1);">
            </a> --> 
            <a class="ui-btn-right" href="${pageContext.request.contextPath}/app/micro/classification!micropage.htm?id=${classification.activity.id}"></a>
        </div>
    </div>
    <div id="overlay">
    </div>
    <div id="win">
        <ul class="dropdown" id="category">
        	<c:forEach items="${clalist}" var="category" varStatus="status">
	    	<li><a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${category.id}'><span>${category.title}</span></a></li>
	    	</c:forEach>
	    	<div class='clr'></div>
        </ul>
    </div>
    <div class="Listpage">
        <div class="top46">
        </div>
        <div id="todayList">
            <ul class="todayList" id="content">
            	<c:forEach items="${articlelist}" var="article" varStatus="status">
            		<li><a href='${pageContext.request.contextPath}/app/micro/article!articleinfo.htm?id=${article.id}'> <div class='img'>
					<img src='${pageContext.request.contextPath}${article.imageurl}'></div>
					<h2> ${article.title}</h2> <p class='onlyheight'> ${article.content}</p> 
					<div class='commentNum'> </div> </a></li>
				</c:forEach>
            </ul>
        </div>
        <section id="Page_wrapper">
<div id="pNavDemo" class="c-pnav-con">
<section class="c-p-sec">
<c:choose>
	<c:when test="${currentPage<=1}">
		<div class="c-p-pre  c-p-grey  "  >
		<span class="c-p-p"><em></em></span><a >上一页</a></div>
	</c:when>
	<c:otherwise><div class="c-p-pre"  ><span class="c-p-p"><em></em></span><a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${classification.id}&currentPage=${currentPage-1}'>上一页</a></div></c:otherwise>
</c:choose>


<div class="c-p-cur">
<div class="c-p-arrow c-p-down"><span>${currentPage}/${totalPage}</span><span></span></div>
                <select class="c-p-select" onchange="location.href = '${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${classification.id}&currentPage='+this.value">

<c:forEach begin="1" end = "${totalPage}" varStatus="status">
	<c:choose>
		<c:when test="${status.index==currentPage}"><option   selected="selected"  value=${status.index} >第${status.index}页</option></c:when>
		<c:otherwise>
		<option value=${status.index} >
		第${status.index}页
		</option>
		</c:otherwise>
	</c:choose>
</c:forEach>
</select>
</div>
<c:choose>
	<c:when test="${currentPage>=totalPage}">
		<div class="c-p-next  c-p-grey  "  >
		<a >下一页</a><span class="c-p-p"><em></em></span>
	</c:when>
	<c:otherwise><div class="c-p-next" ><a href = '${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${classification.id}&currentPage=${currentPage+1}' >下一页</a><span class="c-p-p"><em></em></span></c:otherwise>
</c:choose>
</div>
</section>
</div>
</section>
    </div>
    
    <div style="display: none">
    </div>
  </body>
</html>
