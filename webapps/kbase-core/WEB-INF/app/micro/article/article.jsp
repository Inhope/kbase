<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
 <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>${article.title}</title>
<meta http-equiv=Content-Type content="text/html;charset=utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no"> 
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/micro/article/css/client-page17e52c.css"/>  
<style type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resource/micro/article/css/jquery-te-1.4.0.css"></style>
<style type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resource/micro/article/css/demo.css"></style>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/jquery.min.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/jquery-te-1.4.0.min.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/article/js/emotions.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
    <style> 
     #nickname{overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width: 90%;  }  ol,
       ul{list-style-position:inside;}
	    .jqte_editor, .jqte_source {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid #CCCCCC;
		    max-height: 900px;
		    min-height: 100px;
		    outline: medium none;
		    overflow: auto;
		    padding: 10px;
		    resize: vertical;
		    word-wrap: break-word;
		}

    </style>    
    <style>  #activity-detail .page-content .text{font-size:16px;}</style>
    </head> 
    <body id="activity-detail">   
	    <div class="page-bizinfo">
	    	<div class="header">
	    		<h1 id="activity-name">${article.title}</h1>
	    	</div>
	    </div>
	    <div class="page-content">
	    	<div class="text">
	    		 ${article.outline}
	    	</div>
	    	<!--
	    	<div style="background-color: #BF0C07;border-top-left-radius: 5px;border-top-right-radius: 5px;bottom: 0;color: #FFFFFF; padding: 10px;  position: fixed; right: 20px; text-align: center;">
	    		<div>
	    		<img id="pinglun" src="${pageContext.request.contextPath}/resource/micro/article/image/pinglun.png"/>
	    		</div>
	    		<div>
	    		<img id="zan" src="${pageContext.request.contextPath}/resource/micro/article/image/zan.png"/>
	    		<div style="font-size: 14px;font-weight: bold;padding: 6px 0;"><s:property value="#request.article.comments.size"/></div>
	    		</div>
	    	</div>
	    	-->
	   </div>
	   <s:if test="#request.article.isenable==1">
	   <!-- 文章评论列表 -->
	   <div id="comment-list">
	   		<!-- 文章评论总数 -->
	   		<div class="comment-total" style="background-color:#4F4F4F;">
	   			<s:property value="#request.commlist.size"/>
	   		</div>
	   		<!-- 迭代文章评论信息 -->
	   		<s:iterator value="#request.commlist" var="va" status="i">
	   			<div class="comment-body">
	   			<p style="font-size: 14px;padding: 0 0 15px 15px;">
	   				<span >
	   				<s:if test="#va.nickname==null || #va.nickname==''">匿名用户</s:if>
	   				<s:else>
	   					<s:property value="#va.nickname" />
	   				</s:else>
	   				</span>
	   				<span style="background-color: #CCCCCC;border-bottom-left-radius: 5px;border-top-left-radius: 5px;color: #FFFFFF;float: right;font-size: 14px;padding: 2px 10px;"><s:property value="#i.index+1"/>楼</span>
	   			</p>
	   			<p style="font-size: 15px; padding: 0 15px 10px;">${va.comment }</p>
	   			<p style="color: #555555;font-size: 14px;line-height: 26px;padding: 0 15px 10px 15px;"><s:date name="#va.createDate" format="yyyy-MM-dd HH:mm:ss"/></p>
	   			<p style="border-top: 1px solid #CCCCCC;"></p>
	   			</div>
	   		</s:iterator>
	   		<!-- 发表文章评论 -->
	   		<div class="comment-form" id="topinglun">
	   			<p>评论</p>
	   			昵称：<input id="nickName" type="text" maxlength="10" style="border: 1px solid #CCCCCC;color: #333333; font-size: 12px; height: 30px; padding: 0 6px;"/>
	   			<div id="emotionContent" style="padding: 15px 0;"></div>
   				<textarea rows="5" cols="30" id="content" name="textarea" class="jqte-test" data-origin="textarea"></textarea> 
   				<br>
   				<input type="button" value="发表评论" onclick="saveComment()" style="background-color: #D9452B; border: 0 none; border-radius: 5px; color: #FFFFFF;cursor: pointer; font-size: 14px;padding: 4px 8px;">
	   		</div>
	   </div>
	  
	   
	   <script type="text/javascript">
		$(document).ready(function(){
			initEmotions();
			$('textarea.jqte-test').jqte();
			$("div.jqte_toolbar").css("display","none");
			$("div.jqte_hiddenField").css("display","none");
			
			$('#zan').click(function(){
				var count = $(this).next();
				$.ajax({
	   				type: "POST",
			   		url: "/micromsg/micro/zan!click.action",
			   		data:{articleId:'${article.id}',openId:'${openId}'},
			   		async: true,
			   		dataType : 'json',
			   		success: function(msg){
			   			alert(msg);
			   			if(msg=='点赞成功'){
			   				count.html(parseInt(count.html())+1);
			   			}
						return;
			   		},
			   		error:function (msg) {  
			        }
				});
	   		});
	   		$('#pinglun').click(function(){
	   			window.location.href= "#topinglun";
	   		});
	   		
		});
	   function saveComment(){
	   		var content = $("div.jqte_editor");
	   		var trueContent = $(content).html();
  			/**
  			$("div.jqte_editor span").each(function(){
  				alert($(this).text());
  				$(this).html();
  				var img=$(this).children("img").attr("name") 
  				$(this).children("img").replaceWith(img);
  				alert($(this).text());
  			})
  			
  			 var addrs="";
  			 var imges = $("div.jqte_editor img");
   			 $.each(imges,function(i,e){
   			 	var iname =  $(imges[i]).attr("name");
   			 	addrs+=iname+",";
   			 });
   			 **/
	   		if($(content).html()==''){							//判断内容不可为空
	   			alert('评论内容不可为空');
	   			$(content).val('');
	   		}else if($(content).html().length>3000){			//判断文本框文字字数不能超过200字
	   			alert("评论内容不能超过500个字符,表情也算字符。");
	   			var imgText = $('<div>').append($('div.jqte_editor img').clone()).remove().html();
	   			$("div.jqte_editor").html(imgText+$(content).text().substring(0,500));
	   		}else{
	   			var articleId = '<s:property value="article.id"/>';			//得到文章ID
	   			$.getJSON(
	   				$.fn.getRootPath()+"/app/micro/comment!savecommentinfo.htm",
	   				{"comment.article.id":articleId,"comment.nickname":$("#nickName").val(),"comment.comment":$("div.jqte_editor").html()},
	   				function(result){
	   					if(result.success=="success"){
	   						alert('评论成功！');
	   						$("#nickName").val("");
	   						$("#content").val('');	
	   						$("div.jqte_editor").text('');						//清空评论框
	   						$(".comment-total").html(result.count);	//更新评论总数
	   						var nickname="匿名用户";
	   						if(result.comment.nickname!=''){
	   							nickname=result.comment.nickname;
	   						}
	   						//alert($("div.jqte_editor").html());
	   						$(".comment-form").before(						//加载新增评论至对应位置
	   							'<div class="comment-body">'+
					   			'<p style="font-size: 14px;padding: 0 0 15px 15px;">'+
					   				'<span >'+nickname+'</span>'+
					   				'<span style="background-color: #CCCCCC;border-bottom-left-radius: 5px;border-top-left-radius: 5px;color: #FFFFFF;float: right;font-size: 14px;padding: 2px 10px;">'+result.count+'楼</span>'+
					   			'</p>'+
					   			'<p style="font-size: 15px; padding: 0 15px 10px;">'+result.comment.comment+'</p>'+
					   			'<p style="color: #555555;font-size: 14px;line-height: 26px;padding: 0 15px;">'+result.createtime+'</p>'+
					   			'<p style="border-top: 1px solid #CCCCCC;"></p>'+
					   			'</div>' 
	   						);
	   					}else{
	   						alert('评论失败！');
	   					}
	   				}
	   			);
	   		}
	   }
	   </script>
	   </s:if>
	</body>
</html>