﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>${activity.title}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;"
        name="viewport" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <link href="${pageContext.request.contextPath}/resource/micro/css/base.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/resource/micro/css/templete.css?1" rel="stylesheet" type="text/css" />
</head>
<body>
			   <div class="temslide J_slider">
				    	<div><a href='${pageContext.request.contextPath}/app/micro/article!articleinfo.htm?id=${article.id}'>
				    	<img lazyload='${pageContext.request.contextPath}${article.imageurl}'></a>
				    	<p>${article.title}</p>
				    	</div>
			    </div>

			<div class="wrap">
		        <ul class="menu-ul siteLinkList">
		        	<s:iterator value="#request.clalist" var="va">
				            <li>
				            <div class="menu-btn">
				            <a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${va.id}'>
				            <img src='${pageContext.request.contextPath}${va.imageurl}' /></a>
				            <p class='desc'>${va.title}
				            </p></div></li>
				            <li></li>
				    </s:iterator>    
		        </ul>
		    </div>

	<!-- 
    <c:if test="${fn:length(articleList)>0}">
    <div class="temslide J_slider">
    	<c:forEach items="${articleList}" var="article" varStatus="status">
    	<div><a href='${article.linkUrl}'>
    	<img lazyload='${article.picUrl}'></a>
    	<p>${article.title}</p>
    	
    	</div>
    	</c:forEach>
    
    </div>
    </c:if>

    <div class="wrap">
        <ul class="menu-ul siteLinkList">
        	<c:forEach items="${categoryList}" var="category" varStatus="status">
            <li>
            <div class="menu-btn">
            <a href='${category.activityUrl}'>
            <img src='${category.picUrl}' /></a>
            <p class='desc'>${category.name}
            </p></div></li>
            <li></li>
            </c:forEach>
        </ul>
    </div>
     -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/touch.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.extend.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/ppkextend.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/autoCreatehomePageTemplete.js"></script>
</body>
</html>
