<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>${activity.title}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;"
        name="viewport" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <link href="${pageContext.request.contextPath}/resource/micro/css/base.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/resource/micro/css/templete.css?1" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="temslide J_slider">
	    	<div><a href='${pageContext.request.contextPath}/app/micro/article!articleinfo.htm?id=${article.id}'>
	    	<img lazyload='${pageContext.request.contextPath}${article.imageurl}'></a>
	    	<p>${article.title}</p>
	    	</div>
    </div>
    <iframe id="iFrame1" name="iFrame1" width="100%" onload="this.height=iFrame1.document.body.scrollHeight" frameborder="0" src="<%= request.getContextPath() %>/app/micro/classification!microtemppage.htm?id=${activity.id }"></iframe>
<!-- 
    <div class="wrap">
        <ul class="menu-ul siteLinkList">
        	<c:forEach items="${categoryList}" var="category" varStatus="status">
            <li><div class='menu-btn'>
            <a href='article!cate.action?id=${category.id}&activityId=${activityId}&openId=${openId}'>
            <img src='${category.picUrl}' /></a>
            <p class='desc'>${category.name}
            </p></div></li>
            <li></li>
            </c:forEach>
        </ul>
    </div>
 -->
<script type="text/javascript">
var iframeids=["test"]
var iframehide="yes"
function dyniframesize()
{
var dyniframe=new Array()
for (i=0; i<iframeids.length; i++)
{
if (document.getElementById)
{
dyniframe[dyniframe.length] = document.getElementById(iframeids[i]);
if (dyniframe[i] && !window.opera)
{
dyniframe[i].style.display="block"
if (dyniframe[i].contentDocument && dyniframe[i].contentDocument.body.offsetHeight)
dyniframe[i].height = dyniframe[i].contentDocument.body.offsetHeight;
else if (dyniframe[i].Document && dyniframe[i].Document.body.scrollHeight)
dyniframe[i].height = dyniframe[i].Document.body.scrollHeight;
}
}
if ((document.all || document.getElementById) && iframehide=="no")
{
var tempobj=document.all? document.all[iframeids[i]] : document.getElementById(iframeids[i])
tempobj.style.display="block"
}
}
window.scrollTo(0,0);
}

if (window.addEventListener)
window.addEventListener("load", dyniframesize, false)
else if (window.attachEvent)
window.attachEvent("onload", dyniframesize)
else
window.onload=dyniframesize
</script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/touch.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.extend.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/zepto.ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/ppkextend.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resource/micro/js/autoCreatehomePageTemplete.js"></script>
</body>
</html>
