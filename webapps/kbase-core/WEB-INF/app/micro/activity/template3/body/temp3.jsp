<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html class="ui-mobile"><head><!-- base href="http://192.168.2.105:8084/xmascms/zy/question/newpage.html" -->
    <title>newpage.html</title>
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;"/>
	
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="this is my page">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
<!--    <link rel="stylesheet" type="text/css" href="./styles.css">
 --><!--  <link rel="stylesheet" href="/xmascms/micro/ikea/ikea_files/jquery.css">
 -->  <style type="text/css" charset="utf-8">/* See license.txt for terms of usage */

.min-contentone{width:220px;height:40px;font-size:14px;text-align:right;font-weight:normal;color:#fff;overflow:hidden;line-height:40px;position:absolute;right:0px;bottom:0px;z-index:2000;padding:0px 10px;}
.min-contenttwo{width:190px;height:34px;background-color:#000;filter:alpha(opacity=50);opacity: 0.5;position:absolute;right:0px;bottom:0px;z-index:1000;}

</style></head>
  
  <body class="ui-mobile-viewport ui-overlay-c" style="BACKGROUND-COLOR:#384347;margin:0px;">
<!--     This is my HTML page. <br>
 -->    <!-- Home -->
<div style="min-height: 284px;" class="ui-page ui-body-c ui-page-active" tabindex="0" data-url="page1" data-role="page" id="page1">
    <div data-role="content">
     <s:iterator value="#request.classlist" var="va">
        <div style="height: 130px;position:relative;" onclick="window.top.location='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${va.id}'" href="javascript:void(0);">
        	<div class="min-contentone">${va.title}</div>
            <div class="min-contenttwo"></div>
            <img style="height: 130px; width: 100%;" src="${pageContext.request.contextPath}${va.imageurl}">
        </div>
    </s:iterator>
    </div>
</div>
  

<!-- <div class="ui-loader ui-corner-all ui-body-a ui-loader-default"><span class="ui-icon ui-icon-loading"></span><h1>loading</h1></div></body></html> -->