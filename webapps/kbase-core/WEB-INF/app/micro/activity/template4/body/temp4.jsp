<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>temp4</title>
<!-- base href="http://www.apiwx.com/" -->
<meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta charset="utf-8">
<link href="${pageContext.request.contextPath}/resource/micro/template4/css/cate6_1.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resource/micro/template4/css/iscroll.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/micro/template4/css/plugmenu.css">
<style>
 .c1{
	background-color:#0072BC;float:left;display: block;
}
 .themeStyle{background:#101F80 !important; background-color:#101F80 !important; }  
</style>
<script src="${pageContext.request.contextPath}/resource/micro/template4/js/iscroll.js" type="text/javascript"></script>
<script type="text/javascript">
var myScroll;

function loaded() {
myScroll = new iScroll('wrapper', {
snap: true,
momentum: false,
hScrollbar: false,
onScrollEnd: function () {
document.querySelector('#indicator > li.active').className = '';
document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
}
 });
 
}

document.addEventListener('DOMContentLoaded', loaded, false);
</script>
 
</head>
<body id="cate6">




 <div id="insert1"></div>
<ul class="mainmenu">
  
  
<s:iterator value="#request.classlist" var="va">
<li class="li0 ">
<a onclick="window.top.location='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${va.id}'" href="javascript:void(0);">
<div class="menubtn">
<div style="position:relative;" class="menumesg">

	<s:if test="#va.name.length>3">
		<div class="menutitle" style="width:80%">${va.title}</div>
	</s:if>
	<s:if test="#va.name.length>4">
		<div class="menutitle" style="width:80%;font-size: 19px;">${va.title}</div>
	</s:if>
	<s:else>
		<div class="menutitle">${va.title}</div>
	</s:else>

<div class="menuico" style="position:absolute;right:10px;bottom:10px;z-index:1000;"></div>
</div>
<div style="width:70%" class="menuimg"><img src="${pageContext.request.contextPath}${va.imageurl}"></div>
</div>
</a>
</li>
</s:iterator>
 


 	
<div class="clr"></div>
</ul>

<script>


var count = document.getElementById("thelist").getElementsByTagName("img").length;	
var count2 = document.getElementsByClassName("menuimg").length;


for(i=0;i<count;i++){
 document.getElementById("thelist").getElementsByTagName("img").item(i).style.cssText = " width:"+document.body.clientWidth+"px";

}
for(i=0;i<count2;i++){
 if(i==0||i==4||i==8||i==12){
document.getElementsByClassName("menuimg").item(i).style.cssText = " HEIGHT:"+(document.body.clientWidth/320)*111+"px";
document.getElementsByClassName("menumesg").item(i).style.cssText = " HEIGHT:"+(document.body.clientWidth/320)*111+"px";
 }else{
 document.getElementsByClassName("menuimg").item(i).style.cssText = " HEIGHT:48px";
document.getElementsByClassName("menumesg").item(i).style.cssText = " HEIGHT:48px";
 }
}

document.getElementById("scroller").style.cssText = " width:"+document.body.clientWidth*count+"px";


 setInterval(function(){
myScroll.scrollToPage('next', 0,400,count);
},3500 );

window.onresize = function(){ 
for(i=0;i<count;i++){
document.getElementById("thelist").getElementsByTagName("img").item(i).style.cssText = " width:"+document.body.clientWidth+"px";

}
for(i=0;i<count2;i++){
 
 if(i==0||i==4||i==8||i==12){
document.getElementsByClassName("menuimg").item(i).style.cssText = " HEIGHT:"+(document.body.clientWidth/320)*111+"px";
document.getElementsByClassName("menumesg").item(i).style.cssText = " HEIGHT:"+(document.body.clientWidth/320)*111+"px";
 }else{
 document.getElementsByClassName("menuimg").item(i).style.cssText = " HEIGHT:48px";
document.getElementsByClassName("menumesg").item(i).style.cssText = " HEIGHT:48px";
 }
}

 document.getElementById("scroller").style.cssText = " width:"+document.body.clientWidth*count+"px";
} 

</script>

<script src="${pageContext.request.contextPath}/resource/micro/template4/js/zepto.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/micro/template4/js/plugmenu.js" type="text/javascript"></script>
 <div id="insert2"></div>
  <div style="display:none"> </div>
 

</body></html>