<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>

		<title>${activity.title}</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta name="format-detection" content="telephone=no" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/micro/template6/css/css.css">

	</head>

	<body class="zy-bg01">
		
		<ul class="zy-homecn1">
        	<c:forEach items="${clalist}" var="category" varStatus="status">
            <li>
            
            
            <c:choose>
            <c:when test="${status.index%2==0}">
            <div class="fl">
            <p class="zy-homep1 zy-mr1" style="margin-right:1px;">
            		<a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${category.id}'>
            			<img src='${pageContext.request.contextPath}${category.imageurl}' />
            		</a>
            	</p>
            	<p class="zy-homep2 zy-mr1" style="margin-right:1px;">
					<a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${category.id}'>${category.title}</a>
				</p>
            </div>
            </c:when>
            <c:otherwise>
            <div class="fr">
            <p class="zy-homep1 zy-mr1" style="margin-left:1px;">
            		<a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${category.id}'>
            			<img src='${pageContext.request.contextPath}${category.imageurl}' />
            		</a>
            	</p>
            	<p class="zy-homep2 zy-mr1" style="margin-left:1px;">
					<a href='${pageContext.request.contextPath}/app/micro/classification!showarticleinfo.htm?id=${category.id}'>${category.title}</a>
				</p>
            </div>
            </c:otherwise>
            </c:choose>
            
            
            	
            </li>
            </c:forEach>
        </ul>
		
		
	
	
	
	
		

	</body>
</html>
