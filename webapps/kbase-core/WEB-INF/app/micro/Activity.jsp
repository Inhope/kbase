<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/task/task-menu.js"></script>
		<style type="text/css">
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		<script type="text/javascript">
			function toaddactivity(){
				window._index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改活动信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/activity!toaddorupdate.htm"},
					area: ['30%', '40%']
				});
			}
			function showdetail(names,id){
			   parent.parent.TABOBJECT.open({
					id : 'activity',
					name : '类别列表-',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/micro/classification.htm?id=' + id,
					isRefresh : true
				}, this);
			}
			
			function getselectinfo(){
				var selectvalues=[];
				$("input:checkbox[name='activity_id']:checked").each(function(){
					selectvalues.push($(this).val());
				})
				return selectvalues;
			}
			
			function delactivity(){
				var selectvalue = getselectinfo();
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}
				layer.confirm("确定要删除选中的数据吗？", function(index){
					$.ajax({
					    url:$.fn.getRootPath()+'/app/micro/activity!delactivity.htm',
					    data:{"ids":selectvalue.join(',')},
						type:"post",
						dataType:"json",
						success:function(data){
							layer.alert("成功删除",-1);
					       layer.close(index);
					       window.location.href=$.fn.getRootPath()+'/app/micro/activity.htm';
					    }
			  　　　　 });
				})
			}
			
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/micro/activity.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
			}
			
			function tomodifyinfo(){
				var selectvalue = getselectinfo();
				if(selectvalue.length==0){
					parent.layer.alert("请至少选择一条数据", -1);
					return false;
				}if(selectvalue.length>1){
					parent.layer.alert("不能选择多条数据!", -1);
					return false;
				}
				window._index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: ['添加/修改活动信息', 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+"/app/micro/activity!toaddorupdate.htm?id="+selectvalue},
					area: ['60%', '40%']
				});
			}
			
		</script>
	</head>
		<body>
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="min-width:860px;">
							<div class="gonggao_titile">
								<div class="gonggao_titile_right">
									<a href="javascript:void(0);" onclick="delactivity()" id="">删除</a>
									<a href="javascript:void(0);" onclick="tomodifyinfo()" id="">修改</a>
									<a href="javascript:void(0);" onclick="toaddactivity()" id="" >添加</a>
								</div>
							</div>
						 	
							<div class="yonghu_titile">
								<ul>
									<li>
										创建时间:
										<input id="starttime_select" name="activity.starttime" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
											value="${activity.starttime }" style="width:100px"/>
										至
										<input id="end_select" name="activity.endtime" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
											value="${activity.endtime }"  style="width:100px" />
									</li>
									<li>
										活动名称:
										<input type="text" id="title_select" name="activity.title" value="${activity.title }"/>
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" />
										</a>
									</li>
								</ul>
							</div>
						 	
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="2%">&nbsp;</td>
											<td width="35%">
												微页面地址
											</td>
											<td width="18%">
												活动名称
											</td>
											
											<td width="15%">
												创建时间
											</td>
											<td width="10%">
												当前模板
											</td>
											
											<td width="10%">
												是否启用
											</td>
											<td width="10%">
												查看类别
											</td>
										
										</tr>
										<s:iterator value="#request.page.result" var="va">
											<tr>
												<td>
													<input  name="activity_id" type='checkbox' value="${va.id }" />
												</td>
												<td>
													${pageContext.request.contextPath}/app/micro/classification!micropage.htm?id=${va.id }
												</td>
												<td>
													${va.title }
												</td>
												<td>
													<s:date name="#va.createDate" format="yyyy-MM-dd" />
												</td>
												<td>
													<s:iterator value="#request.modual" var="mode">
														<s:if test="#mode.key==#va.module">${mode.label}</s:if>
													</s:iterator>
												</td>
												<td>
													<s:if test="#va.isenable==1">启用</s:if><s:if test="#va.isenable==0">停用</s:if>
												</td>
												<td>
													<a href="#" onclick="showdetail('${va.title }','${va.id }')">查看类别</a>
												</td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
	</body>
</html>

