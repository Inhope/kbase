<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/link/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/link/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/link/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >  
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/link/js/link.js"></script>
	</head>

	<body>
		<s:set name="maxLength" value="10"/>
		<s:set name="maxLengthAddress" value="30"/>
		<!-- 
		<div class="content_right_bottom">
			<div class="zhishi_titile">
				<a href="${pageContext.request.contextPath}/app/link/quik-link.htm">快速链接列表</a>
				<button>新建</button>&nbsp;<button>修改</button>&nbsp;<button>删除</button>
			</div>
			
			<div class="zhishi_toolbar" style="display: none;">
				<button>新建</button>&nbsp;<button>修改</button>&nbsp;<button>删除</button>
			</div>
	
			<div class="zhishi_con">
				<div class="zhishi_con_title">
					<h1>
						<input type="checkbox">
					</h1>
					<h2>
						<b>名称</b>
					</h2>
					<h3>
						Url
					</h3>
					<h4>
						创建时间
					</h4>
					
				</div>
				<div class="zhishi_con_nr">
					<ul>
						<s:iterator var="fc" value="#request.linklist">
							<li>
								<h1>
									<input id="<s:property value="#fc.id"/>" type="checkbox">
								</h1>
								<h2>
									<a id=<s:property value="#fc.id"/> href="<s:property value="#fc.address"/>" title="<s:property value="#fc.name"/>" target="_blank">
										<s:if test="#fc.name.length() > #maxLength">
											<s:property value="#fc.name.substring(0, #maxLength) + '...'"/>
										</s:if>
										<s:else>
											<s:property value="#fc.name"/>
										</s:else>
									</a>
								</h2>
								<h3>
								<a title="<s:property value="#fc.address"/>" href="javascript:void(0);">
									<s:if test="#fc.address.length() > #maxLengthAddress">
											<s:property value="#fc.address.substring(0, #maxLengthAddress) + '...'"/>
									</s:if>
									<s:else>
										<s:property value="#fc.address"/>
									</s:else>
								</a>
								</h3>
								<h4><s:date name="#fc.createTime" format="yyyy-MM-dd HH:mm:ss"/></h4>
							</li>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>
		-->
		
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="5%"></td>
						<td width="30%"></td>
						<td width="50%"></td>
						<td width="15%"></td>
					</tr>
					<tr>
						<td colspan="4" align="right" valign="middle" style="text-align:right;">
							<div class="gonggao_titile_right">
								<a href="javascript:void(0);" id="btnDel">删除</a>
								<a href="javascript:void(0);" id="btnEdit">修改</a>
								<a href="javascript:void(0);" id="btnAdd">新建</a>
							</div>
						</td>
					</tr>
					<tr class="tdbg">
						<td><input type="checkbox" id="selAll" /></td>
						<td>名称</td>
						<td>链接地址</td>
						<td>创建时间</td>
					</tr>
					<s:iterator var="fc" value="#request.linklist">
						<tr>
							<td><input id="<s:property value="#fc.id"/>" type="checkbox" xname='childCheckBox' xlinkname='<s:property value="#fc.name"/>' xlinkurl='<s:property value="#fc.address"/>'></td>
							<td>
								<a id=<s:property value="#fc.id"/> href="<s:property value="#fc.address"/>" title="<s:property value="#fc.name"/>" target="_blank">
									<s:if test="#fc.name.length() > #maxLength">
										<s:property value="#fc.name.substring(0, #maxLength) + '...'"/>
									</s:if>
									<s:else>
										<s:property value="#fc.name"/>
									</s:else>
								</a>
							</td>
							<td>
								<a href="<s:property value="#fc.address"/>" target="_blank">
									<s:if test="#fc.address.length() > #maxLengthAddress">
											<s:property value="#fc.address.substring(0, #maxLengthAddress) + '...'"/>
									</s:if>
									<s:else>
										<s:property value="#fc.address"/>
									</s:else>
								</a>
							</td>
							<td>
								<s:date name="#fc.createTime" format="yyyy-MM-dd HH:mm:ss"/>
							</td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
		
		<!-- 
		<div class="f-div" style="display: ;">
			<div class="f-diva">
				<div class="f-div-t">
				    <b>添加链接</b>
					    <a href="javascript:void(0);" style="float: right;">
					    	<img width="12" height="12" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg">
					    </a>
				    </div>
				<div class="f-div-c">
					<div class="link_name">
						<span>链接名：</span>
						<input type="text"/>
					</div>
					<div class="link_url">
						<span>URL：</span>&nbsp;
						<input type="text"/>
					</div>
					<div class="link_btn">
						<button>提交</button>
					</div>
				</div>
			</div>
		</div>
		 -->
	</body>
</html>
