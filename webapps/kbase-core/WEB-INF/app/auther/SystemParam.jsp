<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	    
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro-${sessionScope.session_user_key.userInfo.userTheme}/easyui.css" />
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
	    
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.blockUI.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.ajaxfileupload.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
	    
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/resource/auther/js/SystemParam.js"></script>
	    
		<title>systemparam</title>
		
		
		<SCRIPT type="text/javascript">
		$(document).ready(function(){
			$(".textbox").each(function(){
				$(this).css({"width": "400px", "height": "26px", "line-height": "24px"});
			});
			
			$(".textbox-text validatebox-text").each(function(){
				$(this).css({"width": "100%", "margin-right": "0px", "margin-left": "0px", "height": "24px", "line-height": "24px", 
				"padding-right": "0px", "padding-left": "0px", "border-top-width": "0px", "border-right-width": "0px", "border-bottom-width": "0px", 
				"border-left-width": "0px", "border-top-style": "none", "border-right-style": "none", "border-bottom-style": "none", 
				"border-left-style": "none", "background-image": "none", "background-attachment": "scroll", "background-repeat": "repeat", 
				"background-position-x": "0%", "background-position-y": "0%", "background-color": "rgb(255, 255, 255)"});
			});
		});
		</SCRIPT>
		
	</head>

	<body>
		<div class="easyui-panel" title="系统参数设置" style="width:600px">
	        <div style="padding:10px 10px 10px 10px">
		        <form action="${pageContext.request.contextPath }/app/auther/system!uploadImg.htm" method="post" enctype="multipart/form-data">
		            <table cellpadding="5">
		                <s:iterator var="sp" value="#request.systemParams">
							<tr>
								<td style="font-size:10pt;"><label zokee="x-man">${sp.label}</label></td>
								<td>
									<c:choose>
										<c:when test="${sp.name=='system_shortcut_icon' or sp.name=='system_login_logo' or sp.name=='system_banner_logo'}">
											<!-- <input style="width:300px;" class="easyui-filebox" buttonText="请选择图片" name="${sp.name }" id="${sp.name }" xname="${sp.name}" id="${sp.name }" xtext="${sp.label}" value="${sp.value}" i="${sp.i}" zokee="x-man"></input> -->
											<input type="file" name="${sp.name }" id="${sp.name }"/>
											&nbsp;
											<a href="javascript:void(0)" class="easyui-linkbutton" onclick="uploadImage('${sp.name }');" style="height:22px;">上传</a>
											<c:choose>
												<c:when test="${not empty sp.value}">
													<a href="javascript:void(0)" class="easyui-linkbutton" onclick="deleteImage('${sp.name }');" style="height:22px;">删除</a>
												</c:when>
												<c:otherwise>
													<font size="2" color="gray">
													<c:if test="${sp.name=='system_login_logo'}">图片建议大小(330px * 40px)</c:if>
													<c:if test="${sp.name=='system_banner_logo'}">图片建议大小(230px * 40px)</c:if>
													</font>
												</c:otherwise>
											</c:choose>
											<input type="hidden" name="imgName_${sp.name }" value="${sp.value }"/>
										</c:when>
										<c:otherwise>
											<input style="width:400px;" class="easyui-textbox" type="text" name="${sp.name }" xname="${sp.name}" xtext="${sp.label}" value="${sp.value}" i="${sp.i}" zokee="x-man"></input>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</s:iterator>
		            </table>
		        </form>
		        <div style="text-align:center;padding:5px">
		            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">提交</a>&nbsp;&nbsp;
		            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:location.reload();">刷新</a>
		        </div>
	        </div>
    	</div>
	</body>
</html>
