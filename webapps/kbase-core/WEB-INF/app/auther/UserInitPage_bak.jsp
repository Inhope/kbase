<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>用户管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		
		<script type="text/javascript">	
			//***************************************查询条件部门岗位树（开始）***************************	
			var setting = {
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "s", "N": "s" }
				},
				callback: {
					onCheck: zTreeOnCheck									
				}
			};
			
			var zNodes = ${json_deps};		
			
			//勾选
			function zTreeOnCheck(e, treeId, treeNode) {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				v = "";
				depIds = "";
				//nodes.sort(function compare(a,b){return a.id-b.id;});
				for (var i=0, l=nodes.length; i<l; i++) {
					v += nodes[i].name + ",";
					depIds +=  nodes[i].id + ",";
				}
				
				if (v.length > 0 ) v = v.substring(0, v.length-1);
				if (depIds.length > 0 ) depIds = depIds.substring(0, depIds.length-1);
				$("#depNames_select").attr("value", v);
				$("#depIds_select").attr("value", depIds);
			}					
//**************************************下拉框*************************************			
			function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
		</script>
		<script type="text/javascript">
			var intervalTitle = null;//提示标题
			var interval = null;//处理结果
			var prefix = "";//提示标题前缀
			var suffix = "";//提示标题后缀
			//提示标题
			function importWarningTitle(){
				if(suffix.length==4)suffix = "";
				$("#importWarning_title").html("<b>"+prefix+suffix+"<b/>");
				suffix += "*";
			}
			
			//导入导出结束初始化
			function importOrExport_init(){
				if(interval!=null)clearInterval(interval);
				if(intervalTitle!=null)clearInterval(intervalTitle);
				$("#userImportShadeClose").bind("click",function(){
					closeShade("userImportShade");
				});
			}
			
			//导入、导出消息提示
			function importOrExport(type){
				if(type=='import'){
					prefix = "导入中"
				}else if(type=='export'){
					prefix = "导出中"
				}else{
					return false;
				}
				//显示消息提示区
				$('#scBefore').css("display","none");
				$('#importWarning').css("display","block");
				$('#scAfter').css("display","none");
				$('#importHandle').css("display","none");
				$("#rowTotal").html(0);
				$("#fuccessTotal").html(0);
				$("#failureTotal").html(0);
				$("#userImportShadeClose").unbind("click");//去掉关闭事件
				//提示标题
				intervalTitle = setInterval("importWarningTitle(prefix, suffix)",500);
				//提示结果
			   	interval = setInterval(function(){
			   		$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/auther/user!importUserResult.htm', 
						data : {'type':type},
						async: true,
						dataType : 'json',
						success : function(data){
							$("#rowTotal").html(data.rowTotal);
							$("#fuccessTotal").html(data.fuccessTotal);
							$("#failureTotal").html(data.failureTotal);
							if(data.result==true){
								importOrExport_init();
							}
						},
						error : function(msg){
							importOrExport_init();
							parent.layer.alert("网络错误，请稍后再试!", -1);
						}
					});
			   	},500);
			}
			
			//数据导出
			function exportUser(status){
				//打开提示框
				$("#title_importOrExport").html("批量导出");
				openShade("userImportShade");
				
				//导出消息提示
				importOrExport('export');
				
				$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!exportUser.htm?status="+status);
				$("#form0").submit();
			}
			
			//导入
			function importUser(){
				if($("#userFile").val()==null||$("#userFile").val()==''){
					parent.layer.alert("请选择需要上传的文件!", -1);
					return false;
				}
				
				//导入消息提示
				$("#title_importOrExport").html("批量导入");
				importOrExport('import');
				
				//数据导入
				$.ajaxFileUpload({
			        url: $.fn.getRootPath()+"/app/auther/user!importUser.htm",
			        secureuri: false,
			        fileElement: $("#userFile"),
			        uploadFileParamName : 'userFile',
			        success: function(data, status) {
			        	$('#scBefore').css("display","none");
						$('#importWarning').css("display","none");
						$('#scAfter').css("display","block");
						$('#importHandle').css("display","block");
						
						data = eval('(' + $(data).text() + ')');
					   	$('#scAfter p').html(data.returnResult);
			        	importOrExport_init();
			        },
			        error: function(obj, msg, e) {
			        	importOrExport_init();
			        	parent.layer.alert("网络错误，请稍后再试!", -1);
			        }
			    });
			}
		</script>
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!initPage.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
				
			}
			
			//帐号启用/停用
			function accountNumber(status){
				var msg = "停用"
				if(status=="0"){
					//0表示启用，1表示停用
					msg = "启用"
				}
							
				var userId_list = $("input:checkbox[name='userId_list']:checked");
				if(userId_list.length<1){
					parent.layer.alert("请选择要"+msg+"的账号!", -1);
					return;
				}
				
				var bl = true;
				var userIds = "";
				$(userId_list).each(function(){
					var a =  $(this).next().val();
					if(a==status){
						bl = false;
					}else{
						userIds += $(this).val()+",";
					}					
					
				});

				if (bl && userId_list!=""){
					parent.layer.confirm("确定要"+msg+"选中的账号吗？", function(){
						$('body').ajaxLoading('正在保存数据...');
						$.post($.fn.getRootPath()+"/app/auther/user!accountNumber.htm",
							{'userIds':userIds,'status':status},
							function(data){
								if(data=="1"){
									parent.layer.alert("操作成功!", -1);					
									$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!initPage.htm");
									$("#form0").submit();
								}else{
									parent.layer.alert("操作失败!", -1);
									$('body').ajaxLoadEnd();
								}
								
						},"html");
					});
				}else{
					parent.layer.alert("已有账号被"+msg+"!", -1);
				}
				
			}
			
			//密码重置
			function passwordReset(){		
				var bl = false;
				var userIds = "";
				$("input:checkbox[name='userId_list']:checked").each(function(){
					bl = true;
					userIds += $(this).val()+",";
				});
				if(!bl){
					parent.layer.alert("请选择要重置密码的账户!", -1);
					return;
				}
				if (bl){
					parent.layer.confirm("确定重置选中账户的密码吗？", function(){
						$('body').ajaxLoading('正在保存数据...');
						$.post($.fn.getRootPath()+"/app/auther/user!passwordReset.htm",
							{'userIds':userIds},
							function(data){
								if(data=="1"){
									parent.layer.alert("操作成功!", -1);					
									$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!initPage.htm");
									$("#form0").submit();
								}else{
									parent.layer.alert("操作失败!", -1);
									$('body').ajaxLoadEnd();
								}
						},"html");
					});
				}
			}			
			
			//查询条件初始化
			function userSelectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
				});
				
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = treeObj.getCheckedNodes(true);
				for(var i=0;i<nodes.length;i++){
					treeObj.checkNode(nodes[i],false,false);
				}
			}
			
			//弹出框打开
			function openShade(shadeId){
				var w = ($('body').width() - $('#'+shadeId).width())/2;
				$('#'+shadeId).css('left',w+'px');
				$('#'+shadeId).css('top','0px');
				$('body').showShade();
				$('#'+shadeId).show();					
			}
			
			//弹出框关闭
			function closeShade(shadeId){
				$('#'+shadeId).css("display","none");
				$('body').hideShade();			
			}
			
			//全选
			function checkAll(){
				$("input:checkbox[name='userId_list']").each(function(){
					$(this).attr("checked",$(this).attr("checked")=='checked'?false:true);
				});
			}
			//**************************************初始化*************************************		
			$(document).ready(function(){		
				
				//新增用户
				$("#userAdd").click(function(){					
					$.post($.fn.getRootPath()+"/app/auther/user!addOrEditUserTo.htm",
						{},function(data){
						if(data!=null&&data!=''){						
							$("#userShadeCotent").html(data);
							openShade("userShadeCotent");
						}
					},"html");				
					
				});
				
				//编辑用户
				$("#userEdit").click(function(){
					
					var userId_list = $("input:checkbox[name='userId_list']:checked")
					if(userId_list.length==0){
						parent.layer.alert("请选择要编辑的数据!", -1);
						return;
					}else if(userId_list.length>1){
						parent.layer.alert('不能同时编辑多条数据!', -1);
						return;
					}else{
						var userId = $(userId_list[0]).val();
						$.post($.fn.getRootPath()+"/app/auther/user!addOrEditUserTo.htm",
							{'userId':userId},function(data){
							if(data!=null&&data!=''){						
								$("#userShadeCotent").html(data);
								openShade("userShadeCotent");
							}
						},"html")
					}									
				});			
				
				//批量导入
				$("#userImport").click(function(){
				 	$('#scBefore').css("display","block");
					$('#scAfter').css("display","none");
					$('#importWarning').css("display","none");
					$('#importHandle').css("display","block");
					openShade("userImportShade");
				});
				//批量导入弹出窗关闭
				$('#userImportShadeClose').click(
					function(){closeShade("userImportShade");
				});
				
				
						
				//删除用户
				$("#userDel").click(function(){
					var bl = false;
					var userIds = "";
					$("input:checkbox[name='userId_list']:checked").each(function(){
						bl = true;
						userIds += $(this).val()+",";
					});
					if(!bl){
						parent.layer.alert("请选择要删除的数据!", -1);
						return;
					}
					if (bl){
						parent.layer.confirm("确定要删除选中的数据吗？", function(){
							$('body').ajaxLoading('正在保存数据...');
							$.post($.fn.getRootPath()+"/app/auther/user!delUser.htm",
								{'userIds':userIds},
								function(data){
									if(data=="1"){
										parent.layer.alert("操作成功!", -1);					
										pageClick('1');
									}else{
										parent.layer.alert("操作失败!", -1);
										$('body').ajaxLoadEnd();
									}
							},"html");
						});
					}
				});
				
				
								
				//部门岗位树初始化
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);			
				//部门岗位树勾选初始化
				var depIds = $("input[name='depIds']:eq(0)").val();
				if(depIds!=null&&depIds!=''){
					depIds = depIds.split(",");
					var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
					for(var i=0;i<depIds.length;i++){
						var treeNode = treeObj.getNodeByParam("id",depIds[i], null);
						if(treeNode!=null)treeObj.checkNode(treeNode,true,false);
					}					
				}				
				
			});
		
		</script>
	</head>
	<body>
<!--******************************************************用户管理初始化页面*********************************************************************************  -->
		<form id="form0" action="" method="post">
			<div class="content_right_bottom" style="min-width:860px;">

				<div class="gonggao_titile">
					<div class="gonggao_titile_right">
						<!--
							<a id="userImport" href="javascript:void(0)">批量导入</a>
							<a href="javascript:void(0)" onclick="passwordReset()">重置密码</a>
							<a href="javascript:void(0)" onclick="accountNumber('1')">帐号停用</a>
							<a href="javascript:void(0)" onclick="accountNumber('0')">帐号启用</a>
							<a id="userDel" href="javascript:void(0)">删除用户</a>
							<a id="userEdit" href="javascript:void(0)">编 辑</a>
							<a id="userAdd" href="javascript:void(0)">新增用户</a>					
						-->
						<myTag:input type="a" value="导出选择"  key="userManageExportUser0" 		onclick="exportUser('0')" />
						<myTag:input type="a" value="导出全部"  key="userManageExportUser1" 		onclick="exportUser('1')" />
						<myTag:input type="a" value="批量导入"  key="userManageImport" 			id="userImport"  />
						<myTag:input type="a" value="重置密码"  key="userManagePasswordReset" 	onclick="passwordReset()"  />
						<myTag:input type="a" value="帐号停用"  key="userManageAccountNumber1" 	onclick="accountNumber('1')"  />
						<myTag:input type="a" value="帐号启用"  key="userManageAccountNumber0" 	onclick="accountNumber('0')"  />
						<myTag:input type="a" value="删除用户"  key="userManageDel" 				id="userDel"  />
						<myTag:input type="a" value="编辑用户"  key="userManageEdit" 				id="userEdit"  />
						<myTag:input type="a" value="新增用户"  key="userManageAdd" 				id="userAdd"/>
					</div>
				</div>

				<div class="yonghu_titile">
					<ul>
						<li>
							工号：
							<input id="jobNumber_select" style="width: 55px;" name="jobNumber" type="text" value="${jobNumber }"/>							
						</li>
						<li>
							账号：
							<input id="username_select" style="width: 55px;" name="username" type="text" value="${username }"/>
						</li>
						<li>
							姓名：
							<input id="userChineseName_select" style="width: 55px;" name="userChineseName" type="text" value="${userChineseName }"/>
						</li>
						<li>
							角色：
							<s:select id="roleId_select" list="#request.list_Role" name="roleId" headerKey="" headerValue="-请选择-" cssStyle="width: 70px;"
									listKey="id" listValue="name" value="#request.roleId"></s:select>		
						</li>
						<li>
							外网访问：
							<s:select id="visit_select" list="#{'1':'是','0':'否'}" name="visit" headerKey="" headerValue="-请选择-" cssStyle="width: 70px;"></s:select>
						</li>
						<li>
							组织架构：
							<input id="depNames_select" style="width: 80px;"  type="text" name="depNames" value="${depNames }" readonly="readonly" onfocus="showMenu('menuContent','depNames_select')"/>
							<input id="depIds_select" type="hidden" name="depIds" value="${depIds }"/>
						</li>
						<li class="anniu">
							<a href="javascript:void(0)"><input type="button" class="youghu_aa1"
									value="重置" onclick="userSelectReset()"/> </a>
							<a href="javascript:void(0)"><input type="button" class="youghu_aa2"
									value="提交" onclick="javascript:pageClick('1');" /> </a>
						</li>
					</ul>
				</div>


				<div class="gonggao_con">

					<div class="gonggao_con_nr">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="tdbg">
								<td>
									<input type="button" value="选" onclick="checkAll()"/>
								</td>
								<td>
									工号
								</td>
								<td>
									姓名
								</td>
								<td>
									账号
								</td>
								<td>
									部门
								</td>
								<!--<td>
									岗位
								</td>
								--><td>
									性别
								</td>
								<td>
									出生日期
								</td>
								<td>
									手机号码
								</td>
								<td>
									邮件地址
								</td>
								<td>
									角色
								</td>
								<td>
									状态
								</td>
							</tr>
							<s:iterator value="page.result" var="va">
								<tr>
									<td>
										<input name="userId_list" type="checkbox" value="${va.id }" />
										<!--账号启用、停用信息验证  -->
										<input type="hidden" value="${va.status }"/>&nbsp;
									</td>
									<td>
										${va.userInfo.jobNumber }&nbsp;
									</td>
									<td>
										${va.userInfo.userChineseName }&nbsp;
									</td>
									<td>
										${va.username }&nbsp;
									</td>
									<td>
										<s:iterator value="#va.stationsStatus0" var="va1">
											<s:if test="#va1.id==#va.mainStationId">
												${va1.name }
											</s:if>											
										</s:iterator>&nbsp;
									</td>
									<!--<td>
										<s:iterator value="#va.stations" var="va1">
		    							${va1.name }						    								
	    								</s:iterator>
									</td>
									--><td>
										<s:if test='#va.userInfo.sex=="1"'>男</s:if>
										<s:if test='#va.userInfo.sex=="2"'>女</s:if>&nbsp;
									</td>
									<td>
										<s:date name="#va.userInfo.birthday" format="yyyy-MM-dd" />&nbsp;
									</td>
									<td>
										${va.userInfo.mobile }&nbsp;
									</td>
									<td>
										${va.userInfo.email }&nbsp;
									</td>
									<td>
										<s:iterator value="#va.roles" var="va1" status="st">
		    								${va1.name }<s:if test="!#st.last">,</s:if>
		    							</s:iterator>&nbsp;
									</td>
									<td>
										<s:if test="%{#va.status==0}">启用</s:if>
										<s:else>停用</s:else>&nbsp;
									</td>
								</tr>
							</s:iterator>						
							<tr class="trfen">
								<td colspan="11">
									<jsp:include page="../util/part_fenye.jsp"></jsp:include>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
		
		
<!--*******************用户新增、编辑**********************************************************************************************************************-->		
		<div id="userShadeCotent" style="display:none;position:fixed;z-index:10001;_position:absolute;"></div>

<!--******************导入弹出框***********************************************************************************************-->
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/piliang_dan.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		<script type="text/javascript">
			function check(obj){
				var fileName = obj.value;
				var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
				if (suffix != ('.xls')&&suffix != ('.xlsx')) {
					parent.layer.alert("请用模板文件做为上传文件!");
					obj.parentElement.innerHTML += '';
				}
			}
		</script>
		<div id="userImportShade" class="userImportShade" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			<div class="userImportShade_d">
				<div class="userImportShade_dan">
					<div class="userImportShade_dan_title">
						<b id="title_importOrExport"></b><a href="javascript:void(0)" id="userImportShadeClose"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
								width="12" height="12" />
						</a>
					</div>
					<div class="userImportShade_dan_con">
						<ul>
							<li id="scBefore" >
								<span><a href="javascript:void(0)">下载模板</a></span>
								<p>
									注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/auther/user!downloadMouldFile.htm" style="color: blue">点击下载</a>)填写相关数据
									<br />
									特别提示：
									<br />
									1、支持的EXCEL为2003\2007\2010\2013
									<br />
									2、上传需要通过修改模板内容来完成
									<br />
									3、上传内容请严格按照模板要求来填写
									<br />
								</p>
							</li>
							<li id="scAfter" >
								<span><a href="javascript:void(0)">处理结果</a></span>
								<p>
									注：错误文件中标有不同颜色区分的内容为需要修改的内容，请修改完毕后提交这个错误文件
									<br />
									消息提示：
									<br />
								</p>
							</li>
							<li id="importWarning" >
								<span><a href="javascript:void(0)" id="importWarning_title">上传中</a></span>
								<p>
									总计：<b id="rowTotal" style="color: red; size: 24px;">0</b>条，
									成功：<b id="fuccessTotal" style="color: red;size: 24px;">0</b>条，
									失败：<b id="failureTotal" style="color: red;size: 24px;">0</b>条
									<br />
								</p>
							</li>
							<li id="importHandle" class="anniu">	
								<p>
									<!--<input name="" type="text" />&nbsp;<a style="float: r" class="userImportShade_aa1" href="javascript:void(0)">浏览</a>
									-->
									<input type="file" id="userFile" name="userFile" onchange="check(this)" />
								</p>
								<span>
									<a class="userImportShade_aa1" href="javascript:void(0)"  onclick="importUser()">上传</a>
								</span>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</div>
		

<!--******************部门岗位树(查询)*******************************************************************************************************************  -->
		<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
		</div>
		
		
<!--******************新增编辑用(IE7下 弹出窗显示位置有问题，故将其从UserAddOrEdit.jsp移至此页面)*******************************************************************************************************************  -->
		<!--******************部门岗位树(新增或者编辑,主岗)************************************************************************************************************* -->
		<div id="menuContent1" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo1" class="ztree" style="margin-top:0; width:160px;"></ul>
		</div>
		
		<!--******************部门岗位树(新增或者编辑,副岗)************************************************************************************************************* -->
		<div id="menuContent2" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo2" class="ztree" style="margin-top:0; width:160px;"></ul>
		</div>
		
		<!--******************角色树(新增或者编辑)************************************************************************************************************* -->
		<div id="menuContent3" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
			<ul id="treeDemo3" class="ztree" style="margin-top:0; width:160px;"></ul>
		</div>
		
		
	</body>
</html>

