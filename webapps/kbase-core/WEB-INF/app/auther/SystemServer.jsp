<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	    <title>系统服务设置</title>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
	    <style type="text/css">
			table.box{
			    font-family:"Courier New", Courier;
			    font-size:12px;
			    border:1px solid #C0C0C0;
			    padding : 0px;
			}
			.box th {
				color:#000000;
				border-right : 1px solid #C0C0C0;  
				border-bottom: 1px solid #C0C0C0; 
				text-align:right;
				width: 200px;
			}
			
			.box td {
			    font-family : "Courier New", Courier;
				font-size : 12px;
				color:#000000;
				border-right : 1px solid #C0C0C0;  
				border-bottom: 1px solid #C0C0C0;
				text-align:left;
			}
			
			<!-- ************************** -->
			.box_cell {
				font-family:"Courier New", Courier;
			    font-size:12px;
			    border:1px solid #C0C0C0;
			    padding : 0px;
			}
			
			.box_cell th {
				font-size:12px;
				text-align: right;
			}
			.box_cell td {
				text-align: left;
				padding-left: 5px;
			}
			
			.box_cell td [type="text"]{
				border:1px solid #C0C0C0;
				width: 100%;
			}
			
			.box_cell td input[type="button"], .box td button{
				cursor: pointer;
				margin-right: 5px;
				font-weight: bolder;
				height: 25px;
			}
		</style>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<SCRIPT type="text/javascript">
			/*知识库服务地址格式验证*/
			$.validator.addMethod("format_0", function(value, element, param) {
			    return this.optional(element) || /^((\d{1,3}.){3}\d{1,3}:\d{1,5},)*((\d{1,3}.){3}\d{1,3}:\d{1,5})$/.test(value);
			}, jQuery.validator.format("<br><font color='red'>{0}由ip及端口组成(多个地址请用,分隔),例如172.16.2.61:8771,172.16.2.62:8771</font>"));
			
			function setSettingReq(){
				$(form0).validate({
					rules : {
						"network_load.addresses" : {
							required : [ "知识库服务地址" ],
							format_0 : [ "知识库服务地址" ]
						},
						"loginLog.clear.interval" : {
							required : [ "执行周期" ],
							digits : [ "执行周期" ]
						},
						"httpServletRequestURL" : {
							required : [ "登录失败跳转地址" ]
						},
						"paramEncryption" : {
							required : [ "传递工号所用变量名称" ]
						},
						"comment_type" : {
							required : [ "纠错评论类型" ]
						}
					},
					// 验证通过时的处理
					success : function() {
				
					},
					errorPlacement : function(error, element) {
						error.appendTo(element.parent());
					},
					focusInvalid : false,
					onkeyup : false
				});
				
				var flag = $(form0).validate().form();
				if(flag){
					$("#form0").ajaxLoading('正在查询数据...');
					//测试负载服务地址
					$.post($.fn.getRootPath()+"/app/auther/system!checkServerAddress.htm",
						{'network_load.addresses':$('input[name="network_load.addresses"]').val()},function(data){
							if(data.rst){
								//发送同步请求
								var params = $("#form0").serialize();
								$.ajax({
							   		type: "POST",
							   		url : $.fn.getRootPath() + "/app/auther/system!setSettingReq.htm?"+params,
							   		async: false,
							   		dataType : 'json',
							   		success: function(data){
							   			alert(data.msg);
							   			$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/system!server.htm");
										$("#form0").submit();
							   		}
							   　});
							}else{
								alert("知识库服务地址:"+data.msg);
								$('#form0').ajaxLoadEnd();
							}
					},"json");
				}
			}
			
			//页面初始化
			$(document).ready(function(){
				//表格颜色渲染
           		$("table[class='box']").children().children("tr:even").css("background-color","#eee");
			});
		</SCRIPT>
	</head>

	<body>
		<form id="form0" action="" method="post">
			<div style="width: 100%; height: auto;" class="unsolve_xinxi_dan">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tbody>
						<tr>
							<th>
								相关服务地址：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th title="负载均衡所有地址(包括ip及端口多个地址请用逗号分隔,例如172.16.2.61:8771,172.16.2.62:8771)">
											<span style="color: red;">*</span>知识库服务地址：
										</th>
										<td>
											<input type="text" name="network_load.addresses" value='<%=PropertiesUtil.valueToString("network_load.addresses") %>' />
										</td>
									</tr>
									<tr>
										<th title="流程引擎同步地址">流程引擎同步地址：</th>
										<td>
											<input type="text" name="kbase.sync" value='<%=PropertiesUtil.valueToString("kbase.sync") %>' />
										</td>
									</tr>
									<tr>
										<th title="报表服务地址">报表服务地址：</th>
										<td>
											<input type="text" name="kbase.report" value='<%=PropertiesUtil.valueToString("kbase.report") %>' />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
								单点登录(SSO)：
								<!--单点登录(SSO)  -->
								<c:set var="isEnableLogon" value='<%=PropertiesUtil.valueToBoolean("isEnableLogon") %>'></c:set>
								<c:set var="isEncryption" value='<%=PropertiesUtil.valueToBoolean("isEncryption") %>'></c:set>
								<c:set var="httpServletRequestURL" value='<%=PropertiesUtil.valueToString("httpServletRequestURL", "http://www.baidu.com/") %>'></c:set>
								<c:set var="paramEncryption" value='<%=PropertiesUtil.valueToString("paramEncryption", "jobNumber") %>'></c:set>
								<c:set var="paramEncryptionExt" value='<%=PropertiesUtil.valueToString("paramEncryptionExt", "quicklogintoken") %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>功能是否开启：</th>
										<td>
											否<input type="radio" name="isEnableLogon" value="false" <c:if test="${isEnableLogon=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="isEnableLogon" value="true" <c:if test="${isEnableLogon=='true' }">checked="checked"</c:if>/>
										</td>
										<th>工号是否加密：</th>
										<td>
											否<input type="radio" name="isEncryption" value="false" <c:if test="${isEncryption=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="isEncryption" value="true" <c:if test="${isEncryption=='true' }">checked="checked"</c:if>/>
											
										</td>
									</tr>
									<tr>
										<th><span style="color: red;">*</span>登录失败跳转地址：</th>
										<td>
											<input type="text" name="httpServletRequestURL" value="${httpServletRequestURL }" />
										</td>
										<th><span style="color: red;">*</span>传递工号所用变量名称：</th>
										<td>
											<input type="text" name="paramEncryption" value="${paramEncryption }" />
											<input type="text" name="paramEncryptionExt" value="${paramEncryptionExt }" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
								SSO代理验证：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>sso请求参数：</th>
										<td>
											<input type="text" name="sso.params" value='<%=PropertiesUtil.valueToString("sso.params") %>' />
											<p style="color: red;">
												此参数将被“单点登录(SSO)”与“SSO代理验证”共同使用，因此在“单点登录(SSO)”需要使用时，不需要将“SSO代理验证”同时开启
											</p>
											<p>
											作用：1.区分app/login/login.htm 是否为代理请求(判断请求中是否包含第一个参数) 
											</p>
											<p style="color: red;">
												2.“单点登录(SSO)”会将这些参数保持在会话中以供调用（例如：客户方服务器信息）<br>
		  										“SSO代理验证”会将这些参数封装到sso代理请求地址中进行转发，以提供验证数据
		  										<br>
												获取方式：Map&lt;String, String&gt; ssoParams = (Map&lt;String, String&gt;) getSession().getAttribute("SSOParams");
											</p>
										</td>
									</tr>
									<tr>
										<th>功能是否开启：</th>
										<td>
											<%request.setAttribute("sso$proxy$enable", PropertiesUtil.valueToBoolean("sso.proxy.enable")); %>
											<s:select list="#{'true':'开启', 'false':'关闭'}" name="sso.proxy.enable"
												value="#request.sso$proxy$enable + ''"></s:select>
										</td>
									</tr>
									<tr>
										<th>代理请求地址：</th>
										<td>
											不进行配置，默认为知识库（kbase-core）中的app/login/login.htm 
										</td>
									</tr>
									<tr>
										<th>代理服务地址：</th>
										<td>
											<input type="text" name="sso.proxy.url" value='<%=PropertiesUtil.valueToString("sso.proxy.url", 
													"http://ip:port/kbase-sync/login.sso") %>' />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							知识搜索：
							<c:set var="mian_jsp$setting$quanwen$display" value='<%=PropertiesUtil.valueToString("mian_jsp.setting.quanwen.display", "block") %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>搜索框全文按钮是否显示：</th>
										<td>
											否<input type="radio" name="mian_jsp.setting.quanwen.display" value="none" <c:if test="${mian_jsp$setting$quanwen$display=='none' }">checked="checked"</c:if>/>
											是<input type="radio" name="mian_jsp.setting.quanwen.display" value="block" <c:if test="${mian_jsp$setting$quanwen$display=='block' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="platform" value='<%=PropertiesUtil.valueToString("platform") %>'></c:set>
										<th>维度代码：</th>
										<td>
											<input type="text" name="platform" value="${platform}" />
										</td>
									</tr>
									<tr>
										<c:set var="platform_name" value='<%=PropertiesUtil.valueToString("platform_name") %>'></c:set>
										<th>维度名称：</th>
										<td>
											<input type="text" name="platform_name" value="${platform_name }" />
										</td>
									</tr>
									<tr>
										<c:set var="swf_address" value='<%=PropertiesUtil.valueToString("swf_address") %>'></c:set>
										<th>附件预览地址：</th>
										<td>
											<input type="text" name="swf_address" value="${swf_address}" />
										</td>
									</tr>
									<tr>
										<c:set var="category_all" value='<%=PropertiesUtil.valueToBoolean("category_all") %>'></c:set>
										<th>显示所有分类：</th>
										<td>
											否<input type="radio" name="category_all" value="false" <c:if test="${category_all=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="category_all" value="true" <c:if test="${category_all=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="location_abled" value='<%=PropertiesUtil.valueToBoolean("location_abled") %>'></c:set>
										<th>启用归属地：</th>
										<td>
											否<input type="radio" name="location_abled" value="false" <c:if test="${location_abled=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="location_abled" value="true" <c:if test="${location_abled=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="brand_abled" value='<%=PropertiesUtil.valueToBoolean("brand_abled") %>'></c:set>
										<th>启用品牌：</th>
										<td>
											否<input type="radio" name="brand_abled" value="false" <c:if test="${brand_abled=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="brand_abled" value="true" <c:if test="${brand_abled=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="search_into_faq" value='<%=PropertiesUtil.valueToBoolean("search_into_faq") %>'></c:set>
										<th>搜索进入问答：</th>
										<td>
											否<input type="radio" name="search_into_faq" value="false" <c:if test="${search_into_faq=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="search_into_faq" value="true" <c:if test="${search_into_faq=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="linkStyle" value='<%=PropertiesUtil.valueToString("link_style", "1") %>'></c:set>
										<th>[link]内容是否显示中括号：</th>
										<td>
											否<input type="radio" name="link_style" value="0" <c:if test="${linkStyle=='0' }">checked="checked"</c:if>/>
											是<input type="radio" name="link_style" value="1" <c:if test="${linkStyle=='1' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="wukongEnableMerge" value='<%=PropertiesUtil.valueToString("wukong.enable_merge", "1") %>'></c:set>
										<th>搜索详情页面列表默认显示方式：</th>
										<td>
											目录合并<input type="radio" name="wukong.enable_merge" value="1" <c:if test="${wukongEnableMerge=='1' }">checked="checked"</c:if>/>
											条目显示<input type="radio" name="wukong.enable_merge" value="0" <c:if test="${wukongEnableMerge=='0' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="wukongMergeCount" value='<%=PropertiesUtil.valueToString("wukong.merge_count", "10") %>'></c:set>
										<th>目录合并知识显示最大条目数：</th>
										<td>
											<input type="text" name="wukong.merge_count" value="${wukongMergeCount }" />
											<br/>
											<font color="#FF0000">只有当搜索详情页面列表默认显示方式选择 <b>目录合并</b> 时才生效，默认显示10条，请配置大于0的数字</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
								积分管理：
								<!--积分管理  -->
								<c:set var="points$enable" value='<%=PropertiesUtil.valueToBoolean("points.enable") %>'></c:set>
								<c:set var="points$pstrategyext$priority" value='<%=PropertiesUtil.valueToString("points.pstrategyext.priority", "USER,CATALOG,ROLE,DEPT") %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>功能是否开启：</th>
										<td>
											否<input type="radio" name="points.enable" value="false" <c:if test="${points$enable=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="points.enable" value="true" <c:if test="${points$enable=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th title="积分规则仅支持USER、CATALOG、ROLE、DEPT">自定义积分规则优先级：</th>
										<td>
											<input type="text" name="points.pstrategyext.priority" value="${points$pstrategyext$priority }" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
								培训考试：
								<c:set var="learning$enable" value='<%=PropertiesUtil.valueToBoolean("learning.enable") %>'></c:set>
								<c:set var="learning$ques_check_same" value='<%=PropertiesUtil.valueToBoolean("learning.ques_check_same") %>'></c:set><%--题目管理中可以直接设置 --%>
								<c:set var="learning$recommend$cycle" value='<%=PropertiesUtil.valueToString("learning.recommend.cycle", "90") %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>功能是否开启：</th>
										<td>
											否<input type="radio" name="learning.enable" value="false" <c:if test="${learning$enable=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="learning.enable" value="true" <c:if test="${learning$enable=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th>系统推荐课程周期：</th>
										<td>
											<input type="text" name="learning.recommend.cycle" value="${learning$recommend$cycle }" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<kbs:if code="ceair">
							<tr>
								<th>
									任务考试(东航-成浩)：
								</th>
								<td>
									<table cellspacing="0" width="100%" class="box_cell">
										<tr>
											<th>功能是否开启：</th>
											<td>
												否<input type="radio" name="showpetask" value="false" <c:if test="${showpetask == null || showpetask=='false' }">checked="checked"</c:if>/>
												是<input type="radio" name="showpetask" value="true" <c:if test="${showpetask=='true' }">checked="checked"</c:if>/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</kbs:if>
						<tr>
							<th>
								业务模板：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>功能是否开启：</th>
										<td>
											<%request.setAttribute("bizTpl$enable", PropertiesUtil.valueToBoolean("bizTpl.enable")); %>
											<s:select list="#{'true':'开启', 'false':'关闭'}" name="bizTpl.enable"
												value="#request.bizTpl$enable + ''"></s:select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							用户相关：
							<c:set var="showpetask" value='<%=PropertiesUtil.valueToBoolean("showpetask", true) %>'></c:set>
							<c:set var="showindividual" value='<%=PropertiesUtil.valueToBoolean("showindividual", true) %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>首页是否显示个人中心：</th>
										<td>
											否<input type="radio" name="showindividual" value="false" <c:if test="${showindividual=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="showindividual" value="true" <c:if test="${showindividual=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th title="指定内网ip地址或者范围，内网用户如果使用不在规则以内的地址访问，将被拒绝访问">内网访问规则：</th>
										<td>
											<input type="text" name="visit.Intranet" value='<%=PropertiesUtil.valueToString("visit.Intranet", "(^(172)|^(127)).\\d{1,3}.\\d{1,3}.\\d{1,3}") %>' />
										</td>
									</tr>
									<tr>
										<th>是否启用验证码：</th>
										<td>
											<c:set var="captchaOpen" value='<%=PropertiesUtil.valueToBoolean("captchaOpen") %>'></c:set>
											否<input type="radio" name="captchaOpen" value="false" <c:if test="${captchaOpen=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="captchaOpen" value="true" <c:if test="${captchaOpen=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th>密码：</th>
										<td>
											<table cellspacing="0" width="100%" class="box_cell">
												<tr>
													<th>首次登陆是否强制修改默认密码：</th>
													<td>
														<%request.setAttribute("password$force_change", PropertiesUtil.valueToString("password.force_change", "0")); %>
														<s:select list="#{'1':'需要', '0':'不需要'}" name="password.force_change"
															value="#request.password$force_change"></s:select>
														（默认密码：123）
													</td>
												</tr>
												<tr>
													<th>复杂度：</th>
													<td>
														<%request.setAttribute("password$force_rank", PropertiesUtil.valueToString("password.force_rank", "0")); %>
														<s:select list="#{'0':'仅长度校验','1':'长度、字母（小写）、数字','2':'长度、字母（不区分大小写）、数字',
															'3':'长度、字母（不区分大小写）、数字、特殊字符'}" name="password.force_rank"
															value="#request.password$force_rank"></s:select>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<th>纠错发起是否选择审批人：</th>
										<td>
											<c:set var="activate_auditor" value='<%=PropertiesUtil.valueToBoolean("errcorrect.activate_auditor", true) %>'></c:set>
											否<input type="radio" name="errcorrect.activate_auditor" value="false" <c:if test="${activate_auditor=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="errcorrect.activate_auditor" value="true" <c:if test="${activate_auditor=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th>是否影藏顶部导航栏：</th>
										<td>
											<%request.setAttribute("banner_collapse", PropertiesUtil.valueToString("banner_collapse", "0")); %>
											<s:select list="#{'1':'需要', '0':'不需要'}" name="banner_collapse"
												value="#request.banner_collapse"></s:select>
										</td>
									</tr>
									<tr>
										<th>新建用户默认主题：</th>
										<td>
											<%request.setAttribute("system$settings$userTheme", PropertiesUtil.valueToString("system.settings.userTheme", "blue")); %>
											<s:select list="#{'red':'红色', 'blue':'蓝色', 'green':'绿色'}" name="system.settings.userTheme"
												value="#request.system$settings$userTheme"></s:select>
										</td>
									</tr>
									<tr>
										<th>是否启用埋点日志记录：</th>
										<td>
											<c:set var="dataembed$enable" value='<%=PropertiesUtil.valueToBoolean("dataembed.enable") %>'></c:set>
											否<input type="radio" name="dataembed.enable" value="false" <c:if test="${dataembed$enable=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="dataembed.enable" value="true" <c:if test="${dataembed$enable=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							公告设置：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>公告置顶数量：</th>
										<td>
											<input type="text" name="notice.to_top_count" value="<%=PropertiesUtil.valueToString("notice.to_top_count", "5") %>" />
										</td>
									</tr>
									<tr>
										<th>是否激活便签功能：</th>
										<td>
											<c:set var="activate_note" value='<%=PropertiesUtil.valueToBoolean("notice.activate_note") %>'></c:set>
											否<input type="radio" name="notice.activate_note" value="false" <c:if test="${activate_note=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="notice.activate_note" value="true" <c:if test="${activate_note=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							纠错设置：
							<c:set var="leafnodeorobject" value='<%=PropertiesUtil.valueToString("leafnodeorobject", "object") %>'></c:set>
							<c:set var="satisfaction_type" value='<%=PropertiesUtil.valueToString("satisfaction_type", "满意,5;一般,3;不满意,0") %>'></c:set>
							<c:set var="comment_type" value='<%=PropertiesUtil.valueToString("comment_type", "答案错误,答案过期,答案遗漏,其他") %>'></c:set>					
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>报表显示实例或知识分类叶子节点：</th>
										<td>
											实例:<input type="radio" name="leafnodeorobject" value="object" <c:if test="${leafnodeorobject=='object' }">checked="checked"</c:if>/>
											叶子节点:<input type="radio" name="leafnodeorobject" value="leafnode" <c:if test="${leafnodeorobject=='leafnode' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th title="暂只支持三种类型"><span style="color: red;">*</span>满意度：</th>
										<td>
											<input type="text" name="satisfaction_type" value='${satisfaction_type}' />
										</td>
									</tr>
									<tr>
										<th title="每个类型以分号隔开"><span style="color: red;">*</span>评论类型：</th>
										<td>
											<input type="text" name="comment_type" value='${comment_type}' />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							页面设置：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>知识详情页面：</th>
										<td>
											<c:set var="detailPage" value='<%=PropertiesUtil.valueToString("page.search_detail", "detail")%>'></c:set>
											detail<input type="radio" name="page.search_detail" value="detail" <c:if test="${detailPage=='detail' }">checked="checked"</c:if>/>
											detail_bootstrap<input type="radio" name="page.search_detail" value="detail_bootstrap" disabled="disabled" <c:if test="${detailPage=='detail_bootstrap' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<c:set var="catePage" value='<%=PropertiesUtil.valueToString("page.category", "category") %>'></c:set>
										<th>分类展示页面：</th>
										<td>
											category<input type="radio" name="page.category" value="category" <c:if test="${catePage=='category' }">checked="checked"</c:if>/>
											category_view<input type="radio" name="page.category" value="category_view" <c:if test="${catePage=='category_view' }">checked="checked"</c:if>/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
							搜索日志：
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th>日志读取间隔：</th>
										<td>
										<c:set var="read_log_frequency" value='<%=PropertiesUtil.valueToString("read_log_frequency", "10")%>'></c:set>
											<input type="text" name="read_log_frequency" value="${read_log_frequency}" />
										</td>
									</tr>
									<tr>
										<c:set var="export_max_count" value='<%=PropertiesUtil.valueToString("export_max_count", "100000") %>'></c:set>
										<th>报表最大导出数：</th>
										<td>
											<input type="text" name="export_max_count" value="${export_max_count}" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>
								登陆日志：
								<c:set var="loginLog$clear" value='<%=PropertiesUtil.valueToBoolean("loginLog.clear", true) %>'></c:set>
								<c:set var="loginLog$clear$interval" value='<%=PropertiesUtil.valueToString("loginLog.clear.interval", "90") %>'></c:set>
							</th>
							<td>
								<table cellspacing="0" width="100%" class="box_cell">
									<tr>
										<th title="功能是否开启，建议开启">登陆日志是否定时清理：</th>
										<td>
											否<input type="radio" name="loginLog.clear" value="false" <c:if test="${loginLog$clear=='false' }">checked="checked"</c:if>/>
											是<input type="radio" name="loginLog.clear" value="true" <c:if test="${loginLog$clear=='true' }">checked="checked"</c:if>/>
										</td>
									</tr>
									<tr>
										<th title="执行周期(单位：天)"><span style="color: red;">*</span>执行周期(单位：天)：</th>
										<td>
											<input type="text" name="loginLog.clear.interval" value="${loginLog$clear$interval }" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<kbs:if code="gzyd">
							<tr>
								<th>
									广州移动相关设置：
									<c:set var="category_tag" value='<%=PropertiesUtil.valueToBoolean("category_tag") %>'></c:set>
									<c:set var="dept_tag" value='<%=PropertiesUtil.valueToBoolean("dept_tag") %>'></c:set>
								</th>
								<td>
									<table cellspacing="0" width="100%" class="box_cell">
										<tr>
											<th>功能是否开启：</th>
											<td>
												否<input type="radio" name="category_tag" value="false" <c:if test="${category_tag=='false' }">checked="checked"</c:if>/>
												是<input type="radio" name="category_tag" value="true" <c:if test="${category_tag=='true' }">checked="checked"</c:if>/>
											</td>
										</tr>
										<tr>
											<th title="是否可以发送短信的平台标识">送短信的平台标识：</th>
											<td>
												<input type="text" name="gzyd_huawei.sms_dim" value='<%=PropertiesUtil.valueToString("gzyd_huawei.sms_dim") %>' />
											</td>
										</tr>
										<tr>
											<th title="归档服务地址">归档服务地址：</th>
											<td>
												<input type="text" name="gzyd_huawei.achiveNotice.wsdl" value='<%=PropertiesUtil.valueToString("gzyd_huawei.achiveNotice.wsdl") %>' />
											</td>
										</tr>
										<tr>
											<th title="短信服务地址">短信服务地址：</th>
											<td>
												<input type="text" name="gzyd_huawei.kbsSMSRequest.wsdl" value='<%=PropertiesUtil.valueToString("gzyd_huawei.kbsSMSRequest.wsdl") %>' />
											</td>
										</tr>
										
										<tr>
											<th>是否显示顶级部门：</th>
											<td>
												否<input type="radio" name="dept_tag" value="false" <c:if test="${dept_tag=='false' }">checked="checked"</c:if>/>
												是<input type="radio" name="dept_tag" value="true" <c:if test="${dept_tag=='true' }">checked="checked"</c:if>/>
											</td>
										</tr>
										<tr>
											<th>显示的顶级部门：</th>
											<td>
												<input type="text" name="dept_name_show" value='<%=PropertiesUtil.valueToString("dept_name_show") %>' />
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
						</kbs:if>
						<kbs:if code="gz12345">
							<tr>
								<th>
									广州12345：
									<c:set var="loginLog$clear" value='<%=PropertiesUtil.valueToBoolean("loginLog.clear", true) %>'></c:set>
									<c:set var="loginLog$clear$interval" value='<%=PropertiesUtil.valueToString("loginLog.clear.interval", "90") %>'></c:set>
								</th>
								<td>
									<table cellspacing="0" width="100%" class="box_cell">
										<tr>
											<th>短信：</th>
											<td>
												<table cellspacing="0" width="100%" class="box_cell">
													<tr>
														<th>短信请求地址：</th>
														<td>
															<input type="text" name="gz12345.sms.url" value='<%=PropertiesUtil.valueToString("gz12345.sms.url") %>' />
														</td>
													</tr>
													<tr>
														<th>企业id：</th>
														<td>
															<input type="text" name="gz12345.sms.userid" value='<%=PropertiesUtil.valueToString("gz12345.sms.userid") %>' />
														</td>
													</tr>
													<tr>
														<th>企业账号：</th>
														<td>
															<input type="text" name="gz12345.sms.account" value='<%=PropertiesUtil.valueToString("gz12345.sms.account") %>' />
														</td>
													</tr>
													<tr>
														<th>企业密码：</th>
														<td>
															<input type="text" name="gz12345.sms.password" value='<%=PropertiesUtil.valueToString("gz12345.sms.password") %>' />
														</td>
													</tr>
													
												</table>
											</td>
										</tr>
										<tr>
											<th>工单：</th>
											<td>
												<table cellspacing="0" width="100%" class="box_cell">
													<tr>
														<th>账号：</th>
														<td>
															<input type="text" name="gz12345.relate.username" value='<%=PropertiesUtil.valueToString("gz12345.relate.username") %>' />
														</td>
													</tr>
													<tr>
														<th>密码：</th>
														<td>
															<input type="text" name="gz12345.relate.password" value='<%=PropertiesUtil.valueToString("gz12345.relate.password") %>' />
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</kbs:if>
					</tbody>
				</table>
				<br>
				<div style="height: 28px; width: 100%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" onclick="javascript:setSettingReq();" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
