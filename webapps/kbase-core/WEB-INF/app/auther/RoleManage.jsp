<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>知识库-角色管理</title>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/juese.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/Role.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/RoleRelationUser.js"></script>
</head>

<body>

<!--******************内容开始***********************-->
<input type="hidden" id="zyzsdId" value="${zyzsdId}" />
<div class="content_right_bottom">
  <div class="juese">
    <div class="juese_left">
      <div class="juese_left_title">
      	<a href="javascript:void(0);" id="deleteRole">删除角色</a>
      	<a href="javascript:void(0);" id="saveRole">新建角色</a>
      	<a href="javascript:void(0);" id="relationUser">关联用户</a>
      </div>
      <table width="100%" height="24px;" border="0" cellspacing="0" cellpadding="0">
      	<tr class="des_role" >
				<td  width="40%" align="center" style="color:white;" >
			角色名
				</td>
				<td class="ts1" width="60%" style="color:white;" >
					角色描述
				</td>
			</tr>
      	</table>
      <div class="juese_left_con">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <s:iterator var="role" value="#request.roles" status="i">
          	<tr id="${role.id}" class="">
				<td  width="40%" align="center">
				<input id="role_id" type="hidden" value="${role.id}" />
					${role.name}
				</td>
				<td class="ts1" width="60%">
					${role.descContent}
				</td>
			</tr>
		</s:iterator>
        </table>
      </div>
    </div>
    <div class="juese_right">
      <div class="juese_right_bg">
        <div class="juese_right1">
          <div class="juese_right1_title"> <a class="title" href="javascript:void(0);">角色描述</a> </div>
          <div class="juese_content" style="display:none;">
          <div class="juese_right1_con">
            <ul>
               
              <li>
                <p>角色名</p>
                <b>
                <input name="" id="rolename" type="text" />
                </b> </li>
              <li>
                <p>描述</p>
                <b>
                <textarea name="" id="roledesc" cols="" rows=""></textarea>
                </b> </li>
             <li><a href="javascript:void(0);">
                <input type="button" id="save_roleinfo" value="保存" />
                </a></li>
            </ul>
          </div>
          </div>
        </div>
        <div class="juese_right1">
          <div class="juese_right3_title"> <a class="title" href="javascript:void(0);">功能权限</a> </div>
          <div  class="juese_content" style="display:none;">
          <div class="juese_right2_title1">
            <ul class="perm">
              <li><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ico4.jpg" width="15" height="15" class="onepermtree"/></a><a href="javascript:void(0);" class="onepermtree">折叠所有节点</a></li>
          	  <li><a href="javascript:void(0);" id=""><input type="button" id="save_perms_btn" class="save_perms_btn" value="保存" /></a></li>
            </ul>
          </div>
          <div class="juese_right2_con" style="overflow-x:auto;">
           	<ul id="systemTree" class="ztree" style="width: 100%; height: 100%;"></ul>
          </div>
          </div>
        </div>
        <div class="juese_right1">
          <div class="juese_right3_title"> <a class="title" href="javascript:void(0);">目录范围</a><a></a> </div>
          <div  class="juese_content" style="display:none;">
          	<div class="juese_right2_title1">
				<ul class="object">
				<li><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ico1.jpg" width="15" height="15" class="qikongcatalog" /></a><a href="javascript:void(0);" class="qikongcatalog">清空</a></li>
				<li><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ico2.jpg" width="16" height="16" class="refleshcatalog" /></a><a href="javascript:void(0);" class="refleshcatalog">刷新</a></li>
				<li class="ssuo"><a href="javascript:void(0);" id="catagoryButton"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ss.jpg" width="23" height="22" /></a><input id="catagoryInput" name="" type="text" /></li>
				<li><a href="javascript:void(0);" id=""><input type="button" id="save_catalog_btn" class="save_catalog_btn" style="margin-left:5px;" value="保存" /></a></li>
				</ul>
				
         	</div>
          	<div class="juese_right2_con">
          		<ul id="catalogTree" class="ztree" style="width: 100%;height: 100%;"></ul>
          	</div>
          </div>
        </div>
        <div class="juese_right1">
          <div class="juese_right3_title"> <a class="title" href="javascript:void(0);">岗位知识</a> </div>
          <div class="juese_content" style="display:none;">
          <div class="juese_right2_title1">
			<ul class="object">
			<li><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ico1.jpg" width="15" height="15" class="qikongobject" /></a><a href="javascript:void(0);" class="qikongobject">清空</a></li>
			<li><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ico2.jpg" width="16" height="16" class="refleshobject" /></a><a href="javascript:void(0);" class="refleshobject">刷新</a></li>
			<li class="ssuo"><a href="javascript:void(0);" id="treeButton"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/juese/juese_ss.jpg" width="23" height="22" /></a><input id="treeInput" name="" type="text" /></li>
			<li><a href="javascript:void(0);" id=""><input type="button" id="save_objects_btn" class="save_objects_btn" style="margin-left:5px;" value="保存" /></a></li>
			</ul>
          </div>
          <div class="juese_right2_con">
				<ul id="roleObjectTree" class="ztree" style="width: 100%;height: 100%;"></ul>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--******************内容结束***********************-->

</body>
</html>
