<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/shezhi.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/IndexSetting.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    function selectTag(obj,tag,divtag){
       $("#tags li").removeClass();
       $("#"+tag+"").addClass("selectTag");
       $(".shezhi_con_nr").css("display","none");
       $("#"+divtag+"").css("display","block");
    }
</script>
</head>


<body>
<ul id=tags>
        <li class="selectTag" id="shezhitag"><a href="javascript:void(0)" onclick="selectTag(this,'shezhitag','shezhi')">主题设置</a> </li>
        <li id="colorselecttag"><a href="javascript:void(0)" onclick="selectTag(this,'colorselecttag','colorselect')">主题颜色</a> </li>
</ul>
<div id="tagContent">
  <div class="tagContent selectTag" id="tagContent0">
    <div class="user_settings">
    
      <div class="shezhi_con_nr" id="colorselect" style="display: none;">
      <div class="shezhi_left">
        <ul>
          <li class="hongse"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/red_bg.jpg" /></li>
          <li class="hongse1"><span>红色</span> </li>
          <li>
            <label>
              <s:if test="#session.session_user_key.userInfo.userTheme == 'red'">
				<input theme_value="red" type="radio" name="RadioGroup1" value="红色" id="RadioGroup1_0" checked="checked"/>
			</s:if>
			<s:else>
				<input theme_value="red" type="radio" name="RadioGroup1" value="红色" id="RadioGroup1_0"/>
			</s:else>
            </label>
          </li>
        </ul>
      </div>
      <div class="shezhi_center">
        <ul>

          <li class="hongse"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/bule_bg.jpg"></img></li>
          <li class="hongse1"><span>蓝色</span> </li>
          <li>
            <s:if test="#session.session_user_key.userInfo.userTheme == 'blue'">
				<input theme_value="blue" type="radio" name="RadioGroup1" value="蓝色" id="RadioGroup1_0" checked="checked"/>
			</s:if>
			<s:else>
				<input theme_value="blue" type="radio" name="RadioGroup1" value="蓝色" id="RadioGroup1_0"/>
			</s:else>
          </li>
        </ul>
      </div>
      <div class="shezhi_right">
        <ul>

          <li class="hongse"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/green_bg.jpg"></img></li>
          <li class="hongse1"><span>绿色</span> </li>
          <li>
            <label>
                <s:if test="#session.session_user_key.userInfo.userTheme == 'green'">
					<input theme_value="green" type="radio" name="RadioGroup1" value="绿色" id="RadioGroup1_0" checked="checked"/>
				</s:if>
				<s:else>
					<input theme_value="green" type="radio" name="RadioGroup1" value="绿色" id="RadioGroup1_0"/>
				</s:else>            
		   </label>
          </li>
        </ul>
      </div>
      <div class="clear"></div>
      </div>
            <div class="shezhi_con_nr" style="margin:40px 0 0 15%;width:auto;min-width:800px;" id="shezhi">
        <div style="width:50%;height:auto;float:left;">
         <ul id="sortable1" class="connectedSortable">
			<s:iterator var="uip" value="#request.userIndexPanels">
				<s:if test="#uip.isLeft == 0">
					<li panelId="${indexPanel.id}">${indexPanel.title}</li>
				</s:if>
			</s:iterator>
		</ul>
        </div>
        <div style="width:10%;float:left;">
					
		<ul id="sortable2" class="connectedSortable">
			<s:iterator var="uip" value="#request.userIndexPanels">
				<s:if test="#uip.isLeft == 1">
					<li panelId="${indexPanel.id}">${indexPanel.title}</li>
				</s:if>
			</s:iterator>
		</ul>
		</div>
      </div>
      
      <div class="clear"></div>
      
    </div>
  </div>
              <div class="user_settingsbtn themebg">
              <input class="an2" type="button" value="重置" />
              <input class="an1" type="button" value="保存" />
            </div>
    </div>
  </div>
</div>
</body>
</html>
