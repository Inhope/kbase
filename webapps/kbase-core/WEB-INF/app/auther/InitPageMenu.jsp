<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>菜单管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">	
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+"/app/auther/menu!getTreeMenu.htm"
				},				
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},callback: {			
					onClick: zTreeOnClick
				}				
			};
			
			//节点点击
			function zTreeOnClick(event, treeId, treeNode){	
				$(".xinxi_con_nr_right").css("display","block");//显示主区
				$(".zuzhi").css("display","block");//显示主区
				$("#content").css("display","none");//显示影藏内容区
				if(treeNode.id=='0'){
					//主节点的时候不进行编辑删除操作
					$("#editMenu").css("display","none");
					$("#delMenu").css("display","none");						
				}else{
					$("#editMenu").css("display","block");
					$("#delMenu").css("display","block");
				}			
			}
		
			var setting1 = {
				check: {
					enable: true,
					chkStyle: "radio",
					radioType: "all"
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onCheck: zTreeOnCheck1,
					beforeCheck: zTreeBeforeCheck1							
				}
			};
			
			//勾选前验证
			function zTreeBeforeCheck1(treeId, treeNode){
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");				  
				var node = treeObj.getSelectedNodes()[0];
				//验证选择的节点是不是当前选中节点本身
				if(node.id==treeNode.id){
					alert("父菜单不能为当前菜单本身!");
					return false;
				}					
				while(treeNode.getParentNode()!=null){
					//验证选择的节点是不是当前选中节点下级
					treeNode = treeNode.getParentNode();
					if(node.id==treeNode.id){
						alert("父菜单不能为当前菜单的子菜单!");
						return false;
					}
				}
				return true;		
			}
					
			//勾选
			function zTreeOnCheck1(e, treeId, treeNode) {
				$("#name_pmenu").attr("value", treeNode.name);
				$("#id_pmenu").attr("value",  treeNode.id);
			}					
					
			function showMenu1() {			
				var cityObj = $("#name_pmenu");
				var cityOffset = $("#name_pmenu").offset();
				$("#menuContent1").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", onBodyDown1);
			}
			function hideMenu1() {
				$("#menuContent1").fadeOut("fast");
				$("body").unbind("mousedown", onBodyDown1);
			}
			function onBodyDown1(event) {
				if (!(event.target.id == "menuContent1" || $(event.target).parents("#menuContent1").length>0)) {
					hideMenu1();
				}
			}
			
			//******************************************************************************************************
			//编辑、新建
			function addOrEditMenu(status){				
				//显示编辑区
				$("#content").css("display","block");
				
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var node = treeObj.getSelectedNodes()[0];//当前选中节点				
				if(node==null||node==''){					
					alert("请先选择节点！");
					return;
				}
												
				//数据初始化
				resetMenu(status,node);
				
										
				//绑定保存时间
				$("#saveMenu").unbind("click");
				$("#saveMenu").click(function (){
						saveMenu(status,node);		
				});
				
				//绑定重置信息
				$("#resetMenu").unbind("click");
				$("#resetMenu").click(function (){
						resetMenu(status,node);		
				});
									
			}
			
			//重置
			function resetMenu(status,node){
				$("input[id$='_menu']").each(function (){
					$(this).attr("value","");
				});				
				$("#name_pmenu").unbind("click");//事件解绑
				
								
				if(status=="edit"){
					//编辑赋值
					if(node.pId!=null){
						var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
						var pNode = treeObj.getNodeByParam("id",node.pId,null);
						$("#id_pmenu").attr("value",pNode.id);
						$("#name_pmenu").attr("value",pNode.name);
						//回显父节点树
						//编辑页面菜单树初始化
						$.fn.zTree.init($("#treeDemo1"), setting1, $.fn.zTree.getZTreeObj("treeDemo").getNodes());
						var treeObj = $.fn.zTree.getZTreeObj("treeDemo1");
						treeObj.checkNode(treeObj.getNodeByParam("id",node.pId, null),true,false);
					}else{
						$("#id_pmenu").attr("value","");
						$("#name_pmenu").attr("value","");
					}
					
					//绑定父节点树显示事件														
					$("#name_pmenu").click(function (){ showMenu1();});					
					$("#id_menu").attr("value",node.id!=null?node.id:'');
					$("#name_menu").attr("value",node.name!=null?node.name:'');								
					$("#key_menu").attr("value",node.key!=null?node.key:'');
					$("#path_menu").attr("value",node.path!=null?node.path:'');
					$("#level_menu").attr("value",node.grade!=null?node.grade:'');
					$("#order_menu").attr("value",node.order!=null?node.order:'');				
					$("#flag_menu").find("option[value='"+node.flag+"']").attr("selected",true);
					
				}else{
					//新建赋值
					$("#id_pmenu").attr("value",node.id!=null?node.id:'');
					$("#name_pmenu").attr("value",node.name!=null?node.name:'');		
				}
			}
			
			//保存
			function saveMenu(status,node){
				if($("#name_menu").val()==''){
					alert("当前菜单不能为空！");
					return;
				}
				
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				if($("#key_menu").val()==''){
					alert("菜单简记不能为空!");
					return;
				}else{
					var node_key = treeObj.getNodeByParam("key",$("#key_menu").val(), null);
					if((node_key!=null&&status=="add")
						||(node_key!=null&&status=="edit"&&node_key.id!=node.id)){
						alert("菜单简记("+node_key.key+")已存在!");
						return;
					}
				}
				
				
				document.getElementById("saveMenu").disabled = true;			
				$.post($.fn.getRootPath()+"/app/auther/menu!addOrEditMenu.htm",
					{
						'id_menu':$("#id_menu").val(),
						'name_menu':$("#name_menu").val(),
						'id_pmenu':$("#id_pmenu").val()!='0'?$("#id_pmenu").val():'',
						'key_menu':$("#key_menu").val(),
						'path_menu':$("#path_menu").val(),
						'level_menu':$("#level_menu").val(),
						'order_menu':$("#order_menu").val(),
						'flag_menu':$("#flag_menu").val()
					},
					function(data){					
						if(data!=null&&data!=''&&data!='0'){
							alert("操作成功!");
							//var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
							if(status=="edit"){//节点编辑
								var pId = node.pId;
								
								node.name=data.name;
								node.pId = data.pId;
								node.pName = data.pName;
								node.key = data.key;
								node.path = data.path;
								node.grade = data.grade;
								node.order = data.order;
								node.flag = data.flag;					
								if(pId==data.pId){//跟新节点									
									treeObj.updateNode(node);
								}else{//父节点发生变化
									treeObj.moveNode(treeObj.getNodeByParam("id",data.pId,null),node, "inner");
								}							
																			
							}else{//节点新增							
								treeObj.addNodes(treeObj.getNodeByParam("id",data.pId,null),data);						
							}
							$("#content").css("display","none");//影藏编辑区											
						}else{
							alert("保存失败!");
						}
						
						document.getElementById("saveMenu").disabled = false;	
					},
					"json"
				);
			}
			
			//删除节点
			function delMenu(){			
				var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
				var node = treeObj.getSelectedNodes()[0];		
				if(node==null){
					alert("请选择菜单!");
					return;
				}else{
					if(node.children!=null&&node.children.length>0){
						alert("请先删除子菜单!");
						return;
					}
					if(confirm("确定删除" + node.name + "吗？")){
						$.post($.fn.getRootPath()+"/app/auther/menu!delMenu.htm",
							{'id_menu':node.id},
							function(data){					
								if(data=='1'){
									alert("删除成功!", -1);
									treeObj.removeNode(node);
									
									$(".zuzhi").css("display","none");				
								}else{
									alert("删除失败!", -1);
								}				
							},"html");					
					};
				}
				
			}
			
			$(document).ready(function(){
				//菜单树初始化
				$.fn.zTree.init($("#treeDemo"), setting);
				
				//编辑页面菜单树初始化
				$.fn.zTree.init($("#treeDemo1"), setting1);	
				
			});	
		</script>
		
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xinxi_con">
				<div class="xinxi_con_nr">
					<div class="xinxi_con_nr_left">
						<div id="menuContent" class="menuContent">
							<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
						</div>
					</div>								
					<div class="xinxi_con_nr_right" style="display: none;">
						<div class="zuzhi" style="height: 385px">
							<ul>							
								<li class="anniu">
									<a href="#"><input type="button" id="addMenu" class="fkui_aa2"
											value="新建" onclick="addOrEditMenu('add')"/></a>
									<a href="#"><input type="button" id="editMenu" class="fkui_aa2"
											value="编辑" onclick="addOrEditMenu('edit')"/></a>
									<a href="#"><input type="button" id="delMenu" class="fkui_aa2"
											value="删除" onclick="delMenu()"/></a>
								</li>
							</ul>
							<ul id="content" style="display: none;">
								<li>
									<span>所属菜单：</span>
									<p>
										<input id="name_pmenu" type="text" readonly="readonly"/>
										<input id="id_pmenu" type="hidden" />
									</p>
								</li>
								<li>
									<span>当前菜单：</span>
									<p>
										<input id="name_menu" type="text" />
										<input id="id_menu" type="hidden" />
									</p>
								</li>
								<li>
									<span>菜单简记：</span>
									<p>
										<input id="key_menu" type="text" />
									</p>
								</li>
								<li>
									<span>菜单路径：</span>
									<p>
										<input id="path_menu" type="text" />
									</p>
								</li>
								<li>
									<span>菜单等级：</span>
									<p>
										<input id="level_menu" type="text" />
									</p>
								</li>
								<li>
									<span>菜单顺序：</span>
									<p>
										<input id="order_menu" type="text" />
									</p>
								</li>
								<li>
									<span>菜单状态：</span>
									<p>
										<s:select id="flag_menu" list="#{'1':'启用','0':'停用'}" listKey="key" listValue="value"></s:select>
									</p>
								</li>
								<li class="anniu">
									<a href="#"><input type="button" class="fkui_aa1"
											value="重置" id="resetMenu"/> </a>
									<a href="#"><input type="button" class="fkui_aa2"
											value="保存" id="saveMenu"/> </a>
								</li>
							</ul>
						</div>


					</div>
				</div>
			</div>



		</div>


		<!--******************内容结束***********************-->
		<!--******************菜单树(新增或者编辑)************************************************************************************************************* -->
		<div id="menuContent1" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
			<ul id="treeDemo1" class="ztree" style="margin-top:0; width:220px;height:300px;overflow-y:auto;overflow-x:auto;"></ul>
		</div>

	</body>
</html>
