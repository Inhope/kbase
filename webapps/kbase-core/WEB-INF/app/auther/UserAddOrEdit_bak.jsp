<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinjian_dan.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">	
	//**************用户新增、编辑部门岗位树************	
	//主岗		
	var setting1 = {
		check: {
			enable: true,
			chkStyle: "radio",
			radioType: "all"
		},
		view: {
			dblClickExpand: false
		},
		data: {
			simpleData: {
			enable: true
		}
		},
		callback: {
			onCheck: zTreeOnCheck1									
		}
	};
	
	//主岗勾选
	function zTreeOnCheck1(e, treeId, treeNode) {
		$("#stationName_main_user").attr("value", treeNode.name);
		$("#stationId_main_user").attr("value", treeNode.id);
	}
	
	//副岗勾选
	function zTreeOnCheck2(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo2"),
		nodes = zTree.getCheckedNodes(true);
		var depNames = "";
		var depIds = "";
		for (var i=0, l=nodes.length; i<l; i++) {
			depNames += nodes[i].name + ",";
			depIds +=  nodes[i].id + ",";
		}
				
		if (depNames.length > 0 ) depNames = depNames.substring(0, depNames.length-1);
		if (depIds.length > 0 ) depIds = depIds.substring(0, depIds.length-1);
		$("#stationName_deputy_user").attr("value", depNames);
		$("#stationId_deputy_user").attr("value", depIds);
	}
	
	//角色勾选
	function zTreeOnCheck3(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo3"),
		nodes = zTree.getCheckedNodes(true);
		var roleNames = "";
		var roleIds = "";
		for (var i=0, l=nodes.length; i<l; i++) {
			roleNames += nodes[i].name + ",";
			roleIds +=  nodes[i].id + ",";
		}
				
		if (roleNames.length > 0 ) roleNames = roleNames.substring(0, roleNames.length-1);
		if (roleIds.length > 0 ) roleIds = roleIds.substring(0, roleIds.length-1);
		$("#roleNames_user").attr("value", roleNames);
		$("#roleIds_user").attr("value", roleIds);
	}
	
</script>

<script type="text/javascript">	
			
	//内容重置
	function userReset(){
		$("input[id$='_user']").each(function(){
			$(this).attr("value","");
		});
		$("select[id$='_user']").each(function(){
			$(this)[0].selectedIndex = '';
		});
				
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo1");
		var nodes = treeObj.getCheckedNodes(true);
		for(var i=0;i<nodes.length;i++){
			treeObj.checkNode(nodes[i],false,false);
		}
		
		treeObj = $.fn.zTree.getZTreeObj("treeDemo2");
		nodes = treeObj.getCheckedNodes(true);
		for(var i=0;i<nodes.length;i++){
			treeObj.checkNode(nodes[i],false,false);
		}
		
		treeObj = $.fn.zTree.getZTreeObj("treeDemo3");
		nodes = treeObj.getCheckedNodes(true);
		for(var i=0;i<nodes.length;i++){
			treeObj.checkNode(nodes[i],false,false);
		}
		
				
		$("#errorMessage").html("");//清掉提示信息
		$("#birthday_user").next().find("input:eq(0)").val('');//清掉用户生日
		$("input[name='userModel.userInfo.birthday']:eq(0)").val('');//清掉用户生日(隐藏文本域)
				
	}
			
	//用户新增、编辑		
	function addOrEditUser(){
		var bl = true;
		var jobNumber_user =  $("#jobNumber_user").val();
		var userChineseName_user =  $("#userChineseName_user").val();
		var username_user =  $("#username_user").val();
		var stationId_main_user =  $("#stationId_main_user").val();
		var stationId_deputy_user =  $("#stationId_deputy_user").val();
		var roleIds_user =  $("#roleIds_user").val();
		var email_user =  $("#email_user").val();
		var mobile_user =  $("#mobile_user").val();			
				
		var errorMessage = $("#errorMessage");				
		if(jobNumber_user==''){
			$(errorMessage).html("工号不能为空!");
			return;
		}
		if(userChineseName_user==''){
			$(errorMessage).html("姓名不能为空!");
			return;
		}
		if(username_user==''){
			$(errorMessage).html("账号不能为空!");
			return;
		}		
		if(stationId_main_user==''){
			$(errorMessage).html("主岗不能为空!");
			return;
		}
		if(roleIds_user==''){
			$(errorMessage).html("角色不能为空!");
			return;
		}
		if(email_user!=''){//邮箱不为空验证格式
			var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!filter.test(email_user)) {
				$(errorMessage).html("邮箱不符合标准!");
				return;
			}
		}
		if(mobile_user!=''){//手机号码不为空验证格式
			var filter  = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
			if (!filter.test(mobile_user)) {
				$(errorMessage).html("手机号码不符合标准!");
				return;
			}
		}

		var bl = true;			
		$.post(
			$.fn.getRootPath()+"/app/auther/user!checkData.htm",
			{'jobNumber':jobNumber_user,'jobNumber2':$("#jobNumber2_user").val(),
			 'username':username_user,'username2':$("#username2_user").val()},
			function(data){
				if(data!=null&&data!=''){
					data = data.split(",");			
					if(data[0]!='0'){
						$(errorMessage).html("工号已存在!");
						bl= false;
					}
					
					if(data[1]!='0'){
						$(errorMessage).html("账号已存在!");
						bl = false;
					}
							
					if(bl){
						$(errorMessage).html("");
						
						$('#body').ajaxLoading('正在保存数据...');
						
						var params=$("#form1").serialize();
						$.ajax({
					   		type: "POST",
					   		url : $.fn.getRootPath() + "/app/auther/user!addOrEditUserDo.htm?"+params,
					   		async: false,
					   		dataType : 'text',
					   		success: function(msg){
					   			parent.layer.alert(msg, -1);
					   			closeShade("userShadeCotent");
					   			pageClick('1');
					   		}
					   　});
												
						//$("#form1").attr("action", $.fn.getRootPath()+"/app/auther/user!addOrEditUserDo.htm");
						//$("#form1").submit();
					}
				}
		},"html");
			
		
	}
</script>

<script type="text/javascript">	

	function expandParentNode(treeNode,treeObj){
		treeNode = treeNode.getParentNode();
		if(treeNode!=null){
			treeObj.expandNode(treeNode, true, false, true);
			expandParentNode(treeNode,treeObj);
			treeNode = treeNode.getParentNode();
		}
	}
	
//***************页面初始化*******************	
	$(document).ready(function(){
					
		//主岗树初始化	****************			
		var ss = zNodes.concat(${json_stations});
		$.fn.zTree.init($("#treeDemo1"), setting1, ss);
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo1");
		var nodes = treeObj.getNodesByFilter(function (node){
			//获取部门节点
			if((node.id).indexOf("station")==-1) return true;
		});
		for(var i=0, l=nodes.length; i < l; i++) {
			//部门节点禁止点击
			//treeObj.setChkDisabled(nodes[i], true);
			nodes[i].nocheck = true;
			treeObj.updateNode(nodes[i]);
			
		}
		//主岗回显
		var treeNode = treeObj.getNodeByParam("id","station"+$("#stationId_main_user").val(), null);
		if(treeNode!=null){
			treeObj.checkNode(treeNode,true,false);			
			expandParentNode(treeNode,treeObj);
		}
		
		//副岗树初始化	****************	
		setting1.check.chkStyle= 'checkbox';
		setting1.callback.onCheck = zTreeOnCheck2;
		$.fn.zTree.init($("#treeDemo2"), setting1, ss);
		treeObj = $.fn.zTree.getZTreeObj("treeDemo2");
		nodes = treeObj.getNodesByFilter(function (node){
			//获取部门节点
			if((node.id).indexOf("station")==-1) return true;
		});
		for(var i=0, l=nodes.length; i < l; i++) {
			//部门节点禁止点击
			//treeObj.setChkDisabled(nodes[i], true);
			nodes[i].nocheck = true;
			treeObj.updateNode(nodes[i]);
		}
		//副岗回显
		var stationId_deputy = $("#stationId_deputy_user").val();
		if(stationId_deputy!=null&&stationId_deputy!=''){
			var array_stationId = stationId_deputy.split(',');
			for(var i =0;i<array_stationId.length;i++){
				if(array_stationId[i]==null||array_stationId[i]=='')continue;
				treeNode = treeObj.getNodeByParam("id","station"+array_stationId[i], null);
				if(treeNode!=null){
					treeObj.checkNode(treeNode,true,false);
					expandParentNode(treeNode,treeObj);
				}
			}						
		}					
				
		//角色树初始化****************
		setting1.callback.onCheck = zTreeOnCheck3;
		$.fn.zTree.init($("#treeDemo3"), setting1, ${json_Roles});		
		//角色树回显
		var roleIds_user = $("#roleIds_user").val();
		if(roleIds_user!=null&&roleIds_user!=''){
			roleIds_user = roleIds_user.split(",");
			var treeObj = $.fn.zTree.getZTreeObj("treeDemo3");
			for(var i=0;i<roleIds_user.length;i++){
				if(roleIds_user[i]==null||roleIds_user[i]=='')continue;
				treeNode = treeObj.getNodeByParam("id",roleIds_user[i], null);
				if(treeNode!=null)treeObj.checkNode(treeNode,true,false);
			}					
		}
												
	});		
</script>
<div class="userShade_d">
	<div class="userShade_dan">
		<div class="userShade_dan_title">
			<b id="userShadeTitle"><s:if test="userModel!=null&&userModel.id!=null">编辑用户</s:if><s:else>新建用户</s:else></b>
			<a href="javascript:void(0);" onclick="closeShade('userShadeCotent')"> 
				<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg" width="12" height="12" />
			</a>
		</div>
		<div class="userShade_dan_con">
			<form id="form1" action="" method="post">
				<input type="hidden" name="id_user" value="${userModel.id }"/>
				<ul style="max-height: 420px; _height: 420px; overflow: auto;">
					<li>
						<span style="color: red;">工&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</span>
						<p>
							<input id="jobNumber_user" name="jobNumber_user" value="${userModel.userInfo.jobNumber }" type="text" />
							<input id="jobNumber2_user" name="jobNumber2_user" value="${userModel.userInfo.jobNumber }" type="hidden" />
						</p>
					</li>
					<li>
						<span style="color: red;">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名</span>
						<p>
							<input id="userChineseName_user" name="userChineseName_user" value="${userModel.userInfo.userChineseName }" type="text" />
						</p>
					</li>
					<li>
						<span style="color: red;">账&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</span>
						<p>
							<input id="username_user" name="username_user" value="${userModel.username }" type="text" />
							<input id="username2_user" name="username2_user" value="${userModel.username }" type="hidden" />
						</p>
					</li>
					<li>
						<span>外网访问</span>
						<p>
							<input type="radio" style=" height:auto; width:auto;" name="visit_userModel" value="0" <s:if test='%{userModel==null||userModel.visit!="1" }'>checked="checked"</s:if> />否&nbsp;
							<input type="radio" style=" height:auto; width:auto;" name="visit_userModel" value="1" <s:if test='%{userModel.visit=="1" }'>checked="checked"</s:if> />是
						</p>
					</li>
					<li>
						<span style="color: red;">主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;岗</span>
						<p>
							<s:set var="stationId_main_user" value="''"/>
							<s:set var="stationName_main_user" value="''"/>
							<s:if test="%{userModel.stationsStatus0!=null&&userModel.stationsStatus0.size>0}">
								<s:iterator value="userModel.stationsStatus0" var="va">
									<s:if test="#va.id==userModel.mainStationId">
										<s:set var="stationId_main_user" value="#va.id"/>
										<s:set var="stationName_main_user" value="#va.name"/>
									</s:if>										
								</s:iterator>									
							</s:if>
							<input id="stationName_main_user" name="stationName_main_user" readonly="readonly" onclick="showMenu('menuContent1','stationName_main_user')" type="text" value="${stationName_main_user }"/>
							<input id="stationId_main_user" name="stationId_main_user" type="hidden" value="${stationId_main_user }"/>
						</p>
					</li>
					<li>
						<span>副&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;岗</span>
						<p>
							<s:set var="stationId_deputy_user" value="''"/>
							<s:set var="stationName_deputy_user" value="''"/>
							<s:if test="%{userModel.stationsStatus0!=null&&userModel.stationsStatus0.size>0}">
								<s:iterator value="userModel.stationsStatus0" var="va">
									<s:if test="#va.id!=userModel.mainStationId">
										<s:if test="#stationId_deputy_user==''">
											<s:set var="stationId_deputy_user" value="#va.id"/>
											<s:set var="stationName_deputy_user" value="#va.name"/>
										</s:if>
										<s:else>
											<s:set var="stationId_deputy_user" value="#stationId_deputy_user+','+#va.id"/>
											<s:set var="stationName_deputy_user" value="#stationName_deputy_user+','+#va.name"/>
										</s:else>											
									</s:if>										
								</s:iterator>									
							</s:if>
							<input id="stationName_deputy_user" name="stationName_deputy_user" readonly="readonly"
								onclick="showMenu('menuContent2','stationName_deputy_user')" type="text" value="${stationName_deputy_user }"/>
							<input id="stationId_deputy_user" name="stationId_deputy_user" type="hidden" value="${stationId_deputy_user }"/>
							<!--用于查看部门是否发生变化  -->
							<input id="stationId2_deputy_user" type="hidden" value="${stationId_deputy_user }"/>
						</p>
					</li>
					<li>
						<span style="color: red;">角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色</span>
						<p>
							<s:set var="roleIdsUser" value="''"/>
							<s:set var="roleNamesUser" value="''"/>
							<s:if test="%{userModel.roles!=null&&userModel.roles.size>0}">
								<s:iterator value="userModel.roles" var="va">
									<s:if test="#roleIdsUser==''">
										<s:set var="roleIdsUser" value="#va.id"/>
										<s:set var="roleNamesUser" value="#va.name"/>
									</s:if>
									<s:else>
										<s:set var="roleIdsUser" value="#roleIdsUser+','+#va.id"/>
										<s:set var="roleNamesUser" value="#roleNamesUser+','+#va.name"/>
									</s:else>	
								</s:iterator>									
							</s:if>
							<input id="roleNames_user" name="roleNames_user" readonly="readonly" onclick="showMenu('menuContent3','roleNames_user')" type="text" value="${roleNamesUser }"/>
							<input id="roleIds_user" name="roleIds_user" type="hidden" value="${roleIdsUser }"/>
						</p>
					</li>
					<li>
						<span>性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别</span>
						<p>
							<s:select id="sex_user" name="sex_user" list="#{'1':'男','2':'女'}" headerKey="" headerValue="---请选择---"
								listKey="key" listValue="value" value="#request.userModel.userInfo.sex"></s:select>
						</p>
					</li>
					<li>
						<span>出生日期</span>
						<p>
							<input id="birthday_user"  name="birthday_user" onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly" class="Wdate"
								value='<s:date  name="userModel.userInfo.birthday" format="yyyy-MM-dd"/>' 
								type="text"  />
						</p>
					</li>
					<li>
						<span>手机号码</span>
						<p>
							<input id="mobile_user" name="mobile_user" value="${userModel.userInfo.mobile }" type="text" />
						</p>
					</li>
					<li>
						<span>电子邮件</span>
						<p>
							<input id="email_user" name="email_user" value="${userModel.userInfo.email }"  type="text" />
						</p>
					</li>
					
					<li>
						<span>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注</span>
						<p>
							<input id="descContent_user" name="descContent_user" value="${userModel.userInfo.descContent }" type="text" />
						</p>
					</li>
					<li class="anniu">
						<label style="float: left;color: red;" id="errorMessage"></label>
						<a href="#"><input type="button" onclick="addOrEditUser()"
								class="userShade_aa2" value="提交" /> </a>
						<a href="#"><input type="reset" onclick="userReset()"
								class="userShade_aa1" value="重置" /> </a>							
					</li>
				</ul>
			</form>
		</div>
	</div>
</div>


