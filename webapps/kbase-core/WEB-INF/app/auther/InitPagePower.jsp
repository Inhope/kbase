<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>菜单管理</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">	
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+"/app/auther/power!getTreeMenu.htm",
					otherParam: {}
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "s", "N": "s" }
				},
				callback: {
					beforeClick: zTreeBeforeClick
				}
								
			};				
			
			function zTreeBeforeClick(treeId, treeNode, clickFlag) {
			    return false;
			};
			
			//******************************************************************************************************
			
			//角色点击
			function clickLi(roleId,index){
				setting.async.otherParam = {'roleId':roleId};
				//菜单树初始化
				$.fn.zTree.init($("#treeDemo"), setting);

				$("a[id^='a_']").each(function (){
					$(this).parent().css("background-color","");
				});				
				$("#a_"+index).parent().css("background-color","red");
				
				
				//绑定保存事件
				$("#savePower").unbind("click");
				$("#savePower").click(function (){
					savePower(roleId);				
				});				
			}
			
			//保存
			function savePower(roleId){					
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				var menuIds = "";//初始化
				for (var i=0;i<nodes.length;i++) {
					menuIds +=  nodes[i].id + ",";
				}
				if (menuIds.length >0){
					menuIds = menuIds.substring(0, menuIds.length-1);					
					$.post($.fn.getRootPath()+"/app/auther/power!savePower.htm",
						{"menuIds":menuIds,"roleId":roleId},function (data){
							if(data=="1"){
								parent.layer.alert("保存成功!", -1);
							}else{
								parent.layer.alert("保存失败!", -1);
							}						
					},"html");
				}				
			}
			
			//初始化
			$(document).ready(function(){
				$("#a_0").click();					
			});	
		</script>		
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xinxi_con">
				<div class="xinxi_con_nr">
					<div class="xinxi_con_nr_left">
						<ul>							
							<s:iterator value="#request.list_Role" var="va" status="statu">
								<li style="border-bottom: 1px solid #BEBEBE;"><a href="###" id="a_${statu.index }" onclick="clickLi('${va.id }','${statu.index }')">${va.name }</a></li>		
							</s:iterator>
						</ul>						
					</div>						
					<div class="xinxi_con_nr_right" >
						<div id="menuContent" class="menuContent" style="margin-left: 20px;border: 1px solid #BEBEBE;">
							<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
						</div>
						<div class="zuzhi" style="height: 50px;border: 0px;width: 98%">						
							<ul>
								<li class="anniu">
									<a href="#">
										<input type="button" id="savePower" class="fkui_aa2" value="保存"/>
									</a>
								</li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>
