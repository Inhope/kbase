<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_xinjian.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/myInfo_index.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>

<script type="text/javascript">
	if(parent && parent.SystemKeys)SystemKeys=parent.SystemKeys;
	if(parent.parent && parent.parent.SystemKeys)SystemKeys=parent.parent.SystemKeys;
	if(parent.parent.parent && parent.parent.parent.SystemKeys)SystemKeys=parent.parent.parent.SystemKeys;
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jQuery.md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.zokee.js"></script>


<script type="text/javascript">
var modify_type = "${param.mtype}";
function resetForm(){
	$("input[id$='_form']").each(function(){
		$(this).attr("value","");
	});	
}

function submitForm(){
	var pwd0 = $("#pwd0_form").val();
	var pwd1 = $("#pwd1_form").val();
	var pwd2 = $("#pwd2_form").val();
	if(pwd0==""){
		parent.layer.alert("原密码不能为空!", -1);
		return false;
	}
	if(pwd1==""){
		parent.layer.alert("新密码不能为空!", -1);
		return false;
	}
	if(pwd2==""){
		parent.layer.alert("确认密码不能为空!", -1);
		return false;
	}
	if(pwd1==pwd0){
		parent.layer.alert("新密码不能与原密码一致!", -1);
		return false;
	}
	if(pwd1!=pwd2){
		parent.layer.alert("2次输入的密码不一致!", -1);
		return false;
	}
	
	/**
	* 校验新密码格式 modify by heart.cao 2015-08-06
	* force_rank: 0-校验密码长度、
	*             1-校验密码是否为小写字母+数字、
	*             2-校验密码是否为小写字母+数字+大写字母
	*             3-校验密码是否为小写字母+数字+大写字母+特殊符号
	*/
	/**
	 * @author eko.zhan
	 * @modified 2015-08-08 13:30 
	 * 规则：如果没有强制首次登陆修改密码，修改密码等级依然生效。
	 */
    var $pwd = $.kbase.regex(pwd1);
    
    var _forceRank = SystemKeys.force_rank;
    var _forceLength = 8;
    
	if(!$pwd.matchLength(_forceLength)){
	    parent.layer.alert("新密码不能低于 " + _forceLength + " 位!", -1);
		return false;
	}
	
	if (_forceRank>0){
		if (!$pwd.matchNumeric()){
			parent.layer.alert("新密码必须包含数字!", -1);
			return false;
		}
		if (!$pwd.matchLowercase()){
			parent.layer.alert("新密码必须包含小写字母!", -1);
			return false;
		}
	}
	if (_forceRank>1){
		if (!$pwd.matchUppercase()){
			parent.layer.alert("新密码必须包含大写字母!", -1);
			return false;
		}
	}
	if (_forceRank>2){
		if (!$pwd.matchSpecchar()){
			parent.layer.alert("新密码必须包含特殊字符!", -1);
			return false;
		}
	}
	/*
	if(SystemKeys.force_rank!="0" && !pwd.matchNumeric()){
	    parent.layer.alert("新密码必须包含数字!", -1);
		return false;	
	}
	if(SystemKeys.force_rank!="0" && !pwd.matchLowercase()){
	    parent.layer.alert("新密码必须包含小写字母!", -1);
		return false;	
	}
	if((SystemKeys.force_rank=="2" || SystemKeys.force_rank=="3") && !pwd.matchUppercase()){
	    parent.layer.alert("新密码必须包含大写字母!", -1);
		return false;	
	}
	if(SystemKeys.force_rank=="3" && !pwd.matchSpecchar()){
	    parent.layer.alert("新密码必须包含特殊字符!", -1);
		return false;	
	}*/
	
	$('body').ajaxLoading('正在保存数据...');
	$.ajax({
		type : 'post',
		url : $.fn.getRootPath() + "/app/auther/user!pwdEditDo.htm", 
		data : {
			"password0" : $.md5(pwd0),
			"password" : $.md5(pwd1)
		},
		async: true,
		dataType : 'text',
		success : function(data){
			if(data == '1'){
				parent.layer.alert("保存成功!", -1);
				//保存成功以后执行流程模块的密码同步 add by eko.zhan at 2015-05-25 10:55
				$.workflow.password(SystemKeys.userName, $.md5(pwd1));
				
				if($.trim(modify_type)=="1"){
				    window.setTimeout(function(){
						$.workflow.logout();
						window.setTimeout(function(){
							window.location.href = $.fn.getRootPath() + '/app/login/logout.htm';
						}, 300);		
					}, 500);							
				}else{
				   resetForm();	
				}
				
			}else if(data == '2'){
				parent.layer.alert("保存失败，原密码错误!", -1);
			}else{
				parent.layer.alert("网络错误，请稍后再试!", -1);
			}
			$('body').ajaxLoadEnd();
		},
		error : function(msg){
			parent.layer.alert("网络错误，请稍后再试!", -1);
			$('body').ajaxLoadEnd();
		}
	});
}

$(document).ready(function (){
	
	$("#resetForm").unbind("click");
	$("#resetForm").click(function (){
		resetForm();
	});
	
	$("#submitForm").unbind("click");
	$("#submitForm").click(function(){
		submitForm();
	});
});

</script>

</head>

<body>
<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xin_titile">
				<b>修改密码</b>
			</div>
			<div class="xin_con">
				<ul>
					<li>
						<span><b>原密码</b></span>
						<input id="pwd0_form" type="password" />
					</li>
					<li>
						<span><b>新密码</b></span>
						<input id="pwd1_form" type="password" />
					</li>
					<li>
						<span><b>确认密码</b></span>
						<input id="pwd2_form" type="password" />
					</li>
					<li>
						<a href="#"><input class="xin_an1" id="submitForm" type="button" /></a>
						<a href="#"><input class="xin_an2" id="resetForm" type="button" /></a>
					</li>
					<li>
						<span style="color:red;">* 
						<c:choose>
							<c:when test="${sessionScope.force_rank=='0'}">新密码必须是八位及以上</c:when>
							<c:when test="${sessionScope.force_rank=='1'}">新密码必须是字母与数字混合</c:when>
							<c:when test="${sessionScope.force_rank=='2'}">新密码必须是大小写字母与数字混合</c:when>
							<c:otherwise>新密码必须是大小写字母、数字与特殊符号混合</c:otherwise>
						</c:choose>
						</span>
					</li>
				</ul>
			</div>
		</div>
		<!--******************内容结束***********************-->
</body>
</html>
