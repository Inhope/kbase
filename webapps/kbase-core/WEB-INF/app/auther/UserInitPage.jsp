<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>用户管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<style type="text/css">
			/*jquery-easyui与yonghu.css样式冲突*/
			.yonghu_select{height:41px;line-height:41px;background:#edecec;border-top:1px solid #e3e3e3;margin:0 10px;}
			.yonghu_select li{width:auto;height:41px;line-height:41px;float:left;padding-left:15px;color:#666;}
			.yonghu_select li input{width:86px;height:22px;line-height:22px;border:1px solid #cccccc;padding-left:2px;}
			.yonghu_select .anniu{width:auto;height:30px;line-height:30px;margin-bottom:10px;}
			.yonghu_select .anniu a{float:right;margin-left:12px;background:url("${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/yonghu/yonghu_an1.jpg") no-repeat;width:58px;height:24px;line-height:24px;margin-top:8px;color:#FFF;text-align:center; cursor:pointer;}
			.yonghu_select .anniu a:hover,.yonghu_titile a:active{background:url("${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/yonghu/yonghu_an2.jpg") no-repeat;color:#FFF;}
			.yonghu_select .anniu a input{border:none;background:none;color:#FFF;cursor:pointer;width:58px;text-align:center;}
			.yonghu_select div label { padding-left:15px; color: #666;}
			.yonghu_select div label > input {border: 1px solid #cccccc;height: 22px;line-height: 22px;padding-left: 2px;width: 86px;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>	
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>	
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/UserInitPage.js"></script>
		<script type="text/javascript">
			/*part_fenye.jsp使用*/
			function pageClick(pageNo){ 
				UserInitPage.pageClick(pageNo); 
			}
			/*部门查询（异步）数据回显*/
			$(function(){
				UserInitPage.set({
					rangDepts: ${rangDepts} /*所选部门DEPT所有涉及的父子部门id*/
				});
			});
		</script>
	</head>
	<body>
		<input type="hidden" onclick="UserInitPage.repairBH()" value="修复部门bh">
<!--******************************************************用户管理初始化页面*********************************************************************************  -->
		<form id="form0" action="" method="post">
			<div class="content_right_bottom" style="min-width:860px;">

				<div class="gonggao_titile">
					<div class="gonggao_titile_right">
						<!--
							<a id="userImport" href="javascript:void(0)">批量导入</a>
							<a href="javascript:void(0)" onclick="passwordReset()">重置密码</a>
							<a href="javascript:void(0)" onclick="accountNumber('1')">帐号停用</a>
							<a href="javascript:void(0)" onclick="accountNumber('0')">帐号启用</a>
							<a id="userDel" href="javascript:void(0)">删除用户</a>
							<a id="userEdit" href="javascript:void(0)">编 辑</a>
							<a id="userAdd" href="javascript:void(0)">新增用户</a>					
						-->
						<myTag:input type="a" value="查询导出"  key="userManageExportUser0" 		onclick="UserInitPage.exportUser('0')" />
						<myTag:input type="a" value="全部导出"  key="userManageExportUser1" 		onclick="UserInitPage.exportUser('1')" />
						<myTag:input type="a" value="批量导入"  key="userManageImport" 			id="userImport"  />
						<myTag:input type="a" value="重置密码"  key="userManagePasswordReset" 	onclick="UserInitPage.passwordReset()"  />
						<myTag:input type="a" value="帐号停用"  key="userManageAccountNumber1" 	onclick="UserInitPage.accountNumber('1')"  />
						<myTag:input type="a" value="帐号启用"  key="userManageAccountNumber0" 	onclick="UserInitPage.accountNumber('0')"  />
						<myTag:input type="a" value="删除用户"  key="userManageDel" 				id="userDel"  />
						<myTag:input type="a" value="编辑用户"  key="userManageEdit" 				id="userEdit"  />
						<myTag:input type="a" value="新增用户"  key="userManageAdd" 				id="userAdd"/>
						<a href="javascript:void(0)" onclick="window.open('${pageContext.request.contextPath}/app/auther/system!server.htm');" 
							style="display: none;">服务参数</a>
					</div>
				</div>

				<div class="yonghu_select" style="height: 82px;">
					<div style="width: 100%;">
						<label>
							工号：
							<input id="jobNumber_select"  name="jobNumber" type="text" value="${jobNumber }"/>
						</label>
						<label>
							账号：
							<input id="username_select" name="username" type="text" value="${username }"/>
						</label>
						<label>
							姓名：
							<input id="userChineseName_select"  name="userChineseName" type="text" value="${userChineseName }"/>
						</label>
						<label>
							角色：
							<s:select id="roleId_select" list="#request.list_Role" name="roleId" headerKey="" headerValue="-请选择-" 
								listKey="id" listValue="name" value="#request.roleId"></s:select>
						</label>
						<label>
							外网访问：
							<s:select id="visit_select" list="#{'1':'是','0':'否'}" name="visit" headerKey="" headerValue="-请选择-" cssStyle="width: 70px;"></s:select>	
						</label>
					</div>
					<div style="width: 100%;">
						<label style="float: left;">
							状态：
							<s:select id="status_select" list="#{'0':'启用','1':'停用'}" name="status" headerKey="" headerValue="-请选择-"></s:select>
						</label>
						<label style="float: left;">
							组织架构：
							<ul class="easyui-combotree" id="depIds_select" name="depIds" style="width: 200px;" 
								data-options="url:'${pageContext.request.contextPath}/app/auther/dept!asyncData.htm',
								lines: true, value:'${depIds }', 
				    			loadFilter:function(data, parent){
					   				var data_ = new Array();
	                     	 		if(data) {
	                     	 			/*获取已选择的节点的所有子数据*/
	                     	 			var rangDeptNodes = UserInitPage.fn.datas.get('rangDeptNodes');
	                     	 			if(!rangDeptNodes) rangDeptNodes = {};
	                     	 			$(data).each(function(i, item){
	                     	 				if(item.type == 'dept') {
	                     	 					/*当前节点id，当前节点的所有子数据*/
	                     	 					var id = item.id;
	                     	 					var node = {id: id, text: item.name, state: 'closed', bh: item.bh };
	                     	 					/*子节点数据*/
	                     	 					var children = rangDeptNodes[id];
	                     	 					if(children) {/*能获取到子数据*/
	                     	 						node.state = 'open' ;
	                     	 						node.children = children;
	                     	 					} 
	                     	 					data_.push(node);
	                     	 				}
		                     	 		});
	                     	 		}
	                     	 		return data_;
					   			},
					   			onShowPanel:function(){
					   				var t = $('#depIds_select').combotree('tree');	/*获取树对象*/
									var n = t.tree('getSelected');
									if(n) t.tree('scrollTo', n.target); /*移动至选中的节点*/ 
					   			},
					   			onSelect:function(node){
					   				$('#deptBhs_select').val(node.bh);
					   			}"></ul>
					   			<input type="hidden" id="deptBhs_select" name="deptBhs" value="${deptBhs }"/>
						</label>
						<label style="float: left;">
							组织范围：
							<s:select id="depFlag_select" headerKey="" headerValue="-请选择-" list="#{'0':'主岗','1':'副岗','3':'无岗'}" name="depFlag"></s:select>
						</label>
						<div class="anniu" style="float: left;width: 140px;">
							<a href="javascript:void(0)"><input type="button" class="youghu_aa1"
								value="重置" onclick="UserInitPage.userSelectReset()"/> </a>
							<a href="javascript:void(0)"><input type="button" class="youghu_aa2"
								value="提交" onclick="javascript:UserInitPage.pageClick('1');" /> </a>
						</div>
					</div>
				</div>

				<div class="gonggao_con">

					<div class="gonggao_con_nr">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="tdbg">
								<td>
									<input type="checkbox" name="ck_all" onclick="UserInitPage.checkAll()"/>
								</td>
								<td>
									工号
								</td>
								<td>
									姓名
								</td>
								<td>
									账号
								</td>
								<td>
									部门
								</td>
								<!--<td>
									岗位
								</td>
								--><td>
									性别
								</td>
								<td>
									出生日期
								</td>
								<td>
									手机号码
								</td>
								<td>
									邮件地址
								</td>
								<td>
									角色
								</td>
								<td>
									状态
								</td>
							</tr>
							<s:iterator value="page.result" var="va">
								<tr>
									<td>
										<input name="userId_list" type="checkbox" value="${va.id }" />
										<!--账号启用、停用信息验证  -->
										<input type="hidden" value="${va.status }"/>
									</td>
									<td>
										${va.userInfo.jobNumber }&nbsp;
									</td>
									<td>
										${va.userInfo.userChineseName }&nbsp;
									</td>
									<td>
										${va.username }&nbsp;
									</td>
									<td>
										<s:iterator value="#va.stationsStatus0" var="va1">
											<s:if test="#va1.id==#va.mainStationId">
												${va1.name }
											</s:if>											
										</s:iterator>&nbsp;
									</td>
									<!--<td>
										<s:iterator value="#va.stations" var="va1">
		    							${va1.name }						    								
	    								</s:iterator>
									</td>
									--><td>
										<s:if test='#va.userInfo.sex=="1"'>男</s:if>
										<s:if test='#va.userInfo.sex=="2"'>女</s:if>&nbsp;
									</td>
									<td>
										<s:date name="#va.userInfo.birthday" format="yyyy-MM-dd" />&nbsp;
									</td>
									<td>
										${va.userInfo.mobile }&nbsp;
									</td>
									<td>
										${va.userInfo.email }&nbsp;
									</td>
									<td>
										<s:iterator value="#va.roles" var="va1" status="st">
		    								${va1.name }<s:if test="!#st.last">,</s:if>
		    							</s:iterator>&nbsp;
									</td>
									<td>
										<s:if test="%{#va.status==0}">启用</s:if>
										<s:else>停用</s:else>&nbsp;
									</td>
								</tr>
							</s:iterator>						
							<tr class="trfen">
								<td colspan="11">
									<jsp:include page="../util/part_fenye.jsp"></jsp:include>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
		
		
<!--*******************用户新增、编辑**********************************************************************************************************************-->		
		<div id="userShadeCotent" style="display:none;position:fixed;z-index:10001;_position:absolute;"></div>

<!--******************导入弹出框***********************************************************************************************-->
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/piliang_dan.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		<script type="text/javascript">
			function check(obj){
				var fileName = obj.value;
				var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
				if (suffix != ('.xls')&&suffix != ('.xlsx')) {
					parent.layer.alert("请用模板文件做为上传文件!");
					obj.parentElement.innerHTML += '';
				}
			}
		</script>
		<div id="userImportShade" class="userImportShade" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			<div class="userImportShade_d">
				<div class="userImportShade_dan">
					<div class="userImportShade_dan_title">
						<b id="title_importOrExport"></b><a href="javascript:void(0)" id="userImportShadeClose"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
								width="12" height="12" />
						</a>
					</div>
					<div class="userImportShade_dan_con">
						<ul>
							<li id="scBefore" >
								<span><a href="javascript:void(0)">下载模板</a></span>
								<p>
									注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/auther/user!downloadMouldFile.htm" style="color: blue">点击下载</a>)填写相关数据
									<br />
									特别提示：
									<br />
									1、支持的EXCEL为2003\2007\2010\2013
									<br />
									2、上传需要通过修改模板内容来完成
									<br />
									3、上传内容请严格按照模板要求来填写
									<br />
								</p>
							</li>
							<li id="scAfter" >
								<span><a href="javascript:void(0)">处理结果</a></span>
								<p>
									注：错误文件中标有不同颜色区分的内容为需要修改的内容，请修改完毕后提交这个错误文件
									<br />
									消息提示：
									<br />
								</p>
							</li>
							<li id="importWarning" >
								<span><a href="javascript:void(0)" id="importWarning_title">上传中</a></span>
								<p>
									总计：<b id="rowTotal" style="color: red; size: 24px;">0</b>条，
									成功：<b id="fuccessTotal" style="color: red;size: 24px;">0</b>条，
									失败：<b id="failureTotal" style="color: red;size: 24px;">0</b>条
									<br />
								</p>
							</li>
							<li id="importHandle" class="anniu">	
								<p>
									<!--<input name="" type="text" />&nbsp;<a style="float: r" class="userImportShade_aa1" href="javascript:void(0)">浏览</a>
									-->
									<input type="file" id="userFile" name="userFile" onchange="check(this)" />
								</p>
								<span>
									<a class="userImportShade_aa1" href="javascript:void(0)"  onclick="UserInitPage.importUser()">上传</a>
								</span>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</div>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>

