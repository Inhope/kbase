<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>用户管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>	
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<style type="text/css">
			a{ text-decoration:none;}
			.inputflag{ color:red; font-size:14px;}
			.anniu{width:auto;height:30px;line-height:30px;margin-bottom:10px;}
			.anniu a{float:right;margin-left:12px;background:url("${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/yonghu/yonghu_an1.jpg") no-repeat;width:58px;height:24px;line-height:24px;margin-top:8px;color:#FFF;text-align:center; cursor:pointer;}
			.anniu a:hover,.yonghu_titile a:active{background:url("${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/yonghu/yonghu_an2.jpg") no-repeat;color:#FFF;}
			.anniu a input{border:none;background:none;color:#FFF;cursor:pointer;width:58px;text-align:center;}
		</style>
		
		<script type="text/javascript">	
					
			//内容重置
			function userReset(){
				$("input[id$='_user']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_user']").each(function(){
					$(this)[0].selectedIndex = '';
				});
				
				$("#errorMessage").html("");//清掉提示信息
				$("#birthday_user").next().find("input:eq(0)").val('');//清掉用户生日
				$("input[name='userModel.userInfo.birthday']:eq(0)").val('');//清掉用户生日(隐藏文本域)
			}
					
			//用户新增、编辑		
			function addOrEditUser(){
				var bl = true;
				var jobNumber_user =  $("#jobNumber_user").val();
				var userChineseName_user =  $("#userChineseName_user").val();
				var username_user =  $("#username_user").val();
				var stationId_main_user =  $("#stationId_main_user").val();
				var stationId_deputy_user =  $("#stationId_deputy_user").val();
				var roleIds_user =  $("#roleIds_user").val();
				var email_user =  $("#email_user").val();
				var mobile_user =  $("#mobile_user").val();			
						
				var errorMessage = $("#errorMessage");				
				if(jobNumber_user==''){
					$(errorMessage).html("工号不能为空!");
					return;
				}
				if(userChineseName_user==''){
					$(errorMessage).html("姓名不能为空!");
					return;
				}
				if(username_user==''){
					$(errorMessage).html("账号不能为空!");
					return;
				}		
				if(stationId_main_user==''){
					$(errorMessage).html("主岗不能为空!");
					return;
				}
				if(roleIds_user==''){
					$(errorMessage).html("角色不能为空!");
					return;
				}
				if(email_user!=''){//邮箱不为空验证格式
					var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if (!filter.test(email_user)) {
						$(errorMessage).html("邮箱不符合标准!");
						return;
					}
				}
				if(mobile_user!=''){//手机号码不为空验证格式
					var filter  = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
					if (!filter.test(mobile_user)) {
						$(errorMessage).html("手机号码不符合标准!");
						return;
					}
				}
		
				var bl = true;			
				$.post(
					$.fn.getRootPath()+"/app/auther/user!checkData.htm",
					{'jobNumber':jobNumber_user,'jobNumber2':$("#jobNumber2_user").val(),
					 'username':username_user,'username2':$("#username2_user").val()},
					function(data){
						if(data!=null&&data!=''){
							data = data.split(",");			
							if(data[0]!='0'){
								$(errorMessage).html("工号已存在!");
								bl= false;
							}
							
							if(data[1]!='0'){
								$(errorMessage).html("账号已存在!");
								bl = false;
							}
									
							if(bl){
								$(errorMessage).html("");
								var params=$("#form1").serialize();
								$.ajax({
							   		type: "POST",
							   		url : $.fn.getRootPath() + "/app/auther/user!addOrEditUserDo.htm?"+params,
							   		async: false,
							   		dataType : 'text',
							   		success: function(msg){
							   			$.messager.alert('信息', msg, 'info', function(){
							   				parent.pageClick('1');
							   			});
							   		}
							   　});
														
								//$("#form1").attr("action", $.fn.getRootPath()+"/app/auther/user!addOrEditUserDo.htm");
								//$("#form1").submit();
							}
						}
				},"html");
			}
		</script>
		
		<script type="text/javascript">	
		
			function expandParentNode(treeNode,treeObj){
				treeNode = treeNode.getParentNode();
				if(treeNode!=null){
					treeObj.expandNode(treeNode, true, false, true);
					expandParentNode(treeNode,treeObj);
					treeNode = treeNode.getParentNode();
				}
			}
			
		//***************页面初始化*******************	
			$(document).ready(function(){
				/*可视区高度、宽度*/
				var diaWidth = $(window).width()*0.95, 
					diaHeight = $(window).height()*0.90;
				if(diaWidth > 540) diaWidth = 540;
				if(diaHeight > 350) diaHeight = 350;
				//主岗选择
				$('#stationName_main_user').click(function(){
					$.kbase.picker.stationByDept({returnField:"stationName_main_user|stationId_main_user", 
						diaWidth: diaWidth, diaHeight: diaHeight});
				});
				//副岗选择
				$('#stationName_deputy_user').click(function(){
					$.kbase.picker.multiStationByDept({returnField:"stationName_deputy_user|stationId_deputy_user", 
						diaWidth: diaWidth, diaHeight: diaHeight});
				});
				// 角色回显
				$('#role_select').combotree('setValues', $('#roleIds_user').val().split(','));
			});		
		</script>
	</head>
	<body>
		<form id="form1" action="" method="post">
			<input type="hidden" name="id_user" value="${userModel.id }"/>
			<table cellspacing="0" class="box" style="width: 100%;">
				<tr>
					<th>
						<span class="inputflag">*</span>工号：
					</th>
					<td>
						<input id="jobNumber_user" name="jobNumber_user" value="${userModel.userInfo.jobNumber }" type="text" />
						<input id="jobNumber2_user" name="jobNumber2_user" value="${userModel.userInfo.jobNumber }" type="hidden" />
					</td>
					<th>
						<span class="inputflag">*</span>姓名：
					</th>
					<td>
						<input id="userChineseName_user" name="userChineseName_user" value="${userModel.userInfo.userChineseName }" type="text" />
					</td>
				</tr>
				<tr>
					<th>
						<span class="inputflag">*</span>账号：
					</th>
					<td>
						<input id="username_user" name="username_user" value="${userModel.username }" type="text" />
						<input id="username2_user" name="username2_user" value="${userModel.username }" type="hidden" />
					</td>
					<th>
						外网访问：
					</th>
					<td>
						<input type="radio" style=" height:auto; width:auto;" name="visit_userModel" value="0" <s:if test='%{userModel==null||userModel.visit!="1" }'>checked="checked"</s:if> />否&nbsp;
						<input type="radio" style=" height:auto; width:auto;" name="visit_userModel" value="1" <s:if test='%{userModel.visit=="1" }'>checked="checked"</s:if> />是
					</td>
				</tr>
				<tr>
					<th>
						<span class="inputflag">*</span>主岗：
					</th>
					<td colspan="3">
						<s:set var="stationId_main_user" value="''"/>
						<s:set var="stationName_main_user" value="''"/>
						<s:if test="%{userModel.stationsStatus0!=null&&userModel.stationsStatus0.size>0}">
							<s:iterator value="userModel.stationsStatus0" var="va">
								<s:if test="#va.id==userModel.mainStationId">
									<s:set var="stationId_main_user" value="#va.id"/>
									<s:set var="stationName_main_user" value="#va.name"/>
								</s:if>										
							</s:iterator>									
						</s:if>
						<input id="stationName_main_user" name="stationName_main_user" readonly="readonly" type="text" 
							value="${stationName_main_user }" style="width: 200px"/>
						<input id="stationId_main_user" name="stationId_main_user" type="hidden" value="${stationId_main_user }"/>
					</td>
				</tr>
				<tr>
					<th>
						副岗：
					</th>
					<td colspan="3">
						<s:set var="stationId_deputy_user" value="''"/>
						<s:set var="stationName_deputy_user" value="''"/>
						<s:if test="%{userModel.stationsStatus0!=null&&userModel.stationsStatus0.size>0}">
							<s:iterator value="userModel.stationsStatus0" var="va">
								<s:if test="#va.id!=userModel.mainStationId">
									<s:if test="#stationId_deputy_user==''">
										<s:set var="stationId_deputy_user" value="#va.id"/>
										<s:set var="stationName_deputy_user" value="#va.name"/>
									</s:if>
									<s:else>
										<s:set var="stationId_deputy_user" value="#stationId_deputy_user+','+#va.id"/>
										<s:set var="stationName_deputy_user" value="#stationName_deputy_user+','+#va.name"/>
									</s:else>											
								</s:if>										
							</s:iterator>									
						</s:if>
						<input id="stationName_deputy_user" name="stationName_deputy_user" readonly="readonly" type="text" 
							value="${stationName_deputy_user }" style="width: 200px"/>
						<input id="stationId_deputy_user" name="stationId_deputy_user" type="hidden" value="${stationId_deputy_user }"/>
					</td>
				</tr>
				<tr>
					<th>
						<span class="inputflag">*</span>角色：
					</th>
					<td colspan="3">
						<s:set var="roleIdsUser" value="''"/>
						<s:if test="%{userModel.roles!=null&&userModel.roles.size>0}">
							<s:iterator value="userModel.roles" var="va">
								<s:if test="#roleIdsUser==''">
									<s:set var="roleIdsUser" value="#va.id"/>
								</s:if>
								<s:else>
									<s:set var="roleIdsUser" value="#roleIdsUser+','+#va.id"/>
								</s:else>	
							</s:iterator>									
						</s:if>
						<ul class="easyui-combotree" id="role_select" name="roleIds_user" style="width: 200px;" 
							data-options="url:'${pageContext.request.contextPath}/app/util/choose!roleData.htm',
							lines: true, multiple: true,
			    			loadFilter:function(data, parent){
	                    	 		if(data) {
	                    	 			$(data).each(function(i, item){
	                    	 				item.text = item.name;
	                   	 					item.state = 'open';
	                     	 		});
	                    	 		}
	                    	 		return data;
				   			},
				   			onCheck:function(node, checked){
				   				var roleIds = '';
				   				var t = $('#role_select').combotree('tree');	/*获取树对象*/
								var nodes = t.tree('getChecked');	/*获取选择的节点*/
				   				if(nodes) $.each(nodes, function(i, e){
				   					roleIds += ',' + e.id;
				   				});
				   				roleIds = roleIds.replace(/^,|,$/g, '');
				   				$('#roleIds_user').val(roleIds);
				   			}"></ul>
						<input id="roleIds_user" type="hidden" value="${roleIdsUser }"/>
					</td>
				</tr>
				<tr>
					<th>
						性别：
					</th>
					<td>
						<s:select id="sex_user" name="sex_user" list="#{'1':'男','2':'女'}" headerKey="" headerValue="---请选择---"
							listKey="key" listValue="value" value="#request.userModel.userInfo.sex"></s:select>
					</td>
					<th>
						出生日期：
					</th>
					<td>
						<input id="birthday_user" name="birthday_user" type="text" class="easyui-datebox" 
							value='<s:date  name="userModel.userInfo.birthday" format="yyyy-MM-dd"/>'></input>
					</td>
				</tr>
				<tr>
					<th>
						手机号码：
					</th>
					<td>
						<input id="mobile_user" name="mobile_user" value="${userModel.userInfo.mobile }" type="text" />
					</td>
					<th>
						电子邮件：
					</th>
					<td>
						<input id="email_user" name="email_user" value="${userModel.userInfo.email }"  type="text" />
					</td>
				</tr>
				<tr>
					<th>
						备注：
					</th>
					<td colspan="3">
						<input id="descContent_user" name="descContent_user" value="${userModel.userInfo.descContent }" type="text" />
					</td>
				</tr>
				<tr>
					<td colspan="4" class="anniu">
						<label style="float: left;color: red;" id="errorMessage"></label>
						<a href="#"><input type="button" onclick="addOrEditUser()"
								class="userShade_aa2" value="提交" /> </a>
						<a href="#"><input type="reset" onclick="userReset()"
								class="userShade_aa1" value="重置" /> </a>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
