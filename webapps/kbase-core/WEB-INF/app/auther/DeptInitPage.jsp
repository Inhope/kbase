<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>部门管理</title>
		<link rel="stylesheet" type="text/css"  href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/zuzhi.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/Depts.js"></script>
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="xinxi_con">
				<div class="xinxi_con_nr">
					<div class="xinxi_con_nr_left">
						<div id="menuContent" class="menuContent">
							<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
						</div>
					</div>								
					<div class="xinxi_con_nr_right" style="display: none;" id="content0">
						<div class="zuzhi">
							<ul>
								<li>
									<center><span style="float: none;" id="title0"></span></center>						
								</li>
								<li>
									<span>所属部门：</span>
									<p>
										<input id="name_pdept" type="text" readonly="readonly"/>
										<input id="id_pdept" type="hidden" />
									</p>
								</li>
								<li>
									<span>当前部门：</span>
									<p>
										<input id="name_dept" type="text" />
										<input id="id_dept" type="hidden" />
									</p>
								</li>								
								<li>
									<span>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</span>
									<p>
										<textarea id="descContent_dept" cols="" rows=""></textarea>
									</p>
								</li>
								<li class="anniu">									
									<a href="javascript:void(0)"><input type="button" class="fkui_aa2"
											value="保存" id="saveDept"/></a>
									<a href="javascript:void(0)"><input type="button" class="fkui_aa1"
											value="重置" id="resetDept"/></a>
									<a href="javascript:void(0)"><input type="button" class="fkui_aa1"
											value="删除" id="delDept"/></a>							
								</li>
							</ul>
						</div>
					</div>
					
					<div class="xinxi_con_nr_right" style="display: none;" id="content1">
						<div class="zuzhi">
							<ul>
								<li>
									<center><span style="float: none;" id="title1"></span></center>						
								</li>
								<li>
									<span>所属部门：</span>
									<p>
										<input id="name_pstation" type="text" readonly="readonly"/>
										<input id="id_pstation" type="hidden" />
									</p>
								</li>
								<li>
									<span>当前岗位：</span>
									<p>
										<input id="name_station" type="text" />
										<input id="id_station" type="hidden" />
									</p>
								</li>								
								<li>
									<span>备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</span>
									<p>
										<textarea id="descContent_station" cols="" rows=""></textarea>
									</p>
								</li>
								<li class="anniu">									
									<a href="javascript:void(0)"><input type="button" class="fkui_aa2"
											value="保存" id="saveStation"/></a>
									<a href="javascript:void(0)"><input type="button" class="fkui_aa1"
											value="重置" id="resetStation"/></a>
									<a href="javascript:void(0)"><input type="button" class="fkui_aa1"
											value="删除" id="delStation"/></a>								
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--******************内容结束***********************-->
		<!--******************部门岗位树(新增或者编辑)************************************************************************************************************* -->
		<div id="menuContent1" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
			<ul id="treeDemo1" class="ztree" style="margin-top:0; width:220px;height:300px;overflow-y:auto;overflow-x:auto;"></ul>
		</div>
				
		<div id="treeMenu" class="index_dankuang">
			<ul>
				<li id="deptImport"><a href="javascript:void(0);">导入</a></li>
				<li id="addDept"><a href="javascript:void(0);">新建部门</a></li>
				<li id="addStation"><a href="javascript:void(0);">新建岗位</a></li>
			</ul>
		</div>
		
		<!--******************导入弹出框***********************************************************************************************-->
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/piliang_dan.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		<script type="text/javascript">
			function check(obj){
				var fileName = obj.value;
				var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
				if (suffix != ('.xls')&&suffix != ('.xlsx')) {
					parent.layer.alert("请用模板文件做为上传文件!");
					obj.parentElement.innerHTML += '';
				}
			}
			
			//弹出框打开
			function openShade(shadeId){
				var w = ($('body').width() - $('#'+shadeId).width())/2;
				$('#'+shadeId).css('left',w+'px');
				$('#'+shadeId).css('top','0px');
				$('body').showShade();
				$('#'+shadeId).show();				
			}
			
			//弹出框关闭
			function closeShade(shadeId){
				$('#'+shadeId).css("display","none");
				$('body').hideShade();			
			}
			
			var intervalTitle = null;//提示标题
			var interval = null;//处理结果
			var prefix = "";//提示标题前缀
			var suffix = "";//提示标题后缀
			//提示标题
			function importWarningTitle(){
				if(suffix.length==4)suffix = "";
				$("#importWarning_title").html("<b>"+prefix+suffix+"<b/>");
				suffix += "*";
			}
			
			//导入导出结束初始化
			function importOrExport_init(){
				if(interval!=null)clearInterval(interval);
				if(intervalTitle!=null)clearInterval(intervalTitle);
				$("#userImportShadeClose").bind("click",function(){
					closeShade("userImportShade");
				});
			}
			
			//导入、导出消息提示
			function importOrExport(type){
				if(type=='import'){
					prefix = "导入中"
				}else if(type=='export'){
					prefix = "导出中"
				}else{
					return false;
				}
				//显示消息提示区
				$('#scBefore').css("display","none");
				$('#importWarning').css("display","block");
				$('#scAfter').css("display","none");
				$('#importHandle').css("display","none");
				$("#rowTotal").html(0);
				$("#fuccessTotal").html(0);
				$("#failureTotal").html(0);
				$("#userImportShadeClose").unbind("click");//去掉关闭事件
				//提示标题
				intervalTitle = setInterval("importWarningTitle(prefix, suffix)",500);
				//提示结果
			   	interval = setInterval(function(){
			   		$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/auther/dept!importDeptResult.htm', 
						data : {'type':type},
						async: true,
						dataType : 'json',
						success : function(data){
							$("#rowTotal").html(data.rowTotal);
							$("#fuccessTotal").html(data.fuccessTotal);
							$("#failureTotal").html(data.failureTotal);
							if(data.result==true){
								importOrExport_init();
							}
						},
						error : function(msg){
							importOrExport_init();
							parent.layer.alert("网络错误，请稍后再试!", -1);
						}
					});
			   	},500);
			}
			
			//导入
			function importDept(){
				if($("#deptFile").val()==null||$("#deptFile").val()==''){
					parent.layer.alert("请选择需要上传的文件!", -1);
					return false;
				}
				
				//导入消息提示
				$("#title_importOrExport").html("批量导入");
				importOrExport('import');
				
				//数据导入
				$.ajaxFileUpload({
			        url: $.fn.getRootPath()+"/app/auther/dept!importDept.htm",
			        secureuri: false,
			        fileElement: $("#deptFile"),
			        uploadFileParamName : 'deptFile',
			        success: function(data, status) {
			        	$('#scBefore').css("display","none");
						$('#importWarning').css("display","none");
						$('#scAfter').css("display","block");
						$('#importHandle').css("display","block");
						
						data = eval('(' + $(data).text() + ')');
					   	$('#scAfter p').html(data.returnResult);
			        	importOrExport_init();
			        },
			        error: function(obj, msg, e) {
			        	importOrExport_init();
			        	parent.layer.alert("网络错误，请稍后再试!", -1);
			        }
			    });
			}
			
			$(document).ready(function(){
				//批量导入
				$("#deptImport").click(function(){
				 	$('#scBefore').css("display","block");
					$('#scAfter').css("display","none");
					$('#importWarning').css("display","none");
					$('#importHandle').css("display","block");
					openShade("deptImportShade");
				});
				//批量导入弹出窗关闭
				$('#deptImportShadeClose').click(
					function(){closeShade("deptImportShade");
				});
			});
		</script>
		<div id="deptImportShade" class="userImportShade" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			<div class="userImportShade_d">
				<div class="userImportShade_dan">
					<div class="userImportShade_dan_title">
						<b id="title_importOrExport"></b><a href="javascript:void(0)" id="deptImportShadeClose"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
								width="12" height="12" />
						</a>
					</div>
					<div class="userImportShade_dan_con">
						<ul>
							<li id="scBefore" >
								<span><a href="javascript:void(0)">下载模板</a></span>
								<p>
									注：请严格按照模板(<a href="${pageContext.request.contextPath}/doc/deptImportMould.xls" style="color: blue">点击下载</a>)填写相关数据
									<br />
									特别提示：
									<br />
									1、支持的EXCEL为2003\2007\2010\2013
									<br />
									2、上传需要通过修改模板内容来完成
									<br />
									3、上传内容请严格按照模板要求来填写
									<br />
								</p>
							</li>
							<li id="scAfter" >
								<span><a href="javascript:void(0)">处理结果</a></span>
								<p>
									注：错误文件中标有不同颜色区分的内容为需要修改的内容，请修改完毕后提交这个错误文件
									<br />
									消息提示：
									<br />
								</p>
							</li>
							<li id="importWarning" >
								<span><a href="javascript:void(0)" id="importWarning_title">上传中</a></span>
								<p>
									总计：<b id="rowTotal" style="color: red; size: 24px;">0</b>条，
									成功：<b id="fuccessTotal" style="color: red;size: 24px;">0</b>条，
									失败：<b id="failureTotal" style="color: red;size: 24px;">0</b>条
									<br />
								</p>
							</li>
							<li id="importHandle" class="anniu">	
								<p>
									<input type="file" id="deptFile" name="deptFile" onchange="check(this)" />
								</p>
								<span>
									<a class="userImportShade_aa1" href="###"  onclick="importDept()">上传</a>
								</span>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>
