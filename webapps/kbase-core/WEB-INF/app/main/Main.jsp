<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@page import="com.eastrobot.domain.User"%>
<%@page import="com.eastrobot.domain.Menu"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%
	//是否开启新的工作流版本 mode 流程引擎启用版本(1新版本,默认旧版本)
	//modify by eko.zhan at 2015-06-11 从当前版本以后将不再根据mode是否为1来判断是否启用新的流程模式
	//String workFlowMode = PropertiesUtil.value("robot.workflow.mode");
	//全文按钮是否显示(默认显示，即仅当quanwen_display=none时不显示)
	String fullSearchOption = PropertiesUtil.value("mian_jsp.setting.quanwen.display");
	//add by eko.zhan at 2015-04-21 流程地址
	//String workflowPath = WorkflowUtils.getWorkflowPath();
	//pageContext.setAttribute("workflowPath", workflowPath);
	//modify by eko.zhan at 2015-08-07 流程地址从session中获取
	String workflowPath = (String)session.getAttribute("workflowPath");
	//是否启用个人中心功能 modify by eko.zhan at 2015-04-29
	String isOpenIndividual = PropertiesUtil.value("showindividual");
	String isOpentask = PropertiesUtil.value("showpetask");
	pageContext.setAttribute("isOpentask", Boolean.valueOf(isOpentask));
	pageContext.setAttribute("isOpenIndividual", Boolean.valueOf(isOpenIndividual));
	User user = (User)session.getAttribute("session_user_key");
	String theme = user.getUserInfo().getUserTheme();
	String userName = user.getUsername();
	String password = user.getPassword();
	pageContext.setAttribute("enableAdvTab", true);	//add by eko.zhan at 2016-06-16 17:35 是否开启高级Tab模式（true-jeasyui Tab(未完全测试)，false-老的TABOBJECT，详见 Tab.js）
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<c:choose>
			<c:when test="${not empty system_shortcut_icon}">
				<link href="${pageContext.request.contextPath}/theme/${system_shortcut_icon }?t=20150201" type="image/x-icon" rel="shortcut icon">
			</c:when>
			<c:otherwise>
				<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/logo.ico" type="image/x-icon" rel="shortcut icon">
			</c:otherwise>
		</c:choose>
		<title>${system_title}</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/main.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/dan.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/index_dankuang.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/gonggao_zhanshi.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/leftTree.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css">
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/kbs-extend.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			if(!window.USER_THEME){
				window.USER_THEME = '${sessionScope.session_user_key.userInfo.userTheme}';
			}
			var SystemKeys = {
				userName: '${sessionScope.session_user_key.username}',	//User.username
				userNameCN: '${sessionScope.session_user_key.userInfo.userChineseName}',	//UserInfo.userChineseName
				workflowPath: '${sessionScope.workflowPath}',
				theme: '${sessionScope.session_user_key.userInfo.userTheme}',	//UserInfo.theme
				userId: '${sessionScope.session_user_key.id}',	//User.id
				userPwd: '${sessionScope.session_user_key.password}',	//User.password
				userData: {},
		        force_change: '${sessionScope.force_change}',	//force_change 用户首次登陆时，是否提示用户修改密码。1-是  0-否
		        force_rank: '${sessionScope.force_rank}',	//force_rank 用户密码校验策略
		        isWukong: '${sessionScope.isWukong}', //是否走solr搜索引擎
		        enableDataEmbed: '${sessionScope.dataembed_enable}' //是否启动埋点记录
			};
			//add by eko.zhan at 2015-09-24 09:15 Banner栏是否折叠
			SystemKeys.userData.bannerCollapse = '${requestScope.bannerCollapse}';
			
			//add by eko.zhan at 2016-06-07 16:49 是否启用归属地
			SystemKeys.userData.enableLocation = '${sessionScope.locationEnable}';
			
			//是否华为 add by wilson.li at 2016-06-06
			function isHuawei(){
				return '<%=SystemKeys.isHuawei()%>';
			}
			
			//是否为百联 add by eko.zhan at 2016-09-02 12:59
			window.__kbs_isBailian = '<%=SystemKeys.isBailian()%>';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.goup.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSUtils.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Header.js?20160425002"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Left.js"></script>
		<!-- 是否开启高级Tab页模式  add by eko.zhan at 2016-06-16 17:35 -->
		<c:if test="${!enableAdvTab}">
			<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Tab.js"></script>
		</c:if>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Main.js?20150626"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Suggest.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Tree.js"></script>
		<!-- ztree -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<!-- 		日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		<!-- 短信日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSLog.js"></script>
		
		<!-- easy ui -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.tree.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
		<!-- 是否开启高级Tab页模式  add by eko.zhan at 2016-06-16 17:35 -->
		<c:if test="${enableAdvTab}">
			<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/extend-${sessionScope.session_user_key.userInfo.userTheme}.css">
		</c:if>
		<!--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro-${sessionScope.session_user_key.userInfo.userTheme}/easyui.css" />-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript">
			var categoryTag = '${categoryTag}';
			var isOpentask = ${isOpentask};
			window.__IS_KBASE = 1;
			window.__KBASE_WORKFLOW_MODE = 1;
			window.__kbs_enable_bulletin = categoryTag=='true' || '<%=SystemKeys.isBailian()%>'=='true';	//标识是否启用部门级别的公告，add by eko.zhan at 2016-08-04 13:14 
		</script>
		
		<!--[if lte IE 6]>
		<style type="text/css">
			body {behavior:url("theme/csshover.htc"); }
		</style>
		<![endif]-->
		
		
		<style type="text/css">
			html{overflow:hidden}
			.header_title_pmd {
				width: auto;
			    height: 70px;
			    float: left;
			    position: relative;
			}
		</style>
		
		
		
	</head>

	<body style="overflow:<c:if test="${enableAdvTab }">hidden</c:if>;">
		<!--******************头部开始***********************-->
		<div class="header">
			<div class="header_left header_title_pmd" style="left: 0px;top: 0px;">
				<c:choose>
					<c:when test="${empty system_banner_title and empty system_banner_logo}"><b>${system_title}</b></c:when>
					<c:when test="${not empty system_banner_title}"><b>${system_banner_title}</b></c:when>
					<c:otherwise><img src="${pageContext.request.contextPath}/theme/${system_banner_logo }?t=20150201" style="margin:3px;margin-left:10px;margin-top:12px;"/></c:otherwise>
				</c:choose>
			</div>
			<!-- hidden by eko.zhan at 2015-02-13
			<div id="noticeMarquee" class="header_center">
				<ul>
				<li><a href="javascript:void(0);">今日更新来白金卡服务投诉标准，该标准有效期为2014年2月14日</a></li>
				</ul>
			</div>
			 -->
			 <!--  
			<div style="color:#fff;height:24px;line-height:24px;margin-top:24px;margin-left:20px;overflow:hidden;float:left;display:inline;">
				<marquee style="width:300px;height:24px;" direction="up" scrollamount="1" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();">
				</marquee>
			</div>
			-->
			<div style="color:#fff;height:58px;line-height:24px;margin-top:5px;margin-left:20px;overflow:hidden;float:left;display:inline;">
				<marquee style="width:300px;height:58px;" direction="up" scrollamount="1" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();">
					<!-- 那些把灯背在背上的人，把他们的影子投到了自己前面。 -->
				</marquee>
			</div>
			
			<div class="header_right">
				<div class="header_right_top">
					<ul>
						<li>
							<a class="meiyou" style="text-decoration: none;"><s:property value="#session.session_user_key.userInfo.userChineseName"/></a>
						</li>
						<li>
							<a style="text-decoration: none;">
							<s:iterator value="#session.session_user_key.stationsStatus0" var="station" status="i">
								<s:if test="#session.session_user_key.mainStationId==#station.id">
								<s:property value="#station.dept.name"/>
								</s:if>
							</s:iterator>
							</a>
						</li>
					</ul>
				</div>
				<div class="header_right_bottom">
					<ul>
						<li>
							<a class="meiyou" href="javascript:void(0);" id="navLogout" >注销</a>
						</li>
						<!-- hidden by eko.zhan at 2015-04-29	如果脑图功能需要设置权限，则在左侧菜单控制，如果不需要设置权限，可以在此放开，暂时关闭入口
							<li>
							   <kbs:input type="a" key="navigation_mindmap" id="navMindManager" value="我的脑图"/>
							</li>
						 -->
						<c:choose>
							<c:when test="${isOpenIndividual}">
								<!-- 开启个人中心，合理化建议合并至 发起流程 入口 modify by eko.zhan at 2015-04-29 -->
								<li>
									<a href="#nogo" id="navIndividual">个人中心</a>
								</li>
								<c:if test="${categoryTag=='true'}">
									<li>
										<a href="javascript:void(0)" id="navBulletin">公告管理</a>
									</li>
								</c:if>
							</c:when>
							<c:otherwise>
								<li>
									<a href="#nogo" id="navPwdEdit">修改密码</a>
								</li>
								<li>
									<a href="#nogo" id="navSetting">设置</a>
								</li>
								<li>
									<a href="#nogo" id="navUserInfo">我的信息</a>
								</li>
								<li>
									<a href="#nogo" id="navNoticeManage">公告管理</a>
								</li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!--******************头部结束**********************-->

		<!--******************内容开始***********************-->
		<div class="content">
			<div class="content_left_bg">
			<div class="content_left">
				<ul class="juli">
					<li>
						<div class="left_1">
							<p></p>
							<a href="javascript:void(0);">知识分类<b></b>
							</a>
						</div>
						<div class="left_fen">
							<div class="content_left_ss">
								<div class="content_left_s1">
									<div class="content_left_s2">
										<div class="input_left">
											<a href="javascript:void(0);" id="treeButton"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/left_ss1.jpg" width="23"
													height="22" />
											</a>
										</div>
										<div class="input_right">
											<div class="input_k">
												<input id="treeInput" name="" type="text" />
											</div>
										</div>
									</div>
									<!-- 目录高级搜索 -->
									<div class="dir_seniorSearch">
											<a id="dirSearchBtn" href="javascript:void(0);">模糊搜索</a>
									</div>
								</div>
							</div>
							<div class="content_left_er">
								<style type="text/css">
									.tree-indent{
										display: inline-block;
										height: 14px;
										vertical-align: middle;
										width: 12px;
									}
									.tree-collapsed{
										background: rgba(0, 0, 0, 0) url("${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/images/tree_arrows.gif") no-repeat scroll -3px -4px;
										background-position: -3px -4px;
										display: inline-block;
										height: 14px;
										vertical-align: middle;
										width: 12px;
									}
									.tree-expanded{
										background: rgba(0, 0, 0, 0) url("${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/images/tree_arrows.gif") no-repeat scroll -20px -4px;
										background-position: -20px -4px;
										display: inline-block;
										height: 14px;
										vertical-align: middle;
										width: 12px;
									}
								</style>
								<ul id="categoryTree" class="easyui-tree" url="${pageContext.request.contextPath }/app/main/ontology-category!list.htm?type=jeasyuitree"></ul>
							</div>
						</div>
					</li>
					<!--<li>
						<div class="left_2">
							<p></p>
							<a href="javascript:void(0);">系统管理</a>
						</div>
						<ul class="jiedian" style="display: none;">
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/user!initPage.htm">用户管理</a></li>
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/role.htm">角色管理</a></li>
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/dept.htm">部门管理</a></li>
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/power.htm">权限管理</a></li>
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/menu.htm">菜单管理</a></li>
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/auther/perm.htm">权限管理</a></li>
							<li><a href="javascript:void(0);" url="http://oa1.demo.xiaoi.com/j_acegi_security_check?j_username=sysadmin&j_password=1&targeturl=http://oa1.demo.xiaoi.com/workflow/workflow/workflowinfolist.jsp?moduleid=402883253285e613013285ebc5f10002">流程管理<span></span></a></li>
							
						</ul>
					</li>
					<li>
						<div class="left_2">
							<p></p>
							<a href="javascript:void(0);">报表管理</a>
						</div>
						<ul class="jiedian" style="display: none;">
							<li><a href="javascript:void(0);" url="${pageContext.request.contextPath}/app/statement/click-amount.htm">知识点选量明细</a></li>
						</ul>
					</li>
				-->	
					<kbs:if code="gzyd">
						<%--信息模板——渠道宣传 --%>
						<li>
							<div class="left_2">
								<p></p>
								<%--categoryId不为空表示请求来自分类树，不展示操作按钮 --%>
								<a href="javascript:void(0);" onclick="
									TABOBJECT.open({
										id : '渠道宣传',
										name : '渠道宣传',
										title : '渠道宣传',
										hasClose : true,
										url : $.fn.getRootPath() + '/app/custom/gzyd/info-data.htm?mouldName=' 
											+ encodeURI('渠道宣传'),
										isRefresh : true
									}, this);">渠道宣传<b></b></a>
							</div>
						</li>
						<%--短信发送 --%>
						<li>
							<div class="left_2">
								<p></p>
								<a id="smsCategoryMenu" href="javascript:void(0);">短信发送<b></b>
								</a>
							</div>
							<ul id="smsCategoryTree"  checkbox="true"  onlyLeafCheck="true" ></ul>
						</li>
					</kbs:if>
					<!--报表权限用  -->
					<s:set var="roleIds" value="','"></s:set>
					<s:iterator value="#session.session_user_key.roles" var="va">
						<s:set var="roleIds" value="#roleIds+#va.id+','"></s:set>
					</s:iterator>
					<!-- 操作列表输出逻辑如下：	modify by eko.zhan at 2015-04-14
						1、输出level=1的menu
						2、根据父menu输出子menu
							1)、对于key以_report结束的memu，增加userId和roleId参数
							2)、对于url以http开头的menu，1)、解析url中占位符，替换$(username)，$(password)，$(theme)，主要用于流程地址（如果多个配置地址中含有占位符，可以考虑在第一步过滤所有的url）
							3)、其他在url前增加 contextPath
					 -->
					<s:iterator value="#session.session_user_key.list_Menu" var="va1">
						<s:if test="#va1.level==1">
							<li>
								<div class="left_2">
									<p></p>
									<a href="#nogo">${va1.name }</a>
								</div>
								<ul class="jiedian" style="display: none;">
									<s:iterator value="#session.session_user_key.list_Menu" var="va2">
										<s:if test="#va2.parent.key==#va1.key">
											<s:if test="%{#va2.key.endsWith('_report')}">
												<li><a xkey=${va2.key } href="#nogo" url="${va2.url }?userId=${sessionScope.session_user_key.id }&roleIds=${roleIds }">${va2.name }</a></li>
											</s:if>
											<s:elseif test="#va2.key=='workflowManagement'">
												<!-- modify by eko.zhan at 2015-04-21 关于流程管理模块的地址可以在此写死 -->
												<c:if test="${not empty workflowPath}">
													<li><a xkey=${va2.key } href="#nogo" url="<%=workflowPath + "j_acegi_security_check?j_username="+userName+"&j_password="+password+"&targeturl=/kbase.do?theme="+theme %>">${va2.name }</a></li>
												</c:if>
											</s:elseif>
											<s:elseif test="#va2.url.startsWith('http:')">
												<%
													Menu menu = (Menu)pageContext.findAttribute("va2");
													String url = menu.getUrl();
													url = url.replace("${username}", userName);
													url = url.replace("${password}", "1");
													url = url.replace("${theme}", theme);
												%>
												<li><a xkey=${va2.key } href="#nogo" url="<%=url %>">${va2.name }</a></li>
											</s:elseif>
											<s:else>
												<li><a href="#nogo" url="${pageContext.request.contextPath}${va2.url }">${va2.name }</a></li>
											</s:else>
										</s:if>
									</s:iterator>
								</ul>
							</li>						
						</s:if>
					</s:iterator>
				</ul>
			</div>
		</div>
	
		<div class="content_line">
			<a href="javascript:void(0);" flag="left"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/left_san2.jpg" width="3" height="5">
			</a>
		</div>


			<div class="content_right">
				<style type="text/css">
					.content_right_top > div > div {float: left; margin:0px auto;}
					.content_ss > input,.content_ss > a,.content_ss > p, .content_ss > span{float: left; margin:0px auto;}
					.content_ss > a{display: block;width: 54px;}
					.content_yy > a{color: #333; font-size: 12px; height: 32px; line-height: 32px;}
				</style>
				<script type="text/javascript">
					$(function(){
						var query_content = $('div[class="content_right_top"]').children('div:eq(0)');
						var min_width = 0;
						$(query_content).children('div').each(function(){
							if(!$(this).is(":hidden")) /*可见子元素*/
								min_width += $(this).width();
						});
						$(query_content).css({'min-width': min_width});
						//点搜提示位置
						$("#dirSelectedText").css("margin-top",$(query_content).offset().top);
					});
				</script>
				<div id="dirSelectedText" style="text-align: left;position: absolute;margin-top:93px;margin-left: 20px;width: 100%;line-height: 32px;display: none;">
					<font style="padding: 5px;background-color: #F6F6F6;border: 1px solid #E3E3E3;"></font>
					<a id="dirSelectedClear" href="javascript:void(0);" style="margin-left: 5px">╳</a>
				</div>
				<div class="content_right_top" style="text-align: center;width: 100%;">
					<div style="display:inline-block;*display:inline;*zoom:1;width: auto;">
						<div class="content_ss" style="width: auto;float: left;">
							<input class="kuang" name="" type="text" />
							<%-- 
								modify by eko.zhan at 2016-04-08 11:40
								开启wukong版本则只显示一个搜索，不显示归属地、品牌、精准、模糊等，在搜索结果页面里再显示
								
								modify by eko.zhan at 2016-06-07 16:20 
								wukong版本开启，如果系统开启归属地和品牌，这两个选项其实应该显示在首页，方便用户切换地市和品牌
							 --%>
							 <kbs:if prop="version.code=wukong">
								<a href="javascript:void(0);">
									<input class="anniu" id="btnWukongSearch" name="" type="button" />
								</a>
							 </kbs:if>
							 <kbs:if prop="version.code=">
							 	<s:if test="!#request.categoryTag">
									<a href="javascript:void(0);"><input class="anniu" name="" type="button" /></a>
									<p>
										<a frameid="snior_s" id="fujian" href="javascript:void(0);">附件</a>
									</p>
								</s:if>
								
								<span style="display: <%=fullSearchOption %>">
									<a frameid="snior_s" id="quanwen" href="javascript:void(0);">模糊</a>
								</span>
								
								<!-- lvan.li 20160112 精确搜索按钮  -->
								<span>
									<a frameid="exact_s" id="exact" href="javascript:void(0);">精准</a>
								</span>
								<s:if test="#request.categoryTag">
									<span>
										<a frameid="snior_s" id="fujian" href="javascript:void(0);">附件</a>
									</span>
								</s:if>
								<a id="seniorSearchBtn" href="javascript:void(0);">高级</a>
							 </kbs:if>
						</div>
						
						<%-- 归属地和品牌 --%>
						<div class="content_yy">
							<s:if test="#session.dimTags !=null && #session.dimTags.size()>0">
								归属地:
								<select id="location">
									<!--  <option value="">全部</option>-->
									<s:iterator value="#session.dimTags">
										<c:choose>
										     <c:when test="${tag==sessionScope.location}">
										          <option value="${tag}" selected="selected">${name}</option>
										     </c:when>
										     <c:otherwise>
										          <option value="${tag}" >${name}</option>
										     </c:otherwise>
										</c:choose>							    
										<!-- <option value="${tag}" <s:if test="tag==#session.location">selected="selected"</s:if>>${name}</option> -->
									</s:iterator>
								</select>
							</s:if>
							<s:if test="#session.brands !=null && #session.brands.size()>0">
								&nbsp;&nbsp;品牌:
								<select id="brand">
									<!--<option value="">全部</option>-->
									<s:iterator value="#session.brands">
										<option value="${tag}" <s:if test="tag==#session.brand">selected="selected"</s:if>>${name}</option>
									</s:iterator>
								</select>
							</s:if>
							<kbs:if code="gzyd">
								&nbsp;
								<a id="refreshBtn" href="javascript:void(0);" style="line-height: 32px;">刷新</a>
							</kbs:if>
						</div>
					</div>
				</div>
				<!-- 是否开启高级Tab页模式  add by eko.zhan at 2016-06-16 17:35 -->
				<%-- Tab页加载区 --%>
				<c:if test="${!enableAdvTab}">
					<div class="content_right_bottom">
						<div class="content_title">
							<div class="titile">
							</div>
						</div>
						<div class="content_content">
						</div>
					</div>
				</c:if>
				
				<%-- Tab页加载区 jeasyui --%>
				<c:if test="${enableAdvTab}">
					<div id="tabs">
						<div title="&nbsp;&nbsp;首&nbsp;&nbsp;页&nbsp;&nbsp;" style="overflow:hidden;">
				        	<iframe id="home" _isload="0" src="${pageContext.request.contextPath }/app/index/index.htm" scrolling="auto" frameborder="0" style="width:100%;height:98%;"></iframe>
				        </div>
				    </div>
				</c:if>
			</div>
		<!-- 高级搜索弹出框start -->
		<div id="seniorSearchDiv" class="index_d" style="display: none;">
			<div class="index_dan">
				<div class="index_dan_title">
					<b>搜索条件</b><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/xinxi_ico1.jpg" width="12" height="12" />
					</a>
				</div>
				<div class="index_dan_con">
					<form action="">
					<ul>
						<li>
							<b>搜索内容</b>
							<input id="s_content" class="shurukuang" type="text" />
						</li>
						<li class="index_dan_line"></li>
						<li>
							<b>搜索类型</b>
							<label style="float:left;line-height:23px;*width:70px">
								<input type="radio" checked="checked" value="1" name="s_searchType" style="float:left;"><p style="float: right;margin: 0 15px 0 5px;">全文</p>
							</label>
							<label style="float:left;line-height:23px;*width:70px">
								<input type="radio" value="2" name="s_searchType" style="float:left;"><p style="float: right;margin: 0 15px 0 5px;">实例</p>
							</label>
							<label style="float:left;line-height:23px;*width:70px">
								<input type="radio" value="3" name="s_searchType" style="float:left;"><p style="float: right;margin: 0 15px 0 5px;">附件</p>
							</label>
						</li>
						<li>
							<b>知识时效</b>
							<select id="s_agingType">
								<option value="0">全部</option>
								<option value="1">常态</option>
								<option value="2">未过期</option>
								<option value="3">未过追溯期</option>
								<option value="4">已过追溯期</option>
							</select>
						</li>
						<li>
							<b>创建日期</b>
							<p>
								<input id="s_createDate" type="text" />
							</p>
							<b>至</b>
							<p>
								<input id="s_endDate" class="wu" type="text" style="height:24px;line-height:24px;width:100px;" />
							</p>
						</li>
						<li class="index_dan_line"></li>
						<li>
							<b>知识目录</b>
							
							<select id="s_catgory_dir" class="se">
								<option value="" selected="selected">全部</option>
								<s:iterator var="oc" value="#session.ontologyCategorys">
									<option value="<s:property value="#oc.id"/>">
										<s:property value="#oc.name"/>
									</option>
								</s:iterator>
							</select>
							
						</li>
						
						<li>
							<s:if test="#session.locationEnable">
								<b></b><div></div>
							</s:if>
							<s:else>
								<b>地区</b>
								<div class="duochulai">
								<select id="s_location" class="wu">
									<option value="" selected="selected">全部</option>
									<s:iterator var="dt" value="#session.locasInSearch">
										<option value="<s:property value="#dt.tag"/>" <s:if test="tag==#session.location">selected="selected"</s:if>>
											<s:property value="#dt.name"/>
										</option>
									</s:iterator>
								</select>
								</div>
							</s:else>
							<s:if test="#session.brandEnable">
								<b></b><div></div>
							</s:if>
							<s:else>
								<b>品牌</b>
								<div class="duochulai">
								<select id="s_brand" class="wu">
									<option value="" selected="selected">全部</option>
									<s:iterator var="dt" value="#session.brandsInSearch">
										<option value="<s:property value="#dt.tag"/>" <s:if test="tag==#session.brand">selected="selected"</s:if>>
											<s:property value="#dt.name"/>
										</option>
									</s:iterator>
								</select>
								</div>
							</s:else>
						</li>
	
						<li>
							<a id="s_cancel" href="javascript:void(0);">
								<input class="index_aa1" type="reset" value=""/>
							</a>
							<a id="s_commit" href="javascript:void(0);">
								<input class="index_aa2" type="submit" value=""/>
							</a>
						</li>
					</ul>
					</form>
				</div>
			</div>
		</div>
		<!-- 高级搜索弹出框end -->
		
		<!--  目录搜索start -->
		
		<div id="dirSearch" class="index_d" style="display: none;width: 500px;">
			<div class="index_dan" style="width: 500px;">
				<div class="index_dan_title" style="width: 500px;">
					<b>搜索条件</b><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/xinxi_ico1.jpg" width="12" height="12" />
					</a>
				</div>
				<div class="index_dan_con" style="width: 90%;height: 80%;">
					<form id="dir_form" action="">
					<ul>
						<li>
							<b>搜索内容</b>
							<input id="dir_content" class="shurukuang" type="text" />
						<br><br></li>
						<li class="index_dan_line"><br><br></li>
						<li>
							<s:iterator value="#session.allDims" var="dims" status="u">
								<s:if test='code == "location" || code == "brand" || code == "platform"'>
									<s:if test="#u.odd && !#u.first">
										<br/><br/>
									</s:if>
									<b style="width:15%;">${dims.name }</b>
									<div class="duochulai">
										<select id="dim_${dims.code}" class="wu">
											<s:if test='code != "platform"'>
												<option value="" selected="selected">全部</option>
											</s:if>
											<s:iterator value="#dims.dimTags" var="d" >
												<s:if test='code == "location"'>
													<option value="${d.tag }" <s:if test="tag==#session.location">selected="selected"</s:if>>${d.name }</option>
												</s:if>
												<s:elseif test='code == "platform"'>
													<!--  <option value="${d.tag }" <s:if test='tag == "kbase"'>selected="selected"</s:if>>${d.name }</option>-->
													<!-- @lvan.li 20160705广州12345 目录搜索platform只显示kbase维度的 -->
													 <s:if test='tag == "kbase"'>
														<option value="${d.tag }" selected="selected">${d.name }</option>
													 </s:if>
												</s:elseif>
												<s:elseif test='code == "brand"'>
													<option value="${d.tag }" <s:if test="tag==#session.brand">selected="selected"</s:if>>${d.name }</option>
												</s:elseif>
												<s:else>
													<option value="${d.tag }">${d.name }</option>
												</s:else>
											</s:iterator>
										</select>
									</div>
								</s:if>
							</s:iterator>	
						<br><br><br></li>
						
						<li><br><br><br><br>
							<a id="dir_cancel" href="javascript:void(0);"><input class="index_aa1" type="reset" value=""/>
							</a>
							<a id="dir_commit" href="javascript:void(0);">
								<input class="index_aa2" type="submit" value=""/>
							</a>
						<br><br><br></li>
					</ul>
					</form>
				</div>
			</div>
		</div>
		
		<!-- 目录搜索end -->
		
		<!-- 知识树右键菜单start -->
		<div id="treeMenu" class="index_dankuang">
			<ul>
				<li onclick="newPanel();"><a href="javascript:void(0);">在新面板中打开</a></li>
			</ul>
		</div>
		<div id="treeMenuJ" class="easyui-menu" style="width:120px;">
			<div data-options="iconCls:'icon-add'">在新面板中打开</div>
		</div>
		<!-- 知识树右键菜单end -->
		<!-- 右下角公告提示start -->
		<div class="ggzs_dan">
			<div class="ggzs_dan_title">
				<b>知识库公告</b>
				<a href="javascript:void(0);">
					<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/xinxi_ico1.jpg" width="12" height="12"/>
				</a>
			</div>
			<div class="ggzs_dan_con" style="overflow-y:auto;overflow-x:hidden; ">
				<b style="display: inline;">公告标题</b>
				<a href="javascript:void(0);" onfocus="this.blur();"></a>
			</div>
		</div>
		
		<!-- modify by eko.zhan at 2016-08-05 16:24 -->
		<div id="win">
       		<%--
       		公告采用ajax请求，不刷新iframe了，刷新iframe会有空白出现 modify by eko.zhan at 2016-10-09
       		<iframe id="popupbox" scrolling="no" style="position:fixed;bottom:25;right:25;_position:absolute;width:450px;height:190px;border:0;"></iframe>
       		 --%>
       		<table id="popupPanel" class="box" style="width:100%;" cellspacing="1" cellpadding="1">
       			<tr>
       				<th width="60%">标题</th>
       				<th width="20%">发布时间</th>
       				<th width="20%">发布人</th>
       			</tr>
       		</table>
		</div>
		<!-- 右下角公告提示end -->
		
		<!-- 右键菜单start -->
		<div id="menu_close" class="index_dankuang">
			<ul>
				<li><a href="javascript:void(0);">关闭其他标签页</a></li>
			</ul>
		</div>
		<!-- 右键菜单end -->
		<div id="backTop"></div>
		
		
		<!-- 是否开启高级Tab页模式  add by eko.zhan at 2016-06-16 17:35 -->
		<c:if test="${enableAdvTab}">
		
			<%-- add by eko.zhan at 2016-06-16 11:35 首页Tab采用jeasyui  --%>
			<div id="mm" class="easyui-menu" style="width:120px;">
			<!-- 取消菜单右键关联  add by ainloong at 2016-06-28 -->
	        	<div id="menuRefresh">刷新</div>
	        	<div id="menuCloseOther">关闭其他标签</div>
	        </div>
	        
			<script type="text/javascript">
				//modify by eko.zhan at 2016-06-17 14:29 动态设置tab宽度高度
				//modify by eko.zhan at 2016-06-25 11:15 将窗口高宽设置为方法，在多处使用
				//获取主窗体高度
				function getTabHeight(){
					var _height = SystemKeys.userData.bannerCollapse==1?0:70;
					_height += 60 + 20;	//搜索框的高度， 20是一个缓冲值
					var _tabHeight = $(window).height()-_height;
					return _tabHeight;
				}
				//获取主窗体宽度
				function getTabWidth(){
					return $(window).width()-10-$('.content_left_bg').width();
				}
				
				//var _tabWidth = $(window).width()-10-$('.content_left_bg').width();
				//$('#tabs').height(_tabHeight);
				//$('#tabs').width(_tabWidth);
				
				var onTabChange = false;
			
				$('#tabs').tabs({
					plain: true,
					onContextMenu: function(e, title, index){
						e.preventDefault();
						
						onTabChange = true;
						
						$('#tabs').tabs('select', index);
						
						$('#mm').menu('show', {
	                        left: e.pageX,
	                        top: e.pageY
	                    });
					}
				});
				
				//tabs右键菜单
				//刷新
				$('#menuRefresh').on('click', function(){
					var selectedTab = $('#tabs').tabs('getSelected');
					$(selectedTab).find('iframe')[0].contentWindow.location.reload();
				});
				//关闭所所有
				$('#menuCloseAll').on('click', function(e){
					var tabs = $('#tabs').tabs('tabs');
					$(tabs).each(function(i, item){
						var opts = $(item).panel('options');
						if (opts.closable){
							$('#tabs').tabs('close', opts.title);
						}
					});
				});
				//关闭其他菜单
				$('#menuCloseOther').on('click', function(){
					var selectedTab = $('#tabs').tabs('getSelected');
					var title = $(selectedTab).panel('options').title;
					var tabs = $('#tabs').tabs('tabs');
					$(tabs).each(function(i, item){
						var opts = $(item).panel('options');
						if (opts.closable && opts.title!=title){
							$('#tabs').tabs('close', opts.title);
						}
					});
				});
				//关闭右侧标签页
				$('#menuCloseRight').on('click', function(){
					var selectedTab = $('#tabs').tabs('getSelected');
					var index = $('#tabs').tabs('getTabIndex', selectedTab);
					var tabs = $('#tabs').tabs('tabs');
					var len = tabs.length;
					
					for (var i=0; i<len-index; i++){
						$('#tabs').tabs('close', index+1);
					}
				});
				
				var pageContext = pageContext || {};
				$.extend(pageContext, {
					addTab: function(args){
						var opts = $.extend({
							forceOpen: false
						}, args);
						if (opts.forceOpen){	//是否强制打开，如果强制打开则不判断该页面是否存在
							$('#tabs').tabs('add', {
								id: opts.id,
								title: opts.title,
							    content: '<iframe src="" scrolling="auto" frameborder="0" style="width:100%;height:98%;overflow-y:auto;border:0;"></iframe>',
							    closable: opts.closable==undefined?true:opts.closable,
							    width: '100%',
							    height: '98%'
							});
							
							//modify by eko.zhan at 2016-07-11 10:40 jeasyui tab iframe 在低版本浏览器上会加载两次，尽管第一次请求是 aborted，但后台日志依然记录，导致都是两次
							var tab = $('#tabs').tabs('getSelected');
							tab.find('iframe').attr('src', opts.url);
							
						}else if ($('#tabs').tabs('exists', opts.title)){
							//已存在相同标题页签
							$('#tabs').tabs('select', opts.title);
							var tab = $('#tabs').tabs('getSelected');  // get selected panel
							//modify by eko.zhan at 2016-06-20 10:10 相同页签刷新页签的内容
							//console.log(tab);
							//console.log(tab.find('iframe'));
							/*
							$('#tt').tabs('update', {
								tab: tab,
								options: {
									title: opts.title,
									href: opts.url  // the new content URL
								}
							});*/
							//tab.panel('refresh', opts.url);
							tab.find('iframe').attr('src', opts.url);
						}else{
							$('#tabs').tabs('add', {
								id: opts.id,
								title: opts.title,
							    content: '<iframe src="" scrolling="auto" frameborder="0" style="width:100%;height:98%;overflow-y:auto;border:0;"></iframe>',
							    closable: opts.closable==undefined?true:opts.closable,
							    width: '100%',
							    height: '98%'
							});
							
							//modify by eko.zhan at 2016-07-11 10:40 jeasyui tab iframe 在低版本浏览器上会加载两次，尽管第一次请求是 aborted，但后台日志依然记录，导致都是两次
							var tab = $('#tabs').tabs('getSelected');
							tab.find('iframe').attr('src', opts.url);
						}
					}, 
					
					/* 刷新Tab主窗体高宽 */
					resizeTab: function(width, height, timeout){
						/**
						 * 当前选中tab的index
						 * 造成这个问题原因是jquery.easyui.min.js中3395行（cc.width(opts.width).height(opts.height);	），
						 * 因此在无法修改Main.jsp布局的情况下使用再次选中tab的方式来修复tabs宽、高初始化之后tab选中不正确的问题
						 * @author Gassol.Bi @date Jun 29, 2016 11:05:56 AM
						 *
						 * modify by eko.zhan at 2016-07-14 11:52 针对IE浏览器和高版本浏览器修复resize选中的问题
						 *
						 * modify by eko.zhan at 2016-07-19 10:30 
						 */
						var tab = $('#tabs').tabs('getSelected');
						var index = $('#tabs').tabs('getTabIndex', tab);
						
						timeout = timeout || 500;
						window.setTimeout(function(){
							if (width!=null){
								$('#tabs').tabs({width: width});
							}
							if (height!=null){
								$('#tabs').tabs({height: height});
							}
							$('#tabs').tabs('resize');
							if (isLowerBrowser()){
								$('#tabs').tabs('select', $('#tabs').tabs('tabs').length-1); /*选中最后一个tab*/
							}else{
								$('#tabs').tabs('select', index); /*还原选中的tab*/
							}
						}, timeout);
					}
				});
				
				var TABOBJECT = {
					version: 2.0,
					
					open: function(opts){
						if (opts.name=='首页') return false; 
						
						var maxLen = 6;
// 						title名称超限的时候用“...”作后缀
						var titlesuffix = "...";
						var title = $.trim(opts.name || opts.title);
						if (title.length>maxLen){
							/*悬停显示*/
							title = '<span title="'+title+'">'+title.substring(0, maxLen)+titlesuffix+'</span>';
						}
						
						pageContext.addTab({
							url: opts.url,
							title: title,
							id: opts.id,
							forceOpen: opts.forceOpen || false
						});
					},
					
					exists: function(id){
						//smsBox 判断短信窗口是否存在
						if (id=='smsBox'){
							title = '短信发送';
						}
						if ($('#tabs').tabs('exists', title)){
							return 1;
						}
						return -1;
					},
					
					tabContent: {
						//获取短信Tab页签
						children: function(){
							return $('#tabs').tabs('getTab', '短信发送').find('iframe');
						}
					},
					
					initClose: function(){return false;}
				}
				pageContext.resizeTab(getTabWidth(), getTabHeight());
				/** add by eko.zhan at 2016-06-25 10:57 window 最大化刷新 easyuiTab 的高宽 **/
				$(window).resize(function(){
					if (!onTabChange){
						pageContext.resizeTab(getTabWidth(), getTabHeight());
					}
					//onTabChange = false;
				});
				
				/* add by eko.zhan at 2016-06-24 17:10 一些内部的页面会访问parent中的方法，报错 */
				function setCurrentIframeHeight(){return false;};
				
				/**
				 * 判断是否是低版本浏览器 ie6 7 8
				 */
				function isLowerBrowser(){
					var agent = navigator.userAgent.toLowerCase();
					if (agent.indexOf('msie 6')!=-1 || agent.indexOf('msie 7')!=-1 || agent.indexOf('msie 8')!=-1){
						return true;
					}
					return false;
				}
			</script>
		
		</c:if>
	</body>
</html>