<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<div class="gonggao_con_nr_fenye" style="min-width: 650px;" >
	<!--******************分页隐藏域***********************  -->
	<input type="hidden" id="pageNo" name="pageNo" value="${page.pageNo }" />

	<!--***************变量*************** -->
	<!-- 容量 -->
	<s:set var="showPageNoSize" value="9" />
	<!--容量中间位置  -->
	<s:set var="showPageNoSizeMiddle"
		value="#showPageNoSize%2==0?#showPageNoSize/2:(#showPageNoSize+1)/2" />
	<!--***************分页开始*************** -->
	<span>共${page.totalCount }条记录</span>
	<a href="javascript:void(0)" onfocus="this.blur();"
		onclick='<s:if test="%{page.pageNo>1}">pageClick("1")</s:if>'>首页</a>
	<a href="javascript:void(0)" onfocus="this.blur();"
		onclick='<s:if test="%{page.pageNo>1}">pageClick("${page.prePage }")</s:if>'>&lt;上一页</a>
	<s:iterator
		value="page.totalPages<=#showPageNoSize?new int[page.totalPages]:new int[#showPageNoSize]"
		status="statu">
		<s:if
			test="page.totalPages<=#showPageNoSize||page.pageNo<#showPageNoSize">
			<!--总页数不小于容量 或者 当前页码小于容量-->
			<a href="javascript:void(0)" onclick="pageClick('${statu.index+1 }')"
				onfocus="this.blur();"
				class='<s:if test="%{page.pageNo==#statu.index+1 }">dangqian</s:if>'>
				${statu.index+1 } </a>
		</s:if>
		<s:elseif test="page.pageNo+(#showPageNoSize-1)>page.totalPages">
			<!--当前页与容量之和不小于总页数  -->
			<a href="javascript:void(0)"
				onclick="pageClick('${page.totalPages-(showPageNoSize-1)+statu.index }')"
				onfocus="this.blur()"
				class='<s:if test="%{page.pageNo+(#showPageNoSize-1)-page.totalPages == #statu.index }">dangqian</s:if>'>
				${page.totalPages-(showPageNoSize-1)+statu.index } </a>

		</s:elseif>
		<s:else>
			<!--当前页不小于容量&&当前页与容量之和小于总页数  -->
			<a href="javascript:void(0)"
				onclick="pageClick('${page.pageNo-showPageNoSizeMiddle+statu.index+1 }')"
				onfocus="this.blur()"
				class='<s:if test="%{#statu.index+1 == #showPageNoSizeMiddle }">dangqian</s:if>'>
				${page.pageNo-showPageNoSizeMiddle+statu.index+1 } </a>
		</s:else>
	</s:iterator>
	<a href="javascript:void(0)" onfocus="this.blur();"
		onclick='<s:if test="%{page.totalPages>page.pageNo}">pageClick("${page.nextPage }")</s:if>'>下一页&gt;</a>
	<a href="javascript:void(0)" onfocus="this.blur();"
		onclick='<s:if test="%{page.totalPages>page.pageNo}">pageClick("${page.totalPages }")</s:if>'>尾页</a>
	<!--***************分页结束 ***************-->
</div>
