<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>选择部门和用户-单选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			body{
				margin: 3px;
			}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div {
				width: 500px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 400px;
				overflow: auto;
			}
			input {
				border: 1px solid #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/util/picker!deptuserJson.htm",
					autoParam:["id", "name", "orgtype", "level"]
				},
				callback: {
					onClick: ztreeClick
				}
			};
			
			function ztreeClick(event, treeId, treeNode, clickFlag){
				if (treeNode.orgtype=='USER'){
					$("#id").val(treeNode.id);
					var _nval = (treeNode.value!=undefined)?treeNode.value:treeNode.name;
					$("#desc").val(_nval);
				}
			}
			
			$(function(){
				$.fn.zTree.init($("#treeDemo"), setting);
				
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
					}
					
					//回显值
					$("#desc").val(parent.$("#"+descField).val());
					$("#id").val(parent.$("#"+idField).val());
				}
				
				$("#btnOk").click(function(){
					var id = $("#id").val();
					var desc = $("#desc").val();
					if (descField!=null){
						parent.$("#"+descField).val(desc);
						parent.$("#"+idField).val(id);
					}
					$("#btnClose").click();
				});
				
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
			});
		</script>
	</head>

	<body>
		<div class="ztree" id="treeDemo"></div>
		<div style="text-align:right;margin-top:5px;">
			<input id="desc" style="width:200px;">
			<input type="hidden" id="id">
			<button class="button button-primary button-rounded button-small" id="btnOk">确定</button>
			<button class="button button-primary button-rounded button-small" id="btnClose">取消</button>
		</div>
		<br>
	</body>
</html>
