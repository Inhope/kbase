<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>选择部门和用户-多选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:99%;height:99%;}
			body{
				margin: 3px;
			}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 398px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/util/picker!deptpostJson.htm",
					autoParam:["id", "name", "orgtype", "level"],
					dataFilter: ajaxDataFilter,
					otherParam:{"nocheck":"true"}
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "p", "N": "s" }
				},
				callback: {
					onCheck: ztreeOnCheck
				}
			};

			function ztreeOnCheck(event, treeId, treeNode){
			    if (treeNode.orgtype=='POST'){
			        if(treeNode.checked){
			            //右侧选中显示区域去增加当前岗位信息
			            updateSelectOptions(treeNode.id, treeNode.name, 1);	
			        }else{
			            parentNodeUnChecked(treeNode);
			            //右侧选中显示区域去删除当前岗位信息
			            updateSelectOptions(treeNode.id, treeNode.name, 0);	
			        }
		        }else{
                    if(treeNode.checked){
                        var childrenNodes = treeNode.children;
                        if(childrenNodes && childrenNodes.length>0){//选中所有子节点    
			               disAllChildrenNodes(childrenNodes,1);           
                        }else{//加载所有子节点
                        	var ztreeObj = $.fn.zTree.getZTreeObj("treeDemo");
				            ztreeObj.updateNode(treeNode);
				            ztreeObj.reAsyncChildNodes(treeNode, "refresh");
                        }
			        }else{//右侧选中显示区域去除当前节点所有子节点中对应的岗位信息
			            parentNodeUnChecked(treeNode);
			            disAllChildrenNodes(treeNode.children,0);//取消选中所有子节点
			        }
		        }
			}
			
			function ztreeOnBeforeExpand(treeId, treeNode){
				treeNode.nocheck = true;
			}
			
			/**
			* 清除节点所有上级节点的选择状态
			* @param treeNode 当前操作的节点对象 
			*/
			function parentNodeUnChecked(treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj("treeDemo");
				if(treeNode.getParentNode()){
					ztreeObj.checkNode(treeNode.getParentNode(),false);
					parentNodeUnChecked(treeNode.getParentNode());
				}
			}
			
			/**
			* 对后台加载的数据进行过滤处理，
			* 如果父节点(部门节点)为选中状态，选中该部门节点下的所有加载的岗位节点
			* 并在右侧的选中岗位信息显示区域中增加所有选中的岗位信息。
			*/
			function ajaxDataFilter(treeId, parentNode, responseData){
				if (parentNode) {
					if(parentNode.checked){
						for(var i =0; i < responseData.length; i++) {
						    if (responseData[i].orgtype=='POST'){
						        responseData[i].checked = true;
						        updateSelectOptions(responseData[i].id, responseData[i].name, 1);			           
						    }
						}				      
					}
				}
				return responseData;			
			}
			
			/**
			* 选中/取消选中 所有子节点
			* @param childrenNodes 当前节点的所有子节点
			* @param type 1-选中  0-取消选中
			*/
			function disAllChildrenNodes(childrenNodes, type){
			     var ztreeObj = $.fn.zTree.getZTreeObj("treeDemo");
                 if(childrenNodes && childrenNodes.length>0){
			          for(var i=0;i<childrenNodes.length;i++){
			               ztreeObj.checkNode(childrenNodes[i], ((type==1)?true:false));
			               if(childrenNodes[i].orgtype=='POST'){
			                   updateSelectOptions(childrenNodes[i].id, childrenNodes[i].name, type);	
			               }else{
			                   disAllChildrenNodes(childrenNodes[i].children,type);
			               }
			          }                  
                 }			
			}
			
			/**
			* 更新对话框右侧选择条目信息
			* @param id 条目value
			* @param name 条目显示text
			* @param type 更新类型 1-增加条目 2-删除条目 
			*/				
			function updateSelectOptions(id, name, type){
			    if (type == 1){
					if ($("#sel option[value='"+id+"']").length==0){
						$("#sel").append('<option value="'+id+'">'+name+'</option>');
					}
				}else{
					if ($("#sel option[value='"+id+"']").length>0){
						$("#sel option[value='"+id+"']").remove();
					}
				}
			}
			
			$(function(){
				var ztreeObj = $.fn.zTree.init($("#treeDemo"), setting);
				
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
					}
					
					//回显值
					window.setTimeout(function(){
						if (parent.$("#"+descField).val().length>0){
							var descArr = parent.$("#"+descField).val().split(",");
							var idArr = parent.$("#"+idField).val().split(",");
							
							$(descArr).each(function(i, item){
								//TODO 回显取消勾选树有存在bug，只能回显顶级，而且如果选择项太多会对页面性能有影响
								var ztreeNodes = ztreeObj.getCheckedNodes(false);
								$(ztreeNodes).each(function(j, node){
									if (node.id==$.trim(idArr[i])){
										ztreeObj.checkNode(node, true);
									}
								});
								$("#sel").append('<option value="'+$.trim(idArr[i])+'">'+$.trim(item)+'</option');
							});
						}
					}, 100);
				}
				
				//移除选中的值
				$("#btnMoveout").click(function(){
					var id = $('#sel option:selected').val();
					if (id!=undefined && id.length>0){
						var ztreeNodes = ztreeObj.getCheckedNodes();
						$(ztreeNodes).each(function(i, item){
							if (item.id==id){
								ztreeObj.checkNode(item, false);
								parentNodeUnChecked(item);
							}
						});
					}
					$('#sel option:selected').remove();
				});
				//双击取消选中的select
				$("#sel").dblclick(function(){
					$("#btnMoveout").click();
				});
				
				//确定
				$("#btnOk").click(function(){
					/**
					 * 不能清空选择的值
					 * @author Gassol.Bi
					 */
					var descArr = [];
					var idArr = [];
					$("#sel option").each(function(i, item){
						descArr.push($(item).text());
						idArr.push($(item).val());
					});
					if (descField!=null){
						parent.$("#"+descField).val(descArr.join(","));
						parent.$("#"+idField).val(idArr.join(","));
					}
					
					$("#btnClose").click();
				});
				//取消
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
				
				/*初始化页面高度*/
				$("#treeDemo").css("height", $(window).height()-80);
				$('#sel').css("height", $(window).height()-70);
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<div class="ztree" id="treeDemo"></div>
				</td>
				<td width="6%">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%">
					<select id="sel" size="28" style="width:230px;height:398px;border:1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<button class="button button-primary button-rounded button-small" id="btnOk">确定</button>
					<button class="button button-primary button-rounded button-small" id="btnClose">取消</button>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
