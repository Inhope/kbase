<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>选择部门和用户-多选(回调版)</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:99%;height:99%;}
		</style>
		<style type="text/css">
			body{
				margin: 3px;
			}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div {
				width: 240px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 398px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript">
			$(function(){
				/*树单选radio、多选checkbox, 父部门是否允许勾选, 初始化数据*/
				var _chkStyle = '${param.chkStyle}', 
					_deptNocheck = '${param.deptNocheck}', _initData;
				if(!_chkStyle) _chkStyle = 'checkbox';
				if(!_deptNocheck) _deptNocheck = 'true';
				/*相关操作*/
				var TreeUtils = {
					tree:{
						treeId: 'treeDemo',
						treeObj: null,
						setting:  {
							async: {
								enable: true,
								url: '${pageContext.request.contextPath}/app/util/picker!deptuserJson.htm',
								autoParam:["id", "name", "orgtype", "level"],
								otherParam:{"nocheck": _deptNocheck}
							},
							view: {
								dblClickExpand: false,
								selectedMulti: true
							},
							data: {
								simpleData: {
									enable: true
								}
							},
							check: {
								enable: true,
								chkStyle: _chkStyle,
								chkboxType: { "Y": "s", "N": "s" }
							},
							callback: {
								onCheck: function(event, treeId, treeNode){
									var fn = TreeUtils.fn, tree = TreeUtils.tree;
									/*选中的节点*/
									var nodes = new Array();
									if(treeNode.orgtype == 'USER')
										nodes.push(treeNode);
									else {
										nodes = tree.getChildNodes(nodes, treeNode);
									}
									
									/*右侧区域回显*/
									if (treeNode.checked) fn.rightHandle(nodes, 'Add');
									else fn.rightHandle(nodes, 'Del');
								},
								onAsyncSuccess: function(event, treeId, treeNode, msg){
									var _that = TreeUtils, /*回显数据*/
										initData = _that.fn.getObjByName('${param.initData}');
									if(!_initData) {
										/*数据回显*/
										if(initData.length > 1)/*回掉*/
											_initData = initData[1].call(initData[0]);
										else _initData = initData();
									}
									if(_initData){
										if($.isArray(_initData)){/*返回指定类型的数据*/
											var nodes = _initData;
											if(nodes && nodes.length > 0){
												_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
												_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
											}
										} else {/*返回所有的数据*/
											for(k in _initData){
												var nodes = _initData[k];
												if(nodes && nodes.length > 0){
													_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
													_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
												}
											}
										}
									}
								} 
							}
						},
						/*获取树对象*/
						getZTreeObj: function(){
							return $.fn.zTree.getZTreeObj(this.treeId);
						},
						/*获取当前节点的所有子节点（不包括用户节点）*/
						getChildNodes: function(nodes, treeNode){
							var _that = this;
							if(treeNode.orgtype == 'USER') nodes.push(treeNode);
							else {
								if (treeNode.isParent){
									for(var ind in treeNode.children){
										_that.getChildNodes(nodes, treeNode.children[ind]);
									}
								}
							}
							return nodes;
						},
						/*树初始化*/
						init: function(){
							/*初始化页面高度*/
							$("#" + this.treeId).css("height", $(window).height()-85);
							$('#sel').css("height", $(window).height()-70);
							/*初始化树*/
							return this.treeObj = $.fn.zTree.init($("#" + this.treeId), this.setting);
						}
					},
					fn: {
						/*右侧区域数据处理*/
						leftHandle: function (objs, flag){
							if(!objs) return false;
							var _that = this, 
								ztreeObj = TreeUtils.tree.getZTreeObj();
							if(flag == 'Add'){//添加
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) ztreeObj.checkNode(node, true, null);
								});
							} else if(flag == 'Del'){//删除
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) {
										ztreeObj.checkNode(node, false);
										_that.removeNode('id', node.id);
									}
								});
							}
						},
						/*左侧区域数据删除*/
						rightHandle: function (objs, flag){
							if(!objs) return false;
							var _that = this;
							if(flag == 'Add'){/*添加*/
								/*单选的应该先移除掉其他属性*/
								var ztreeObj = TreeUtils.tree.getZTreeObj();
								if(ztreeObj.setting.check.chkStyle == 'radio')
									$("#sel").empty();
								$(objs).each(function(i, node){
									if ($("#sel option[value='"+node.id+"']").length==0){
										$('#sel').append('<option value="' + node.id + '">' + node.name + '</option>');
									}
								});
							} else if(flag == 'Del'){/*删除*/
								$(objs).each(function(i, node){
									if ($("#sel option[value='" + node.id + "']").length > 0){
										$("#sel option[value='" + node.id + "']").remove();
										_that.removeNode('id', node.id);
									}
								});
							}
						},
						searchLabelObj: function (){
							var keywords = $('#keywords').val(), i = 0;/*搜索结果*/
							if(keywords){
								var ztreeObj = TreeUtils.tree.getZTreeObj();
								ztreeObj.getNodesByFilter(function(node){
									var pattern = new RegExp('.*' + keywords + '.*', 'g');
									if(node.type == 'paper' && pattern.test(node.name)){
										ztreeObj.selectNode(node, true);
										i++;
									} else {
										ztreeObj.cancelSelectedNode(node);
									}
									return null;
								}); 
							}
							$('#keywords')[0].focus();
							if(i == 0) parent.layer.alert('未查询到相关试卷!', -1);
						},
						/*名称获取父页面对象*/
						getObjByName: function(name){
							var obj = parent;
							if(name.indexOf('.') > -1){
								var _that;
								$(name.split('.')).each(function(i, item){
									_that = obj;/*this对象应改为上一级*/
									obj = $(obj).attr(item);
								});
								return [_that, obj];
							} else {
								return $(obj).attr(name);
							}
						},
						/*节点移除*/
						removeNode: function(key, value){
							if(_initData && key) {
								var nodes = new Array();
								$(_initData).each(function(ind, node){
									if(node[key] != value) nodes.push(node);
								});
								_initData = nodes;
							}
						}
					},
					/*初始化*/
					init: function(){
						/*树初始化*/
						var _that = this, ztreeObj = _that.tree.init(), 
						 	callback = _that.fn.getObjByName('${param.callback}');/*回掉函数*/
						
						/*确定*/
						$("#btnOk").click(function(){
							var data = new Array();
							if ($("#sel option").length>0){
								$("#sel option").each(function(i, item){
									data.push({id: $(item).val(), name: $(item).text()});
								});
								/*回掉成功后*/
								var backSucc = function(){
									$("#btnClose").click();
								};
								/*回掉*/
								if(callback.length > 1) 
									callback[1].call(callback[0], data, backSucc);
								else callback(data, backSucc);
							}
						});
						
						/*取消*/
						$("#btnClose").click(function(){
							parent.layer.close(parent.__kbs_picker_index);
						});
						
						/*移除选中的值*/
						$("#btnMoveout").click(function(){
							var id = $('#sel option:selected').val();
							if (id){
								var nodes = new Array();//回显节点
								nodes.push({'id':id});
								_that.fn.rightHandle(nodes, 'Del');//回显操作（右侧）
								_that.fn.leftHandle(nodes, 'Del');//回显操作（左侧）
							}
						});
						
						/*双击取消选中的select*/
						$("#sel").dblclick(function(){
							$("#btnMoveout").click();
						});
						
						/*移入选中的值*/
						$('#btnMovein').click(function(){
							var nodes = ztreeObj.getSelectedNodes();
							_that.fn.rightHandle(nodes, 'Add');//回显操作（右侧）
							_that.fn.leftHandle(nodes, 'Add');//回显操作（左侧）
						});
						
						/*点击搜索按钮，搜索用户*/
						$('#searchLabelObj').click(function(){
							_that.fn.searchLabelObj();
						});
						
						/*回车，搜索用户*/
						$('#keywords').bind('keydown', function(event) {
							if (event.keyCode=="13") {
								_that.fn.searchLabelObj();
							}
						});
					}
				}
				TreeUtils.init();
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<div class="ztree" id="treeDemo"></div>
				</td>
				<td width="6%">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%">
					<select id="sel" size="28" style="width:230px;height:398px;border:1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<button class="button button-primary button-rounded button-small" id="btnOk">确定</button>
					<button class="button button-primary button-rounded button-small" id="btnClose">取消</button>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
