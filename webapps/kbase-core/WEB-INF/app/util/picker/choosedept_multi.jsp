<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML">
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>用户部门多选</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			body{
				margin: 3px;
			}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div {
				width: 240px;
			}
			.ztree {
				border: 1px solid #F3F3F3;
				height: 398px;
				overflow: auto;
			}
			table {
				border: 1px solid #F3F3F3;
			}
			td {
				border:1px dotted #F3F3F3;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/util/picker!deptJson.htm",
					autoParam:["id", "name",  "level"]
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "", "N": "" }
				},
				callback: {
					onCheck: ztreeOnCheck,
					beforeCheck: ztreeOnBeforeCheck,
					onExpand: ztreeOnExpand
				}
			};
			
			function ztreeOnCheck(event, treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				if (treeNode.checked){
					if ($("#sel option[value='"+treeNode.id+"']").length==0){
						$("#sel").append('<option value="'+treeNode.id+'" _pid="'+treeNode.bh+'">'+treeNode.name+'</option>');
					}
				}else{
					if ($("#sel option[value='"+treeNode.id+"']").length>0){
						$("#sel option[value='"+treeNode.id+"']").remove();
					}
				}
				
			}
			
			function ztreeOnBeforeCheck(treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				return true;
			}
			
			function ztreeOnExpand(event, treeId, treeNode){
				var ztreeObj = $.fn.zTree.getZTreeObj(treeId);
				if (treeNode.checked){
					var subNodes = ztreeObj.getNodesByParam("objtype", "KBVAL", treeNode);
					$(subNodes).each(function(i, item){
						ztreeObj.checkNode(item, treeNode.checked, true, true);
					});
				}
			}
			
			function ztreeOnBeforeExpand(treeId, treeNode){
				treeNode.nocheck = true;
			}
			
			$(function(){
				var ztreeObj = $.fn.zTree.init($("#treeDemo"), setting);
				
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				var pidField = null;
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
						if (paramArr.length>2){
							pidField = paramArr[2]
						}
					}
					
					//回显值
					window.setTimeout(function(){
						if (parent.$("#"+descField).val().length>0){
							var descArr = parent.$("#"+descField).val().split(",");
							var idArr = parent.$("#"+idField).val().split(",");
							var pidArr = null;
							if (pidField!=null){
								pidArr = parent.$("#"+pidField).val().split(",");
							}
							
							$(descArr).each(function(i, item){
								//TODO 回显取消勾选树有存在bug，只能回显顶级，而且如果选择项太多会对页面性能有影响
								var ztreeNodes = ztreeObj.getCheckedNodes(false);
								$(ztreeNodes).each(function(j, node){
									if (node.id==$.trim(idArr[i])){
										ztreeObj.checkNode(node, true);
									}
								});
								if (pidArr==null){
									$("#sel").append('<option value="'+$.trim(idArr[i])+'">'+$.trim(item)+'</option>');
								}else{
									$("#sel").append('<option value="'+$.trim(idArr[i])+'" _pid="'+$.trim(pidArr[i])+'">'+$.trim(item)+'</option>');
								}
							});
						}
					}, 100);
				}
				
				//移除选中的值
				$("#btnMoveout").click(function(){
					var id = $('#sel option:selected').val();
					if (id!=undefined && id.length>0){
						var ztreeNodes = ztreeObj.getCheckedNodes();
						$(ztreeNodes).each(function(i, item){
							if (item.id==id){
								ztreeObj.checkNode(item, false);
							}
						});
					}
					$('#sel option:selected').remove();
				});
				//双击取消选中的select
				$("#sel").dblclick(function(){
					$("#btnMoveout").click();
				});
				
				//确定
				$("#btnOk").click(function(){
					if ($("#sel option").length>0){
						var descArr = [];
						var idArr = [];
						var pdescArr = [];
						var pidArr = [];
						//
						$("#sel option").each(function(i, item){
							descArr.push($(item).text());
							idArr.push($(item).val());
							/*
							if ($.inArray($(item).attr('_pid'), pidArr)==-1){
								pidArr.push($(item).attr('_pid'));
							}*/
							pidArr.push($(item).attr('_pid'));
						});
						
						if (descField!=null){
							parent.$("#"+descField).val(descArr.join(","));
							parent.$("#"+idField).val(idArr.join(","));
							if (pidField!=null){
								parent.$("#"+pidField).val(pidArr.join(","));
							}
						}
					}
					
					$("#btnClose").click();
				});
				//取消
				$("#btnClose").click(function(){
					parent.layer.close(parent.__kbs_picker_index);
				});
			});
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<div class="ztree" id="treeDemo"></div>
				</td>
				<td width="6%">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png" alt="移入" style="cursor: pointer;" id="btnMovein">
					<br><br><br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png" alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%">
					<select id="sel" size="28" style="width:230px;height:398px;border:1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<button class="button button-primary button-rounded button-small" id="btnOk">确定</button>
					<button class="button button-primary button-rounded button-small" id="btnClose">取消</button>
				</td>
			</tr>
		</table>
		<br>
	</body>
</html>
