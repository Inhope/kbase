<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
<style type="text/css">
	.cke_button__myimage_icon {background: url(${pageContext.request.contextPath }/library/ckeditor/skins/moono/icons.png) no-repeat 0 -936px !important;}  
  	.cke_button__myimage_label {display: inline !important;}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	$.extend($.fn, {
		getRootPath : function(){
		    return "${pageContext.request.contextPath }";
		}
	});
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/CkEditImage.js"></script>

<%-- 图片插入 --%>
<div id="ckEdit-myImage">
	<div id="ckEdit-myImage-tabs" class="easyui-tabs" style="width:100%;display: none;" data-options="fit:true">
	    <div title="选择" data-options="fit:true">
	    	<div style="margin: 5px;">
	    		<input type="text" class="easyui-textbox" data-options="required:true,readonly:true" 
		    		id="image_link" style="width: 240px; height: 26px;">
		    	<input type="hidden" id="imagepath_link" />
		    	<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search'" 
		    		id="imagechoose_link">浏览</a>
		    	<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-save', 
		    		onClick: function(){
		    			var editor = CkEditImage.create.getEditor();
				    	var imagepath = $('#imagepath_link').val();
				    	/*插入图片*/
				    	if(imagepath) {
				    		var image = editor.document.createElement('img', {attributes:{'src': imagepath}});
							editor.editable().insertElement(image);
				    	}
				    }" 
		    		id="imageadd_link" style="margin-left: 2px;">插入</a>
	    	</div>
	    </div>   
	    <div title="上传" data-options="">
	    	<form id="form-ckEdit-myImage" action="" method="post" enctype="multipart/form-data">
		    	<div style="margin: 5px;">
		    		<%--图片选择框 --%>
		    		<input type="file" name="upload" style="width: 365px;float: left;" 
						onchange="javascript:CkEditImage.fn.check(this);">
					<br>
					<p>
					 图片名称：<input class="easyui-textbox" id="image_rename" style="width: 240px;height: 26px;"  name="image_rename">
				        <%--上传按钮 --%>
				        <a href="javascript:void(0);" id="exe_upload" class="easyui-linkbutton"
				        	data-options="iconCls:'icon-search',onClick:function(){
				        	    
				        		var rename = $.trim($('#image_rename').textbox('getValue'));
				        		$('#form-ckEdit-myImage').form('submit', {
				        			/*附件上传地址*/
								    url: '${pageContext.request.contextPath}/app/biztpl/article!imageUpload.htm?rename='+rename,    
								    onSubmit: function(param){
								    	return $(this).form('validate');
								    },
								    success: function(jsonResult){
								    	jsonResult = eval('(' + jsonResult + ')');
								    	$.messager.alert('消息', jsonResult.msg);
								    	if(jsonResult.rst) {
								    		$('#tabs').tabs('select', '选择');/*切换tab*/
								    		$('#image_link').textbox('setText', jsonResult.filename);
								    		$('#imagepath_link').val(jsonResult.basepath + jsonResult.filepath);
								    	}
								    }
								});
				        	}">上传</a>
					</p>
		    	</div>
	        </form>
	    </div>   
	</div>
</div>

<%-- 图片插入->图片选择 --%>
<div id="choose_file-myImage"></div>
