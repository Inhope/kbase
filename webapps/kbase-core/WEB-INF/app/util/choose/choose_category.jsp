<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>知识分类选择</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="styleSheet" type="text/css" href="${pageContext.request.contextPath}/library/layer/skin/layer.css"  id="layermcss" />
		<style type="text/css">
			.xubox_botton2 {
			    background-position: -5px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: -76px;
			    width: 71px;
			}
			.xubox_botton3 {
			    background-position: -81px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: 10px;
			    width: 71px;
			}
			
			.xubox_botton a {
			    bottom: 10px;
			    color: #fff;
			    font-size: 14px;
			    font-weight: bold;
			    left: 50%;
			    overflow: hidden;
			    position: absolute;
			    text-align: center;
			    text-decoration: none;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+'/app/main/ontology-category!list.htm',
					autoParam: ["id"],
					dataFilter: ajaxDataFilter
				},				
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkboxType: { "Y": "", "N": "" },
					radioType: "all"
				}
			};
			
			//用于对 Ajax 返回数据进行预处理的函数
			function ajaxDataFilter(treeId, parentNode, responseData) {
			    if (responseData) {
			    	var cIds = $("#"+$("#cIds").val(), parent.document).val();
			    	var cFIds = '';
			    	if($("#cFIds").val()){
			    		cFIds = $("#"+$("#cFIds").val(), parent.document).val();
			    	}
			        for(var i =0; i < responseData.length; i++) {
			        	//回显(半勾选,认为半勾选的节点都有子节点，所以半勾选数据都是.结尾)
			        	if(cFIds.length>0 && cFIds.indexOf(responseData[i].id+".")>-1)
			        		responseData[i].halfCheck = true;
			        	//回显(勾选)
			        	if(cIds.length>0 && (cIds+",").indexOf(responseData[i].id+",")>-1)
			        		responseData[i].checked = true;
			        }
			    }
			    return responseData;
			};
			
			
			//勾选数据处理
			function zTreeOnCheck() {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				var names = "",fNames = "";
				var ids = "",fIds = "";
				for (var i=0, l=nodes.length; i<l; i++) {
					names += nodes[i].name + ",";
					ids +=  nodes[i].id + ",";
					
					//完整名称及id
					var fNames_ = nodes[i].name;
					var fIds_ = nodes[i].id;
					while((nodes[i] = nodes[i].getParentNode()) != null){
						fNames_ = nodes[i].name + "." + fNames_;
						fIds_ = nodes[i].id + "." + fIds_;
					}
					fNames += fNames_ + ",";
					fIds += fIds_ + ",";
				}
				var reg = new RegExp("^,|,$","gmi");
				names = names.replace(reg,"");
				ids = ids.replace(reg,"");
				fNames = fNames.replace(reg,"");
				fIds = fIds.replace(reg,"");
				
				$("#names").val(names);
				$("#ids").val(ids);
				$("#fNames").val(fNames);
				$("#fIds").val(fIds);
			}
			
			//关闭当前弹窗
			function layer_close(){
				var c_layer_index = parent.choose_layer.c_layer_index;
				parent.choose_layer.close(c_layer_index);
			}
			
			//确认数据并回显
			function confirm_choose(){
				//勾选数据处理
				zTreeOnCheck();
				var ids = $("#ids").val(),fIds = $("#fIds").val();
				var names = $("#names").val(),fNames = $("#fNames").val();
				
				var cIds = $("#cIds").val(),cFIds = $("#cFIds").val();
				var cNames = $("#cNames").val(),cFNames = $("#cFNames").val();
				if(ids.length<1){
					parent.layer.confirm("未选择知识分类,是否关闭?", function(index){
						$("#"+cIds, parent.document).val('');
						$("#"+cNames, parent.document).val('');
						$("#"+cFIds, parent.document).val('');
						$("#"+cFNames, parent.document).val('');
						parent.choose_layer.close(index);
						layer_close();
					});
				}else{
					$("#"+cIds, parent.document).val(ids);
					$("#"+cNames, parent.document).val(names);
					$("#"+cFIds, parent.document).val(fIds);
					$("#"+cFNames, parent.document).val(fNames);
					layer_close();
				}
			}
			
			$(document).ready(function(){
				var chkStyle = $("#chkStyle").val();
				if(chkStyle=="radio")setting.check.chkStyle = chkStyle;
				$.fn.zTree.init($("#treeDemo"), setting);
			});
		</script>
	</head>
	<body>
		<input type="hidden" id="chkStyle" value="${chkStyle }"/>
		<input type="hidden" id="ids"/>
		<input type="hidden" id="names"/>
		<input type="hidden" id="fIds"/>
		<input type="hidden" id="fNames"/>
		<!--父页面回显数据所存标签id  -->
		<input type="hidden" id="cIds" value="${cIds }"/>
		<input type="hidden" id="cNames" value="${cNames }"/>
		<input type="hidden" id="cFIds" value="${cFIds }"/>
		<input type="hidden" id="cFNames" value="${cFNames }"/>
		<div id="menuContent" class="menuContent" style="position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:220px;height:${height}px;overflow-y:auto;overflow-x:auto;"></ul>
		</div>
		<span class="xubox_botton">
			<a class="xubox_yes xubox_botton2" href="###" onclick="javascript:confirm_choose();">确定</a>
			<a class="xubox_no xubox_botton3" href="###"  onclick="javascript:layer_close();">取消</a>
		</span>
	</body>
</html>
