<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>服务类型选择</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="styleSheet" type="text/css" href="${pageContext.request.contextPath}/library/layer/skin/layer.css"  id="layermcss" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.xubox_botton2 {
			    background-position: -5px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: -76px;
			    width: 71px;
			}
			.xubox_botton3 {
			    background-position: -81px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: 10px;
			    width: 71px;
			}
			
			.xubox_botton a {
			    bottom: 10px;
			    color: #fff;
			    font-size: 14px;
			    font-weight: bold;
			    left: 50%;
			    overflow: hidden;
			    position: absolute;
			    text-align: center;
			    text-decoration: none;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+"/app/custom/gzyd/gzyd-mappings!treeSrtype.htm",
					autoParam: ["id"],
					dataFilter: ajaxDataFilter
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkboxType: { "Y": "", "N": "" },
					radioType: "all"
				}
			};
			
			//用于对 Ajax 返回数据进行预处理的函数
			var array_filter = ",办理,咨询,查询,取消,投诉,";
			function ajaxDataFilter(treeId, parentNode, responseData) {
			    if (responseData.length > 0) {
			    	if(parentNode == null || (parentNode!= null && parentNode.id != 'srRst')){
			    		var sIds = $("#"+$("#sIds").val(), parent.document).val();
				    	var sFIds = '';
				    	if($("#sFIds").val()){
				    		sFIds = $("#"+$("#sFIds").val(), parent.document).val();
				    	}
				        for(var i =0; i < responseData.length;) {
				        	//回显(半勾选,认为半勾选的节点都有子节点，所以半勾选数据都是.结尾)
				        	if(sFIds.length>0 && sFIds.indexOf(responseData[i].id+".")>-1)
				        		responseData[i].halfCheck = true;
				        	//回显(勾选)
				        	if(sIds.length>0 && (sIds+",").indexOf(responseData[i].id+",")>-1)
				        		responseData[i].checked = true;
				        	//判断是否是业务类型
				        	if(array_filter.indexOf(","+responseData[i].name+",")>-1){
				        		responseData.splice(i,1);
				    		}else{
				    			i++;
				    		}
				        }
			    	}
			    } else {
			    	//var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			       	//parentNode.isParent = false;
			        //zTree.updateNode(parentNode);
			    }
			    return responseData;
			};
			
			
			//勾选数据处理
			function zTreeOnCheck() {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				var names = "",fNames = "";
				var ids = "",fIds = "";
				for (var i=0, l=nodes.length; i<l; i++) {
					names += nodes[i].name + ",";
					ids +=  nodes[i].id + ",";
					
					//完整名称及id
					var fNames_ = nodes[i].name;
					var fIds_ = nodes[i].id;
					while((nodes[i] = nodes[i].getParentNode()) != null){
						fNames_ = nodes[i].name + "." + fNames_;
						fIds_ = nodes[i].id + "." + fIds_;
					}
					fNames += fNames_ + ",";
					fIds += fIds_ + ",";
				}
				var reg = new RegExp("^,|,$","gmi");
				names = names.replace(reg,"");
				ids = ids.replace(reg,"");
				fNames = fNames.replace(reg,"");
				fIds = fIds.replace(reg,"");
				
				$("#names").val(names);
				$("#ids").val(ids);
				$("#fNames").val(fNames);
				$("#fIds").val(fIds);
			}
			
			//关闭当前弹窗
			function layer_close(){
				var gs_layer_index = parent.choose_layer.gs_layer_index;
				parent.choose_layer.close(gs_layer_index);
			}
			
			//确认数据并回显
			function confirm_choose(){
				//勾选数据处理
				zTreeOnCheck();
				
				//结果回显
				var ids = $("#ids").val(),fIds = $("#fIds").val();
				var names = $("#names").val(),fNames = $("#fNames").val();
				var sIds = $("#sIds").val(),sFIds = $("#sFIds").val();
				var sNames = $("#sNames").val(),sFNames = $("#sFNames").val();
				//针对检索的数据做特殊处理
				if(/^(srRst)\S+/.test(fIds)) {
					var loadindex = parent.layer.load('数据检索中...');
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!getSrtypePath.htm', 
						data : {'srtypeid': ids},
						async: false,
						dataType : 'json',
						success : function(jsonResult){
							if(jsonResult.rst){
								fIds = jsonResult.fIds;
								fNames = jsonResult.fNames;
								names = jsonResult.names;
							} else {
								alert(jsonResult.msg);
							}
						},
						error : function(msg){
							alert('网络错误，请稍后再试!');
						}
					});
					parent.layer.close(loadindex);/*关闭加载条*/
				}
				
				if(ids.length<1){
					parent.layer.confirm("未选择服务类型,是否关闭?", function(index){
						$("#"+sIds, parent.document).val('');
						$("#"+sNames, parent.document).val('');
						$("#"+sFIds, parent.document).val('');
						$("#"+sFNames, parent.document).val('');
						parent.choose_layer.close(index);
						layer_close();
					});
				}else{
					$("#"+sIds, parent.document).val(ids);
					$("#"+sNames, parent.document).val(names);
					$("#"+sFIds, parent.document).val(fIds);
					$("#"+sFNames, parent.document).val(fNames);
					layer_close();
				}
			}
			
			$(document).ready(function(){
				var chkStyle = $("#chkStyle").val();
				if(chkStyle=="radio")setting.check.chkStyle = chkStyle;
				$.fn.zTree.init($("#treeDemo"), setting);
			});
		</script>
	</head>
	<body>
		<input type="hidden" id="chkStyle" value="${chkStyle }"/>
		<input type="hidden" id="ids"/>
		<input type="hidden" id="names"/>
		<input type="hidden" id="fIds"/>
		<input type="hidden" id="fNames"/>
		<!--父页面回显数据所存标签id  -->
		<input type="hidden" id="sIds" value="${sIds }"/>
		<input type="hidden" id="sNames" value="${sNames }"/>
		<input type="hidden" id="sFIds" value="${sFIds }"/>
		<input type="hidden" id="sFNames" value="${sFNames }"/>
		<div id="menuContent" class="menuContent" 
			style="background-color: white; top: 5px; left: 5px;">
			<input id="ss" class="easyui-searchbox" style="width:300px; max-width: ${param.width-20}px;" 
				data-options="prompt:'请输入检索内容!',searcher:function(value, name){
					if(value && !/\s+/.test(value)){
						$('body').ajaxLoading('正在查询数据...');
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!searchSrtype.htm', 
							data : {'searchContent': value},
							async: false,
							dataType : 'json',
							success : function(jsonResult){
								if(jsonResult.rst){
									var treeObj = $.fn.zTree.getZTreeObj('treeDemo');
									var srRst = treeObj.getNodeByParam('id', 'srRst', null);
									treeObj.removeChildNodes(srRst);
									treeObj.addNodes(srRst, jsonResult.data);
								} else {
									alert(jsonResult.msg);
								}
							},
							error : function(msg){
								alert('网络错误，请稍后再试!');
							}
						});
						/*延时关闭*/
						var init = setTimeout(function (){
							clearTimeout(init);/*取消定时器*/
							$('body').ajaxLoadEnd();
						}, 500);
					}
				}"></input> 
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:${param.width-20}px;height:${height-30}px;overflow-y:auto;overflow-x:auto;border: 1px solid black;"></ul>
		</div>
		<span class="xubox_botton">
			<a class="xubox_yes xubox_botton2" href="###" onclick="javascript:confirm_choose();">确定</a>
			<a class="xubox_no xubox_botton3" href="###"  onclick="javascript:layer_close();">取消</a>
		</span>
	</body>
</html>
