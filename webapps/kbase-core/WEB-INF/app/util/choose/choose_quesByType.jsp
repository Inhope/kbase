<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>按类型选题目</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:99%;height:99%;}
		</style>
		<style type="text/css">
			body{margin: 3px;}
			.ztree li span.button {
				background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.png"); *background-image:url("${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/img/zTreeStandard_dept.gif")
			}
			div { width: 240px;}
			.ztree {border: 1px solid #F3F3F3; height: 358px; overflow: auto;}
			table { border: 1px solid #F3F3F3; }
			td {border:1px dotted #F3F3F3;}
			input[type="text"]{border: 1px solid #F3F3F3;}
			
			.searchNode{
				background-color: #ffe6b0;
			    border: 1px solid #ffb951;
			    color: black;
			    height: 16px;
			    opacity: 0.8;
			    padding-top: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript">
			$(function(){
				var _key = '${param.key}',/*题目类型*/
					_chkStyle = '${param.chkStyle}';/*树单选radio、多选checkbox*/
				if(!_chkStyle) _chkStyle = 'checkbox';
				var TreeUtils = {
					tree:{
						treeId: 'treeDemo',
						treeObj: null,
						setting:  {
							async: {
								enable: true,
								url: $.fn.getRootPath() + '/app/learning/paper!syncQuesData.htm',
								autoParam:['id'],
								otherParam:{'key': _key}
							},
							view: {
								dblClickExpand: false,
								selectedMulti: true
							},
							data: {
								simpleData: {
									enable: true
								}
							},
							check: {
								enable: true,
								chkStyle: _chkStyle,
								chkboxType: { "Y": "s", "N": "s" }
							},
							callback: {
								onCheck: function(event, treeId, treeNode){
									var _that = TreeUtils.fn;
									/*选中的节点*/
									var nodes = new Array();
									nodes.push(treeNode);
									/*右侧区域回显*/
									if (treeNode.checked) _that.rightHandle(nodes, 'Add');
									else _that.rightHandle(nodes, 'Del');
								},
								onAsyncSuccess: function(event, treeId, treeNode, msg){
									var _that = TreeUtils, /*回显数据*/
										initData = _that.fn.getObjByName('${param.initData}');
									/*数据回显*/
									if(initData.length > 1)/*回掉*/
										initData = initData[1].call(initData[0], _key);
									else initData = initData(_key);
									if(initData){
										if($(initData).hasOwnProperty('key')){
											if($.isArray(initData)){/*返回指定类型的数据*/
												var nodes = initData;
												if(nodes && nodes.length > 0){
													_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
													_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
												}
											} else {/*返回所有的数据*/
												for(key in initData){
													var nodes = initData[key];
													if(nodes && nodes.length > 0){
														_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
														_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
													}
												}
											}
										} else {
											var nodes = initData;
											if(!$.isArray(initData)) {
												nodes = new Array();
												nodes.push(initData);
											}
											if(nodes && nodes.length > 0){
												_that.fn.rightHandle(nodes, 'Add');/*回显操作（右侧）*/
												_that.fn.leftHandle(nodes, 'Add');/*回显操作（左侧）*/
											}
										}
									}
								} 
							}
						},
						/*获取树对象*/
						getZTreeObj: function(){
							return $.fn.zTree.getZTreeObj(this.treeId);
						},
						/*树初始化*/
						init: function(){
							/*初始化页面高度*/
							$("#" + this.treeId).css("height", $(window).height()-85);
							$('#sel').css("height", $(window).height()-55);
							/*初始化树*/
							return this.treeObj = $.fn.zTree.init($("#" + this.treeId), this.setting);
						}
					},
					fn: {
						/*右侧区域数据处理*/
						leftHandle: function (objs, flag){
							if(!objs) return false;
							var ztreeObj = TreeUtils.tree.getZTreeObj();
							if(flag == 'Add'){//添加
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) ztreeObj.checkNode(node, true, null);
								});
							} else if(flag == 'Del'){//删除
								$(objs).each(function(i, node){
									node = ztreeObj.getNodeByParam('id', node.id, null);
									if(node) ztreeObj.checkNode(node, false);
								});
							}
						},
						/*左侧区域数据删除*/
						rightHandle: function (objs, flag){
							if(!objs) return false;
							if(flag == 'Add'){/*添加*/
								if(_chkStyle == 'radio') /*单选应当移除所有已有项*/
									$('#sel option').remove();
								$(objs).each(function(i, node){
									if ($("#sel option[value='"+node.id+"']").length == 0){
										$('#sel').append('<option value="' + node.id + '">' + node.name + '</option>');
										$("#sel option[value='" + node.id + "']").data('content', node.content);
									}
								});
							} else if(flag == 'Del'){/*删除*/
								$(objs).each(function(i, node){
									if ($("#sel option[value='"+node.id+"']").length>0){
										$("#sel option[value='"+node.id+"']").remove();
									}
								});
							}
						},
						searchLabelObj: function (){
							var keywords = $('#keywords').val(), i = 0;/*搜索结果*/
							if(keywords){
								var ztreeObj = TreeUtils.tree.getZTreeObj();
								ztreeObj.getNodesByFilter(function(node){
									var pattern = new RegExp('.*' + keywords + '.*', 'g');
									if(node.type == 'ques' && pattern.test(node.name)){
										ztreeObj.selectNode(node, true);
										i++;
									} else {
										ztreeObj.cancelSelectedNode(node);
									}
									return null;
								}); 
							}
							$('#keywords')[0].focus();
							if(i == 0) parent.layer.alert('未查询到相关题目!', -1);
						},
						/*名称获取父页面对象*/
						getObjByName: function(name){
							var obj = parent;
							if(name.indexOf('.') > -1){
								var _that;
								$(name.split('.')).each(function(i, item){
									_that = obj;/*this对象应改为上一级*/
									obj = $(obj).attr(item);
								});
								return [_that, obj];
							} else {
								return $(obj).attr(name);
							}
						}
					},
					/*初始化*/
					init: function(){
						/*树初始化*/
						var _that = this, ztreeObj = _that.tree.init(), 
						 	callback = _that.fn.getObjByName('${param.callback}');/*回掉函数*/
						
						/*确定*/
						$("#btnOk").click(function(){
							var data = new Array();
							if ($("#sel option").length>0){
								$("#sel option").each(function(i, item){
									data.push({
										id: $(item).val(), 
										name: $(item).text(),
										content: $(item).data('content')
									});
								});
								if(callback.length > 1) /*回掉*/
									callback[1].call(callback[0], _key, data);
								else callback(_key, data);
							}
							$("#btnClose").click();
						});
						
						/*取消*/
						$("#btnClose").click(function(){
							parent.layer.close(parent.__kbs_picker_index);
						});
						
						/*移除选中的值*/
						$("#btnMoveout").click(function(){
							var id = $('#sel option:selected').val();
							if (id){
								var nodes = new Array();//回显节点
								nodes.push({'id':id});
								_that.fn.rightHandle(nodes, 'Del');//回显操作（右侧）
								_that.fn.leftHandle(nodes, 'Del');//回显操作（左侧）
							}
						});
						
						/*双击取消选中的select*/
						$("#sel").dblclick(function(){
							$("#btnMoveout").click();
						});
						
						/*移入选中的值*/
						$('#btnMovein').click(function(){
							var nodes = ztreeObj.getSelectedNodes();
							_that.fn.rightHandle(nodes, 'Add');//回显操作（右侧）
							_that.fn.leftHandle(nodes, 'Add');//回显操作（左侧）
						});
						
						/*点击搜索按钮，搜索用户*/
						$('#searchLabelObj').click(function(){
							_that.fn.searchLabelObj();
						});
						
						/*回车，搜索用户*/
						$('#keywords').bind('keydown', function(event) {
							if (event.keyCode=="13") {
								_that.fn.searchLabelObj();
							}
						});
					}
				}
				TreeUtils.init();
			})
		</script>
	</head>

	<body>
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td width="47%">
					<input type="text" id="keywords" placeholder="请输入题目名称">
					<img id="searchLabelObj" title="搜题目名称"
						src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/find.png"
						style="cursor: pointer;" />
				</td>

				<td width="6%" rowspan="2">
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-next.png"
						alt="移入" style="cursor: pointer;" id="btnMovein">
					<br>
					<br>
					<br>
					<img src="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/img/diy/go-previous.png"
						alt="移出" style="cursor: pointer;" id="btnMoveout">
				</td>
				<td width="47%" rowspan="2">
					<select id="sel" size="28" style="width: 100%; height: 400px; border: 1px solid #F3F3F3;"></select>
				</td>
			</tr>
			<tr>
				<td>
					<div class="ztree" id="treeDemo"></div>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<input type="button" value="确定" id="btnOk"  >
					<input type="button" value="取消" id="btnClose" >
				</td>
			</tr>
		</table>
	</body>
</html>
