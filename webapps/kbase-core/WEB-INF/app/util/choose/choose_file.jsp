<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>文件选择</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/choose_file.js"></script>
	</head>
	<body class="easyui-layout">  
		<input id="data" type="hidden" />
		<div id="now-toolbar" style="padding:5px;height:auto">
			图片原名称：<input id="imgName" class="easyui-textbox" style="width:100px;border: 1px solid #DDDDDD;">
			创建人：<input id="createUser" class="easyui-textbox" style="width:100px;border: 1px solid #DDDDDD;">
			创建日期:
				<input id="startDate" type="text" class="easyui-datebox"  style="width:100px;border: 1px solid #DDDDDD;" editable="false">
				~
				<input id="endDate" type="text" class="easyui-datebox"  style="width:100px;border: 1px solid #DDDDDD;" editable="false">
			&nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" id="btnSearch" iconCls="icon-search" onClick="doSearch()">查询</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" id="btnReset" iconCls="icon-clear" onClick="btnReset()">重置</a>
			<a id="btnDel" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onClick="btnDel()">删除</a>
		</div>
		
		<table id="dataList" class="easyui-datagrid" style="width:100%;height:100%" data-options="url:'${pageContext.request.contextPath }/app/biztpl/article!imageDataJson.htm',
    		fitColumns:true,singleSelect:false,rownumbers:true,fit:true,toolbar:'#now-toolbar',pagination:true">
			<thead>
				<tr>
					<th data-options="field:'id',width:'5%',align:'center',checkbox:true"></th>
					<th data-options="field:'fileName',width:'15%',align:'center'">图片新名称</th>
					<th data-options="field:'name',width:'15%',align:'center'">图片原名称</th>
					<th data-options="field:'createUser',width:'15%',align:'center'">创建人</th>
					<th data-options="field:'createDate',width:'20%',
							formatter:function(value,row,index){return fmtDate(value);}">创建时间</th>
					<th data-options="field:'filePath',width:'30%',align:'center',
						formatter:function(value,row,index){return fmtImg(value, row, index);}">预览</th>
					
				</tr>
			</thead>
		</table>
	
		
		
		<!--<table id="tt" class="easyui-treegrid" style="width:100%;height:100%"   
		        data-options="url:'${pageContext.request.contextPath}/app/util/choose!fileData.htm',
		        rownumbers:true,fit:true,fitColumns:true,idField:'filename',treeField:'filename',
		        onCheck:function(rowIndex, rowData){
		        	if(rowData == null) /*jquery easyui 的bug 貌似有时候会丢掉参数rowIndex*/
		        		rowData = rowIndex;
		        	if(rowData == null) /*当无法获取的时候，采用getSelections方式*/
		        		rowData = $('#tt').treegrid('getSelections')[0];
		        	if(rowData){
		        		$('#data').attr('isFile', rowData.isFile);
		        		$('#data').attr('filename', rowData.filename);
		        		$('#data').attr('filepath', rowData.filepath);
		        	}
		        }">
		    <thead>
		        <tr>
		        	<th data-options="field:'ck',checkbox:true">文件</th>
		            <th data-options="field:'filename',width:300">文件</th>
		            <th data-options="field:'filepath',width:100,formatter:formatter1">预览</th>
		        </tr>   
		    </thead>   
		</table>  
	--></body>
	
	<%-- 图片放大 --%>
	<div id="choose_image_toBig" style="display: none;position: absolute;background-color: #DFDFDF;text-align: center;top:0;">
		<span style="height: 100%;display: inline-block;vertical-align: middle;"></span>
		<img src="" ondblclick="$(this).parent().hide();" style="vertical-align: middle;"/>
	</div>
</html>