<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷分类选择</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="styleSheet" type="text/css" href="${pageContext.request.contextPath}/library/layer/skin/layer.css"  id="layermcss" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.xubox_botton2 {
			    background-position: -5px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: -76px;
			    width: 71px;
			}
			.xubox_botton3 {
			    background-position: -81px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: 10px;
			    width: 71px;
			}
			
			.xubox_botton a {
			    bottom: 10px;
			    color: #fff;
			    font-size: 14px;
			    font-weight: bold;
			    left: 50%;
			    overflow: hidden;
			    position: absolute;
			    text-align: center;
			    text-decoration: none;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath() + '/app/learning/paper-cate!initTree.htm',
					autoParam: ["id"]
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkboxType: { "Y": "", "N": "" },
					radioType: "all"
				},
				callback: {
					onAsyncSuccess: function (event, treeId, treeNode, msg) {
						/*树勾选回显*/
						var ids = parent.$("#" + idField).val();
						if(ids){
							var nodes = treeObj.getNodesByFilter(function(node){
								if((',' + ids + ',').indexOf(',' + node.id + ',') > -1){
									return true;
								}
								return false;
							});
							$(nodes).each(function(i, node){
								treeObj.expandNode(node, true);
								treeObj.checkNode(node, true, true);
							});
							
						}
					}
				}
			};
			
			
			/*存放返回值的标签id*/
			var returnField = '${param.returnField}'.split('|'), 
				descField = returnField[0], /*存放返回值name的标签id*/
				idField = returnField[1];/*存放返回值id的标签id*/
			var treeObj;/*树*/
			$(function(){
				/*树相关参数设置*/
				var chkStyle = '${param.chkStyle}';
				if(chkStyle=="radio") setting.check.chkStyle = chkStyle;
				treeObj = $.fn.zTree.init($("#treeDemo"), setting);
				
				/*确定*/
				$("#btnOk").click(function(){
					var descArr = [];
					var idArr = [];
					$(treeObj.getCheckedNodes()).each(function(i, node){
						descArr.push(node.name);
						idArr.push(node.id);
					});
					if (descField != null){
						parent.$("#"+descField).val(descArr.join(","));
						parent.$("#"+idField).val(idArr.join(","));
					}
					/*关闭弹窗*/
					$("#btnClose").click();
				});
				/*取消*/
				$("#btnClose").click(function(){/*关闭弹窗*/
					parent.layer.close(parent.__kbs_picker_index);
				});
				
				/*树的高度，宽度调整*/
				var h = $("body").height(), w = $("body").width();
				$('#treeDemo').css({height: h-70, width: w-20});
			});
		</script>
	</head>
	<body>
		<div id="menuContent" class="menuContent" style="text-align: center;">
			<br>
			<ul id="treeDemo" class="ztree" style="overflow: auto; background-color: white;border: 1px solid black; margin-left: 3px;margin-top: -10px;"></ul>
		</div>
		<span class="xubox_botton">
			<a class="xubox_yes xubox_botton2" href="###" id="btnOk">确定</a>
			<a class="xubox_no xubox_botton3" href="###"  id="btnClose">取消</a>
		</span>
	</body>
</html>
