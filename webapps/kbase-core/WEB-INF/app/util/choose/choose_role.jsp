<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>角色选择</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="styleSheet" type="text/css" href="${pageContext.request.contextPath}/library/layer/skin/layer.css"  id="layermcss" />
		<style type="text/css">
			.xubox_botton2 {
			    background-position: -5px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: -76px;
			    width: 71px;
			}
			.xubox_botton3 {
			    background-position: -81px -114px;
			    height: 29px;
			    line-height: 29px;
			    margin-left: 10px;
			    width: 71px;
			}
			
			.xubox_botton a {
			    bottom: 10px;
			    color: #fff;
			    font-size: 14px;
			    font-weight: bold;
			    left: 50%;
			    overflow: hidden;
			    position: absolute;
			    text-align: center;
			    text-decoration: none;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: $.fn.getRootPath()+'/app/util/choose!roleData.htm',
					autoParam: ["id"],
					dataFilter: ajaxDataFilter
				},				
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					radioType: "all"
				}
			};
			
			//用于对 Ajax 返回数据进行预处理的函数
			function ajaxDataFilter(treeId, parentNode, responseData) {
			    if (responseData) {
			    	var rIds = $("#"+$("#rIds").val(), parent.document).val();
			        for(var i =0; i < responseData.length; i++) {
			        	//回显
			        	if(rIds.length>0 && rIds.indexOf(responseData[i].id)>-1)
			        		responseData[i].checked = true;
			     		if(responseData[i].isParent == "true"){
			     		   	responseData[i].icon = $.fn.getRootPath()
			     			   	+"/library/ztree/css/zTreeStyle/img/diy/dept.gif";
			     		   	responseData[i].nocheck = true;
			     	    }else{
			     		   	responseData[i].icon = $.fn.getRootPath()
			     			   	+"/library/ztree/css/zTreeStyle/img/diy/station.gif";
			     	    }
			        }
			    }
			    return responseData;
			};
			
			
			//勾选数据处理
			function zTreeOnCheck() {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				var names = "";
				var ids = "";
				for (var i=0, l=nodes.length; i<l; i++) {
					names += nodes[i].name + ",";
					ids +=  nodes[i].id + ",";
				}
				var reg = new RegExp("^,|,$","gmi");
				names = names.replace(reg,"");
				ids = ids.replace(reg,"");
				$("#names").val(names);
				$("#ids").val(ids);
			}
			
			//关闭当前弹窗
			function layer_close(){
				var r_layer_index = parent.choose_layer.r_layer_index;
				parent.choose_layer.close(r_layer_index);
			}
			
			//确认数据并回显
			function confirm_choose(){
				//勾选数据处理
				zTreeOnCheck();
				var ids = $("#ids").val();
				var names = $("#names").val();
				
				var rIds = $("#rIds").val();
				var rNames = $("#rNames").val();
				if(ids.length<1){
					parent.layer.confirm("未选择角色,是否关闭?", function(index){
						$("#"+rIds, parent.document).val('');
						$("#"+rNames, parent.document).val('');
						parent.choose_layer.close(index);
						layer_close();
					});
				}else{
					$("#"+rIds, parent.document).val(ids);
					$("#"+rNames, parent.document).val(names);
					layer_close();
				}
			}
			
			$(document).ready(function(){
				var chkStyle = $("#chkStyle").val();
				if(chkStyle=="radio")setting.check.chkStyle = chkStyle;
				$.fn.zTree.init($("#treeDemo"), setting);
			});
		</script>
	</head>
	<body>
		<input type="hidden" id="chkStyle" value="${chkStyle }"/>
		<input type="hidden" id="ids"/>
		<input type="hidden" id="names"/>
		<!--父页面回显数据所存标签id  -->
		<input type="hidden" id="rIds" value="${rIds }"/>
		<input type="hidden" id="rNames" value="${rNames }"/>
		<div id="menuContent" class="menuContent" style="position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
			<ul id="treeDemo" class="ztree" style="margin-top:0; width:220px;height:${height}px;overflow-y:auto;overflow-x:auto;"></ul>
		</div>
		<span class="xubox_botton">
			<a class="xubox_yes xubox_botton2" href="###" onclick="javascript:confirm_choose();">确定</a>
			<a class="xubox_no xubox_botton3" href="###"  onclick="javascript:layer_close();">取消</a>
		</span>
	</body>
</html>
