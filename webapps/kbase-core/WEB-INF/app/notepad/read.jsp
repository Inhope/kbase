<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>查看</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-title{
				font-size:18px;
				font-weight:bolder;
			}
			.kbs-subtitle{
				font-size:12px;
			}
		</style>		
		<script type="text/javascript">
		    $(function(){		
		          //编辑
		          $("#btnEdit").click(function(){
		                var _url = '${pageContext.request.contextPath}/app/notice/notepad!edit.htm?id='+$("#id").val();
						window.location.href = _url;					
		          }); 
				 /**
				  * 关闭
				  */	 
				 $('#btnClose').click(function(){
			           var index = parent.layer.getFrameIndex(window.name); 
			           parent.layer.close(index);
				 }); 		          
		    });				
		</script>
		
	</head>

	<body>
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td align="center">
					<span class="kbs-title">${note.title}</span><br>
					<span class="kbs-subtitle">
						<s:date format="yyyy-MM-dd HH:mm" name="#request.note.createTime"/>
					</span></td>
			</tr>
			<tr>
				<td>${note.content}</td>
			</tr>
			<tr>
				<td align="right">
					<input type="button" value="编辑" id="btnEdit">
					<input type="button" value="关闭" id="btnClose">
				</td>
			</tr>		
		</table>
		<div style="display:none">
		    <input type="text" id="id" value="${note.id}">
		</div>
	</body>
</html>
