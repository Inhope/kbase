<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
	  		td a{
	  			cursor: pointer;
	  		}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript">
		     var exists_unsaved = 0;//是否存在未保存对象 
		     $(function(){
		          //var _pObj = (parent!=undefined&&parent!=null)?parent:document;
		          //_pObj = (parent.parent!=undefined&&parent.parent!=null)?parent.parent:parent;
		          //新增
		          $("#btnNew").click(function(){
						$.layer({
						    id: 'edit_notepad',
							type: 2,
						    title: '我的空间',
						    closeBtn: false,
						    shade: [0.3, '#000'],
						    border: [5, 0.3, '#000'],
						    area: ['820px', '500px'],
						    iframe: {src: $.fn.getRootPath() + '/app/notice/notepad!edit.htm'}
						});
						/* var _url = '${pageContext.request.contextPath}/app/notice/notepad!edit.htm';
						openWindow(820,550,_url);*/							
		          });
		          //编辑
		          $("#btnEdit").click(function(){
		                var ids = getCheckIds();
						if(ids.length<1){
							$(document).hint('请选择需要编辑的条目!');
							return false;
						}
						$.layer({
						    id: 'edit_notepad',
							type: 2,
						    title: '我的空间',
						    closeBtn: false,
						    shade: [0.3, '#000'],
						    border: [5, 0.3, '#000'],
						    area: ['820px', '500px'],
						    iframe: {src: $.fn.getRootPath() + '/app/notice/notepad!edit.htm?id='+ids[0]}
						});
						/* var _url = '${pageContext.request.contextPath}/app/notice/notepad!edit.htm?id='+ids[0];
						openWindow(820,550,_url);	*/					
		          });
		          //删除
		          $("#btnDel").click(function(){
		                var ids = getCheckIds();
						if(ids.length<1){
							$(document).hint("请选择需要删除的条目!");
							return;
						}	
				  		var idArray = '';
				  		for(var i = 0; i < ids.length; i++){
				  			if(i==0){
				  				idArray = ids[i];
				  			}else{
				  				idArray +="," + ids[i];
				  			}
				  		}						
			        	$.ajax({
					   		type: 'POST',
					   		url: $.fn.getRootPath()+'/app/notice/notepad!delete.htm',
					   		data: {
					   			id : idArray
					   		},
					   		dataType: 'json',
					   		async: false,
					   		success: function(jsonObj){
					   		    $(document).hint(jsonObj.msg);
					   		    if(jsonObj.err==0){
					   		       location.reload();
					   		    }
					   		},
					   		error: function(){
					   		    $(document).hint("删除操作执行失败!");
					   		}
						});			                
		          });	
		          var _layerHeight = $(document).height() * 0.9;
		          var _layerWidth = $(document).width() * 0.8;
		          //查看
		          $(".notepadTitle").click(function(){
		               var _notepadId = $(this).attr("id");
		               $.layer({
							type: 2,
						    title: '我的空间',
						    shade: [0.3, '#000'],
						    border: [5, 0.3, '#000'],
						    closeBtn: false,
						    //area: [_layerWidth, _layerHeight],
						    area: ['820px', '500px'],
						    iframe: {src: $.fn.getRootPath() + '/app/notice/notepad!read.htm?id='+_notepadId}
						});
						/* var _url = '${pageContext.request.contextPath}/app/notice/notepad!read.htm?id='+_notepadId;
						openWindow(820,550,_url);*/
		          });	
		          //全选/反选
		          $("#ckAll").change(function(){
		               if($(this).attr("checked")=='checked'){
		                   $("[name='ckNotepad']").attr("checked",'checked')
		               }else{
		                   $("[name='ckNotepad']").removeAttr("checked")
		               }
		          });
		          //分页          		          
		          fenye();
		          
		          //初始化
		           $("[name='ckNotepad']").removeAttr("checked")
		     });
		     function getCheckIds(){
		     	 var checkbox = $('input[name="ckNotepad"]');
				 var checkIds = new Array();
				 checkbox.each(function(index){
					 if($(this).attr('checked')){
						 checkIds.push($(this).attr("id"));
					 }
				 });
				 return checkIds;
		     }
		     /**
		     * 打开新窗口
		     */
		     function openWindow(_diaWidth,_diaHeight,_url){
					var _scrWidth = screen.width;
					var _scrHegiht = screen.height;
					var _diaLeft = (_scrWidth-_diaWidth)/2;
					var _diaTop = (_scrHegiht-_diaHeight)/2;
					var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=1, toolbar=0';
					window.open(_url, '_blank', params);		     
		     }
		     /**
		     * 分页
		     */
			 function fenye(){
				var btns = $('div.gonggao_con_nr_fenye a');
				var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
				var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
				var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
				var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
				btns.each(function(index){
					var btn= $(this);
					if(btn.html().indexOf('上一页') > -1 ){
						if( currentPage > 1){
							btn.click(function(){
								var prev = parseInt(start)-parseInt(limit);
								fenyeSubmit(prev);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('下一页') > -1 ){
						if(currentPage < totalPage){
							btn.click(function(){
								var next = parseInt(start)+parseInt(limit);
								fenyeSubmit(next);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('首页') > -1 ){
						if(currentPage > 1){
							btn.click(function(){
								fenyeSubmit(0);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('尾页') > -1 ){
						if(currentPage < totalPage){
							btn.click(function(){
								var last = parseInt(limit)*(parseInt(totalPage)-1);
								fenyeSubmit(last);	
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else {
						var text = $(this).html();
						if(text != currentPage){
							var click = parseInt(limit)*(parseInt(text)-1);
							btn.click(function(){
								fenyeSubmit(click);
							});
						}
					}
					
				});
				function fenyeSubmit(start){
					$('#notepadForm input[name="start"]').val(start);
					$('#notepadForm').submit();
				}
			}	
		</script>
		
	</head>

	<body>
		<!--******************内容开始***********************-->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
			    <form id="notepadForm" method="post" action="${pageContext.request.contextPath}/app/notice/notepad!view.htm">
					<input type="hidden" name="start"/>			    
			    </form>
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="3%"></td>
						<td width="47%"></td>
						<td width="25%"></td>
						<td width="25%"></td>
					</tr>
					
					<tr>
						<td colspan="4" align="right" valign="middle" style="text-align:right;" >	
							<table cellpadding="0" cellspacing="0" style="border:0px;width:100%;">
								<tr>
									<td valign="middle" style="padding-left:5px;">
										<div align="left">
																					
										</div>
										<div class="gonggao_titile_right">
											<a href="javaScript:void(0);" id="btnDel">删除</a>											
											<a href="javaScript:void(0);" id="btnEdit">编辑</a>											
											<a href="javaScript:void(0);" id="btnNew">新增</a> 																						
										</div>
									</td> 
								</tr>
							</table>
						</td>
					</tr>					
					
					<tr class="tdbg">
						<td><input type="checkbox" id="ckAll"/></td>
						<td>标题</td>
						<td>创建时间</td>
						<td>最新修改时间</td>
					</tr>
					<s:iterator value="notes" var="notepad">
						<tr>
							<td><input type="checkbox" name="ckNotepad" id="${notepad.id}"/></td>
							<td><a title="${notepad.title}" id="${notepad.id}" class="notepadTitle">${notepad.title}</a></td>
							<td><s:date format="yyyy-MM-dd HH:mm" name="#notepad.createTime"/></td>
							<td><s:date format="yyyy-MM-dd HH:mm" name="#notepad.lastEditTime"/></td>
						</tr>					
					</s:iterator>
																		
					<s:if test="totalPage>1">
						<tr>
							<td colspan="4">
								<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
									<a href="javaScript:void(0);" onfocus="this.blur();">首页</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">&lt;上一页</a>
									<s:iterator value="new int[totalPage]" status="i">
										<s:if test="#i.index+1 == currentPage"> 
											<a href="javaScript:void(0);" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
										</s:if>
										<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
											<a href="javaScript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
										</s:elseif>
									</s:iterator>
									
									<a href="javaScript:void(0);" onfocus="this.blur();">下一页&gt;</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">尾页</a>
								</div>
							</td>
						</tr>
					</s:if>	
																						
				</table>
			</div>
		</div>
	</body>
</html>
