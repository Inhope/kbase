<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>编辑</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		
		<script type="text/javascript">
		    $(function(){
		        parent.exists_unsaved = 1;//调整父窗口状态
			    /*
			    * 保存
			    */
			    $('#btnSave').click(function(){
			        var id =  $('#id').val();
			        var title = $('#title').val();
			        if($.trim(title)==''){
			           $(document).hint("标题不能为空");	
			           return false;
			        }
			       var content = editor.html(); 
			        if($.trim(content)==''){
			           $(document).hint("内容不能为空");	
			           return false;
			        }
		        	$.ajax({
				   		type: 'POST',
				   		url: $.fn.getRootPath()+'/app/notice/notepad!save.htm',
				   		data: {
				   			id : id,
				   			title : title,
				   			content : content
				   		},
				   		dataType: 'json',
				   		async: false,
				   		success: function(jsonObj){
				   		    $(document).hint(jsonObj.msg);
				   		    if(jsonObj.err==0){
				   		       parent.location.reload();
				   		    }
				   		},
				   		error: function(){
				   		    alert("保存操作执行失败!");
				   		}
					});						        
			    });	
			    
			    /**
			    * 关闭
			    */	 
			   $('#btnClose').click(function(){
			          if(confirm('确定已保存表单信息？')){
			              parent.exists_unsaved = 0; //调整父窗口状态
			              var index = parent.layer.getFrameIndex(window.name); 
                          parent.layer.close(index);
			          }
			   });   
		    });
			/////////////////////////////////////////////////
			///////////////便签内容富文本插件////////////////////
			var editor;			
			KindEditor.ready(function(K) {
				editor = K.create('textarea[id="content"]', {
		            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist','|','table','image','preview'], 
		            allowImageUpload : true,
		            uploadJson: $.fn.getRootPath()+'/library/utils/upload_json.jsp?img_dir=notepad',
		            formatUploadUrl: false
				});
			});
			/////////////////////////////////////////////////					
		</script>
		
	</head>

	<body>
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">标题<span class="inputflag">*</span></td>
				<td width="75%" style="border-right-style:hidden"><textarea id="title" rows="2" style="width:650px;" >${note.title}</textarea></td>
				<td width="10%" style="border-left-style:hidden" valign="bottom"></td>
			</tr>
			<tr>
				<td>内容<span class="inputflag">*</span></td>
				<td valign="bottom" style="border-right-style:hidden"><textarea id="content" style="width:655px;height:250px;visibility:hidden;">${note.content}</textarea></td>
			    <td valign="bottom" style="border-left-style:hidden"></td>
			</tr>	
			<tr>
				<td align="right" colspan="3">
					<span id="tipNotepad" style="color:red;"></span>&nbsp;
					<input type="button" value="保存" id="btnSave">
					<input type="button" value="关闭" id="btnClose">
				</td>
			</tr>
		</table>
		<div style="display:none">
		    <input type="text" id="id" value="${note.id}">
		</div>		
		
	</body>
</html>
