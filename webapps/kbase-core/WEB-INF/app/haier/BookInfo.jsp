<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>通讯录管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			//弹出框打开
			function openShade(shadeId){
				var w = ($('body').width() - $('#'+shadeId).width())/2;
				$('#'+shadeId).css('left',w+'px');
				$('#'+shadeId).css('top','0px');
				$('body').showShade();
				$('#'+shadeId).show();					
			}
			
			//弹出框关闭
			function closeShade(shadeId){
				$('#'+shadeId).css("display","none");
				$('body').hideShade();			
			}

			
			//全选
			function checkAll(){
				var checked = $('input[type="checkbox"][name="ck_all"]').attr('checked');
				if(checked == 'checked')
					$('input[type="checkbox"][name="bookInfoId_list"]').attr('checked', checked);
				else $('input[type="checkbox"][name="bookInfoId_list"]').removeAttr('checked', checked);
			}


			//数据导出
			function exportBookInfo(status){
				$("#form0").attr("action", $.fn.getRootPath()+"/app/haier/book-info!exportBookInfo.htm?optype="+status);
				$("#form0").submit();
			}

			//数据导入
			function importBookInfo(){
				$.layer({
					type: 1,
				    title: false,//["测试", 'font-size:14px;font-weight:bold;'],
				    area: ['400', '300'],
				    border: [5, 0.3, '#000'], //去掉默认边框
				    shade: [0], //去掉遮罩
				    //closeBtn: [0, false], //去掉默认关闭按钮
				    shift: 'left', //从左动画弹出
				    page: {
				        dom: '#ImportShade'
				    }
				});
			}

			//**************************************初始化*************************************		
			
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/haier/book-info.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
				
			}

			//查询条件重置
			function bookInfoReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
			}
			
			$(document).ready(function(){		
				
				//新增通讯录
				$("#bookInfoAdd").click(function(){
					$('body').ajaxLoading('正在加载数据...');			
					$.post($.fn.getRootPath()+"/app/haier/book-info!addOrEditBookInfoTo.htm",
						{},function(data){
						if(data!=null&&data!=''){
							$("#bookInfoCotent").html(data);
							$('body').ajaxLoadEnd();
							openShade("bookInfoCotent");
						}
					},"html");				
				});
				
				//编辑通讯录
				$("#bookInfoEdit").click(function(){
					var bookInfoId_list = $("input:checkbox[name='bookInfoId_list']:checked")
					if(bookInfoId_list.length==0){
						parent.layer.alert("请选择要编辑的数据!", -1);
						return;
					}else if(bookInfoId_list.length>1){
						parent.layer.alert('不能同时编辑多条数据!', -1);
						return;
					}else{
						$('body').ajaxLoading('正在加载数据...');
						var bookInfoId = $(bookInfoId_list[0]).val();
						$.post($.fn.getRootPath()+"/app/haier/book-info!addOrEditBookInfoTo.htm",
							{'bookInfoId':bookInfoId},function(data){
							if(data!=null&&data!=''){	
								$("#bookInfoCotent").html(data);
								$('body').ajaxLoadEnd();
								openShade("bookInfoCotent");
							}
						},"html")
					}									
				});			
				
				//删除通讯录
				$("#bookInfoDel").click(function(){
					var bl = false;
					var bookInfoIds = "";
					$("input:checkbox[name='bookInfoId_list']:checked").each(function(){
						bl = true;
						bookInfoIds += $(this).val()+",";
					});
					if(!bl){
						parent.layer.alert("请选择要删除的数据!", -1);
						return;
					}
					if (bl){
						parent.layer.confirm("确定要删除选中的数据吗？", function(){
							$('body').ajaxLoading('正在保存数据...');
							$.post($.fn.getRootPath()+"/app/haier/book-info!delBookInfo.htm",
								{'bookInfoIds':bookInfoIds},
								function(data){
									$('body').ajaxLoadEnd();

									if($.parseJSON(data).rst){
										parent.layer.alert($.parseJSON(data).msg, -1);					
										pageClick('1');
									}else{
										parent.layer.alert($.parseJSON(data).msg, -1);
									}
							},"html");
						});
					}
				});
			});
		
		</script>
		<style type="text/css">
			.bookinfo_titile div label {margin:2px 2px; color: #666;}
			.bookinfo_titile div label a input {
				border: 1px solid #cccccc;
			  	height: 28px;
			  	line-height: 25px;
			  	padding-left: 2px;
			  	width: 86px;
			}
		</style>
	</head>
	<body>
<!--******************************************************通讯录管理初始化页面*********************************************************************************  -->
		<form id="form0" action="" method="post">
			<div class="content_right_bottom" style="min-width:860px;">

				<div class="gonggao_titile">
					<div class="gonggao_titile_right">
						<a href="javascript:void(0);" title="导出选择"  onclick="exportBookInfo('0')">导出选择</a>
				    	<a href="javascript:void(0);" title="导出全部"  onclick="exportBookInfo('1')">导出全部</a>
				    	<a href="javascript:void(0);" title="批量导入"  onclick="importBookInfo()">批量导入</a>
				    	<a href="javascript:void(0);" title="删除"  id="bookInfoDel"">删除</a>
				    	<a href="javascript:void(0);" title="编辑"  id="bookInfoEdit">编辑</a>
				    	<a href="javascript:void(0);" title="新增"  id="bookInfoAdd">新增</a>
					</div>
				</div>

				<div class="bookinfo_titile" style="height: 40px;">
					<div style="width: 100%;">
						<label>
							姓名：
							<input style="height:20px;width:100px;" id="name_select"  name="name" type="text" value="${name }"/>
						</label>
						<label>
							工贸：
							<input style="height:20px;width:100px;" id="gm_select" name="gm" type="text" value="${gm }"/>
						</label>
						<label>
							公司职务：
							<input style="height:20px;width:100px;" id="station_select"  name="position" type="text" value="${position }"/>
						</label>
						<label>
							<a href="javascript:void(0)"><input type="button" class="bookInfo_aa1" value="重置" onclick="javascript:bookInfoReset()"/></a>
							<a href="javascript:void(0)"><input type="button" class="bookInfo_aa2" value="查询" onclick="javascript:pageClick('1');"/></a>
						</label>
					</div>
				</div>
				<div class="gonggao_con">
					<div class="gonggao_con_nr">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="tdbg">
								<td>
									<input type="checkbox" name="ck_all" onclick="checkAll()"/>
								</td>
								<td>
									姓名
								</td>
								<td>
									办公电话
								</td>
								<td>
									部门
								</td>
								<td>
									工贸
								</td>
								<td>
									产品
								</td>
								<td>
									公司职务
								</td>
								<td>
									手机
								</td>
								<td>
									邮箱
								</td>
								<td>
									产品技术经理
								</td>
								<td>
									传真
								</td>
								<td>
									备注
								</td>
							</tr>
							<s:iterator value="page.result" var="va">
								<tr>
									<td>
										<input name="bookInfoId_list" type="checkbox" value="${va.id }" />
									</td>
									<td>
										${va.name }
									</td>
									<td>
										${va.officePhone }
									</td>
									<td>
										<s:iterator value="#va.stations" var="va1">
											${va1.name }									
										</s:iterator>
									</td>
									<td>
										${va.gm }
									</td>
									<td>
										${va.product }
									</td>
									<td>
										${va.position }
									</td>
									<td>
										${va.phone }
									</td>
									<td>
										${va.email }
									</td>
									<td>
										${va.productManager }
									</td>
									<td>
										${va.fax }
									</td>
									<td>
										${va.remark }
									</td>
								</tr>
							</s:iterator>						
							<tr class="trfen">
								<td colspan="12">
									<jsp:include page="../util/part_fenye.jsp"></jsp:include>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
		
		
<!--*******************用户新增、编辑**********************************************************************************************************************-->		
		<div id="bookInfoCotent" style="display:none;position:fixed;z-index:10001;_position:absolute;"></div>
		
		<jsp:include page="import.jsp"></jsp:include>
	</body>
</html>

