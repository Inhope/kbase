<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
<link
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinjian_dan.css"
	rel="stylesheet" type="text/css" />
<style type="text/css">
.inputflag {
	color: red;
	font-size: 14px;
}
</style>
<script type="text/javascript">	
			
	//内容重置
	function BookInfoReset(){
		$("input[id$='_bookinfo']").each(function(){
			$(this).attr("value","");
		});
		
		$("#errorMessage").html("");//清掉提示信息
	}
			
	//用户新增、编辑		
	function addOrEditBookInfo(){

		var name_bookinfo =  $("#name_bookinfo").val();
		var stationName_bookinfo =  $("#stationName_bookinfo").val();
		var officePhone_bookinfo =  $("#officePhone_bookinfo").val();
		var gm_bookinfo =  $("#gm_bookinfo").val();
		var product_bookinfo =  $("#product_bookinfo").val();
		var position_bookinfo =  $("#position_bookinfo").val();
		var phone_bookinfo =  $("#phone_bookinfo").val();			
		var email_bookinfo =  $("#email_bookinfo").val();
		var productManager_bookinfo =  $("#productManager_bookinfo").val();
		var fax_bookinfo =  $("#fax_bookinfo").val();
		var remark_bookinfo =  $("#remark_bookinfo").val();

		var errorMessage = $("#errorMessage");		
				
		if(name_bookinfo==''){
			$(errorMessage).html("姓名不能为空!");
			return;
		}
		if(stationName_bookinfo==''){
			$(errorMessage).html("部门不能为空!");
			return;
		}		
		//if(phone_bookinfo==''){
		//	$(errorMessage).html("手机号不能为空!");
		//	return;
		//}
		if(email_bookinfo!=''){//邮箱不为空验证格式
			var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!filter.test(email_bookinfo)) {
				$(errorMessage).html("邮箱不符合标准!");
				return;
			}
		}
		if(phone_bookinfo!=''){//手机号码不为空验证格式
			var filter  = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
			if (!filter.test(phone_bookinfo)) {
				$(errorMessage).html("手机号码不符合标准!");
				return;
			}
		}

		//分页跳转
		function pageClick(pageNo){
			$('body').ajaxLoading('正在查询数据...');
			$("#form0").attr("action", $.fn.getRootPath()+"/app/haier/book-info.htm");
			$("#pageNo").val(pageNo);
			$("#form0").submit();
			
		}
		
		$.post(
			$.fn.getRootPath()+"/app/haier/book-info!checkData.htm",
			{'name':name_bookinfo,'name2':$("#name2_bookinfo").val()},
			function(data){
				if($.parseJSON(data).rst){
						$(errorMessage).html("");
						$('#form_bookinfo').form('submit', {
					        url: $.fn.getRootPath() + "/app/haier/book-info!addOrEditBookInfoDo.htm",  
					        onSubmit:function(param){
								return $(this).form('validate');
					        },
					        success:function(data){
					        	parent.layer.alert($.parseJSON(data).msg, -1);
					   			closeShade("bookInfoCotent");
					   			pageClick('1');
					        },
					        error:function(data){
					        	$(errorMessage).html($.parseJSON(data).msg);
						    }
					    });
				}else{
					$(errorMessage).html($.parseJSON(data).msg);
				}
		},"html");
	}

	$(document).ready(function(){
		
		//部门选择
		$('#stationName_bookinfo').click(function(){
			$.kbase.picker.stationByDept({returnField:"stationName_bookinfo|stationId_bookinfo"});
		});
	});		
</script>
<div class="userShade_d" style="width: auto;">
	<div class="userShade_dan" style="width: 500px;">
		<div class="userShade_dan_title">
			<b id="userShadeTitle"><s:if
					test="bookInfo!=null&&bookInfo.id!=null">编辑通讯录</s:if>
				<s:else>新建通讯录</s:else>
			</b>
			<a href="javascript:void(0);" onclick="closeShade('bookInfoCotent')">
				<img
					src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
					width="12" height="12" /> </a>
		</div>
		<div class="userShade_dan_con">
			<form id="form_bookinfo" action="" method="post">
				<input type="hidden" name="bookInfo.id" value="${bookInfo.id }" />
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th>
							姓名
							<span class="inputflag">*</span>：
						</th>
						<td>
							<input id="name_bookinfo" name="bookInfo.name" value="${bookInfo.name }" type="text" />
							<input id="name2_bookinfo" name="name2" value="${bookInfo.name }" type="hidden" />
						</td>
						<th>
							部门
							<span class="inputflag">*</span>：
						</th>
						<td colspan="1">
							<s:set var="stationId_bookinfo" value="''" />
							<s:set var="stationName_bookinfo" value="''" />
							<s:if test="#request.bookInfo.stations!=null && #request.bookInfo.stations.size>0">
								<s:iterator value="#request.bookInfo.stations" var="va">
									<s:set var="stationId_bookinfo" value="#va.id" />
									<s:set var="stationName_bookinfo" value="#va.name" />
								</s:iterator>
							</s:if>
							
							<input id="stationName_bookinfo" name="stationName_bookinfo" readonly="readonly" type="text" value="${stationName_bookinfo }" />
							<input id="stationId_bookinfo" name="stationId_bookinfo" type="hidden" value="${stationId_bookinfo }" />
						</td>
					</tr>
					<tr>
						<th>
							办公电话:
						</th>
						<td>
							<input id="officePhone_bookinfo" name="bookInfo.officePhone" value="${bookInfo.officePhone }" type="text" />
						</td>
						<th>
							工贸：
						</th>
						<td>
							<input id="gm_bookinfo" name="bookInfo.gm" value="${bookInfo.gm }" type="text" />
						</td>
					</tr>
					<tr>
						<th>
							产品:
						</th>
						<td>
							<input id="product_bookinfo" name="bookInfo.product" value="${bookInfo.product }" type="text" />
						</td>
						<th>
							公司职务:
						</th>
						<td>
							<input id="position_bookinfo" name="bookInfo.position" value="${bookInfo.position }" type="text" />
						</td>
					</tr>
					<tr>
						<th>
							手机:
						</th>
						<td>
							<input id="phone_bookinfo" name="bookInfo.phone" value="${bookInfo.phone }" type="text" />
						</td>
						<th>
							邮箱：
						</th>
						<td>
							<input id="email_bookinfo" name="bookInfo.email" value="${bookInfo.email}" type="text" />
						</td>
					</tr>
					<tr>
						<th>
							产品技术经理:
						</th>
						<td>
							<input id="productManager_bookinfo" name="bookInfo.productManager" value="${bookInfo.productManager }" type="text" />
						</td>
						<th>
							传真：
						</th>
						<td>
							<input id="fax_bookinfo" name="bookInfo.fax" value="${bookInfo.fax}" type="text" />
						</td>
					</tr>
					<tr>
						<th>
							备注：
						</th>
						<td colspan="3">
							<input id="remark_bookinfo" name="bookInfo.remark" value="${bookInfo.remark}" type="text" />
						</td>
					</tr>
					<tr>
						<td colspan="4" class="anniu">
							<label style="float: left; color: red;" id="errorMessage"></label>
							<a href="#"><input type="button"
									onclick="addOrEditBookInfo()" class="userShade_aa2" value="提交" />
							</a>
							<a href="#"><input type="reset" onclick="BookInfoReset()"
									class="userShade_aa1" value="重置" /> </a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>


