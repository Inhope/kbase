<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<script type="text/javascript">
	$(function(){
		$("#importBookInfo").click(function(){
			if($("#bookInfoFile").val()==null||$("#bookInfoFile").val()==''){
				parent.layer.alert("请选择需要上传的文件!", -1);
				return false;
			}
			//数据导入
			$.ajaxFileUpload({
				url: $.fn.getRootPath()+"/app/haier/book-info!importBookInfo.htm",
				secureuri: false,
				fileElement: $("#bookInfoFile"),
				uploadFileParamName : 'bookInfoFile',
				dataType : 'json',
				success: function(data, status) {
					$('body').ajaxLoadEnd();
					var obj = jQuery.parseJSON(data);
					if(obj.rst){
		   				location.reload();
					}
					parent.layer.alert(obj.msg, -1);
				},
				error: function(obj, msg, e) {
					parent.layer.alert("网络错误，请稍后再试!", -1);
				}
			});
		
		});
	});
</script>
<div id="ImportShade" style="display:none;">
	<ul>
		<li id="scBefore" style="padding-left: 30px;">
			<p>
				<br />
				注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/haier/book-info!downloadMouldFile.htm?fileName=bookInfoImportMould.xls" style="color: blue">点击下载</a>)填写相关数据
				<br />
				特别提示：
				<br />
				1、请及时下载最新模板
				<br />
				2、上传内容请严格按照模板要求来填写
				<br />
				<br />
			</p>
		</li>
		<li id="importHandle">	
			<p style="float: left;">
				<input type="file" id="bookInfoFile" name=bookInfoFile />
			</p>
			<span style="float: left;">
				<input href="javascript:void(0)" id="importBookInfo" value="上传" type="button">
			</span>
		</li>
	</ul>						
</div>
