<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=10"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/css/main.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/css/index.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">
			window.__kbs_isBailian = '<%=SystemKeys.isBailian() %>';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/index/js/IndexAjax.js"></script>
	</head>
	<body>
		<!-- 广州移动定制(Gassol.Bi 2016-4-8 15:33:24) -->
		<kbs:if test="<%=SystemKeys.isGzyd() %>">
			<script type="text/javascript">
				isGzyd = true;/*判断是否为广州移动项目*/
				var jobNumber = '${sessionScope.session_user_key.userInfo.encryptJobNumber }';/*获取加密后的工号免登陆使用*/
				function qkLinklog(obj){
					/*打开独立窗口*/
					window.open ($.fn.getRootPath() + '/app/login/login.htm?jobNumber=' + jobNumber, $(obj).attr('title'), 
						'left=0,top=0,width='+ (screen.availWidth - 10) +',height='+ (screen.availHeight - 10) + ',scrollbars,resizable=yes,toolbar=no');
					/*记录点击日志*/
					$.post($.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!qkLinkLog.htm',
						{title: $(obj).attr('title')}, function(jsonResult){ }, 'json');
					return false;
				}
			</script>
		</kbs:if>
		<div class="content_index">
			<div class="content_index_news">
				<ul>
					<s:iterator var="adl" value="#request.allLeftDatas">
							<li>
								<div id="dataType_<s:property value='value'/>" class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
		
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div class="content_news1_con">
											<ul>
												
												<!-- 
												<li class="content_news1_more">
													<a href="javascript:void(0);" class="more">更多</a>
												</li> -->
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
		
									<div class="clear"></div>
								</div>
							</li>
					</s:iterator>
				</ul>
			</div>
			
			<s:set name="rightPanelDataSize" value="7"></s:set>
			<div class="content_you">
				<ul>
					<s:iterator value="#request.allRightDatas">
						<li class="content_dq">
							<div id="dataType_<s:property value='value'/>" isRight="isRight">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div class="dq_con" style="width: 90%;">
								</div>
								<div class="dq_more"><a class="more" href="javascript:void(0);">更多&gt;&gt;</a></div>
							</div>
						</li>
					</s:iterator>
				</ul>
			</div>
		</div>

	</body>
</html>
