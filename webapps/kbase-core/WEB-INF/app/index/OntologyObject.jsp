<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/object.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<!-- 		日志记录 -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/index/js/OntologyObject.js"></script>
<script type="text/javascript">
</script>
</head>

<body>



<!--******************内容开始***********************-->

<div class="content_right_bottom">

<div class="zhishi_titile">
<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico.jpg" width="16" height="15" />
<span id="categoryNv"></span>
</div>


<div class="zhishi_con">

<div class="zhishi_con_title">
<table>
<tr>
<td class="biaoti"><a href="javascript:void(0);"><b>名称</b></a></td>
	<td><a href="javascript:void(0);">开始时间</a></td>
	<td><a href="javascript:void(0);">结束时间</a></td>
	<td><a href="javascript:void(0);">修改时间</a></td>
</tr>
</table>
</div>


<div class="zhishi_con_nr">
<ul>
	
<s:iterator value="#request.list" status='st'>
<s:if test="objectId==null || objectId==''">
	<s:if test="#st.last">
		<li>
	</s:if>
	<s:else>
		<li style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
	</s:else>
		<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;">
			<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg" width="16" height="15" />
			<b style="cursor:pointer" categoryId="${id},${categoryId}" >${name}</b>
		</h1>
	</li>
</s:if>
<s:else>
	<s:if test="#st.last">
		<li>
	</s:if>
	<s:else>
		<li style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
	</s:else>
	<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
		<b style="cursor:pointer" categoryId="${id},${categoryId},${objectId},${searchMode}">
			${name}
			<s:if test="pastEnd">
				<span style="color:red">(未过追溯期)</span>
			</s:if>
		</b>
	</h1>
	<h2><s:date format="yyyy-MM-dd" name="startTime"/><s:if test="startTime==null">/</s:if></h2>
	<h3><s:date format="yyyy-MM-dd" name="endTime"/><s:if test="endTime==null">/</s:if></h3>
	<h4><s:date format="yyyy-MM-dd" name="editTime"/><s:if test="editTime==null">/</s:if></h4>
	</li>
</s:else>
</s:iterator>

</ul>
</div>
</div>

</div>




<!--******************内容结束***********************-->

</body>
</html>

