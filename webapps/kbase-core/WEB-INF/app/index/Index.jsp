<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/css/main.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/css/index.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/index/js/Index.js"></script>
		<script type="text/javascript">
			pageContext = {
				isWukong: '${requestScope.isWukong}',
				contextPath: '${pageContext.request.contextPath}'
			}
		</script>
	</head>
	<body>
		<s:set name="panelDataSize" value="6"/>
		<s:set name="maxLengthLeft" value="20"/>
		<s:set name="maxLengthRight" value="12"/>
		<!-- 广州移动定制(Gassol.Bi 2016-4-8 15:33:24) -->
		<kbs:if test="<%=SystemKeys.isGzyd() %>">
			<script type="text/javascript">
				function qkLinklog(obj){
					$.post($.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!qkLinkLog.htm',
						{title: $(obj).attr('title')}, function(jsonResult){ }, 'json');
				}
			</script>
		</kbs:if>
		<div class="content_index">
			<div class="content_index_news">
				<ul>
					<s:iterator var="adl" value="#request.allLeftDatas">
						<s:if test="key == '我的课堂'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
		
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
											<span id="trainOrExam">
											<!-- class="dangqian" --><a href="javascript:void(0);">培训</a>|<a href="javascript:void(0);">考试</a>
											</span>
										</div>
										<div class="content_news1_con" id="trainOrExamDiv">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="nkSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="nkSize" value="5"></s:set>
												</s:else>
												<s:iterator var="nk" value="value" begin="0" end="#nkSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0);" _type="<s:property value="#nk.type"/>" _title="<s:property value="#nk.name"/>" _id="<s:property value="#nk.id"/>" _courseId="<s:property value="#nk.courseId"/>">
															<s:if test="#nk.type == 'exam'">
																<font color="red">【考试】</font>
															</s:if>
															<s:if test="#nk.type == 'train'">
																<font color="green">【培训】</font>
															</s:if>
															<s:if test="#nk.name.length() > #maxLengthLeft">
																<s:property value="#nk.name.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																<s:property value="#nk.name"/>
															</s:else>
														</a>
														<span>
															<s:property value="#nk.time"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more" id="ceMore" style="display: none;">
													<s:if test="value.isEmpty == false">
														<a href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
		
									<div class="clear"></div>
								</div>
							</li>
						</s:if>
						<s:if test="key == '最新知识'">
							<li>
								<div id="newPanel" class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
		
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="nkSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="nkSize" value="5"></s:set>
												</s:else>
												<s:iterator var="nk" value="value" begin="0" end="#nkSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0);" title="${nk.question}" title_="${nk.ontologyObject.name}" _id="${nk.valueId}" 
															bizTplEnable="${nk.bizTplEnable }" bizTplArticleId="${nk.bizTplArticleId }">
															<s:if test="#nk.question.length() > #maxLengthLeft">
																<s:property value="#nk.question.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																<s:property value="#nk.question"/>
															</s:else>
														</a>
														<span>
															<s:date name="#nk.createTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
		
									<div class="clear"></div>
								</div>
							</li>
						</s:if>
						<s:elseif test="key == '最热知识'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
		
										<div class="content_news1_title">
											<p>
												<a href="javascript:void(0);"><s:property value="key"/></a>
											</p>
											<span id="period">
												<a href="javascript:void(0);" class="dangqian">周</a>|<a href="javascript:void(0);">月</a>|<a href="javascript:void(0);">日</a>
											</span>
										</div>
		
										<div id="hotDiv" class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="nwSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="nwSize" value="5"></s:set>
												</s:else>
												<s:iterator var="nw" value="value" begin="0" end="#nwSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a id="${nw.objectId}" href="javascript:void(0);" title="${nw.name}">
															<s:if test="#nw.name.length() > #maxLengthLeft">
																<s:property value="#nw.name.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																${nw.name}
															</s:else>
														</a>
														<span>
															<s:date name="#nw.startTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '收藏夹'">
							<li>
								<div id="favClipDiv" class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="fcSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="fcSize" value="5"></s:set>
												</s:else>
												<s:iterator var="fc" value="value" begin="0" end="#fcSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a id="${fc.id}" href="javascript:void(0);" title="${fc.favClipName}">
															<s:if test="#fc.favClipName.length() > #maxLengthLeft">
																<s:property value="#fc.favClipName.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																${fc.favClipName}
															</s:else>
														</a>
														<span>
															<s:date name="#fc.createTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<a id="favClipMore" href="javascript:void(0);">更多</a>
													<!-- 
													<s:if test="value.isEmpty == false">
														<a id="favClipMore" href="javascript:void(0);">更多</a>
													</s:if>
													 -->
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '岗位知识'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div id="roleKnowledgePanel" class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="skSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="skSize" value="5"></s:set>
												</s:else>
												<s:iterator var="sk" value="value" begin="0" end="#skSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a id="${sk.objectId}" href="javascript:void(0);" title="${sk.name}">
															<s:if test="#sk.name.length() > #maxLengthLeft">
																<s:property value="#sk.name.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																${sk.name}
															</s:else>
														</a>
														<span>
															<s:date format="yyyy-MM-dd" name="#sk.startTime"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '公布栏'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);">
											  <!-- <s:property value="key"/> --> 公告栏&nbsp;
											   <s:if test="#request.countNotice!=null">
											       <c:choose>
													  <c:when test="${countNotice==0}"></c:when>
													  <c:when test="${countNotice>99}">(<b style="font-size:16px;color:yellow;">99+</b>)</c:when>
													  <c:otherwise>(<b style="font-size:16px;color:yellow;">${countNotice}</b>)</c:otherwise>
												   </c:choose>
											   </s:if>
											</a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="nkSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="nkSize" value="5"></s:set>
												</s:else>
												<s:iterator var="nk" value="value" begin="0" end="#nkSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0);" onclick="javascript:parent.getNoticeContent('<s:property value="#nk.id"/>')" title="${nk.title}">
															<s:if test="#nk.title.length() > #maxLengthLeft">
																<s:property value="#nk.title.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																<s:property value="#nk.title"/>
															</s:else>
														</a>
														<span>
															<s:date name="#nk.startTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a id="noticeMoreBtn" href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '便签'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);">
											  <!-- <s:property value="key"/> --> 便签&nbsp;
											   <s:if test="#request.countNote!=null">
											       <c:choose>
													  <c:when test="${countNote==0}"></c:when>
													  <c:when test="${countNote>99}">(<b style="font-size:16px;color:yellow;">99+</b>)</c:when>
													  <c:otherwise>(<b style="font-size:16px;color:yellow;">${countNote}</b>)</c:otherwise>
												   </c:choose>
											   </s:if>
											</a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="nkSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="nkSize" value="5"></s:set>
												</s:else>
												<s:iterator var="nk" value="value" begin="0" end="#nkSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0);" onclick="javascript:parent.getNoteContent('<s:property value="#nk.id"/>')" title="${nk.title}">
															<s:if test="#nk.title.length() > #maxLengthLeft">
																<s:property value="#nk.title.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																<s:property value="#nk.title"/>
															</s:else>
														</a>
														<span>
															<s:date name="#nk.startTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a id="noteMoreBtn" href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>						
						<s:elseif test="key == '快速链接'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="qlSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="qlSize" value="5"></s:set>
												</s:else>
												<!-- 广州移动定制(Gassol.Bi 2016-4-8 15:33:24) -->
												<kbs:if test="<%=SystemKeys.isGzyd() %>">
													<s:if test="#qlSize == 5">
														<s:set name="qlSize" value="#qlSize-1"></s:set>
													</s:if>
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="${pageContext.request.contextPath}/app/login/login.htm?jobNumber=${sessionScope.session_user_key.userInfo.encryptJobNumber }" 
															onclick="javascript:qkLinklog(this);"
															title="原子化知识库" target="_blank">原子化知识库</a>
														<span>
															2099-01-01
														</span>
													</li>
												</kbs:if>
												<s:iterator var="ql" value="value"  begin="0" end="#qlSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="${ql.address}" title="${ql.name}" target="_blank">
															<s:if test="#ql.name.length() > #maxLengthLeft">
																<s:property value="#ql.name.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																${ql.name}
															</s:else>
														</a>
														<span>
															<s:date name="#ql.createTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<a id="more_quick_link" href="javascript:void(0);">更多</a>
													<!--  
													<s:if test="value.isEmpty == false">
														<a id="more_quick_link" href="javascript:void(0);">更多</a>
													</s:if>-->
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '推荐知识'">
							<li>
								<div class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
											<!-- 
											<span style=""><a href="javascript:void(0);" class="dangqian">周</a>|<a href="javascript:void(0);">月</a>|<a href="javascript:void(0);">日</a></span>
											 -->
										</div>
										<div id="recom_object_left" class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="qlSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="qlSize" value="5"></s:set>
												</s:else>
												<s:iterator var="rem" value="value" begin="0" end="#qlSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0)" title="${rem.kbVal.question}" title_="${rem.kbVal.kbObject.name}">
															<s:if test="#rem.kbVal.question.length() > #maxLengthLeft">
																<s:property value="#rem.kbVal.question.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																${rem.kbVal.question}
															</s:else>
														</a>
														<!-- 
														<span>
															<s:date name="#rem.createTime" format="yyyy-MM-dd"/>
														</span>
														 -->
													</li>
												</s:iterator>
												<li  id="recom_object_more_left" class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a id="rem_object" href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
						<s:elseif test="key == '更新知识'">
							<li>
								<div id="updatePanel" class="content_news1">
									<div class="content_index_news_left"></div>
									<div class="content_index_news_center">
		
										<div class="content_news1_title">
											<a href="javascript:void(0);"><s:property value="key"/></a>
										</div>
										<div class="content_news1_con">
											<ul>
												<s:if test="value.size <= #panelDataSize">
													<s:set name="ukSize" value="value.size - 1"></s:set>
												</s:if>
												<s:else>
													<s:set name="ukSize" value="5"></s:set>
												</s:else>
												<s:iterator var="uk" value="value" begin="0" end="#ukSize">
													<li>
														<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/index/images/index_dian.jpg" wclassth="3" height="3" />
														<a href="javascript:void(0);" title="${uk.question}" title_="${uk.ontologyObject.name}" _id="${uk.valueId}"
															bizTplEnable="${uk.bizTplEnable }" bizTplArticleId="${uk.bizTplArticleId }">
															<s:if test="#uk.question.length() > #maxLengthLeft">
																<s:property value="#uk.question.substring(0, #maxLengthLeft) + '...'"/>
															</s:if>
															<s:else>
																<s:property value="#uk.question"/>
															</s:else>
														</a>
														<span>
															<s:date name="#uk.editTime" format="yyyy-MM-dd"/>
														</span>
													</li>
												</s:iterator>
												<li class="content_news1_more">
													<s:if test="value.isEmpty == false">
														<a href="javascript:void(0);">更多</a>
													</s:if>
												</li>
											</ul>
										</div>
									</div>
									<div class="content_index_news_right"></div>
		
									<div class="clear"></div>
								</div>
							</li>
						</s:elseif>
					</s:iterator>
				</ul>
			</div>
			
			<s:set name="rightPanelDataSize" value="7"></s:set>
			<div class="content_you">
				<ul>
					<s:iterator value="#request.allRightDatas">
						<s:if test="key == '我的课堂'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div class="dq_con" style="width: 90%;" id="rightTrainOrExam">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="nkSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="nkSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="nk" value="value" begin="0" end="#nkSize">
										<a href="javascript:void(0);" title="${nk.name}" _type="<s:property value="#nk.type"/>" _id="<s:property value="#nk.id"/>" _courseId="<s:property value="#nk.courseId"/>">
											<s:if test="#nk.name.length() > #maxLengthRight">
												<s:property value="#nk.name.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${nk.name}
											</s:else>
										</a>
									</s:iterator>
								</div>
								<!-- <div class="dq_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div> -->
							</li>
						</s:if>
						<s:if test="key == '最新知识'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div id="newPanel1" class="dq_con" style="width: 90%;">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="nkSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="nkSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="nk" value="value" begin="0" end="#nkSize">
										<a href="javascript:void(0);" title="${nk.question}" title_="${nk.ontologyObject.name}" _id="${nk.valueId}"
											bizTplEnable="${nk.bizTplEnable }" bizTplArticleId="${nk.bizTplArticleId }">
											<s:if test="#nk.question.length() > #maxLengthRight">
												<s:property value="#nk.question.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${nk.question}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:if>
						<s:elseif test="key == '最热知识'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div id="hotDiv1" class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="hkSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="hkSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="hk" value="value" begin="0" end="#hkSize">
										<a id="${hk.objectId}" href="javascript:void(0);" title="${hk.name}">
											<s:if test="#hk.name.length() > #maxLengthRight">
												<s:property value="#hk.name.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${hk.name}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '收藏夹'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div class="dq_con" id="favClipDiv1">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="fcSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="fcSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="fc" value="value" begin="0" end="#fcSize">
										<a id="${fc.id}" href="javascript:void(0);" title="${fc.favClipName}">
											<s:if test="#fc.favClipName.length() > #maxLengthRight">
												<s:property value="#fc.favClipName.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${fc.favClipName}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a id="favClipMore" href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '岗位知识'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div id="roleKnowledgePanel1" class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="skSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="skSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="sk" value="value" begin="0" end="#skSize">
										<a id="${sk.objectId}" href="javascript:void(0);" title="${sk.name}">
											<s:if test="#sk.name.length() > #maxLengthRight">
												<s:property value="#sk.name.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${sk.name}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '公布栏'">
							<li class="content_dq">
								<div class="dq_titile">
									<a href="javascript:void(0);">
									  <!--  <s:property value="key"/> -->公告栏&nbsp;
									    <s:if test="#request.countNotice!=null">
									       <c:choose>
											  <c:when test="${countNotice==0}"></c:when>
											  <c:when test="${countNotice>99}">(<b style="font-size:16px;color:yellow;">99+</b>)</c:when>
											  <c:otherwise>(<b style="font-size:16px;color:yellow;">${countNotice}</b>)</c:otherwise>
										   </c:choose>
									   </s:if>
									</a>
								</div>
								<div class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="nkSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="nkSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="nk" value="value" begin="0" end="#nkSize">
										<a href="javascript:void(0);" onclick="javascript:parent.getNoticeContent('<s:property value="#nk.id"/>')" title="${nk.title}">
											<s:if test="#nk.title.length() > #maxLengthRight">
												<s:property value="#nk.title.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${nk.title}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a id="noticeMoreBtn" href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '便签'">
							<li class="content_dq">
								<div class="dq_titile">
									<a href="javascript:void(0);">
									  <!--  <s:property value="key"/> -->便签&nbsp;
									   <s:if test="#request.countNote!=null">
									       <c:choose>
											  <c:when test="${countNote==0}"></c:when>
											  <c:when test="${countNote>99}">(<b style="font-size:16px;color:yellow;">99+</b>)</c:when>
											  <c:otherwise>(<b style="font-size:16px;color:yellow;">${countNote}</b>)</c:otherwise>
										   </c:choose> 
									   </s:if>
									</a>
								</div>
								<div class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="nkSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="nkSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="nk" value="value" begin="0" end="#nkSize">
										<a href="javascript:void(0);" onclick="javascript:parent.getNoteContent('<s:property value="#nk.id"/>')" title="${nk.title}">
											<s:if test="#nk.title.length() > #maxLengthRight">
												<s:property value="#nk.title.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${nk.title}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a id="noteMoreBtn" href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>						
						<s:elseif test="key == '快速链接'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="qlSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="qlSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<!-- 广州移动定制(Gassol.Bi 2016-4-8 15:33:24) -->
									<kbs:if test="<%=SystemKeys.isGzyd() %>">
										<s:if test="#qlSize == (#rightPanelDataSize - 1)">
											<s:set name="qlSize" value="#qlSize-1"></s:set>
										</s:if>
										<a href="${pageContext.request.contextPath}/app/login/login.htm?jobNumber=${sessionScope.session_user_key.userInfo.encryptJobNumber }" 
											onclick="javascript:qkLinklog(this);"
											title="原子化知识库" target="_blank">原子化知识库</a>
									</kbs:if>
									<s:iterator var="ql" value="value" begin="0" end="#qlSize">
										<a href="${ql.address}" title="${ql.name}" target="_blank">
											<s:if test="#ql.name.length() > #maxLengthRight">
												<s:property value="#ql.name.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${ql.name}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more" id="quick_link_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '推荐知识'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div id="recom_object_right" class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="qlSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="qlSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="rem" value="value" begin="0" end="#qlSize">
										<a href="###"  title="${rem.kbVal.question}" title_="${rem.kbVal.kbObject.name}" >
											<s:if test="#rem.kbVal.question.length() > #maxLengthRight">
												<s:property value="#rem.kbVal.question.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${rem.kbVal.question}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div id="recom_object_more_right" class="dq_more" id="rem_object"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
						<s:elseif test="key == '更新知识'">
							<li class="content_dq">
								<div class="dq_titile"><a href="javascript:void(0);"><s:property value="key"/></a></div>
								<div id="updatePanel1" class="dq_con">
									<s:if test="value.size <= #rightPanelDataSize">
										<s:set name="ukSize" value="value.size - 1"></s:set>
									</s:if>
									<s:else>
										<s:set name="ukSize" value="#rightPanelDataSize - 1"></s:set>
									</s:else>
									<s:iterator var="uk" value="value" begin="0" end="#ukSize">
										<a href="javascript:void(0);" title="${uk.question}" title_="${uk.ontologyObject.name}" _id="${uk.valueId}"
											bizTplEnable="${uk.bizTplEnable }" bizTplArticleId="${uk.bizTplArticleId }">
											<s:if test="#uk.question.length() > #maxLengthRight">
												<s:property value="#uk.question.substring(0, #maxLengthRight) + '...'"/>
											</s:if>
											<s:else>
												${uk.question}
											</s:else>
										</a>
									</s:iterator>
								</div>
									<div class="dq_more"><a href="javascript:void(0);">更多&gt;&gt;</a></div>
							</li>
						</s:elseif>
					</s:iterator>
				</ul>
			</div>

		</div>

	</body>
</html>
