<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@page import="com.eastrobot.util.SystemKeys"%>
<% 
  //searchMode="3" 最新知识   searchMode="11" 更新知识 
  String searchMode = (String)request.getAttribute("searchMode");
  String actionURL = "/app/index/knowledges!newKnowledges.htm";
  if(searchMode!=null&&searchMode.trim().equals("11")){
      actionURL = "/app/index/knowledges!updateKnowledges.htm";
  }
%>
<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yiban.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/index/js/NormSearch.js"></script>
	</head>
	<script type="text/javascript">
	var searchMode = '${searchMode}';
	</script>

	<body>



		<!--******************内容开始***********************-->

		<div class="content_right_bottom">

			<div class="yiban_title">
				<div class="yiban_title_left">
					共
					<span>${totalCount}</span>条记录
				</div>
				<div class="yiban_title_right">
					<a href="javascript:void(0);">预览</a>
					<input  type="checkbox" value="" />
				</div>
			</div>


			<div class="yiban_con">
				<div class="yiban_left" style="margin:0;">

					<div class="yiban_con_title">
						<h1>
							<b>知识名称</b>
						</h1>
						<h2>
							<a id="createClick" href="javascript:void(0);">创建时间</a>
						</h2>
						<h3>
							<a id="editClick" href="javascript:void(0);">修改时间</a>
						</h3>
						<h4>
							创建人
						</h4>
						<h5>
							<s:if test="#request.orderFiled == 'hot'">
								<a style="cursor:pointer" filed="hot" href="javascript:void(0);" desc="${desc}">
									热度
									<s:if test="#request.desc == 'desc'">
										<img style="margin-top: 13px; src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/yiban_ico3.jpg">
									</s:if>
									<s:else>
										<img style="margin-top: 13px; src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/gaoji_ico1.jpg">
									</s:else>
								</a>
							</s:if>
							<s:else>
								<a style="cursor:pointer">热度</a>
							</s:else>
							
						</h5>
						<h6></h6>
					</div>
					<div class="yiban_con_nr" style="word-break: break-all; word-wrap:break-word;">
						<ul>
							<s:iterator value="#request.list" var="ov">
							<li class="dangqian">
							<div class="dangqian_1">
								<h1 style="text-align:left;">
									<a href="javascript:void(0);" title="${ov.ontologyValue.question}" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width:80%;text-align:left;">
										${ov.ontologyValue.question}
									</a>
									<s:if test="#ov.ontologyValue.editTime != null && (new java.util.Date().getTime() - #ov.ontologyValue.editTime.getTime())<=24*3600*1000">
										<span style="width:10px;color:#f00;font-size:10px;line-height:21px;">
											new
										</span>
									</s:if>
								</h1>
								<h2>
									<s:date format="yyyy-MM-dd" name="#ov.ontologyValue.createTime" />
								</h2>
								<h3>
									<s:date format="yyyy-MM-dd" name="#ov.ontologyValue.editTime" />
								</h3>
								<h4>
									<s:if test="#ov.ontologyValue.editor==null || #ov.ontologyValue.editor==''" >/</s:if>
									<s:else>${ov.ontologyValue.editor}</s:else>
								</h4>
								<h5>
									${ov.ontologyValue.op}
								</h5>
								<kbs:if test="<%=!SystemKeys.isGzyd() %>">
								<h6>
									<a class="detail" href="${ov.ontologyValue.question}"><font color="#CC0000">详情</font></a>
								</h6>
								</kbs:if>
							</div>
								<div style="display:none;">
									<div class="dangqian_2">
										${ans}
									</div>
									<s:iterator value="url_p4">
										<iframe src="<s:property/>">
										</iframe>
										<br/>
									</s:iterator>
								</div>
							</li>
							</s:iterator>
						</ul>


						<s:if test="totalPage>1">
						<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
							<a href="javascript:void(0);" onfocus="this.blur();">首页</a>
							<a href="javascript:void(0);" onfocus="this.blur();">&lt;上一页</a>
							<s:iterator value="new int[totalPage]" status="i">
								<s:if test="#i.index+1 == currentPage"> 
									<a href="javascript:void(0);" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
								</s:if>
								<s:elseif test="currentPage < 5 && #i.index<10">
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
								</s:elseif>
								<s:elseif test="currentPage > totalPage -5 && #i.index < totalPage && #i.index > totalPage-11">
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
								</s:elseif>
								<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
								</s:elseif>
							</s:iterator>
							
							<a href="javascript:void(0);" onfocus="this.blur();">下一页></a>
							<a href="javascript:void(0);" onfocus="this.blur();">尾页</a>
						</div>
						</s:if>
						<form id="fenyeForm" method="post" style="display:none;" action="${pageContext.request.contextPath}<%=actionURL%>">
							<input type="text" hidden="hidden" name="start" value="${start}"/>
							<input type="text" hidden="hidden" name="orderFiled" value="${orderFiled}"/>
							<input type="text" hidden="hidden" name="desc" value="${desc}"/>
						</form>
					</div>
				</div>



			</div>


		</div>




		<!--******************内容结束***********************-->

	</body>
</html>

