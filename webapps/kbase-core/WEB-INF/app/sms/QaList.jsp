<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>标准问搜索列表管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gaoji.css" rel="stylesheet" type="text/css" />
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.tree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/main/js/Tab.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		
		<!-- 短信日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSLog.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.tree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
	
		<style type="text/css">
			.yonghu_titile li{padding-left:10px}
			
			.search_content {
			    width: 90px;
			    height: 25px;
			    line-height: 25px;
			    background: #004da7;
			    color: #FFF;
			    border: 0px solid #004da7;
			    cursor: pointer;
			    font-size: 12px;
			}
			
		</style>
		<script type="text/javascript">

		$(document).ready(function(){
			
			/**
			* 搜索按钮点击事件
			*/
			$('#btnSeacher').click(function(){
			   var keywords = $('input[name="content"]').val();
			   if(keywords==undefined || $.trim(keywords)==''){
			       alert('请输入搜索内容!');
			       return false;
			   }
			   PAGE.FUNC.PageInit(1);     
			});
			/**
			* 关键字输入框回车事件
			*/
			$('input[name="content"]').keypress(function(e){
			   var curKey = e.which;
			   if(curKey == 13){
			       $('#btnSeacher').click();
			   }
			});

			//单击标题展开答案预览
			$('.kbs-row-question').click(function(){
				var $row = $(this).parent('tr').next('tr');
				if ($row.length>0){
					if ($row.get(0).style.display=='none'){
						$row.show();
					}else{
						$row.hide();
					}
				}
			});


			//复选框点击事件
			$('input[name="qa_checkbox"]').click(function(){
				var tab = $('#box_dx',parent.document);
				//var tab = $('#box_dx',window.parent.opener.document);
				var qId = $(this).val();

				//选中
				if($(this).attr("checked") == "checked"){
					var answer = $("#" + qId + "_answer").text();
					var question = $("#" + qId + "_question").text();
					var cId = $("#"+qId+"_cid").text();
					if(answer!=null && answer!=undefined && answer != ''){
						answer = ReplaceAll(answer,"<font color=\"red\">","");
						answer = ReplaceAll(answer,"</font>","");
					}

					if(question!=null && question!=undefined && question != ''){
						question = ReplaceAll(question,"<font color=\"red\">","");
						question = ReplaceAll(question,"</font>","");
					}
					
					PAGE.FUNC.AppendDXAA(tab,qId,question,answer,cId);
				//取消
				}else{
					PAGE.FUNC.RemoveDXAA(qId,tab);
				}
			});
			
			$('input[name="content"]').focus();
		});	

		//分页跳转
		function pageClick(pageNo){
			$('body').ajaxLoading('正在查询数据...');
			$("#form0").attr("action", "${pageContext.request.contextPath}/app/sms/sms-ontology!searchForList.htm");
			$("#pageNo").val(pageNo);
			$("#form0").submit();		
		} 


		//替换
		function ReplaceAll(str, sptr, sptr1){
            while (str.indexOf(sptr) >= 0){
               str = str.replace(sptr, sptr1);
            }
            return str;
    	}
		var PAGE = PAGE || {};

		PAGE.FUNC = {
				AppendDXAA:function(tab,qId,question,answer,cId){
					if($('#'+qId,tab)==undefined || $('#'+qId,tab).attr('id')==undefined || $('#'+qId,tab)== []){
	                     $(tab).append('<tr id="'+qId+'" categoryid="'+cId+'">' + 
							'<td class="content_dx">'+question+'</td>' + 
							'<td class="content_dx" colspan="2">'+answer+'</td>' + 
							'<td class="checkbox_dx">' +
							'<a onclick="removeQa(\''+qId+'\');" href="javascript:void(0);">移除</a>' +
							'|<a class="kbs-fav" href="javascript:void(0);">收藏</a>' +
							'</td>' + 
						 '</tr>');  	
	                      //记录日志  add by heart.cao 2016-06-06
	                      SMSLog.log({
	                           categoryid: cId,
	                           category: $("#" + qId + "_category").text(),
	                           objectid: $("#" + qId + "_oid").text(),
	                           object: $("#" + qId + "_object").text(),
	                           questionid: qId,
	                           question: question,
	                           bh: $("#" + qId + "_bh").text()
	                      });  						 					                      
	                }           
				},
				RemoveDXAA:function(qId,tab){
					if($('#'+qId,tab)!=undefined || $('#'+qId,tab).attr('id')!=undefined || $('#'+qId,tab)!= []){
						$("#"+qId,tab).remove();
					}
				},
				PageInit:function(pageNo){
				    pageNo = (pageNo==undefined || pageNo==null)?pageNo=1:pageNo;
					pageClick(pageNo);
				}
		};
</script>
	</head>
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border: 0px;">
					<form id="form0" action="${pageContext.request.contextPath}/app/sms/sms-ontology!searchForList.htm" method="post">
					  <div class="content_right_bottom" style="height:380px;">
							<div class="top-opa">
								<input name="content" value="${content}" type="text" style="width:80%;height:20px;margin-left:10px;margin-top:10px;"/>
								<a href="javascript:void(0);"><input id="btnSeacher" class="search_content" type="submit" value="搜索"/></a>
							</div>
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%">
										<s:iterator value="page.result" var="va">
											<tr>
												<td width="5%"><input type="checkbox" name="qa_checkbox" id="id_${va.qid }" value="${va.qid }"/></td>
												<td width="95%" style="text-align: left;" class="kbs-row-question">【${va.bhname}】${va.question}</td>
											</tr>
											<tr style="display:none" class="kbs-row-answer">
												<td></td>
												<td class="kbs-answer-helios" style="text-align:left;">${va.answer}</td>
											</tr>
											<tr>
												<td colspan = "2" style="display:none">
													<textarea id="${va.qid }_answer" cols="" rows=""  >${va.answer}</textarea>
													<textarea id="${va.qid }_question" cols="" rows="" >${va.question}</textarea>
													<textarea id="${va.qid }_cid" cols="" rows="" >${va.cid}</textarea>
													<textarea id="${va.qid }_category" cols="" rows="" >${va.category}</textarea>
													<textarea id="${va.qid }_oid" cols="" rows="" >${va.oid}</textarea>
													<textarea id="${va.qid }_object" cols="" rows="" >${va.object}</textarea>
													<textarea id="${va.qid }_bh" cols="" rows="" >${va.bh}</textarea>
												</td>
											</tr>
										</s:iterator>
										<tr>
											<td></td>
											<td>
												<jsp:include page="../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

