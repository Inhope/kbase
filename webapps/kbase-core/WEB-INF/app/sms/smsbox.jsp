<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>发送短信</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}			
			table.box_dx {
				font-family: "Courier New", Courier;
				font-size: 12px;
				border: 1px solid #C0C0C0;
				padding: 0px;
			}			
			.box_dx th {
				text-align: center;
				background-color: #C0C0C0;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}			
			.box_dx td {
				font-family: "Courier New", Courier;
				font-size: 12px;
				color: #000000;
				border-right: 1px solid #C0C0C0;
				border-bottom: 1px solid #C0C0C0;
			}
			.checkbox_dx{text-align: center;}
			.content_dx{text-align: left;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSUtils.js?20160617"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSTools.js?20160617"></script> 
		
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		
		<script type="text/javascript">
			$(function(){	
			    var _submitted = 0;//是否已提交	1-是 0-否	 
				var sms = {};  
				sms.extend = $.extend;
				sms.extend({
					/*短信发送*/
					send: function (){
						var SERIALNO = $('#jclsh_duanxin').val();/*接触流水号*/
						var CONTACTID = $('#jcbh_duanxin').val();/*接触编号*/
						var SUBSNUMBER = $('#slbh_duanxin').val();/*接收短信号码*/
						var _qas = sms.getQas();	
						
						if(typeof(SERIALNO) == 'undefined' ||　SERIALNO == ''){
							alert('接触流水号不应为空!');
							return false;
						}
						if(typeof(CONTACTID) == 'undefined' ||　CONTACTID == ''){
							alert('接触编号不应为空!');
							return false;
						}
						if(typeof(SUBSNUMBER) == 'undefined'　||　SUBSNUMBER == ''){
							alert('接收短信号码不应为空!');
							return false;
						}else{
							if(!(/^1[3|4|5|7|8][0-9]\d{4,8}$/.test(SUBSNUMBER))){
								alert('接收短信号码应为手机号码!');
								return false;
							}	
						}
						if(typeof(_qas) == 'undefined' || _qas == ''){
							alert('请选择需要发送的短信信息!');
							return false;						    
						}
						$('#sms_qas').text('['+_qas+']');
						//如果已经提交，停止执行，防止重复提交  modify by heart.cao 2016-10-31
						if(_submitted == 1){
						   return false;
						}										
						if(confirm("确认发送短信？")) {
							_submitted = 1;	
							window.__kbs_layer_index = layer.load('正在发送短信，请稍候...');							
							$('#sms_from').form('submit', {
							    url: '${pageContext.request.contextPath}/app/custom/gzyd/gzyd-mappings!duanxin.htm',  
							    success:function(data){
							        layer.close(window.__kbs_layer_index);	
							        _submitted = 0;
							    	var json = eval("(" + data + ")"); 
							    	var result = json.result;
									var content = json.content;
									if(typeof(result) == "undefined" && content){
										var m = "";
									 	for(var i=0; i<content.length; i++){
									 		var q = content[i].q;
									 		var r = content[i].r;
									 		m += "知识点：'"+q+"',"
									 		if(r==0) m += '短信发送成功!'; 
									 		else if(r==1) m += '短信发送失败!'; 
									 		else if(r==-100) m += '短信发送异常!'; 
									 		else m += '操作异常，请于华为短信接口提供人联系!';
											m += "\n";
								  	 	}
								  	 	
								  	 	if(json.gd_rst == '0'){
								  	 		m += '归档结果：自动归档成功!';
								  	 	} else {
								  	 		m += '归档结果：' + json.gd_msg;
								  	 	}
								  	 	// alert(m);
								  	 	//提示窗设置自动关闭 modify  by heart.cao 2016-10-31
								  	 	layer.msg(m, 1, -1);
								  	 	/**
								  	 	* 发送成功，清空待发列表
								  	 	*/
                                        var _SMSTreePlugin = parent.SMSTreePlugin;								  	 	
								  	 	var trObjs = $('tr',$('#box_dx'));
										for(var i=0; i<trObjs.length; i++){
										    if(trObjs[i]!=undefined && $(trObjs[i]).attr('id')!=undefined
										             && $.trim($(trObjs[i]).attr('id'))!=''){
										        var _qaId = $(trObjs[i]).attr('id');
										        // $('#'+_qaId).remove();	
								                _SMSTreePlugin.unCheck(_qaId); 					             
										    }
										}
										//发送成功，关闭待发列表
										_SMSTreePlugin.closeSMSBox(); 
									}else {
										alert('网络异常，请稍后再试!');
									}
							    }
							});	
						}										
					},
					/*获取需要发送短信的标准问*/
					getQas: function(){
					    var categoryId = '';
						var qa = '';
						if($("#box_dx").length!=0){
							//来自短信待发列表
							var trObjs = $('tr',$('#box_dx'));
							for(var i=0; i<trObjs.length; i++){
							    if(trObjs[i]!=undefined && $(trObjs[i]).attr('id')!=undefined
							             && $.trim($(trObjs[i]).attr('id'))!=''){
							        var qId = $(trObjs[i]).attr('id'); 
							        var q = sms.tagHandle($(trObjs[i]).children().eq(0).text());
								    var a = sms.tagHandle($(trObjs[i]).children().eq(1).text());
								    if(categoryId==''){
								         categoryId = $(trObjs[i]).attr('categoryid');    
								    }
								    qa += '{qid:"'+qId+'",q:"'+q+'",a:"'+a+'"},';						             
							    }
							}
						}
						$("#categoryId").val(categoryId);	
						return qa.replace(/^,|,$/,'');		
					},
					/*
					 * 标准问、标准回复内容处理
					 */
					tagHandle: function(con){
						con = con.replace(/<[^>]+>/g, "");//html标签处理
						con = con.replace(/(^\s*)|(\s*$)/g, "");//去除首尾空白
						con = con.replace(/\"/g, "\\\"");//转义双引号
						return con;
					},
					/*初始化短信发送参数信息*/
					init: function(){
						var ngccObj = SMSUtils.getData();
						if(ngccObj){
							/*获取相关数据*/
							$("#jclsh_duanxin").val(ngccObj.SERIALNO);/*接触流水号*/
							$("#jcbh_duanxin").val(ngccObj.CONTACTID);/*接触编号*/
							$("#slbh_duanxin").val(ngccObj.SUBSNUMBER);/*受理编号*/
							$("#send_duanxin").val("10086");//发送短信号码
							$("#jrsj_duanxin").val(ngccObj.DATATIME);//发送短信号码										         
						}					
					}
				});
				/**
				* 短信发送
				*/
				$('#btnSend').click(function(){
					 sms.send();        
				});	
			    /*初始化数据*/
				sms.init();		
				
				/**
				 * 短信收藏
				 * @auther eko.zhan at 2016-07-29 14:52 短信收藏
				 */
				$('#btnFav').click(function(){
					/*
					var param = [];
					window.__kbs_fav_params = param;
					$('#box_dx').find('tr:gt(2)').each(function(i, item){
						
						param.push({
							id: $(item).attr('id'),
							cateid: $(item).attr('categoryid'),
							question: $(item).find('td:eq(0)').text(),
							answer: $(item).find('td:eq(1)').text()
						});
					});
					
					window.__kbs_fav_params = param;
				
					var url = '${pageContext.request.contextPath}/app/fav/fav-detail.htm?key=SMS';
					openWin(url, '请选择收藏夹', 800, 400);*/
					
					/*
					parent.TABOBJECT.open({
						title: '短信收藏',
						url: '${pageContext.request.contextPath}/app/fav/fav-folder!view.htm?key=SMS'
					});*/
					var url = '${pageContext.request.contextPath}/app/fav/fav-folder!view.htm?key=SMS&type=SMS_PICK';
					//openWin(url, '请选择收藏夹', 800, 400);
					window._open(url, '请选择收藏夹', 800, 400);
				});	
				
				//单个短信收藏
				$('.box_dx').on('click', '.kbs-fav', function(){
					var param = [];
					window.__kbs_fav_params = param;
					
					var $tr = $(this).parent('td').parent('tr');
					
					param.push({
						id: $tr.attr('id'),
						cateid: $tr.attr('categoryid'),
						question: $tr.find('td:eq(0)').text(),
						answer: $tr.find('td:eq(1)').text()
					});
					
					window.__kbs_fav_params = param;
				
					var url = '${pageContext.request.contextPath}/app/fav/fav-detail.htm?key=SMS';
					window._open(url, '请选择收藏夹', 800, 400);
				});
			});
            /**
            * 标准问搜索
            */
			function searchQaList(){
				var title = "标准问搜索列表";
				var url = $.fn.getRootPath()+'/app/sms/sms-ontology!searchForList.htm';

				parent.__kbs_layer_index = $.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: [title, 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src :url},
					area: ['80%', '90%'],
					end: function(){
					     document.getElementById('jclsh_duanxin').focus(); //关闭搜索框后，重新获取短信发送页面焦点
					}
				});


				//openWin(url,name,800,400);	            
			}



			function openWin(url,name,iWidth,iHeight) { 
	            //获得窗口的垂直位置 
	            var iTop = (window.screen.availHeight - 30 - iHeight) / 2; 
	            //获得窗口的水平位置 
	            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2; 
	            window.open(url, name, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no'); 
	        }
			
            /**
            * 删除标准问
            * @param qaId
            */			
			function removeQa(qaId){
			    if($('#'+qaId)){
		            $('#'+qaId).remove();	
		            parent.SMSTreePlugin.unCheck(qaId);			    
			    }
			}


			function openObject(title, url){
				parent.TABOBJECT.open({
					title : title,
					hasClose : true,
					url : url,
					isRefresh : true
				}, this);
			}
		</script>
	</head>

	<body>
	    <form id="sms_from" action="" method="post">
    		<!-- 发送短信号码 -->
			<input type="hidden" name="SENDNUMBER" value="10086" />
			<input type="hidden" id="jcbh_duanxin" name="CONTACTID" />
			<input type="hidden" name="SERVICETYPEID" value="SERVICETYPEID" />
			<input type="hidden" name="SERVICEFULLNAME" value="SERVICEFULLNAME" />
			<!-- 短信分类 -->
			<input type="hidden" id="categoryId" name="categoryId"/>
			<!-- 短信内容 -->
			<textarea rows="1" cols="1" id="sms_qas" name="qas" style="display: none;"></textarea>			
			<table id="box_dx"  class="box_dx" cellspacing="0" width="100%">
				<tr>  
			       <td width="30%"></td>
			       <td width="40%"></td>
			       <td width="22%"></td>
			       <td width="8%"></td>
			    </tr>
			    <tr>
			       <td colspan="2" width="70%" style="align:left;border-right: 0px;" >
				       接触流水号:<input type="text" id="jclsh_duanxin" name="SERIALNO" style="width:120px;"/>
				       接入时间:<input type="text" id="jrsj_duanxin" name="SERIALNO" style="width:120px;" readOnly="readOnly"/>
				       接收短信号码:<input type="text" id="slbh_duanxin" name="SUBSNUMBER" style="width:90px;"/>
				       发送短信号码:<input type="text" id="send_duanxin" name="SERIALNO" style="width:90px;" readOnly="readOnly"/>
			       </td>		    
			       <td align="right" width="30%" colspan="2">
			       		<input id="btnFav" type="button" value="收藏夹">
						<input type="button" value="搜索" onclick="searchQaList();"/>
			       		<input id="btnSend" type="button" value="发送"/>
			       	</td>
			    </tr>
				<tr>
					<th width="30%">
						标题
					</th>
					<th width="70%"  colspan="3">
						内容
					</th>
				</tr>
			</table>	    
	    </form>
	</body>
</html>
