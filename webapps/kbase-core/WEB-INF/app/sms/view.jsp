<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>短信发送</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(function(){
			    var _url = '${pageContext.request.contextPath }/app/sms/sms-ontology!list.htm';
                $("#smsCategoryTree").tree({ 
                     url: _url,
                     onBeforeLoad:function(node,param){
                        var _isLeaf = (node && node.attributes && node.attributes.bh.indexOf('L')!=-1)?true:false;
                        var _hasChildren = (node && node.children==undefined)?true:false;
                        if( _isLeaf && _hasChildren){                            
                              $.post( _url,{ 
	                                 'id': node.id,
	                                 'bh': node.attributes.bh 
	                                 }, 
	                                 function(json) { 
	                                    if(json!=undefined&&json!=null){
	                                 	    $('#smsCategoryTree').tree('append', { 
			                                    parent : node.target, 
			                                    data : json 
			                                });  
			                                $('#smsCategoryTree').tree('collapse',node.target)  
			                                $('#smsCategoryTree').tree('expand',node.target)     
	                                     }
	                                 }, "json");     
	                          return false;                
                        }
	                 },
	                 onCheck:function(){
	                 
	                 }       
                });   
			});
		</script>
	</head>

	<body>

	     <div class="easyui-panel" style="width:100%;height:700px;padding:1px;">
			<div class="easyui-layout" data-options="fit:true">
				<div data-options="region:'west',split:true" style="width:20%;padding:3px">
				    <ul id="smsCategoryTree" class="easyui-tree tree" checkbox="true"  onlyLeafCheck="true"
				    style="height: 100%; width: 100%; overflow: auto;" ></ul>
				</div>
				<div data-options="region:'center'" style="width:80%;padding:3px">
				    <table id="smsQAGrid" class="easyui-datagrid" style="width:100%;height:500px;" >
				    	<thead>
							<tr>
								<th field="name1" width="3%"></th>
								<th field="name2" width="27%">标准问</th>
								<th field="name3" width="70%">标准答</th>
							</tr>                          
						</thead> 
				    </table>
				</div>
			</div>
		</div>
	</body>
</html>
