<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>脑图-编辑</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jsmind/jsmind.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/colorpicker/spectrum.css">
		
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.kbs-input{
				border: 1px solid #cdcdcd;
				width: 150px;
			}
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/jsmind.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum-zh-cn.js"></script>
		
		<script type="text/javascript">
			$(function(){
				
				//加载数据
				var mind = ${mindmap.minddata};
		        var options = {
		            container: 'panel',
		            editable: true,
		            theme: 'primary'
		        }
		        var _jm = jsMind.show(options, mind);
		        
		        //树加载
				$('#cateTree').tree({
				    url: '${pageContext.request.contextPath}/app/main/ontology-category!list.htm?type=jeasyuitree',
				    onDblClick: function(node){
				    	//双击节点
				    	var checked = _jm.get_selected_node();
			        	if (checked==null){
			        		alert('请选择主题节点');
			        		return false;
			        	}
			        	
			        	var isOnto = checked.data && checked.data.isonto?checked.data.isonto:false;
			        	if (isOnto){
			        		alert('请选择主题节点');
			        		return false;
			        	}
			        	
			        	var hasNode = _jm.get_node(node.id)!=null;
			        	if (hasNode){
			        		alert('脑图中已经存在 [' + node.text + '] , 不可重复插入');
			        		return false;
			        	}
			        	
			        	var iscate = false;
			        	var url = '${pageContext.request.contextPath}/app/object/ontology-object.htm?searchMode=1&isCate=false&id=' + node.id;
						if (node.isParent){
							iscate = true;
							if ('${enableCateTag}'=='true'){
								url = '${pageContext.request.contextPath}/app/category/category.htm?categoryId=' + node.id;
							}else{
								url = '${pageContext.request.contextPath}/app/object/ontology-object.htm?searchMode=1&isCate=true&id=' + node.id;
							}
						}
						
						var data = {'src': url, 'isonto': true, 'iscate': iscate};
		        		_jm.add_node(checked, node.id, node.text, data);
				    }
				});
		        
		        
		        //刷新
		        $('#btnRefresh').on('click', function(){
		        	location.reload();
		        });
		        //插入节点
		        $('#btnAdd').on('click', function(){
		        	var checked = _jm.get_selected_node();
		        	if (checked==null){
		        		alert('请选择主题节点');
		        		return false;
		        	}
		        	
		        	var isOnto = checked.data && checked.data.isonto?checked.data.isonto:false;
		        	if (isOnto){
		        		alert('请选择主题节点');
		        		return false;
		        	}
		        	
		        	//console.log(checked);
		        	
		        	var nodeid = jsMind.util.uuid.newid();
			        var node = _jm.add_node(checked, nodeid, '分支主题');
		        });
		        
		        //删除节点
		        $('#btnDel').on('click', function(){
		        	var checked = _jm.get_selected_node();
		        	if (checked==null){
		        		alert('请选择节点');
		        		return false;
		        	}
		        	
		        	if (checked.id=='root'){
		        		alert('无法删除根节点');
		        		return false;
		        	}
		        	
		        	_jm.remove_node(checked.id);
		        	
		        });
		        
		        //保存脑图
		        $('#btnSave').on('click', function(){
		        	var rootNode = _jm.get_root();
		        	
		        	var _data = _jm.get_data();
					var _param = {
						'mindmap.id': '${mindmap.id}',
						'mindmap.title': rootNode.topic,
						'mindmap.user_id': '${mindmap.user_id}',
						//'mindmap.create_time': '${mindmap.create_time}',
						'mindmap.minddata': jsMind.util.json.json2string(_data)
					}
					
					window.__kbs_layer_index = layer.load('请稍候...');
					$.post('${pageContext.request.contextPath}/app/mindmap/mindmap!save.htm', _param, function(data){
						layer.close(window.__kbs_layer_index);
						alert(data.message);
						if ('${mindmap.id}'==''){
							//新增
							location.href = '${pageContext.request.contextPath}/app/mindmap/mindmap!edit.htm?id=' + data.data;
						}
					}, 'json');
					
		        });
		        
		        
		        $("#colorpicker").spectrum({color: "#f00"});
		        $("#colorpicker").on("hide.spectrum", function(e, color) {
		        	var checked = _jm.get_selected_node();
					if(checked){
						$('#' + checked.id).css('background', color.toHexString()); 
						_jm.update_node(checked.id, checked.topic, color.toHexString());
					}
				});
			});
		</script>
	</head>

	<body>
		<div class="easyui-layout" style="width:100%;height:500px;">
	        <div data-options="region:'west',split:true" title="知识分类树" style="width:200px;">
	        	<ul id="cateTree"></ul>
	        </div>
	        <div data-options="region:'center'">
	            <div style="margin:10px;">
	            	<a href="javascript:void(0)" class="easyui-linkbutton" id="btnRefresh">刷新</a>
			        <a href="javascript:void(0)" class="easyui-linkbutton" id="btnAdd">插入节点</a>
			        <a href="javascript:void(0)" class="easyui-linkbutton" id="btnDel">删除节点</a>
			        <a href="javascript:void(0)" class="easyui-linkbutton" id="btnSave">保存</a>
			        &nbsp;<input id='colorpicker' />
			    </div>
			    <div id="panel" style="height:440px;border:1px solid #ccc;margin-left:10px;margin-right:10px;"></div>
	        </div>
	    </div>
	</body>
</html>
