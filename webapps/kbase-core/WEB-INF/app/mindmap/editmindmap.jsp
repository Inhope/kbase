<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <base href="<%=basePath%>">
    
    <title></title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<c:choose>
		<c:when test="${not empty system_shortcut_icon}">
			<link href="${pageContext.request.contextPath}/app/auther/system!getImg.htm?imgFieldName=${system_shortcut_icon }" type="image/x-icon" rel="shortcut icon">
		</c:when>
		<c:otherwise>
			<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/logo.ico" type="image/x-icon" rel="shortcut icon">
		</c:otherwise>
	</c:choose>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jsmind/jsmind.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/colorpicker/spectrum.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/leftTree.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/jsmind.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/jquery.colorpicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum-zh-cn.js"></script>
	<script type="text/javascript">
			if(!window.USER_THEME)
				window.USER_THEME = '${sessionScope.session_user_key.userInfo.userTheme}';
		</script>
		<script type="text/javascript">
			var categoryTag = '${categoryTag}';
			window.__IS_KBASE = 1;
		</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
	    <style type="text/css">
        li{margin-top:2px; margin-bottom:2px;}
        button{width:140px;}
        select{width:140px;}
        #layout{width:1230px;}
        #jsmind_nav{width:210px;height:600px;border:solid 1px #ccc;overflow:auto;float:left;}
        .file_input{width:100px;}
        button.sub{width:100px;}

        #jsmind_container{
            float:left;
            width:1000px;
            height:600px;
            border:solid 1px #ccc;
        }
        #jsmind_panel{
            height:560px;
            border:solid 1px #ccc;
            background:#f4f4f4;
        }
        #jsmind_edit{
            height:40px;
            border:solid 1px #ccc;
            background:white;
        }
    </style>
	<script type="text/javascript">
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		var setting = {
			view: {
					expandSpeed: "fast"//空不显示动画
			},
			async : {
				enable : true,
				url : "app/main/ontology-category!list.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam : {},
				dataFilter: ajaxDataFilter
			},
			callback : {
				onClick : onClick
			}
			
		};
		
		function ajaxDataFilter(treeId, parentNode, responseData) {
		    if (responseData) {
		      for(var i =0; i < responseData.length; i++) {
		      	if(responseData[i].isParent=="false")
		        	responseData[i].icon = 'theme/' + window.USER_THEME + '/resource/main/images/left_ico5.jpg';
		      }
		    }
		    return responseData;
		};
		
		function onClick(event, treeId, treeNode, clickFlag) {
		    var selected_node = _jm.get_selected_node(); // as parent of new node
	        
	        var url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id='+treeNode.id;
			if(!treeNode.isParent && categoryTag=='true'){
				url = $.fn.getRootPath() + '/app/category/category.htm?categoryId='+treeNode.id;
			}
			if(!selected_node){
			  alert("请选择一个结点");
			}else{
		        var data={"src":url};
		        _jm.add_node(selected_node, treeNode.id, treeNode.name,data);
	        }
		}
		
		window.__IS_KBASE = 1;
		function open_ajax(){
        var mind_url = 'data_example.json';
        jsMind.util.ajax.get(mind_url,function(mind){
            _jm.show(mind);
        });
       }

    function show_data(){
        var str=prompt("请输入脑图标题","");
	    if(str)
	    {
	       var mind_data = _jm.get_data();
	       var mind_string = jsMind.util.json.json2string(mind_data);
	       $.ajax({
	   		type: "POST",
	   		url : "app/mindmap/mindmap!savemindmap.htm",
	   		data:{title:str,minddata:mind_string,id:"${mindmap.id}"},
	   		async: false,
	   		dataType : 'json',
	   		success: function(msg){
	   			alert("保存成功");
	   			window.location.href=$.fn.getRootPath()+"/app/mindmap/mindmap.htm";
	   		}
	   　}); 
	    }
    }


    function add_node(){
        var selected_node = _jm.get_selected_node(); // as parent of new node
        if(!selected_node){
          alert("请选择一个结点");
        }else{
        var nodeid = jsMind.util.uuid.newid();
        var topic = '分支主题';
        var node = _jm.add_node(selected_node, nodeid, topic);
        }
        
    }


    function remove_node(){
        if(!_jm.get_selected_node()){
           alert("请选择一个结点");
        }
        if(_jm.get_selected_node().id=='root'){
             alert("不能删除主结点");  
        }
        else{
	        var selected_id = _jm.get_selected_node().id;
	        _jm.remove_node(selected_id);
        }
    }

    
	</script>
  </head>
  
  <body>
	  	<div id="layout">
	    <div id="jsmind_nav">
	        <div class="zTreeDemoBackground left">
				<ul id="treeDemo" class="ztree"></ul>
			</div>
	    </div>
	    
	    <div id="jsmind_container">
	       <div id="jsmind_edit">
	          <div style="float:left;margin:10px;"><span onclick="add_node();">插入下级主题</span></div>
	          <div style="float:left;margin:10px;"><span >编辑</span></div>
	          <div style="float:left;margin:10px;"><span onclick="remove_node();">删除</span></div>
	          <div style="float:left;margin:10px;"><span >主题颜色:<input type='color' name='color' /></span></div>
	          <div style="position:absolute;right:60px;top:20px"><span onclick="show_data();">保存</span></div>
	          <div style="position:absolute;right:10px;top:20px"><span onclick="javascript:history.go(-1);">返回</span></div>
	       </div>
	     <div id="jsmind_panel"></div>
	   </div>
	</div>
     <script type="text/javascript">
		var _jm = null;
		jsMind.prototype.dblclick_handle = function(e){
			if(this.get_editable()){
	            var element = e.target || event.srcElement;
	            var isnode = this.view.is_node(element);
	            if(isnode){
	                var nodeid = this.view.get_nodeid(element);
	                var node = this.mind.get_node(nodeid);
	                if (node.data && node.data.src){
	                	//window.open(node.data.src)
	                	parent.TABOBJECT.open({
							id : Math.random(),
							name : node.topic,
							title : node.topic,
							hasClose : true,
							url : node.data.src,
							isRefresh : true
						}, this);
	                }else{
	                	this.begin_edit(nodeid);
	                }
	            }
        	}
   		}
   	
    function load_jsmind(){
        var mind = ${mindmap.minddata};
        var options = {
            container:'jsmind_panel',
            editable:true,
            theme:'primary'
        }
        _jm = jsMind.show(options,mind);
        // jm.set_readonly(true);
        // var mind_data = jm.get_data();
        // alert(mind_data);
    }
    
    load_jsmind();
    
    //子节点收缩
    var nodes=_jm.mind.nodes
    for(var nodeid in nodes){
        _jm.toggle_node(nodes[nodeid].id);
    }
    
    $("input[name='color']").spectrum();
		$("input[name='color']").on("hide.spectrum", function(e, color) {
		if(_jm.get_selected_node()){
		    var selected_node = _jm.get_selected_node();
			$("#"+selected_node.id+"").css("background", color.toHexString()); 
			_jm.update_node(selected_node.id, selected_node.topic,color.toHexString());
		}
	});
    
</script>

  </body>
</html>
