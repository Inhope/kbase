<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <base href="<%=basePath%>">
    
    <title> starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<c:choose>
		<c:when test="${not empty system_shortcut_icon}">
			<link href="${pageContext.request.contextPath}/app/auther/system!getImg.htm?imgFieldName=${system_shortcut_icon }" type="image/x-icon" rel="shortcut icon">
		</c:when>
		<c:otherwise>
			<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/logo.ico" type="image/x-icon" rel="shortcut icon">
		</c:otherwise>
	</c:choose>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jsmind/jsmind.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/css/leftTree.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/jsmind.js"></script>
	<script type="text/javascript">
			if(!window.USER_THEME)
				window.USER_THEME = '${sessionScope.session_user_key.userInfo.userTheme}';
		</script>
		<script type="text/javascript">
			var categoryTag = '${categoryTag}';
			window.__IS_KBASE = 1;
		</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
	    <style type="text/css">
        li{margin-top:2px; margin-bottom:2px;list-style:none;color:#666;}
        button{width:140px;}
        select{width:140px;}
        #layout{width:100%;}
        #jsmind_nav{width:20%;height:700px;border:solid 1px #ccc;overflow:auto;float:left;}
        .file_input{width:100px;}
        button.sub{width:100px;}

        #jsmind_container{
            float:left;
            width:1000px;
            height:600px;
            border:solid 1px #ccc;
        }
    </style>
	
  </head>
  
  <body>
	 <div id="layout">
	    <div id="jsmind_nav">
	         <ul style="display: block;line-height:30px">
	           <li onclick="showmind();">我的脑图</li>
	             <div id="mymindmap" style="display: none" >
		              <c:forEach items="${mindmaps}" var="mindmap">
		                 <li><a href="app/mindmap/mindmap!findmindmap.htm?id=${mindmap.id}">${mindmap.title}</a></li>
		              </c:forEach>
	             </div>
	         </ul>
	    </div>
	    <div id="jsmind_container"></div>
	</div>
    <script type="text/javascript">
      $(function(){ 
         
         jsMind.prototype.dblclick_handle = function(e){
			if(this.get_editable()){
	            var element = e.target || event.srcElement;
	            var isnode = this.view.is_node(element);
	            if(isnode){
	                var nodeid = this.view.get_nodeid(element);
	                var node = this.mind.get_node(nodeid);
	                if (node.data.src!=undefined){
	                	//window.open(node.data.src)
	                	parent.TABOBJECT.open({
							id : Math.random(),
							name : node.topic,
							title : node.topic,
							hasClose : true,
							url : node.data.src,
							isRefresh : true
						}, this);
	                }else{
	                	//this.begin_edit(nodeid);
	                }
	            }
        	}
   		};
   		
        $.ajax({
	   		type: "POST",
	   		url :"app/mindmap/mindmap!getmindmaps.htm",
	   		async: false,
	   		dataType : 'json',
	   		success: function(data){
	   		    $("#jsmind_container").css({"width":"79.5%","height":"700px"});
	   			for(var i=0;i<data.length;i++){
			        var mind=eval("("+data[i].minddata+")");
			        var options = {
			            container:'jsmind_container',
			            editable:true,
			            theme:'primary'
			        }
			        jsMind.show(options,mind);
	   			}
	   		}
	   　});
      });  
      
      function  showmind(){
           $("#mymindmap").toggle();
      } 
</script>

  </body>
</html>
