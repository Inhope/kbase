<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>脑图</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.kbs-input{
				border: 1px solid #cdcdcd;
				width: 150px;
			}
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//grid 双击行
				$('#grid').datagrid({
					onDblClickRow: function(index, row){
						var id = row.id;
						var title = row.title;
						var _url = '${pageContext.request.contextPath}/app/mindmap/mindmap!read.htm?id=' + id;
						parent.TABOBJECT.open({	 
							id : id,
							name : title,
							title : title,
							hasClose : true,
							url : _url,
							isRefresh : true
						}, this);
					},
					onLoadSuccess: function(data){
					/* 写着玩的代码
						console.log(data);
						var ids = [];
						$(data.rows).each(function(i, item){
							ids.push(item.id);
						});
						alert(ids.join(','));*/
					}
				});
				//新增
				$('a[name="btnNew"]').on('click', function(){
					parent.TABOBJECT.open({
						id: 'mindmap',
						name: '新增脑图',
						hasClose : true,
						isRefresh : true,
						url: '${pageContext.request.contextPath}/app/mindmap/mindmap!edit.htm'
					});
				});
				//删除
				$('a[name="btnDel"]').on('click', function(){
					var row = $('#grid').datagrid('getSelected');
					if (row!=null){
						if (window.confirm('确定删除吗')){
							$.post('${pageContext.request.contextPath}/app/mindmap/mindmap!delete.htm', {'id': row.id}, function(data){
								var rowInd = $('#grid').datagrid('getRowIndex', row);
								$('#grid').datagrid('deleteRow', rowInd);
							}, 'json');
						}
					}
				});
				//刷新
				$('a[name="btnRefresh"]').on('click', function(){
					$('#grid').datagrid('reload'); 
				});
			});
		</script>
	</head>

	<body>
		<div id="tb-inbox" style="padding:5px;display:none;">
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnRefresh">刷新</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnNew">新建</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" name="btnDel">删除</a>
		</div>
		<table id="grid" style="overflow:hidden;width:100%;height:500px;"
	        url="${pageContext.request.contextPath }/app/mindmap/mindmap!load.htm"
	        rownumbers="true" pagination="true"
	        data-options="toolbar:'#tb-inbox', singleSelect:true">
			<thead>
				<tr>
					<th field="title" width="70%">标题</th>
					<th field="createtime" width="25%">创建时间</th>
				</tr>
			</thead>
		</table>
	</body>
</html>
