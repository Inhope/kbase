<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>脑图-只读</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jsmind/jsmind.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/colorpicker/spectrum.css">
		
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			jmnode{
				cursor: pointer;
			}
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/excanvas.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jsmind/jsmind.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/colorpicker/spectrum-zh-cn.js"></script>
		
		<script type="text/javascript">
			$(function(){
				
				//加载数据
				var mind = ${mindmap.minddata};
		        var options = {
		            container: 'panel',
		            editable: false,
		            theme: 'primary'
		        }
		        var _jm = jsMind.show(options, mind);
		        
		        //编辑节点
		        $('#btnEdit').on('click', function(){
		        	location.href = '${pageContext.request.contextPath}/app/mindmap/mindmap!edit.htm?id=${mindmap.id}';
		        	
		        });
		        
		        $('jmnode').on('dblclick', function(){
		        	var checked = _jm.get_selected_node();
		        	var isOnto = checked.data && checked.data.isonto?checked.data.isonto:false;
		        	if (isOnto){
		        		//alert(checked.data.src);
		        		parent.TABOBJECT.open({
							id: checked.id,
							name: checked.topic,
							hasClose : true,
							isRefresh : true,
							url: checked.data.src
						});
		        	}
		        });
			});
		</script>
	</head>

	<body>
		 <div style="margin:10px;">
           	<a href="javascript:void(0)" class="easyui-linkbutton" id="btnEdit">编辑</a>
	    </div>
	    <div id="panel" style="height:440px;border:1px solid #ccc;margin-left:10px;margin-right:10px;"></div>
	</body>
</html>
