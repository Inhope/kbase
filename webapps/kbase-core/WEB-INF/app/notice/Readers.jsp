<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.eastrobot.domain.UserNotice"%>
<%@page import="com.eastrobot.domain.User"%>
<%@page import="com.eastrobot.domain.UserInfo"%>
<%@page import="com.eastrobot.domain.Station"%>
<%@page import="com.eastrobot.domain.Dept"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_con{margin:0px 0px 0;}
			.gonggao_titile_right a{margin: 5px;}
	  		td a{cursor: pointer;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script language="javaScript1.2">
		     $(function(){
		          //分页
		          fenye();
		     });
		     /**
		     * 分页
		     */
			 function fenye(){
				var btns = $('div.gonggao_con_nr_fenye a');
				var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
				var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
				var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
				var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
				btns.each(function(index){
					var btn= $(this);
					if(btn.html().indexOf('上一页') > -1 ){
						if( currentPage > 1){
							btn.click(function(){
								var prev = parseInt(start)-parseInt(limit);
								fenyeSubmit(prev);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('下一页') > -1 ){
						if(currentPage < totalPage){
							btn.click(function(){
								var next = parseInt(start)+parseInt(limit);
								fenyeSubmit(next);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('首页') > -1 ){
						if(currentPage > 1){
							btn.click(function(){
								fenyeSubmit(0);
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else if(btn.html().indexOf('尾页') > -1 ){
						if(currentPage < totalPage){
							btn.click(function(){
								var last = parseInt(limit)*(parseInt(totalPage)-1);
								fenyeSubmit(last);	
							});
						}else{
							btn.css('text-decoration','none').css('cursor','default');
						}
					}else {
						var text = $(this).html();
						if(text != currentPage){
							var click = parseInt(limit)*(parseInt(text)-1);
							btn.click(function(){
								fenyeSubmit(click);
							});
						}
					}
					
				});
				function fenyeSubmit(start){
					$('#ReadersForm input[name="start"]').val(start);
					$('#ReadersForm').submit();
				}
			}		     
		</script>
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
			    <form id="ReadersForm" method="post" action="${pageContext.request.contextPath}/app/notice/notice!readers.htm?id=${id}&type=${type}">
					<input type="hidden" name="start"/>		
				</form>		
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">			
					<tr>
						<td width="4%"></td>
						<td width="32%"></td>
						<td width="32%"></td>
						<td width="32%"></td>
					</tr>
					<tr class="tdbg">
						<td></td>
						<td>姓名</td>
						<td>部门</td>
						<td>阅读时间</td>
					</tr>
					<%
					   List receive = (List)request.getAttribute("receive"); 
					   if(receive!=null&&receive.size()>0){
					      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					      for(int i=0;i<receive.size(); i++){
					         UserNotice un = (UserNotice)receive.get(i);
					         if(un!=null && un.getUser() != null){
								out.println("<tr><td></td>");
								out.println("<td>"+un.getUser().getUserInfo().getUserChineseName()+"</td>");
								if (un.getUser().getMainStation() != null) {
									out.println("<td>"+un.getUser().getMainStation().getDept().getName()+"</td>");
								} else {
									out.println("<td>--</td>");
								}
								if (un.getReadTime() != null) {
									out.println("<td>"+df.format(un.getReadTime())+"</td>");		
								} else {
									out.println("<td>--</td>");	
								}							
								out.println("</tr>");	            
					         }
					      }
					   }
					   
					System.out.println("==================3");
					%>
					<s:if test="totalPage>1">
						<tr>
							<td colspan="7">
								<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
									<a href="javaScript:void(0);" onfocus="this.blur();">首页</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">&lt;上一页</a>
									<s:iterator value="new int[totalPage]" status="i">
										<s:if test="#i.index+1 == currentPage"> 
											<a href="javaScript:void(0);" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
										</s:if>
										<s:elseif test="#i.index+4 > currentPage && #i.index-4 < currentPage" >
											<a href="javaScript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
										</s:elseif>
									</s:iterator>
									<a href="javaScript:void(0);" onfocus="this.blur();">下一页&gt;</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">尾页</a>
								</div>
							</td>
						</tr>
					</s:if>					
				</table>
			</div>
		</div>
	</body>
</html>

