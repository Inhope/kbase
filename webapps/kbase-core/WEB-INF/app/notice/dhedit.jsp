<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>新增公告</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Object.js"></script>
		
		<script type="text/javascript">
			
		
			$(function(){
				$.fn.zTree.init($("#noticeTree"), setting);
				
				$('#depSelect').click(function(){
					$.kbase.picker.noticedeptsearch({returnField: 'depSelect|deptscope'});
				});
				
				
				$('#btnSave').click(function(){
					var deptscope = $('#deptscope').val();
					var deptscopename = $('#depSelect').val();
					var title = $('#title').val();
					var content = $('#content').val();
					var objects = $('#objects').val();
					if(objects == '多个实例之间请用分号分格')
						objects = '';
					var type1 = $('#type1');
					var type2 = $('#type2');
					var type3 = $('#type3');
					
					var type = 0;
					
					if(type1.attr('checked')=='checked'){
						type += 1;
					}
					if(type2.attr('checked')=='checked'){
						type += 2;
						
					}
					
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					var tipNotice = $('#tipNotice');
					
					var statistics = 0;
					var top = 0;
					if($('#statistics').attr('checked')=='checked'){
						statistics = 1;
					}
					if($('#top').attr('checked')=='checked'){
						top = 1;
					}
					
					if( title==null || $.trim(title)=='' ){
						tipNotice.html('* 标题不能为空');
					}else if( content==null || $.trim(content)=='' ){
						tipNotice.html('* 内容不能为空');
					}else if(deptscope==null || $.trim(deptscope)=='' ){
						tipNotice.html('* 发布范围不能为空');
					}else if( startTime==null || $.trim(startTime)=='' || endTime==null || $.trim(endTime)==''){
						tipNotice.html('* 公告周期不能为空');
					}/*else if(testDate(startTime,endTime)){
						tipNotice.html('* 开始时间不能晚于结束时间');
					}*/else{
						//$('body').hideShade();
						window.__kbs_layer_index = layer.load('请稍候...');
						
						//验证实例
						var flag = false;
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/notice/notice!testObject.htm",
					   		data: {
					   			objects : objects
					   		},
					   		async: false,
					   		success: function(msg){
					   			if(msg=='"ok"'){
					   				flag = true;
					   			}else{
					   				tipNotice.html(msg.replace("\"","").replace("\"",""));
					   			}
					   		},
					   		error: function(){
					   		}
						});
						if(!flag){
							$('body').ajaxLoadEnd();
							//$('body').showShade();
							return;
						}
						//$('div.gonggao_d').hide();
						//$('body').hideShade();
						
						tipNotice.html('');
						
						//提交数据
						
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/custom/dh/dh-notice!add.htm",
					   		data: {
					   			depScopename:deptscopename,
					   			title : title,
					   			content : content,
					   			objects : objects,
					   			type : type,
					   			top : top,
					   			startTime : startTime,
					   			endTime : endTime,
					   			statistics : statistics,
					   			depScope:deptscope
					   		},
					   		async: true,
					   		success: function(msg){
					   			parent.layer.alert('发布成功',-1);
					   			var _iframe = $(parent.document).find('div.content_content').find('iframe:visible')
								_iframe.attr('src', _iframe.attr('src'));
					   			parent.layer.close(parent.__kbs_layer_index);
					   		},
					   		error: function(){
					   			parent.parent.$(document).hint("发布失败");
					   			$('body').ajaxLoadEnd();
					   		}
						});
						
					} //end if-else
				});
				
				/******************提示框*******************/
				if($('#objects')[0]){
					getObject.init($('#objects')[0]);
				}
				
			});
		</script>
		
	</head>

	<body>
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="20%">标题</td>
				<td width="80%"><input id="title" type="text" style="width:92%;" /></td>
			</tr>
			<tr>
				<td>内容</td>
				<td><textarea id="content" rows="5" style="width:92%;"></textarea></td>
			</tr>
			<tr>
				<td>加载知识</td>
				<td>
					<input id="objects" type="text" style="width:92%;"/>
					<br><span style="color:red;">多个实例之间请用西文分号分隔</span>
				</td>
			</tr>
			<tr>
				<td>发布范围</td>
				<td>
					<input id="depSelect" type="text" readonly="readonly" style="width:92%;"/>
					<input id ="deptscope" type ="hidden" />
					<!-- 
					<div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #C0C0C0; _position:absolute;">
						<a href="javaScript:void(0);" style="margin-left:260px;" id="btnDeptOk">[确定]</a>
						<a href="javaScript:void(0);" id="btnDeptCancel">[关闭]</a>
						<ul id="noticeTree" class="ztree" style="height:150px;overflow:auto;"></ul>
					</div>
					-->
				</td>
			</tr>
			<tr>
				<td>显示类型</td>
				<td>
					<input type="checkbox" id="type1" name="type" value="1" />跑马灯
					<input type="checkbox" id="type2" name="type" value="2" />弹屏
					 <input type="checkbox" id="type3" name="type" value="3" checked="checked" disabled="disabled"/>公告栏
				</td>
			</tr>
			<tr>
				<td>公告周期</td>
				<td>
					<input type="text" id="startTime" readonly="readonly" style="width:120px;" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd'})"/> ～ 
					<input type="text" id="endTime" readonly="readonly" style="width:120px;" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd'})"/>
				</td>
			</tr>
			<tr>
				<td>是否置顶</td>
				<td><input type="checkbox" id="top" value="1" />置顶</td>
			</tr>
			<tr>
				<td align="right" colspan="2">
					<span id="tipNotice" style="color:red;"></span>&nbsp;
					<input type="button" value="保存" id="btnSave">
				</td>
			</tr>
		</table>
		
		
	</body>
</html>
