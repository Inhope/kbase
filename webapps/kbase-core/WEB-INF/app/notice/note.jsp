<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String _toTopCount = (String)session.getAttribute("to_top_count");//用户最多可置顶公告数量
String _topCount = (String)request.getAttribute("top_count");//当前用户已置顶公告数量
String _categoryTag = (String)session.getAttribute("category_tag");//是否为分类展示，"1"-是 "0"-否，如果是，选择关联知识时，关联到分类的叶子节点 针对广东移动
String _objectsNotice = "多个实例之间请用西文分号分隔";
if("1".equals(_categoryTag))_objectsNotice = "多个分类之间请用西文分号分隔";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>新增公告</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Object.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Category.js"></script>
				<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			var _toTopCount = "<%=_toTopCount%>"; //用户最多可置顶便签数量
			var _topCount = "<%=_topCount%>"; //当前用户已置顶便签数量
			var _categoryTag = "<%=_categoryTag%>"
		
			$(function(){
			
				$('#btnSave').click(function(){
					var title = $('#title').val();
					var content = editor.html(); //$('#content').val();
					/*
					var objects = $('#objects').val();
					if(objects == '多个实例之间请用分号分格' 
					|| objects == '多个分类之间请用分号分格')
						objects = '';
					//针对广东移动，获取分类
					var categorys = "";
					if(_categoryTag=="1")categorys = getCategory.checkIds();	
					*/
					//便签知识关联调整为对话框选择  modify by heart.cao 2016-05-04
					var objects = $('#kbValueName').val();
					var categorys = $('#kbValueId').val();
					objects = (objects!=undefined && objects!='')?objects.replace(new RegExp(',','gm'),';'):'';
					categorys = (categorys!=undefined && categorys!='')?categorys.replace(new RegExp(',','gm'),';'):'';
						
					var type1 = $('#type1');
					var type2 = $('#type2');
					var type3 = $('#type3');
					
					var type = 0;
					
					if(type1.attr('checked')=='checked'){
						type += 1;
					}
					if(type2.attr('checked')=='checked'){
						type += 2;					
					}
					
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					var tipNotice = $('#tipNotice');
					
					var statistics = 0;
					var top = 0;
					if($('#statistics').attr('checked')=='checked'){
						statistics = 1;
					}

					//发布范围-接收人
					var _users = $("#users").val();	
					if(_users == undefined || _users==null || $.trim(_users) == '多个接收人之间请用分号分格')_users = '';
					
					//发布范围-岗位
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
					var depStr="";
					//modify by eko.zhan at 2015-06-16 部门岗位采用异步加载，如果未完成加载当管理员选择父级部门，不会默认选中岗位
					var _stationIds = ''
					if(nodes.length>0){
						for(var i=0; i<nodes.length; i++){
							depStr+=nodes[i].id+",";
							if(!nodes[i].isParent){
								if(_stationIds == ''){
									_stationIds += nodes[i].id;
								}else{
									_stationIds += ','+nodes[i].id;
								}
							}
						}					
					}
					//去掉逗号
					if (depStr.length>0){
						depStr = depStr.substring(0, depStr.length-1);
					}
					
					//附件
					var fileName = $('#fileName').val();
					
					if( title==null || $.trim(title)=='' ){
						tipNotice.html('* 标题不能为空');
					}else if( content==null || $.trim(content)=='' ){
						tipNotice.html('* 内容不能为空');
					}else if($.trim(depStr) != '' && $.trim(_stationIds) == ''){
						tipNotice.html('* 发布范围请勾选岗位');
					}else if($.trim(_users) == '' && (nodes.length==0 || $.trim(_stationIds) == '')){
						tipNotice.html('* 接收人/发布范围不能都为空');
					}else if( startTime==null || $.trim(startTime)=='' || endTime==null || $.trim(endTime)==''){
						tipNotice.html('* 有效周期不能为空');
					}else{
						//$('body').ajaxLoading('正在提交数据...');
						//调整为layer遮罩层  modify by heart 2016-05-13
						window.__kbs_layer_index = layer.load('请稍候...');
						
						//验证实例
						var flag = false;
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/notice/notice!testObject.htm",
					   		data: {
					   			objects : objects,
					   			categorys : categorys
					   		},
					   		async: false,
					   		success: function(msg){
					   			if(msg=='"ok"'){
					   				flag = true;
					   			}else{
					   				tipNotice.html(msg.replace("\"","").replace("\"",""));
					   			}
					   		},
					   		error: function(){
					   		}
						});
						
						if(!flag){
							//$('body').ajaxLoadEnd();
							//return;
							layer.close(window.__kbs_layer_index);
							return false;
						}

						tipNotice.html('');
						
						//提交数据
						//$('body').ajaxLoading('正在提交数据...');
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/notice/notice!add.htm",
					   		data: {
					   			title : title,
					   			content : content,
					   			objects : objects,
					   			categorys : categorys,
					   			type : type,
					   			top : top,
					   			startTime : startTime,
					   			endTime : endTime,
					   			statistics : statistics,
					   			users : $.trim(_users),//接收人
					   			stationsId : $.trim(_stationIds),//接收岗位
					   			depScope:$.trim(depStr),
					   			isNote : '1', //便签
					   			fileName : fileName //附件
					   		},
					   		async: true,
					   		success: function(msg){
					   			//parent.parent.$(document).hint(msg.replace("\"","").replace("\"",""));
					   			//$('body').ajaxLoadEnd();
					   			//reNotice();
					   			//window.location.reload();
					   			//parent.location.reload();
					   			layer.close(window.__kbs_layer_index);
					   			alert('发布成功');
					   			var _iframe = $(parent.document).find('div.content_content').find('iframe:visible')
								_iframe.attr('src', _iframe.attr('src'));
								parent.layer.close(parent.__kbs_layer_index);
					   		},
					   		error: function(){
					   			//parent.parent.$(document).hint("发布失败");
					   			//$('body').ajaxLoadEnd();
					   			layer.close(window.__kbs_layer_index);
					   			alert('发布失败');
					   		}
						});
						
					} //end if-else
				});	
				
				initFormInfo(); //初始化表单内容		
				
			});
			/**
			* 初始化表单内容
			*/
			function initFormInfo(){
				/////////////////////////////////////////////////
				///////////////接收人(发布范围)////////////////////
				$('#users').val('多个接收人之间请用分号分格');
			    $('#users').css('color','#cccccc');
				$('#users').blur(function() {
					if($(this).val() == ''){
						$(this).val('多个接收人之间请用分号分格');
						$(this).css('color','#cccccc');
					}
					return false;
				});
				$('#users').focus(function() {
					if($(this).val() == '多个接收人之间请用分号分格'){
						$(this).val('');
						$(this).css('color','');
					}
					return false;
				});
				/////////////////////////////////////////////////
				
				/////////////////////////////////////////////////
				///////////////发布范围(发布范围)////////////////////			
				$.fn.zTree.init($("#noticeTree"), setting);
				$('#depSelect').click(function(){
					var _offset = $(this).offset();
					var _scrollTop = $(document).scrollTop()
					$(this).next().css({left:_offset.left + "px", top:_offset.top - _scrollTop + $(this).outerHeight() + "px"}).slideDown("fast");
				});
				
				$('#btnDeptOk').click(function(){
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);		 
					//moidfy by eko.zhan at 2015-06-05 选择1个节点就为已选择
					if(nodes.length>0){
						$('#depSelect').val('已选择');
					}else{
						$('#depSelect').val('请选择');
					}
					$('#btnDeptCancel').click();
				});
				
				$('#btnDeptCancel').click(function(){
					$('#depSelect').next().hide();
				});				
				////////////////////////////////////////////////
				
				/******************提示框*******************/
				/*
				if($('#objects')[0]){
				    if(_categoryTag=="1"){ //针对广东移动，获取分类
				        getCategory.init($('#objects')[0]);
				    }else{//获取实例
				        getObject.init($('#objects')[0]);
				    }
				}*/
				//便签知识关联调整为对话框选择  modify by heart.cao 2016-05-04
				//分类选择
				$('#kbValueName').click(function(){
					$.kbase.picker.ontoCate({returnField: 'kbValueName|kbValueId'});
				});
				
				//清空分类
				$('#btnClearKbValue').click(function(){
					$('#kbValueName').val('');
					$('#kbValueId').val('');
				});
				
				/////////////////////////////////////////////////
				///////////////标题字数提示信息////////////////////
				$("#title").keyup(function(){
				    var title = $(this).val().replace(/[\r\n]/g,"");
				    var length = title.length;
				    var tipNotice = $('#tipNotice');
				    if(length > 30){
				        tipNotice.html('* 标题最多不能超过30个字符');
				        $(this).val(title.substring(0,30))
				        length = 30;
				    }else{
				        tipNotice.html('');
				    }
				    $("#title_lenght_show").text("30/"+length);
				});
				/////////////////////////////////////////////////
				
				//上传附件
				$('#attachments').parent('td').on('change', '#attachments', function(){
				    if($(this).val() != ''){
						uploadFiles();
					}
				});
				
				//删除附件
				$('#attachmentView').on('click', '.delAttach', function(){
					var _this = this;
					var delfname = "";
					
					if(confirm('您确定要删除该附件吗？')){
						delfname = $(_this).prev().attr("_fileName");
						
						$(_this).parent().remove();
						//重置附件序号
						$("#attachmentView").find("li").each(function(index, item){
							var $ordernum = $(item).find('div:eq(0)');
							$(item).find('div:eq(0)').text(index+1);
						});
					}
					
					if(delfname != ""){
						var fname = $("[name='fileName']").val();
						console.log(fname);
						
						var patten = new RegExp(delfname+'\\|[^,]+,?');
		    			fname = fname.replace(patten, '');
		    			
		    			var patten2 = new RegExp("^,|,$");
		    			fname = fname.replace(patten2, "");
		    			console.log(fname);
		    			$("[name='fileName']").val(fname);
					}
					
					
				});
				
				
				
				
			}
			/////////////////////////////////////////////////
			///////////////便签内容富文本插件////////////////////
			var editor;			
			KindEditor.ready(function(K) {
				editor = K.create('textarea[id="content"]', {
				    afterChange:function(){
				        ///////////////便签内容长度提示信息////////////////////
                        var content = this.text().replace(/[\r\n]/g,"");
					    var length = content.length;
					    var tipNotice = $('#tipNotice');
					    if(length > 1500){
					        tipNotice.html('* 内容最多不能超过1500个字符');
					        this.text(content.substring(0,1500))
					        length = 1500;
					    }else{
					        tipNotice.html('');
					    }
					    $("#content_lenght_show").text("1500/"+length);
                    },
		            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist'] 
				});
			});
			/////////////////////////////////////////////////	
			
			
			//上传附件（不转换格式）
			function uploadFiles() {
				var upload_idx = layer.load('正在上传，请稍后……');
			
				$.ajaxFileUpload({
					url: '${pageContext.request.contextPath}/app/util/file-upload!upload.htm', 
					type: 'post',
					data: { dir: 'bulletin'}, 
					secureuri: false, //一般设置为false
					fileElementId: 'attachments', // 上传文件的id、name属性名
					dataType: 'json', //返回值类型，一般设置为json、application/json
					success: function(data, status){  
						if (data.rst == "1") {
							
							var attachHtml = '';
							attachHtml += '<li style="list-style:none;line-height:30px;">';
							attachHtml += '<div style="display: inline; margin-right: 10px;">' + ($("#attachmentView").find("li").length+1) + '.</div>&nbsp;';
							attachHtml += '<div style="display: inline; margin-right: 10px;" _fileid="" _filename="' + data.attachFileName + '">' + data.attachRealName + '</div>';
							attachHtml += '<div style="display: inline; margin-right: 10px;" class="delAttach"><a href="javascript:void(0)" style="text-decoration:none;">删除</a></div>';
							attachHtml += '</li>';
							
							$("#attachmentView").append(attachHtml);
							
							var fname = $("[name='fileName']").val();
							var _fname = data.attachFileName + "|" + data.attachRealName
							if($.trim(fname) == ''){
								$("[name='fileName']").val(_fname);
							}else{
								$("[name='fileName']").val(fname+"," + _fname);
							}
						} else {
							alert(data.msg);
						}
					},
					error: function(data, status, e){ 
						alert(e);
					},
					complete: function () {
						layer.close(upload_idx);
        			}
				});
			}
			
			
							
		</script>
		
	</head>

	<body>
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">标题<span class="inputflag">*</span></td>
				<td width="75%" style="border-right-style:hidden"><textarea id="title" rows="2" style="width:550px;"></textarea></td>
				<td width="10%" style="border-left-style:hidden" valign="bottom"><span id="title_lenght_show">30/0</span></td>
			</tr>
			<tr>
				<td>内容<span class="inputflag">*</span></td>
				<td valign="bottom" style="border-right-style:hidden"><textarea id="content" style="width:100%;height:150px;visibility:hidden;"></textarea></td>
			    <td valign="bottom" style="border-left-style:hidden"><span id="content_lenght_show" style="float:left;">1500/0</span></td>
			</tr>	
			<tr>
				<td>加载知识</td>
				<!-- <td colspan="2">
					<input id="objects" type="text" style="width:550px;"/>
					<br><span style="color:red;"><%=_objectsNotice %></span>
				</td> -->
				<td colspan="2">
					<input id="kbValueName" type="text" style="width:550px;" /><input type="hidden" id="kbValueId" />
					<input type="button" value="清空" id="btnClearKbValue">
				</td>				
			</tr>
			<tr>
				<td>接收人</td>
				<td colspan="2">
					<input id="users" type="text" style="width:550px;"/>
					<br><span style="color:red;">多个接收人之间请用西文分号分隔</span>
				</td>
			</tr>
			<tr>
				<td>发布范围</td>
				<td colspan="2">
					<input id="depSelect" type="text" readonly="readonly" value="请选择" style="width:550px;"/>
					<div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #C0C0C0; _position:absolute;">
						<a href="javaScript:void(0);" style="margin-left:260px;" id="btnDeptOk">[确定]</a>
						<a href="javaScript:void(0);" id="btnDeptCancel">[关闭]</a>
						<ul id="noticeTree" class="ztree" style="height:120px;overflow:auto;"></ul>
					</div>
				</td>
			</tr>			
			<tr>
				<td>显示类型</td>
				<td colspan="2">
					<input type="checkbox" id="type1" name="type" value="1" checked="checked" />跑马灯
					<!-- <input type="checkbox" id="type2" name="type" value="2" disabled="disabled"/>弹屏 -->
					<input type="checkbox" id="type3" name="type" value="3" checked="checked" disabled="disabled"/>便签栏
				</td>
			</tr>
			<tr>
				<td>有效周期<span class="inputflag">*</span></td>
				<td colspan="2">
					<input type="text" id="startTime" readonly="readonly" value="${startTime}" style="width:140px;" onFocus="WdatePicker({startDate:'%y-%M-%d 00:00:00',maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})" /> ～ 
					<input type="text" id="endTime" readonly="readonly" value="${endTime}" style="width:140px;" onclick="WdatePicker({startDate:'%y-%M-%d 23:59:59',minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})"/>
				</td>
			</tr>
			<!-- 
			<tr>
				<td>是否置顶</td>
				<td><input type="checkbox" id="top" value="1" />置顶</td>
			</tr> -->
			
			<tr>
				<td>附件</td>
				<td colspan="2">
					<input type="file" id="attachments" name="attachments" style="width: 222px;"/>
					<input type="hidden" id="fileName" name="fileName"/>
					<ul id="attachmentView" style="margin-top: 10px;">
				     	
					</ul>
				</td>
			</tr>
			
			
			
			<tr>
				<td align="right" colspan="3">
					<span id="tipNotice" style="color:red;"></span>&nbsp;
					<input type="button" value="发送" id="btnSave">
				</td>
			</tr>
		</table>
		
		
	</body>
</html>
