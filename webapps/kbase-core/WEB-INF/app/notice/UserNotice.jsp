<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">

	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/deptTree.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/gonggao.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Notice.js"></script>
</head>

<body>



<!--******************内容开始***********************-->

<div class="content_right_bottom">

<div class="gonggao_titile">
<div class="gonggao_titile_left">
共<span>${totalCount}</span>条记录
</div>
</div>


<div class="gonggao_con">
<div class="gonggao_con_title">
<h1><b>名称</b></h1><h2>创建日期</h2><h3>创建人</h3><h4>公告有效期</h4><h5>状态</h5>
</div>
<div class="gonggao_con_nr">
<ul>
<s:iterator value="#request.userList" var="list">
<li>
	<h1><input name="" type="checkbox" value="" />
	<input type="hidden" value="${notice.id}">
	<b>
		<s:if test="#list.notice.top==1">
			<b style="color:red;">置顶&nbsp;&nbsp;</b>
		</s:if>
		<s:else>
		<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
		</s:else>
		
		<s:if test="#list.read==0">
			<b style="color:red;">未读</b>
		</s:if>
		<s:else>
			<b style="color:#cccccc;">已读</b>
		</s:else>
		&nbsp;&nbsp;
		
		<a href="#" class="noticeTitle" noticeId="${notice.id}">
		<s:if test="notice.title.length()>20">
			<s:property value="notice.title.substring(0,20)+'...'" />
		</s:if>
		<s:else>
			${notice.title}
		</s:else>
		</a>
	</b>
	</h1>
	<h2><s:date format="yyyy-MM-dd" name="notice.editTime" /></h2>
	<h3>${notice.issue.username}</h3>
	<h4><s:date format="yyyy-MM-dd" name="notice.startTime"/>至<s:date format="yyyy-MM-dd" name="notice.endTime"/></h4>
	<h5>
		<s:if test="(new java.util.Date()).getTime() < notice.endTime.getTime()">
			未过期
		</s:if>
		<s:else>已过期</s:else>
	</h5>
</li>
</s:iterator>

</ul>
<s:if test="totalPage>1">
<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
	<a href="#" onfocus="this.blur();">首页</a>
	<a href="#" onfocus="this.blur();">&lt;上一页</a>
	<s:iterator value="new int[totalPage]" status="i">
		<s:if test="#i.index+1 == currentPage"> 
			<a href="#" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
		</s:if>
		<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
			<a href="#" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
		</s:elseif>
	</s:iterator>
	
	<a href="#" onfocus="this.blur();">下一页&gt;</a>
	<a href="#" onfocus="this.blur();">尾页</a>
</div>
</s:if>
<form id="fenyeForm" method="post" style="display:none;" action="${pageContext.request.contextPath}/app/notice/notice!all.htm">
	<input type="text" hidden="hidden" name="start"/>
	<input type="text" hidden="hidden" name="order" value="${order}"/>
	<input type="text" hidden="hidden" name="lastOrder" value="${lastOrder}"/>
	<input type="text" hidden="hidden" name="asc" value="${asc}"/>
</form>
</div>


</div>


</div>


</body>
</html>
