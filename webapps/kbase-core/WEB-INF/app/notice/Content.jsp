<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String _categoryTag = (String)session.getAttribute("category_tag");//是否为分类展示，"1"-是 "0"-否，如果是，选择关联知识时，关联到分类的叶子节点 针对广东移动
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>公告展示</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/gonggao_zhanshi.css?2016010101" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		     var _categoryTag = '<%=_categoryTag%>'; 
		     var _isNote = '${isNote}';//是否为便签 1-是 0-否
		</script>	
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Content.js?20150925"></script>
	</head>

	<body>
		<!--******************内容开始***********************-->
		<div class="content_right_bottom">
			<div class="ggzs">
			    <s:if test="#request.hideReturn != 1">
					<div class="ggzs_titile">
					    <s:if test="#request.isNote == 1">
						     <a href="javascript:void(0);" onfocus="this.blur();" id="backNotice" >返回便签列表</a>
						</s:if>
						<s:else>
						     <a href="javascript:void(0);" onfocus="this.blur();" id="backNotice" >返回公告列表</a>
						</s:else>
					</div>
			    </s:if>
				<div class="ggzs_con">
					<div class="ggzs_con_nr">
						<ul>
							<li>
								<b class="title">${notice.title}</b>
							</li>
							<li class="date">
								<b>创建人：</b><span>${notice.issue.username}</span><b>创建日期：</b><span><s:date format="yyyy-MM-dd" name="#request.notice.editTime" /></span>
							</li>
							<li class="kuang">
								<div>
									<span>
										<!--<s:property value="#request.notice.content.replace(\"\n\",\"<br/>\")" escape="false"/>-->
										<s:if test="#request.notice.content==null">
											<s:property value="#request.notice.htmlContent" escape="false"/>
										</s:if>
										<s:else>
											<s:property value="#request.notice.content" escape="false"/>
										</s:else>
									</span>
								</div>
								<div>
									<s:iterator value="#request.objList" var="obj" >
									    <s:if test="#session.category_tag == 1">
									        <a href="javascript:void(0);" objId="${obj.id}" objName="${obj.name}" onfocus="this.blur();">${obj.name}</a><br/>
									    </s:if>
									    <s:else>
									       <a href="javascript:void(0);" objId="${obj.objectId}" objName="${obj.name}" onfocus="this.blur();">${obj.name}</a><br/>
									    </s:else>
									</s:iterator>
								</div>
								<div style="margin-bottom: 5px;">
									<span>
										<s:if test="#request.notice.fileName!=null">
											<!--
											<span style="float: left; height:24px; line-height:24px;">附件</span>
											 便签的附件 -->
											<s:iterator value="#request.notice.attachFileList" var="attach" status="status">
											    <a href="${swfAddress }/convert/download.do?dir=bulletin&filename=${attach.fileRefName }&realname=${attach.fileRealName }" style="cursor: pointer; text-decoration:none;">
											    	[附件]&nbsp;
											    	<s:if test="#attach.fileRealName.length()>25">
														<s:property value="#attach.fileRealName.substring(0,12)+'...'+#attach.fileRealName.substring(#attach.fileRealName.length()-10)" />
													</s:if>
													<s:else>
														${attach.fileRealName}
													</s:else>
											    </a><br/>
											</s:iterator>
												
										</s:if>
									</span>
								</div>
							</li>
							<li style="height:100px;">
								<a href="${pageContext.request.contextPath}/app/notice/notice!next.htm?id=${notice.id}&isNote=${isNote}&hideReturn=${hideReturn}" class="button2">下一条</a>
								<a href="${pageContext.request.contextPath}/app/notice/notice!prev.htm?id=${notice.id}&isNote=${isNote}&hideReturn=${hideReturn}" class="button1">上一条</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--******************内容结束***********************-->

	</body>
</html>