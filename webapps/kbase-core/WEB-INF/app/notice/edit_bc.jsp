<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String _toTopCount = (String)session.getAttribute("to_top_count");//用户最多可置顶公告数量
String _topCount = (String)request.getAttribute("top_count");//当前用户已置顶公告数量
String _categoryTag = (String)session.getAttribute("category_tag");//是否为分类展示，"1"-是 "0"-否，如果是，选择关联知识时，关联到分类的叶子节点 针对广东移动
String _objectsNotice = "多个实例之间请用西文分号分隔";
if("1".equals(_categoryTag))_objectsNotice = "多个分类之间请用西文分号分隔";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>新增公告</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!-- kindeditor -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/themes/default/default.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.css" />
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/kindeditor_hc.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/lang/zh_CN.js"></script>
	    <script charset="utf-8" src="${pageContext.request.contextPath}/library/kindeditor/plugins/code/prettify.js"></script>	
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.inputflag{
			    color:red;
			    font-size:14px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>	
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js?20160114"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Object.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Category.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/BCategoryTree.js"></script>
		
		
		<script type="text/javascript">
			/*
			 * add by eko.zhan at 2016-01-06 19:35 复写KindEditor的预览事件
			 */
			KindEditor.plugin('preview', function(K) {
				var self = this, name = 'preview', undefined;
				self.clickToolbar(name, function() {
					var lang = self.lang(name + '.'),
						html = '<div style="padding:10px 20px;">' +
							'<iframe class="ke-textarea" frameborder="0" style="width:630px;height:380px;"></iframe>' +
							'</div>',
						dialog = self.createDialog({
							name : name,
							width : 700,
							title : self.lang(name),
							body : html
						}),
						iframe = K('iframe', dialog.div),
						doc = K.iframeDoc(iframe);
					doc.open();
					doc.write(self.fullHtml());
					doc.close();
					K(doc.body).css('background-color', '#FFF');
					iframe[0].contentWindow.focus();
				});
			});
		</script>
		
		<script type="text/javascript">
			var _toTopCount = "<%=_toTopCount%>"; //用户最多可置顶公告数量
			var _topCount = "<%=_topCount%>"; //当前用户已置顶公告数量
			var _categoryTag = "<%=_categoryTag%>"
					
			$(function(){
			
				//发布选择部门
				$('#deptDesc').click(function(){
					$.kbase.picker.multiDeptForNotice({returnField: 'deptDesc|deptId'});
				});
				$('#btnClearDept').click(function(){
					$('#deptDesc').val('');
					$('#deptId').val('');
				});
			
				$.fn.zTree.init($("#noticeTree"), setting);
				
				$('#depSelect').click(function(){
					var _offset = $(this).offset();
					var _scrollTop = $(document).scrollTop()
					$(this).next().css({left:_offset.left + "px", top:_offset.top - _scrollTop + $(this).outerHeight() + "px"}).slideDown("fast");
				});

				//选中后显示选择的岗位 modify by eko.zhan at 2016-01-07 14:21
				//部门选择确认
				$('#btnDeptOk').click(function(){
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);		 
					//moidfy by eko.zhan at 2015-06-05 选择1个节点就为已选择
					if(nodes.length>0){
						//$('#depSelect').val('已选择');
						var _stations = '';
						$(nodes).each(function(i, item){
							if (item.isParent==false){
								_stations +=  item.name + ',';
							}
						});
						if (_stations.length>0){
							_stations = _stations.substring(0, _stations.length-1);
						}
						$('#depSelect').val(_stations);
					}else{
						$('#depSelect').val('请选择');
					}
					$('#btnDeptCancel').click();
				});
				
				//部门选择取消
				$('#btnDeptCancel').click(function(){
					$('#depSelect').next().hide();
				});
				
				//发送公告
				$('#btnSave').click(function(){
					var title = $('#title').val();
					var content = editor.html(); //$('#content').val();
					var objects = $('#objects').val();
					if(objects == '多个实例之间请用分号分格' 
					|| objects == '多个分类之间请用分号分格')
						objects = '';
					var type1 = $('#type1');
					var type2 = $('#type2');
					var type3 = $('#type3');
					
					var type = 0;
					
					if(type1.attr('checked')=='checked'){
						type += 1;
					}
					if(type2.attr('checked')=='checked'){
						type += 2;
						
					}
					
					var startTime = $('#startTime').val();
					var endTime = $('#endTime').val();
					var tipNotice = $('#tipNotice');
					
					var statistics = 0;
					var top = 0;
					if($('#statistics').attr('checked')=='checked'){
						statistics = 1;
					}
					if($('#top').attr('checked')=='checked'){
						top = 1;
					}
					
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
					var depStr="";
					
					//modify by eko.zhan at 2015-06-16 部门岗位采用异步加载，如果未完成加载当管理员选择父级部门，不会默认选中岗位
					/*
					var stationsId = ''
					for(var i=0; i<nodes.length; i++){
						depStr+=nodes[i].id+",";
						if(!nodes[i].isParent){
							if(stationsId == ''){
								stationsId += nodes[i].id;
							}else{
								stationsId += ','+nodes[i].id;
							}
						}
					}
					//去掉逗号
					if (depStr.length>0){
						depStr = depStr.substring(0, depStr.length-1);
					}
					*/
					
					var busLocation = $('#blSelect').val(); //地市
					var busCategory = $('#bcSelect').val(); //业务类型
					
					if( title==null || $.trim(title)=='' ){
						tipNotice.html('* 标题不能为空');
					}else if( content==null || $.trim(content)=='' ){
						tipNotice.html('* 内容不能为空');
					}/*else if(nodes.length < 1){
						tipNotice.html('* 发布范围不能为空');
					}else if (stationsId==''){
						tipNotice.html('* 发布范围请勾选岗位');
					}*/
					else if ($('#deptDesc').val()==''){
						tipNotice.html('* 发布范围不能为空');
					}else if( startTime==null || $.trim(startTime)=='' || endTime==null || $.trim(endTime)==''){
						tipNotice.html('* 公告周期不能为空');
					}else if(top==1 && !isNaN(_toTopCount)&&!isNaN(_topCount)&&Number(_topCount)>=Number(_toTopCount)){
						tipNotice.html('* 您已经置顶'+_toTopCount+'条(含'+_toTopCount+'条)以上公告，无法继续置顶');
					}else{
						//$('body').hideShade();
						//调整为layer遮罩层  modify by heart 2016-05-13
						window.__kbs_layer_index = layer.load('请稍候...');
						
						//针对广东移动，获取分类
						var categorys = "";
						if(_categoryTag=="1")categorys = getCategory.checkIds();
						
						//验证实例
						var flag = false;
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/notice/notice!testObject.htm",
					   		data: {
					   			objects : objects,
					   			categorys : categorys
					   		},
					   		async: false,
					   		success: function(msg){
					   			if(msg=='"ok"'){
					   				flag = true;
					   			}else{
					   				tipNotice.html(msg.replace("\"","").replace("\"",""));
					   			}
					   		},
					   		error: function(){
					   		}
						});
						
						if(!flag){
							//$('body').ajaxLoadEnd();
							//$('body').showShade();
							layer.close(window.__kbs_layer_index);
							return false;
						}
						//$('div.gonggao_d').hide();
						//$('body').hideShade();
						
						tipNotice.html('');
						
						//提交数据
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath()+"/app/notice/notice!add.htm",
					   		data: {
					   			title : title,
					   			content : content,
					   			objects : objects,
					   			categorys : categorys,
					   			type : type,
					   			top : top,
					   			startTime : startTime,
					   			endTime : endTime,
					   			statistics : statistics,
					   			//stationsId : stationsId,
					   			//depScope: depStr,
					   			deptDesc : $('#deptDesc').val(),
					   			deptId : $('#deptId').val(),
					   			isNote : '0', //公告
					   			busLocation : busLocation, //地市
					   			busCategory : busCategory  //业务类型
					   		},
					   		async: true,
					   		success: function(msg){
					   			layer.close(window.__kbs_layer_index);
					   			alert('发布成功');
					   			var _iframe = $(parent.document).find('div.content_content').find('iframe:visible')
								_iframe.attr('src', _iframe.attr('src'));
								parent.layer.close(parent.__kbs_layer_index);
					   		},
					   		error: function(){
					   			layer.close(window.__kbs_layer_index);
					   			alert('发布失败');
					   		}
						});
						
					} //end if-else
				});
				
				/******************提示框*******************/
				if($('#objects')[0]){
				    if(_categoryTag=="1"){ //针对广东移动，获取分类
				       getCategory.init($('#objects')[0]);
				    }else{//获取实例
				       getObject.init($('#objects')[0]);
				    }
				}
				
				/////////////////////////////////////////////////
				///////////////标题字数提示信息////////////////////
				//modify by eko.zhan at 2016-01-07 11:00 广东移动提出字数放开至 200
				//测试出一个bug，如果字数超出，依然可以提交，后来发现不是bug，毕竟标题输入的时候切割了字数
				var maxTitleLength = 200;
				$("#title_lenght_show").text(maxTitleLength + "/0");
				$("#title").keyup(function(){
				    var title = $(this).val().replace(/[\r\n]/g,"");
				    var length = title.length;
				    var tipNotice = $('#tipNotice');
				    if(length > maxTitleLength){
				        tipNotice.html('* 标题最多不能超过' + maxTitleLength + '个字符');
				        $(this).val(title.substring(0, maxTitleLength))
				        length = maxTitleLength;
				    }else{
				        tipNotice.html('');
				    }
				    $("#title_lenght_show").text(maxTitleLength + "/" + length);
				});
				////////////////////////////////////////////////					
			}); //end dom load ready
			
			/////////////////////////////////////////////////
			///////////////公告内容富文本插件////////////////////
			//modify by eko.zhan at 2016-01-07 11:16 优化代码，字数限制统一维护
			//增加CLOB存储公告，这里不再做字数限制
			var maxContentLength = 1500;
			//$('#content_lenght_show').text(maxContentLength + "/0");
			var editor;			
			KindEditor.ready(function(K) {
				editor = K.create('textarea[id="content"]', {
					/* 如果需要长度验证，请放开这一段代码
				    afterChange:function(){
				        ///////////////公告长度校验////////////////////
                        var content = this.text().replace(/[\r\n]/g,"");
					    var length = content.length;
					    var tipNotice = $('#tipNotice');
					    if(length > maxContentLength){
					        tipNotice.html('* 内容最多不能超过' + maxContentLength + '个字符');
					        this.text(content.substring(0, maxContentLength))
					        length = maxContentLength;
					    }else{
					        tipNotice.html('');
					    }
					    $("#content_lenght_show").text(maxContentLength + "/" + length);
                    },*/
		            items : ['formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist', '|', 'preview'] 
				});
			});
			/////////////////////////////////////////////////	
		</script>
		
	</head>

	<body>
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">标题<span class="inputflag">*</span></td>
				<td width="75%" style="border-right-style:hidden"><textarea id="title" rows="2" style="width:550px;"></textarea></td>
				<td width="10%" style="border-left-style:hidden" valign="bottom"><span id="title_lenght_show"></span></td>
			</tr>
			<tr>
				<td>内容<span class="inputflag">*</span></td>
				<td valign="bottom" style="border-right-style:hidden"><textarea id="content" style="width:100%;height:150px;visibility:hidden;"></textarea></td>
			    <td valign="bottom" style="border-left-style:hidden"><span id="content_lenght_show" style="float:left;"></span></td>
			</tr>			
			<tr>
				<td>加载知识</td>
				<td colspan="2">
					<input id="objects" type="text" style="width:550px;"/>
					<br><span style="color:red;"><%=_objectsNotice %></span>
				</td>
			</tr>
			<tr>
				<td>发布范围<span class="inputflag">*</span></td>
				<td colspan="2">
					<!-- 
					<input id="depSelect" type="text" readonly="readonly" value="请选择" style="width:550px;"/> -->
					<input id="deptDesc" type="text" style="width:550px;">
					<input id="deptId" type="hidden"/>
					<input type="button" value="清空" id="btnClearDept">
					<div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #C0C0C0; _position:absolute;">
						<a href="javaScript:void(0);" style="margin-left:260px;" id="btnDeptOk">[确定]</a>
						<a href="javaScript:void(0);" id="btnDeptCancel">[关闭]</a>
						<ul id="noticeTree" class="ztree" style="height:150px;overflow:auto;"></ul>
					</div>
				</td>
			</tr>
			<tr>
				<td>公告分类</td>
				<td colspan="2">
				    <input id="blSelect" type="text" readonly="readonly" style="width:180px;"/>
				    <div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #C0C0C0; _position:absolute;">
				    	<span class="bc_btn" style="margin-left:95px;" id="btnBLOk">[确定]</span>
						<span class="bc_btn" id="btnBLCancel">[关闭]</span>
						<ul id="blTree" class="ztree" style="height:100px;overflow:auto;"></ul>
					</div>
				    <input id="bcSelect" type="text" readonly="readonly" style="width:220px;"/>
				    <div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #C0C0C0; _position:absolute;">
				    	<span class="bc_btn" style="margin-left:135px;" id="btnBCOk">[确定]</span>
						<span class="bc_btn" id="btnBCCancel">[关闭]</span>
						<ul id="bcTree" class="ztree" style="height:100px;overflow:auto;"></ul>
					</div>				    
				</td>
			</tr>						
			<tr>
				<td>显示类型</td>
				<td colspan="2">
					<input type="checkbox" id="type1" name="type" value="1" />跑马灯
					<input type="checkbox" id="type2" name="type" value="2" disabled="disabled"/>弹屏
					<input type="checkbox" id="type3" name="type" value="3" checked="checked" disabled="disabled"/>公告栏
				</td>
			</tr>
			<tr>
				<td>公告周期<span class="inputflag">*</span></td>
				<td colspan="2">
					<input type="text" id="startTime" readonly="readonly" value="${startTime}" style="width:140px;" onFocus="WdatePicker({startDate:'%y-%M-%d 00:00:00',maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})" /> ～ 
					<input type="text" id="endTime" readonly="readonly" value="${endTime}" style="width:140px;" onclick="WdatePicker({startDate:'%y-%M-%d 23:59:59',minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})"/>
				</td>
			</tr>
			<tr>
				<td>是否置顶</td>
				<td colspan="2"><input type="checkbox" id="top" value="1" />置顶</td>
			</tr>
			<tr>
				<td align="right" colspan="3">
					<span id="tipNotice" style="color:red;"></span>&nbsp;
					<input type="button" value="发送" id="btnSave">
				</td>
			</tr>
		</table>
		
		
	</body>
</html>