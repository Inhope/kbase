<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
	  		td a{
	  			cursor: pointer;
	  		}
	  		input[type="text"]{
	  			border:1px solid #C0C0C0;
	  		}
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">
        <script language="javaScript1.2">
             _isNote = '${isNote}';//是否为便签 1-是 0-否 
        </script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Send.js?20160114"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Object.js"></script>
		<STYLE type="text/css">
		   .gonggao_con_title td{color:#666;font-size:12px;}
		   .gonggao_titile{margin:0;}
		   .gonggao_con{margin:0px 0px 0;}
		   .gonggao_con_nr_fenye a{font-size:12px;}
	    </STYLE>
	</head>

	<body>
		<!--******************内容开始***********************-->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="3%"></td>
						<td width="25%"></td>
						<td width="10%"></td>
						<td width="5%"></td>
						<td width="10%"></td>
						<td width="5%"></td>
						<td width="8%"></td>
						<td width="8%"></td>
						<td width="16%"></td>
					</tr>
					<tr>
						<td colspan="3" align="left" valign="middle" style="padding-left:5px;">
							<div class="gonggao_titile_left">
								共 <span>${totalCount}</span> 条记录						
								<select id="select" style="margin-left: 10px;">
									<option value="0" >
										收件箱
									</option>
									<option value="1" selected="selected">
										发件箱
									</option>
								</select>
							</div>
						</td>
						<td colspan="6">
							<div class="gonggao_titile_right">
								<s:if test="isNote==1">
									<a href="javascript:void(0);" id="btnDelete">召回便签</a>
									<a href="javascript:void(0);" id="btnNew">新建便签</a>
								</s:if>
								<s:else>
									<!-- <a href="javascript:void(0);" id="btnCateWH">分类维护</a> -->									
									<a href="javascript:void(0);" id="btnCancelTop">取消置顶</a>
									<a href="javascript:void(0);" id="btnTop">置顶</a>								
									<a href="javascript:void(0);" id="btnDelete">删除公告</a>
									<a href="javascript:void(0);" id="btnNew">新建公告</a>							
								</s:else>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="9">
							<form id="receiveForm" method="post" action="${pageContext.request.contextPath}/app/notice/notice!send.htm">
								<input type="hidden" name="start"/>
								<input type="hidden" name="order" value="${order}"/>
								<input type="hidden" name="lastOrder" value="${lastOrder}"/>
								<input type="hidden" name="asc" value="${asc}"/>
								<input type="hidden" name="filtrate" value="${filtrate}"/>
								<input type="hidden" name="isNote" value="${isNote}"/>
								<table cellpadding="0" cellspacing="0" style="border:0px;width:100%;">
									<tr>
										<td valign="middle" style="padding-left:5px;text-align: left;">
											名称/内容：
											<input name="title" type="text" value="${title}" style="width:120px"/>
											时间：
											<input id="startTimeForm" name="startTime" readonly="readonly" type="text" value="${startTime}" style="width:80px" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endTimeForm\')}',dateFmt:'yyyy-MM-dd'})"/>
											～
											<input id="endTimeForm" name="endTime" readonly="readonly" type="text" value="${endTime}" style="width:80px" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startTimeForm\')}',dateFmt:'yyyy-MM-dd'})"/>
											创建人：
											<input class="mingzi shaixuaninput" name="issueName" type="text" value="${issueName}" style="width:80px"/>
											公告状态：
											<select name="pastEndTime" value="${pastEndTime}">
											    <s:if test="pastEndTime==0">
												       <option value="0" selected>全部</option>
												    </s:if>
												    <s:else>
												       <option value="0">全部</option>
												    </s:else>	
                                                    <s:if test="pastEndTime==1">
												       <option value="1" selected>未过期</option>
												    </s:if>
												    <s:else>
												       <option value="1">未过期</option>
												    </s:else>													
												    <s:if test="pastEndTime==2">
												       <option value="2" selected>已过期</option>
												    </s:if>
												    <s:else>
												       <option value="2">已过期</option>
												    </s:else>
												    <s:if test="pastEndTime==3">
												       <option value="3" selected>未生效</option>
												    </s:if>
												    <s:else>
												       <option value="3">未生效</option>
												    </s:else>												
											</select>
										</td>
										<td style="width:200px;">
											<div class="gonggao_titile_right">
												<a href="javaScript:void(0);">提交</a>
												<a href="javaScript:void(0);">重置</a>
											</div>
										</td>
									</tr>
								</table>
							</form>		
						</td>
					</tr>
					<tr class="tdbg">
						<td><input id="checkAll" name="" type="checkbox" value="" /></td>
						<td>名称</td>
						<td>创建时间</td>
						<td>创建人</td>
						<td>公告有效期</td>
						<td>状态</td>
						<td>公告点击率</td>
						<td>明细</td>
						<td>发布范围</td>
					</tr>
					<s:iterator value="send" var="list">
						<tr>
							<td>
								<input name="rowCheck" type="checkbox" value="" />
								<input type="hidden" value="${id}">
							</td>
							<td style="text-align: left;padding-left: 5px;">
								<s:if test="isTop==1">
									<font color="red">置顶&nbsp;&nbsp;</font>
								</s:if>
								<a title="${title}" noticeId="${id}" class="noticeTitle">
									<s:if test="title.length()>25">
										<s:property value="title.substring(0,24)+'...'" />
									</s:if>
									<s:else>
										${title}
									</s:else>
								</a>
							</td>
							<td>
								<s:date format="yyyy-MM-dd" name="editTime" />
							</td>
							<td>
								${issue.userInfo.userChineseName}
							</td>
							<td>
								<s:date format="yyyy-MM-dd" name="endTime"/>
							</td>
							<td>
							    <s:if test="(new java.util.Date()).getTime() < startTime.getTime()">
									未生效
								</s:if>
								<s:else>
									<s:if test="(new java.util.Date()).getTime() < endTime.getTime()">
										未过期
									</s:if>
									<s:else>已过期</s:else>
								</s:else>
							</td>
							<td>
								${statistics}
							</td>
							<td> 
								<a href="javascript:void(0);" onclick="readList(1,'${id}');">已阅</a>|
								<a href="javascript:void(0);" onclick="readList(0,'${id}');">未阅</a>
							</td>
							<td style="text-align:left;padding-left: 5px;" title="${depScope}">
								<s:if test="depScope.length()>25">
									<s:property value="depScope.substring(0,24)+'...'" />
								</s:if>
								<s:else>
									${depScope}
								</s:else>
							</td>
						</tr>
					</s:iterator>
					<s:if test="totalPage>1">
						<tr>
							<td colspan="9">
								<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
									<a href="javaScript:void(0);" onfocus="this.blur();">首页</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">&lt;上一页</a>
									<s:iterator value="new int[totalPage]" status="i">
										<s:if test="#i.index+1 == currentPage"> 
											<a href="javaScript:void(0);" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
										</s:if>
										<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
											<a href="javaScript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
										</s:elseif>
									</s:iterator>
									
									<a href="javaScript:void(0);" onfocus="this.blur();">下一页&gt;</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">尾页</a>
								</div>
							</td>
						</tr>
					</s:if>
				</table>
			</div>
		</div>
		<!--******************弹出框***********************-->
		<div class="gonggao_d" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			<div class="gonggao_dan">
				<div class="gonggao_dan_title"><b>新建公告</b><a href="javaScript:void(0);" id="closeNotice"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/images/xinxi_ico1.jpg" width="12" height="12" /></a></div>
					  <form name="form1" method="post" action="">
						<div class="gonggao_dan_con">
						<ul>
							<li><b>标题：</b><input id="title" type="text" /></li>
							<li><b>内容：</b><textarea id="content"></textarea></li>
							<li><b>加载知识：</b><input id="objects" type="text" /></li>
							<li><b>发布范围：</b><input id="depSelect" type="text" readonly="readonly" style="width:200px;" value="请选择"/></li>
							<li><b>显示类型：</b>
							    <label>
							    	<input type="checkbox" id="type1" name="type" value="1" />跑马灯
							    </label>
							    <label>
							  		<input type="checkbox" id="type2" name="type" value="2" />弹屏
							    </label>
							    <label>
							        <input type="checkbox" id="type3" name="type" value="3" checked="checked" disabled="disabled"/>公告栏
						        </label>
							    
							</li>
							<li><b>公告周期：</b>
							<input type="text" id="startTime"/>
							<input type="text" id="endTime"/>
							<li><b>特别应用：</b>
						  		<!-- <label>
						    		<input type="checkbox" id="statistics" value="1" checked="true" disabled="disabled"/>点击率统计
						    	</label> 
						    	-->
						  		<input type="hidden" id="statistics" value="1" checked="true" disabled="disabled"/>
						  		<label>
							    	<input type="checkbox" id="top" value="1" />置顶
							    </label>
						</li>
						<li><span id="tipNotice" style="color:red;"></span><a href="javaScript:void(0);" id="resetBtn" ><input class="gonggao_an2" type="button" /></a><a href="javaScript:void(0);" id="issueBtn"><input class="gonggao_an1" type="button" /></a></li>
						</ul>
						</div>
					  </form>
				</div>
			</div>
			<!--******************弹出框***********************-->
			<div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
				<a href="javaScript:void(0);" style="margin-left:170px;">[关闭]</a>
				<ul id="noticeTree" class="ztree" style="height:150px;overflow:auto;"></ul>
			</div>
			<!-- 已读|未读 -->
			<div id="readListDiv" style="display:none;overflow-y:auto;max-height:300px;">
				<table cellspacing="0" cellpadding="0" class="box" style="width:500px;">
					<tr id="readListTR">
						<th>姓名</th>
						<th>部门</th>
						<th>阅读时间</th>
					</tr>
				</table>
			</div>
		
			<div class="index_d" style="display:none;z-index:10002;">
			<div class="index_dan">
			<div class="index_dan_title"><b></b><a href="javaScript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/images/xinxi_ico1.jpg" width="12" height="12" /></a></div>
			<div class="shai_con">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr class="shai_title">
			    <td>姓名</td>
			    <td>部门</td>
			    <td>阅读时间</td>
			    <td>
			   <!-- <select name="">
			      <option>部门1</option>
			      <option>部门2</option>
			    </select>
			     --> 
			    </td>
			  </tr>
			  <!-- 
			  <tr>
			    <td>张三</td>
			    <td>部门1</td>
			    <td>2014年7月14日</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr>
			    <td>李四</td>
			    <td>部门2</td>
			    <td>2014年7月24日</td>
			    <td>&nbsp;</td>
			  </tr>
			   -->
			</table>
			
			
			</div>
			</div>
		</div>
		<!--******************内容结束***********************-->

	</body>
</html>

