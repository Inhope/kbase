<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">

	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/deptTree.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/gonggao.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/DeptTree.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Notice.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Object.js"></script>
</head>

<body>



<!--******************内容开始***********************-->

<div class="content_right_bottom">

<div class="gonggao_titile">
<div class="gonggao_titile_left">
共<span>${totalCount}</span>条记录
</div>
<s:if test="#request.admin=='admin'">
<div class="gonggao_titile_right">
<a href="#" id="cancelTop">取消置顶</a><a href="#" id="topNotice">置顶</a><a id="deleteNotice" href="#">删除公告</a><a id="newNotice" href="#">新建公告</a>
</div>
</s:if>
</div>


<div class="gonggao_con">
<div class="gonggao_con_title">
<h1><b>名称</b></h1><h2>创建日期</h2><h3>创建人</h3><h4>公告有效期</h4><h5>状态</h5>
</div>
<div class="gonggao_con_nr">
<ul>
<s:iterator value="list" >
<li>
	<h1><input name="" type="checkbox" value="" />
	<input type="hidden" value="${notice.id}">
	<b style="width:85%;text-align:left;white-space: nowrap; text-overflow: ellipsis;  overflow: hidden;">
		<s:if test="notice.top==1">
			<b style="color:red;">置顶&nbsp;&nbsp;</b>
		</s:if>
		<s:else>
		<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
		</s:else>
		<s:if test="id != null">	
			<s:if test="isRead==0">
				<b style="color:red;">未读</b>
			</s:if>
			<s:else>
				<b style="color:#cccccc;">已读</b>
			</s:else>
			&nbsp;&nbsp;
		</s:if>
		
		<a href="javascript:void(0);" title="${notice.title}" class="noticeTitle" noticeId="${notice.id}">
			${notice.title}
		</a>
	</b>
	</h1>
	<h2><s:date format="yyyy-MM-dd" name="notice.editTime" /></h2>
	<h3>${notice.issue.username}</h3>
	<h4><s:date format="yyyy-MM-dd" name="notice.startTime"/>至<s:date format="yyyy-MM-dd" name="notice.endTime"/></h4>
	<h5>
		<s:if test="(new java.util.Date()).getTime() < notice.endTime.getTime()">
			未过期
		</s:if>
		<s:else>已过期</s:else>
	</h5>
</li>
</s:iterator>

</ul>
<s:if test="totalPage>1">
<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
	<a href="#" onfocus="this.blur();">首页</a>
	<a href="#" onfocus="this.blur();">&lt;上一页</a>
	<s:iterator value="new int[totalPage]" status="i">
		<s:if test="#i.index+1 == currentPage"> 
			<a href="#" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
		</s:if>
		<s:elseif test="currentPage < 5 && #i.index<10">
			<a href="javascript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
		</s:elseif>
		<s:elseif test="currentPage > totalPage -5 && #i.index < totalPage && #i.index > totalPage-11">
			<a href="javascript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
		</s:elseif>
		<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
			<a href="#" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
		</s:elseif>
	</s:iterator>
	
	<a href="#" onfocus="this.blur();">下一页></a>
	<a href="#" onfocus="this.blur();">尾页</a>
</div>
</s:if>
<form id="fenyeForm" method="post" style="display:none;" action="${pageContext.request.contextPath}/app/notice/notice!list.htm">
	<input type="text" hidden="hidden" name="start" value="${start}"/>
	<input type="text" hidden="hidden" name="order" value="${order}"/>
	<input type="text" hidden="hidden" name="lastOrder" value="${lastOrder}"/>
	<input type="text" hidden="hidden" name="asc" value="${asc}"/>
</form>
</div>


</div>


</div>

<!--******************弹出框***********************-->
<div class="gonggao_d" style="display:none;position:fixed;z-index:10001;_position:absolute;">
<div class="gonggao_dan">
<div class="gonggao_dan_title"><b>新建公告</b><a href="#" id="closeNotice"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/images/xinxi_ico1.jpg" width="12" height="12" /></a></div>
  <form name="form1" method="post" action="">
<div class="gonggao_dan_con">
<ul>
	<li><b>标题：</b><input id="title" type="text" /></li>
	<li><b>内容：</b><textarea id="content"></textarea></li>
	<li><b>加载知识：</b><input id="objects" type="text" /></li>
	<li><b>发布范围：</b><input id="depSelect" type="text" readonly="readonly" style="width:200px;" value="请选择"/></li>
	<li><b>显示类型：</b>
	    <label>
	    	<input type="checkbox" id="type1" name="type" value="1" />跑马灯
	    </label>
	    <label>
	  		<input type="checkbox" id="type2" name="type" value="2" />弹屏
	    </label>
	    <label>
	        <input type="checkbox" id="type3" name="type" value="3" checked="checked" disabled="disabled"/>公告栏
        </label>
	    
	</li>
	<li><b>公告周期：</b>
	<input type="text" id="startTime"/>
	<input type="text" id="endTime"/>
	<li><b>特别应用：</b>
  		<label>
    		<input type="checkbox" id="statistics" value="1" />点击率统计
    	</label>
  		<label>
	    	<input type="checkbox" id="top" value="1" />置顶
	    </label>
</li>
<li><span id="tipNotice" style="color:red;"></span><a href="#" id="resetBtn" ><input class="gonggao_an2" type="button" /></a><a href="#" id="issueBtn"><input class="gonggao_an1" type="button" /></a></li>
</ul>
</div>
  </form>
</div>
</div>
<!--******************弹出框***********************-->
<div style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;">
<a href="#" style="margin-left:170px;">[关闭]</a>
<ul id="noticeTree" class="ztree"></ul>
</div>

<!--******************内容结束***********************-->

</body>
</html>
