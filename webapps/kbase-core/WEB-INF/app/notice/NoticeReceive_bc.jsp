<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<style type= "text/css" >
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
	  		td a{
	  			cursor: pointer;
	  		}
	  		input[type="text"]{
	  			border:1px solid #C0C0C0;
	  		}
	  		.no_read_tr td{
	  			color: red;
	  		}
	  		.no_read_tr td a{
	  			color: red;
	  		}
	  		.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		    padding: 0 2px 0 0;
	  		    margin-left:1px;
	  		}
	  		.button{
			  font-size: 12px;
			  height: 28px;
			  line-height: 20px;
			  padding: 0 20px;
	  		}
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">
        <script language="javaScript1.2">
             _isNote = '${isNote}';//是否为便签 1-是 0-否
        </script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/Receive.js?20160114"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/notice/js/BCategoryTree.js"></script>
	
	   <STYLE type="text/css">
		   .gonggao_con_title td{color:#666;font-size:12px;}
		   .gonggao_titile{margin:0;}
		   .gonggao_con{margin:0px 0px 0;}
		   .gonggao_con_nr_fenye a{font-size:12px;}
	   </STYLE>
	
	</head>
	<body>
		<!--******************内容开始***********************-->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="3%"></td>
						<td width="35%"></td>
						<td width="8%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="9%"></td>
						<td width="13%"></td>
						<td width="5%"></td>
						<td width="7%"></td>
					</tr>
					<tr>
						<td colspan="9" align="left" valign="middle" style="padding-left:5px;">
							<div class="gonggao_titile_left">
								共 <span>${totalCount}</span> 条记录							
								<select id="select" style="margin-left: 10px;">
									<option value="0" selected="selected">
										收件箱
									</option>
									<s:iterator value="#session.session_user_key.list_Menu" var="va">
										<s:if test="#va.key=='NoticeManagement'">
										    <option value="1">发件箱</option>
										</s:if>
								    </s:iterator>										
								</select>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="9" align="right" valign="middle" style="text-align:right;" >
							<form id="receiveForm" method="post" action="${pageContext.request.contextPath}/app/notice/notice!list.htm">
								<input type="hidden" name="start"/>
								<input type="hidden" name="order" value="${order}"/>
								<input type="hidden" name="lastOrder" value="${lastOrder}"/>
								<input type="hidden" name="asc" value="${asc}"/>
								<input type="hidden" name="filtrate" value="${filtrate}"/>
								<input type="hidden" name="isNote" value="${isNote}"/>
								<input type="hidden" name="busLocation" value="${busLocation}"/>
								<input type="hidden" name="busCategory" value="${busCategory}"/>
								<table cellpadding="0" cellspacing="0" style="border:0px;width:100%;">
									<tr>
										<td valign="middle" style="padding-left:5px;" colspan="2">
											<div align="left">
												名称/内容：
												<input class="shaixuaninput" name="title" type="text" value="${title}" style="width:120px"/>
												&nbsp;时间：
												<input id="startTime" name="startTime" readonly="readonly" type="text" value="${startTime}" style="width:90px" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endTime\')}',dateFmt:'yyyy-MM-dd'})"/>
												～
												<input id="endTime" name="endTime" readonly="readonly" type="text" value="${endTime}" style="width:90px" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd'})"/>
												&nbsp;创建人：
												<input class="mingzi shaixuaninput" name="issueName" type="text" value="${issueName}" style="width:100px"/>									
											    &nbsp;公告状态：
												<select name="pastEndTime" value="${pastEndTime}">												    
												    <s:if test="pastEndTime==0">
												       <option value="0" selected>全部</option>
												    </s:if>
												    <s:else>
												       <option value="0">全部</option>
												    </s:else>	
                                                    <s:if test="pastEndTime==1">
												       <option value="1" selected>未过期</option>
												    </s:if>
												    <s:else>
												       <option value="1">未过期</option>
												    </s:else>													
												    <s:if test="pastEndTime==2">
												       <option value="2" selected>已过期</option>
												    </s:if>
												    <s:else>
												       <option value="2">已过期</option>
												    </s:else>
												    <s:if test="pastEndTime==3">
												       <option value="3" selected>未生效</option>
												    </s:if>
												    <s:else>
												       <option value="3">未生效</option>
												    </s:else>																	    												    
												</select>
												<select name="pastReadTime" value="${pastReadTime}">
												    <s:if test="pastReadTime==0">
												        <option value="0" selected>全部</option>
												    </s:if>
												    <s:else>
												        <option value="0">全部</option>
												    </s:else>
												    <s:if test="pastReadTime==1">
												        <option value="1" selected>未读</option>
												    </s:if>
												    <s:else>
												        <option value="1">未读</option>
												    </s:else>
												    <s:if test="pastReadTime==2">
												        <option value="2" selected>已读</option>
												    </s:if>
												    <s:else>
												        <option value="2">已读</option>
												    </s:else>		
												</select>	
											</div>
										</td>								
									</tr>
									<tr>
										<td valign="middle" style="padding-left:5px;">
										    <div align="left">
										        公告分类：
											    <input id="blSelect" type="text" readonly="readonly" value="${busLocation}" style="width:180px;"/>
											    <div style="display:none;position: fixed;z-index:10002;background-color: white;line-height: 20px;border: 1px solid #C0C0C0; _position:absolute;">
											    	<span class="bc_btn" style="margin-left:110px;" id="btnBLOk">[确定]</span>
													<span class="bc_btn" id="btnBLCancel">[关闭]</span>
													<ul id="blTree" class="ztree" style="height:150px;overflow:auto;"></ul>
												</div>
											    <input id="bcSelect" type="text" readonly="readonly" value="${busCategory}" style="width:180px;"/>
											    <div style="display:none;position: fixed;z-index:10002;background-color: white;line-height: 20px;border: 1px solid #C0C0C0; _position:absolute;">
											    	<span class="bc_btn" style="margin-left:110px;" id="btnBCOk">[确定]</span>
													<span class="bc_btn" id="btnBCCancel">[关闭]</span>
													<ul id="bcTree" class="ztree" style="height:150px;overflow:auto;"></ul>
												</div>&nbsp;			
											</div>																						
										</td>
										<td style="width:240px;">
											<div align="right">
											    <input type="button" id="btnCZ" class="button button-primary button-rounded button-small" value="重置">
											    <input type="button" id="btnTJ" class="button button-primary button-rounded button-small" value="提交">
										    </div>
										</td> 
									</tr>						
								</table>
							</form>
						</td>
					</tr>
					
					<tr class="tdbg">
						<td><!-- <input type="checkbox" id="selAll" /> --></td>
						<td>名称</td>
						<td>创建时间</td>
						<td>创建人</td>
						<td>公告有效期</td>
						<td>地市</td>
						<td>业务类型</td>
						<td colspan="2">状态</td>
					</tr>
					<s:iterator value="receive" var="list">
						<s:if test="readTime==null">
							<tr class="no_read_tr">
								<td><!-- <input name="" type="checkbox" value="" /> --></td>
								<td style="text-align: left;padding-left: 5px;">
									<s:if test="notice.isTop==1">
										置顶&nbsp;&nbsp;
									</s:if>
									<a class="noticeTitle" title="${notice.title}" noticeId="${notice.id}" >
										<s:if test="notice.title.length()>23">
											<s:property value="notice.title.substring(0,22)+'...'" />
										</s:if>
										<s:else>
											${notice.title}
										</s:else>
									</a>
								</td>
								<td><s:date format="yyyy-MM-dd" name="notice.editTime" /></td>
								<td>${notice.issue.userInfo.userChineseName}</td>
								<td><s:date format="yyyy-MM-dd HH:mm" name="notice.endTime"/></td> 
								<td style="text-align:left;padding-left: 5px;" title="${notice.busLocation}">
								<s:if test="notice.busLocation.length()>9">
									<s:property value="notice.busLocation.substring(0,8)+'...'" />
								</s:if>
								<s:else>
									${notice.busLocation}
								</s:else>
								</td>
								<td style="text-align:left;padding-left: 5px;" title="${notice.busCategory}">
								<s:if test="notice.busCategory.length()>13">
									<s:property value="notice.busCategory.substring(0,12)+'...'" />
								</s:if>
								<s:else>
									${notice.busCategory}
								</s:else>	
								</td>							
								<td>
								    <s:if test="(new java.util.Date()).getTime() < notice.startTime.getTime()">
										未生效
									</s:if>
									<s:else>
										<s:if test="(new java.util.Date()).getTime() < notice.endTime.getTime()">
											未过期
										</s:if>
										<s:else>已过期</s:else>
									</s:else>						
								</td>
								<td>未读</td>
							</tr>
						</s:if>
						<s:else>
							<tr>
								<td><!-- <input name="" type="checkbox" value="" /> --></td>
								<td style="text-align: left;padding-left: 5px;">
									<s:if test="notice.isTop==1">
										<font color="red">置顶&nbsp;&nbsp;</font>
									</s:if>
									<a class="noticeTitle" title="${notice.title}" noticeId="${notice.id}">
										<s:if test="notice.title.length()>23">
											<s:property value="notice.title.substring(0,22)+'...'" />
										</s:if>
										<s:else>
											${notice.title}
										</s:else>
									</a>
								</td>
								<td><s:date format="yyyy-MM-dd" name="notice.editTime" /></td>
								<td>${notice.issue.userInfo.userChineseName}</td>
								<td><s:date format="yyyy-MM-dd HH:mm" name="notice.endTime"/></td>
								<td style="text-align:left;padding-left: 5px;" title="${notice.busLocation}">
								<s:if test="notice.busLocation.length()>9">
									<s:property value="notice.busLocation.substring(0,8)+'...'" />
								</s:if>
								<s:else>
									${notice.busLocation}
								</s:else>
								</td>
								<td style="text-align:left;padding-left: 5px;" title="${notice.busCategory}">
								<s:if test="notice.busCategory.length()>13">
									<s:property value="notice.busCategory.substring(0,12)+'...'" />
								</s:if>
								<s:else>
									${notice.busCategory}
								</s:else>		
								</td>							
								<td>
								    <s:if test="(new java.util.Date()).getTime() < notice.startTime.getTime()">
										未生效
									</s:if>
									<s:else>
										<s:if test="(new java.util.Date()).getTime() < notice.endTime.getTime()">
											未过期
										</s:if>
										<s:else>已过期</s:else>
									</s:else>									
								</td>
								<td>已读</td>
							</tr>	
						</s:else>
					</s:iterator>
					
					<s:if test="totalPage>1">
						<tr>
							<td colspan="9">
								<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}" currentPage="${currentPage}" totalPage="${totalPage}">
									<a href="javaScript:void(0);" onfocus="this.blur();">首页</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">&lt;上一页</a>
									<s:iterator value="new int[totalPage]" status="i">
										<s:if test="#i.index+1 == currentPage"> 
											<a href="javaScript:void(0);" class="dang" style="text-decoration:none;cursor:default;" ><s:property value="#i.index+1"/></a>
										</s:if>
										<s:elseif test="#i.index+6 > currentPage && #i.index-5 < currentPage" >
											<a href="javaScript:void(0);" onfocus="this.blur();"><s:property value="#i.index+1"/></a>
										</s:elseif>
									</s:iterator>
									
									<a href="javaScript:void(0);" onfocus="this.blur();">下一页&gt;</a>
									<a href="javaScript:void(0);" onfocus="this.blur();">尾页</a>
								</div>
							</td>
						</tr>
					</s:if>		
																						
				</table>
			</div>
		</div>
	</body>
</html>