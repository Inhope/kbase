<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>不同实例之间对比</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/comparison/css/css.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/comparison/css/shili.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/comparison/js/ComparisonObject.js"></script>
	</head>

	<body>
		<div class="content_right_bottom">
			<div class="shili">
				<div class="shili_titile">
					<div class="shili_titile_left">
						<a href="javascript:void(0);" onfocus="this.blur();">实例对比</a>
					</div>
					<div class="shili_titile_right">
						<ul>
							<li>
								<input name="show" type="radio" value="1" />
								显示所有属性
							</li>
							<li>
								<input name="show" type="radio" value="2" />
								显示共有属性
							</li>
						</ul>
					</div>
				</div>
				<div class="shili_con">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="kdu">
								&nbsp;
							</td>
							<s:iterator var="ol" value="#request.ooList" status="i">
								<td id="<s:property value="#ol.objectId"/>">
									<b title="<s:property value="#ol.name"/>">
										<s:if test="#request.ooList.size() == 2">
											<!-- 什么都不做 -->
											<s:property value="#ol.name"/>
										</s:if>
										<!-- 当有很多实例进行比对的时候，由于存放内容的格子比较小所以对显示的内容进行截取 -->
										<s:elseif test="#request.ooList.size() == 3">
											<s:if test="#ol.name.length() > 18">
												<s:property value="#ol.name.substring(0, 18) + '...'"/>
											</s:if>
											<s:else>
												<s:property value="#ol.name"/>
											</s:else>
										</s:elseif>
										<s:elseif test="#request.ooList.size() == 4">
											<s:if test="#ol.name.length() > 13">
												<s:property value="#ol.name.substring(0, 13) + '...'"/>
											</s:if>
											<s:else>
												<s:property value="#ol.name"/>
											</s:else>
										</s:elseif>	
										<s:elseif test="#request.ooList.size() == 5">
											<s:if test="#ol.name.length() > 10">
												<s:property value="#ol.name.substring(0, 10) + '...'"/>
											</s:if>
											<s:else>
												<s:property value="#ol.name"/>
											</s:else>
										</s:elseif>	
										<s:else>
											<s:property value="#ol.name"/>
										</s:else>
									</b>
									<p>
										<s:if test="#i.index == 0">
											<a style="color: gray;text-decoration: none;">置于首列</a>
										</s:if>
										<s:else>
											<a id="setIndex" href="javascript:void(0);">置于首列</a>
										</s:else>
										<a id="deleteCol" href="javascript:void(0);">删除</a>
									</p>
								</td>
							</s:iterator>
						</tr>
						
						<s:iterator var="al" value="#request.attrList">
							<tr id="<s:property value="#al.id"/>" private="<s:property value="#al.isPrivate"/>">
								<td class="kdu">
									<b title="<s:property value="#al.name"/>">
										<s:if test="#al.name.length() > 10">
											<s:property value="#al.name.substring(0, 9) + '...'"/>
										</s:if>
										<s:else>
											<s:property value="#al.name"/>
										</s:else>
									</b>
								</td>
								<s:iterator value="#request.ooList" status="i">
									<td>
										<s:property value="#al.values[#i.index].content"/>
									</td>
								</s:iterator>
								<%-- 非法比较相同的实例时，会导致显示有误 modify by eko.zhan at 2016-10-25 15:51
								<s:iterator var="alv" value="#al.values">
									<td>
										<s:property value="#alv.content"/>
									</td>
								</s:iterator>
								<s:set var="cols" value="%{#request.ooList.size - #al.values.size}"></s:set>
								<s:iterator value="%{new int[#cols]}">
									<td>
										&nbsp;
									</td>
								</s:iterator>--%>
							</tr>
						</s:iterator>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
