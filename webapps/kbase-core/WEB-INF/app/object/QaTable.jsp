<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>

<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库-二维表格</title>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/resource/object/js/QaTable.js"></script>
<script type="text/javascript">
	var swfAddress = '${swfAddress}/attachment/preview.do';
</script>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	margin: 0 auto;
	padding: 10px 0;
}

.outTable {
	overflow: auto;
	border-collapse: collapse;
	border: 0px solid #e3e3e3;
	font-size: 12px;
	width: 100%;
	border-collapse: collapse;
	width: 100%;
	border: 1;
}

.outTable td {
	border: 1px solid #e3e3e3;
	padding-left: 10px;
}

.outTable th {
	width: 15%;
	border: 1px solid #e3e3e3;
}

.outTable tr {
	height: 30px;
}

.outTable tr.lastrow td {
	border-bottom: 0;
}

.outTable tr td.lastCol {
	border-right: 0;
}

.content {
	width: 100%;
	margin: 10px 10px 0;
	width: auto;
}

.title {
	background: #f6f6f6 none repeat scroll 0 0;
	color: #a50e10;
	font-size: 14px;
	height: 34px;
	line-height: 34px;
	margin: 0 10px;
	padding-left: 10px;
	width: auto;
}

.title a {
	background: rgba(0, 0, 0, 0)
		url("../images/fankui/fankui_sanjiaoxing.jpg") no-repeat scroll left
		center;
	color: #333;
	font-weight: bold;
	margin-right: 12px;
	padding-left: 10px;
}

.ansTable {
	border-collapse: collapse;
	border: 0px solid #e3e3e3;
	font-size: 13px;
	width: 100%;
}

.ansTable td {
	width: 25%;
	border-top: 0;
	border-right: 1px solid #e3e3e3;
	border-bottom: 0;
	border-left: 0;
}

a {
	text-decoration: none;
}
</style>

	</head>

	<body>
		<div class="title">
			<span> </span>
		</div>
		<div class="content">
			<table class="outTable">
				<tr>
					<!--  <th>
						目录分类
					</th>-->
					<s:iterator value="#request.titles" var="title" status="t">
						<th>
							<s:property/>
						</th>
					</s:iterator>
					<th>附件预览</th>
				</tr>
				
				<s:iterator value="#request.qas" var="qa" status="u">
					<tr>
						<s:if test="#qa.categoryName != null">
							<td>
								<a class="categoryName" href="javascript:void(0);" title="${qa.categoryName}"
														id="${qa.categoryId}">${qa.categoryName}</a>
							</td>
						</s:if>
						<s:iterator value="#qa.params" var="ca">
							<td>
								<s:if test="#ca == ''">
									<font color = "red">无</font>
								</s:if>
								<s:else>
									${ca }
								</s:else>
								<!--<s:property escape="false"/>-->
							</td>
						</s:iterator>
						<s:if test="#qa.attachmentList.size > 0">
								<td>
								<div>
									<s:iterator value="#qa.attachmentList" var="attachment" status="a">
											<a class="showAttachment" title="预览 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">${attachment.name}</a>
											<s:if test="!#a.last">
												<br>
											</s:if>
									</s:iterator>
								</div>
								<div></div>
								</td>
						</s:if>
						<s:else>
							<td><font color = "red">无</font></td>
						</s:else>
					</tr>
				</s:iterator>
				<!--
				<s:iterator value="#request.qas" var="qa" status="u">
					<tr>

						<td>
							<table class="ansTable" height="100%;">
								<tr>
									<s:iterator value="#qa.ans" var="ca" status="a">
										<s:if test="#a.last">
											<td>
												<a href="javascript:void(0);" tilte="${ca.name}"
													id="${ca.id}">${ca.name}</a>
											</td>
											<s:if test="#ca.tag!=null">
												<td style="border-right: 0; width: auto;">${ca.tag}</td>
											</s:if>
										</s:if>
										<s:else>
											<td>
												<a href="javascript:void(0);" tilte="${ca.name}"
													id="${ca.id}">${ca.name}</a>
											</td>
											<s:if test="#ca.tag!=null">
												<td>${ca.tag}</td>
											</s:if>
										</s:else>
									</s:iterator>
								</tr>
							</table>
						</td>
					</tr>
				</s:iterator>
               --> 
				<!--<s:iterator value="#request.qas" var="qa" status="u">
					<tr>
						<td rowspan="<s:property value='#qa.ans.size()'/>">
							${qa.q }
						</td>
						<s:iterator value="#qa.ans" status="a">
							<s:if test="#a.first">
								<td>
									<s:property />
								</td>
							</s:if>
						</s:iterator>
					</tr>
					<s:iterator value="#qa.ans" status="a">
						<s:if test="!#a.first">
							<tr>
								<td>
									<s:property />
								</td>
							</tr>
						</s:if>
					</s:iterator>
				</s:iterator>-->
			</table>
		</div>
	</body>
</html>
