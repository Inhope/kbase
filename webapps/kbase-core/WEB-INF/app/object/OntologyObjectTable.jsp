<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/object.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<!-- 		日志记录 -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/object/js/OntologyObject.js"></script>
<script type="text/javascript">
var robot_context_path='${robot_context_path}';
var categoryId_current = '${id}';//当前分类id（积分策略用）
</script>
<style type="text/css">
table{
  border-collapse:collapse;
  border:0px solid #e3e3e3;
  font-size:12px;
  width: 100%;
}
table td{
  width:50%;
  border-top:0;
  border-right:1px solid #e3e3e3;
  border-bottom:1px solid #e3e3e3;
  border-left:0;
}
table tr.lastrow td{
  border-bottom:0;
}
table tr td.lastCol{
  border-right:0;
}
</style>
</head>

<body>



<!--******************内容开始***********************-->

<div class="content_right_bottom">
<div class="zhishi_titile">
		<s:if test="#request.endtime1_flag&&after_endtime1!=1">
			<p style="float:right;color:red;cursor:pointer;" after_endtime1="1">转历史</p>
		</s:if>
		<s:elseif test="#request.endtime1_flag&&after_endtime1==1">
			<p style="float:right;color:red;cursor:pointer;" after_endtime1="0">转正常</p>
		</s:elseif>
<span id="categoryNv">&nbsp;</span>

</div>

<div class="zhishi_con"  style="height: 450px;overflow-y: scroll;">
 <!-- 
<div class="zhishi_con_title">

<table>
<tr>
<td class="biaoti"><a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=objname&click=1&lastOrder=${lastOrder}&asc=${asc}"><b>名称</b></a></td>
	<td><a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=start&click=1&lastOrder=${lastOrder}&asc=${asc}">开始时间</a></td>
	<td><a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=end&click=1&lastOrder=${lastOrder}&asc=${asc}">结束时间</a></td>
	<td><a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=edit&click=1&lastOrder=${lastOrder}&asc=${asc}">修改时间555</a></td>
</tr>
</table>
</div>-->


<div class="zhishi_con_nr">
<ul>
<table>
<s:iterator value="#request.list" status="u">
	<s:if test="#u.odd">
		<tr>
	</s:if>
			<td>
				<li>
					<s:if test="objectId==null || objectId==''">
						<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
							<s:if test="topObject">
								<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/left_ico3.jpg" width="16" height="15" />
							</s:if>
							<s:else>
								<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg" width="16" height="15" />
							</s:else>
							<b style="cursor:pointer" categoryId="${id},${categoryId}"  title="${name}" _top="<s:property value='topObject'/>" _hasObj="${hasObj }">${name}
								<s:if test="#request.endtime1_flag&&after_endtime1==1"><span style="color:red;">(已过追溯期)</span></s:if>
							</b>
						</h1>
					</s:if>
					<s:else>
						<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
							<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
							<b style="cursor:pointer" categoryId="${id},${categoryId},${objectId}" title="${name}">
								${name}
								<s:if test="pastEnd">
									<span style="color:red">(未过追溯期)</span>
								</s:if>
							</b>
						</h1>
					</s:else>
				</li>
			</td>
	<s:if test="#u.odd && #u.last">
		<td></td></tr>
	</s:if>
	<s:if test="#u.even">
		</tr>
	</s:if>
<%-- 
<s:if test="#u.odd">
	<tr>
		<td><li>
			<s:if test="objectId==null || objectId==''">
				<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
					<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg" width="16" height="15" />
					<b style="cursor:pointer" categoryId="${id},${categoryId}"  title="${name}">${name}
						<s:if test="#request.endtime1_flag&&after_endtime1==1"><span style="color:red;">(已过追溯期)</span></s:if>
					</b>
				</h1>
			</s:if>
			<s:else>
				<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
					<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
					<b style="cursor:pointer" categoryId="${id},${categoryId},${objectId}" title="${name}">
						${name}
						<s:if test="pastEnd">
							<span style="color:red">(未过追溯期)</span>
						</s:if>
					</b>
				</h1>
			</s:else>
		</li></td>
	<s:if test="#u.last"><td></td></tr></s:if>	
</s:if>
<s:else>
		<td><li>
			<s:if test="objectId==null || objectId==''">
				<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
					<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg" width="16" height="15" />
					<b style="cursor:pointer" categoryId="${id},${categoryId}"  title="${name}">${name}
						<s:if test="#request.endtime1_flag&&after_endtime1==1"><span style="color:red;">(已过追溯期)</span></s:if>
					</b>
				</h1>
			</s:if>
			<s:else>
				<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;width: 100%;">
					<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
					<b style="cursor:pointer" categoryId="${id},${categoryId},${objectId}" title="${name}">
						${name}
						<s:if test="pastEnd">
							<span style="color:red">(未过追溯期)</span>
						</s:if>
					</b>
				</h1>
			</s:else>
		</li></td>
	</tr>
</s:else>

--%>




<s:if test="objectId==null || objectId==''">
	 <!-- <li>
	<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg" width="16" height="15" />
	<b style="cursor:pointer" categoryId="${id},${categoryId}"  title="${name}">${name}</b>
	</h1>
	</li>-->
</s:if>
<s:else>
	 <!-- <li>
	<h1 style="overflow: hidden; white-space: nowrap;text-overflow: ellipsis;"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
		<b style="cursor:pointer" categoryId="${id},${categoryId},${objectId}" title="${name}">
			${name}
			<s:if test="pastEnd">
				<span style="color:red">(未过追溯期)</span>
			</s:if>
		</b>
	</h1>
	<h2><s:date format="yyyy-MM-dd" name="startTime"/><s:if test="startTime==null">/</s:if></h2>
	<h3><s:date format="yyyy-MM-dd" name="endTime"/><s:if test="endTime==null">/</s:if></h3>
	<h4><s:date format="yyyy-MM-dd" name="editTime"/><s:if test="editTime==null">/</s:if></h4>
	</li>-->
</s:else>
</s:iterator>
</table>
</ul>
</div>
</div>
</div>




<!--******************内容结束***********************-->

</body>
</html>

