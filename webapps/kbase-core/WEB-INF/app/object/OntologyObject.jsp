<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/object.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/object/css/css.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!-- 		日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/object/js/OntologyObject.js"></script>

		<script type="text/javascript">
			var robot_context_path='${robot_context_path}';
			navigate_init('${robot_context_path}','${id}');
			var categoryId_current = '${id}';//当前分类id（积分策略用）
			
			/*分类下实例、文章、附件过滤显示*/
			$(function(){
				$('input[type="radio"][name="dataType"]').click(function(){
					var dataType = $(this).val();
					$('.zhishi_con_nr ul:eq(0) li').each(function(){
						if(dataType == $(this).attr('dataType') 
							|| dataType == 'all') {
							$(this).css({display: ''});
						} else {
							$(this).css({display: 'none'});
						}
					});
				});
				
				/*附件预览*/
				$('div.zhishi_con_nr ul li[dataType="attach"] h1 b').click(function() {
					var options = $(this).attr('options');
					if(options)  {
						options = eval('(' + '{' + options + '}' + ')');
						var fileId = options.id,
							fileName = options.name,
							fileType = options.type,
							categoryId = '${id}'; /*当前分类id*/
						var url = $.fn.getRootPath() + '/app/category/category-attach!preview.htm?fileId=' 
							+ fileId + '&fileName=' + fileName + '&fileType=' + fileType + '&categoryId=' + categoryId;
						var scrHeight = screen.height - 85, 
							scrWidth = screen.width - 20;
						var win = window.open(url , fileId, 
							'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
							+ scrWidth + ',height=' + scrHeight);
						win.focus();
					}
				});
			});
		</script>
	</head>

	<body>
		<!--******************内容开始***********************-->

		<div class="content_right_bottom">
			<div class="zhishi_titile" style="height: auto;">
				<span id="categoryNv">&nbsp;</span>
				<div style="width: 240px; float: right;">
					<input type="radio" name="dataType" value="all" checked="checked">
					全部 &nbsp;&nbsp;
					<input type="radio" name="dataType" value="object">
					实例 &nbsp;&nbsp;
					<kbs:if prop="version.code=wukong">
						<input type="radio" name="dataType" value="article">
						文章 &nbsp;&nbsp;
					</kbs:if>
					<s:if test="#request.attachs!=null && #request.attachs.size() > 0">
						<input type="radio" name="dataType" value="attach">
						附件 &nbsp;&nbsp;
					</s:if>
				</div>
			</div>

			<div class="zhishi_con">
				<div class="zhishi_con_title">
					<table>
						<tr>
							<td class="biaoti">
								<a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=objname&click=1&lastOrder=${lastOrder}&asc=${asc}">
									<b>名称</b>
								</a>
							</td>
							<td>
								<a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=start&click=1&lastOrder=${lastOrder}&asc=${asc}">
									开始时间</a>
							</td>
							<td>
								<a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=end&click=1&lastOrder=${lastOrder}&asc=${asc}">
									结束时间</a>
							</td>
							<td>
								<a href="${pageContext.request.contextPath}/app/object/ontology-object.htm?id=${id}&order=edit&click=1&lastOrder=${lastOrder}&asc=${asc}">
									修改时间</a>
							</td>
						</tr>
					</table>
				</div>


				<div class="zhishi_con_nr">
					<ul>
						<s:iterator value="#request.list" status='st' var="va">
							<%--分类数据 --%>
							<s:if test="objectId==null || objectId==''">
								<li dataType="category" 
									<s:if test="!#st.last || (#st.last && #request.attachs.size() > 0)">
										style="background: url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;"
									</s:if>>
									<h1 style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
										<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico2.jpg"
											width="16" height="15" />
										<b style="cursor: pointer" categoryId="${id},${categoryId}"
											title="${name}">${name}</b>
									</h1>
								</li>
							</s:if>
							<s:else>
								<%--实例、文章数据 --%>
								<li <s:if test="#va.bizTplArticleId != null && #va.bizTplArticleId != ''">
										dataType="article" </s:if><s:else>dataType="object"</s:else> 
									<s:if test="!#st.last || (#st.last && #request.attachs.size() > 0) ">
										style="background: url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;"
									</s:if>>
									<h1 style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
										<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg"
											width="16" height="15" />
										<b style="cursor: pointer"
											categoryId="${id},${categoryId},${objectId}" title="${name}"
											bizTplEnable="${bizTplEnable }"
											bizTplArticleId="${bizTplArticleId }"> ${name} <s:if
												test="pastEnd">
												<span style="color: red">(未过追溯期)</span>
											</s:if> </b>
									</h1>
									<h2>
										<s:date format="yyyy-MM-dd" name="startTime" />
										<s:if test="startTime==null">/</s:if>
									</h2>
									<h3>
										<s:date format="yyyy-MM-dd" name="endTime" />
										<s:if test="endTime==null">/</s:if>
									</h3>
									<h4>
										<s:date format="yyyy-MM-dd" name="editTime" />
										<s:if test="editTime==null">/</s:if>
									</h4>
								</li>
							</s:else>
						</s:iterator>
						<s:iterator value="#request.attachs" status="st" var="va">
							<%--附件数据 --%>
							<li dataType="attach"
								<s:if test="!#st.last">
									style="background: url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;"
								</s:if>>
								<h1 style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
									<s:if test="#va.type == 'docx' || #va.type == 'doc'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/Word.png"
												width="16" height="15" />
									</s:if>
									<s:elseif test="#va.type == 'xlsx' || #va.type == 'xls'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/Excel.png"
												width="16" height="15" />
									</s:elseif>
									<s:elseif test="#va.type == 'pptx' || #va.type == 'ppt'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/PowerPoint.png"
												width="16" height="15" />
									</s:elseif>
									<s:elseif test="#va.type == 'png' || #va.type == 'bmp' 
										|| #va.type == 'gif' || #va.type == 'jpeg'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/picture.png"
												width="16" height="15" />
									</s:elseif>
									<s:elseif test="#va.type == 'pdf'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/pdf.png"
												width="16" height="15" />
									</s:elseif>
									<s:elseif test="#va.type == 'html' || #va.type == 'htm'">
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/p4page.png"
												width="16" height="15" />
									</s:elseif>
									<s:else>
										<img src="${pageContext.request.contextPath}/resource/wukong/imgs/txt.png"
												width="16" height="15" />
									</s:else>
									<b style="cursor: pointer" options="id:'${id }',name:'${va.name }',type:'${type }'"> ${va.name } </b>
								</h1>
								<h2>
									/
								</h2>
								<h3>
									/
								</h3>
								<h4>
									/
								</h4>
							</li>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>




		<!--******************内容结束***********************-->

	</body>
</html>

