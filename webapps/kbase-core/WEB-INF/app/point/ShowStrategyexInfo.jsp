<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<input type="hidden" id="deuserbh" value="${deptuserbh }" />
<table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
	    <tr>
		  <td width="100%" height="50" >
		    策略<span id="rulecount">1</span>  
		  </td>
		</tr>
	  </table>
	
	      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="select_con" >
	            <tr>
				  <td width="10%"> <label class="control-label" >目录</label>
				  </td>
				  <td align="left">
                                  <div class="controls">
                                       <input type="text" id="categoryname" value="${categoryrule.remark }" onclick="choose_category();" readonly="readonly" style="margin-left:10px"/>
									   <input type="hidden" name="category_id"  value="CATALOG" />
									   <input type="hidden" id="categoryid"  name="categoryid" value="${categoryrule.ruleRange }"/>
									   <input type="hidden" id="categorybh"  name="categorybh" value="${categoryrule.ruleBh }"/>
                                  </div>
                                  <div id="categorycontent"  style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;margin-left:10px">
										<ul id="categorytree" class="ztree" style="margin-top:0;"></ul>
								   </div>
				  </td>
				  </tr>
				  <tr>
				    <td  width="10%"> <label class="control-label" for="inputSelect">部门</label>
					</td>
					<td align="left">
                                  <div class="controls">
                                        <input type="text" id="deptusername" value="${deptuserrule.remark }" onclick="choose_dept();" readonly="readonly" style="margin-left:10px"/>
										<input type="hidden" name="category_id" value="DEPT" />
										<input type="hidden" id="deptuserid" name="deptuserid" value="${deptuserrule.ruleRange }"/>
										<input type="hidden" id="deptuserbh" name="deptuserbh" value="${deptuserrule.ruleBh }"/>
                                  </div>
                                  <div id="deptusercontent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;margin-left:10px">
										<ul id="deptusertree" class="ztree" style="margin-top:0;"></ul>
								   </div>
				  </td>
				</tr>
	     </table>
		 
		 <div style="border-bottom:#ededed dotted 1px; margin-top:15px;"></div>
		 
		 <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto; ">
	    <tr>
		  <td>
		  
			    <div class="form_all">		
                                       <label for="name" class="label_title">积分配置</label>  
				</div>
			<div class="form_all">
				<table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="jf_pz">
				 	 <tr>
				    <th width="50%" align="left" >
					  积分项名称
					</th>
					<th width="50%"  align="left">
					  积分分值
					</th>
				  </tr>
				  <s:iterator value="#request.configlist" var="config">
				      <tr> 
					       <td>
							  ${config.pointsname }
							</td>
							<td>
							  <input type="hidden" name="configex_ids"  value="${config.id }"/>
							  <input type="text"  name="pointsex" placeholder="" value="${config.remark }"  class="input_kz_4" />  
							</td>
						  </tr> 
				  </s:iterator>
				  <tr>
					    <td  align="center" colspan="2" valign="middle" style="height:60px; border-top:#f5f5f5 solid 1px; padding-top:10px;">
	              
		                <a href="#" onclick="updatestrategyext('${id }');"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="resetstrategyext();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a>
					 
		            </td>
				  </tr>
				</table>
				</div>
		  </td>
		 </tr>
	  </table>
