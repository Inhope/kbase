<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<div class="layer_4">
<form id="editform">
<input type="hidden" value="${user.id}"  name="userid" />
<div class="layer_3_box">
     <div class="layer3_title">
	    <p> 积分编辑</p>   <img src="${pageContext.request.contextPath}/resource/point/images/cha_tc_05.gif" onMouseOver="this.src='${pageContext.request.contextPath}/resource/point/images/cha_tc_03.gif'" onMouseOut="this.src='${pageContext.request.contextPath}/resource/point/images/cha_tc_05.gif'" onclick="closediv('editinfo');"/>
	 </div>
	 <div  style="margin:15px;">
	      <div class="layer3_choose la_height_02">
		    <div class="range">
	          <label class="label_title"> 工号：</label> 
			  <div class="layer3_right_2">
                                            <p> ${user.userInfo.jobNumber }</p>
                                   </div>
			</div>
			<div class="range">  
			  <label class="label_title"> 姓名：</label>  
			   <div class="layer3_right_2">
			        
			          <p> ${user.userInfo.userChineseName } </p>  
			   </div>
			</div>   
			 
			  
	      </div>
		  
		  <div>
		      
				<table cellpadding="0" cellspacing="0" border="0"  width="100%" style="margin:0 auto;" class="jf_pz">
				 	 <tr>
				    <th width="50%" align="left" >
					  积分项名称
					</th>
					<th width="50%"  align="left">
					  积分分值
					</th>
				  </tr>
				  <s:iterator value="#request.configlist" var="config">
				     <tr>
					    <td>
						  ${config.pointsname }
						</td>
						<td>
						  <input type="hidden" value="${config.configKey}" name="configkey" />
						  <input type="text"  placeholder="" class="input_kz_4" value="${config.points }" name="points" /> 
						</td>
					  </tr>
				  </s:iterator>
				  
				  <tr>
					    <td  align="center" colspan="2" valign="middle" style="height:60px; border-top:#f5f5f5 solid 1px;">
	              
		                <a href="#" onclick="edituserpoints();"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_ss_03.gif"></a>&nbsp;<a href="#" onclick="editreset();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_ss_05.gif"></a>
					 
		            </td>
				  </tr>
				</table>
				
		  </div>
	 </div>
	 
 </div>
</form>
</div>