<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.module.point.vo.PConfigKey"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE >

<html>  
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">  <meta charset="utf-8" />   
<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resource/point/css/css3.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
<title>小i知识库积分</title>
<SCRIPT type="text/javascript">
       $(function() {
           $.fn.zTree.init($("#depttree"), setting);
       });
       
       var setting = {
			view: {
					expandSpeed: "fast"
			},
			async : {
				enable : true,
				url : $.fn.getRootPath()+"/app/point/p-strategy-set!getdepttree.htm",
				autoParam : ["id", "name=n", "level=lv","bh"]
			},
			check: {
				enable: true,
				chkStyle: "checkbox",
				chkboxType: { "Y": "s", "N": "s" }
			},
			callback: {
				onCheck: deptcheck
			}
			
		};
		function deptcheck(){
		    var zTree = $.fn.zTree.getZTreeObj("depttree");
			var nodes = zTree.getCheckedNodes(true);
			var name = "";
			var category_id = "";
			
			for (var i=0; i<nodes.length; i++) {
				name += nodes[i].name + ",";
				category_id +=  nodes[i].id + ",";
			}
			
			if (name.length > 0 ) name = name.substring(0, name.length-1);
			if (category_id.length > 0 ) category_id = category_id.substring(0, category_id.length-1);
			
			$("#depNames_select").attr("value", name);
			$("#depIds_select").attr("value", category_id);
		}
		
		function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			
			function pageClick(pageNo){
			    var _index = parent.layer.load("请稍候...");
			    $("#pageno").val(pageNo);
			    $("#forms").attr("action",$.fn.getRootPath()+'/app/point/p-strategy-set.htm');
			    $("#forms").submit();
			    parent.layer.close(_index);
			}
			
			function formReset(){
			　　var treeObj = $.fn.zTree.getZTreeObj("depttree");
			　　var nodes = treeObj.getCheckedNodes(true);
			　　for(var i=0;i<nodes.length;i++){
					treeObj.checkNode(nodes[i],false,false);
			　　}
			   $("#depNames_select").attr("value", "");
			   $("#depIds_select").attr("value", "");
			   $("input[id$='_select']").each(function(){
					$(this).attr("value","");
			   });
			   $("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
			   });
			}
			
			function showdetailinfo(id){
			   $("#loguserid").val(id);
			   $("#detailiframe").attr("src",$.fn.getRootPath() + "/app/point/p-log!finddetailinfo.htm?log.userid="+id);
			   openShade("detailpoints");
			}
			
			function openShade(shadeId){
				var w = ($('body').width() - $('#'+shadeId).width())/2;
				$('#'+shadeId).css('left',w+'px');
				$('#'+shadeId).css('top','170px');
				$('body').showShade();
				$('#'+shadeId).show();					
			}
			
			
			
			
			function toeditinfo(userid){
			   $.ajax({
			  		type: "POST",
			  		url : $.fn.getRootPath() + "/app/point/p-strategy-set!finduserinfo.htm",
			  		async: false,
			  		data:{"userid":userid},
			  		dataType : 'html',
			  		success: function(msg){
			  		   $("#editinfo").html(msg);
			  		   openShade("editinfo");
			  		}
			  });
			}
			
			function closediv(div){
			    $("#"+div+"").hide();
			    $('body').hideShade();	
			}
			
			function edituserpoints(){
			   var params=$("#editform").serialize();
			   $.ajax({
			  		type: "POST",
			  		url : $.fn.getRootPath() + "/app/point/p-strategy-set!updateuserpoints.htm?"+params,
			  		async: false,
			  		dataType : 'html',
			  		success: function(msg){
			  		   pageClick($("#pageNo").val());
			  		}
			  });
			}
			
			function exportdata(){
			   $.ajax({
			  		type: "POST",
			  		url : $.fn.getRootPath() + "/app/point/p-strategy-set!exportpointsdata.htm",
			  		async: false,
			  		dataType : 'json',
			  		success: function(data){
			  		  if(data.success) {
							var iframe = document.createElement("iframe");
				            iframe.src = $.fn.getRootPath()+"/app/point/p-strategy-set!downLoad.htm?fileName=" + data.message;
				            iframe.style.display = "none";
				            document.body.appendChild(iframe);
						}
			  		}
			  });
			
			}
			function exportselect(){
			   var params=$("#forms").serialize();
			   $.ajax({
			  		type: "POST",
			  		url : $.fn.getRootPath() + "/app/point/p-strategy-set!exportpointsdata.htm?"+params,
			  		async: false,
			  		dataType : 'json',
			  		success: function(data){
			  		  if(data.success) {
							var iframe = document.createElement("iframe");
				            iframe.src = $.fn.getRootPath()+"/app/point/p-strategy-set!downLoad.htm?fileName=" + data.message;
				            iframe.style.display = "none";
				            document.body.appendChild(iframe);
						}
			  		}
			  });
			
			}
			
			
			//重置编辑
			function editreset(){
			   $("input[name='points']").each(function(){
				    $(this).val(0)
			   });
			}
    
</SCRIPT> 
</head>
<body>
   <div class="mainr_con">
		<div class="mainr_box ">
		    <div class="jifen_jl">
			  
		       <table cellpadding="0" cellspacing="0" border="0" width="100%">
			        <tr>
				      <td nowrap="nowrap">
				       <form id="forms" method="post">
					    <ul class="btn_cz">
					      <li>
						    <ul>
							  <li>工号</li>
							  <input type="hidden" id="pageno" name="pageNo" value="" />
							  <li><input type="text"  placeholder="请输入工号" id="jobNumber_select" name="jobNumber" value="${jobNumber }" class="input_kz_5" style="width:120px;"/> </li>
							</ul>
						  </li>
						  <li>
						    <ul>
							  <li>姓名</li>
							  <li><input type="text"  placeholder="请输入姓名" id="username_select" name="userChineseName" value="${userChineseName }" class="input_kz_5" style="width:120px;"/> </li>
							</ul>
						  </li>
						   <li>
						    <ul>
							  <li>部门</li>
							  <li>
							    <input type="text" id="depNames_select"  placeholder="请输入部门" name="depNames" value="${depNames }" class="input_kz_5" style="width:120px;" readonly="readonly" onfocus="showMenu('deptcontent','depNames_select')"/>
							    <input id="depIds_select" type="hidden" name="depIds" value="${depIds }"/> 
							  </li>
							  
							</ul>
							
						  </li>
						  
						   <li>
						    <ul>
							  <li>角色</li>
							  <li>
							     <s:select id="roleId_select" list="#request.list_Role" name="roleId" headerKey="" headerValue="-请选择-" cssStyle="width: 150px;"
									listKey="id" listValue="name" value="#request.roleId"></s:select>
							     <!-- 
							     <s:select id="roleId_select" list="#request.list_Role" name="roleId" headerKey="" headerValue="-请选择-" listKey="id" listValue="name" value="#request.roleId"></s:select> 
							      --> 
							  </li>
							</ul>
						  </li>
						  <!-- 
						   <li>
						    <ul>
							  <li>岗位</li>
							  <li><input type="text"  placeholder="请输入岗位" class="input_kz_5" /> </li>
							</ul>
						  </li>
						   -->
						    <li>
						    <ul>
							  <li><a href="#" onclick="pageClick('1');"><img src="${pageContext.request.contextPath}/resource/point/images/btn_search_03.gif"></a></li>
							  <li><a href="#" onclick="formReset();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a></li>
							</ul>
						  </li>
					   </ul> 
					   </form>
					     <div id="editinfo" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			             </div>
			              					  </td>
				    </tr> 
				   <tr>
				     <td width="100%"  valign="top">
					    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_left" >
						   <tr>
						     <th>工号</th>
							 <th>姓名</th>
							 <th>岗位</th>
							 <th>角色</th>
							 <s:iterator value="#request.configlist" var="config">
							     <th>${config.pointsname }</th>
							 </s:iterator>
							 <th>操作</th>
						   </tr>
						   <s:iterator value="page.result" var="va" status="ind">
						      <tr <s:if test="#ind.index%2 != 0">class="color_gray"</s:if>>
						      <td>${va.userInfo.jobNumber }</td>
						      <td>${va.userInfo.userChineseName }</td>
						      <td>
						         <s:iterator value="#va.stationsStatus0" var="va1">
									<s:if test="#va1.id==#va.mainStationId">
										${va1.name }
									</s:if>											
								 </s:iterator>
							  </td>
							  <td>
									<s:iterator value="#va.roles" var="va1" status="st">
	    								${va1.name }<s:if test="!#st.last">,</s:if>
	    							</s:iterator>&nbsp;
								</td>
							  <s:iterator value="#request.configlist" var="config">
							  	<td>
							  	 <s:set name="score" value="0"></s:set>
							  	 <s:iterator value="#va.userPoints" var="va2">
							  	 	<s:if test="#config.configKey+''==#va2.key+''">
							  	 		<s:set name="score" value="#va2.value"></s:set>
							  	 	</s:if>
							  	 </s:iterator>
							  	 ${score}	
							     </td>
							  </s:iterator>
						      <td><a href="#" onclick="toeditinfo('${va.id}');" class="blue_b">编辑</a>&nbsp;&nbsp;<a href="#" onclick="showdetailinfo('${va.id }');" class="blue_b">积分明细</a></td>
						      </tr>
						   </s:iterator>
						</table>
					 </td>
					
				   </tr>
				   <tr class="trfen">
						<td colspan="11">
							<jsp:include page="../util/part_fenye.jsp"></jsp:include>
						</td>
					</tr>
			  </table> 
		  </div>
			 
			 <div style="border-bottom:#cccccc solid 1px; margin-bottom:20px;"></div>
			  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:30px;">
			   <tr>
				 <td  align="center" colspan="2" valign="middle" style="height:60px;">
								              
			     <a href="#" onclick="exportdata();"> <img src="${pageContext.request.contextPath}/resource/point/images/btndc_03.gif"></a>&nbsp;<a href="#" onclick="exportselect();"><img src="${pageContext.request.contextPath}/resource/point/images/btndc_05.gif"></a></td>
			   </tr>
			   </table>
		</div>
   </div> 
   <div id="deptcontent"  style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;">
		<ul id="depttree" class="ztree" style="margin-top:0;"></ul>
   </div>
   <div id="detailpoints" style="display:none;position:fixed;z-index:10001;_position:absolute;">
        <iframe id="detailiframe" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" width="765" height="1000"></iframe>
   </div>
</body>
</html>
