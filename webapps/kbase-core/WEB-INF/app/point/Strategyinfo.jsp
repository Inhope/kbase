<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">  <meta charset="utf-8" />   
<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resource/point/css/css3.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
<title>小i知识库积分</title>
<SCRIPT type="text/javascript">
       var strategyext_id="";
       $(function() {
       	   $('input[name="pstrategy.actCyc"]').each(function(){
	           if($(this).val()=='5'){
	           		$(this).attr("checked","checked");
	           }
	       })
           $.fn.zTree.init($("#categorytree"), setting);
           $.fn.zTree.init($("#deptusertree"), setting1);
       });
  
       
       
  	   var setting = {
			view: {
					expandSpeed: "fast"
			},
			async : {
				enable : true,
				url : $.fn.getRootPath()+"/app/point/p-strategy-ext!getCategorytree.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam:{"strategyext_id":strategyext_id},
				dataFilter: categoryDataFilter
			},
			check: {
				enable: true,
				chkStyle: "checkbox",
				chkboxType: { "Y": "p", "N": "p" }
			},
			callback: {
				onCheck: categorycheck									
			}
		};
		
		var setting1 = {
			view: {
					expandSpeed: "fast"
			},
			async : {
				enable : true,
				url : $.fn.getRootPath()+"/app/point/p-strategy-ext!getdeptusertree.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam:{"strategyext_id":strategyext_id},
				dataFilter: deptuserDataFilter
			},
			check: {
				enable: true,
				chkStyle: "checkbox",
				chkboxType: { "Y": "p", "N": "p" }
			},
			callback: {
				onCheck: deptusercheck
			}
			
		};
		
		function categoryDataFilter(treeId, parentNode, responseData) {
			var category_id = $("#categoryid").val()+",";
			var bh=$("#categorybh").val()+",";
		    if (responseData.length > 0) {
			        for(var i =0; i < responseData.length;i++) {
			        	if(bh.length>0 && bh.indexOf(responseData[i].bh)>-1)
			        		responseData[i].halfCheck = true;
			        	if(category_id.length>0 && category_id.indexOf(responseData[i].id+",")>-1){
			        		responseData[i].checked = true;
			        		responseData[i].halfCheck = false;
			        	}
			        		
			        }
			 }
		    return responseData;
		}				
		
		
		function deptuserDataFilter(treeId, parentNode, responseData) {
		    var category_id = $("#deptuserid").val()+",";
			var bh=$("#deptuserbh").val()+",";
		    if (responseData.length > 0) {
			        for(var i =0; i < responseData.length;i++) {
			        	if(bh.length>0 && bh.indexOf(responseData[i].bh)>-1 && responseData[i].bh.indexOf("P")<=-1)
			        		responseData[i].halfCheck = true;
			        	if(category_id.length>0 && category_id.indexOf(responseData[i].id+",")>-1){
			        		responseData[i].checked = true;
			        		responseData[i].halfCheck = false;
			        	}
			        }
			 }
		    return responseData;
		}
		
		
		function categorycheck(e,treeId,treeNode){
		    var zTree = $.fn.zTree.getZTreeObj("categorytree");
			var nodes = zTree.getCheckedNodes(true);
			var name = $("#categoryname").val()==''?'':$("#categoryname").val()+",";
			var category_id = $("#categoryid").val()==''?'':$("#categoryid").val()+",";
			var bh=$("#categorybh").val()==''?'':$("#categorybh").val()+",";
		    
		    if(category_id.indexOf(treeNode.id)>-1){
		       name=name.replace(treeNode.name+",","");
		       category_id=category_id.replace(treeNode.id+",","");
		       bh=bh.replace(treeNode.bh+",","");
		    }else{
		       name += treeNode.name + ",";
			   category_id +=  treeNode.id + ",";
			   bh += treeNode.bh + ",";
		    }
				
			if (name.length > 0 ) name = name.substring(0, name.length-1);
			if (category_id.length > 0 ) category_id = category_id.substring(0, category_id.length-1);
			if (bh.length > 0 ) bh = bh.substring(0, bh.length-1);
			
			$("#categoryname").attr("value", name);
			$("#categoryid").attr("value", category_id);
			$("#categorybh").attr("value", bh);
		}
		
		function deptusercheck(e,treeId,treeNode){
		    var zTree = $.fn.zTree.getZTreeObj("deptusertree");
			var nodes = zTree.getCheckedNodes(true);
			var name = $("#deptusername").val()==''?'':$("#deptusername").val()+",";
			var category_id = $("#deptuserid").val()==''?'':$("#deptuserid").val()+",";
			var bh=$("#deptuserbh").val()==''?'':$("#deptuserbh").val()+",";
		    
		    if(category_id.indexOf(treeNode.id)>-1){
		       name=name.replace(treeNode.name+",","");
		       category_id=category_id.replace(treeNode.id+",","");
		       bh=bh.replace(treeNode.bh+",","");
		    }else{
		       name += treeNode.name + ",";
			   category_id +=  treeNode.id + ",";
			   bh += treeNode.bh + ",";
		    }
			
			if (name.length > 0 ) name = name.substring(0, name.length-1);
			if (category_id.length > 0 ) category_id = category_id.substring(0, category_id.length-1);
			if (bh.length > 0 ) bh = bh.substring(0, bh.length-1);
			
			$("#deptusername").attr("value", name);
			$("#deptuserid").attr("value", category_id);
			$("#deptuserbh").attr("value", bh);
		}
  
  function savestrategyInfo(){
     var point=true;
     $("input[name='points']").each(function(){
		 if($(this).val()!=''){
		   point=false;
		   return;
		 }
	 });
     
     if($("#actName").val()==''){
        parent.layer.alert("策略名称不能为空!", -1);
        return false;
     }if($("#act_id").val()==''){
        parent.layer.alert("动作资源不能为空!", -1);
        return false;
     }if(point){
      　　 parent.layer.alert("至少填一个积分分值!", -1);
        return false;
     }else{
       var params = $("#forms").serialize();
       $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy!savePStrategy.htm?"+params,
	   		async: false,
	   		dataType : 'json',
	   		success: function(data){
	   		   if(data.rst){
	   		   		parent.layer.alert(data.msg, -1);
	   		        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
	   		   }else{
	   		   		parent.layer.alert(data.msg, -1);
	   		   }
	   		}
	  });
     }
     			
  }
  
  function showresource(){
      $("#resources").toggle();
      $("body").bind("mousedown", function(event){
			if (!(event.target.id == 'resources' || $(event.target).parents("#resources").length>0)){
			   $("#resources").hide();
			}
	   });
  }
  
  // 追加对动作资源的判断，使其不能是重复
  function selectResource(key,resourcename,id){
      $("#resources").hide();
      //获取action传递的list值，String类型
      var actDescList = $("#actDescHidden").val();
      actDescList = actDescList.substring(1,actDescList.length-1);//去掉前后的"["和"]"
      var actDescArr = new Array();//动作资源名称数组
      actDescArr = actDescList.split(", ");
      var exist = $.inArray(resourcename, actDescArr);
      if(exist > -1){
            parent.layer.alert("动作资源有重复，请修改!", -1);
            return false;
        }else{
            $("#act_key").val(key);
            $("#act_id").val(id);
            $("#actdesc").val(resourcename);
            }
  }
  
  function savecircle(){
     var circle_type=$('input[name="pstrategy.actCyc"]:checked').val();
     var label = $("#label_"+circle_type+"").html();
     var starttime = $('#starttime').val();
     var endtime = $('#endtime').val();
     if($("#maxtimes_"+circle_type+"").length>0){
        var maxtimes = $("#maxtimes_"+circle_type+"").val()==''?0:$("#maxtimes_"+circle_type+"").val();
        $("#acttimes").val(maxtimes);
        $("#circleconfig").html("每 <span class='red'>"+label+"</span> 奖励次数 <span class='red'>"+maxtimes+"</span> 次<br />有效期 <span class='red'>"+starttime+"</span> 到 <span class='red'>"+endtime+"</span><br />");
     }else{
         $("#circleconfig").html(""+label+"<br />有效期 <span class='red'>"+starttime+"</span> 到 <span class='red'>"+endtime+"</span><br />");
     }
     $("#configinfo").show();
     $("#detailInfo").hide();
     $("#detailbutton").hide();
  
  }
  function tomodicircle(){
     $("#circleconfig").html("");
     $("#configinfo").hide();
     $("#detailInfo").show();
     $("#detailbutton").show();
  }
  
  function stopstrategyinfo(){
       var check =$("input:checkbox[name='strategy_id']:checked");
       if(check.length>1 || check.length<=0){
          parent.layer.alert("请选择一列!", -1);
          return false;
       }if($(check).attr("status")==1){
          parent.layer.alert("请选择启用项!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要停用选中的积分策略吗？", function(index){
      　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-strategy!stopPStrategy.htm',
			    data:{"strategyid":check.val()},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
					if(data.rst){
						parent.layer.close(index);
		   		   		parent.layer.alert(data.msg, -1);
		   		        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
		   		    }else{
		   		   		parent.layer.alert(data.msg, -1);
		   		    }
				}	
			});
		});	　　
    }
    
     function startstrategyinfo(){
       var check =$("input:checkbox[name='strategy_id']:checked");
       if(check.length>1 || check.length<=0){
          parent.layer.alert("请选择一列!", -1);
          return false;
       }if($(check).attr("status")==0){
          parent.layer.alert("请选择未启用项!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要启用选中的积分策略吗？", function(index){
      　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-strategy!startPStrategy.htm',
			    data:{"strategyid":check.val()},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
				   if(data.rst){
						parent.layer.close(index);
		   		   		parent.layer.alert(data.msg, -1);
		   		        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
		   		   }else{
		   		   		parent.layer.alert(data.msg, -1);
		   		   }
				}	
			});
		});
    }
    
    
     function delstrategynfo(){
      var strategyids = "";
	   $("input:checkbox[name='strategy_id']:checked").each(function(){
		  strategyids += $(this).val()+",";
	   });
       if(strategyids==''){
          parent.layer.alert("请至少选择一列!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要删除选中的积分策略吗？", function(index){
       　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-strategy!delPStrategyinfo.htm',
			    data:{"strategyids":strategyids.substring(0,strategyids.length-1)},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
				   if(data.rst){
						parent.layer.close(index);
		   		   		parent.layer.alert(data.msg, -1);
		   		        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
		   		   }else{
		   		   		parent.layer.alert(data.msg, -1);
		   		   }
				}	
			});
		})	　　
    }
    function definedset(){
       if($("#actName").val()==''){
          parent.layer.alert("请填写积分名称!", -1);
       }else{
         $("#definedset").show();
         $("#stategyset").hide();
       }
    
    }
    
    function back(){
       $("#definedset").hide();
       $("#stategyset").show();
    }
    
    function savestrategyex(){
     var category = $("#categoryid").val();
     var deptuser = $("#deptuserid").val();
     /**
     if(category ==''){
       parent.layer.alert("请选取目录!", -1);
       return false;
     }if(deptuser ==''){
       parent.layer.alert("请选取部门或人员!", -1);
       return false;
     }**/
     if(category =='' && deptuser ==''){
       parent.layer.alert("请至少选择目录或部门!", -1);
       return false;
     }
     else{
        var params = $("#forms").serialize();
	    $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy-ext!savePStrategyExt.htm?"+params,
	   		async: false,
	   		dataType : 'json',
	   		success: function(data){
	   		    if(data.rst){
					parent.layer.alert("保存成功",-1);
		   		    if($("#pstrategyid").length<=0){
		   		      window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
		   		    }  
		   		    showdetailinfo(data.info);
		   		    definedset();
		   		}else{
		   		   parent.layer.alert(data.msg,-1);
		   		}  
	   		}
	   });
      
     }
        
    }
    
    function showdetailinfo(id){
       $("#infotable tr").removeClass();
       $("#"+id+"").addClass("color_gray");
       $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy!findStrategyinfo.htm",
	   		async: false,
	   		data:{"id":id},
	   		dataType : 'html',
	   		success: function(msg){
	   		    $("#addinfo").html(msg);
	   		}
	  }); 
    
    
    }
    
    function showstrategyext(id,rule){
       $("#detailstrategyext").html("");
       $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy-ext!findStrategyextinfo.htm",
	   		async: false,
	   		data:{"id":id},
	   		dataType : 'html',
	   		success: function(msg){
	   		   $("#detailstrategyext").html(msg);
	   		   setting.async.otherParam = {"strategyext_id":id};
	   		   setting1.async.otherParam = {"strategyext_id":id};
	   		   $.fn.zTree.init($("#categorytree"), setting);
               $.fn.zTree.init($("#deptusertree"), setting1);
	   		}
	   });
	  $("#rulecount").html(rule); 
	   
    }
    
    function delStrategyext(id,trid){
       parent.layer.confirm("确定要删除该自定义策略?", function(){
	       $.ajax({
		   		type: "POST",
		   		url : $.fn.getRootPath() + "/app/point/p-strategy-ext!delStrategyext.htm",
		   		async: false,
		   		data:{"id":id},
		   		dataType : 'json',
		   		success: function(data){
		   		   if(data.rst){
					   parent.layer.alert(data.msg,-1);
			   		   $("#detailstrategyext").html("");
			   		   $("#"+trid+"").remove();
		   		   }else{
		   		   		parent.layer.alert(data.msg,-1);
		   		   }
		   		   
		   		}
		   });
    　　});　
    }
    
    //选取目录
    function choose_category(){
    	$.kbase.picker.categoryselect({returnField:"categoryname|categoryid|categorybh"});
       /**
       $("#categorycontent").show();
       $("body").bind("mousedown", function(event){
			if (!(event.target.id == 'categorycontent' || $(event.target).parents("#categorycontent").length>0)){
			  $("#categorycontent").fadeOut("fast");
			}
	   });
	   **/
    }
    
    //选取部门人员
    function choose_dept(){
       $.kbase.picker.deptmultiselect({returnField:"deptusername|deptuserid|deptuserbh"});
       /**
       $("#deptusercontent").show();
       $("body").bind("mousedown", function(event){
			if (!(event.target.id == 'deptusercontent' || $(event.target).parents("#deptusercontent").length>0)){
			   $("#deptusercontent").hide();
			}
	   });
	   **/
    }
    
    function toaddstrategyext(id){
        var length=$("#ruletable tr").length;
        $("#detailstrategyext").html("");
        $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy-ext!toaddstrategyext.htm",
	   		async: false,
	   		dataType : 'html',
	   		success: function(msg){
	   		   $("#detailstrategyext").html(msg);
	   		   $("#pstrategy_id").val(id);
	   		   setting.async.otherParam = {"strategyext_id":""};
	   		   setting1.async.otherParam = {"strategyext_id":""};
	   		   $.fn.zTree.init($("#categorytree"), setting);
               $.fn.zTree.init($("#deptusertree"), setting1);
	   		}
	   });
	   $("#rulecount").html(length);
    }
    
    function updatestrategyext(id){
       var params = $("#forms").serialize();
       $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy-ext!updateStrategyExt.htm?"+params,
	   		async: false,
	   		data:{"pstrategyExt.id":id},
	   		dataType : 'json',
	   		success: function(data){
	   		　　 if(data.rst){
					parent.layer.alert("更新成功",-1);
		   		    showdetailinfo(data.info);
		   		    definedset();
		   		}else{
		   		   parent.layer.alert(data.msg,-1);
		   		}    
	   		}
	  });
    }
    
    function updatestrategy(){
       var params = $("#forms").serialize();
       $.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/point/p-strategy!updatePStrategy.htm?"+params,
	   		async: false,
	   		dataType : 'json',
	   		success: function(data){
	   		   if(data.rst){
	   		   		parent.layer.alert(data.msg, -1);
	   		        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
	   		   }else{
	   		   		parent.layer.alert(data.msg, -1);
	   		   }
	   		}
	  });
    }
    
    function toaddstrategy(){
        window.location.href=$.fn.getRootPath() + "/app/point/p-strategy.htm";
    }
    
    function resetcircle(){
       $('input[name="maxtimes"]').each(function(){
           $(this).val("");
       })
       $('input[name="pstrategy.actCyc"]').each(function(){
           $(this).removeAttr("checked");
           if($(this).val()=='5'){
           		$(this).attr("checked","checked");
           }
       })
       $("#starttime").val("");
       $("#endtime").val("");
    }
    
    function resetstrategyext(){
        $('input[name="pointsex"]').each(function(){
           $(this).val("");
        })
        $('.controls input').each(function(){
           $(this).val("");
        })
        setting.async.otherParam = {"strategyext_id":""};
	    setting1.async.otherParam = {"strategyext_id":""};
	    $.fn.zTree.init($("#categorytree"), setting);
        $.fn.zTree.init($("#deptusertree"), setting1);
    }
    function checkall(obj){
    	var checked = $(obj).attr('checked');
		if(checked == 'checked')
			$('input[type="checkbox"][name="strategy_id"]').attr('checked', checked);
		else $('input[type="checkbox"][name="strategy_id"]').removeAttr('checked', checked);
    }
    // 新增对策略名称的判断函数
    function strategyCheck(){
        //获取action传递的list值，String类型
        var actNameList = $("#actNameHidden").val();
        actNameList = actNameList.substring(1,actNameList.length-1);//去掉前后的"["和"]"
        var actNameArr = new Array();//策略名称数组
        actNameArr = actNameList.split(", ");
        var actName = $("#actName").val();//获取新建的策略名称
        var exist = $.inArray(actName, actNameArr);
        if(exist > -1){
            parent.layer.alert("策略名称有重复，请修改!", -1);
            return false;
        }else{
            }
    }
    
</SCRIPT> 
</head>
<body>
<div class="mainr_box jifen_jl">
     <table cellpadding="0" cellspacing="0" border="0" width="100%" >
	       <tr>
		     <td colspan="3">
			   <ul class="btn_cz">
			      <li><a href="#" onclick="toaddstrategy();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_03.gif" /></a></li>
				  <li><a href="#" onclick="stopstrategyinfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_05.gif" /></a></li>
				  <li><a href="#" onclick="startstrategyinfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_18.gif" /></a></li>
				  <li><a href="#" onclick="delstrategynfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_07.gif" /></a></li>
			   </ul>
			 </td>
		   </tr> 
		   <tr>
		     <td width="70%"  valign="top">
			    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_left" id="infotable" >
				   <tr>
				     <th><input name="" type="checkbox" value="" onclick="checkall(this);"/></th>
					 <th>策略名称</th>
					 <th>周期</th>
					 <th>奖励次数</th>
					 <th>状态</th>
				   </tr>
				   <s:iterator value="#request.strategylist" var="strategy" status="ind">
					   <tr  id="${strategy.id}" onclick="showdetailinfo('${strategy.id}')" style="cursor: pointer;">
					     <td><input name="strategy_id" type="checkbox" status="${strategy.status }" value="${strategy.id}" /></td>
						 <td>${strategy.actName }</td>
						 <td>
						  <s:iterator value="#request.cycletype" var="cyctype">
						     <s:if test="#request.cyctype.key == #request.strategy.actCyc">${cyctype.label}</s:if>
						  </s:iterator>
						 </td>
						 <td>${strategy.actCyc=='5'?'不限':(strategy.actTimes) }</td>
						 <td><s:if test="#strategy.status == 1">未启用</s:if><s:if test="#strategy.status == 0">启用</s:if></td>
					   </tr>
				   </s:iterator>
				</table>
			 </td>
			 <td width="2%"></td>
			
			 <td width="28%" valign="top">
			 <form action="" method="post" id="forms">
			     <div id="addinfo">
			     
			     <div class="table_c" id="stategyset">
				      <div class="jf_r_title"> 
				           <img src="${pageContext.request.contextPath}/resource/point/images/icon_06.gif" /> 新建策略
					  </div>
					  <div class="table_b">
					      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
						    <tr>
							  <td width="100%">
                                     <fieldset>  
								    <div class="form_all">		
                                            <label for="name" class="label_title">策略名称:</label> 
                                            
									     <div class="form_r"><input type="text" id="actName" name="pstrategy.actName"  placeholder="请输入策略名称" class="input_kz" onchange="strategyCheck();" />
									                         <input type="hidden" id="actNameHidden" name="actNameHidden" value="${actNameList}" />  
									     </div>
									</div>
									
									<div class="form_all">		
                                            <label for="name" class="label_title">动作资源:</label> 
								      <div class="form_r">
								           <input  type="hidden" class="input_kz_5" name="pstrategy.act.actKey" id="act_key" value="" placeholder="请输入动作资源" />
								           <input  type="hidden" class="input_kz_5" name="pstrategy.act.id" id="act_id" value="" placeholder="请输入动作资源" />
									       <input  type="text" class="input_kz_5" name="pstrategy.act.actDesc" readonly="readonly" id="actdesc"  placeholder="请输入动作资源" />
									       <input type="hidden" id="actDescHidden" name="actDescHidden" value="${actDescList}" />
									       <div class="btn_browse"><a href="#" onclick="showresource();">浏览</a></div>
										   
								           <div class="layer_2" id="resources" style="display: none">
										       <div class="layer_2_box">
											      <ul>
												  <li>
												    <ul>
													  <li class="lp_01">&nbsp;</li>
													  <li class="lp_02">序号</li>
													  <li class="lp_03">资源</li>
													</ul>
												  </li>
												  <s:iterator value="#request.actlist" var="act" status="va">
												      <li>
													    <ul>
														  <li class="lp_01"><input name="act_key" type="radio" value="" onclick="selectResource('${act.actKey }','${act.actDesc }','${act.id }');" /></li>
														  <li class="lp_02">0${va.index+1 }</li>
														  <li title="${act.actDesc }"  class="lp_03">${act.actDesc }</li>
														</ul>
													   </li>
												  </s:iterator>
												  <li><a href="#"></a></li>
												  </ul>
											   </div>
										   </div>
								      </div>
									</div>
									
									<div class="form_all" id="configinfo" style="display: none">		
                                            <label for="name" class="label_title">周期设置:</label> 
										     <div class="form_r">
										     　<div id="circleconfig"></div>
											   <a href="#" onclick="tomodicircle();">修改</a>
											 </div>
									</div>
									
								 <div id="detailInfo">
									<div class="form_all">		
                                            <label for="name" class="label_title">周期设置:</label> 
									     <input type="hidden" id="acttimes" name="pstrategy.actTimes" value="0" />
									     <div class="form_r">
									     <s:iterator value="#request.cycletype" var="cyctype">
									         <s:if test="#request.cyctype.label=='一次' || #request.cyctype.label=='不限'">
												    <label class="radio"><input name="pstrategy.actCyc" type="radio" value="${cyctype.key}"/>&nbsp; <span id="label_${cyctype.key}">${cyctype.label}</span></label>
									         </s:if>
									         <s:else>
									            <div class="hang_cycle">
                                                     <label class="radio">
                                                           <input name="pstrategy.actCyc" type="radio"  value="${cyctype.key}" />&nbsp; 每<span id="label_${cyctype.key}">${cyctype.label}</span></label>
                                                           <input type="text" id="maxtimes_${cyctype.key}" name="maxtimes" placeholder="最多奖励次数" class="input_kz_2" style="float:right;padding:0 0 0 1px;width:53%;margin-top:5px"/> 
											    </div>
									         </s:else>
									     </s:iterator>
									 </div>
									 </div>
									<div class="form_all">		
                                            <label for="name" class="label_title"></label> 
									     <div class="form_r" style="width:100%"> 	
										      有效期 <br />
										      <input type="text" id="starttime"  placeholder="日期" class="input_kz_3" name="pstrategy.cycFromDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" style="width:40%"/> 到 <input type="text" id="endtime" name="pstrategy.cycEndDate" placeholder="日期" class="input_kz_3" onclick="WdatePicker({minDate:'#F{$dp.$D(\'starttime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly"  style="width:40%"/>
									     </div>
									</div>
                                  </fieldset>  	
                                </td>
							</tr>
							<tr id="detailbutton">
							  
							  <td height="60" align="center">
							     
							      <a href="#" onclick="savecircle()";> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="resetcircle();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a>									  </td>
							</tr>
						  </table>
						  <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
						    <tr>
							  <td>
								    <div class="form_all">		
                                            <label for="name" class="label_title">积分配置</label>  
									</div>
								<div class="form_all">
									<table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="jf_pz">
									 	 <tr>
									    <th width="50%" align="left" >
										  积分项名称
										</th>
										<th width="50%"  align="left">
										  积分分值
										</th>
										<s:iterator value="#request.configlist" var="config">
										  <tr>
										    <td>
											   ${config.pointsname }
											</td>
											<td>
											  <input type="hidden" name="config_ids" value="${config.id }" />
											  <input type="text" name="points" id="points" class="input_kz_4" /> 
											</td>
										  </tr>
										
										</s:iterator>
									  </tr>
									</table>
									
								</div>
							  </td>
							  
						    </tr>
						  </table>
						  <table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto;" >
						    <tr>
							    <td  align="center" colspan="2" valign="middle" style="height:60px; border-top:#f5f5f5 solid 1px;">
				                   <a href="#" onclick="savestrategyInfo();"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="definedset();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_17.gif"></a>
				                </td>
				           
						   </tr>
						  </table>
					  </div>
				 </div>
				 <div class="table_c" id="definedset" style="display: none">
				      <div class="jf_r_title"> 
				            <img src="${pageContext.request.contextPath}/resource/point/images/icon_06.gif" />新建自定义策略
				            <span onclick="back();" style="cursor:pointer"><img src="${pageContext.request.contextPath}/resource/point/images/icon_19.gif" /></span>
					  </div>
					  <div class="table_b">
					      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
						    <tr>
							  <td width="100%" height="50" style="color:#000000;">
							    自定义策略
							  </td>
							  <td>
							    <a href="#"><img src="${pageContext.request.contextPath}/resource/point/images/add.gif"></a> 
							  </td>
							</tr>
							
						  </table>
						
						      <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_left ">
							    <div class="table_right">
						          <tr>
					                 <th>名称</th>
					                 <th>类型</th>
					                 <th>操作</th>
				                 </tr>
								  
						     </table>
						 
						  
						  
					  </div>
					  
					  
					   <div class="table_b" id="toaddrules">
					      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
						    <tr>
							  <td width="100%" height="50" >
							    策略<span id="rulecount">1</span>  
							  </td>
							</tr>
							
						  </table>
						
						      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="select_con" >
							   
						            <tr>
									  <td width="10%"> <label class="control-label" >目录</label>
									  </td>
									  <td align="left">
                                                     <div class="controls">
                                                        <input type="text" id="categoryname" onclick="choose_category();" readonly="readonly"/>
												  <input type="hidden" name="category_id"  value="CATALOG" />
												  <input type="hidden" id="categoryid"  name="categoryid"/>
												  <input type="hidden" id="categorybh"  name="categorybh"/>
                                                     </div>
                                                     <div id="categorycontent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;">
													<ul id="categorytree" class="ztree" style="margin-top:0;"></ul>
											   </div>
									  </td>
									  </tr>
									  <tr>
									    <td  width="10%"> <label class="control-label" for="inputSelect">部门</label>
										</td>
										<td align="left">
                                                     <div class="controls">
                                                        <input type="text" id="deptusername" onclick="choose_dept();" readonly="readonly"/>
												  <input type="hidden" name="category_id" value="DEPT" />
												  <input type="hidden" id="deptuserid" name="deptuserid" />
												  <input type="hidden" id="deptuserbh" name="deptuserbh"/>
                                                     </div>
                                                     <div id="deptusercontent"  style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;">
													<ul id="deptusertree" class="ztree" style="margin-top:0;"></ul>
											   </div>
									  </td>
									  
									</tr>
								
						     </table>
							 
							 <div style="border-bottom:#ededed dotted 1px; margin-top:15px;"></div>
							 
							 <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
						    <tr>
							  <td>
							  
								    <div class="form_all">		
                                            <label for="name" class="label_title">积分配置</label>  
									</div>
								<div class="form_all">
									<table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="jf_pz">
									 	 <tr>
									    <th width="50%" align="left" >
										  积分项名称
										</th>
										<th width="50%"  align="left">
										  积分分值
										</th>
									  </tr>
									  <s:iterator value="#request.configlist" var="config">
										  <tr>
										    <td>
											   ${config.pointsname }
											</td>
											<td>
											  <input type="hidden" name="configex_ids" value="${config.id }" />
											  <input type="text" name="pointsex" class="input_kz_4" /> 
											</td>
										  </tr>
										
										</s:iterator>
									  
									</table>
									<table table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto;">
									<tr>
										    <td  align="center" colspan="2" valign="middle" style="height:60px; border-top:#f5f5f5 solid 1px; padding-top:10px;">
						              
							                <a href="#" onclick="savestrategyex();"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a>
										 
							            </td>
									  </tr>
									 </table> 
									</div>
							  </td>
							 </tr>
						  </table>
					  </div>
					  
					  
				 </div>
				 
				 </div>
				 </form>
				 <div id="detailinfo"></div>
			 </td>
			 
		   </tr>
		   
	 </table> 
   
  
  
</div>
   
</body>
</html>
