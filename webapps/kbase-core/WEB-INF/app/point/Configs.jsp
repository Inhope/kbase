<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE >

<html>  
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">  <meta charset="utf-8" />   
<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resource/point/css/css3.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<title>小i知识库积分</title> 
<script type="text/javascript">
    $(function() {
          $("#tables tr").eq(1).click(); 
          if($.trim($("#pointsinit").val()) == ''){
	          $("#pointsinit").val('0')
	      }
     });
    
    function formsubmit(){
       var pointsname = $("#pointsname").val();
       if(pointsname == ''){
          parent.layer.alert('积分名称不能为空',-1);
       }else{
          $("#configform").submit();
       }
    
    }
   
    function stopconfiginfo(){
       var check =$("input:checkbox[name='config_id']:checked");
       if(check.length>1 || check.length<=0){
          parent.layer.alert("请选择一列!", -1);
          return false;
       }if($(check).attr("status")==1){
          parent.layer.alert("请选择停用项!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要停用选中的积分配制吗？", function(index){
       　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-config!stopPConfiginfo.htm',
			    data:{"configid":check.val()},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
				   if(data.rst){
				   		parent.layer.alert(data.msg, -1);
				   		window.location.href=$.fn.getRootPath() + "/app/point/p-config!findPConfiginfo.htm";
				   		parent.layer.close(index);
				   } else {
						parent.layer.alert(data.msg, -1);
				   }
				}	
			});
		})	　　
    }
    
    function startconfiginfo(){
       var check =$("input:checkbox[name='config_id']:checked");
       if(check.length>1 || check.length<=0){
          parent.layer.alert("请选择一列!", -1);
          return false;
       }if($(check).attr("status")==0){
          parent.layer.alert("请选择启用项!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要启用选中的积分配制吗？", function(index){
       　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-config!startPConfiginfo.htm',
			    data:{"configid":check.val()},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
				   if(data.rst){
				   		parent.layer.alert(data.msg, -1);
				   		window.location.href=$.fn.getRootPath() + "/app/point/p-config!findPConfiginfo.htm";
				   } else {
						parent.layer.alert(data.msg, -1);
				   }
				}	
			});
		})	　　
    }
   /** 
    function delconfiginfo(){
       var check =$("input:checkbox[name='config_id']:checked");
       if(check.length>1 || check.length<=0){
          parent.layer.alert("请选择一列!", -1);
          return false;
       }if($(check).attr("status")==1){
          parent.layer.alert("请选择启用项!", -1);
          return false;
       }
    　　 parent.layer.confirm("确定要删除选中的积分配制吗？", function(){
       　　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-config!deletePConfig.htm',
			    data:{"configid":check.val()},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"html",
				success:function(data){
				}	
			});
		})	　　
    }
    **/
    function showconfiginfo(id){
       $("#tables tr").removeClass();
       $("#"+id+"").addClass("color_gray");
       $("#configinfo").html("");
       $.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-config!getPconfiginfo.htm',
			    data:{"pconfig.id":id},
				type:"post",
				timeout:5000,
				async:false,
				dataType:"html",
				success:function(data){
				    $("#newconfig").hide();
				    $("#configinfo").append(data);
				}	
	   });
    }
    
    function updateconfig(){
       var pointsname = $.trim($("#points_name").val());
       var pointsmin = $.trim($("#pointsmin").val());
       var pointsmax = $.trim($("#pointsmax").val());
       var pointsinit = $.trim($("#pointsinit").val());
       if(pointsname == ''){
          parent.layer.alert('积分名称不能为空',-1);
       }else if(pointsinit == ''){
           parent.layer.alert('初始积分不能为空',-1);
       }else if(pointsmin!='' && isNaN(pointsmin)){
       		parent.layer.alert('积分下限请填写数字',-1);
       }else if(pointsmax!='' && isNaN(pointsmax)){
       		parent.layer.alert('积分上限请填写数字',-1);
       }else if(pointsmin!='' && pointsmax!='' && pointsmin>pointsmax){
       		parent.layer.alert('积分下限不能大于上限',-1);
       }
       else{
       　　　var params = $("#forms").serialize();
       　　　$.ajax({
			    url:$.fn.getRootPath()+'/app/point/p-config!updatePConfiginfo.htm?'+params,
				type:"post",
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
					if(data.rst){
						parent.layer.alert("更新成功!", -1);
						window.location.href=$.fn.getRootPath() + "/app/point/p-config!findPConfiginfo.htm";
					} else {
						parent.layer.alert(data.msg, -1);
					}
				}	
	   });
       }
    }
    
    function toaddconfig(){
      $("#configinfo").html("");
      $("#newconfig").show();
    }
    
    function formreset(){
       $("#configform")[0].reset();
    }
    
    function checkall(_this){
    	if($(_this).is(":checked")){
    		$("input:checkbox[name='config_id']").each(function(){
				$(this).attr("checked",true);
			});
    	}else{
    		$("input:checkbox[name='config_id']").each(function(){
				$(this).attr("checked",false);
			});
    	}
	}
</script>
</head>
<body>
	<div class="mainr_box jifen_jl">
	     <table cellpadding="0" cellspacing="0" border="0" width="100%" >
		       <tr>
			     <td colspan="3">
				   <ul class="btn_cz">
				      <!-- 
				      <li><a href="#" onclick="toaddconfig();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_03.gif" /></a></li>
					   -->
					  <li><a href="#" onclick="stopconfiginfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_05.gif" /></a></li>
					  <li><a href="#" onclick="startconfiginfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_18.gif" /></a></li>
					  <!--
					  <li><a href="#" onclick="delconfiginfo();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_07.gif" /></a></li>
				      -->
				   </ul>
				 </td>
			   </tr> 
			   <tr>
			     <td width="70%"  valign="top">
				    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_left" id="tables">
					   <tr>
					     <th><input name="" id="${config.id }" type="checkbox" value="${config.id }" onclick="checkall(this);"/></th>
						 <th>积分名称</th>
						 <th>积分单位</th>
						 <th>初始积分</th>
						 <th>积分上限</th>
						 <th>积分下限</th>
						 <th>状态</th>
					   </tr>
					   <s:iterator value="#request.configlist" var="config" status="ind">
						    <tr id="${config.id }" onclick="showconfiginfo('${config.id }');" style="cursor: pointer"> 
							    <td align="center"><input name="config_id" status="${config.status }" id="config_id" type="checkbox" value="${config.id }" /></td>
								<td>${config.pointsname }</td>
								<td>${config.pointsunit }</td>
								<td>${config.pointsinit }</td>
								<td>${config.pointsmax }</td>
								<td>${config.pointsmin }</td>
								<td><s:if test="#config.status == 1">停用</s:if><s:if test="#config.status == 0">启用</s:if></td>
						     </tr>
					   </s:iterator>
					</table>
				 </td>
				 <td width="2%"></td>
				 <td width="28%" valign="top" id="operaconfig">
				      <!-- 	
				     <div class="table_c" id="newconfig" style="display: none">
				        <form action="${pageContext.request.contextPath}/app/point/p-config!savePConfig.htm" method="post" id="configform">
					      <div class="jf_r_title"> 
					           <img src="${pageContext.request.contextPath}/resource/point/images/icon_06.gif" /> 新建积分项
						  </div>
						  <div class="table_b ">
						      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
							    <tr>
								  <td width="100%">
                                      <fieldset>  
									    <div class="form_all">		
                                             <label for="name" class="label_title">积分名称:</label> 
										     <div class="form_r"> 	<input type="text" id="pointsname" placeholder="请输入积分名称" name="pconfig.pointsname" class="input_kz" />
										          <div class="red">*必填</div>
										     </div>
										</div>
										
										<div class="form_all">		
                                             <label for="name" class="label_title">积分单位:</label> 
										     <div class="form_r"> 	<input type="text" id="name" placeholder="请输入积分单位" name="pconfig.pointsunit" class="input_kz" />
										          <div class="blue_b">积分单位可为空</div>
										     </div>
										</div>
										
										<div class="form_all">		
                                             <label for="name" class="label_title">初始积分:</label> 
										     <div class="form_r"> 	<input type="text" id="name" placeholder="请输入初始积分" name="pconfig.pointsinit" class="input_kz" />
										          <div class="blue_b">默认初始积分可为零</div>
										     </div>
										</div>
										
										<div class="form_all">		
                                             <label for="name" class="label_title">积分下限:</label> 
										     <div class="form_r"> 	<input type="text" id="name" placeholder="请输入积分下限" name="pconfig.pointsmin" class="input_kz" />
										          <div class="blue_b">积分下限可为空</div>
										     </div>
										</div>
										  
										 <div class="form_all">		
                                             <label for="name" class="label_title">积分上限:</label> 
										     <div class="form_r"> 	<input type="text" id="name" placeholder="请输入积分上限" name="pconfig.pointsmax" class="input_kz" />
										          <div class="blue_b">积分上限可为空</div>
										     </div>
										</div>
										<div class="form_all">		
                                             <label for="name" class="label_title">是否启用:</label> 
										     <div class="form_r"> 
										     　　　　<select name="pconfig.status">
										              <option value="0">启用</option>
										              <option value="1">未启用</option>
										          </select>	
										     </div>
										</div>
										  
                                         </fieldset>  	
                                      
								  </td>
								</tr>
								<tr>
								  
								  <td height="60" align="center">
								   <a href="#" onclick="formsubmit();"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#"  onclick="formreset()"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a>
								  </td>
								</tr>
							  </table>
						  </div>
						 </form>   
					 </div>
					  -->
				    <div id="configinfo"></div>
				 </td>
			   </tr>
		 </table> 
	   
	  
	  
	</div> 
</body>
</html>
