<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<form action="" method="post" id="forms">
 <div class="table_c"  id="stategyset">
    <div class="jf_r_title"> 
         策略详情
    </div>
  <div class="table_b">
      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
	    <tr>
		  <td width="100%">
		    <form action="#" method="post">  	
		    <input  type="hidden" class="input_kz_5" id="pstrategyid" name="pstrategy.id"  value="${strategy.id}" />
		    <input  type="hidden" class="input_kz_5" name="pstrategy.status"  value="${strategy.status}" />
                                <fieldset>  
			    <div class="form_all">		
                                       <label for="name" class="label_title">策略名称:</label> 
				     <div class="form_r"> 	<input type="text" name="pstrategy.actName" placeholder="请输入策略名称" class="input_kz" value="${strategy.actName }"/> 
				          
				     </div>
				</div>
				
				<div class="form_all">		
	                                      <label for="name" class="label_title">动作资源:</label> 
			      <div class="form_r">
			           <input  type="hidden" class="input_kz_5" name="pstrategy.act.actKey" id="act_key" value="${strategy.act.actKey }" placeholder="请输入动作资源" />
					   <input  type="hidden" class="input_kz_5" name="pstrategy.act.id" id="act_id" value="${strategy.act.id }" placeholder="请输入动作资源" />
				       <input  type="text" class="input_kz_5" name="pstrategy.act.actDesc" readonly="readonly" id="actdesc" value="${strategy.act.actDesc}"  placeholder="请输入动作资源" />
				       <div class="btn_browse"><a href="#" onclick="showresource();">浏览</a></div>
					   
			           <div class="layer_2" id="resources" style="display: none">
					       <div class="layer_2_box">
						      <ul>
							  <li>
							    <ul>
								  <li class="lp_01">&nbsp;</li>
								  <li class="lp_02">序号</li>
								  <li class="lp_03">资源</li>
								</ul>
							  </li>
							  <s:iterator value="#request.actlist" var="act" status="va">
							      <li>
								    <ul>
									  <li class="lp_01">
									   	<input name="act_key" type="radio" onclick="selectResource('${act.actKey }','${act.actDesc }','${act.id }');" <s:if test="#request.strategy.status==0"><s:iterator value="#request.strategylist" var="va1"><s:if test="#va1.status == 0 && #va1.act.actKey==#act.actKey & #va1.id!=#request.strategy.id">disabled</s:if></s:iterator></s:if> value="" />
									  </li>
									  <li class="lp_02">0${va.index+1 }</li>
									  <li title="${act.actDesc }"  class="lp_03">${act.actDesc }</li>
									</ul>
								   </li>
							  </s:iterator>
							  <li><a href="#"></a></li>
							  </ul>
						   </div>
					   </div>
			      </div>
				</div>
				<div class="form_all" id="configinfo">		
                                       <label for="name" class="label_title">周期设置:</label> 
					     <div class="form_r">
					     　<div id="circleconfig">
					          <s:iterator value="#request.cycletype" var="cyctype">
					              <s:if test="#request.strategy.actCyc==#request.cyctype.key">
					                 <s:if test="#request.cyctype.label=='一次' || #request.cyctype.label=='不限'">
					                     ${cyctype.label }<br />有效期 <span class='red'><fmt:formatDate  value="${strategy.cycFromDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></span> 到 <span class='red'><fmt:formatDate  value="${strategy.cycEndDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></span><br />
					                 </s:if>
					                 <s:else>
					                         每 <span class='red'>${cyctype.label }</span> 奖励次数 <span class='red'>${strategy.actTimes}</span> 次<br />有效期 <span class='red'><fmt:formatDate  value="${strategy.cycFromDate}" pattern="yyyy-MM-dd 	HH:mm:ss" /></span> 到 <span class='red'><fmt:formatDate  value="${strategy.cycEndDate}" pattern="yyyy-MM-dd HH:mm:ss" /></span><br />
					                 </s:else>
					              </s:if>
					          </s:iterator>
					      </div>
						   <a href="#" onclick="tomodicircle();">修改</a>
						 </div>
				</div>
				<div id="detailInfo" style="display: none">
					<div class="form_all">		
                                        <label for="name" class="label_title">周期设置:</label> 
					     <input type="hidden" id="acttimes" name="pstrategy.actTimes" value="${strategy.actTimes }" />
					     <div class="form_r"> 	
					     <s:iterator value="#request.cycletype" var="cyctype">
					         <s:if test="#request.cyctype.label=='一次' || #request.cyctype.label=='不限'">
								    <label class="radio"><input name="pstrategy.actCyc" type="radio" value="${cyctype.key}" <s:if test="#request.cyctype.key==#request.strategy.actCyc">checked="checked"</s:if>  />&nbsp; <span id="label_${cyctype.key}">${cyctype.label}</span></label>
					         </s:if>
					         <s:else>
					            <div class="hang_cycle">
                                                 <label class="radio">
                                                       <input name="pstrategy.actCyc" type="radio" <s:if test="#request.cyctype.key==#request.strategy.actCyc">checked="checked"</s:if> value="${cyctype.key}" />&nbsp; 每<span id="label_${cyctype.key}">${cyctype.label}</span></label>
                                                       <input type="text" id="maxtimes_${cyctype.key}" name="maxtimes" <s:if test="#request.cyctype.key==#request.strategy.actCyc">value="${strategy.actTimes }"</s:if> placeholder="最多奖励次数" class="input_kz_2" style="float:right;padding:0 0 0 1px;width:53%;margin-top:5px" /> 
							    </div>
					         </s:else>
					     </s:iterator>
					      </div>
					      </div>
					<div class="form_all">		
                                        <label for="name" class="label_title"></label> 
					     <div class="form_r" style="width:100%"> 	
						      有效期 <br />
						      <input type="text" id="starttime"  placeholder="日期" class="input_kz_3" name="pstrategy.cycFromDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<fmt:formatDate  value="${strategy.cycFromDate}" pattern="yyyy-MM-dd HH:mm:ss" />" readonly="readonly" style="width:40%"/> 到 <input type="text" id="endtime" name="pstrategy.cycEndDate" value="<fmt:formatDate  value="${strategy.cycEndDate}" pattern="yyyy-MM-dd HH:mm:ss" />"placeholder="日期" class="input_kz_3" onclick="WdatePicker({minDate:'#F{$dp.$D(\'starttime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" style="width:40%"/>
					     </div>
					
				  </div>
                    </fieldset>  	
		  </td>
		</tr>
		  <tr id="detailbutton" style="display: none">
									  
									  <td height="60" align="center">
									     
									      <a href="#" onclick="savecircle()";> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="resetcircle();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_15.gif"></a>									  </td>
									</tr>
	  </table>
   
	  <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
	    <tr>
		  <td>
		  
			    <div class="form_all">		
                                       <label for="name" class="label_title">积分配置</label>  
				</div>
			<div class="form_all">
				<table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;" class="jf_pz">
				 	 <tr>
				    <th width="50%" align="left" >
					  积分项名称
					</th>
					<th width="50%"  align="left">
					  积分分值
					</th>
				  </tr>
				  <s:iterator value="#request.configlist" var="config">
				      <tr> 
					       <td>
							  ${config.pointsname }
							</td>
							<td>
							　<input type="hidden" name="config_ids" value="${config.id }" />　
							  <input type="text" name="points" id="points" value="${config.remark }"　 class="input_kz_4" /> 
							</td>
						  </tr> 
				  </s:iterator>
				  <!-- 
				  <tr>
					    <td  align="center" colspan="2" valign="middle"  style="height:60px; border-top:#f5f5f5 solid 1px; padding-top:10px;">
	              
		                <a href="#" onclick="updatestrategy()"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="definedset();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_17.gif"></a>
					 
		            </td>
				  </tr>
				   -->
											</table>
											
										</div>
									  </td>
									  
								    </tr>
								  </table>
								  <table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto;" >
								  <tr>
									    <td  align="center" colspan="2" valign="middle"  style="height:60px; border-top:#f5f5f5 solid 1px; padding-top:10px;">
					              
						                <a href="#" onclick="updatestrategy()"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>&nbsp;<a href="#" onclick="definedset();"><img src="${pageContext.request.contextPath}/resource/point/images/btn_17.gif"></a>
									 
						            </td>
								  </tr>
								  </table>
							  </div>
						 </div>
				 <div class="table_c" id="definedset" style="display: none">
						      <div class="jf_r_title"> 
						            <img src="${pageContext.request.contextPath}/resource/point/images/icon_06.gif" />自定义策略详情
							        <span onclick="back();" style="cursor:pointer"><img src="${pageContext.request.contextPath}/resource/point/images/icon_19.gif" /></span>
							  </div>
							  <div class="table_b">
							      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
								    <tr>
									  <td width="100%" height="50" style="color:#000000;">
									   	自定义策略 
									  </td>
									  <td>
									    <a href="#" onclick="toaddstrategyext('${strategy.id }');"><img src="${pageContext.request.contextPath}/resource/point/images/add.gif"></a> 
									  </td>
									</tr>
									
								  </table>
								
								      <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table_left " id="ruletable">
									    <div class="table_right">
								          <tr>
							                 <th>名称</th>
							                 <th>类型</th>
							                 <th>操作</th>
						                 </tr>
								     <s:iterator value="#request.strategy.strategyExts" var="strategyExt" status="inde">
								         <tr style="cursor: pointer;" id="trext_${inde.index+1}">
							                 <td><a href="#" onclick="showstrategyext('${strategyExt.id}','${inde.index+1}')">策略${inde.index+1}</a></td>
							                 <td  class="ml_l" title="目录,部门" >目录,部门</td>
							                 <td class="blue_b"><a href="#" onclick="delStrategyext('${strategyExt.id}','trext_${inde.index+1}')"><img src="${pageContext.request.contextPath}/resource/point/images/icon_xq_03.gif"> 删除</a></td>
						                  </tr>
								     </s:iterator>
								  </table>
								  
							  </div>
							  
							  
							  <div class="table_b" id="detailstrategyext">
							      
							  </div>
							  
							  
						 </div>
						 <form>