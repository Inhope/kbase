<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>  
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">  <meta charset="utf-8" />
<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resource/point/css/css3.css" type="text/css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
  .layer3_choose .layer3_right{width:72%}
  .label_title{width:25%}
  .layer_3_box{width:100%}
  .gonggao_con_nr_fenye{
  	width: 670px;
  }
</style>
<SCRIPT type="text/javascript">
  $(function() {
	$("#month").bind('click',function(){
		$("#starttime").val("");
		$("#endtime").val("");
		WdatePicker({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false })
	})
	$("#starttime").bind('focus',function(){
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		var mindate=$("#month").val()+"-01";
		WdatePicker({minDate:mindate,maxDate:maxdate});
	})
	$("#endtime").bind('focus',function(){
		var mindate=$("#month").val()+"-01";
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		if($("#starttime").val()!=''){
			mindate=$("#starttime").val();
		}
		WdatePicker({minDate:mindate,maxDate:maxdate});
	})
  })
  
  function closediv(div){
        $("#"+div+"").css("display","none");
        parent.$('#detailpoints').hide();
	    parent.$('body').hideShade();
	    //add by eko.zhan at 2015-07-29
	    //当前页面在 个人中心-积分 中也会调用，采用 layer 打开，关闭时，需同时关闭 layer
	    //如果有必要，可以try-catch，暂时不使用
	    if (parent.__kbs_layer_ind!=undefined){
	    	parent.layer.close(parent.__kbs_layer_ind);
	    }
  }
  
  
  function searchinfo(){
       if($("#month").val()==''){
           parent.layer.alert("请选择月份!", -1);
          return false;
       }else{
           var _index = parent.layer.load("请稍候...");
		   var params=$("#searchform").serialize();
		   window.location.href = $.fn.getRootPath() + "/app/point/p-log!finddetailinfo.htm?"+params;
		   parent.layer.close(_index);
	 }
	}
	function pageClick(pageNo){
	       $("#pageNo").val(pageNo);
		   var params=$("#searchform").serialize();
		   $.ajax({
		  		type: "POST",
		  		url : $.fn.getRootPath() + "/app/point/p-log!finddetailinfo.htm?"+params,
		  		async: false,
		  		dataType : 'html',
		  		success: function(msg){
		  		   $("body").html(msg);
		  		}
		 });
	}
</script>
</head>
	<body>
		<div class="layer_3" id="detailinfo" style="width: 100%">
			<form id="searchform">
				<div class="layer_3_box">
					<div class="layer3_title">
						<p>
							积分明细
						</p>
						<img
							src="${pageContext.request.contextPath}/resource/point/images/cha_tc_05.gif"
							onMouseOver="this.src='${pageContext.request.contextPath}/resource/point/images/cha_tc_03.gif'"
							onMouseOut="this.src='${pageContext.request.contextPath}/resource/point/images/cha_tc_05.gif'"
							onclick="closediv('detailinfo');" />
					</div>
					<div style="margin: 12px;">
						<div class="layer3_choose la_height">
							<input type="hidden" name="log.userid" value="${log.userid }"
								id="loguserid" />
							<label class="label_title">
								积分配置：
							</label>
							<div class="layer3_right">
								<select name="log.configKey">
									<option value="">
										--请选择--
									</option>
									<s:iterator value="#request.configlist" var="config">
										<option value="${config.configKey }"
											<s:if test="#request.log.configKey==#config.configKey">selected</s:if>>
											${config.pointsname }
										</option>
									</s:iterator>
								</select>
							</div>
							<label class="label_title">
								选择月份：
							</label>
							<div class="layer3_right">
								<input type="text" name="log.month" placeholder="日期" id="month"
									class="input_kz_3" readonly="readonly" value="${log.month }" />
							</div>
							<label class="label_title">
								日期：
							</label>
							<div class="layer3_right">
								<input type="text" id="starttime" name="log.startTime"
									placeholder="日期" class="input_kz_3" readonly="readonly"
									value="${log.startTime }" />
								到
								<input type="text" name="log.endTime" placeholder="日期"
									class="input_kz_3" id="endtime" readonly="readonly"
									value="${log.endTime }" />
							</div>

							<div class="layer3_right">
								<a href="#" onclick="searchinfo();"><img
										src="${pageContext.request.contextPath}/resource/point/images/btn_search_07.gif">
								</a>
							</div>
						</div>

						<div class="layer_3_table" id="contentinfo"
							style="overflow-y: auto; overflow-x: hidden; max-height: 400px;">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td align="center" width="5%">
										序号
									</td>
									<td align="center" width="25%">
										日期
									</td>
									<td align="center" width="25%">
										动作
									</td>
									<td align="center" width="5%">
										得分
									</td>
									<td align="center" width="40%">
										备注
									</td>
								</tr>
								<s:iterator value="#request.page.result" var="va" status="ind">
									<tr>
										<td align="center" width="5%">
											${ind.index+1 }
										</td>
										<td align="center" width="25%">
											${va.createDate }
										</td>
										<td align="center" width="25%">
											${actKeys[va.actKey].desc }
										</td>
										<td align="center" width="5%">
											${va.points}
										</td>
										<td align="center" width="40%">
											${va.remark}
											<%--
												${actKeys[va.actKey].desc }
												<s:set var="pointsunit" value=""></s:set>
												<s:iterator value="#request.configlist" var="va1">
													<s:if test="#va1.configKey+'' == #va.configKey">
														${va1.pointsname }
														<s:set var="pointsunit" value="#va1.pointsunit"></s:set>
													</s:if>
												</s:iterator>
												${va.points} ${pointsunit }
											 --%>
										</td>
									</tr>
								</s:iterator>
								<tr class="trfen">
									<td colspan="5">
										<jsp:include page="../util/part_fenye.jsp"></jsp:include>
									</td>
								</tr>
							</table>
						</div>

					</div>

				</div>
			</form>
		</div>
	</body>
</html>

