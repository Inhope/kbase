<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>小i知识库积分</title>
		<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
		<STYLE type="text/css">
			a:focus{outline:none;}
		</STYLE>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript">
			var pIframe,points_variable = 0;
			function pointsRequest(pRequest){
				var url = '';
				if(pRequest == 'points_summary'){//积分概要
					$(pIframe).css("height","700px");
					url = '/app/point/p-manager.htm?pRequest='+pRequest;
				}else if (pRequest == 'points_configs'){//积分配置
					$(pIframe).css("height","1000px");
					url = '/app/point/p-config!findPConfiginfo.htm';
				}else if (pRequest == 'points_strategy'){//积分策略
					$(pIframe).css("height","1300");
					url = '/app/point/p-strategy.htm';
				}else if (pRequest == 'points_setting'){//积分设置
					$(pIframe).css("height","900");
					url = '/app/point/p-strategy-set.htm';
				}
				
				if(url != ''){
					if(url.indexOf('?')==-1)url += "?1=1";
					return $.fn.getRootPath()+url+"&points_variable="+points_variable++;
				}
				return false;
			}
			
			$(document).ready(function(){
				//请求初始化
				$('.mianr_con_title li a').each(function(){
					var id = $(this).attr("id");
					$(this).click(function(){
						//地址
						var url = pointsRequest(id);
						if(url){
							pIframe.src = url;
							//修改li选中样式
							$('.mianr_con_title li a').each(function(index,element){
								$(element).parent().attr("class","");
							});
							$('#'+id).parent().attr("class","focus");
						}
					});
				});
				
				//初始化页面
				pIframe = document.getElementById('pointsRequest');
				$('#points_summary').click();
				$(pIframe).css('display','block');
				
			});
		</script>
	</head>
	<body>
		<div class="page_right">
			<div class="mainr_con">
				<ul class="mianr_con_title">
					<li>
						<a href="###" id="points_summary">积分概要</a>
					</li>
					<li>
						<a href="###" id="points_configs">积分项配置</a>
					</li>
					<li>
						<a href="###" id="points_strategy">积分策略</a>
					</li>
					<li>
						<a href="###" id="points_setting">积分设置</a>
					</li>
				</ul>
				<iframe id="pointsRequest" src="" scrolling="no" frameborder="0" style="width: 100%;display: none;" /></iframe>
			</div>
		</div>
	</body>
</html>
