<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
 <div class="table_c">
   <form action="${pageContext.request.contextPath}/app/point/p-config!updatePConfiginfo.htm" method="post" id="forms">
     <div class="jf_r_title"> 
          详细信息
  </div>
  <div class="table_b ">
      <table cellpadding="0" cellspacing="0" border="0"  width="85%" style="margin:0 auto;">
	    <tr>
		  <td width="100%">
                                <fieldset>  
			    <div class="form_all">		
                                       <label for="name" class="label_title">积分名称:</label>
                                       <input type="hidden" name="pconfig.id" value="${pconfig.id }" />
                                       <input type="hidden" name="pconfig.status" value="${pconfig.status}" />
                                       <input type="hidden" name="pconfig.configKey" value="${pconfig.configKey }" />
				     <div class="form_r"> 	<input type="text" id="points_name" placeholder="请输入积分名称" name="pconfig.pointsname" value="${pconfig.pointsname }" class="input_kz" />
				          <div class="red">*必填</div>
				     </div>
				</div>
				
				<div class="form_all">		
                                       <label for="name" class="label_title">积分单位:</label> 
				     <div class="form_r"> 	<input type="text" id="name" placeholder="请输入积分单位" name="pconfig.pointsunit" value="${pconfig.pointsunit }" class="input_kz" />
				          <div class="blue_b">积分单位可为空</div>
				     </div>
				</div>
				
				<div class="form_all">		
                                       <label for="name" class="label_title">初始积分:</label> 
				     <div class="form_r"> 	<input type="text" id="pointsinit" placeholder="请输入初始积分" name="pconfig.pointsinit" value="${pconfig.pointsinit }" class="input_kz" />
				          <div class="blue_b">默认初始积分可为零</div>
				     </div>
				</div>
				
				<div class="form_all">		
                                       <label for="name" class="label_title">积分下限:</label> 
				     <div class="form_r"> 	<input type="text" id="pointsmin" placeholder="请输入积分下限" name="pconfig.pointsmin" value="${pconfig.pointsmin }" class="input_kz" />
				          <div class="blue_b">积分下限可为空</div>
				     </div>
				</div>
				  
				 <div class="form_all">		
                                       <label for="name" class="label_title">积分上限:</label> 
				     <div class="form_r"> 	<input type="text" id="pointsmax" placeholder="请输入积分上限" name="pconfig.pointsmax" value="${pconfig.pointsmax }" class="input_kz" />
				          <div class="blue_b">积分上限可为空</div>
				     </div>
				</div>
				
				  
                                   </fieldset>  	
                                
		  </td>
		</tr>
		<tr>
		  
		  <td height="60" align="center">
		   <a href="#" onclick="updateconfig();"> <img src="${pageContext.request.contextPath}/resource/point/images/btn_13.gif"></a>
		  </td>
		</tr>
	  </table>
  </div>
 </form>   
</div>
