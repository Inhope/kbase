<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body{
				margin: 3px;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/paper/paper.js"></script>
		<script type="text/javascript">
			var SystemKeys = parent.SystemKeys==undefined?{}:parent.SystemKeys;
			SystemKeys.userData = SystemKeys.userData==undefined?{}:SystemKeys.userData;
			SystemKeys.userData.viewUrl = $.fn.getRootPath() + "/app/task/pp-paper.htm?ispicker=1&returnField="+'${param.returnField}';
			
			$(function(){
				var param = '${param.returnField}';
				var descField = null;
				var idField = null;
				
				if (param.length>0){
					var paramArr = param.split("|");
					if (paramArr.length==1){
						descField = param;
						idField = param;
					}else{
						descField = paramArr[0];
						idField = paramArr[1];
					}
				}
			
				$('#btnOk').click(function(){
					//alert('hehe');
					//alert($('input[name="ppPaperId_list"]:checked').val());
					//alert($.trim($('input[name="ppPaperId_list"]:checked').parent('td').next().text()));
					
					var _id = $('input[name="ppPaperId_list"]:checked').val();
					var _desc = $.trim($('input[name="ppPaperId_list"]:checked').parent('td').next().text());
					
					if (_id==undefined || _id==''){
						layer.alert('请选择试卷', -1);
						return false;
					}
					
					if (descField!=null){
						parent.$("#"+descField).val(_desc);
						parent.$("#"+idField).val(_id);
					}
					
					$('#btnCancel').click();
				});
				
				$('#btnCancel').click(function(){
					try{
						parent.layer.close(parent.window.__kbs_layer_index);
					}catch(e){
						window.close();
					}
				});
			});
		</script>
	</head>
	<body>
<!--******************************************************试卷管理初始化页面*********************************************************************************  -->
		<form id="form0" action="" method="post">
			<div class="content_right_bottom" style="min-width:300px;">

				<div class="gonggao_titile">
					<div class="gonggao_titile_right">
						<a href="javascript:void(0);" id="btnCancel" >取消</a>
						<a href="javascript:void(0);" id="btnOk">确定</a>
					</div>
				</div>
				<div class="yonghu_titile">
					<ul>
						<li>
							试卷名称：
							<input type="text" id="paperName_select" name="ppPaperModel.paperName" value="${ppPaperModel.paperName }" />
						</li>
						<li class="anniu">
							<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
							<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="查询" onclick="javascript:pageClick('1');" />
							</a>
						</li>
					</ul>
				</div>
				
				<div class="gonggao_con">
					<div class="gonggao_con_nr">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="tdbg">
								<td colspan="2">
									试卷名称
								</td>
								<td>
									创建人
								</td>
								<td>
									创建时间
								</td>
								<td>
									试卷状态
								</td>
							</tr>
							<s:iterator value="page.result" var="va">
								<tr>
									<td>
										<input name="ppPaperId_list" type='radio' value="${va.id }" />
									</td>
									<td>
										${va.paperName }&nbsp;
									</td>
									<td>
										${va.user.userInfo.userChineseName}&nbsp;
									</td>
									<td>
										<s:date name="#va.createDate" format="yyyy-MM-dd HH:mm"/>&nbsp;
									</td>
									<td>
										<s:if test="#va.isEnable==0">
											正常
										</s:if>
										<s:elseif test="#va.isEnable==1">
											已归档
										</s:elseif>
																					
									</td>
								</tr>
							</s:iterator>
							<tr class="trfen">
								<td colspan="5">
									<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>

