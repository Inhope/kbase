<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>试卷管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/paper/paper.js"></script>
		<script type="text/javascript">
			var SystemKeys = parent.SystemKeys==undefined?{}:parent.SystemKeys;
			SystemKeys.userData = SystemKeys.userData==undefined?{}:SystemKeys.userData;
			SystemKeys.userData.viewUrl = $.fn.getRootPath() + "/app/task/pp-paper.htm";
		</script>
		
	</head>
	<body>
		<jsp:include page="/resource/task/task_menu_begin.jsp"></jsp:include>
				
					<!--******************************************************试卷管理初始化页面*********************************************************************************  -->
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="min-width:860px;">
			
							<div class="gonggao_titile">
								<div class="gonggao_titile_right">
									<a href="javascript:void(0);" id="ppPaperDel">删除试卷</a>
									<a href="###" onclick="openShade(this.id)" id="ppPaperEdit">编辑试卷</a>
									<a href="###" onclick="openShade(this.id)" id="ppPaperAdd" >新增试卷</a>
									<a href="###" id="ppPaperShare" >分享试卷</a>
								</div>
							</div>
							<div class="yonghu_titile">
								<ul>
									<li>
										试卷名称：
										<input type="text" id="paperName_select" name="ppPaperModel.paperName" value="${ppPaperModel.paperName }" />
									</li>
									<li>
										试卷状态：
										<s:select id="isEnable_select" name="ppPaperModel.isEnable" headerKey="" headerValue="-请选择-" list="#{'0':'使用中','1':'未使用'}"></s:select>
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button"
												class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
												class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" />
										</a>
									</li>
								</ul>
							</div>
							
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td colspan="2" width="55%">
												试卷名称
											</td>
											<td width="10%">
												创建人
											</td>
											<td width="15%">
												创建时间
											</td>
											<td width="10%">
												试卷状态
											</td>
											<td width="10%">
												详情
											</td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td>
													<input name="ppPaperId_list" sharedscope="${va.sharedscope }" shareddesc="${va.shareddesc }" type='checkbox' value="${va.id }" />
												</td>
												<td>
													${va.paperName }&nbsp;
												</td>
												<td>
													${va.user.userInfo.userChineseName}&nbsp;
												</td>
												<td>
													<s:date name="#va.createDate" format="yyyy-MM-dd HH:mm"/>&nbsp;
												</td>
												<td>
													<s:if test="#va.isEnable==0">
														使用中
													</s:if>
													<s:elseif test="#va.isEnable==1">
														未使用
													</s:elseif>
																								
												</td>
												<td>
													<span style="cursor:pointer;" paperId="${va.id }" id="btnInfo" shareddesc="${va.shareddesc }" usedPaperTask="">详情</span>
												</td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="6">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
						
				
		<jsp:include page="/resource/task/task_menu_end.jsp"></jsp:include>
	
	

	</body>
	
	
	
	
	<!-- **********************************************************分享功能（开始）*********************************************************************** -->
	<!--子页面(分享功能)选择用户  -->
	<script type="text/javascript">
		//分享任务地址
         var id, sharedurl = "/app/task/pp-paper!shareDo.htm";
		$(function (){
			//绑定分享试卷事件
			$('#ppPaperShare').click(function(){
				var ppPaperId_list = $("input:checkbox[name='ppPaperId_list']:checked")
				if (ppPaperId_list.length == 0) {
					layer.alert("请选择要操作的数据!", -1);
					return false;
				} else if (ppPaperId_list.length > 1) {
					layer.alert("不能同时操作多条数据!", -1);
					return false;
				} else {
					$("#shareddesc").val($(ppPaperId_list[0]).attr("shareddesc"));
					$("#sharedscope").val($(ppPaperId_list[0]).attr("sharedscope"));
					id = $(ppPaperId_list[0]).val();
				}
				
				$.layer({
					type : 1,
					border : [ 2, 0.3, '#000' ],
					title : ['试卷分享', 'font-size:14px;font-weight:bold;'],
					closeBtn : [ 0, true ],
					area : [ '700px', '400px' ],
					page: {dom: '#sharedshade' },
				    success: function(layero){
				       //$(layero).style("display", "block");
				       //相关知识选择
				    }
				});
			});
			
			//单击详情
            $('span[id="btnInfo"]').click(function(){
            	var _this = this;
            	var shareddesc = $(_this).attr("shareddesc");
            	var _html = '';
            	_html += '<b>[被分享人]</b>：' + (shareddesc!=''?shareddesc:'未分享') + '<br>';
            	//试卷被引用详情 add by wilson.li at 2016-07-06
            	if($(_this).attr("usedPaperTask") == ""){
            		var paperIds = [];
					paperIds.push($(_this).attr("paperId"));
	            	$.ajax({
						type: "POST",
						async: false,
						url: $.fn.getRootPath()+"/app/task/pp-paper!checkPaperUsed.htm",
						data: "paperIds="+paperIds,
						dataType:"json",
						success: function(data) {
							if(data.status){
								var taskNames = "";
								data = data.list;
								for(var i=0;i<data.length;i++){
									taskNames += (i==0?"":",")+data[i].name
								}
								$(_this).attr("usedPaperTask",'<b>[被引用任务]</b>：' + (taskNames!=''?taskNames:'未引用') + '<br>');
							}
						}
					});
            	}
				_html += $(_this).attr("usedPaperTask");
            	layer.tips(_html, $(_this).parent('td') , {maxWidth:500, style: ['background-color:#78BA32; color:#fff', '#78BA32'], guide: 1, time: 20, closeBtn:[0, true]});
            });
		});
	</script>
	<!--分享  -->
	<jsp:include page="../PShare.jsp"></jsp:include>
	<!-- **********************************************************分享功能（结束）*********************************************************************** -->
	
</html>

