<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>考试管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<style type="text/css">
			.gonggao_con{margin:0px;}
			.yonghu_titile{margin:0px;}
			td a{color:blue}
		</style>
		<script type="text/javascript">
			function checkAll(){
				$("input:checkbox[name='task_id']").each(function(){
					$(this).attr("checked",$(this).attr("checked")=='checked'?false:true);
				});
			}
			
			//清除数据
			function cleandata(task_id){
				parent.layer.confirm("确定要清除此条数据吗？", function(index){
					$('body').ajaxLoading('正在提交数据...');
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/task/task-user!CleanTaskData.htm', 
						data:{"task_id":task_id},
						async: true,
						dataType : 'json',
						success : function(data){
							$('body').ajaxLoadEnd();
							if(data.result == true){
								layer.alert("清除成功!", -1, function(){
									window.location.reload();
								});
							}else{
								layer.alert("清除失败!", -1);
							}
						},
						error : function(msg){
							$('body').ajaxLoadEnd();
							layer.alert("网络错误，请稍后再试!", -1);
						}
					});
					parent.layer.close(index);
				});
			}
			//重置
			function selectReset(){
				$("#job_number").val("");
				$("#task_name").val("");
				$("#user_name").val("");
				$("#dept_name").val("");
				$("#dept_bh").val("");
				$("#start_time").val("");
				$("#end_time").val("");
				$("#status option:first").attr("selected","selected");
			}
			
			function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/task/task-user!findTaskData.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}
			$(function(){
					var deptTreeObject = {
						deptTypeEl : $('#dept_name'),
						ktreeEl : $('div.menuContent'),
						treeAttr : {
							view : {
								expandSpeed: ''
							},
							async : {
								enable : true,
								url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
								autoParam : ["id", "name=n", "level=lv", "bh"],
								otherParam : {}
							},
							callback : {
								onClick : function(event, treeId, treeNode) {
									deptTreeObject.deptTypeEl.val(treeNode.name);
									$('#dept_bh').val(treeNode.bh);
								}
							}
						},
						render : function() {
							var self = this;
							self.deptTypeEl.focus(function(e){
								self.ktreeEl.css({
									'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
									'left' : $(this).offset().left + 'px'
								});
								if(self.ktreeEl.is(':hidden'))
									self.ktreeEl.show();
							});
							
							$('*').bind('click', function(e){
								if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
									
								} else {
									if(!self.ktreeEl.is(':hidden'))
										self.ktreeEl.hide();
								}
							});
							
							self.deptTypeEl.bind('keydown', function(keyArg) {
								if(keyArg.keyCode == 8) {
									$(this).val('');
									$('#dept_bh').val('');
								} else if(keyArg.keyCode == 13) {
									self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
								}
							});
							
							self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
								$(this).parent().parent().parent().hide();
							});
							
							$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
						}
					}
					
				
				deptTreeObject.render();
				
				$('#cleandata').click(function(){
					var params=$("#form0").serialize();
					parent.layer.confirm("确定要清除所有数据吗？", function(index){
						$('body').ajaxLoading('正在保存数据...');
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/task/task-user!CleanTaskData.htm?'+params, 
							data:{"task_id":""},
							async: true,
							dataType : 'json',
							success : function(data){
								$('body').ajaxLoadEnd();
								if(data.result == true){
									layer.alert("清除成功!", -1, function(){
										window.location.reload();
									});
								}else{
									layer.alert("清除失败!", -1);
								}
							},
							error : function(msg){
								$('body').ajaxLoadEnd();
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
						parent.layer.close(index);
					});
				});
				
				//清除选择
				$('#cleanchoosedata').click(function(){
					var checkids = [];
					$("input:checkbox[name='task_id']:checked").each(function(i, item){
						checkids.push($(this).val());
					});
					if(checkids.length<=0){
						parent.layer.alert('请至少勾选一条数据进清除!',-1);
						return false;
					}
					parent.layer.confirm("确定要清除勾选的数据吗？", function(index){
					$('body').ajaxLoading('正在提交数据...');
					$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/task/task-user!CleanTaskData.htm', 
							data:{"task_id":checkids.join(',')},
							async: true,
							dataType : 'json',
							success : function(data){
								$('body').ajaxLoadEnd();
								if(data.result == true){
									layer.alert("清除成功!", -1, function(){
										window.location.reload();
									});
								}else{
									layer.alert("清除失败!", -1);
								}
							},
							error : function(msg){
								$('body').ajaxLoadEnd();
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
						parent.layer.close(index);
					});
				
				});
				
			})
				
		</script>
	</head>
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border: 0px;">
					<form id="form0" action="" method="post">
					  <div class="content_right_bottom">
							<div class="yonghu_titile" style="height: 82px">
								<div>
									<ul>
										<li>
											工号：<input type="text" id="job_number"  name="info.jobnumber" value="${info.jobnumber }"/>
										</li>
										<li>
											姓名：<input type="text" id="user_name"  name="info.username" value="${info.username}"/>
										</li>
										<li>
											部门：<input type="text" id="dept_name" name ="info.deptname" value="${info.deptname}" onclick="showMenu('menuContent','dept_name')"  style="width:90px"/>
												<input id="dept_bh"  type="hidden" name="info.deptbh" value="${info.deptbh }"/>
										</li>
										<li>
											状态:
											<select name="info.status" id="status">
												<option value=""></option>
												<option value="1" <c:if test="${info.status==1 }">selected="selected"</c:if>>停用</option>
												<option value="0"  <c:if test="${info.status==0 }">selected="selected"</c:if>>启用</option>
											</select>
										</li>
									</ul>	
								</div>
								<div>
									<ul>	
										<li>
											考试时间范围：<input type="text" id="start_time"  name="info.starttime" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate" value="${info.starttime }"/>
											至<input type="text" id="end_time"  name="info.endtime" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate" value="${info.endtime }"/>
										</li>
										<li>
											任务名称：<input type="text" id="task_name"  name="info.taskname" value="${info.taskname}"/>
										</li>
										<li class="anniu">
											<a href="javascript:void(0)"><input type="button" id="cleanchoosedata" class="youghu_aa2" value="清除选择" /> </a>
											<a href="javascript:void(0)"><input type="button" id="cleandata" class="youghu_aa2" value="清除全部" /> </a>
											<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
											<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" /> </a>
										</li>
									</ul>
								</div>
							</div>
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="5%">
												<input type="checkbox" onclick="checkAll();"/>
											</td>	
											<td width="15%">
												姓名
											</td>
											<td width="15%">
												工号
											</td>
											<td width="15%">
												部门
											</td>
											<td width="5%">
												状态
											</td>
											<td width="15%">
												任务开始时间
											</td>
											<td width="15%">
												禁用时间
											</td>
											<td width="15%">
												任务名称
											</td>
										</tr>
										<s:iterator value="page.result" var="va">
											<tr>
												<td>
													<input type="checkbox" name="task_id" value="${va.task_id}"/>
												</td>
												<td>${va.user_name }</td>
												<td>${va.jobnumber }</td>
												<td>${va.dept_name }</td>
												<td><s:if test="#va.user_status==0">启用</s:if><s:else>停用</s:else></td>
												<td><s:date name="#va.start_time" format="yyyy-MM-dd" /></td>
												<td><s:date name="#va.end_time" format="yyyy-MM-dd" /></td>
												<td>${va.task_name }</td>
											</tr>
										</s:iterator>
										<tr class="trfen">
											<td colspan="8">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
									<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
										<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

