<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
        	body {
        		margin-top: -14px;
        	}
        	.gonggao_titile_right ul{line-height: 30px;}
        </style>
        
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/task/pe-task.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
				
			}
			
			function searchinfo(status,result){
			   $('#result').val(result);
			   $('#circlestatus').val(status);
			   $("#pageNo").val(1);
			   $("#form0").attr("action", $.fn.getRootPath()+"/app/task/pe-task.htm");
			   $("#form0").submit();
			}
			
			function detailtask(id){
			   location.href=$.fn.getRootPath()+"/app/task/pe-task!findtaskobj.htm?task_id="+id;
			}				
			
		</script>
	</head>
	<body>
<!--******************************************************任务管理初始化页面*********************************************************************************  -->
		<form id="form0" action="" method="post" style="">
			<!-- 
			<div class="content_right_bottom" style="min-width:860px;">
				<div class="gonggao_titile">
					<div class="gonggao_titile_left">
						<ul>
						　　　<li style="float:left">
								<input type="radio" <s:if test="#request.petask.taskResult==0">checked</s:if>  name="petask.taskResult" value="0" onclick="searchinfo('','0');"/>未完成
							</li>
							<li style="float:left;margin-left: 40px">
								<input type="radio" <s:if test="#request.petask.circlestatus==0">checked</s:if> name="petask.circlestatus" value="0" onclick="searchinfo('0','');"/>周期内任务
							</li>
							<li style="float:left;margin-left: 40px">
								<input type="radio" <s:if test="#request.petask.circlestatus==1">checked</s:if>  name="petask.circlestatus" value="1" onclick="searchinfo('1','');"/>周期外任务
							</li>
							<li style="float:left;margin-left: 40px">
								<input type="radio" <s:if test="#request.petask.circlestatus==2">checked</s:if>  name="petask.circlestatus" value="" onclick="searchinfo('2','');"/>全部
							</li>
							
						</ul>
					</div>
				</div>
			 -->	
				<div class="gonggao_con">
			       <div class="gonggao_con_nr">
						<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="5" align="right" valign="middle" style="text-align:right;border-top:1px solid #edecec;">
									<div class="gonggao_titile_right">
										<input type="hidden" id="result" name="petask.taskResult" value="${petask.taskResult }" />
										<input type="hidden" id="circlestatus" name="petask.circlestatus" value="${petask.circlestatus }" />
										<ul>
										　　　<li style="float:left">
												<input type="radio" <s:if test="#request.petask.taskResult==0">checked</s:if>  name="type" value="0" onclick="searchinfo('','0');"/>未完成
											</li>
											<li style="float:left;margin-left: 40px">
												<input type="radio" <s:if test="#request.petask.taskResult==1">checked</s:if>  name="type" value="0" onclick="searchinfo('','1');"/>已完成
											</li>
											<li style="float:left;margin-left: 40px">
												<input type="radio" <s:if test="#request.petask.circlestatus==0">checked</s:if> name="type" value="0" onclick="searchinfo('0','');"/>周期内任务
											</li>
											<li style="float:left;margin-left: 40px">
												<input type="radio" <s:if test="#request.petask.circlestatus==1">checked</s:if>  name="type" value="1" onclick="searchinfo('1','');"/>周期外任务
											</li>
											<li style="float:left;margin-left: 40px">
												<input type="radio" <s:if test="#request.petask.circlestatus==2">checked</s:if>  name="type" value="" onclick="searchinfo('2','');"/>全部
											</li>
											
										</ul>
									</div>
								</td>
							</tr>
							<tr class="tdbg">
								<td>
									任务名称
								</td>
								<td>
									任务类型
								</td>
								<td>
									任务周期
								</td>
								<td>
									任务状态
								</td>
								<td>
									完成状态
								</td>
							</tr>
							<s:iterator value="page.result" var="va">
								<tr>
									<td>
										<a href="#" onclick="detailtask('${va.id}');"><span style="color:blue">${va.pbTask.taskname }</span></a>
									</td>
									<td>
										<s:if test="#request.va.tasktype==0">学习</s:if><s:if test="#request.va.tasktype==1">考试</s:if>
									</td>
									<td>
										<fmt:formatDate  value="${va.pbTask.fromdate }" pattern="yyyy-MM-dd	HH:mm:ss" /> ~~ <fmt:formatDate  value="${va.pbTask.todate }" pattern="yyyy-MM-dd	HH:mm:ss" />
									</td>
									<td>
									   <s:set name="todaytime" value="new java.util.Date().getTime()"/>
									   <c:if test="${va.pbTask.todate.time-todaytime<0}">已禁用</c:if>
									   <c:if test="${va.pbTask.todate.time-todaytime>=0}">
									    	<s:if test="#request.va.pbTask.isrelease==1">进行中</s:if><s:if test="#request.va.pbTask.isrelease==2">停止</s:if>
										</c:if>
									</td>
									<td>
									   <s:if test="#request.va.taskResult==0">未完成</s:if><s:if test="#request.va.taskResult==1">已完成</s:if>
									</td>
								</tr>
							</s:iterator>
							<tr class="trfen">
								<td colspan="5">
									<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
								</td>
							</tr>
						</table>
				</div>
			</div>
		</form>
	</body>
</html>

