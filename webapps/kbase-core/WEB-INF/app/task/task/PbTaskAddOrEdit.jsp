<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
		
			
			//账号新增、编辑		
			function addOrEditPbTask(){
				if($("#taskname").val()==''){
			 		layer.alert("任务名称不能为空!", -1);
					return false;
				}
				if($("#tasktype").val()==''){
			 		layer.alert("任务类型不能为空!", -1);
					return false;
			}
				if($("#fromdate").val()==''){
				 		layer.alert("任务开始时间不能为空!", -1);
						return false;
				}
				if($("#todate").val()==''){
				 		layer.alert("任务禁用时间不能为空!", -1);
						return false;
				}
				var fromdate=$("#fromdate").val();
				var todate=$("#todate").val();
				var fdate = new Date(fromdate.replace(/-/g,"/"));
			    var tdate=new Date(todate.replace(/-/g,"/")); 
			    if(fdate.getTime()-tdate.getTime()>0){
		    			layer.alert("开始时间不能大于工号禁用时间!", -1);
						return false;
			    }
				if($("#studyscopedesc").val()==''&&$("#tasktype").val()!="1"){
				 		layer.alert("学习人员不能为空!", -1);
						return false;
				}
				if($("#examscopedesc").val()==''&&$("#tasktype").val()!="0"){
				 		layer.alert("考试人员不能为空!", -1);
						return false;
				}
				if($("#kbvaldesc").val()==''){
				 		layer.alert("相关知识不能为空!", -1);
						return false;
				}
				//只有考试类型的才需要选择考试试卷 modify by eko.zhan at 2015-08-06
				if ($('#tasktype').val()==1 && $("#paperdesc").val()==''){
				 		layer.alert("选择考试试卷不能为空!", -1);
						return false;
				}
				$('#body').ajaxLoading('正在保存数据...');
				var params = $("#form1").serialize();
				
				$.post($.fn.getRootPath()+"/app/task/pb-task!addOrEditDo.htm", params, function(data){
					$('#body').ajaxLoadEnd();
			   		layer.alert(data.msg, -1, function(index){
			   			layer.close(index);
			   			if (data.result==1){
				   			$(parent.document).find('div.content_content').find('iframe').each(function(){
								if (!$(this).is(":hidden")){
									$(this).attr("src", $.fn.getRootPath()+'/app/task/pb-task.htm'); 
									return false;
								}
							});
				   			parent.layer.close(parent.__kbs_layer_index);
				   		}
			   		});
				}, 'json');
				}
			//部门人员 学习人员 添加
		</script>
		<script type="text/javascript">
			
			/**
			 * 
			 */
			function _taskTypeChange(){
				if($('#tasktype').val()==0){
				 	$('#examtr').hide();
				 	$('#studytr').show();
			 	}else if($('#tasktype').val()==1){
				 	$('#studytr').hide();
				 	$('#examtr').show();
			 	}else{
				 	$('#examtr').show();
				 	$('#studytr').show();
			 	}
			}
		
			$(function(){
				
				//学习/考试类型切换
				$('#tasktype').change(function(){
				 	_taskTypeChange();
				});
				
				_taskTypeChange();
				
				
				//学习人员发布范围选择
				$('#studyscopedesc').click(function(){
					$.kbase.picker.taskdeptsearch({returnField:"studyscopedesc|studyscopeids"});
					//$.kbase.picker.deptusersearch({returnField:"studyscopedesc|studyscopeids"});
				});
				
				//考试人员发布范围选择
				$('#examscopedesc').click(function(){
					$.kbase.picker.taskdeptsearch({returnField:"examscopedesc|examscopeids"});
					//$.kbase.picker.deptusersearch({returnField:"examscopedesc|examscopeids"});
				});
				
				//相关知识选择
				$('#kbvaldesc').click(function(){
					$.kbase.picker.kbvalS({returnField:"kbvaldesc|kbvalids|kbobjids"});
				});
				
				//考卷选择
				$('#paperdesc').click(function(){
					window.__kbs_layer_index = $.layer({
						type:2, 
						border: [5, 0.3, '#000'], 
						title: false,
						closeBtn:[0, true],
						iframe:{src:$.fn.getRootPath() + "/app/task/pp-paper.htm?ispicker=1&returnField=paperdesc|paperid"}, 
						area:["800px", "520px"]
					});
				});
				
				//清空学习发布范围
				$('#btnClearStudy').click(function(){
					$('#studyscopedesc').val('');
					$('#studyscopeids').val('');
				});
				
				//清空考试发布范围
				$('#btnClearExam').click(function(){
					$('#examscopedesc').val('');
					$('#examscopeids').val('');
				});
				
				//清空相关知识
				$('#btnClearKb').click(function(){
					$('#kbvalids').val('');
					$('#kbobjids').val('');
					$('#kbvaldesc').val('');
				});
			
			});	//end document.ready							
		
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" name="pbTask.id" value="${pbTask.id }" />
			<table width="100%" cellspacing="0" cellpadding="0" class="box">
				<tr>
					<td>
						任务名称
					</td>
					<td>
						<input type="text" id="taskname" name="pbTask.taskname" value="${pbTask.taskname }" />
					</td>
					<td>
						任务类型
					</td>
					<td>
					  <s:if test="#request.pbTask.isstudy==1&&#request.pbTask.isexam==null"> <s:select id="tasktype" name="tasktype" value="0" headerKey=""  headerValue="--请选择--" list="#{'0':'学习','1':'考试','2':'学习考试'}"></s:select></s:if>
					  <s:elseif test="#request.pbTask.isexam==1&&#request.pbTask.isstudy==null"> <s:select id="tasktype" name="tasktype" value="1" headerKey="" headerValue="--请选择--" list="#{'0':'学习','1':'考试','2':'学习考试'}"></s:select></s:elseif>
					  <s:elseif test="#request.pbTask.isexam==1&&#request.pbTask.isstudy==1"> <s:select id="tasktype" name="tasktype"  value="2" headerKey="" headerValue="--请选择--" list="#{'0':'学习','1':'考试','2':'学习考试'}"></s:select></s:elseif>
					  <s:else> <s:select id="tasktype" name="tasktype" headerKey=""  headerValue="--请选择--" list="#{'0':'学习','1':'考试','2':'学习考试'}"></s:select></s:else>
					 
					</td>
				</tr>

				<tr>
					<td>
						任务开始时间
					</td>
					<td>
						<input id="fromdate" name="pbTask.fromdate"    onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" class="Wdate" value='<s:date  name="pbTask.fromdate" format="yyyy-MM-dd HH:mm:ss"/>' type="text" />
					</td>
					<td>
						工号禁用时间
					</td>
					<td>
						<input   id="todate"  name="pbTask.todate"   
							onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							readonly="readonly" class="Wdate"
							value='<s:date  name="pbTask.todate" format="yyyy-MM-dd HH:mm:ss"/>'
							type="text" />
					</td>
				</tr>
				<tr>
					<td>
						是否允许重复考试
					</td>
					<td colspan="3">
						<input name="pbTask.isrepeat" type="radio" value="0" checked="checked" />允许重复考试 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="pbTask.isrepeat" type="radio" value="1" />只允许一次
					</td>
				</tr>
				
				<tr id="studytr">
					<td>
						学习人员
					</td>
					<td colspan="3">
						<input id="studyscopedesc" name="pbTask.studyscopedesc"  value="${pbTask.studyscopedesc}" style="width:90%;"/>
						<input type="hidden" id="studyscopeids" name="pbTask.studyscope" value="${pbTask.studyscope}" />
						<input type="button" value="清空" style="height:20px;" id="btnClearStudy">
					</td>
				</tr>
				<tr  id="examtr">
					<td>
						考试人员
					</td>
					<td colspan="3">
						<input id="examscopedesc" name="pbTask.examscopedesc"  value="${pbTask.examscopedesc}" style="width:90%;"/>
						<input type="hidden" id="examscopeids" name="pbTask.examscope"  value="${pbTask.examscope}"/>
						<input type="button" value="清空" style="height:20px;" id="btnClearExam">
					</td>
				</tr>
				<tr>
					<td>
						相关知识
					</td>
					<td colspan="3">
						<input type="hidden" id="kbvalids" name="pbTask.kbval" value="${pbTask.kbval }"/>
						<input type="hidden" id="kbobjids" name="pbTask.kbobj" value="${pbTask.kbobj }"/>
						<input id="kbvaldesc" name="pbTask.kbvaldesc"  value="${pbTask.kbvaldesc}" style="width:90%;"/>
						<input type="button" value="清空" style="height:20px;" id="btnClearKb">
					</td>
				</tr>
				<tr>
					<td>
						选择考试
					</td>
					<td colspan="3">
						<div>
							<input type="hidden" id="paperid" name="pbTask.paperid" value="${pbTask.paperid }" />
							<input id="paperdesc" name="pbTask.paperdesc"  value="${pbTask.paperdesc}" style="width:90%;"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="right">
						<input type="button" onclick="addOrEditPbTask()" value="保存" />
					</td>
				</tr>

			</table>
		</form>
	</body>
</html>

