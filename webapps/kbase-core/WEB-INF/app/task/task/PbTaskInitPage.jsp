<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			/*
			.gonggao_con_nr td{
				line-height: 20px;
			}*/
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
		</style>
		
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		<script type="text/javascript">
			//**************************************公共方法（开始）*************************************
			//分页跳转
			function pageClick(pageNo){
				$('body').ajaxLoading('正在查询数据...');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/task/pb-task.htm");
				$("#pageNo").val(pageNo);
				$("#form0").submit();
			}				
			
			//查询条件初始化
			function selectReset(){
				$("input[id$='_select']").each(function(){
					$(this).attr("value","");
				});
				$("select[id$='_select']").each(function(){
					$(this)[0].selectedIndex = '';
				});
			}
			
			//弹出框打开 
			function openShade(id){
				var url,bl = false,title,parameters = {};
				if (id=="pbTaskAdd"){
					url = "/app/task/pb-task!addOrEditTo.htm?1=1";
					title="新增任务";	
				}else if(id=="pbTaskEdit"){
					url = "/app/task/pb-task!addOrEditTo.htm?1=1";
					title="编辑任务";	
					bl = true;
				}else{
					return;
				}
				
				if(bl){
					var pbTaskId_list = getUnReleaseIds();
					if(pbTaskId_list.length==0){
						parent.layer.alert("请选择未发布的考试任务!", -1);
						return false;
					}else if(pbTaskId_list.length>1){
						parent.layer.alert("不能同时操作多条数据!", -1);
						return false;
					}else{
						var pbTaskId = pbTaskId_list[0];
						url += "&pbTaskId="+pbTaskId;	
					}				
				}
				
				parent.__kbs_layer_index = parent.$.layer({
					type: 2,
					border: [2, 0.3, '#000'],
					title: [title, 'font-size:14px;font-weight:bold;'],
					closeBtn: [0, true],
					iframe: {src : $.fn.getRootPath()+url},
					area: ['1000px', '600px']
				});
			}
			
			/**
			 * 删除任务
			 */
			function noShade(id){
				var url,bl = false,title,parameters = {};
				if (id=="pbTaskDel"){//删除任务
					url = "/app/task/pb-task!delPbTask.htm";
					title= "删除";	
				}else{
					return;
				}
				
				var pbTaskIds = "";
				/*
				$("input:checkbox[name='task_id']:checked").each(function(){
					bl = true;
					pbTaskIds += $(this).val()+",";
				});
				if (pbTaskIds.length>0){
					pbTaskIds = pbTaskIds.substring(0, pbTaskIds.length);
				}
				parameters.pbTaskIds = pbTaskIds;
				*/
				var pbTaskId = $("input:checkbox[name='task_id']:checked").val();
				var length =$("input:checkbox[name='task_id']:checked").length;
				if(length>1 || length<=0){
					parent.layer.alert("请选择一条数据删除", -1);
					retun;	
				}else{
					layer.confirm("确定要"+title+"选中的数据吗？", function(index){
						$('body').ajaxLoading('正在保存数据...');
						$.post(
							$.fn.getRootPath()+url,
							{pbTaskId: pbTaskId},
							function(data){
								if(data=="1"){
									layer.alert("操作成功!", -1);
									pageClick('1');
								}else{
									layer.alert("操作失败!", -1);
									$('body').ajaxLoadEnd();
								}
						},"html");
						layer.close(index);
					});
				}
				/**
				$("input:checkbox[name='task_id']:checked").each(function(i, item){
						pbTaskIdArr.push($(this).val());
				});
				
				if(pbTaskIdArr.length>0){
					parameters = pbTaskIdArr.join(',');
					layer.confirm("确定要"+title+"选中的数据吗？", function(index){
						$('body').ajaxLoading('正在保存数据...');
						$.post(
							$.fn.getRootPath()+url,
							{pbTaskIds: parameters},
							function(data){
								if(data=="1"){
									layer.alert("操作成功!", -1);
									pageClick('1');
								}else{
									layer.alert("操作失败!", -1);
									$('body').ajaxLoadEnd();
								}
						},"html");
						layer.close(index);
					});
				}else{
					parent.layer.alert("请选择要"+title+"的未发布数据!", -1);
				}
				**/
			}
			/**
			 * 发布任务
			 */
			function publish(id){
			 	var url = "/app/task/pb-task!publish.htm";
				var releaseIds = getUnReleaseIds();
				
				var length =$("input:checkbox[name='task_id']:checked").length;　
				if(releaseIds.length>1 || releaseIds.length<=0){
					parent.layer.alert("请选择一条未发布的考试任务", -1);
					retun;	
				}else{
					parent.layer.confirm("确定要发布选中的数据吗？", function(index){
						var _loadIndex = parent.layer.load('请稍候...');
						$.post($.fn.getRootPath() + url, {pbTaskIds: releaseIds.join(',')}, function(data){
							parent.layer.close(_loadIndex);
							if(data.result==1){
								layer.alert("操作成功!", -1, function(){
									location.reload();
								});
							}else{
								layer.alert("操作失败!", -1);
							}
						}, 'json');
						parent.layer.close(index);
					});
					
				}
				
			}
				
			/**
			 * 撤销发布
			 */
			function revoke(){
				var url = "/app/task/pb-task!revoke.htm";
				var releaseIdArr = getUnReleaseIds(1);
				if (releaseIdArr.length==0){
					parent.layer.alert('请选择已发布的任务', -1);
					return false;
				}
				parent.layer.confirm("确定要撤销发布选中的数据吗？", function(index){
					$.post($.fn.getRootPath() + url, {pbTaskIds: releaseIdArr.join(',')}, function(data){
						$('body').ajaxLoadEnd();
						if(data.result==1){
							layer.alert("操作成功!", -1, function(){
								location.reload();
							});
						}else{
							layer.alert(data.msg, -1);
						}
					}, 'json');
					parent.layer.close(index);
				});
			}
			
			/**
			 * @author eko.zhan
			 * 获取选中的任务中所有未发布的任务
			 * 传入参数 1或0， 默认为获取未发布（0）的任务，传入1表示获取已发布的任务
			 */
			function getUnReleaseIds(isrelease){
				isrelease = isrelease==undefined?0:isrelease;
				var unReleaseIds = [];
				$("input:checkbox[name='task_id']:checked").each(function(i, item){
					if($(item).attr('_isrelease')==isrelease){
						unReleaseIds.push($(this).val());
					}
				});
				return unReleaseIds;
			}
			
			$(function(){
				//清除数据
				$('#clearData').click(function(){
					parent.$.layer({
						type : 2,
						border : [ 2, 0.3, '#000' ],
						title : [ "数据清除", 'font-size:14px;font-weight:bold;' ],
						closeBtn : [ 0, true ],
						iframe : {
							src : $.fn.getRootPath() + "/app/task/task-user!findTaskData.htm"
						},
						area : [ '70%', '90%' ]
					});
				});
				
				
	            //样式切换
	            $('span[id="btnInfo"], span[id="btnFinish"], span[id="btnProcess"]').hover(function(){
	            	$(this).css('color', '#FF0000');
	            }, function(){
	            	$(this).css('color', '#666');
	            });
	            
	            //单击详情
	            $('span[id="btnInfo"]').click(function(){
	            	var _this = this;
	            	var _taskId = $(_this).attr('taskid');
	            	var studyScope = $(_this).next().text();
	            	var examScope = $(_this).next().next().text();
	            	var kbQues = $(_this).next().next().next().text();
	            	var _html = '';
	            	_html += '<b>[学习范围]</b>：' + studyScope.replace(/,/gi, ", ") + '<br>';
	            	_html += '<b>[考试范围]</b>：' + examScope.replace(/,/gi, ", ") + '<br>';
	            	_html += '<b>[任务知识]</b>：' + kbQues.replace(/,/gi, ", ") + '<br>';
	            	var shareddesc = $(_this).attr("shareddesc");
	            	_html += '<b>[被分享人]</b>：' + (shareddesc!=''?shareddesc:'未分享') + '<br>';
	            	layer.tips(_html, $(_this).parent('td') , {maxWidth:500, style: ['background-color:#78BA32; color:#fff', '#78BA32'], guide: 1, time: 20, closeBtn:[0, true]});
	            });
	            
	            //单击已完成
	            $('span[id="btnFinish"]').click(function(){
	            	var _this = this;
	            	var _taskId = $(_this).attr('taskid');
	            	var _typeId = 1;
	            	_getFinishUserList(_taskId, _typeId);
	            });
	            
	            //单击未完成
	            $('span[id="btnProcess"]').click(function(){
	            	var _this = this;
	            	var _taskId = $(_this).attr('taskid');
	            	var _typeId = 0;
	            	_getFinishUserList(_taskId, _typeId);
	            });
			});
			
			/**
			 * private
			 * 获取已完成和未完成的用户清单
			 */
			function _getFinishUserList(taskid, typeid){
            	var _ind = parent.layer.load('加载中，请稍候...', 3);
				/**
				$.post($.fn.getRootPath() + url, {taskid: taskid, type: typeid}, function(data){
					_open(data);
				},'html');
				**/
				parent.layer.close(_ind);
				parent.$.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "任务详情", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/task-user!getUsers.htm?taskid="+taskid+"&type="+typeid
					},
					area : [ '60%', '80%' ]
				});
			}
			
			/**
			 * private
			 * 打开
			 */
			function _open(html){
				parent.$.layer({
           			type: 2,
				    title: false,
				    area: ['auto', '300px'],
					border: [5, 0.3, '#000'],
				    iframe: {
				        html: html
				    }
           		});
			}
			//完成状态详情
			function showmoredetail(id){
				parent.$.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "完成详情", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pb-task!detailcomplete.htm?taskid="+id
					},
					area : [ '50%', '70%' ]
				});
			}
			
		</script>
	</head>
		<body>
			<jsp:include page="/resource/task/task_menu_begin.jsp"></jsp:include>
			
					<!--******************************************************任务管理初始化页面*********************************************************************************  -->
					<form id="form0" action="" method="post">
						<div class="content_right_bottom" style="min-width:860px;">
							<div class="gonggao_titile">
								<div class="gonggao_titile_right">
								    <a href="javascript:void(0);" onclick="revoke();" id="doRevoke" >撤销发布</a>
								    <a href="javascript:void(0);" onclick="publish(this.id)" id="publish" >发布任务</a>
									<a href="javascript:void(0);" onclick="noShade(this.id)" id="pbTaskDel">删除任务</a>
									<a href="javascript:void(0);" onclick="openShade(this.id)" id="pbTaskEdit">编辑任务</a>
									<a href="javascript:void(0);" onclick="openShade(this.id)" id="pbTaskAdd" >新增任务</a>
									<a href="###" id="pbTaskShare" >分享任务</a>
									<myTag:input type="a" value="清除数据" key="cleanManagement" id="clearData"/>
									<!-- <a href="javascript:void(0);" id="clearData" >清除数据</a> -->
								</div>
							</div>
							<div class="yonghu_titile">
								<ul>
									<li>
										任务名称：
										<input type="text" id="remark_select" name="pbTaskModel.taskname" value="${pbTaskModel.taskname }" />
									</li>
									<li>
										任务类型：
									    <s:select id="isPublish_select" name="tasktype" headerKey="" headerValue="-请选择-" list="#{'0':'学习','1':'考试','2':'学习考试'}"></s:select>
									</li>
									
									<li>
										任务周期：
										<input id="validStart_select"  name="pbTaskModel.fromdate" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
											value='<s:date  name="pbTaskModel.fromdate" format="yyyy-MM-dd"/>' 
											type="text"  style="width:100px"/>
										至
										<input id="validEnd_select"  name="pbTaskModel.todate" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" class="Wdate"
											value='<s:date  name="pbTaskModel.todate" format="yyyy-MM-dd"/>' 
											type="text"  style="width:100px" />
									</li>
									<li class="anniu">
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
										<a href="javascript:void(0)"><input type="button"
											class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" />
										</a>
									</li>
								</ul>
							</div>
							
							<div class="gonggao_con">
								<div class="gonggao_con_nr">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="tdbg">
											<td width="3%">&nbsp;</td>
											<td width="20%">
												任务名称
											</td>
											<td width="8%">
												任务类型
											</td>
											
											<td width="8%">
												开始时间
											</td>
											<td width="8%">
												禁用时间
											</td>
											<!-- 
											<td width="10%">
												学习人员
											</td>
											<td width="10%">
												考试人员
											</td>
											<td width="25%">
												任务知识
											</td>
											 -->
											<td width="15%">
												任务试卷
											</td>
											<td width="6%">
												发布状态
											</td>
											<td width="6%">
												创建人
											</td>
											<td width="6%">
												完成状态
											</td>
											<td width="20%">详情</td>
										</tr>
										<s:iterator value="#request.tlist" var="va">
											<tr>
												<td>
													<input _isrelease="${va.isrelease}" name="task_id" sharedscope="${va.sharedscope }" shareddesc="${va.shareddesc }" type='checkbox' value="${va.id }" />
												</td>
												<td>
													${va.taskname} 
												</td>
												<td>
													<s:if test="#va.isstudy==1">学习 </s:if>
													<s:if test="#va.isexam==1">考试</s:if>
												</td>
												<td>
													<s:date name="#va.fromdate" format="yyyy-MM-dd" />&nbsp;
												</td>
												<td>
													<s:date name="#va.todate" format="yyyy-MM-dd"/>&nbsp;
												</td>
												<!-- 
												<td>
													${va.studyscopedesc}
												</td>
												<td>
													${va.examscopedesc}
												</td>
												<td>
													${va.kbvaldesc}
												</td>
												 -->
												<td>
													${va.paperdesc}
												</td>
												<td>
												<s:if test="#va.isrelease==0">未发布</s:if>
												<s:if test="#va.isrelease==1">已发布</s:if>
												</td>
												<td>
												 	${va.createUserNameCN} 
												</td>
												<td>
													<s:if test="#va.isrelease==1">
														<a href="#" onclick="showmoredetail('${va.id }')">
															<font color="blue">${va.successPercent}</font>
														</a>
													</s:if>
													<s:else>
														${va.successPercent}
													</s:else>
												</td>
												<td >
													<span style="cursor:pointer;" id="btnInfo"  taskid="${va.id }" shareddesc="${va.shareddesc }">详情</span>
													<span style="display:none">${va.studyscopedesc}</span>
													<span style="display:none">${va.examscopedesc}</span>
													<span style="display:none">${va.kbvaldesc}</span>
													<s:if test="#va.isrelease==1">
														 | 
														<span style="cursor:pointer;" id="btnFinish" taskid="${va.id }">已完成(${va.finishcount })</span>
														 | 
														<span style="cursor:pointer;" id="btnProcess" taskid="${va.id }">未完成(${va.nofinishcount })</span>
													</s:if>
												</td>
											</tr>
										    <div >
												<input type="hidden" name="pbTask.studyscope" value="${va.studyscope}" />
												<input type="hidden"" name="pbTask.examscope" value="${va.examscope}" />
											</div>
										</s:iterator>
										<tr class="trfen">
											<td colspan="11">
												<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</form>
					<div id="taskinfo"style="position: absolute;z-index:100010;display:none;">
						
					</div>
		<jsp:include page="/resource/task/task_menu_end.jsp"></jsp:include>
		
		
		
		
		<!-- **********************************************************分享功能（开始）*********************************************************************** -->
	<!--子页面(分享功能)选择用户  -->
	<script type="text/javascript">
		//分享试卷地址及id
        var id,sharedurl = "/app/task/pb-task!shareDo.htm";
		$(function (){
			//绑定分享试卷事件
			$('#pbTaskShare').click(function(){
				var taskId_list = $("input:checkbox[name='task_id']:checked")
				if (taskId_list.length == 0) {
					layer.alert("请选择要操作的数据!", -1);
					return false;
				} else if (taskId_list.length > 1) {
					layer.alert("不能同时操作多条数据!", -1);
					return false;
				} else {
					$("#shareddesc").val($(taskId_list[0]).attr("shareddesc"));
					$("#sharedscope").val($(taskId_list[0]).attr("sharedscope"));
					id = $(taskId_list[0]).val();
				}
				
				$.layer({
					type : 1,
					border : [ 2, 0.3, '#000' ],
					title : ['试卷分享', 'font-size:14px;font-weight:bold;'],
					closeBtn : [ 0, true ],
					area : [ '700px', '400px' ],
					page: {dom: '#sharedshade' },
				    success: function(layero){
				       //$(layero).style("display", "block");
				       //相关知识选择
				    }
				});
			});
		});
	</script>
	<!--分享  -->
	<jsp:include page="../PShare.jsp"></jsp:include>
	<!-- **********************************************************分享功能（结束）*********************************************************************** -->
	
	</body>
</html>

