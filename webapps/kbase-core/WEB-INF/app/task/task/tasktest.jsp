<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript">
   var corrid=[];
   var times=0;
   var interval;
   $(function(){
   		interval = setInterval(function(){
			times++;
		},1000);
   });
   function completetest(){
   	   var corrid=[];
       var flag=true;
       var rigcount=0;
       var judgelength=$("#judgelength").val();
       var signlelength=$("#signlelength").val();
       var morelength=$("#morelength").val();
       var total=parseInt(judgelength)+parseInt(signlelength)+parseInt(morelength);
       
       for(var i=0;i<=judgelength;i++){
	       $("input[id^='judge_"+i+"']").each(function(){
	       		if($(this).is(":checked")){
	       			$(this).attr("checked","checked");
	       		}
	       });
       }
       for(var i=0;i<=signlelength;i++){
	       $("input[id^='signle_"+i+"']").each(function(){
	       		if($(this).is(":checked")){
	       			$(this).attr("checked","checked");
	       		}
	       });
       }
       for(var i=0;i<=morelength;i++){
	       $("input[id^='more_"+i+"']").each(function(){
	       		if($(this).is(":checked")){
	       			$(this).attr("checked","checked");
	       		}
	       });
       }
       
       if(judgelength>0){
           for(var i=1;i<=judgelength;i++){
           		$("input[id^='judge_"+i+"']").each(function(){
				    if($(this).val()=='' && $(this).is(":checked")){
				       flag=false;
				       corrid.push($("#judgeindex_"+i+"").attr("name"));
				       $("#judgeindex_"+i+"").html("X"); 
				       return false;
				    }if($(this).val()!='' && !$(this).is(":checked")){
				       flag=false;
				       corrid.push($("#judgeindex_"+i+"").attr("name"));
				       $("#judgeindex_"+i+"").html("X"); 
				       return false;
				    }else{
				    	$("#judgeindex_"+i+"").html("√"); 
				    }
				
			   });
			   if($("#judgeindex_"+i+"").html()=='X'){
			   		rigcount=rigcount+1;
			   }
           }
       }
       
       if(signlelength>0){
           for(var i=1;i<=signlelength;i++){
			   $("input[id^='signle_"+i+"_']").each(function(){
				    if($(this).val()!=1 && $(this).is(":checked")){
				       flag=false;
				       corrid.push($("#signleindex_"+i+"").attr("name"));
				       $("#signleindex_"+i+"").html("X"); 
				       return false;
				    }if($(this).val()==1 && !$(this).is(":checked")){
				       flag=false;
				       corrid.push($("#signleindex_"+i+"").attr("name"));
				       $("#signleindex_"+i+"").html("X"); 
				       return false; 
				    }else{
				    	$("#signleindex_"+i+"").html("√");
				    }
			   });
			   if($("#signleindex_"+i+"").html()=='X'){
			   		rigcount=rigcount+1;
			   }
	   		}
	   }
	   
	   if(morelength>0){
           for(var i=1;i<=morelength;i++){
			   $("input[id^='more_"+i+"_']").each(function(){
				    if($(this).val()!=1 && $(this).is(":checked")){
				       flag=false;
				       corrid.push($("#moreindex_"+i+"").attr("name"));
				       $("#moreindex_"+i+"").html("X");
				       return false;
				    }if($(this).val()==1 && !$(this).is(":checked")){
				       flag=false;
				       corrid.push($("#moreindex_"+i+"").attr("name"));
				       $("#moreindex_"+i+"").html("X");
				       return false;
				    }else{
				    	$("#moreindex_"+i+"").html("√");
				    }
			   });
			   if($("#moreindex_"+i+"").html()=='X'){
			   		rigcount=rigcount+1;
			   }
	   		}
	   }
	   
	   rigcount=parseInt(total)-parseInt(rigcount);
	   var result='';
	   if($('#repeatstatus').val()=='1'){
	   		parent.layer.alert("考试完成",-1);
			result = '1';
			parent.$("#finished").show();
			parent.$("#nofinish").hide();
			savehistory(total,rigcount,corrid.join(','),result);
	   }else if(flag && $('#repeatstatus').val()=='0'){
	        parent.layer.alert("考试通过",-1);
			result = '1';
			parent.$("#finished").show();
			parent.$("#nofinish").hide();
			savehistory(total,rigcount,corrid.join(','),result);
	   }else{
	   	  result = '0';
	      var index= parent.$.layer({
			    shade: [0],
			    area: ['auto','auto'],
			    closeBtn: false,
			    dialog: {
			        msg: '考试未通过是否继续考核',
			        btns: 2,                    
			        type: 4,
			        btn: ['是','否'],
			        yes: function(){
			            parent.layer.close(index);
			            savehistory(total,rigcount,corrid.join(','),result);
			        }, no: function(){
			            parent.layer.close(index);
			            savehistory(total,rigcount,corrid.join(','),result);
			        }
			    }
			});
	   }
	   
   }
   
   function savehistory(totalcount,rigcount,corrid,result){
       $("#subdiv").empty();
	   var record = $('body').html();
       $.ajax({
				url : $.fn.getRootPath()+ "/app/task/pe-task!saverecord.htm",
				type : "post",
				data : {'id':'${task.id}','totalcount':totalcount,'paperrecord':record,'rigcount':rigcount,'corrid':corrid,'result':result,'times':times},
				dataType : "json",
				success : function(data) {
					parent.layer.close(parent.window._index);
				}
			});
   }
   
</script>
<s:set var="select_var" value="{'a','b','c','d','e','f','g','h','i','j','q','l','m','n'}"></s:set>
<div style="overflow:auto;width:100%;height:100%;font-size:14px;">
      <div style="margin:20px 20px 20px 20px;">
      <input type="hidden" id="judgelength"  value="<s:property value='#request.judgequestion.size()'/>"/>
      <s:if test="#request.judgequestion.size()>0"><span style="font-weight:bold;">判断</span><br/></s:if>
       <s:iterator value="#request.judgequestion" var="question" status="ind">
            ${ind.index+1 }.${question.questionName}<span style="display:none;width:25px;color:red;" name="${question.id}" id="judgeindex_${ind.index+1 }" class="indexquestion"></span><br/>
            <span><input type="radio" name="judge_${ind.index+1 }" id="judge_${ind.index+1 }" value="<s:if test="#question.isRight==1">1</s:if>" /></span><span id="<s:if test="#question.isRight==1">showanswer</s:if>">a</span>.正确<br/>
            <span><input type="radio" name="judge_${ind.index+1 }" id="judge_${ind.index+1 }" value="<s:if test="#question.isRight!=1">0</s:if>" /></span><span id="<s:if test="#question.isRight==0">showanswer</s:if>">b</span>.错误<br/><br/>
       </s:iterator>
       <input type="hidden" id="signlelength"  value="<s:property value='#request.signlequestion.size()' />"/>
       <s:if test="#request.signlequestion.size()>0"><br/><span style="font-weight:bold;">单选</span><br/></s:if>
       <s:iterator value="#request.signlequestion" var="question" status="inde">
             ${inde.index+1 }.${question.questionName}<span style="display:none;width:25px;color:red;" name="${question.id}" id="signleindex_${inde.index+1 }" class="indexquestion"></span><br/>
             <s:iterator value="#question.ppAnswer" var="ans" status="ind">
                <span><input type="radio" name="signle_${inde.index+1 }" id="signle_${inde.index+1 }_${ind.index+1 }"  value="${ans.isRight }" /></span><span id="<s:if test="#ans.isRight==1">showanswer</s:if>">${select_var[ind.index]}</span>.${ans.content}<br/>
             </s:iterator><br/>
       </s:iterator>
       <input type="hidden" id="morelength"  value="<s:property value='#request.morequestion.size()' />"/>
       <s:if test="#request.morequestion.size()>0"><br/><span style="font-weight:bold;">多选</span><br/></s:if>
       <s:iterator value="#request.morequestion" var="question" status="inde">
              ${inde.index+1 }.${question.questionName}<span style="display:none;width:25px;color:red;" name="${question.id}" id="moreindex_${inde.index+1 }" class="indexquestion"></span><br/>
			<s:iterator value="#question.ppAnswer" var="ans" status="ind">
				<span><input type="checkbox" name="more_${ind.index+1 }" id="more_${inde.index+1 }_${ind.index+1 }" value="${ans.isRight}" /></span>
				 <span id="<s:if test="#ans.isRight==1">showanswer</s:if>">${select_var[ind.index]}</span>. ${ans.content}<br/>
              </s:iterator>
			<br />
		</s:iterator>
   </div>
   <input type="hidden" id="repeatstatus" value="${task.pbTask.isrepeat }" />
   <div style="margin-left:400px" id="subdiv"><input type="button" value="提交" onclick="completetest();"/></div> 
</div>
	

