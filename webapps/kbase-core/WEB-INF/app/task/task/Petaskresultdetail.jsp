<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
	<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
	<style type="text/css">
			/*
			.gonggao_con_nr td{
				line-height: 20px;
			}*/
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table #tableinfo td{
				border:solid 1px #888888;
			}
			table #tableinfo th{
				border:solid 1px #888888;
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
			
		</style>
	<script type="text/javascript">
		var _indx="";
		
		function showinfo(id){
			$("#"+id+"").toggle();
		}
		
		function showpaper(id){
			if(_indx!=''){
				layer.close(_indx);
			}
			_indx = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "考试记录", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pe-task!showrecord.htm?id="+id
					},
					area : [ '90%', '100%' ]
				});
		}
	
	</script>
  </head>
  <body>
  	<div style="width:100%;height:100%;overflow:auto;">
		<table class="box" style="width:100%;border:solid 1px #666666" id="tableinfo">
    			<tr>
	    			<td colspan="3" align="center">个人考试记录</td>
	    		</tr>
    			<tr>
	    			<th width="40" align="center">准确率</th><th width="40%" align="center">做题时间</th><th align="center" width="20%">做题情况</th>
	    		</tr>
    			<s:iterator value="#request.resultlist" var="result" status="ind">
    				<tr>
    					<td align="center">${result.degree}</td>
    					<td align="center"><fmt:formatDate  value="${result.createDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></td>
    					<td	align="center"><a href="#" onclick="showpaper('${result.id}');">详情</a></td>
    				</tr>
    			</s:iterator>
 			</table>
	    			
    	</div>
    </body>	
</html>
