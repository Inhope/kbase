<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
　　　　　
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<style type="text/css">
		   #tables td{border:0px;}
		   a{text-decoration:none;}
		</style>
		<script type="text/javascript">
		
		   function showqainfo(obj_id,value_id,task_id){
		      location.href=$.fn.getRootPath()+"/app/task/pe-task!findquestion.htm?task_id="+task_id+"&obj_id="+obj_id+"&value_id="+value_id;
		   }
		   
		</script>
	</head>
	<body>
	    <table width="100%" cellspacing="0" cellpadding="0" class="box">
	       <tr>
	          <td></td>
	          <td  align="center">涉及实例</td>
	          <td  align="center">关注标准问</td>
	       </tr>
	       <tr>
	          <td align="center" width="15%">${task.pbTask.taskname }</td>
	          <td  align="center" width="25%" valign="top">
	             <table class="box" style="width:100%;height:100%;border: 0px;margin-top:0px" id="tables">
	                 <s:iterator value="#request.task.pbTask.objlist" var="va">
					　<tr>
					   <td align="center">
						<a href="#" onclick="showqainfo('${va.objectId }','','${task.id}');">${va.name }</a>
					   </td>
					  </tr> 
					</s:iterator>
	             </table>
	          </td>
	          <td  align="center" width="40%" style="vertical-align: top;">
	          	<div style="height: 500px;overflow-y: auto;vertical-align: top;">
	             <table  class="box" style="width:100%;border: 0px;margin-top:0px" id="tables">
	                 <s:iterator value="#request.task.pbTask.kbvallist" var="va">
					　<tr>
					   <td align="center">
						<a href="#" onclick="showqainfo('','${va.valueId }','${task.id}');">${va.question }</a>
					   </td>
					  </tr> 
					</s:iterator>
	             </table>
	            </div> 
	          </td>
	       </tr>
	    </table>
	</body>
</html>

