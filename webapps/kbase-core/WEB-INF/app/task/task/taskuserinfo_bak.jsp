<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
	<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
	<style type="text/css">
			/*
			.gonggao_con_nr td{
				line-height: 20px;
			}*/
			table.box{
    			table-layout:fixed;/* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */  
			}
			table #tableinfo td{
				border:solid 1px #888888;
			}
			table #tableinfo th{
				border:solid 1px #888888;
			}
			table.box td{
				word-break:keep-all;/* 不换行 */  
			    white-space:nowrap;/* 不换行 */  
			    overflow:hidden;/* 内容超出宽度时隐藏超出部分的内容 */  
			    text-overflow:ellipsis;/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/  
			}
			
		</style>
	<script type="text/javascript">
		var _indx="";
		
		function showinfo(id){
			$("#"+id+"").toggle();
		}
		
		function showpaper(id){
			if(_indx!=''){
				layer.close(_indx);
			}
			_indx = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "考试记录", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pe-task!showrecord.htm?id="+id
					},
					area : [ '90%', '100%' ]
				});
			alert($(document).parent.html());	
			$(parent.document).find('div').find('iframe').each(function(){
				alert(this.height());
				if (!$(this).is(":hidden")){
					this.scrollTop(800);
				}
			});	
		}
	
	</script>
  </head>
  <body>
   <s:if test="#request.type==1">
  	<div style="width:100%;height:100%;overflow:auto;">
    	<table class="box" style="width:100%;">
    		<s:if test="#request.studypetasks.size()>0">
	    		<tr>
	    			<th colspan="5" align="center">学习范围</th>
	    		</tr>
	    		<tr>
	    			<th width="20">用户名</th><th width="20">工号</th><th width="40%">所在部门</th><th width="20%" colspan="2">完成时间</th>
	    		</tr>
	    		<s:iterator value="#request.studypetasks" var="va">
	    			<tr>
	    				<td align="center">${va.user.userInfo.userChineseName }&nbsp;</td>
	    				<td align="center">${va.user.userInfo.jobNumber }&nbsp;</td>
	    				<td align="center">${va.user.mainStation.dept.name}&nbsp;</td>
	    				<td  align="center" colspan="2"><fmt:formatDate  value="${va.finishdate }" pattern="yyyy-MM-dd	HH:mm:ss" />&nbsp;</td>
	    			</tr>
	    		</s:iterator>
    		</s:if>	
    		<s:if test="#request.testpetasks.size()>0">
    		<tr>
    			<th colspan="5" align="center">考试范围</th>
    		</tr>
    		<tr>
    			<th width="20">用户名</th><th width="20%">工号</th><th width="40%">所在部门</th><th width="20%">完成时间</th><th width="10%">详情</th>
    		</tr>
    		<s:iterator value="#request.testpetasks" var="va1" status="inde">
    			<tr>
    				<td align="center">${va1.user.userInfo.userChineseName }&nbsp;</td>
    				<td align="center">${va1.user.userInfo.jobNumber }&nbsp;</td>
    				<td align="center">${va1.user.mainStation.dept.name}</td>
    				<td><fmt:formatDate  value="${va1.finishdate }" pattern="yyyy-MM-dd	HH:mm:ss" />&nbsp;</td>
    				<td	align="center"><a href="#" onclick="showinfo('detail_${inde.index }');">详情(<s:property value="#request.va1.peTaskResults.size" />)</a>&nbsp;</td>
    			</tr>
    			<tr	style="display:none" id="detail_${inde.index }">
    				<td colspan="5" align="center">
    				<s:if test="#request.va1.peTaskResults.size()>0">
	    				<table class="box" border="solid 5px #888888" style="width:100%;" id="tableinfo">
					    			<tr>
						    			<th width="30" align="center">准确率</th><th width="40%" align="center">做题时间</th><th align="center" width="30%" colspan="2">做题情况</th>
						    		</tr>
					    			<s:iterator value="#va1.peTaskResults" var="result" status="ind">
					    				<tr>
					    					<td width="30" align="center">${result.degree}</td>
					    					<td width="40" align="center"><fmt:formatDate  value="${result.createDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></td>
					    					<td	width="30" align="center"	colspan="2"><a href="#" onclick="showpaper('${result.id}');">详情</a></td>
					    				</tr>
					    			</s:iterator>
		    			</table>
	    			</s:if>
	    			<s:elseif test="#request.va1.peTaskResults.size()<=0">
	    				无记录
	    			</s:elseif>
	    			</td>
    			</tr>
    		</s:iterator>
    		</s:if>
    		<s:elseif test="#request.studypetasks.size()<=0">
    			<tr>
	    			<th colspan="4" align="center">无记录</th>
	    		</tr>
    		</s:elseif>
    	</table>
    	</div>
    </s:if>	
    <s:if test="#request.type==0">
  	<div style="width:100%;height:100%;overflow:auto;">
    	<table class="box" style="width:100%;">
    		<s:if test="#request.studypetasks.size()>0">
	    		<tr>
	    			<th colspan="4" align="center">学习范围</th>
	    		</tr>
	    		<tr>
	    			<th width="30%">用户名</th><th width="30%">工号</th><th width="40%" colspan="2">所在部门</th>
	    		</tr>
	    		<s:iterator value="#request.studypetasks" var="va">
	    			<tr>
	    				<td align="center">${va.user.userInfo.userChineseName }&nbsp;</td>
	    				<td align="center">${va.user.userInfo.jobNumber }&nbsp;</td>
	    				<td align="center" colspan="2">${va.user.mainStation.dept.name}&nbsp;</td>
	    			</tr>
	    		</s:iterator>
    		</s:if>	
    		<s:if test="#request.testpetasks.size()>0">
    		<tr>
    			<th colspan="4" align="center">考试范围</th>
    		</tr>
    		<tr>
    			<th width="20">用户名</th><th width="20%">工号</th><th width="40%">所在部门</th><th width="20%">详情</th>
    		</tr>
    		<s:iterator value="#request.testpetasks" var="va1" status="inde">
    			<tr>
    				<td align="center">${va1.user.userInfo.userChineseName }&nbsp;</td>
    				<td align="center">${va1.user.userInfo.jobNumber }&nbsp;</td>
    				<td align="center">${va1.user.mainStation.dept.name}</td>
    				<td	align="center"><a href="#" onclick="showinfo('detail_${inde.index }');">详情(<s:property value="#request.va1.peTaskResults.size" />)</a>&nbsp;</td>
    			</tr>
    			<tr	style="display:none" id="detail_${inde.index }">
    				<td colspan="4" align="center">
    				<s:if test="#request.va1.peTaskResults.size()>0">
	    				<table class="box" style="width:100%;border:solid 1px #666666" id="tableinfo">
					    			<tr>
						    			<th width="30" align="center">准确率</th><th width="40%" align="center">做题时间</th><th align="center" width="30%" colspan="2">做题情况</th>
						    		</tr>
					    			<s:iterator value="#va1.peTaskResults" var="result" status="ind">
					    				<tr>
					    					<td align="center">${result.degree}</td>
					    					<td align="center"><fmt:formatDate  value="${result.createDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></td>
					    					<td	align="center"	colspan="2"><a href="#" onclick="showpaper('${result.id}');">详情</a></td>
					    				</tr>
					    			</s:iterator>
		    			</table>
	    			</s:if>
	    			<s:elseif test="#request.va1.peTaskResults.size()<=0">
	    				无记录
	    			</s:elseif>
	    			</td>
    			</tr>
    		</s:iterator>
    		</s:if>
    		<s:elseif test="#request.studypetasks.size()<=0">
    			<tr>
	    			<th colspan="4" align="center">无记录</th>
	    		</tr>
    		</s:elseif>
    	</table>
    	</div>
    </s:if>	
    </body>	
</html>
