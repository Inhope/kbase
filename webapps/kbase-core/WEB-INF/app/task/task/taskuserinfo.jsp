<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
	<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	
	<script type="text/javascript">
		var _indx="";
		
		function showinfo(id){
			$("#"+id+"").toggle();
		}
		
		function showpaper(id){
			if(_indx!=''){
				layer.close(_indx);
			}
			_indx = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "考试记录", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pe-task!showrecord.htm?id="+id
					},
					area : [ '90%', '100%' ]
				});
			alert($(document).parent.html());	
			$(parent.document).find('div').find('iframe').each(function(){
				alert(this.height());
				if (!$(this).is(":hidden")){
					this.scrollTop(800);
				}
			});	
		}
		function pageClick(pageNo){
			$('body').ajaxLoading('正在查询数据...');
			$("#form0").attr("action", $.fn.getRootPath()+"/app/task/task-user!getUsers.htm");
			$("#pageNo").val(pageNo);
			$("#form0").submit();
		}
		
		function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			$(function(){
					var deptTreeObject = {
						deptTypeEl : $('#dept_name'),
						ktreeEl : $('div.menuContent'),
						treeAttr : {
							view : {
								expandSpeed: ''
							},
							async : {
								enable : true,
								url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
								autoParam : ["id", "name=n", "level=lv", "bh"],
								otherParam : {}
							},
							callback : {
								onClick : function(event, treeId, treeNode) {
									deptTreeObject.deptTypeEl.val(treeNode.name);
									$('#dept_bh').val(treeNode.bh);
								}
							}
						},
						render : function() {
							var self = this;
							self.deptTypeEl.focus(function(e){
								self.ktreeEl.css({
									'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
									'left' : $(this).offset().left + 'px'
								});
								if(self.ktreeEl.is(':hidden'))
									self.ktreeEl.show();
							});
							
							$('*').bind('click', function(e){
								if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
									
								} else {
									if(!self.ktreeEl.is(':hidden'))
										self.ktreeEl.hide();
								}
							});
							
							self.deptTypeEl.bind('keydown', function(keyArg) {
								if(keyArg.keyCode == 8) {
									$(this).val('');
									$('#dept_id').val('');
								} else if(keyArg.keyCode == 13) {
									self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
								}
							});
							
							self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
								$(this).parent().parent().parent().hide();
							});
							
							$.fn.zTree.init($('ul#treeDemo'), this.treeAttr );
						}
					}
					deptTreeObject.render();
			});
	</script>
  </head>
  <body>
   <div style="width: 100%;">
			<div data-options="region:'center',title:''" style="width: 100%;float: left;">
				<div id="p" class="easyui-panel" data-options="fit:false" style="border: 0px;">
				<form id="form0" action="" method="post">
				  <input type="hidden" name="taskid" value="${map.task_id }" />
				  <input type="hidden" name="type" value="${map.type_id }" />
				  <div class="content_right_bottom">
						<div class="yonghu_titile" style="margin:0px;">
							<ul>
								<li>
									<input type="hidden" value="${examineeinfo.exam_id }" name="examineeinfo.exam_id" />
									姓名：<input type="text" id="user_name"  name="user_name" value="${map.user_name }"/>
								</li>
								<li>
									部门：<input type="text" id="dept_name" name ="dept_name" value="${map.dept_name}" onclick="showMenu('menuContent','dept_name')"  style="width:150px"/>
										<input id="dept_bh"  type="hidden" name="dept_bh" value="${map.dept_bh }"/>
								</li>
								<li>
									考试范围:
									<select name="scope_status">
										<option value="">--请选择--</option>
										<option <c:if test="${map.scope_status==0 }">selected="selected"</c:if> value="0">学习范围</option>
										<option <c:if test="${map.scope_status==1 }">selected="selected"</c:if> value="1">考试范围</option>
									</select>
								</li>
								<li class="anniu">
									<a href="javascript:void(0)"><input type="button" class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
									<a href="javascript:void(0)"><input type="button" class="youghu_aa2" value="提交" onclick="javascript:pageClick('1');" /> </a>
								</li>
								
							</ul>
						</div>
						<c:if test="${map.type_id==0 }">
							<div class="gonggao_con" style="margin:0px;">
							<div class="gonggao_con_nr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr class="tdbg">
										<td width="20%">
											用户名
										</td>
										<td width="20%">
											工号
										</td>
										<td width="30%">
											所在部门
										</td>
									</tr>
									<s:iterator value="#request.page.result" var="va1" status="inde">
										<tr>
											<td align="center">${va1.user_name }</td>
						    				<td align="center">${va1.jobnumber}</td>
						    				<td align="center">${va1.dept_name}</td>
										</tr>
									</s:iterator>
									<tr class="trfen">
										<td colspan="5" align="center">
											<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</c:if>
					<c:if test="${map.type_id==1 }">
						<div class="gonggao_con" style="margin:0px;">
							<div class="gonggao_con_nr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr class="tdbg">
										<td width="20%">
											用户名
										</td>
										<td width="20%">
											工号
										</td>
										<td width="30%">
											所在部门
										</td>
										<td width="20%">
											完成时间
										</td>
										<td width="10%">
											详情
										</td>
									</tr>
									<s:iterator value="#request.page.result" var="va1" status="inde">
										<tr>
											<td align="center">${va1.user_name }</td>
						    				<td align="center">${va1.jobnumber}</td>
						    				<td align="center">${va1.dept_name}</td>
						    				<td><fmt:formatDate  value="${va1.finishdate }" pattern="yyyy-MM-dd	HH:mm" />&nbsp;</td>
						    				<s:if test="#va1.task_type==1">
						    					<td	align="center"><a href="#" onclick="showinfo('detail_${inde.index }');">详情(<s:property value="#request.va1.peTaskResults.size" />)</a>&nbsp;</td>
						    				</s:if>
						    				<s:elseif test="#va1.task_type==0">
						    					<td	align="center">学习类型</td>
						    				</s:elseif>
						    				
										</tr>
										<tr	style="display:none" id="detail_${inde.index }">
						    				<td colspan="5" align="center">
						    				<s:if test="#request.va1.peTaskResults.size()>0">
							    				<table class="box"  style="width:100%;" id="tableinfo">
											    			<tr>
												    			<th width="30" align="center">准确率</th><th width="40%" align="center">做题时间</th><th align="center" width="30%" colspan="2">做题情况</th>
												    		</tr>
											    			<s:iterator value="#va1.peTaskResults" var="result" status="ind">
											    				<tr>
											    					<td width="30" align="center">${result.degree}</td>
											    					<td width="40" align="center"><fmt:formatDate  value="${result.createDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></td>
											    					<td	width="30" align="center"	colspan="2"><a href="#" onclick="showpaper('${result.id}');">详情</a></td>
											    				</tr>
											    			</s:iterator>
								    			</table>
							    			</s:if>
							    			<s:elseif test="#request.va1.peTaskResults.size()<=0">
							    				无记录
							    			</s:elseif>
							    			</td>
						    			</tr>
									</s:iterator>
									<tr class="trfen">
										<td colspan="5" align="center">
											<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
										</td>
									</tr>
								</table>
							</div>
						</div>
						</c:if>
						<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
							<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 
   <table class="box" style="width:100%;">
    		<tr>
    			<th width="20%">用户名</th><th width="20%">工号</th><th width="30%">所在部门</th><th width="20%">完成时间</th><th width="10%">详情</th>
    		</tr>
    		<s:iterator value="#request.page.result" var="va1" status="inde">
    			<tr>
    				<td align="center">${va1.user_name }&nbsp;</td>
    				<td align="center">${va1.jobnumber}&nbsp;</td>
    				<td align="center">${va1.dept_name}</td>
    				<td><fmt:formatDate  value="${va1.finishdate }" pattern="yyyy-MM-dd	HH:mm" />&nbsp;</td>
    				<td	align="center"><a href="#" onclick="showinfo('detail_${inde.index }');">详情(<s:property value="#request.va1.peTaskResults.size" />)</a>&nbsp;</td>
    			</tr>
    			<tr	style="display:none" id="detail_${inde.index }">
    				<td colspan="5" align="center">
    				<s:if test="#request.va1.peTaskResults.size()>0">
	    				<table class="box" border="solid 5px #888888" style="width:100%;" id="tableinfo">
					    			<tr>
						    			<th width="30" align="center">准确率</th><th width="40%" align="center">做题时间</th><th align="center" width="30%" colspan="2">做题情况</th>
						    		</tr>
					    			<s:iterator value="#va1.peTaskResults" var="result" status="ind">
					    				<tr>
					    					<td width="30" align="center">${result.degree}</td>
					    					<td width="40" align="center"><fmt:formatDate  value="${result.createDate}" pattern="yyyy-MM-dd	HH:mm:ss" /></td>
					    					<td	width="30" align="center"	colspan="2"><a href="#" onclick="showpaper('${result.id}');">详情</a></td>
					    				</tr>
					    			</s:iterator>
		    			</table>
	    			</s:if>
	    			<s:elseif test="#request.va1.peTaskResults.size()<=0">
	    				无记录
	    			</s:elseif>
	    			</td>
    			</tr>
    		</s:iterator>
    		<tr class="trfen">
				<td colspan="11">
					
				</td>
			</tr>
    	</table>
    	 -->	
    </body>	
</html>
