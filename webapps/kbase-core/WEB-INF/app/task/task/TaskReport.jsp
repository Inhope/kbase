<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/statement/css/ca.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
  	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/TaskReport.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
  <style>
    .input_title{width:100%;height:auto;background:#f4f4f4;float:left;font-size:12px;line-height:35px;padding:7px 0 0 0;}
	.input_title li{height:30px;width:auto;float:left;line-height:30px;margin-bottom:7px;list-style:none;}
	.input_title li p{float:left;color:#333;margin-right:5px;width:65px;text-align:right;margin-right:2px;padding-left:10px;margin-top:0px;}
	.input_title input{border:1px solid #aaabb0;width:120px;background:#FFF;font-size:12px;float:left;padding:0 3px;color:#666;height:24px;line-height:24px;vertical-align:middle;}
	.input_title .report_title_btn{background:#c72528;text-align:center;width:50px;height:26px;line-height:26px;border:none;color:#FFF;border-radius:3px;cursor:pointer;margin-left:10px;}
	.report_title_btn:hover,.report_title_btn:active{background:#b51c1f;color:#FFF;}
  </style>
  </head>
  
  <body>
    <table>
    </table>
    <div class="input_title" id="searchCondition" style="height: 80px;">
		<form action="" method="get">
			<ul>
			<li><p>任务时间从</p>
				<input id="starttime" name="" type="text"  readonly="readonly"
					onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" style="width:120px;margin:0;"
					value="${beforeDay }"/></li>
			<li><p style="width:30px;text-align:center;padding:0;">到</p>
				<input id="endtime" name="" type="text"
					onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" style="width:120px;"
					value="${today }"/></li>
			<li><p>任务名称:</p><input id="taskname" name="" type="text"/></li>
			<li><p>试卷名称:</p><input id="papername" name="" type="text"  style="width:120px;margin:0;" /></li>
			<li><p>状态：</p>
				<select id="status">
					<option value="">--请选择--</option>
					<option value="1">(事前)已完成</option>
					<option value="2">(事后)已完成</option>
					<option value="0">未完成</option>
				</select>
			</li>
			<!-- 
			<li><p>IP：</p>
				<select id="ipstatus">
					<option value="">--请选择--</option>
					<option value="1">相同ip</option>
					<option value="2">包含多个ip</option>
				</select>
			</li>
			 -->
			<li><p>部门:</p><input id="dept" readonly="readonly" onclick="showMenu('menuContent','dept')" name="" type="text" /><input id="deptid"  type="hidden" value=""/></li>
			<li><p>虚拟部门:</p>
			<input type="text" id="virtualname" name="virtualname" onclick="showMenu('virtualdept','virtualname')"/><input type="hidden" id ="virtualdeptid" value="" />
			</li>
			<li><p>发布人员:</p><input id="username" name="" type="text" /></li>
			<!-- 
			<s:if test="#request.user==null">
				<li><p>发布人员:</p><input id="username" name="" type="text" /></li>
			</s:if>
			<s:else>
				<li><p>发布人员:</p><input id="username" name="" type="text" value="${user.userInfo.userChineseName }" readonly="readonly" style="background: #C0C0C0"/></li>
				<input type="hidden" value="${user.id}" id="user_id"/>
			</s:else>
			 -->
			<li><input class="report_title_btn" type="button" id="searchBtn" value="查询" /><input id="exportBtn" class="report_title_btn" type="button" value="导出" /></li>
			</ul>
		</form>
	</div>
	
	<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
		<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
	</div>
	<div id="virtualdept" class="virtualdept" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
		<ul id="virtualdepttree" class="ztree" style="margin-top:0; width:160px;"></ul>
	</div>
    <!-- 
    <div id="searchCondition" style="padding: 5px;height: 60px;">
    	<span>选择月份:</span>
    	<input type="text" id="month" style="width:100px;border:1px solid #abadb3;height:24px;line-height:24px;"/>
		<span>时间:</span>
		从<input type="text" id="startTime" style="width:140px;border:1px solid #abadb3;height:24px;line-height:24px;"/>
		到&nbsp;<input type="text" id="endTime" value="" style="width:140px;border:1px solid #abadb3;height:24px;line-height:24px;"/>
		<span>知识分类:</span>
		<input type="text" id="categoryName" readonly="readonly" style="width:120px;border:1px solid #abadb3;height:20px;line-height:20px;"/>
		<span>文章名:</span>
		<input type="text" id="objectContent" maxlength="30" style="width:120px;border:1px solid #abadb3;height:20px;line-height:20px;"/>
		<span>标准问:</span>
		<input type="text" id="question" maxlength="30" style="width:120px;border:1px solid #abadb3;height:20px;line-height:20px;"/>
		<div style="margin-top: 8px;">
			<span>部门:</span>
			<input type="text" id="dept" readonly="readonly" style="width:120px;border:1px solid #abadb3;height:20px;line-height:20px;"/>
			<span>&nbsp;操作人员:</span>
			<input type="text" id="person" style="width:120px;border:1px solid #abadb3;height:20px;line-height:20px;" />
			<a id="searchBtn" href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
			<a id="exportBtn" href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-print'">导出</a>
		</div>
	</div>
	<div class="t-p-d">
		<iframe style="width:100%;filter:alpha(opacity=0);-moz-opacity:0; position:absolute; z-index:-1;display:none;"></iframe> 
		<div id="catetory_title" class="title1">
			<div class="left">知识分类选择</div>
			<div class="right">
				<a href="javascript:void(0)">×</a>
			</div>
		</div>
		<ul id="categoryTree" class="ztree" style="width: 174px; height: 260px; margin-top: 0px; overflow: auto;"></ul>
	</div>
	
	<div class="t-p-d1">
		<iframe style="width:100%;filter:alpha(opacity=0);-moz-opacity:0; position:absolute; z-index:-1;display:none;"></iframe> 
		<div id="catetory_dept" class="title1">
			<div class="left">部门选择</div>
			<div class="right">
				<a href="javascript:void(0)">×</a>
			</div>
		</div>
		<ul id="deptTree" class="ztree" style="width: 174px; height: 220px; margin-top: 0px; overflow: auto;"></ul>
	</div>
	 -->
  </body>
</html>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>