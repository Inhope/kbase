<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
　　　　　
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
　　　　　<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<style type="text/css">
		  .taskstyle{
		    background: #67a909 none repeat scroll 0 0;
		    color: #fff;
		    display: inline;
		    float: left;
		    font-weight: normal;
		    height: 24px;
		    line-height: 24px;
		    margin-right: 10px;
		    padding: 0 10px;
		    width: auto;
		  }
		</style>
		<script type="text/javascript">
			function toread(){
			    parent.$(document).hint('已签读');
			    $("#read").html("已签读");
			    $("#read").css("cursor","default");
			    $("#read").css("background-color","grey");
			    //$("#totest").css("cursor","pointer");
				//$("#totest").css("background-color","#67a909")
				//$("#totest").attr("onclick","totest();");
				$("#totest").hide();
				$("#test").show();
				if('${task.pbTask.isrelease}'==1){
					$.ajax({
					    url:$.fn.getRootPath()+'/app/task/pe-task!updatestudytask.htm',
					    data:{"task_id":'${task.pbTask.id}'},
						type:"post",
						timeout:5000,
						dataType:"json",
						success:function(data){
					       $("#toread").removeAttr("onclick");
					   }
			  　　　　 });
				   }
			}
			
			function totest(){
				window._index = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "任务考试", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pe-task!totest.htm?id=${task.id}"
					},
					area : [ '90%', '100%' ]
				});
			}
			
			$(function(){
				if('${task.taskResult}'==1){
					$("#finished").show();
				}else{
					$("#nofinish").show();
				}
				var stime = 19;
				var interval = setInterval(function(){
					if(stime>0){
						$("#toread").html("签读"+stime);
						stime--;
					}else{
						clearInterval(interval);//取消倒计时
						//$("#toread").css("background-color","#67a909");
						//$("#toread").css("cursor","pointer");
						//$("#toread").html("签读");
						//$("#toread").attr("onclick","toread();");
						$("#toread").hide();
						$("#read").show();
					}
				},1000);
				
				
				$("#read").click(function(event){
					parent.$(document).hint('已签读');
				    $("#read").html("已签读");
				    $("#read").css("cursor","default");
				    $("#read").css("background-color","grey");
				    //$("#totest").css("cursor","pointer");
					//$("#totest").css("background-color","#67a909")
					//$("#totest").attr("onclick","totest();");
					$("#totest").hide();
					$("#test").show();
					if('${task.pbTask.isrelease}'==1){
						$.ajax({
						    url:$.fn.getRootPath()+'/app/task/pe-task!updatestudytask.htm',
						    data:{"task_id":'${task.pbTask.id}'},
							type:"post",
							timeout:5000,
							dataType:"json",
							success:function(data){
						       
						   	}
				  　　　　 });
					   }
					 event.stopPropagation();	
				})
			   
			});
			function showdetail(question){
			   parent.parent.TABOBJECT.open({
					id : 'detail',
					name : '知识详情',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=7&askContent=' + question,
					isRefresh : true
				}, this);
			}
			function showdetailtask(id){
				var index = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ "任务考试详情", 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + "/app/task/pe-task!finddetailtask.htm?id=${task.id}"
					},
					area : [ '90%', '100%' ]
				});
			}
		</script>
	</head>
	<body>
	<s:set name="todaytime" value="new java.util.Date().getTime()"/>
	<c:if test="${task.pbTask.todate.time-todaytime<0}">
		<input type="hidden" id="taskstatus" value="false" />
	</c:if>
	<input type="hidden" id="flag" value="0" />
	<div style="width: 90%;height: 500px;border: 0px red solid;">
	   <div style="height:30px;">
	      <div  style="display: none;" id="nofinish">
	      	  <s:if test="#request.task.tasktype==1 && #request.task.pbTask.isrelease==1">
		         <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="totest">考试</a><a class="taskstyle" href="javascript:void(0);" style="display: none;discursor: pointer;background-color: #67a909" id="test" onclick="totest();">考试</a></span>
		      </s:if>
		      <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: #00b0f0" id="toread">签读20</a><a class="taskstyle" href="javascript:void(0);" style="display: none;discursor: pointer;background-color: #67a909" id="read">签读</a></span>
	      </div>
	      <div style="display: none" id="finished">
	          <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="">已签读</a></span>
	          <s:if test="#request.task.tasktype==1 && #request.task.pbTask.isrelease==1">
	          　　　<span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="">已通过考试</a></span>
	      　		 <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" onclick="showdetailtask('${task.id }')" style="cursor: pointer;background-color: #67a909" id="">考试详情</a></span>　　
	      	  </s:if>
	      </div>
	   </div>
	   <div style="margin-left: 50px;height: 500px;overflow: auto">  
		   <s:iterator value="#request.rqlist" var="va" status="ind">
		     <ul style="margin-top: 10px">
		      <li style="font-weight:bold">${ind.index+1}.${va.question }<span><a href="#" onclick="showdetail('${va.question }')" style="color:red;font-size:14px;margin-left:20px">详情</a></span></li>
		      <li>${va.answer }</li>
		    </ul>  
		   </s:iterator>
	  </div>
    </div>
    <!--
     <table width="100%" cellspacing="0" cellpadding="0" class="box">
     	<tr>
     		<td>
     			 <div  style="display: none;" id="nofinish">
			      	  <s:if test="#request.task.tasktype==1 && #request.task.pbTask.isrelease==1">
				         <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="totest">考试</a><a class="taskstyle" href="javascript:void(0);" style="display: none;discursor: pointer;background-color: #67a909" id="test" onclick="totest();">考试</a></span>
				      </s:if>
				      <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: #00b0f0" id="toread">签读20</a><a class="taskstyle" href="javascript:void(0);" style="display: none;discursor: pointer;background-color: #67a909" id="read">签读</a></span>
			      </div>
			      <div style="display: none" id="finished">
			          <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="">已签读</a></span>
			          <s:if test="#request.task.tasktype==1 && #request.task.pbTask.isrelease==1">
			          　　　<span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" style="cursor: default;background-color: grey" id="">已通过考试</a></span>
			      　		 <span style="font-size:14px;float:right;"><a class="taskstyle" href="javascript:void(0);" onclick="showdetailtask('${task.id }')" style="cursor: pointer;background-color: #67a909" id="">考试详情</a></span>　　
			      	  </s:if>
			      </div>
     		</td>
     		 
     		 <td></td>
     	</tr>
     	<s:iterator value="#request.rqlist" var="va" status="ind">
     		<tr>
     			<td>${ind.index+1}.${va.question }<span><a href="#" onclick="showdetail('${va.question }')" style="color:red;font-size:14px;margin-left:20px">详情</a></span></td>
     		</tr>
     		<tr>
     			<td>${va.answer }</td>
     		</tr>
     	</s:iterator>
     </table>
     -->	  	 
	</body>
</html>

