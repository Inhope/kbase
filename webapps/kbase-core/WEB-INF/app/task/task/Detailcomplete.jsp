<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!--ztree-->		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
			var setting = {
				async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/custom/dh/dhpicker!choosedhdept.htm?taskid="+'${taskid}',
					autoParam:["id", "name", "orgtype", "level"]
				},
				callback: {
					onClick: ztreeClick
				}
			};
			//update by wilson.li at 2016-07-07
			function ztreeClick(event, treeId, treeNode){
				var top = $(document).scrollTop()+40;
				$('#tableinfo').css("margin-top",top);
				$('#tableinfo').html("");
				if(treeNode.orgtype == "USER"){//点击用户
					$.ajax({
					    url:$.fn.getRootPath()+'/app/task/pe-task!findMyComplete.htm',
					    data:{"userId":treeNode.id,"taskId":'${taskid}'},
						type:"post",
						dataType:"json",
						success:function(data){
							var html = "<tr><td colspan='"+$(data).length+"' align='center'>"+treeNode.name+"</td></tr>";
							html += "<tr>";
							$(data).each(function(i,item){
								var type = "";
								var result = "";
								if(item.taskType == 0){//学习
									type = "学习";result = "完成";
									if(item.taskResult == 0){
										result = "未完成";
									}
								}else{//考试
									type = "考试";result = "完成";
									if(item.taskResult == 0){
										result = "未完成";
									}
								}
								html += "<td align='center'>"+type+":"+result+"</td>";
							});
							html += "</tr>";
							$('#tableinfo').append(html);
						}
					});
				}else{
					$.ajax({
					    url:$.fn.getRootPath()+'/app/task/pe-task!findcomplete.htm',
					    data:{"deptid":treeNode.id,"taskid":'${taskid}',"orgtype":treeNode.orgtype},
						type:"post",
						dataType:"json",
						success:function(data){
							var da =  Math.round(parseFloat(data.completecount) / parseFloat(data.totalcount) * 10000) / 100.00 + "%";
							if(data.totalcount==0){
								da = '0%';
							} 
							var html="<tr><td colspan='3' align='center'>"+treeNode.name+"</td></tr><tr><td align='center'>部门人数</td><td align='center'>完成人数</td><td align='center'>完成率</td></tr><tr><td align='center'>"+data.totalcount+"</td><td align='center'>"+data.completecount+"</td><td align='center'>"+da+"</td></tr>";
							$('#tableinfo').append(html);
						}
					});
				}
			}
			$(function(){
				 $.fn.zTree.init($("#treeDemo"), setting);
			});
		</script>
	</head>
	<body>
		<div style="margin-top: 20px;">
			<div id="left" style="float: left;width: 29%;min-height: 80px;overflow:auto;max-height: 380px;">
				<ul id="treeDemo" class="ztree"></ul>
			</div>
			<div style="float: right;width: 65%">
				<table align="center" width="99%" cellspacing="0" cellpadding="0" class="box" id="tableinfo">
				</table>
			</div>
		</div>	
	</body>
</html>

