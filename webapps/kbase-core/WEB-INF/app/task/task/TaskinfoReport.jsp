<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/statement/css/ca.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
  	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/TaskinfoReport.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
  <style>
    .input_title{width:100%;height:auto;background:#f4f4f4;float:left;font-size:12px;line-height:35px;padding:7px 0 0 0;}
	.input_title li{height:30px;width:auto;float:left;line-height:30px;margin-bottom:7px;list-style:none;}
	.input_title li p{float:left;color:#333;margin-right:5px;width:65px;text-align:right;margin-right:2px;padding-left:10px;margin-top:0px;}
	.input_title input{border:1px solid #aaabb0;width:120px;background:#FFF;font-size:12px;float:left;padding:0 3px;color:#666;height:24px;line-height:24px;vertical-align:middle;}
	.input_title .report_title_btn{background:#c72528;text-align:center;width:50px;height:26px;line-height:26px;border:none;color:#FFF;border-radius:3px;cursor:pointer;margin-left:10px;}
	.report_title_btn:hover,.report_title_btn:active{background:#b51c1f;color:#FFF;}
  </style>
  </head>
  
  <body>
    <table>
    </table>
    <div class="input_title" id="searchCondition" style="height: 50px;">
		<form action="" method="get">
			<ul>
			<li><p>任务时间从</p><input id="start_time" name="" type="text"  readonly="readonly" onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" style="width:120px;margin:0;" /></li>
			<li><p style="width:30px;text-align:center;padding:0;">到</p><input id="end_time" name="" type="text"  onclick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" style="width:120px;" /></li>
			<li><p>任务名称:</p><input id="taskname" name="" type="text"/></li>
			<li><p>虚拟部门:</p>
			<select id="virtualdept_id">
				<s:iterator value="#request.list" var="dept">
					<option value="${dept.id }">${dept.virtualname }</option>
				</s:iterator>
			</select>
			</li>
			<li><input class="report_title_btn" type="button" id="searchBtn" value="查询" /><input id="exportBtn" class="report_title_btn" type="button" value="导出" /></li>
			</ul>
		</form>
	</div>
	
	<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
		<ul id="treeDemo" class="ztree" style="margin-top:0; width:160px;"></ul>
	</div>
	<div id="virtualdept" class="virtualdept" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
		<ul id="virtualdepttree" class="ztree" style="margin-top:0; width:160px;"></ul>
	</div>
    
  </body>
</html>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>