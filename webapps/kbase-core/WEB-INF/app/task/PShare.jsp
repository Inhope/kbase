<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/corrections/css/css.css"  />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
   <style type="text/css">
	table.box{
	    font-family:"Courier New", Courier;
	    font-size:12px;
	    border-top : 1px solid #C0C0C0;  
		border-left: 1px solid #C0C0C0;
	    padding : 0px;
	}
	.box th {
		color:#000000;
		border-right : 1px solid #C0C0C0;  
		border-bottom: 1px solid #C0C0C0; 
		text-align:right;
		width: 200px;
	}
	
	.box td {
	    font-family : "Courier New", Courier;
		font-size : 12px;
		color:#000000;
		border-right : 1px solid #C0C0C0;  
		border-bottom: 1px solid #C0C0C0;
		text-align:left;
	}
	
	<!-- ************************** -->
	.box_cell {
		font-family:"Courier New", Courier;
	    font-size:12px;
	    border:1px solid #C0C0C0;
	    padding : 0px;
	}
	
	.box_cell th {
		font-size:12px;
		text-align: right;
	}
	.box_cell td {
		text-align: left;
		padding-left: 5px;
	}
	
	.box_cell td [type="text"]{
		border:1px solid #C0C0C0;
		width: 100%;
	}
	
	.box_cell td input[type="button"], .box td button{
		cursor: pointer;
		margin-right: 5px;
		font-weight: bolder;
		height: 25px;
	}
</style>

<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.picker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>

<SCRIPT type="text/javascript">
	//保存分享
	function share(){
		$(form_shared).validate({
			rules : {
				"sharedcopedesc" : {
					required : [ "被分享的部门" ]
				}
			},
			// 验证通过时的处理
			success : function() {
				
			},
			errorPlacement : function(error, element) {
				error.appendTo(element.parent());
			},
			focusInvalid : false,
			onkeyup : false
		});
		
		var flag = $(form_shared).validate().form();
		if(flag){
			var loadi = layer.load('提交中…');
			//测试负载服务地址
			$.post($.fn.getRootPath() + sharedurl,
				{'id':id, 'sharedscope':$('#sharedscope').val()},
				function(data){
					layer.close(loadi);
					layer.alert(data.msg, -1);
					if(data.rst){//操作成功
						pageClick('1');//刷新页面
					}
			},"json");
		}
	}
	
	//绑定点击事件（部门人员选择）
	$(function (){
		$('#shareddesc').click(function(){
			$.kbase.picker.multiUserByDeptS({returnField:"shareddesc|sharedscope"});
		});
	});
</SCRIPT>


<form id="form_shared" action="" method="post">
	<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;display: none;">
		<table cellspacing="0" class="box" style="width: 100%;">
			<tr>
				<th>
					被分享的部门(人员)：
				</th>
				<td>
					<textarea id="shareddesc" name="shareddesc" rows="1" cols="60"></textarea>
					<input id="sharedscope" name="sharedscope" type="hidden" />
				</td>
			</tr>
		</table>
		<br>
		<div style="height: 28px; width: 99%; padding: 0 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
			<input type="button" onclick="javascript:share();" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
		</div>
	</div>
</form>


