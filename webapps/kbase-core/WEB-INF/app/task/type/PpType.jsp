<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
<style type="text/css">
	#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
	#rMenu ul li{
		margin: 1px 0;
		padding: 0 5px;
		cursor: pointer;
		list-style: none outside none;
		background-color: #DFDFDF;
	}
	.rTab th {text-align: left}
	.rTab td {float: left;margin-left: 25px;}
	.rTab td input {line-height: 20px;}
	.fkui_aa2{width:67px;height:28px;line-height:28px;float:right;color:#FFF;background:url(${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/fankui/ggzs_bg2.jpg) no-repeat;border:none;cursor:pointer;}
</style>		


<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript">
var fImage = $.fn.getRootPath()+"/library/ztree/css/zTreeStyle/img/diy/dept.gif";
var cImage = $.fn.getRootPath()+"/library/ztree/css/zTreeStyle/img/diy/station.png";

//***************************************zTree start****************************************
var setting = {
	edit: {
		enable: true,
		showRemoveBtn: false,
		showRenameBtn: false
	},
	view: {
		dblClickExpand: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},callback: {			
		onRightClick: onRightClick,
		beforeDrag: zTreeBeforeDrag,
		beforeDrop: zTreeBeforeDrop,
		onClick: zTreeOnClick
	}				
};
		
//右击事件
function onRightClick(event, treeId, treeNode) {
	if(treeNode){
		zTree.selectNode(treeNode);
		if (treeNode.id == "root") {
			showRMenu("root", event.clientX, event.clientY);
		}else{
			showRMenu("node", event.clientX, event.clientY);
		}
	}else{
		zTree.cancelSelectedNode();
		showRMenu("root", event.clientX, event.clientY);
	}
	
}
//拖曳前
function zTreeBeforeDrag(treeId, treeNodes){
	for (var i=0,l=treeNodes.length; i<l; i++) {
		if (treeNodes[i].id == "root")return false;
	}
	return true;
}
//拖曳操作执行前
function zTreeBeforeDrop(treeId, treeNodes, targetNode){
	//不允许拖曳成根节点
	//if(targetNode == null)return false;
	var node,nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length>0) {
		newNode = nodes[0];
		newNode.pId = targetNode.id;
		saveTreeNode(newNode,targetNode,"drag");
	}
	return false;
}
function zTreeOnClick(e, treeId, treeNode){
	//赋值分类id方便数据查询
	$("#ppTypeId").val(treeNode.id);
	//分类路径
	var ppTypeNames = treeNode.name;
	while((treeNode = treeNode.getParentNode()) != null){
		ppTypeNames = treeNode.name + "->" + ppTypeNames;
	}
	$("#ppTypeNames").text(ppTypeNames);
	$("#ppTypeNamesInput").val(ppTypeNames);
	
	//刷新分页区
	pageClick("1");
	
}
//***************************************zTree end****************************************


//***************************************显示功能 start****************************************
//显示右键菜单
function showRMenu(type, x, y) {
	$("#rMenu ul").show();
	if (type=="node") {
		$("#m_edt").show();
		$("#m_del").show();
	} else {
		$("#m_edt").hide();
		$("#m_del").hide();
	}
	
	rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});

	$("body").bind("mousedown", onBodyMouseDown);
}
//影藏右键菜单
function hideRMenu() {
	if (rMenu) rMenu.css({"visibility": "hidden"});
	$("body").unbind("mousedown", onBodyMouseDown);
}
//点击边框外影藏有点菜单
function onBodyMouseDown(event){
	if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
		rMenu.css({"visibility" : "hidden"});
	}
}
//显示编辑区
function showRContent(id, name, pId, pName){
	//编辑区赋值
	$("#nId").val(id);
	$("#nName").val(name);
	$("#pId").val(pId);
	$("#pName").val(pName);
	
	//修改z-tree树高度
	$("#rContent").show();
	var height = rHeight-130;
	$("#treeDemo").css("height",height+"px");
	
	//修改下（上）拉按钮
	$("#updown").attr("flag","0");
	$("#updown").html("&#9650;");
}
//影藏编辑区
function hideRContent(){
	//修改z-tree树高度
	$("#rContent").hide();
	$("#treeDemo").css("height",rHeight+"px");
	
	//修改下（上）拉按钮
	$("#updown").attr("flag","1");
	$("#updown").html("&#9660;");
}
//下（上）拉按钮，控制编辑区显示及影藏
function updown(){
	var flag = $("#updown").attr("flag");
	//绑定新增事件
	if(flag=="0"){
		zTree.cancelSelectedNode();
		hideRContent();
	}else{
		addTreeNode();
	}
}
//***************************************显示功能 end****************************************

//***************************************业务功能 start****************************************
//获取编辑区数据
function getNode(){
	var id = $("#nId").val();
	var name = $("#nName").val();
	var pId = $("#pId").val();
	var pName = $("#pName").val();
	if(!id||id==null||id == "null")id='';
	if(!name||name==null || name == "null")name='';
	if(!pId||pId==null || pId == "null")pId='';
	if(!pName||pName==null || pName == "null")pName='';
	return { 'id':id,'name':name,'pId':pId,'pName':pName };
}

//新增或者编辑
function saveTreeNode(newNode,node,flag){
	if(newNode.name == ''){
		layer.alert("节点名称不为空!", -1);
		return false;
	}
	if(flag == "edit"){
		if(newNode.id==''){
			layer.alert("请选择要编辑的节点!", -1);
			return false;
		}
	}
	$.ajax({
		type : 'post',
		url : $.fn.getRootPath() + '/app/task/pp-type!addOrEdit.htm', 
		data : newNode,
		async: true,
		dataType : 'json',
		success : function(data){
			if(data.result == true){
				newNode = data.content;
				if(flag == "add"){
					//z-tree添加节点
					zTree.addNodes(node, newNode);
				}else{
					//根据id获取z-tree上的节点
					var newNode_ = zTree.getNodeByParam("id", newNode.id, null);
					if(flag == "edit"){
						newNode_.name = newNode.name;
						newNode_.pId = newNode.pId;
						newNode_.icon = newNode.icon;
						//更新节点
						zTree.updateNode(newNode_);
					}else if(flag == "drag"){
						//节点移动
						newNode_.pId = newNode.pId;
						zTree.moveNode(node,newNode_, "inner");
						//更新节点图标
						if(newNode_.pId == "root"){
							newNode_.icon = fImage;
						}else{
							newNode_.icon = cImage;
						}
						zTree.updateNode(newNode_);
					}
				} 
				//影藏编辑区
				hideRContent();
				try{
					layer.close(window.__kbs_layer_index);
				}catch(e){}
			}else{
				layer.alert("操作失败!", -1);
			}
		},
		error : function(msg){
			newNode = null;
			layer.alert("网络错误，请稍后再试!", -1);
		}
	});
}

//新增
function addTreeNode(){
	//影藏右键菜单
	hideRMenu();
	//获取当前选中节点
	var node,nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length>0) {
		node = nodes[0];
	}else{
		//选中根节点
		zTree.selectNode(rootNode);
		node = rootNode;
	}
	var pId = node.id;
	var pName = node.name;
	//显示编辑区
	showRContent('','',pId,pName);
	//绑定新增事件
	$("#addOrEdt").unbind("click");
	$("#addOrEdt").bind("click",function(){
		saveTreeNode(getNode(),node,"add");
	})
}

//编辑
function editTreeNode(){
	//影藏右键菜单
	hideRMenu();
	//获取当前选中节点
	var node,nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length>0) {
		node = nodes[0];
		var id = node.id;
		var name = node.name;
		var pId = node.pId;
		var pName = node.pName;
		//显示编辑区
		showRContent(id, name, pId, pName);
		//绑定编辑事件
		$("#addOrEdt").unbind("click");
		$("#addOrEdt").bind("click",function(){
			saveTreeNode(getNode(),node,"edit");
		})
	}
}


//删除
function removeTreeNode() {
	hideRMenu();
	var nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length>0) {
		var msg = "确定需要删除当前节点？";
		if (nodes[0].children && nodes[0].children.length > 0) {
			msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
		} 
		layer.confirm(msg, function(index){
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/task/pp-type!delPpType.htm', 
				data : {'id': nodes[0].id},
				async: true,
				dataType : 'json',
				success : function(data){
					if(data.result == true){
						zTree.removeNode(nodes[0]);
						layer.alert("操作成功!", -1);
					}else{
						layer.alert("操作失败!", -1);
					}
					layer.close(index);
				},
				error : function(msg){
					newNode = null;
					layer.alert("网络错误，请稍后再试!", -1);
				}
			});
		});
	}
}
//***************************************业务功能 end****************************************


//***************************************数据初始化 start****************************************
//初始化数据
var zTree, rootNode, rMenu, rHeight;
$(document).ready(function(){
	$.ajax({
		type: "POST",
		dataType : 'json',
		timeout : 5000,
		url: $.fn.getRootPath()+'/app/util/choose!ppTypeData.htm',
		async: false,
		success: function(data){
			//部门树初始化
			zTree = $.fn.zTree.init($("#treeDemo"), setting, data);
			rootNode = zTree.getNodeByParam("id", "root", null);
			rMenu = $("#rMenu");
			rHeight = $("#treeDemo").css("height").replace("px","");
		}
	});
	
	
	//新增
	$('#btnNewCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			
			$('#pName').val(node.name);
			$('#pId').val(node.id);
			$('#nName').val('');
			$('#nId').val('');
			
			//绑定新增事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				saveTreeNode(getNode(),node,"add");
			})
			
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: false,
			    area: ['auto', 'auto'],
			    border: [5, 0.3, '#000'], //去掉默认边框
			    shade: [0], //去掉遮罩
			    //closeBtn: [0, false], //去掉默认关闭按钮
			    shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	//编辑
	$('#btnEditCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			
			if (node.id=='root'){
				layer.alert('无法编辑根节点', -1);
				return false;
			}
			
			$('#pName').val(node.pName);
			$('#pId').val(node.pId);
			$('#nName').val(node.name);
			$('#nId').val(node.id);
			
			//绑定编辑事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				saveTreeNode(getNode(),node,"edit");
			})
			
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: false,
			    area: ['auto', 'auto'],
			    border: [5, 0.3, '#000'], //去掉默认边框
			    shade: [0], //去掉遮罩
			    //closeBtn: [0, false], //去掉默认关闭按钮
			    shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	//删除
	$('#btnDelCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			if (node.id=='root'){
				layer.alert('无法删除根节点', -1);
				return false;
			}
			
			var msg = "确定需要删除当前节点？";
			if (node.children && node.children.length > 0) {
				msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
			} 
			layer.confirm(msg, function(index){
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + '/app/task/pp-type!delPpType.htm', 
					data : {'id': node.id},
					async: true,
					dataType : 'json',
					success : function(data){
						if(data.result == true){
							zTree.removeNode(node);
							layer.alert("操作成功!", -1);
						}else{
							layer.alert("操作失败!", -1);
						}
						layer.close(index);
					},
					error : function(msg){
						newNode = null;
						layer.alert("网络错误，请稍后再试!", -1);
					}
				});
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	//因为页面整体刷新，z-tree节点回显
	var ppTypeId = $("#ppTypeId").val();
	if(ppTypeId != null && ppTypeId != ''){
		var node = zTree.getNodeByParam("id", ppTypeId, null);
		if(node != null){
			zTree.selectNode(node);
		}
	}
});
//***************************************数据初始化 end****************************************
</script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<div id="catePanel" class="easyui-panel" title="" style="padding:5px;border: 0px;">
	<button class="easyui-linkbutton" id="btnNewCate">新增</button>
	<button class="easyui-linkbutton" id="btnEditCate">编辑</button>
	<button class="easyui-linkbutton" id="btnDelCate">删除</button>
</div>
<div id="treeDemo" class="ztree" style="height:90%;border:0px solid #000;;margin-top:0; overflow-y: auto; overflow-x: auto;"></div>
<div id="rContent" style="border-bottom: 1px solid #e3e3e3;display:none;">
	<table class="rTab" width="100%">
		<tr>
			<th>所属分类</th>
		</tr>
		<tr>
			<td>
				<input id="pName" type="text" readonly="readonly"/>
				<input id="pId" type="hidden" />
			</td>
		</tr>
		<tr>
			<th>当前分类</th>
		</tr>
		<tr>
			<td>
				<input id="nName" type="text" />
				<input id="nId" type="hidden" />
			</td>
		</tr>
		<tr>
			<td style="float: right;margin-right: 15px;margin-top: 5px;">
				<a href="###" id="addOrEdt"><span class="fkui_aa2" style="text-align: center;">提交</span></a>
			</td>
		</tr>
	</table>
</div>
<div id="rMenu" style="width: 60px;z-index: 999999;">
	<ul>
		<li id="m_add" onclick="addTreeNode();">增加节点</li>
		<li id="m_edt" onclick="editTreeNode();">编辑节点</li>
		<li id="m_del" onclick="removeTreeNode();">删除节点</li>
	</ul>
</div>
