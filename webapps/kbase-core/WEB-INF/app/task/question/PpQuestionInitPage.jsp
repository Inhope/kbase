<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>题库管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		
		<jsp:include page="/resource/task/task_header.jsp"></jsp:include>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		
		<style type="text/css">
			
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/question/question.js"></script>
		<script type="text/javascript">
			var fImage = $.fn.getRootPath()+"/library/ztree/css/zTreeStyle/img/diy/dept.gif";
			var cImage = $.fn.getRootPath()+"/library/ztree/css/zTreeStyle/img/diy/station.gif";
			
			//***************************************zTree start****************************************
			var setting = {
				edit: {
					enable: true,
					showRemoveBtn: false,
					showRenameBtn: false
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},callback: {			
					onRightClick: onRightClick,
					beforeDrag: zTreeBeforeDrag,
					beforeDrop: zTreeBeforeDrop,
					onClick: zTreeOnClick
				}				
			};
					
			//右击事件
			function onRightClick(event, treeId, treeNode) {
				if(treeNode){
					zTree.selectNode(treeNode);
					if (treeNode.id == "root") {
						showRMenu("root", event.clientX, event.clientY);
					}else{
						showRMenu("node", event.clientX, event.clientY);
					}
				}else{
					zTree.cancelSelectedNode();
					showRMenu("root", event.clientX, event.clientY);
				}
				
			}
			//拖曳前
			function zTreeBeforeDrag(treeId, treeNodes){
				for (var i=0,l=treeNodes.length; i<l; i++) {
					if (treeNodes[i].id == "root")return false;
				}
				return true;
			}
			//拖曳操作执行前
			function zTreeBeforeDrop(treeId, treeNodes, targetNode){
				//不允许拖曳成根节点
				//if(targetNode == null)return false;
				var node,nodes = zTree.getSelectedNodes();
				if (nodes && nodes.length>0) {
					newNode = nodes[0];
					newNode.pId = targetNode.id;
					saveTreeNode(newNode,targetNode,"drag");
				}
				return false;
			}
			function zTreeOnClick(e, treeId, treeNode){
				//赋值分类id方便数据查询
				$("#ppTypeId").val(treeNode.id);
				//分类路径
				var ppTypeNames = treeNode.name;
				while((treeNode = treeNode.getParentNode()) != null){
					ppTypeNames = treeNode.name + "->" + ppTypeNames;
				}
				$("#ppTypeNames").text(ppTypeNames);
				$("#ppTypeNamesInput").val(ppTypeNames);
				
			}
			//***************************************zTree end****************************************
		
			//***************************************数据初始化 start****************************************
			//初始化数据
			var zTree, rootNode, rMenu, rHeight;
			$(document).ready(function(){
				$.ajax({
					type: "POST",
					dataType : 'json',
					timeout : 5000,
					url: $.fn.getRootPath()+'/app/util/choose!ppTypeData.htm',
					async: false,
					success: function(data){
						//部门树初始化
						$.fn.zTree.init($("#treeDemo"), setting, data);
						zTree = $.fn.zTree.getZTreeObj("treeDemo");
						rootNode = zTree.getNodeByParam("id", "root", null);
						rMenu = $("#rMenu");
						rHeight = $("#treeDemo").css("height").replace("px","");
					}
				});
			});
			//***************************************数据初始化 end****************************************
		</script>
	</head>
	<body>
		<jsp:include page="/resource/task/task_menu_begin.jsp"></jsp:include>
				
				<div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'west',split:true" title="" style="width:160px;">
						<jsp:include page="../type/PpType.jsp"></jsp:include>
					</div>
					<div data-options="region:'center',title:''">
						<div id="p" class="easyui-panel" data-options="fit:true" style="border:0px;">
							
							
							<form id="form0" action="" method="post">
								<!--题库分类  -->
								<input type="hidden" name="pbTaskModel.ppType.id" id="ppTypeId"  value="${pbTaskModel.ppType.id}"/>
								<input type="hidden" name="pbTaskModel.ppType.path" id="ppTypeNamesInput"  value="${pbTaskModel.ppType.path}"/>
								<div class="content_right_bottom">
									<div class="gonggao_titile">
										<div style="float: left;"><b id="ppTypeNames" style="">${pbTaskModel.ppType.path}</b></div>
										<div class="gonggao_titile_right">
										   	<a href="###"  id="export">导出全部</a>
										   	<a href="###"  id="import">导入</a>
											<a href="javascript:void(0);" onclick="openShade(this.id)" id="ppQuestionDel">删除试题</a>
											<a href="###" onclick="openShade(this.id)" id="ppQuestionEdit">编辑试题</a>
											<a href="###" onclick="openShade(this.id)" id="ppQuestionAdd">新增试题</a>
											<a href="###" onclick="openShade(this.id)" id="ppQuestionMove">移至目录</a>
										</div>
									</div>
									<div class="yonghu_titile">
										<ul>
											<li>
												试题题目：
												<input type="text" id="remark_select"
													name="pbTaskModel.questionName"
													value="${pbTaskModel.questionName}" />
											</li>
											<li>
												试题类型：
												<s:select id="checkType_select" name="pbTaskModel.questionType"
													headerKey="" headerValue="-请选择-" list="#{'0':'判断','1':'单选','2':'多选'}"></s:select>
											</li>
											<li>
												创建人：
												<input type="text" id="remark_select"
													name="pbTaskModel.user.userInfo.userChineseName"
													value="${pbTaskModel.user.userInfo.userChineseName}" />
											</li>
											<li class="anniu">
												<a href="javascript:void(0)"><input type="button"
														class="youghu_aa1" value="重置" onclick="selectReset()" /> </a>
												<a href="javascript:void(0)"><input type="button"
														class="youghu_aa2" value="提交"
														onclick="javascript:pageClick('1');" /> </a>
											</li>
										</ul>
									</div>
				
									<div class="gonggao_con">
										<div class="gonggao_con_nr">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr class="tdbg">
													<td width="10%">
														<input type="checkbox" value="" onclick="checkAll()" id=""/>
													</td>
													<td width="50%">
														试题题目
													</td>
													<td width="10%">
														试题类型
													</td>
													<td width="10%">
														创建人
													</td>
													<td width="20%">
														创建时间
													</td>
												</tr>
												<s:iterator value="page.result" var="va">
													<tr>
														<td>
															<input name="ppQuestionId_list" type='checkbox'
																value="${va.id }" />
														</td>
														<td>
															${va.questionName}&nbsp;
														</td>
														<td>
														
															<s:if test="#va.questionType==0">
																判断
															</s:if>
															<s:elseif test="#va.questionType==1">
																单选
															</s:elseif>
															<s:elseif test="#va.questionType==2">
																多选
															</s:elseif>
														</td>
														<td>
															${va.user.userInfo.userChineseName}&nbsp;
														</td>
														<td>
															<s:date name="#va.createDate" format="yyyy-MM-dd HH:mm" />
															&nbsp;
														</td>
													</tr>
												</s:iterator>
												<tr class="trfen">
													<td colspan="6">
														<jsp:include page="../../util/part_fenye.jsp"></jsp:include>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</form>
							
							
						</div>
					</div>
				</div>
					
	<jsp:include page="/resource/task/task_menu_end.jsp"></jsp:include>
	
	
	<!--******************导入弹出框***********************************************************************************************-->
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/task/css/piliang_dan.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/AjaxFileUpload.js"></script>
		<script type="text/javascript">
			function check(obj){
				var fileName = obj.value;
				var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
				if (suffix != ('.xls')&&suffix != ('.xlsx')) {
					parent.layer.alert("请用模板文件做为上传文件!");
					obj.parentElement.innerHTML += '';
				}
			}
		</script>
		<div id="ImportShade" class="ImportShade" style="display:none;position:fixed;z-index:10001;_position:absolute;">
			<div class="ImportShade_d">
				<div class="ImportShade_dan">
					<div class="ImportShade_dan_title">
						<b id="title_importOrExport"></b><a href="javascript:void(0)" id="ImportShadeClose"><img  id="imgShadeClose" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"
								width="12" height="12" />
						</a>
					</div>
					<div class="ImportShade_dan_con">
						<ul>
							<li id="scBefore" >
								<span><a href="javascript:void(0)">下载模板</a></span>
								<p>
									注：请严格按照模板(<a href="${pageContext.request.contextPath}/app/task/pp-question!downloadMouldFile.htm" style="color: blue">点击下载</a>)填写相关数据
									<br />
									特别提示：
									<br />
									1、支持的EXCEL为2003\2007\2010\2013
									<br />
									2、上传需要通过修改模板内容来完成
									<br />
									3、上传内容请严格按照模板要求来填写
									<br />
								</p>
							</li>
							<li id="scAfter" >
								<span><a href="javascript:void(0)">处理结果</a></span>
								<p>
									注：错误文件中标有不同颜色区分的内容为需要修改的内容，请修改完毕后提交这个错误文件
									<br />
									消息提示：
									<br />
								</p>
							</li>
							<li id="importWarning" >
								<span><a href="javascript:void(0)" id="importWarning_title">上传中</a></span>
								<p>
									总计：<b id="rowTotal" style="color: red; size: 24px;">0</b>条，
									成功：<b id="fuccessTotal" style="color: red;size: 24px;">0</b>条，
									失败：<b id="failureTotal" style="color: red;size: 24px;">0</b>条
									<br />
								</p>
							</li>
							<li id="importHandle" class="anniu">	
								<p>
									<!--<input name="" type="text" />&nbsp;<a style="float: r" class="userImportShade_aa1" href="javascript:void(0)">浏览</a>
									-->
									<input type="file" id="userFile" name="userFile" onchange="check(this)" />
								</p>
								<span>
									<a class="ImportShade_aa1" href="javascript:void(0)" id="importQuestion">上传</a>
								</span>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</div>
		
	
		
	</body>
</html>

