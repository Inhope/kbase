<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css"/>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		
		<!-- ztree -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/question/questionEdit.js"></script>
		<script type="text/javascript">
			var questionType='${ppQuestion.questionType}';
			var ppAnswer='${ppAnswer}';
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" name="ppQuestion.id" value="${ppQuestion.id }" />
			<table class="box" align="center" style="width:100%">
				<tr>
					<td>
						移至目录
					</td>
					<td>
						<input type="hidden" name="ppQuestion.ppType.id" value="${ppQuestion.ppType.id}"/>
						<input id="ppTypeCheck" type="text" name="ppQuestion.ppType.path" value="${ppQuestion.ppType.path }" style="width:400px;" readonly="readonly"/>
						<div style="position: fixed; z-index: 10002; background-color: white; border: 1px solid #C0C0C0; display: none;">
							<div style="float:right;margin-right:5px;">
								<a id="treeChecked" href="javaScript:void(0);">[确定]</a>
								<a href="javaScript:void(0);">[关闭]</a>
							</div>
							<ul id="ppTypeCheckTree" class="ztree" style="height:200px;width:400px;overflow:auto;"></ul>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						已选题目
					</td>
					<td>
						<div>
							<s:iterator value="#request.questionlist" var="question" status="ind">
								${ind.index+1 }.${question.questionName }<input type="hidden" name="question_id" value="${question.id}"/><br/>
							</s:iterator>
						</div>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" align="right">
						<input type="button" id="tomove" class="userShade_aa2" value="提交" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

