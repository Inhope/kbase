<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>任务管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/theme/table-form.css" rel="stylesheet" type="text/css"/>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.validate.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/messages_cn.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!-- choose -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/util_choose.js"></script>
		
		<!-- ztree -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/task/question/questionEdit.js"></script>
		<script type="text/javascript">
			var questionType='${ppQuestion.questionType}';
			var ppAnswer='${ppAnswer}';
		</script>
	</head>
	<body>
		<form name="form1" id="form1" method="post" action="">
			<input type="hidden" name="ppQuestion.id" value="${ppQuestion.id }" />
			<table class="box" align="center" style="width:100%">
				<tr>
					<td>
						关联目录
					</td>
					<td>
						<input type="hidden" name="ppQuestion.ppType.id" value="${ppQuestion.ppType.id}"/>
						<input id="ppTypeCheck" type="text" name="ppQuestion.ppType.path" value="${ppQuestion.ppType.path }" style="width:400px;" readonly="readonly"/>
						<div style="position: fixed; z-index: 10002; background-color: white; border: 1px solid #C0C0C0; display: none;">
							<div style="float:right;margin-right:5px;">
								<a id="treeChecked" href="javaScript:void(0);">[确定]</a>
								<a href="javaScript:void(0);">[关闭]</a>
							</div>
							<ul id="ppTypeCheckTree" class="ztree" style="height:200px;width:400px;overflow:auto;"></ul>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						试题题目
					</td>
					<td>
						<textarea name="ppQuestion.questionName" style="resize: none;height:100px;width:400px;" maxlength="500">${ppQuestion.questionName}</textarea>
					</td>
				</tr>
				<tr>
					<td>
						试题类型
					</td>
					<td>
						 <input type="radio" name="ppQuestion.questionType" value="0" <s:if test="ppQuestion.questionType==0">checked="checked"</s:if>>判断</input>
						 <input type="radio" name="ppQuestion.questionType" value="1" <s:if test="ppQuestion.questionType==1">checked="checked"</s:if>>单选</input>
						 <input type="radio" name="ppQuestion.questionType" value="2" <s:if test="ppQuestion.questionType==2">checked="checked"</s:if>>多选</input>
					</td>
				</tr>
				
				<tr style="display:none;">
					<td style="display:none;">
						答案
					</td>
					<td>
						<div ansType="0" style="display:none;">
							<input type="radio" name="ppQuestion.isRight" value="1" <s:if test="ppQuestion.isRight==1">checked="checked"</s:if>>正确</input>
							<input type="radio" name="ppQuestion.isRight" value="0" <s:if test="ppQuestion.isRight==0">checked="checked"</s:if>>错误</input>
						</div>
						<div ansType="1" style="display:none;">
							<div class="all_ans">
								<div>
									<input type="radio" name="ppAnswers.isRight" value="0" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="radio" name="ppAnswers.isRight" value="1" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="radio" name="ppAnswers.isRight" value="2" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="radio" name="ppAnswers.isRight" value="3" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
							</div>
							<div>
								<input type="button" class="ansAdd" value="增加一条" />
							</div>
						</div>
						<div ansType="2" style="display:none;">
							<div class="all_ans">
								<div>
									<input type="checkbox" name="ppAnswers.isRight" value="0" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="checkbox" name="ppAnswers.isRight" value="1" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="checkbox" name="ppAnswers.isRight" value="2" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
								<div>
									<input type="checkbox" name="ppAnswers.isRight" value="3" />
									<input type="hidden" name="ppAnswers.id" />
									<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>
									<input type="button" class="ansDel" value="删除" />
								</div>
							</div>
							<div>
								<input type="button" class="ansAdd" value="增加一条" />
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<input type="button" id="submit" class="userShade_aa2" value="提交" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

