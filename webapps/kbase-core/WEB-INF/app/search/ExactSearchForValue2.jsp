<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@page import="com.eastrobot.util.SystemKeys"%>

<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gaoji.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSUtils.js?20160617"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSTools.js?20160617"></script> 
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ExactSearchForValue.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		
		<script type="text/javascript">
			var askContent = '<s:property value="#request.content.split(' ')[0]"/>';
			$(function(){
			
				$('div.bh').each(function(){
					var bh =$(this).html();
					var category = '<a style="cursor:pointer;text-decoration:underline" bh="'+$(this).attr('bh')+'">'
						+ bh.substr(bh.lastIndexOf('--&gt; ')+7)
						+ '</a>';
					var start = bh.indexOf('--&gt; ')+7;
					var end = bh.lastIndexOf('--&gt; ')+7;
					var text =  bh.substr(0, end);
					text = text.substr(start,text.length);
					text = text+category;
					if($.trim(bh.replace(new RegExp('--&gt;', 'ig'), '')) == ''){
						text = '未找到分类';
					}
					$(this).html(text);
				});
				
				$('div.bh a').click(function(){
					/**
					*@lvan.li 20160505 针对广州移动模糊搜索出来二维表格中的知识点，点击目录不能跳转问题优化
					* isCate = true 则为非叶子节点，点击目录跳转到二维表格页面，否则按照正常逻辑跳转到分类展示页面
					*/
					var isCate = true;
					var bh = $(this).attr('bh');
					var end = bh.length-1
					if(bh.charAt(end) == 'L'){
						isCate = false;
					}
					var url = $.fn.getRootPath()+"/app/category/category.htm?type=1&categoryId="+$(this).parent().attr('categoryId')+"&categoryName="+$(this).html() + '&t=' +new Date().getTime();
					if(isCate){
						url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id=' + $(this).parent().attr('categoryId')+'&isCate='+isCate + '&t=' +new Date().getTime();
					}
					parent.TABOBJECT.open({
						id : $(this).parent().attr('categoryId'),
						name : $(this).text(),
						hasClose : true,
						url : url,
						isRefresh : true
					}, this);
				});
			});
		</script>
	</head>

	<body>
		<div class="content_right_bottom">
			<div class="gaoji_title">
				<div class="gaoji_title_left">
					精准搜索，共
					<span>${totalCount}</span>条记录
				</div>
				<div class="gaoji_title_right">
					<a href="javascript:void(0);">预览</a>
					<input type="checkbox"/>
				</div>
			</div>
			<div class="gaoji_con">
				<div class="gaoji_left">
					<div class="gaoji_con_title">
						<h1 style="width: 75%"> 
							<b>
								知识名称
							</b> 
						</h1>
						<h2 style="width: 15%"> 修改时间 </h2>
						<h3 style="width: 5%"> 热度 </h3>
						<h5 style="width: 5%"></h5>
					</div>
					<div class="gaoji_con_nr">
						<s:if test="#request.list == null || #request.list.isEmpty()">
							<ul style="min-height: 200px;"></ul>
						</s:if>
						<s:else>
							<s:iterator var="clist" value="#request.list" id="clist">
								<div>
								<s:iterator var="ov" value="#clist" status="status">
									<s:if test="#status.index==0">
									<div categoryId="${ov.cid}" class="bh" style="font-size:14px;font-family:微软雅黑;line-height:28px;margin-left:20px;font-weight:bold;">${ov.bhname}</div>
									</s:if>
									<ul <s:if test="#status.index > 2"> style="display: none" class="moreul"</s:if> >
										<li class="dangqian">
											<div class="dangqian_1">
												<h1 style="width: 75%"> 
												<a title="${ov.question}" href="javascript:void(0);" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:70%;text-align:left;">
													&nbsp;&nbsp;&nbsp;&nbsp;● <s:property value="#ov.question" escape="false"/>
												</a> 
												<a class="_ymzs" href="javascript:void(0);" style="width:auto;margin-top: 3px;" id="${ov.qid}" ftp="0"><img style="width:24px;height:24px;"  src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/category.png"/></a>
												<s:if test='#ov.isableSMS==true'>
												<a class="_ymzs" href="javascript:void(0);" style="width:auto;margin-top: 3px;" id="${ov.qid}" ftp="1"><img style="width:24px;height:24px;"  src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/sms.png"/></a>
												<div style="display:none;">
												    <textarea id="${ov.qid }_question" cols="" rows="" isableSMS="${ov.isableSMS}">${ov.question}</textarea>
													<textarea id="${ov.qid }_answer" cols="" rows=""  >${ov.answer}</textarea> 												
												</div>
												</s:if>											
											</h1>
											<h2 style="width: 15%"> <s:date name="#ov.edittime"/> </h2>
											<h5 style="width: 5%"> <s:property value="#ov.dscount"/> </h5>
											
											<kbs:if test="<%=!SystemKeys.isGzyd() %>">
												<h6 style="width: 5%"> <a href="javascript:void(0);" value="<s:property value="#ov.question"/>" title_="<s:property value="#ov.objname"/>"><font color="#CC0000">详情</font></a> </h6> 
											</kbs:if>
											</div>
											<div class="dangqian_2" style="display: none;">
												<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
													<!--  <s:iterator value="#ovw.url_p4">
														<tr>
															<td align="center">
																<iframe frameborder="0" height="300" width="100%" src="${robotContextPath}p4data/<s:property/>"> </iframe>
															<br></td>
														</tr>
													</s:iterator>
													 -->
												</table>
												<div>
												<s:property value="#ov.answer" escape="false"/>
											</div>
										</div>
										</li>
									</ul>
									<s:if test="#clist.size() > 3">
										<s:if test="#status.index==2">
											<ul>
												<li>
													<a class="moreQa" href="javascript:void(0);" style="font-size:14px;font-family:微软雅黑;line-height:28px;margin-left:20px;font-weight:bold;color:#e30000">&darr;显示更多</a>
												</li>
											</ul>
											<div style="clear:both;"></div>
										 </s:if>
									</s:if>
								</s:iterator>
								<div>
							</s:iterator>
						</s:else>
						<div class="gonggao_con_nr_fenye">
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">首页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">上一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">上一页</a>
							</s:else>
							<s:iterator begin="1" end="#request.arrayPage.pageTotalNo" status="i">
								<s:if test="(#i.index + 1) + 4 > #request.arrayPage.pageNo && (#i.index + 1) - #request.arrayPage.pageNo < 5">
									<s:if test="(#i.index + 1) == #request.arrayPage.pageNo">
										<a href="javascript:void(0)" class="dang" onfocus="this.blur();"><s:property/></a>
									</s:if>
									<s:else>
										<a href="javascript:void(0)" onfocus="this.blur();" start="<s:property value="#i.index * #request.arrayPage.pageSize"/>" limit="${arrayPage.pageSize}"><s:property/></a>
									</s:else>
								</s:if>
							</s:iterator>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">下一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">下一页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">尾页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a>
							</s:else>
							<div id="currentPage" style="display: none;">
								<input id="start" value="${arrayPage.start}">
								<input id="pageSize" value="${arrayPage.pageSize}">
								<input id="totalPage" value="${arrayPage.pageTotalNo}">
							</div>
						</div>
					<form id="fenyeForm" method="get" style="display: none;"
						action="${pageContext.request.contextPath}/app/search/exact-search.htm">
						<input type="text" hidden="hidden" name="start" value="${arrayPage.nextPageStart}" />
						<input type="text" hidden="hidden" name="content" value="<s:property value="#request.content.split(' ')[0]"/>" />
						<input type="text" hidden="hidden" name="searchType" value="4" />
					</form>
					</div>
		
	</body>
</html>