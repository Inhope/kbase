<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML >
<html>
  <head>
  	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
  	<title>实例版本-广州移动分类展示右键菜单版本对比</title>
	<style type="text/css">
		html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
		a:focus{outline:none;}
	</style>
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dianzi_dan1.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
  	<script type="text/javascript">
		$(function(){
			function renderComp (versionIds) {
				window.opener.parent.TABOBJECT.open({
					id : 'comparison_object',
					name : '实例对比',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/object-search!comparisonObject.htm?objId=${param.objId }&versionIds=' + versionIds,
					isRefresh : true
				}, this);
			}
			
			var compBtn = $('div.xgai a.tijiao input[type=button]');
			compBtn.click(function() {
				var checkVserion = $('table input[type=checkbox]:checked');
				if(checkVserion.size() == 0) {
					alert('请选择比对项')
				} else if(checkVserion.size() == 2) {
					var versionIds = new Array(), bl = true;
					$.each(checkVserion, function(i) {
						if($("#"+$(this).attr('id')+"_1").length>0){
							alert('不应选择已经屏蔽的数据!');
							return bl = false;
						}
						versionIds.push($(this).attr('id') + ';' + $(this).attr('index'));
					});
					if(bl) renderComp(versionIds.join(','));
				} else {
					alert('只能请选择两项进行对比');
				}
			});
		});
		
	</script>
  
  </head>
  
  <body>
    <div class="index_dan_con" style="width: 100%;padding: 0 0;">
		<ul>
			<li class="biaoge">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="biaoti" align="center">
							版本号
						</td>
						<td class="biaoti" align="center">
							版本变更时间
						</td>
						<td class="biaoti" align="center">
							版本变更原因
						</td>
					</tr>
					<%--是否有屏蔽/公开权限 --%>
					<s:set var="maskObject" value="1" ></s:set>
					<s:iterator value="#session.session_user_key.roles" var="va1">
						<s:iterator value="#va1.menus" var="va2">
							<s:if test="#va2.key == 'K-maskObject'"><s:set var="maskObject" value="0" ></s:set></s:if>
						</s:iterator>
					</s:iterator>
					<s:iterator var="sr" value="#request.sntryRevisions" status="i">
						<s:if test='#maskObject=="0"'>
							<tr>
								<td align="center">
									<input type="checkbox" id="${sr.revisionId}" index="<s:property value="#request.sntryRevisions.size() - #i.index"/>"/>
									<s:property value="#request.sntryRevisions.size() - #i.index"/>
									<s:if test="#i.index == 0">
										(当前)
									</s:if>
									<span id="${sr.revisionId}_${sr.status }"><s:if test='#sr.status == "1"'>(屏蔽)</s:if></span>
								</td>
								<td align="center">
									<s:date format="yyyy-MM-dd HH:mm:ss" name="#sr.commitTime"/>
								</td>
								<td align="center">
									${sr.message}
								</td>
							</tr>
						</s:if>
						<s:else>
							<s:if test='#sr.status == "0"'>
								<tr>
									<td align="center">
										<input type="checkbox" id="${sr.revisionId}" index="<s:property value="#request.sntryRevisions.size() - #i.index"/>"/>
										<s:property value="#request.sntryRevisions.size() - #i.index"/>
										<s:if test="#i.index == 0">
											(当前)
										</s:if>
										<span id="${sr.revisionId}_${sr.status }"><s:if test='#sr.status == "1"'>(屏蔽)</s:if></span>
									</td>
									<td align="center">
										<s:date format="yyyy-MM-dd HH:mm:ss" name="#sr.commitTime"/>
									</td>
									<td align="center">
										${sr.message}
									</td>
								</tr>
							</s:if>
						</s:else>
					</s:iterator>
				</table>
			</li>
		</ul>
	</div>
	<div class="xgai">
		<a href="javascript:void(0);" class="tijiao"><input type="button" value="对比" /></a>
	</div>
  </body>
</html>
