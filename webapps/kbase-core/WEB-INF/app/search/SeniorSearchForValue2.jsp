<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@page import="com.eastrobot.util.WukongUtil"%>
<%@page import="com.eastrobot.module.object.vo.AttachFile"%>

<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gaoji.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/resource/docicons/css/doc-icons.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSUtils.js?20160617"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSTools.js?20160617"></script>  
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForValue.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForm.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForValue2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/Attachment.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert-1.0.2.js"></script>
		
		<script type="text/javascript">
			var categoryId = '${categoryId}';
			var objname = '${objname}'
			var swfAddress='${swfAddress}';
			var enableAdvReview = (swfAddress.indexOf("/attachment!")==-1);	//是否开启高级预览模式
			
			var questionId = '${questionId}';
			
			var askContent = '<s:property value="#request.content.split(' ')[0]"/>';
			$(function(){
			
				$('div.bh').each(function(){
					var bh =$(this).html();
					var category = '<a style="cursor:pointer;text-decoration:underline" bh="'+$(this).attr('bh')+'">'
						+ bh.substr(bh.lastIndexOf('--&gt; ')+7)
						+ '</a>';
					var start = bh.indexOf('--&gt; ')+7;
					var end = bh.lastIndexOf('--&gt; ')+7;
					var text =  bh.substr(0, end);
					text = text.substr(start,text.length);
					text = text+category;
					if($.trim(bh.replace(new RegExp('--&gt;', 'ig'), '')) == ''){
						text = '未找到分类';
					}
					$(this).html(text);
				});
				
				$('div.bh a').click(function(){
					
					/**
					*@lvan.li 20160505 针对广州移动模糊搜索出来二维表格中的知识点，点击目录不能跳转问题优化
					* isCate = true 则为非叶子节点，点击目录跳转到二维表格页面，否则按照正常逻辑跳转到分类展示页面
					*/
					var isCate = true;
					var bh = $(this).attr('bh');
					var end = bh.length-1
					if(bh.charAt(end) == 'L'){
						isCate = false;
					}
					var url = $.fn.getRootPath()+"/app/category/category.htm?type=1&categoryId="+$(this).parent().attr('categoryId')+"&categoryName="+$(this).text() + '&t=' +new Date().getTime();
					if(isCate){
						url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id=' + $(this).parent().attr('categoryId')+'&isCate='+isCate + '&t=' +new Date().getTime();
					}
					parent.TABOBJECT.open({
							id : $(this).parent().attr('categoryId'),
							name :  $(this).text(),
							hasClose : true,
							url : url,
							isRefresh : true
					}, this);
				});
				
				//add by eko.zhan at 2016-01-13 IE8 兼容IE7 文档模式样式有误，请忽略这段傻逼代码
				if ($.browser.msie &&  $.browser.version<8){
					window.setTimeout(function(){
						$('div.gaoji_title_right a').click();
					}, 200);
					
					window.setTimeout(function(){
						$('div.gaoji_title_right a').click();
					}, 400);
				}
				
				if (enableAdvReview){
					$.kbase.convert.init({
						contextPath: swfAddress
					});
				}
				
			});
		</script>
	</head>

	<body>
		<div class="content_right_bottom">
			<form action="${pageContext.request.contextPath}/app/search/senior-search.htm" method="post">
			<div style="height:14px;"><span style="line-height:14px;float:right;cursor:pointer;"><a id="showgj" >&darr;显示高级搜索条件</a></span></div>	
			<div class="gaoji" style="display:none;">
				<div class="gaoji_1">
					<ul>
						<li>
							<p>
								搜索内容
							</p>
							<input type="hidden" value="<s:property value="#request.content.split(' ')[0]"/>"/>
							<input id="content" name="content" type="text"/>
						</li>
						<li>
							<p>
								搜索类型
							</p>
							<label>
								<s:if test="#request.searchType == 1">
									<input type="radio" name="searchType" value="1" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="1"/>
								</s:else>
								全文
							</label>
							<label>
								<s:if test="#request.searchType == 2">
									<input type="radio" name="searchType" value="2" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="2"/>
								</s:else>
								实例
							</label>
							<label>
								<s:if test="#request.searchType == 3">
									<input type="radio" name="searchType" value="3" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="3"/>
								</s:else>
								附件
							</label>
						</li>
						<li>
							<p>知识目录</p>
							<select id="catgoryDirId" name="catgoryDirId">
								<option value="" selected="selected">全部</option>
								<s:iterator var="oc" value="#session.ontologyCategorys">
									<option value="<s:property value="#oc.id"/>" <s:if test="#oc.id == #request.catgoryDirId">selected="selected"</s:if>>
										<s:property value="#oc.name"/>
									</option>
								</s:iterator>
							</select>
						</li>
					</ul>
				</div>
				<div class="gaoji_2">
					<ul>
						<li>
							<p>
								创建日期
							</p>
							<input id="startDate" name="startDate" type="text" value="${startDate}" class="Wdate"/>
						</li>
						<li>
							<p>
								知识时效
							</p>
							<select id="agingType" name="agingType">
								<option value="0">全部</option>
								<s:if test="#request.agingType == 1">
									<option selected="selected" value="1">常态</option>
								</s:if>
								<s:else>
									<option value="1">常态</option>
								</s:else>
								<s:if test="#request.agingType == 2">
									<option selected="selected" value="2">未过期</option>
								</s:if>
								<s:else>
									<option value="2">未过期</option>
								</s:else>
								<s:if test="#request.agingType == 3">
									<option selected="selected" value="3">未过追溯期</option>
								</s:if>
								<s:else>
									<option value="3">未过追溯期</option>
								</s:else>
								<s:if test="#request.agingType == 4">
									<option selected="selected" value="4">已过追溯期</option>
								</s:if>
								<s:else>
									<option value="4">已过追溯期</option>
								</s:else>
							</select>
						</li>
					</ul>
				</div>
				<div class="gaoji_3">
					<ul>
						<li>
							<p>
								至
							</p>
							<input id="endDate" name="endDate" type="text" value="${endDate}" class="Wdate"/>
						</li>
						<s:if test="#session.locationEnable">
								<li><p></p></li>
						</s:if>
						<s:else>
							<li>
								<p>地区</p>
								<select id="location" name="location">
									<option value="">全部</option>
									<s:iterator var="dt" value="#session.dimTags">
										<option value="<s:property value="#dt.tag"/>" <s:if test="#request.location == #dt.tag">selected="selected"</s:if>><s:property value="#dt.name"/></option>
									</s:iterator>
								</select>
							</li>
						</s:else>
						<s:if test="#session.brandEnable">
								<li><p></p></li>
						</s:if>
						<s:else>
							<li>
								<p>品牌</p>
								<select id="brand" name="brand">
									<option value="">全部</option>
									<s:iterator var="dt" value="#session.brands">
										<option value="<s:property value="#dt.tag"/>" <s:if test="#request.brand == #dt.tag">selected="selected"</s:if>><s:property value="#dt.name"/></option>
									</s:iterator>
								</select>
							</li>
						</s:else>
						<li>
							<input class="an1" type="button" value="搜索" />
							<input class="an2" type="reset" value="重置" />
						</li>
					</ul>
				</div>
			</div>
			
			<div class="slzs_con" style="height:auto;<s:if test='#request.question==null || #request.answer==null'>display:none;</s:if>">
			<div class="slzs_left" style="margin-right:0;height:auto;">
				<div class="slzs_con_nr">
					<div class="dangqian">
						<div class="dangqian_1">
								<kbs:if test="<%=SystemKeys.isGzyd() %>">
									<h5>
										<s:if test="#request.isableSMS==true">
										<a style="float:right;" href="javascript:void(0);" id="gzyd_sms" onclick="SMSTools.sms.send('','');">短信发送</a>
										<div style="display:none;">
											<textarea id="gzyd_sms_question" cols="" rows="" isableSMS="${isableSMS}"
											gzyd_sms_qId="${questionId}" gzyd_sms_cId="${categoryId}">${question}</textarea>
											<textarea id="gzyd_sms_answer" cols="" rows="" >${answer}</textarea> 												
										</div>
										</s:if>									
										<a style="float:right;" href="javascript:void(0);" id="gzyd_flzs">页面展示</a>
									</h5>
								</kbs:if>
								<kbs:if test="<%=!SystemKeys.isGzyd() %>">
								  	<a style="float:right;text-decoration:underline" class="detail" href="javascript:void(0);" question="${question}" title_="${question}"><font
									color="#CC0000">详情</font> </a>
								</kbs:if>
						</div>
						<div class="dangqian_1" style="float:left;">
						
							<h1>
								<font size="3">${question}</font>
								
							</h1>
							
						</div>
						<%--modify by eko.zhan at 2016-11-14 13:23 移动提出颜色保持一致性，下面这个div之前定义了颜色 color:#666666; 现在去掉 --%>
						<div style="word-break: break-all; word-wrap: break-word;width:95%;height:auto;line-height:18px;margin-bottom:10px;">
							${answer}</div>
						
						<s:if test="#request.p4Data.size > 0">
									<s:iterator value="#request.p4Data">
					        			<a href="javascript:void(0);" onclick="openP4('${url}');">[${title}]</a><br/>
					        			<iframe src="${url}" height="300" width="100%" frameborder="no" border="0"></iframe>
					        		</s:iterator>
						</s:if>
						
						<div class="dq_wz">
								<s:iterator value="#request.attachmentList" var="attch">
										<%
												AttachFile att = (AttachFile)pageContext.findAttribute("attch");
										%>
										<div class="kbs-office <%=WukongUtil.getIcon(att.getName()) %>"></div>
									<span>
				        				<!--<a href="${url}">${name}</a>-->
				        				<kbs:input type="a" href="${url}" value="${name}"  key="K-AttachmentDownload" 
				        					isDisplay="true" contentDisplay='<a href="javascript:void(0)" style="cursor:default;text-decoration:none;" >${name}</a>' />
									</span>
									<b>
									<script type="text/javascript">
										if (enableAdvReview){
											document.write("<a href=\"javascript:void(0);\" kbsconvert=\"1\" kbsname=\"${name }\" kbsid=\"${id }\">打开全部</a>");
										}else{
											document.write("<a href=\"javascript:openAttachment('${valueId}x','${index}','${id}','${name }');\">打开全部</a>");
										}
									</script>
									<!-- <a href="javascript:openAttachment('${valueId}x','${index}','${id}','${name }')">打开全部</a> -->
									<!-- <a href="javascript:void(0);" kbsconvert="1" kbsname="${name }" kbsid="${id }">打开全部</a> -->
				        			</b>
				        			<br>
								</s:iterator>
								<div id="openAttachment${attValId}" style="display:none;margin-top:5px;height:510px;" class="attachmentIndex shipin">xx</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			
			
			<div class="gaoji_title">
				
				<div class="gaoji_title_left">
					高级搜索，共
					<span>${totalCount}</span>条记录
				</div>
				<div class="gaoji_title_right">
					<a href="javascript:void(0);">预览</a>
					<input type="checkbox"/>
				</div>
			</div>
			<div class="gaoji_con">
				<div class="gaoji_left">
					<div class="gaoji_con_title">
						<h1 style="width: 75%"> 
							<b>
								知识名称
							</b> 
						</h1>
						<h2 style="width: 15%"> 修改时间 </h2>
						<h3 style="width: 5%"> 热度 </h3>
						<h5 style="width: 5%"></h5>
					</div>
					<div class="gaoji_con_nr">
						<s:if test="#request.list == null || #request.list.isEmpty()">
							<ul style="min-height: 200px;"></ul>
						</s:if>
						<s:else>
							<s:iterator var="clist" value="#request.list" id="clist">
								<div>
								<s:iterator var="ov" value="#clist" status="status">
									<s:if test="#status.index==0">
									<div bh="${ov.bh }" categoryId="${ov.cid}" class="bh" style="font-size:14px;font-family:微软雅黑;line-height:28px;margin-left:20px;font-weight:bold;">${ov.bhname}</div>
									</s:if>
									<ul <s:if test="#status.index > 2"> style="display: none" class="moreul"</s:if> >
										<li class="dangqian">
											<div class="dangqian_1">
											<h1 style="width: 75%"> 
												<a title="${ov.question}" href="javascript:void(0);" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:70%;text-align:left;">
													&nbsp;&nbsp;&nbsp;&nbsp;● <s:property value="#ov.question" escape="false"/>
													<s:if test="#session.searchDebug">
													&nbsp;&nbsp;score:<s:property value="#ov.score" escape="false"/>
													</s:if>
												</a>
												<a class="_ymzs" href="javascript:void(0);" style="width:auto;margin-top: 3px;" id="${ov.qid}" ftp="0"><img style="width:24px;height:24px;"  src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/category.png"/></a>
												<s:if test='#ov.isableSMS==true'>
												<a class="_ymzs" href="javascript:void(0);" style="width:auto;margin-top: 3px;" id="${ov.qid}" ftp="1"><img style="width:24px;height:24px;"  src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/sms.png"/></a>
												<div style="display:none;">
												    <textarea id="${ov.qid }_question" cols="" rows="" isableSMS="${ov.isableSMS}">${ov.question}</textarea>
													<textarea id="${ov.qid }_answer" cols="" rows=""  >${ov.answer}</textarea> 												
												</div>
												</s:if>											
											</h1>
											<h2 style="width: 15%"> <s:date name="#ov.edittime"/> </h2>
											<h5 style="width: 5%"> <s:property value="#ov.dscount"/> </h5>
											
											<kbs:if test="<%=!SystemKeys.isGzyd() %>">
												<h6 style="width: 5%"> <a href="javascript:void(0);" value="<s:property value="#ov.question"/>" title_="<s:property value="#ov.objname"/>"><font color="#CC0000">详情</font></a> </h6>
											</kbs:if>
											</div>
											<div class="dangqian_2" style="display: none;">
												<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
													<!--  <s:iterator value="#ovw.url_p4">
														<tr>
															<td align="center">
																<iframe frameborder="0" height="300" width="100%" src="${robotContextPath}p4data/<s:property/>"> </iframe>
															<br></td>
														</tr>
													</s:iterator>
													 -->
												</table>
												<div>
													<s:property value="#ov.answer" escape="false"/>
												</div>
											</div>
										</li>
									</ul>
									<s:if test="#clist.size() > 3">
										<s:if test="#status.index==2">
											<ul>
												<li>
													<a class="moreQa" href="javascript:void(0);" style="font-size:14px;font-family:微软雅黑;line-height:28px;margin-left:20px;font-weight:bold;color:#e30000">&darr;显示更多</a>
												</li>
											</ul>
											<div style="clear:both;"></div>
										 </s:if>
									</s:if>
								</s:iterator>
								</div>
							</s:iterator>
						</s:else>
						<!-- 
						<s:else>
							<div class="gaoji_ss">
								<input class="gaoji_ss_kuang" name="" type="text" />
								<a href="javascript:void(0);" style="margin-left: -3px;"><input type="button" class="gaoji_ss_anniu"
										value="在结果中搜索" />
								</a>
							</div>
						</s:else>
						 -->
						<div class="gaoji_ss">
							<input class="gaoji_ss_kuang" name="appendContent" value="${appendContent}" type="text" />
							<a href="javascript:void(0);" style="margin-left: -3px;"><input type="button" class="gaoji_ss_anniu"
									value="在结果中搜索" />
							</a>
						</div>
						</form>
						<div class="gonggao_con_nr_fenye">
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">首页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">上一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">上一页</a>
							</s:else>
							<s:iterator begin="1" end="#request.arrayPage.pageTotalNo" status="i">
								<s:if test="(#i.index + 1) + 4 > #request.arrayPage.pageNo && (#i.index + 1) - #request.arrayPage.pageNo < 5">
									<s:if test="(#i.index + 1) == #request.arrayPage.pageNo">
										<a href="javascript:void(0)" class="dang" onfocus="this.blur();"><s:property/></a>
									</s:if>
									<s:else>
										<a href="javascript:void(0)" onfocus="this.blur();" start="<s:property value="#i.index * #request.arrayPage.pageSize"/>" limit="${arrayPage.pageSize}"><s:property/></a>
									</s:else>
								</s:if>
							</s:iterator>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">下一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">下一页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">尾页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a>
							</s:else>
							<div id="currentPage" style="display: none;">
								<input id="start" value="${arrayPage.start}">
								<input id="pageSize" value="${arrayPage.pageSize}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>
