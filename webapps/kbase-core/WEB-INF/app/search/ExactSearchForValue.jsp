<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gaoji.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ExactSearchForValue.js"></script>
	
		<script type="text/javascript">
			var askContent = '<s:property value="#request.content.split(' ')[0]"/>';
			var enableCateTag = '${requestScope.enableCateTag }';
		</script>
	</head>

	<body>
		<div class="content_right_bottom">
			<div class="gaoji_title">
				<div class="gaoji_title_left">
					精准搜索，共
					<span>${totalCount}</span>条记录
				</div>
				<div class="gaoji_title_right">
					<a href="javascript:void(0);">预览</a>
					<input type="checkbox"/>
				</div>
			</div>
			<div class="gaoji_con">
				<div class="gaoji_left">
					<div class="gaoji_con_title">
						<h1 style="width: 30%"> 
							<b>
								知识名称
							</b> 
						</h1>
						<h2 style="width: 15%"> 修改时间 </h2>
						<h3 style="width: 5%"> 热度 </h3>
						<h4 style="width: 45%"> 路径 </h4>
						<h5 style="width: 5%"></h5>
					</div>
					<div class="gaoji_con_nr">
						<s:if test="#request.list == null || #request.list.isEmpty()">
							<ul style="min-height: 200px;"></ul>
						</s:if>
						<s:else>
							<s:iterator var="ov" value="#request.list" status='st'>
								<ul>
									<li class="dangqian">
										<div class="dangqian_1">
											<h1 style="width: 30%;text-align:left;"> 
												<a title="${ov.name}" href="javascript:void(0);" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width:80%;text-align:left;"
												logInfo="${st.index + 1}|#|${arrayPage.pageNo}|#|${arrayPage.pageSize}|#|${ov.id}|#|${ov.name}">
													<s:property value="#ov.name" escape="false"/>
												</a> 
											<s:if test="#ov.editTime != null && (new java.util.Date().getTime() - #ov.editTime.getTime())<=24*3600*1000">
												<span style="width:10px;color:#f00;font-size:10px;line-height:21px;">
													new
												</span>
											</s:if>
											</h1>
											<h2 style="width: 15%"> <s:date name="#ov.editTime" format="yyyy-MM-dd"/> </h2>
											<h5 style="width: 5%"> <s:property value="#ov.hot"/> </h5>
											<!-- <h4> <s:property value="#ov.categoryPath"/></h4> -->
											
											
											<h4 style="width: 45%;"> 
												<a categoryId="${ov.categoryId}" categoryName="${ov.categoryName}" class="categorySenior" style="width:auto;background:#67a909 none repeat scroll 0 0;color:fff;text-decoration:none;line-height:33px;padding:5px 10px;margin-top:5px;cursor:pointer;">
													<s:property value="#ov.categoryPath"/>
												</a>
											</h4>
											
											
											
											<h6 style="width: 5%"> 
												<a href="javascript:void(0);" value="<s:property value="#ov.name"/>" title_="<s:property value="#ov.objectName"/>"
												logInfo="${st.index + 1}|#|${arrayPage.pageNo}|#|${arrayPage.pageSize}|#|${ov.id}|#|${ov.name}">
													<font color="#CC0000">详情</font>
												</a> 
											</h6> 
											<!-- 
											<h6>
												<div style="width:120px;">
													<a class="detail" href="javascript:void(0);" value="<s:property value="#ov.name"/>" title_="<s:property value="#ov.objectName"/>"  style="float: right;">
														<font color="#CC0000">详情</font> 
													</a>
													
													高级搜索结果列表中添加分类展示按钮
													update by alan.zhang at 2015-12-11 16:11
													
													<s:if test="#ov.categoryId!=null">
														<div categoryId="${ov.categoryId}" categoryName="${ov.categoryName}" class="categorySenior" style="width:50px;background:#67a909 none repeat scroll 0 0;color:fff;text-decoration:none;height:24px;float:left;margin-right:10px;line-height:24px;padding:0 10px;margin-top:5px;cursor:pointer;">
														分类展示
														</div>
													</s:if>
													
												</div>
												
											</h6>
											-->
										</div>
										<div class="dangqian_2" style="display: none;">
											<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0">
												<!--  <s:iterator value="#ovw.url_p4">
													<tr>
														<td align="center">
															<iframe frameborder="0" height="300" width="100%" src="${robotContextPath}p4data/<s:property/>"> </iframe>
														<br></td>
													</tr>
												</s:iterator>
												 -->
											</table>
											<div>
											<s:property value="#ov.value" escape="false"/>
											</div>
										</div>
									</li>
								</ul>
							</s:iterator>
						</s:else>
						<div class="gonggao_con_nr_fenye" logInfo="${arrayPage.pageNo}|#|${arrayPage.pageTotalNo}">
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">首页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">上一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">上一页</a>
							</s:else>
							<s:iterator begin="1" end="#request.arrayPage.pageTotalNo" status="i">
								<s:if test="(#i.index + 1) + 4 > #request.arrayPage.pageNo && (#i.index + 1) - #request.arrayPage.pageNo < 5">
									<s:if test="(#i.index + 1) == #request.arrayPage.pageNo">
										<a href="javascript:void(0)" class="dang" onfocus="this.blur();"><s:property/></a>
									</s:if>
									<s:else>
										<a href="javascript:void(0)" onfocus="this.blur();" start="<s:property value="#i.index * #request.arrayPage.pageSize"/>" limit="${arrayPage.pageSize}"><s:property/></a>
									</s:else>
								</s:if>
							</s:iterator>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">下一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">下一页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">尾页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a>
							</s:else>
							<div id="currentPage" style="display: none;">
								<input id="start" value="${arrayPage.start}">
								<input id="pageSize" value="${arrayPage.pageSize}">
								<input id="totalPage" value="${arrayPage.pageTotalNo}">
							</div>
						</div>
						<form id="fenyeForm" method="get" style="display: none;"
								action="${pageContext.request.contextPath}/app/search/exact-search.htm">
						<input type="text" hidden="hidden" name="start" value="${arrayPage.nextPageStart}" />
						<input type="text" hidden="hidden" name="content" value="<s:property value="#request.content.split(' ')[0]"/>" />
						<input type="text" hidden="hidden" name="searchType" value="4" />
					</form>
					</div>
		
	</body>
</html>
