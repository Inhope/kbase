<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gaoji1.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<!--	日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForObject.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForm.js"></script>
		<script type="text/javascript">
			var askContent = '<s:property value="#request.content.split(' ')[0]"/>';
		</script>
	</head>

	<body>
		<div class="content_right_bottom">
			<form action="${pageContext.request.contextPath}/app/search/senior-search.htm" method="post">
			<div class="gaoji">
				<div class="gaoji_1">
					<ul>
						<li>
							<p>
								搜索内容
							</p>
							<input type="hidden" value="<s:property value="#request.content.split(' ')[0]"/>"/>
							<input id="content" name="content" type="text"/>
						</li>
						<li>
							<p>
								搜索类型
							</p>
							<label>
								<s:if test="#request.searchType == 1">
									<input type="radio" name="searchType" value="1" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="1"/>
								</s:else>
								全文
							</label>
							<label>
								<s:if test="#request.searchType == 2">
									<input type="radio" name="searchType" value="2" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="2"/>
								</s:else>
								实例
							</label>
							<label>
								<s:if test="#request.searchType == 3">
									<input type="radio" name="searchType" value="3" checked="checked"/>
								</s:if>
								<s:else>
									<input type="radio" name="searchType" value="3"/>
								</s:else>
								附件
							</label>
						</li>
						<li>
							<p>知识目录</p>
							<select id="catgoryDirId" name="catgoryDirId">
								<option value="" selected="selected">全部</option>
								<s:iterator var="oc" value="#session.ontologyCategorys">
									<option value="<s:property value="#oc.id"/>" <s:if test="#oc.id == #request.catgoryDirId">selected="selected"</s:if>>
										<s:property value="#oc.name"/>
									</option>
								</s:iterator>
							</select>
						</li>
					</ul>
				</div>
				<div class="gaoji_2">
					<ul>
						<li>
							<p>
								创建日期
							</p>
							<input id="startDate" name="startDate" type="text" value="${startDate}" class="Wdate"/>
						</li>
						<li>
							<p>
								知识时效
							</p>
							<select id="agingType" name="agingType">
								<option value="0">全部</option>
								<s:if test="#request.agingType == 1">
									<option selected="selected" value="1">常态</option>
								</s:if>
								<s:else>
									<option value="1">常态</option>
								</s:else>
								<s:if test="#request.agingType == 2">
									<option selected="selected" value="2">未过期</option>
								</s:if>
								<s:else>
									<option value="2">未过期</option>
								</s:else>
								<s:if test="#request.agingType == 3">
									<option selected="selected" value="3">未过追溯期</option>
								</s:if>
								<s:else>
									<option value="3">未过追溯期</option>
								</s:else>
								<s:if test="#request.agingType == 4">
									<option selected="selected" value="4">已过追溯期</option>
								</s:if>
								<s:else>
									<option value="4">已过追溯期</option>
								</s:else>
							</select>
						</li>
					</ul>
				</div>
				<div class="gaoji_3">
					<ul>
						<li>
							<p>
								至
							</p>
							<input id="endDate" name="endDate" type="text" value="${endDate}" class="Wdate"/>
						</li>
						<s:if test="#session.locationEnable">
								<li><p></p></li>
						</s:if>
						<s:else>
							<li>
								<p>地区</p>
								<select id="location" name="location">
									<option value="" selected="selected">全部</option>
									<s:iterator var="dt" value="#session.dimTags">
										<option value="<s:property value="#dt.tag"/>" <s:if test="#request.location == #dt.tag">selected="selected"</s:if>><s:property value="#dt.name"/></option>
									</s:iterator>
								</select>
							</li>
						</s:else>
						<s:if test="#session.brandEnable">
								<li><p></p></li>
						</s:if>
						<s:else>
							<li>
								<p>品牌</p>
								<select id="brand" name="brand">
									<option value="">全部</option>
									<s:iterator var="dt" value="#session.brands">
										<option value="<s:property value="#dt.tag"/>" <s:if test="#request.brand == #dt.tag">selected="selected"</s:if>><s:property value="#dt.name"/></option>
									</s:iterator>
								</select>
							</li>
						</s:else>
						<li>
							<input class="an1" type="button" value="搜索" />
							<input class="an2" type="reset" value="重置" />
						</li>
					</ul>
				</div>
			</div>
			</form>
			<div class="gaoji_title">
				<div class="gaoji_title_left">
					高级搜索，共
					<span>${totalCount}</span>条记录
				</div>
			</div>
			<div class="gaoji_con">
				<div class="gaoji_left">
					<div class="gaoji_con_title">
						<h1> 
							<b>
								实例名称
							</b> 
						</h1>
						<h2> 创建时间 </h2>
						<h3> 修改时间 </h3>
						<h4> 创建人 </h4>
						<h5> 热度 </h5>
						<h6></h6>
					</div>
					<div class="gaoji_con_nr">
						<s:iterator var="oo" value="#request.list">
							<ul>
								<li class="dangqian">
									<div class="dangqian_1">
										<h1 style="text-align:left;"> 
											<a title="${oo.name}" href="javascript:void(0);" categoryId="<s:property value="#oo.categoryId"/>" title='${oo.name}' value="<s:property value="#oo.objectId"/>" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width:80%;text-align:left;">
												<s:property value="#oo.name" escape="false"/>
											</a>
											<s:if test="#oo.editTime != null && (new java.util.Date().getTime() - #oo.editTime.getTime())<=24*3600*1000">
												<span style="width:10px;color:#f00;font-size:10px;line-height:21px;">
													new
												</span>
											</s:if>
										</h1>
										<h2> <s:date name="#oo.createTime" format="yyyy-MM-dd"/> &nbsp;</h2>
										<h3> <s:date name="#oo.editTime" format="yyyy-MM-dd"/> &nbsp;</h3>
										<h4> 创建人 </h4>
										<h5> <s:property value="#oo.hot"/> </h5>
									</div>
								</li>
							</ul>
						</s:iterator>
						<div class="gaoji_ss">
							<input class="gaoji_ss_kuang" value="${appendContent}" type="text" />
							<a href="javascript:void(0);" style="margin-left: -3px;"><input type="button" class="gaoji_ss_anniu"
									value="在结果中搜索" />
							</a>
						</div>
						<div class="gonggao_con_nr_fenye">
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">首页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">上一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">上一页</a>
							</s:else>
							<s:iterator begin="1" end="#request.arrayPage.pageTotalNo" status="i">
								<s:if test="(#i.index + 1) + 4 > #request.arrayPage.pageNo && (#i.index + 1) - #request.arrayPage.pageNo < 5">
									<s:if test="(#i.index + 1) == #request.arrayPage.pageNo">
										<a href="javascript:void(0)" class="dang" onfocus="this.blur();"><s:property/></a>
									</s:if>
									<s:else>
										<a href="javascript:void(0)" onfocus="this.blur();" start="<s:property value="#i.index * #request.arrayPage.pageSize"/>" limit="${arrayPage.pageSize}"><s:property/></a>
									</s:else>
								</s:if>
							</s:iterator>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">下一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">下一页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">尾页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a>
							</s:else>
							<div id="currentPage" style="display: none;">
								<input id="start" value="${arrayPage.start}">
								<input id="pageSize" value="${arrayPage.pageSize}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>