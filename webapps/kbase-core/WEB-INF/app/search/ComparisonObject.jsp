<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML >
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    
    <title>历史版本实例对比</title>
    <link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dianzi_dan.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert.js"></script>
	<script type="text/javascript">
		var swfAddress='${swfAddress}';
		var enableAdvReview = (swfAddress.indexOf("/attachment!")==-1);	//是否开启高级预览模式
		$(function(){
			$('div.shangmian input[type=checkbox]').attr('checked', false);
			
			var changeClasses = ['yan1', 'yan2', 'yan3'];
			//显示变动
			$('div.shangmian input[type=checkbox]:eq(0)').click(function() {
				if($(this).attr('checked')) {
					var allTable = $('table');
					$.each($('table'), function() {
						for(var i = 0; i < changeClasses.length; i ++) {
							if($(this).find('tr:not(.hide)').find('td:not(:hidden)').hasClass(changeClasses[i])) {
							
								allTable = allTable.not($(this));
								break;
							}
						}
					});
					allTable.hide();
				} else {
					$('table').show();
				}
			});
			
			//显示全部
			$('div.shangmian input[type=checkbox]:eq(1)').click(function() {
				if($(this).attr('checked')) {
					$('table tr.hide').addClass('show').removeClass('hide');
				} else {
					$('table tr.show').addClass('hide').removeClass('show');
				}
			});
			
			//初始化高级预览模式
			if (enableAdvReview){
				$.kbase.convert.init({
					contextPath: swfAddress
				});
			}
		});
	</script>
  </head>
  
  <body>
  		<!-- 版本对比 -->
	<div class="index_d">
		<div class="index_dan">
			<div class="index_dan_title">
				<b>${ontologyObject.name}</b>
			</div>
			<div class="shangmian">
			<div class="xgai">
				<div class="xgai_con">
					<p class="pleft">
						<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/dianzi_bg1.jpg" width="50" height="20" />
						修改
					</p>
					<p class="pcenter">
						<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/dianzi_bg2.jpg" width="50" height="20" />
						增加
					</p>
					<p class="pright">
						<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/dianzi_bg3.jpg" width="50" height="20" />
						删除
					</p>
				</div>
			</div>
				<p style="z-index: 1;">
					<input class="dian" name="" type="checkbox" value="" />
					<span>仅显示变动</span>
				</p>
				<p style="z-index: 1;">
					<input class="dian" name="" type="checkbox" value="" />
					<span>加载全部</span>
				</p>
			</div>
			<div class="index_dan_con">
				<ul>
					<s:iterator var="oa" value="#request.objectAttributes" status="i">
						<li>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" style="word-break: break-all; word-wrap:break-word;">
								<tbody>
									<s:if test="#i.index == 0">
										  <tr>
										    <td class="diyi2"><p class="biaoti">版本号</p></td>
										    <td class="dier2"><p class="biaoti"><s:property value="#request.versionIds.split(',')[0].split(';')[1]"/></p></td>
										    <td class="disan2"><p class="biaoti"><s:property value="#request.versionIds.split(',')[1].split(';')[1]"/></p></td>
										  </tr>
									</s:if>
									<tr>
										<td class="diyi1">
											<p class="biaoti">
												问题
											</p>
										</td>
										<!-- 删除 -->
										<s:if test="#oa[0].question == '' || #oa[0].question == null">
											<td class="yan3">
												<p class="biaoti" style="color: white;">
													
												</p>
											</td>
											<td class="disan1">
												<p class="biaoti">
													<s:property value="#oa[1].question"/>
												</p>
											</td>
										</s:if>
										<!-- 新增 -->
										<s:elseif test="#oa[1].question == null || #oa[1].question == ''">
											<td class="yan2" style="width: 40%;">
												<p class="biaoti" style="color: white;">
													<s:property value="#oa[0].question"/>
												</p>
											</td>
											<td class="disan1">
												<p class="biaoti">
													
												</p>
											</td>
										</s:elseif>
										<s:else>
											<s:if test="#oa[0].question.equals(#oa[1].question)">
												<td class="dier1">
													<p class="biaoti">
														<s:property value="#oa[0].question"/>
													</p>
												</td>
												<td class="disan1">
													<p class="biaoti">
														<s:property value="#oa[1].question"/>
													</p>
												</td>
											</s:if>
											<!-- 修改 -->
											<s:else>
												<td class="yan1" style="width: 40%;">
													<p class="biaoti" style="color: white;">
														<s:property value="#oa[0].question"/>
													</p>
												</td>
												<td class="disan1">
													<p class="biaoti">
														<s:property value="#oa[1].question"/>
													</p>
												</td>
											</s:else>
										</s:else>
									</tr>
									<tr>
										<td class="diyi">
											<p class="wenzi1">
												答案
											</p>
										</td>
										<!-- 删除 -->
										<s:if test="#oa[0].answer == null || #oa[0].answer == ''">
											<td class="dier yan3">
												<p class="wenzi1">
													
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													<s:property value="#oa[1].answer"/>
												</p>
											</td>
										</s:if>
										<!-- 新增 -->
										<s:elseif test="#oa[1].answer == null || #oa[1].answer == ''">
											<td class="dier yan2">
												<p class="wenzi1" style="color: white;">
													<s:property value="#oa[0].answer"/>
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													
												</p>
											</td>
										</s:elseif>
										<s:else>
											<s:if test="#oa[0].answer == #oa[1].answer">
												<td class="dier">
													<p class="wenzi1">
														<s:property value="#oa[0].answer"/>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1">
														<s:property value="#oa[1].answer"/>
													</p>
												</td>
											</s:if>
											<!-- 修改 -->
											<s:else>
												<td class="dier yan1">
													<p class="wenzi1" style="color: white;">
														<s:property value="#oa[0].answer"/>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1">
														<s:property value="#oa[1].answer"/>
													</p>
												</td> 
											</s:else>
										</s:else>
									</tr>
									<tr class="hide">
										<td class="diyi">
											<p class="wenzi1">
												P4
											</p>
										</td>
										
										<!-- 删除 -->
										<s:if test="#oa[0].p4s.isEmpty() && #oa[1].p4s.isEmpty()">
											<td class="dier">
												<p class="wenzi1">
													
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
												</p>
											</td>
										</s:if>
										<s:elseif test="#oa[0].p4s.isEmpty()">
											<td class="dier yan3">
												<p class="wenzi1">
													
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													<s:iterator var="p4" value="#oa[1].p4s">
														<s:if test="#p4.p4Url.length() == 32">
															<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
														</s:if>
														<s:else>
															<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
														</s:else>
													</s:iterator>
												</p>
											</td>
										</s:elseif>
										<!-- 新增 -->
										<s:elseif test="#oa[1].p4s.isEmpty()">
											<td class="dier yan2">
												<p class="wenzi1" style="color: white;">
													<s:iterator var="p4" value="#oa[0].p4s">
														<s:if test="#p4.p4Url.length() == 32">
															<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
														</s:if>
														<s:else>
															<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
														</s:else>
													</s:iterator>
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													
												</p>
											</td>
										</s:elseif>
										<s:else>
											<s:if test="#oa[0].p4s == #oa[1].p4s">
												<td class="dier">
													<p class="wenzi1">
														<s:iterator var="p4" value="#oa[0].p4s">
															<s:if test="#p4.p4Url.length() == 32">
																<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:if>
															<s:else>
																<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:else>
														</s:iterator>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1" style="color: white;">
														<s:iterator var="p4" value="#oa[1].p4s">
															<s:if test="#p4.p4Url.length() == 32">
																<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:if>
															<s:else>
																<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:else>
														</s:iterator>
													</p>
												</td>
											</s:if>
											<!-- 修改 -->
											<s:else>
												<td class="dier yan1">
													<p class="wenzi1" style="color: white;">
														<s:iterator var="p4" value="#oa[0].p4s">
															<s:if test="#p4.p4Url.length() == 32">
																<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:if>
															<s:else>
																<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:else>
														</s:iterator>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1">
														<s:iterator var="p4" value="#oa[1].p4s">
															<s:if test="#p4.p4Url.length() == 32">
																<a href="${robot_context_path}p4data/${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:if>
															<s:else>
																<a href="${p4.p4Url}" target="_blank">${p4.p4Name}</a>&nbsp;&nbsp;
															</s:else>
														</s:iterator>
													</p>
												</td>
											</s:else>
										</s:else>
									</tr>
									<tr class="hide">
										<td class="diyi">
											<p class="wenzi1">
												附件
											</p>
										</td>
										<!-- 删除 -->
										<s:if test="(#oa[0].attachments == null || #oa[0].attachments.isEmpty()) && (#oa[1].attachments == null || #oa[1].attachments.isEmpty())">
											<td class="dier">
												<p class="wenzi1">
													
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													
												</p>
											</td>
										</s:if>
										<s:elseif test="#oa[0].attachments == null || #oa[0].attachments.isEmpty()">
											<td class="dier yan3">
												<p class="wenzi1">
													
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													<s:iterator var="attas" value="#oa[1].attachments">
														<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
														<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
													</s:iterator>
												</p>
											</td>
										</s:elseif>
										<!-- 新增 -->
										<s:elseif test="#oa[1].attachments == null || #oa[1].attachments.isEmpty()">
											<td class="dier yan2">
												<p class="wenzi1" style="color: white;">
													<s:iterator var="attas" value="#oa[0].attachments">
														<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
														<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
													</s:iterator>
												</p>
											</td>
											<td class="disan">
												<p class="wenzi1">
													
												</p>
											</td>
										</s:elseif>
										<s:else>
											<s:if test="#oa[0].attachments == #oa[1].attachments">
												<td class="dier">
													<p class="wenzi1">
														<s:iterator var="attas" value="#oa[0].attachments">
															<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
															<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
														</s:iterator>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1">
														<s:iterator var="attas" value="#oa[1].attachments">
															<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
															<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
														</s:iterator>
													</p>
												</td>
											</s:if>
											<!-- 修改 -->
											<s:else>
												<td class="dier yan1">
													<p class="wenzi1" style="color: white;">
														<s:iterator var="attas" value="#oa[0].attachments">
															<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
															<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
														</s:iterator>
													</p>
												</td>
												<td class="disan">
													<p class="wenzi1">
														<s:iterator var="attas" value="#oa[1].attachments">
															<a href="${robotSearchContextPath}attachmentDown?attachmentId=<s:property value="#attas.split(',')[1]"/>&someTime=<%= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) %>"><s:property value="#attas.split(',')[0]"/></a>
															<a href="javascript:void(0);" kbsconvert="1" kbsname="<s:property value="#attas.split(',')[0]"/>" kbsid="<s:property value="#attas.split(',')[1]"/>">打开全部</a>
														</s:iterator>
													</p>
												</td>
											</s:else>
										</s:else>
									</tr>
								</tbody>
							</table>
						</li>
					</s:iterator>
				</ul>
			</div>
			
		</div>
	</div>
  </body>
</html>
