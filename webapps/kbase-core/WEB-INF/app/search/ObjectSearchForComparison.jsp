<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/shili_dan.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/json/json2.js"></script>
		<!-- 
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery-1.4.4.min.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script> -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ObjectSearchForComparison.js"></script>
	</head>
	<body>
		<div class="fdong" style="width:799px">
			<div class="fkui_d">
				<div class="fkui_dan">
					<div class="fkui_dan_title">
						<b>实例对比文件添加</b><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg" width="12" height="12" />
						</a>
					</div>
					<div class="shili_dan_con">
						<div class="shili_dan_left">
							实例选择				  
							    <ul id="roleObjectTree" class="ztree" style="width: 100%;height: 100%;"></ul>							  
						</div>
						<div class="shili_dan_center">
							实例名：
							<input name="" type="text" />
							<a href="javascript:void(0);"><input type="button" class="anniu1" value="查询" />
							</a>
						</div>
						<div class="shili_dan_right">
							对比文章列表
						</div>
					</div>
					<div class="shili_dan_con1">
						<div class="shili_dan_left1">
							<div class="xinxi_con_nr_left">
								<ul id="catalogTree" class="ztree">
									
								</ul>
							</div>
						</div>
						<div class="shili_dan_center1">
							<div class="shili_dan_center1_top">

								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<thead>
										<td>
											<b>实例名</b>
										</td>
										<td>
											<b>开始时间</b>
										</td>
										<td>
											<b>结束时间</b>
										</td>
										<td>
											<b>修改时间</b>
										</td>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="shili_dan_center1_bottom">

								<div class="gonggao_con_nr_fenye">
									<a href="javascript:void(0);" onfocus="this.blur();">首页</a>
									<a href="javascript:void(0);" onfocus="this.blur();">上一页</a>
									<a href="javascript:void(0);" onfocus="this.blur();">下一页></a>
									<a href="javascript:void(0);" onfocus="this.blur();">尾页</a>
								</div>

							</div>
						</div>
						<div class="shili_dan_right1" style="width:167px;">
							<div class="shili_dan_right1_top" style="width:167px;">
								<div class="shili_dan_right1_con" style="width:167px;">
									<ul style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
										<s:iterator value="#request.cmpareOnto">
											<li>
												<p style="color:red;">${name}</p>
											</li>
										</s:iterator>
									</ul>
									<ul id="compareSortable">
									
									</ul>
								</div>
								<div class="shili_dan_right1_an">
<!-- 								    <a class="delete" href="javascript:void(0);">删除</a> -->
<!-- 									<a href="javascript:void(0);" class="duibi">对比</a> -->
									<a href="javascript:void(0);" class="duibi" id="sureBtn">保存</a>
								</div>
							</div>
							<div class="shili_dan_right1_bottom">
								共
								<span id="totlePageSize">0</span>条记录
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
