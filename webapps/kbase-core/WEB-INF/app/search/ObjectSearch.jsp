<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>
<%
	/*业务模板功能开启——请求转向*/
	if(request.getAttribute("articleId") != null){/*是业务模板的数据*/
		String articleId = request.getAttribute("articleId").toString();
		if (PropertiesUtil.valueToBoolean("bizTpl.enable") 
				&& StringUtils.isNotBlank(articleId)) {
			String faqId = request.getParameter("faqId");/*标准问id*/
			if (StringUtils.isNotBlank(articleId)) {
				String targetUrl = "/app/biztpl/article-search.htm?&articleId="
						+ articleId;
				if (StringUtils.isNotBlank(faqId)) /*锚点位置*/
					targetUrl += "&spotId=" + faqId;
				response.sendRedirect(request.getContextPath() + targetUrl);
			}
		}
	}
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识展示-从实例进入知识列表</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gonggao.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/slzs.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<!--	日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/workFlowKnowledgeFeedback.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ObjectSearch.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ui/jquery-ui-sortable.min.js"></script>
		<script type="text/javascript">
			objId = '${objId}';
			var objName = '${objname}';//@lvan.li 2060308业务模板使用
			navigate_init('${robot_context_path}', objId);
			searchCtxPath='${robotSearchContextPath}';
			var categoryId = '${categoryId_current}';//当前分类id（积分策略用）
		</script>
	</head>

	<body>
		<div class="content_right_bottom">
			<div class="gonggao_titile">
				<div class="gonggao_titile_left">
					<span id="categoryNv"></span>
				</div>
				<div class="gonggao_titile_right">
					<a href="javascript:void(0);">原 文</a>
					<s:if test="#request.sntryRevisions.size()>0">
					<a href="javascript:void(0);">版本对比</a>
					</s:if>
					<s:else> <a href="javascript:void(0);" style="background: url(${pageContext.request.contextPath}/resource/search/images/gonggao_bg1.jpg);cursor:default;">历史版本</a></s:else>
					<a href="javascript:void(0);" onclick="workFlowKnowledgeFeedback('${objId }')" style="display: none;">反 馈</a>
					<a href="javascript:void(0);">收 藏</a>
					<!-- @lvan.li20160308 业务模板按钮-->
					<s:if test='#request.isBiz'>
						<a href ="javascript:void(0);" id="yewumoban">业务模板</a>
					</s:if>
				</div>
			</div>
			<div class="slzs_title">
				<div class="slzs_title_right">
					<a href="javascript:void(0);">预览</a>
					<input name="" type="checkbox" value="" logInfo="${categoryId_current}|#|${objId}"/>
				</div>
			</div>
			<div class="slzs_con">
				<div class="slzs_left">
					<div class="slzs_con_title">
						<h1>
							<b>知识名称</b>
						</h1>
						<h2>
							<!-- <a href="javascript:void(0);" style="background:url();">创建时间</a> -->
							创建时间
						</h2>
						<h3>
							<!-- <a href="javascript:void(0);" style="background:url();">修改时间</a>  -->
							修改时间
						</h3>
						<h4>
							创建人
						</h4>
						<h5>
							热度
						</h5>
						<h6></h6>
					</div>
					<div class="slzs_con_nr">
						<ul>
							<s:iterator value="#request.rqValue" var="ov" status='st'>
								<li class="dangqian">
									<div class="dangqian_1">
										<h1 style="text-align:left;">
											<a href="javascript:void(0);" title="${ov.question}" logInfo="${st.index + 1}|#|${ov.nodeId}|#|${ov.question}|#|${categoryId_current}|#|${objId}" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width:80%;text-align:left;"> 
													${ov.question}
											</a>
											<s:if test="#ov.attachment.editTime != null && (new java.util.Date().getTime() - #ov.attachment.editTime.getTime())<=24*3600*1000">
												<span style="width:10px;color:#f00;font-size:10px;line-height:21px;vertical-align:top;">
													new
												</span>
											</s:if> 
										</h1>
										<h2>
											<s:if test="#ov.attachment.createTime==null || #ov.attachment.createTime==''">/</s:if>
											<s:else>
												<s:date format="yyyy-MM-dd" name="#ov.attachment.createTime" />
											</s:else>
										</h2>
										<h3>
											<s:if test="#ov.attachment.editTime==null || #ov.attachment.editTime==''">/</s:if>
											<s:else>
												<s:date format="yyyy-MM-dd" name="#ov.attachment.editTime" />
											</s:else>
										</h3>
										<h4>
											<s:if test="#ov.attachment.editor==null || #ov.attachment.editor==''">/</s:if>
											<s:else>
												${ov.attachment.editor}
											</s:else>
										</h4>
										<h5>
											${ov.hot}
										</h5>
										<h6>
											<a class="detail" href="${ov.question}" qid="${ov.nodeId}" logInfo="${st.index + 1}|#|${ov.nodeId}|#|${ov.question}|#|${categoryId_current}|#|${objId}">
												<font color="#CC0000">详情</font>
											</a>
											&nbsp;
											<s:if test="#session.dataembed_enable">
											<a href="javascript:void(0);" logInfo="${ov.nodeId}|#|${ov.question}|#|${categoryId_current}|#|${st.index + 1}" name="signl">
												<font color="#CC0000">解决</font>
											</a> 
											</s:if>
										</h6>
									</div>
									<div style="display: none;">
										<div class="dangqian_2">
											${ov.answer}
										</div>
										<s:iterator value="#ov.p4" var="p">
											<a href="javascript:void(0);" onclick="openP4('${p.url}');">[${p.title}]</a>
											
											<iframe src="${p.url}" height="300" width="100%" frameborder="no" border="0">
											</iframe>
											<br />
										</s:iterator>
									</div>
								</li>
							</s:iterator>
						</ul>
					</div>
				</div>
				<div class="slzs_right">
					<div class="slzs_right_title">
						<ul>
							<li class="dq">
								<p></p>
								<a href="javascript:void(0);">实例对比</a><b></b>
							</li>
							<li>
								<p></p>
								<a href="javascript:void(0);">实例关联</a><b></b>
							</li>
						</ul>
					</div>
					<div class="slzs_right_con">
						<ul>
							<div style="overflow-y:auto;max-height:250px;">
								<s:iterator value="#request.cmpareOnto">
									<li>
										<input type="checkbox"> <a href="javascript:void(0);" title="${name}" objId="${objectId}">${name}</a>
									</li>
								</s:iterator>
								<div id="showCmpareOntoDiv">
								
								</div>
							</div>
							<div>
								<li class="slzs_anniu">
									<a href="javascript:void(0);">选中对比</a>
									<a id="addObject" href="javascript:void(0);">添加文章</a>
								</li>
							</div>
						</ul>
						<ul style="display: none;">
							<s:iterator value="#request.relationOnto">
								<li>
									<a href="javascript:void(0);" title="${name}" objId="${objectId}">${name}</a>
								</li>
							</s:iterator>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--******************弹出框***********************-->
<div class="gonggao_d" style="display:none;z-index:10001;position:absolute;">
<div class="gonggao_dan">
<div class="gonggao_dan_title"><b>原文</b><a href="#" id="closeYuanwen"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/images/xinxi_ico1.jpg" width="12" height="12" /></a></div>
	<div class="gonggao_dan_con">
		<ul id="fileTree" class="ztree" style="height:180px;width:420px;"></ul>
	</div>
</div>
</div>
<!--******************弹出框***********************-->
		
		<jsp:include page="./ObjectVersion.jsp"></jsp:include>
		<jsp:include page="./ObjectSearchForComparison.jsp"></jsp:include>
		<jsp:include page="./FavBall.jsp"></jsp:include>
		<%--<jsp:include page="./Recommend.jsp"></jsp:include>  --%>
	</body>
</html>

