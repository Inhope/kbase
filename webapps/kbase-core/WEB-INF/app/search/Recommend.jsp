<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/recommend/css/recommend.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<script type="text/javascript">
			var depNames = "",depPIds = "",depIds = "";
			//***************************************查询条件部门岗位树（开始）***************************
			//modify by eko.zhan at 2015-10-08 17:20 逗号没去掉，IE下报错	
			var setting = {
				async: {
					enable: true,
					//url: $.fn.getRootPath()+"/app/util/choose!deptData.htm",
					url: $.fn.getRootPath()+"/app/recommend/recommend!recomDeptTree.htm"
					//autoParam: ["id"],
					//dataFilter: ajaxDataFilter
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "s", "N": "s" }
				},
				callback: {
					onCheck: zTreeOnCheck1									
				}
			};
			
			//勾选(异步)
			function zTreeOnCheck(e, treeId, treeNode) {
				var pre = new RegExp("^,.*"), suf = new RegExp(".*,$");
				if(!pre.test(depNames)) depNames = "," + depNames;
				if(!pre.test(depIds)) depIds = "," + depIds;
				if(!pre.test(depPIds)) depPIds = "," + depPIds;
				if(!suf.test(depNames)) depNames += ",";
				if(!suf.test(depIds)) depIds += ",";
				if(!suf.test(depPIds)) depPIds += ",";
				if(treeNode.checked){//勾选
					depNames += treeNode.name + ",";
					depIds +=  treeNode.id + ",";
					depPIds += treeNode.bh + ",";
					depPIds = depPIds.replace(/./g, '');
				}else{//取消勾选
					depNames = depNames.replace("," + treeNode.name + "," ,',');
					depIds = depIds.replace("," + treeNode.id + ",", ',');
					depPIds = depPIds.replace(("," + treeNode.bh + ",").replace(/./g,","), ',');
				}
				
				var test = new RegExp("^[,]*$");
				if(test.test(depNames))depNames = '';
				if(test.test(depIds))depIds = '';
				
				$("#depNames_select").attr("value", depNames);
				$("#depIds_select").attr("value", depIds);
			}
			
			//勾选（同步）
			function zTreeOnCheck1(e, treeId, treeNode) {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getCheckedNodes(true);
				v = "";
				depIds = "";
				for (var i=0, l=nodes.length; i<l; i++) {
					v += nodes[i].name + ",";
					depIds +=  nodes[i].id + ",";
				}
				
				if (v.length > 0 ) v = v.substring(0, v.length-1);
				//if (depIds.length > 0 ) depIds = depIds.substring(0, depIds.length-1);
				$("#depNames_select").attr("value", v);
				$("#depIds_select").attr("value", ','+depIds);
			}
			
			function ajaxDataFilter(treeId, parentNode, responseData) {
				var status  = $("#status_select").val();
				if(status == '0'){
					depNames = $("#depNames_select").val();
					depIds = $("#depIds_select").val();
					depPIds = $("#depPIds_select").val();
					
					$("#status_select").val(1);
				}
			    if (responseData.length > 0) {
			        for(var i =0; i < responseData.length;i++) {
			        	//回显(半勾选,认为半勾选的节点都有子节点，所以半勾选数据都是.结尾)
			        	if(depPIds.length>0 && depPIds.indexOf(","+responseData[i].id+",")>-1)
			        		responseData[i].halfCheck = true;
			        	//回显(勾选)
			        	if(depIds.length>0 && depIds.indexOf(","+responseData[i].id+",")>-1){
			        		responseData[i].checked = true;
			        		//去掉半勾选
			        		responseData[i].halfCheck = false;
			        	}
			        		
			        }
			    }
			    return responseData;
			}				
//**************************************下拉框*************************************			
			function showMenu(menuContent,inputName) {
				var cityObj = $("#"+inputName);
				var cityOffset = $(cityObj).offset();
				$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	
				$("body").bind("mousedown", function(event){
					onBodyDown(event,menuContent)
				});
			}
			function hideMenu(menuContent) {
				$("#"+menuContent).fadeOut("fast");
				$("body").unbind("mousedown",function(event){
					onBodyDown(event,menuContent)
				});
			}
			function onBodyDown(event,menuContent) {
				if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
					hideMenu(menuContent);
				}
			}
			function expandParentNode(treeNode,treeObj){
				treeNode = treeNode.getParentNode();
				if(treeNode!=null){
					treeObj.expandNode(treeNode, true, false, true);
					expandParentNode(treeNode,treeObj);
					treeNode = treeNode.getParentNode();
				}
			}
			$(document).ready(function(){
				
				/*
				//部门岗位树初始化
				$.fn.zTree.init($("#treeDemo"), setting);
				改成在弹出窗打开时加载(参见：recommend.js 方法 showRemPanel)
				*/
				
				//部门岗位树勾选初始化
				/*
					var depIds = $("input[name='depIds']:eq(0)").val();
					if(depIds!=null&&depIds!=''){
						depIds = depIds.split(",");
						var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
						for(var i=0;i<depIds.length;i++){
							var treeNode = treeObj.getNodeByParam("id",depIds[i], null);
							if(treeNode!=null)treeObj.checkNode(treeNode,true,false);
						}					
					}
				*/
			});
		</script>
		<script type="text/javascript">
			$(function(){
				var recommendObject_window = {
					remPanel : $('div.rem-ball'),
					btnClosePanel : $('div.rem-ball div.main div.title img'),
					btnSaveRemmend : $('#recommendCommit'),
					
					endTime : $('ul#rem_time input'),
					depIds : null,
						
					//关闭推荐面板
					closeRemPanel : function(){
						$(document).hideShade();
						this.remPanel.hide();
					},
					//保存推荐知识
					saveRemmend : function(recomId,objId){
						var self = this;
						
						var endTime = '';
						for(var i=0; i<self.endTime.length; i++){
							if(self.endTime[i].checked){
								endTime = $(self.endTime[i]).val();
							}
						}
							
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath()+"/app/recommend/recommend!save.htm",
							data : {
								objId : objId,
								deptId : this.depIds,
								recomId : recomId,
								endTime : endTime,
								categoryId : categoryId
							},
							dataType : 'json',
							success : function(data){
								parent.$(document).hint(data.message);
								self.closeRemPanel();
							},
							error : function(){
								parent.$(document).hint('出错了');
							}
						});
						return false;
					},
					//验证
					testRecommend : function(){
						var self = this;
						self.depIds = $('#depIds_select').val();
							
						var result = false;
						if(self.depIds == null || self.depIds == ''){
							parent.layer.alert('* 推荐范围不能为空', -1);
						}else{
							result = true;
						}
						return result;
					},
					init : function(){
						var self = this;
						//保存
						self.btnSaveRemmend.click(function(){
							if(self.testRecommend()){
								var recomId = self.btnSaveRemmend.attr('recomid');
								if(recomId == null || recomId == ''){
									self.saveRemmend(null,self.btnSaveRemmend.attr('faqId'));
								}else{
									self.saveRemmend(recomId,self.btnSaveRemmend.attr('faqId'));
								}
							}
						});
						//关闭
						self.btnClosePanel.click(function(){
							self.closeRemPanel();
						});
					}
				};
				recommendObject_window.init();
			});
		</script>
</head>
	<body>
    <div class="rem-ball" style="display: none;">
			<div class="main" style="height: 190px;">
				<div class="title">
					<b>推荐知识</b>
					<img width="12" height="12" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg">
				</div>
				<div class="fcontent">
					<div class="content-data" style="height: auto;">
						<form>
							推荐范围: 
							<div style="height:8px"></div>
							<input type="text" id="depNames_select" readonly="readonly" value="请选择" 
								readonly="readonly" onfocus="showMenu('menuContent','depNames_select')" style="width: 230px;height: 22px;" />
							<input type="hidden" id="depIds_select" />
							<input type="hidden" id="depPIds_select" />
							<input type="hidden" id="status_select" value="0"/>
							<div style="height:14px"></div>
							<br /><br />
							推荐周期: <br />
							<ul id="rem_time" style="">
								<li><input type="radio" name="rem_time" id="day" value="day" />&nbsp;日</li>
								<li><input type="radio" name="rem_time" id="week" value="week" />&nbsp;周</li>
								<li><input type="radio" name="rem_time" id="mooth" value="mooth" />&nbsp;月</li>
							</ul>
							<!-- 
							<input type="text" id="startTime"/><input type="text" id="endTime"/>
							 -->
							 
						</form>
					</div>
					<div class="bottom">
						<input class="commit" type="button" id="recommendCommit" value="推荐">
					</div>
				</div>
				
				<div id="menuContent" class="menuContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid black; _position:absolute;overflow: auto; max-height: 200px;">
					<ul id="treeDemo" class="ztree" style="margin-top:0; width:222px;"></ul>
				</div>
			</div>
		</div>
	</body>
</html>