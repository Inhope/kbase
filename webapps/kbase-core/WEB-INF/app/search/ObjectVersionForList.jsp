<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML >
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dianzi_dan1.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ObjectVersionForList.js" ></script>
  </head>
  
  <body>
    <!-- 实例历史版本列表 -->
	<div class="index_chu" style="display: none;">
		<div class="index_d">
			<div class="index_dan">
				<div class="index_dan_title">
					<b>版本对比</b><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg"
							width="12" height="12" />
					</a>
				</div>
				<div class="index_dan_con">
					<ul>
						<li class="biaoge">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="biaoti" align="center">
										版本号
									</td>
									<td class="biaoti" align="center">
										版本变更时间
									</td>
									<td class="biaoti" align="center">
										版本变更原因
									</td>
								</tr>
							</table>
						</li>
					</ul>
				</div>
				<div class="xgai">
					<a href="javascript:void(0);" class="tijiao"><input type="button" value="对比" /></a>
					<a href="javascript:void(0);" class="tijiao" id="pingbi"><input type="button" value="屏蔽" /></a>
					<a href="javascript:void(0);" class="tijiao" id="gongkai"><input type="button" value="公开" /></a>
				</div>
			</div>
		</div>
	</div>
	
  </body>
</html>
