<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识展示-${question }</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gonggao.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/resource/corrections/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/resource/corrections/css/satisfaction.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<!-- add by eko.zhan at 2015-01-07 图片预览插件中用到的jquery.js，对原生态jquery的扩展，与其他插件比如flash.js,Flexpaper.js会有冲突，所以在这里引入，不可以挪动位置 -->
		<!-- hidden by eko.zhan at 2015-01-19 样式冲突，不启用imagezoom预览图片 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.flash.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<!--	日志记录 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchDetail.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/Flexpaper.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/Attachment.js"></script>
		<!-- 
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/recommend/js/recommend.js"></script>
		 -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert-1.0.2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ui/jquery-ui-sortable.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">
		
		<!-- delete imagezoom 相关js插件 by eko.zhan at 2015-08-13 -->
		
		<script type="text/javascript">
			var swfAddress='${swfAddress}';
			var enableAdvReview = (swfAddress.indexOf("/attachment!")==-1);	//是否开启高级预览模式
			var hotWord='${hotWord}';
			objId = '${objId}';
			
			var human_id = '${job_id}';
			var faqId = '${rbtResponse_faqId}';
			var robot_context_path = '${robot_context_path}';
			var sessionUId = '${sessionUId}';
			var brand = '${brand}';
			var inner = '${inner}';
			var askContent = '${askContent}';
			var searchCtxPath = '${searchCtxPath}';
			var firstP4 = '<s:property value="#request.p4Data[0].url"/>';
			var categoryId = '${categoryId}';
			var stime = 19;//签读倒计时(大于0认为启用，反之不启用)
			$(function(){
				//分类路径加载 modify by eko.zhan at 2015-10-08
				navigate_init('${robot_context_path}','${objId}');
			
				//初始化高级预览模式
				if (enableAdvReview){
					$.kbase.convert.init({
						contextPath: swfAddress
					});
				}
			});
		</script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/auther/js/workFlowKnowledgeFeedback.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultGraph.js"></script>
		<kbs:if code="gzyd"><%--广州移动 --%>
			<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/GzydMappings.js"></script>
		</kbs:if>
		<kbs:if code="gz12345"><%--广州12345 --%>
			<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gz12345/js/Gz12345.js"></script>
		</kbs:if>
	</head>
	<body>
		<div class="content_right_bottom">
			<div class="gonggao_titile">
				<div class="gonggao_titile_left" id="categoryNv">

				</div>
				<div class="gonggao_titile_right">
					<a href="javascript:void(0);">原 文</a>
					<s:if test="#request.sntryRevisions.size()>0">
					<a href="javascript:void(0);">版本对比</a>
					</s:if>
					<s:else> <a href="javascript:void(0);" style="background: url(${pageContext.request.contextPath}/resource/search/images/gonggao_bg1.jpg);cursor:default;">历史版本</a></s:else>
					<a href="javascript:void(0);" onclick="workFlowKnowledgeFeedback('${objId }')" style="display: none;">反 馈</a>
					<a href="javascript:void(0);">实例收藏</a>
					
				</div>
			</div>
			<div class="slzs_con">
				<div class="slzs_left">
					<div class="slzs_con_nr">
						<div class="dangqian">
							<div class="dangqian_1">
								
								<!-- <h2>
									2014-03-03 17:55
								</h2>
								<h3>
									ADMIN
								</h3>
								<h4>
									4567
								</h4>
								 -->
								<h5>
									<s:if test='#session.dataembed_enable'>
										<a href="javascript:void(0);" id="logSign" logInfo="${rbtResponse_faqId}|#|${question}|#|${categoryId}">解决</a>
									</s:if>
									<s:else>
										<a style="display:none" href="javascript:void(0);" id="logSign" logInfo="${rbtResponse_faqId}|#|${question}|#|${categoryId}">解决</a>
									</s:else>
									<s:if test='#request.flag_objectMarkread=="1"'>
										<a href="javascript:void(0);" style="cursor: default;background-color: gray" id="${flag_objectMarkread }_${catePath_ObjectMarkread }">已阅</a>
									</s:if>
									<s:else>
										<a href="javascript:void(0);" id="${flag_objectMarkread }_${catePath_ObjectMarkread }">签读</a>
									</s:else>
									<a href="javascript:void(0);" style="display: none;">点赞</a>
									<a class="hongse" href="javascript:void(0);">纠错(<span id="average">${average }</span>)</a>
									<kbs:input type="a" id="recommendbtn"  value="推 荐"  key="recommendObject" />
									<%-- 广州移动定制(Gassol.Bi 2016-4-8 15:33:24) --%>
									<kbs:if code="gzyd">
										<a href="javascript:void(0);" id="gzyd_flzs">分类展示</a>
										<a href="javascript:void(0);" onclick="javascript:GzydMappings.filing.guidang();">归档</a>
										<s:if test='#request.isableSMS==true'>
											<a href="javascript:void(0);" onclick="javascript:GzydMappings.sms.send(false);">发短信</a>
										</s:if>
									</kbs:if>
									<%--标准问收藏 --%>
									<!--@lvan.li 20160308 业务模板按钮 -->
									<s:if test='#request.isBiz'>
										<a href ="javascript:void(0);" id="yewumoban">业务模板</a>
									</s:if>
									<a href="javascript:void(0);" id="favClipQuestion">收 藏 </a>
									<kbs:if key="faultTree">
										<a href="javascript:void(0);" name="faultTree" 
											options="{params:{req:'${question }', reqId:'${rbtResponse_faqId}', flag:'look'}}"
											style="display: none;">故障树</a>
									</kbs:if>
									<kbs:if code="gz12345">
										<a href="javascript:void(0);" onclick="Gz12345.sms.open({
											question:'${question}'
										});">短 信 </a>
										<a href="javascript:void(0);" onclick="Gz12345.req.relate({
											req:'${question}', id:'${rbtResponse_faqId }'
										});">知识关联 </a>
										<%-- modify by eko.zhan at 2016-10-13 13:45 广州12345需求变更，不同的知识点最大化窗口能独立打开，而不是在同一个窗口加载 --%>
										<a href="javascript:void(0);" onclick="
											/*广州12345 Gassol.Bi 2016-8-29 15:33:24*/
											var url = $.fn.getRootPath() + '/app/search/search.htm?searchMode=3&askContent=${question }';
											var scrHeight = screen.height - 85, 
												scrWidth = screen.width - 20;
											var win = window.open(url , '${question }', 'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
												+ scrWidth + ',height=' + scrHeight);
											win.focus();
										">最大化</a>
										<%--广州12345 短信弹窗--%>
										<div id="dg-dialog"></div>
									</kbs:if>
								</h5>
								
							</div>
							
							<h1 style="display:inline-block;margin-bottom:10px;">
								<a href="javascript:void(0);" id="gzyd_sms_question" gzyd_sms_qId="${rbtResponse_faqId}" isableSMS="${isableSMS }">${question}</a>
							</h1>
							
							<div class="dangqian_2" id="gzyd_sms_answer" style="word-break: break-all; word-wrap: break-word;">
								${answer}
							</div>
							
							<s:if test="#request.p4Data.size > 0">
								<s:iterator value="#request.p4Data">
									<script type="text/javascript">
										$(function(){
											/*广州12345 Gassol.Bi 2016-8-29 15:33:24*/
											var conHeight = $('div[class="dangqian"]').height(),
												qaHeight = conHeight - 300 ;
											/*qaHeight 标准问及答案区高度 + 40操作按钮区高度 + 10操作区与答案区间隔 + 30修正值 */
											var ifHeight = $(window).height() - (qaHeight + 40 + 10 + 30);
											if(ifHeight > 300) $('iframe[src="${url}"]').height(ifHeight);
										});
									</script>
				        			<a href="javascript:void(0);" onclick="openP4('${url}');">[${title}]</a><br/>
				        			<iframe src="${url}" height="300" width="100%" frameborder="no" border="0"></iframe>
				        		</s:iterator>
							</s:if>
							<div class="dq_wz">
								<!-- <A href="javascript:void(0);"></A> -->
								<s:iterator value="#request.attachmentList" var="attch">
									<span>
				        				<!--<a href="${url}">${name}</a>-->
				        				<kbs:input type="a" href="${url}" value="${name}"  key="K-AttachmentDownload" 
				        					isDisplay="true" contentDisplay='<a href="javascript:void(0)" style="cursor:default;text-decoration:none;" >${name}</a>' />
									</span>
									<b>
									<script type="text/javascript">
										if (enableAdvReview){
											document.write("<a href=\"javascript:void(0);\" kbsconvert=\"1\" kbsname=\"${name }\" kbsid=\"${id }\">打开全部</a>");
										}else{
											document.write("<a href=\"javascript:openAttachment('${valueId}x','${index}','${id}','${name }');\">打开全部</a>");
										}
									</script>
									<!-- <a href="javascript:openAttachment('${valueId}x','${index}','${id}','${name }')">打开全部</a> -->
									<!-- <a href="javascript:void(0);" kbsconvert="1" kbsname="${name }" kbsid="${id }">打开全部</a> -->
				        			</b>
				        			<br>
								</s:iterator>
						<div id="openAttachment${attValId}" style="display:none;margin-top:5px;height:510px;" class="attachmentIndex shipin">xx</div>
							</div>

								

						</div>
						<s:if test="#request.rqValue.size > 1">
							<div class="xiamian">
								<p>
									<input name="" type="checkbox" value="" />
									<a href="javascript:void(0);" logInfo="${categoryId}|#|${objId}" id="preview">预览</a>
								</p>
							</div>
						</s:if>
						
						
						<s:iterator value="#request.rqValue" var="ov" status='st'>
						<s:if test="#request.rbtResponse_faqId != #ov.nodeId">
						<div>
							<div class="dkfi_news">
								<p style="width:88%;">
									<a href="javascript:void(0);" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:85%;text-align:left;"
									logInfo="${st.index + 1}|#|${ov.nodeId}|#|${ov.question}|#|${categoryId}|#|${objId}">
										${ov.question}
									</a>
									<s:set name="nowTime" value="new java.util.Date()"></s:set>
                        			<s:if test="#ov.attachment.vtimeExpire!=null  && #ov.attachment.vtimeExpire.getTime() < #nowTime.getTime()">
                        				<span style='color:red;'>(已过追溯期)</span>
                        			</s:if>
                        			<%-- modify by eko.zhan at 2016-01-06 11:10 
                        				已过追溯期逻辑：知识配置了追溯日期且当前时间大于追溯日期
                        				未过追溯期逻辑：知识配置了追溯日期和结束日期，且 当前时间大于结束日期 且 当前时间小于追溯日期
                        			 --%>
                        			<s:elseif test="#ov.attachment.vtimeEnd!=null && #ov.attachment.vtimeExpire!=null && #nowTime.getTime()>#ov.attachment.vtimeEnd.getTime() && #ov.attachment.vtimeExpire.getTime() > #nowTime.getTime()">
                        				<span style='color:red;'>(未过追溯期)</span>
                        			</s:elseif>

								</p>
								<span style="width:10%;float:right;">
									<a class="detail" href="javascript:void(0);" 
									question="${ov.question}" 
									logInfo="${st.index + 1}|#|${ov.nodeId}|#|${ov.question}|#|${categoryId}|#|${objId}"
									style="float:left;padding-left:2px;">
										<font color="#CC0000">详情</font>
									</a>
									<s:if test="#session.dataembed_enable">
									<a href="javascript:void(0);" style="float:right;padding-right:2px;"
										logInfo="${ov.nodeId}|#|${ov.question}|#|${categoryId}|#|${st.index + 1}"
										name="signl">
										<font color="#CC0000">解决</font>
									</a>
									</s:if>
								</span>
							</div>
							<div style="display: none; break-all; word-wrap: break-word;">
								<div class="dangqian_2">
									${ov.answer}
								</div>
								<s:if test="#ov.p4.size > 0">
								<s:iterator value="#ov.p4" var="p">
									<a href="javascript:void(0);" onclick="openP4('${p.url}');">[${p.title}]</a><br/>
									 
									<iframe src="${p.url}"  height="300" width="100%" frameborder="no" border="0">
									</iframe>
									<br />
								</s:iterator>
								</s:if>
							</div>
						</div>
						</s:if>
						</s:iterator>
						
						
						<!--<s:iterator value="ontologyValues" var="ov">
							<div class="dkfi_news">
								<p>
									<a href="javascript:void(0);"> 
										<s:if test="#ov.ontologyValue.question.length()>22">
											<s:property value="#ov.ontologyValue.question.substring(0,20)+'...'" />
										</s:if> 
										<s:else>
											${ov.ontologyValue.question}
										</s:else> 
									</a>
								</p>
								<span>
									<a class="detail" href="${ov.ontologyValue.question}">详情</a> 
								</span>
							</div>
							<div style="display: none; break-all; word-wrap: break-word;">
								<div class="dangqian_2">
									${ans}
								</div>
								<s:iterator value="url_p4">
									<iframe src="${robot_context_path}p4data/<s:property/>">
									</iframe>
									<br />
								</s:iterator>
							</div>
						</s:iterator>-->
					</div>
				</div>
				<div class="slzs_right">
					<div class="slzs_right_title">
						<ul>
							<li class="dq">
								<p></p>
								<a href="javascript:void(0);">知识关联</a><b></b>
							</li>
							<li>
								<p></p>
								<a href="javascript:void(0);">实例对比</a><b></b>
							</li>
							<li>
								<p></p>
								<a href="javascript:void(0);">实例关联</a><b></b>
							</li>
						</ul>
					</div>
					<div class="slzs_right_con">
						<ul>
							<s:iterator value="#request.rbtResponse_relatedQuestions"
								var="rq">
								<li>
									<a href="javascript:void(0);" title="${rq}">${rq}</a>
								</li>
							</s:iterator>
						</ul>
						<ul style="display: none;">
							<div style="overflow-y:auto;max-height:250px;">
								<s:iterator value="#request.cmpareOnto">
									<li>
										<input type="checkbox"> <a href="javascript:void(0);" title="${name}" objId="${objectId}">${name}</a>
									</li>
								</s:iterator>
								<div id="showCmpareOntoDiv">
									
								</div>
							</div>
							<div><li class="slzs_anniu"><a href="javascript:void(0);">选中对比</a><a id="addObject" href="javascript:void(0);">添加文章</a></li></div>
						</ul>
						<ul style="display: none;">
							<s:iterator value="#request.relationOnto">
								<li>
									<a href="javascript:void(0);" title="${name}" objId="${objectId}">${name}</a>
								</li>
							</s:iterator>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--******************弹出框***********************-->
		<div class="satisfaction" id="satisfaction" style="position: absolute;z-index:100010;display:none;">
		
		<div class="satisfaction_x"><a href="#" onclick="closesatisfaction()">&nbsp;</a></div>
		  <form id="form1" name="form1" method="post" action="">
		  <input type="hidden" name="obj_id" value="${objId}" />
		  <input type="hidden" name="category_id" value="${category_id}" />
		  <div class="satisfaction_title">
		    <div class="satisfaction_title_div1" id="title_div">
		        <ul>
		          <li class="cl_333 fs14 fw ff_wei mt10">知识满意度：</li>
		          <li class="satisfaction_title_div1_fen"><span class="cl_hongse fs36 fw" id="score">5</span>分
	              <s:iterator value="#request.satisfacttype" var="va" status="sta">
	               <s:if test="#sta.index==0">
	                   <label>
	                  <input type="radio" name="satisfaction" value="${va.type}" id="RadioGroup1_0" onclick="changeinfo('${va.type}','${va.name}');" checked="checked"/>${va.name}
	                </label>
	               </s:if>
	               <s:else>
	               <label>
	                  <input type="radio" name="satisfaction" value="${va.type}" id="RadioGroup1_0" onclick="changeinfo('${va.type}','${va.name}');" />${va.name}
	                </label>
	                </s:else>
	              </s:iterator>
		         </li>
		         <li class="satisfaction_title_bottom">共<span class="fs14 cl_hongse ff_english fw" id="commcount"></span>人评价</li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div2" id="satisfaction_type">
		       <ul>
		         <li class="cl_333 fs14 fw ff_wei">选择评论类型：</li>
		         <li id="typelist" style="display: none">
		          <s:iterator value="#request.commenttype" var="va">
	               <label>
	                  <input type="radio" name="type" value="${va.type}" id="RadioGroup1_0" />${va.name}
	                </label>
	               </s:iterator>
		         </li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div3">
		       <ul>
		         <li class="cl_333 fs14 fw ff_wei">填写评论内容：</li>
		         <li>
		           <textarea name="content" cols="" rows="" id="sat_content"></textarea>
		         </li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div4">
		       <input type="button" class="satisfaction_btn" onclick="satisfactionsubmit();" value="提交评论" />
		   </div>
		 </div>
		 </form>
		 <div class="satisfaction_title1 cl_666">
		   <form id="form2">
		   <s:iterator value="#request.satisfacttype" var="va">
             <label>
                <input type="radio" name="filter" onclick="filterinfo('${va.type}');" value="${va.type}" id="filter" />${va.name}
              </label>
            </s:iterator>
            </form>
		 </div>
		 <div class="satisfaction_title2 cl_333 fw" id="satisfaction_content">
		   <p>评价内容</p>
		   <span>评价人</span> <span>评价时间</span> <span>满意度</span> <span>类型</span> <span>紧急程度</span> </div>
		  
		</div>
<!--******************弹出框***********************-->
<!--******************弹出框 原文***********************-->
<div class="gonggao_d" style="display:none;z-index:10001;position:absolute;">
	<div class="gonggao_dan">
		<div class="gonggao_dan_title"><b>原文</b><a href="#" id="closeYuanwen"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/images/xinxi_ico1.jpg" width="12" height="12" /></a></div>
		<div class="gonggao_dan_con">
			<ul id="fileTree" class="ztree" style="height:180px;width:420px;overflow:auto;"></ul>
		</div>
	</div>
</div>
<!--******************弹出框 原文***********************-->
		<jsp:include page="./ObjectVersion.jsp"></jsp:include>
		<jsp:include page="./ObjectSearchForComparison.jsp"></jsp:include>
		<jsp:include page="./FavBall.jsp"></jsp:include>
		<jsp:include page="./Recommend.jsp"></jsp:include>

<!--*********故障树选择****  -->
		<div id="dg-faultTree">
			<div id="da-faultTree"> </div>
		</div>
	</body>
</html>
