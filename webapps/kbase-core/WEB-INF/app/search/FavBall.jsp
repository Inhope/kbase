<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/f.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>
	</head>
	<body>
		<div class="fav-ball" style="display: none;">
			<div class="main">
				<div class="title">
					<b>收藏知识</b>
					<a href="javascript:void(0);">
						<img width="12" height="12" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg">
					</a>
				</div>
				<div class="fcontent">
					<div class="content-data">
						<form>
							收藏夹选择: 
							<select id="favOption">
							</select>
						</form>
					</div>
					<div class="bottom">
						<a class="tijiao" href="javascript:void(0);">
							<input type="button" value="保存">
						</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
