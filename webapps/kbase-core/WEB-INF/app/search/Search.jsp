<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yiban.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resource/search/js/NormSearch.js"></script>
<script type="text/javascript">
	var askContent = '${askContent}';
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
</head>

<body>



	<!--******************内容开始***********************-->

	<div class="content_right_bottom">
		<div class="slzs_con" style="height:auto;">
			<div class="slzs_left" style="margin-right:0;height:auto;">
				<div class="slzs_con_nr">
					<div class="dangqian">
						<div class="dangqian_1" style="float:left;">
						
							<h1>
								<font size="3">${question}</font>
								
							</h1>
							<!-- <h2>
									2014-03-03 17:55
								</h2>
								<h3>
									ADMIN
								</h3>
								<h4>
									4567
								</h4>
								 -->
							<a style="float:right;text-decoration:underline" class="detail" href="javascript:void(0);" question="${question}" title_="${question}"><font
							color="#CC0000">详情</font> </a>
						</div>
						<div class="dangqian_2"
							style="word-break: break-all; word-wrap: break-word;width:95%">
							${answer}</div>
					</div>
				</div>
			</div>
		</div>

		<div class="yiban_title">
			<div class="yiban_title_left">
				一般搜索，共 <span>${totalCount}</span>条记录
			</div>
			<div class="yiban_title_right">
				<a href="javascript:void(0);">预览</a> <input type="checkbox" value="" />
			</div>
		</div>


		<div class="yiban_con">
			<s:if test="#request.rbtResponse_relatedQuestions !=null && #request.rbtResponse_relatedQuestions.length>0">
				<div class="yiban_left">
			</s:if>
			<s:else>
				<div class="yiban_left" style="margin:0;">
			</s:else>

			<div class="yiban_con_title">
				<h1>
					<b>知识名称</b>
				</h1>
				<h2>
					<a id="createClick" href="javascript:void(0);">创建时间</a>
				</h2>
				<h3>
					<a id="editClick" href="javascript:void(0);">修改时间</a>
				</h3>
				<h4>创建人</h4>
				<h5>
					<a id="dscount" href="javascript:void(0);">热度</a>
				</h5>
				<h6></h6>
			</div>
			<div class="yiban_con_nr">
				<ul>
					<s:iterator value="#request.valueList" var="ov">
						<li class="dangqian">
							<div class="dangqian_1">
								<h1 style="text-align:left;">
									<a href="javascript:void(0);" title="${ov.name}"
										style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;max-width:80%;text-align:left;">
										${ov.name} 
									</a>
									<s:if test="#ov.editTime != null && (new java.util.Date().getTime() - #ov.editTime.getTime())<=24*3600*1000">
										<span style="width:10px;color:#f00;font-size:10px;line-height:21px;">
											new
										</span>
									</s:if>
								</h1>
								<h2>
									<s:date format="yyyy-MM-dd" name="#ov.createTime" />
								</h2>
								<h3>
									<s:date format="yyyy-MM-dd" name="#ov.editTime" />
								</h3>
								<h4>
									<s:if
										test="#ov.ontologyValue.editor==null || #ov.ontologyValue.editor==''">/</s:if>
									<s:else>${ov.editor}</s:else>
								</h4>
								<h5>${ov.hot}</h5>
								<h6>
									<div style="width:120px;">
									<a class="detail" href="javascript:void(0);" question="${ov.name}" title_="${ov.objectName }" style="float: right;"><font
										color="#CC0000">详情</font> </a>
									
									<s:if test="#ov.categoryId==null">
										<a objectId="${ov.objectId}" class="objectVersion" href="javascript:void(0)" style="width:50px;background:#67a909 none repeat scroll 0 0;color:fff;text-decoration:none;height:24px;float:left;margin-right:10px;line-height:24px;padding:0 10px;margin-top:5px;">
										版本对比</a>
									</s:if>
									<s:else>
										<a categoryId="${ov.categoryId}" categoryName="${ov.categoryName}" class="categoryTag" href="javascript:void(0)" style="width:50px;background:#67a909 none repeat scroll 0 0;color:fff;text-decoration:none;height:24px;float:left;margin-right:10px;line-height:24px;padding:0 10px;margin-top:5px;">
										分类展示
										</a>
									</s:else>
									</div>
								</h6>
							</div>
							<div style="display:none;">
								<div class="dangqian_2">${value}</div>
								<!-- 
									<s:iterator value="url_p4">
										<iframe src="${robot_context_path}p4data/<s:property/>" height="300" width="600" frameborder="no" border="0">
										</iframe>
										<br/>
									</s:iterator>
									 -->
							</div>
						</li>
					</s:iterator>
				</ul>

				<div class="yiban_ss">
					<input class="yiban_ss_kuang" name="askMore" type="text"
						value="${askMore}" /> <a href="javascript:void(0);"><input
						type="button" class="yiban_ss_anniu" value="在结果中搜索" /> </a>
				</div>

				<s:if test="totalPage>1">
					<div class="gonggao_con_nr_fenye" start="${start}" limit="${limit}"
						currentPage="${currentPage}" totalPage="${totalPage}">
						<a href="javascript:void(0);" onfocus="this.blur();">首页</a> <a
							href="javascript:void(0);" onfocus="this.blur();">上一页
						</a>
						<s:iterator value="new int[totalPage]" status="i">
							<s:if test="#i.index+1 == currentPage">
								<a href="javascript:void(0);" class="dang"
									style="text-decoration:none;cursor:default;"><s:property
										value="#i.index+1" /> </a>
							</s:if>
							<s:elseif test="currentPage < 5 && #i.index<10">
								<a href="javascript:void(0);" onfocus="this.blur();"><s:property
										value="#i.index+1" /> </a>
							</s:elseif>
							<s:elseif
								test="currentPage > totalPage -5 && #i.index < totalPage && #i.index > totalPage-11">
								<a href="javascript:void(0);" onfocus="this.blur();"><s:property
										value="#i.index+1" /> </a>
							</s:elseif>
							<s:elseif
								test="#i.index+6 > currentPage && #i.index-5 < currentPage">
								<a href="javascript:void(0);" onfocus="this.blur();"><s:property
										value="#i.index+1" /> </a>
							</s:elseif>
						</s:iterator>

						<a href="javascript:void(0);" onfocus="this.blur();">下一页></a> <a
							href="javascript:void(0);" onfocus="this.blur();">尾页</a>
					</div>
				</s:if>
				<form id="fenyeForm" method="post" style="display:none;"
					action="${pageContext.request.contextPath}/app/search/search.htm">
					<input type="text" hidden="hidden" name="start" value="${start}" />
					<input type="text" hidden="hidden" name="askContent"
						value="${askContent}" /> <input type="text" hidden="hidden"
						name="askMore" value="${askMore}" /> <input type="text"
						hidden="hidden" name="order" value="${order}" /> <input
						type="text" hidden="hidden" name="lastOrder" value="${lastOrder}" />
					<input type="text" hidden="hidden" name="asc" value="${asc}" /> <input
						type="text" hidden="hidden" name="isSearch" value="y" /> <input
						type="text" hidden="hidden" name="searchMode" value="" />
				</form>
			</div>
		</div>

		<s:if test="#request.rbtResponse_relatedQuestions !=null && #request.rbtResponse_relatedQuestions.length>0">
			<div class="yiban_right">
				<ul>
					<li class="yiban_right_title"><b>知识关联</b>
					</li>
					<s:iterator value="#request.rbtResponse_relatedQuestions"
						var="rq">
						<li>
							<a href="javascript:void(0);" title="${rq}">${rq}</a>
						</li>
					</s:iterator>
				</ul>
			</div>
		</s:if>


	</div>





	<!--******************内容结束***********************-->
<jsp:include page="./ObjectVersionForList.jsp"></jsp:include>
</body>
</html>

