<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		
		<script type="text/javascript">
			$(function(){
				$('a').click(function(){
					var _id = $(this).attr('_id');
					var _title = $(this).text();
					parent.TABOBJECT.open({
						id : _id,
						name : _title,
						title : _title,
						hasClose : true,
						url : '${pageContext.request.contextPath}/app/search/fault-graph!graphShow.htm?reqId=${param.questionId}&graphId=' + _id,
						isRefresh : true
					}, this);
				});
			});
		</script>
		
	</head>

	<body>
		<table cellpadding="1" cellspacing="1" class="box" width="100%">
			<tr>
				<td width="15%">分类</td>
				<td width="55%">名称</td>
				<td width="15%">创建人</td>
				<td width="15%">创建时间</td>
			</tr>
			<c:forEach items="${requestScope.list}" var="item">
				<tr>
					<td>${item.typeName }</td>
					<td><a href="javascript:void(0);" _id="${item.id }">${item.name }</a></td>
					<td>${item.userName }</td>
					<td>${item.createDate }</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>