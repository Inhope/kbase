<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinjian_dan.css"  />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: 30%;}
			.box td{width: 70%;}
			.box td>input[type=text]{width: 97%;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/utils/Math.uuid.js"></script>
		<script type="text/javascript"> 
			mxBasePath = '${pageContext.request.contextPath}/resource/mxGraph/';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/mxGraph/js/mxClient.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultGraph.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultGraph-ex.js"></script>
		<script type="text/javascript">
			/*检索内容*/
			window._PAGE_SEARCH = '${param.searchContent}';
			/*故障树图形数据*/
			window._GRAPH_ID = '${faultTree.id}';
			window._GRAPH_NAME = '${faultTree.name}';
			window._GRAPH_ROOT_ID = window._GRAPH_ID;
			/*初始化当前页面事件*/
			$(function(){
				/*节点编辑*/
				FaultGraph.edit.init();
				
				/*节点关联验证*/
				window.iframe_content = '<iframe name="dd-tree-choose-iframe" frameborder="0"  src="url" style="width:100%;height:99%;"></iframe>';
				window.iframe_buttons = 
				[{
	                text: '确定',
	                iconCls: 'icon-save',
	                handler: function () {
	                	var doc = $(window.frames['dd-tree-choose-iframe'].document),
	                		cellId = $('#cellId', doc).val(),
	                		cellName = $('#cellName', doc).val(),
	                		treeId = $('#treeId', doc).val(),
	                		treeName = $('#treeName', doc).val();
						if(cellId && cellName && treeId && treeName){/*添加关联节点数据*/
							var linkdesc = treeName + '->' + cellName;
							$('#linkId').val(cellId);
							$('#linkTreeId').val(treeId);
							$('#ct').textbox('setText', linkdesc);
							$('#linkdesc').val(linkdesc);
						} else {/*清空关联节点数据*/
							$('#linkId').val('');
							$('#linkTreeId').val('');
							$('#ct').textbox('setText', '');
						}
						$('#dd-tree-choose').dialog('close');
	                }
	            }, {
	                text: '取消',
	                iconCls: 'icon-cancel',
	                handler: function () {
	                	$('#dd-tree-choose').dialog('close');
	                }
	            }];
				$.extend($.fn.validatebox.defaults.rules, {
					selectValueType: {
						validator: function(value, param){
							var t = $(param[0]).combotree('tree');	// 获取树对象
							var n = t.tree('getSelected');
							if(n && n.type == 'type'){
								return false;
							}
							return true;
						}, 
						message: '请选择需要关联节点的故障树!' 
					} 
				});
			});
		</script>
	</head>
	<body>
		<!-- Creates a container for the outline -->
		<div id="outlineContainer" style="z-index: 1; position: absolute; overflow: hidden; 
			top: 0px; left: 1px; background-color: white;">
			<input id="ss" style="width:275px"></input>
		</div>
		
		<!-- 节点编辑窗 -->
		<div id="graphCellEditDIV" class="easyui-window" title="编辑" 
			data-options="draggable:true, closed:true, maximizable:false, minimizable:false" 
			style="top: 25px; left: 1px; width:275px;">
			<div style="padding:10px 10px;">
		        <form id="graphCellEdit" action="" method="post" enctype="multipart/form-data">
		        	<input type="hidden" id="cellId" name="cellId">
					<input type="hidden" id="treeId" name="treeId">
					<input type="hidden" id="cellPId" name="cellPId">
		            <table cellpadding="1" width="100%">
		                <tr>
		                    <td style="width: 25%;">名称:</td>
		                    <td style="width: 75%;">
		                    	<input class="easyui-textbox" id="cellName" name="cellName"
									data-options="width: '99%', required:true" />
		                    </td>
		                </tr>
		                <tr>
		                    <td>内容:</td>
		                    <td>
		                    	<input class="easyui-textbox" id="eventContent" name="eventContent"
		                    		data-options="height:'60px', width: '99%', multiline:true" />
		                    </td>
		                </tr>
		                <tr>
		                    <td>所属知识:</td>
		                    <td>
		                    	<input class="easyui-combobox" id="cc"
		                    		style="width: 75%; float: left;"
									data-options="
								        valueField: 'id',
								        textField: 'question',
								        url: $.fn.getRootPath() + '/app/search/fault-graph!listSuggested.htm',
								        mode: 'remote',
								        delay: 1000,
								        onBeforeLoad: function(param){
											var ques = $('#cc').combobox('getText');
											if(!ques) return false;
											param.ques = ques;
										},
										onSelect: function(record){
											$('#req').val(record.question);
											$('#reqId').val(record.id);
										}" />
									<input type="hidden" id="req" name="req"/>
									<input type="hidden" id="reqId" name="reqId"/>
								<a href="#" class="easyui-linkbutton" style="width: 24%; float: right; height: 22px;" 
									data-options="onClick: function(){
										$('#req').val('');
										$('#reqId').val('');
										$('#cc').combobox('clear');
									}">清除</a>
		                    </td>
		                </tr>
		                <tr>
		                    <td>附件:</td>
		                    <td>
		                    	<input class="easyui-filebox" id="attachment" name="attachment" 
		                    		style="width: 75%; float: left;"
		                    		data-options="buttonText: '选择', editable:false">
		                    	<a href="#" class="easyui-linkbutton" style="width: 24%; float: right; height: 22px;" 
		                    		data-options="onClick: function(){
										$('#attachment').filebox('clear');
									}">清除</a>
		                    </td>
		                </tr>
		                <tr>
		                    <td>节点:</td>
		                    <td>
		                    	<input class="easyui-combotree"  validType="selectValueType['#ct']" id="ct" 
		                    		style="width: 75%; float: left;"
									data-options="editable: false,
								        valueField: 'id',
								        textField: 'text',
								        url: $.fn.getRootPath() + '/app/search/fault-graph!syncGraphTree.htm',
										onBeforeSelect: function(record){
											if(record.type == 'tree'){
												var url = $.fn.getRootPath() + '/app/search/fault-graph!graphShow.htm';
									        		url += '?graphId=' + record.id + '&graphName=' + record.text;
												var content = window.iframe_content.replace(/url/g, url);
												$('#dd-tree-choose').dialog({
												    title: '节点关联(' + record.text + ')',
												    width: document.body.clientWidth*0.95,
												    height: document.body.clientHeight*0.85,
												    closed: false,
												    cache: false,
												    content: content,
												    modal: true,
												    buttons: window.iframe_buttons
												});    
												$('#dd-tree-choose').dialog('open');
											}
											return false;
										},
										onChange: function(newValue, oldValue){
											if(newValue && !/[\S]+\-\>[\S]+/.test(newValue))
												$('#ct').combotree('setValue', oldValue);
											if(!newValue) 
												$('#ct').combotree('setValue', '');
										}" />
								<input type="hidden" id="linkId" name="linkId">
								<input type="hidden" id="linkTreeId" name="linkTreeId">
								<input type="hidden" id="linkdesc" name="linkdesc">
								<a href="#" class="easyui-linkbutton" style="width: 24%; float: right; height: 22px;" 
									data-options="onClick: function(){
										$('#linkTreeId').val('');
										$('#linkId').val('');
										$('#linkdesc').val('');
										$('#ct').combotree('clear');
									}">清除</a>
		                    </td>
		                </tr>
		            </table>
		        </form>
		        <div style="text-align:center;padding:5px">
		            <a href="javascript:void(0);" class="easyui-linkbutton" id="graphCellSave">提交</a>
		        </div>
	        </div>
		</div>
		
		
		<!--故障树节点选择  -->
		<div id="dd-tree-choose"></div>  
	</body>
</html>
