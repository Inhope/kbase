<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/xinjian_dan.css"  />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: 30%;}
			.box td{width: 70%;}
			.box td>input[type=text]{width: 97%;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"> 
			mxBasePath = '${pageContext.request.contextPath}/resource/mxGraph/';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/mxGraph/js/mxClient.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultGraph.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultGraph-ex.js"></script>
		<script type="text/javascript">
			/*检索内容（节点关联-查看用）*/
			window._PAGE_SEARCH = '${param.searchContent}';
			/*当前标准问*/
			window._PAGE_REQID = '${param.reqId}';
			
			/*故障树图形数据*/
			window._GRAPH_ID = '${param.graphId}';
			window._GRAPH_ROOT_ID = window._GRAPH_ID;
			/*初始化当前页面事件*/
			$(function(){
				/*节点查看*/
				FaultGraph.show.init();
			});
		</script>
	</head>
	<body>
		<!-- Creates a container for the outline -->
		<div id="outlineContainer" style="z-index: 1; position: absolute; overflow: hidden; 
			top: 0px; left: 1px; background-color: white;">
			<input id="ss" style="width:275px"></input>
		</div>
		
		<!-- 节点编辑窗 -->
		<div id="graphCellShowDIV" class="easyui-window" title="查看" 
			data-options="draggable:true, closed:true, maximizable:false, minimizable:false" 
			style="top: 25px; left: 1px; width:275px;">
			<div style="padding:10px 10px">
		        <input type="hidden" id="swf_address"  name="swf_address" 
	        		value="<%=PropertiesUtil.value("swf_address") + "/convert/download.do?" %>">
	        	<!-- 关联节点用 -->
	        	<input type="hidden" id="cellId">
	        	<input type="hidden" id="treeId">
	        	<input type="hidden" id="treeName" value="${param.graphName }">
	        	
	            <table cellpadding="1" width="100%">
	                <tr>
	                    <td>名称:</td>
	                    <td>
	                    	<input class="easyui-textbox" id="cellName"
								data-options="width: '99%', editable:false" />
	                    </td>
	                </tr>
	                <tr>
	                    <td>内容:</td>
	                    <td>
	                    	<input class="easyui-textbox" id="eventContent"
	                    		data-options="height:'60px', width: '99%', multiline:true, editable:false" />
	                    </td>
	                </tr>
	                <tr>
	                    <td>所属知识:</td>
	                    <td>
	                    	<input class="easyui-textbox" id="cc"
								data-options="width: '99%', editable:false" />
	                    </td>
	                </tr>
	                <tr>
	                    <td>附件:</td>
	                    <td id="attachmentTd">
	                    	<input class="easyui-textbox" id="attachment"
								data-options="width: '80%', editable:false" />
	                    </td>
	                </tr>
	                <tr>
	                    <td>节点:</td>
	                    <td id="ctTd">
							<input class="easyui-textbox" id="ct"
								data-options="width: '80%', editable:false" />
	                    </td>
	                </tr>
	            </table>
	        </div>
		</div>
		<!--故障树节点选择  -->
		<div id="dd-tree-choose"></div>  
	</body>
</html>
