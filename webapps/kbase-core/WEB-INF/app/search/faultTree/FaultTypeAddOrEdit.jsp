<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树分类-编辑、新增</title>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	</head>
	<body>
		<form id="faultTypeAddOrEdit" action="" method="post">
			<input type="hidden" id="id_faultType" name="id_faultType" value="${faultType.id }" />
			<table cellspacing="0" class="box" style="width: 100%;margin-top: 1px;">
				<tr>
					<th width="30%">故障树分类名称：</th>
					<td width="70%">
						<input class="easyui-textbox" id="name_faultType" name="name_faultType" value="${faultType.name }"
							data-options="width: '99%', required:true" />
						<input type="hidden" id="name2_faultType" name="name2_faultType" value="${faultType.name }"/>
					</td>
				</tr>
				<tr>
					<th width="30%">故障树分类：</th>
					<td width="70%">
						<input class="easyui-combotree" style="width: 99%;" value="${faultType.parent.name }" id="pid_faultType" name="pid_faultType"
							data-options="url:'${pageContext.request.contextPath}/app/search/fault-type!syncData.htm',
	                     	 	animate:true,
	                     	 	loadFilter: function(data, parent){
	                     	 		var data_ = new Array();
	                     	 		$(data).each(function(){
	                     	 			if(this.id != '${faultType.id }') 
	                     	 				data_.push(this);
	                     	 		});/*不允许选择当前节点或者及其子节点作为父节点*/
	                     	 		return data_;
	                     	 	},
			    				onSelect: function(record){
			    					$('input[name=\'pid_faultType\']:eq(0)').val(record.id);
								}">
					</td>
				</tr>
				<tr>
					<th width="30%">备注：</th>
					<td width="70%">
						<input class="easyui-textbox"  id="remark_faultType" name="remark_faultType" value="${faultType.remark }"
							data-options="width: '99%', required:true" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
