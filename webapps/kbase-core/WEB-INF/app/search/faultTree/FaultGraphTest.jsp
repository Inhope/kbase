<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"  />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"> 
			mxBasePath = '${pageContext.request.contextPath}/resource/mxGraph/';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/mxGraph/js/mxClient-fomat.js"></script>
		<script type="text/javascript">
			function main(container) {
			    if (!mxClient.isBrowserSupported()) {
			        mxUtils.error('Browser is not supported!', 200, false);
			    } else {
			        var graph = new mxGraph(container);
			        graph.setEnabled(false);
			        var highlight = new mxCellTracker(graph, '#00FF00');
			        graph.setTooltips(true);
			        graph.addListener(mxEvent.CLICK, function(sender, evt) {
			            var cell = evt.getProperty('cell');
			            if (cell != null) {
			                console.log(cell);
							cell.setAttribute('firstName', 'Daffy');
							cell.setAttribute('lastName', 'Duck');
							console.log(cell);
							console.log(cell.getAttribute('firstName'));
							console.log(cell.getAttribute('lastName'));
			            }
			        });
			
			        graph.addListener(mxEvent.DOUBLE_CLICK, function(sender, evt) {
			            var cell = evt.getProperty('cell');
			            mxUtils.alert('Doubleclick: ' + ((cell != null) ? 'Cell': 'Graph'));
			            evt.consume();
			        });
			
			        var parent = graph.getDefaultParent();
			        graph.getModel().beginUpdate();
			        try {
			            var v1 = graph.insertVertex(parent, null, 'Click,', 20, 20, 60, 40);
			            var v2 = graph.insertVertex(parent, null, 'Doubleclick', 200, 150, 100, 40);
			            var e1 = graph.insertEdge(parent, null, '', v1, v2);
			        } finally {
			            // Updates the display
			            graph.getModel().endUpdate();
			        }
			    }
			};
		</script>
	</head>
	<body onload="main(document.getElementById('graphContainer'))">
		<div id="graphContainer"
			style="overflow:hidden;width:321px;height:241px;background:url('editors/images/grid.gif')">
		</div>
	</body>
</html>
