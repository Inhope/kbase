<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树分类管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$(function(){
				var that = window.FaultTree = {}, fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				
				fn.extend({
					getyyyyMMdd: function(){
						var date = new Date();
						date.setDate(date.getDate());
						var y = date.getFullYear();
						var m = date.getMonth()+1;
						var d = date.getDate();
						if(m < 10) m = '0' + m;
						if(d < 10) d = '0' + d;
						return y + '-' + m + '-' + d;
					},
					/*有弹窗*/
					shade: function(options){
						var before = options.before;
						/*事件不存在，或者事件存在但是返回true*/
						if(!before || (before && before.call(that, options))){
								var h = options.height, w = options.width;
								if(!w) w = document.body.clientWidth*0.98;
								if(!h) h = document.body.clientHeight*0.9;
								/*打开弹窗*/
								$('#dg-dialog').dialog({
									iconCls:'icon-save', resizable:true, 
									closed: true, modal:true,
									width:w, height:h, 
									title: options.title,
									queryParams: options.params,
									href: options.url,
									buttons: options.buttons
								});
								$('#dg-dialog').dialog('open');
						}
					},
					/*无弹窗*/
					submit: function(options){
						var title = options.title, url = options.url, 
							params = options.params, before = options.before;
						/*事件不存在，或者事件存在但是返回true*/
						if(!before || (before && before.call(that))){
							$.messager.confirm('确认', '确定要' + title + '选中的数据吗？', 
							function(r){
								if(r){
									$.post(url, params, 
										function(jsonResult) {
											var data = $.parseJSON(jsonResult);
											$.messager.alert('信息', data.msg);
											if(data.rst){
												$('#now-search').click();
											}
									}, 'json');
								}
							});
						}
					},
					
					
					/*获取选中的行*/
					getChecked: function(){
						var ids = new Array();
						$($('#now-tab').datagrid('getChecked')).each(function(){
							ids.push(this.id);
						});
						return ids;
					},
					/*获取单行选中的行*/
					getSingleChecked: function(){
						var ids = fn.getChecked();
						if(ids.length == 0) {
							$.messager.alert('警告', '请选择要操作的数据!');
						} else if(ids.length > 1) {
							$.messager.alert('警告', '不能同时操作多条数据!');
						} else {
							return ids.join(',');
						}
						return false;
					},
					/*获取多行行选中的行*/
					getMultiChecked: function(){
						var ids = fn.getChecked();
						if(ids == 0) {
							$.messager.alert('警告', '请选择要操作的数据!');
						} else {
							return ids.join(',');
						}
						return false;
					},

				});
				
				that.extend({
					init: function(){
						/*刷新列表*/
						var init = setTimeout(function (){
							clearTimeout(init);/*取消定时器*/
							$('#now-tab').treegrid({
								url: $.fn.getRootPath() + '/app/search/fault-type!syncData.htm',
								queryParams: {}
				            });
						}, 500);



						/*查询按钮*/
						$('#now-search').click(function(){
							var queryParams = $("#now-tab").datagrid('options').queryParams;
							queryParams.name = $('#name').textbox('getValue');
							queryParams.userName = $('#userName').textbox('getValue');
					        queryParams.createDate = $('#createDate').datebox('getValue');


					        $('#now-tab').datagrid({
								url: $.fn.getRootPath() + '/app/search/fault-type!syncData.htm',
								queryParams: queryParams
				            });
					        $("#now-tab").datagrid('reload');
						});
						
						/*重置按钮*/
						$('#now-clear').click(function(){
							$('#name').textbox('setValue', '');
							$('#userName').textbox('setText', '');
							$('#createDate').datebox('setValue', '');
						});

						/*编辑、新增弹窗-相关按钮*/
						var buttons = 
							[{
				                text: '保存',
				                iconCls: 'icon-save',
				                handler: function () {

								var name = $("#name_faultType").val();
								var name2 = $("#name2_faultType").val();

								if(name == ''){
									 $.messager.alert('信息', "故障树分类名称不能为空!");
									 return;
								}
				                
								$.post($.fn.getRootPath() + '/app/search/fault-type!checkData.htm',
										 {"name":name,"name2":name2},
										 function(data){
											 var d = $.parseJSON(data);
											 if(d.rst){
												 $('#faultTypeAddOrEdit').form('submit', {
												        url: $.fn.getRootPath() + '/app/search/fault-type!addOrEditFaultTypeDo.htm', 
												        onSubmit:function(params){
															return $(this).form('validate');
												        },
												        success:function(jsonResult){
												        	jsonResult = eval('(' + jsonResult + ')');
												        	$.messager.alert('信息', jsonResult.msg);
												        	if(jsonResult.rst) {
												        		$('#now-search').click();
																$('#dg-dialog').dialog('close');
												        	}
												        }
												    });
											  }else{
												  $.messager.alert('信息', d.msg);
											  }
											
										 });
				                }
				            }, {
				                text: '取消',
				                iconCls: 'icon-cancel',
				                handler: function () {
				                	$('#dg-dialog').dialog('close');
				                }
				            }];

						/*新增按钮*/
						$('#add').click(function(){
							fn.shade({
								width: 400, 
							    height: 250, 
							    title: '新增',
							    url: $.fn.getRootPath() 
							    	+ '/app/search/fault-type!addOrEditFaultTypeTo.htm',
							    buttons: buttons
							});

						});
						
						/*编辑按钮*/
						$('#edit').click(function(){
							var id_faultType = fn.getSingleChecked();
							if(id_faultType) fn.shade({
								width: 400, 
							    height: 250,  
							    title: '编辑',  
							   	params: {'id_faultType': id_faultType}, 
							    url: $.fn.getRootPath() 
							    	+ '/app/search/fault-type!addOrEditFaultTypeTo.htm',
							   	buttons: buttons
							});

							fn.clearInput();
						});
						
						/*删除按钮*/
						$('#del').click(function(){
							var ids = fn.getMultiChecked();
							if(ids) fn.submit({
							    title: '删除',
							    params: {'ids': ids}, 
							    url: $.fn.getRootPath() 
							    	+ '/app/search/fault-type!delFaultType.htm'
							});
						});
					}
				});
				/*初始化*/
				that.init();
			});
		</script>
	</head>
	<body>
    	<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto;">
			<div>
				名称：<input class="easyui-textbox" id="name" style="width:150px;">&nbsp;&nbsp;
				创建人：<input class="easyui-textbox" id="userName" style="width:150px;">&nbsp;&nbsp;
				创建日期：<input class="easyui-datebox" id="createDate" style="width:100px;">&nbsp;&nbsp;
				<a href="javascript:void(0);" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
				<a href="javascript:void(0);" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
			</div>
			<div>
				<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" plain="true" id="add">新增</a>
		    	<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" plain="true" id="edit">编辑</a>
		    	<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" plain="true" id="del">删除</a>
		    </div>
		</div>
		<!-- 列表 -->
		<table id="now-tab" title="故障树分类管理(海尔)" 
			data-options="url: '', method: 'POST', 
				idField:'id', treeField:'text',
				iconCls: 'icon-reload', 
				nowrap: false, rownumbers: true,
				toolbar: '#now-toolbar', 
		        fit: true,  fitColumns:true,
		        singleSelect:true,
		        checkOnSelect:false,
		        selectOnCheck:false">
			<thead>
				<tr>
					<th data-options="field:'id',checkbox:true"></th>
					<th data-options="field:'text',width:120,align:'left'">类型</th>
					<th data-options="field:'remark',width:150,align:'left'">备注</th>
					<th data-options="field:'createUserName',width:70,align:'left'">创建人</th>
					<th data-options="field:'createDate',width:70,align:'left'">创建日期</th>
				</tr>
			</thead>
		</table>
		
		<!-- 编辑、新增  -->
		<div id="dg-dialog"></div>
	</body>
</html>
