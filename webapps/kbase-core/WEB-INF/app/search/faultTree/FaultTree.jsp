<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树管理</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultTree.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#tree-help').click(function(){
					window.open($.fn.getRootPath() 
						    	+ '/app/search/fault-tree!test.htm', '测试');
				});
			});
		</script>
	</head>
	<body class="easyui-layout">
    	<div data-options="region:'west',title:'分类',split:true" style="width:21%;">
    		<div style="margin:10px 10px; text-align: right;">
    			<kbs:if key="faultTypeAdd">
    				<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" id="faultType_add">新增</a>
    			</kbs:if>
    			<kbs:if key="faultTypeEdit">
    				<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" id="faultType_edit">编辑</a>
    			</kbs:if>
    			<kbs:if key="faultTypeDel">
    				<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" id="faultType_del">删除</a>
    			</kbs:if>
			</div>
    	
    		<ul id="now-tree" class="easyui-tree" 
    			data-options="url:'${pageContext.request.contextPath}/app/search/fault-type!syncData.htm', 
    				animate:true, 
    				onClick: function(node){
    					/*刷新列表*/
    					$('#now-search').click();
					}
				">
    			
    		</ul>
    	</div>
    	<div data-options="region:'center',title:'列表',iconCls:'icon-ok',split:true" style="width:79%;">
			<!--导航栏  -->
			<div id="now-toolbar" style="padding:5px;height:auto;">
				<div>
					名称：<input class="easyui-textbox" id="name" style="width:150px;">&nbsp;&nbsp;
					创建人：<input class="easyui-textbox" id="userName" style="width:150px;">&nbsp;&nbsp;
					创建日期：<input class="easyui-datebox" id="createDate" style="width:100px;">&nbsp;&nbsp;
					<a href="javascript:void(0);" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
				</div>
				<div>
					<s:set var="sum" value="0"></s:set>
					<kbs:if key="faultTreeAdd">
						<a href="javascript:void(0);" title="新增" class="easyui-linkbutton"  iconCls="icon-add" plain="true" id="add">新增</a>
						<s:set var="sum" value="#sum+1"></s:set>
					</kbs:if>
					<kbs:if key="faultTreeEdit">
						<a href="javascript:void(0);" title="编辑" class="easyui-linkbutton"  iconCls="icon-edit" plain="true" id="edit">编辑</a>
						<s:set var="sum" value="#sum+1"></s:set>
					</kbs:if>
			    	<kbs:if key="faultTreeDel">
						<a href="javascript:void(0);" title="删除" class="easyui-linkbutton"  iconCls="icon-remove" plain="true" id="del">删除</a>
						<s:set var="sum" value="#sum+1"></s:set>
					</kbs:if>
					<s:if test="#sum>0">
						<span class="datagrid-btn-separator" style="vertical-align: middle; height: 15px;display:inline-block;float:none"></span>
					</s:if>
			    	<kbs:if key="faultGraphEdit">
			    		<a href="javascript:void(0);" title="故障树" class="easyui-linkbutton"  iconCls="icon-archive" plain="true" id="tree-edit">故障树(编辑)</a>
			    	</kbs:if>
			    	<kbs:if key="faultGraphShow">
			    		<a href="javascript:void(0);" title="故障树" class="easyui-linkbutton"  iconCls="icon-archive" plain="true" id="tree-show">故障树(查看)</a>
			    	</kbs:if>
			    	<a href="javascript:void(0);" title="故障树" class="easyui-linkbutton" style="display: none;" 
			    		iconCls="icon-help" plain="true"  id="tree-help">故障树(测试)</a>
			    </div>
			</div>
			<!-- 列表 -->
			<table id="now-tab" data-options="url: '', method: 'POST', 
					rownumbers: true, nowrap: false, 
					pagination:true,
					toolbar: '#now-toolbar',
			        fit: true,  fitColumns:true,
			        singleSelect:true">
				<thead>
					<tr>
						<th data-options="field:'id',checkbox:true"></th>
						<th data-options="field:'name',width:100,align:'left'">名称</th>
						<th data-options="field:'typeName',width:100,align:'left'">类型</th>
						<th data-options="field:'userName',width:80,align:'left'">创建人</th>
						<th data-options="field:'createDate',width:80,align:'left'">创建日期</th>
					</tr>
				</thead>
			</table>
		</div> 
		
		<!-- 编辑、新增  -->
		<div id="dg-dialog"></div>
	</body>
</html>
