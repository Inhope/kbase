<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>故障树管理-编辑、新增</title>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	</head>
	<body>
		<form id="faultTreeEdit" action="" method="post">
			<input type="hidden" name="id" value="${faultTree.id }" />
			<table cellspacing="0" class="box" style="width: 100%;margin-top: 1px;">
				<tr>
					<th width="30%">故障树名称：</th>
					<td width="70%">
						<input class="easyui-textbox" name="name" value="${faultTree.name }"
							data-options="width: '99%', required:true" />
					</td>
				</tr>
				<tr>
					<th width="30%">故障树分类：</th>
					<td width="70%">
						<input class="easyui-combotree" style="width: 99%;" value="${faultTree.faultType.name }"
							data-options="url:'${pageContext.request.contextPath}/app/search/fault-type!syncData.htm',
	                     	 	animate:true, 
			    				onSelect: function(record){
			    					$('input[name=\'typeId\']:eq(0)').val(record.id);
								}">
	                     <input type="hidden" name="typeId" value="${faultTree.faultType.id }">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
