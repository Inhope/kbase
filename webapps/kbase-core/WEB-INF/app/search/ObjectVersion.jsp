<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML >
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dianzi_dan1.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/ObjectVersion.js"></script>
  	<script type="text/javascript">
		$(function(){
			//初始化方法
			function init(){
				//去掉勾选
				$("input[type=checkbox]:checked").each(function(){
					$(this).attr('checked',false);
				});
			}
			//屏蔽或者公开的方法(flag=='0'公开,flag=='1'屏蔽)
			function maskObject(flag){
				var bl = true, parameters = {},title = "屏蔽", versionIds = new Array();
				$("input[type=checkbox]:checked").each(function(){
					//如果是公开，数据中不应有已公开的数据
					if(flag=='0'&&$("#"+$(this).attr('id')+"_0").length>0){
						parent.layer.alert("不应包含已公开的数据!", -1);
						return bl = false;
					}
					//如果是屏蔽，数据中不应有已屏蔽的数据
					if(flag=='1'&&$("#"+$(this).attr('id')+"_1").length>0){
						parent.layer.alert("不应包含已公开的屏蔽!", -1);
						return bl = false;
					}
					versionIds.push("'"+$(this).attr('id')+"'");
				});
				parameters.flag = flag;
				if(flag=='0')title = "公开";//标题
				//获取的数据不正确,跳出方法
				if(!bl)return bl;
				if(versionIds.length>0){
					parent.layer.confirm("你确定要"+title+"这些版本吗？", function(){
						versionIds = versionIds.join(',');
						parameters.objectId = window.objId;
						parameters.versionIds = versionIds;
						$.ajax({
					   		type: "POST",
					   		url : $.fn.getRootPath()+'/app/search/search!maskObject.htm',
					   		data : parameters,
					   		async: true,
					   		dataType : 'json',
					   		success: function(result){
					   			if(result=="1"){
									parent.layer.alert("操作成功!", -1);
									$("input[type=checkbox]:checked").each(function(){
										//公开
										if(flag=='0'){
											$("#"+$(this).attr('id')+"_1").text("");
											$("#"+$(this).attr('id')+"_1").attr("id",$(this).attr('id')+"_0");
										}
										//屏蔽
										if(flag=='1'){
											$("#"+$(this).attr('id')+"_0").text("("+title+")");
											$("#"+$(this).attr('id')+"_0").attr("id",$(this).attr('id')+"_1");
										}
									});
									//修改父页面"上个版本"的版本号
									bl = true;
									$("input[type=checkbox]").each(function(index,element){
										//公开
										if(index!=0&&$("#"+$(this).attr('id')+"_0").length>0){
											$("#prevVersionCompBtn").attr("pv",$(this).attr('id'));
											return bl = false;
										}
									});
									if(bl)$("#prevVersionCompBtn").attr("pv",'');
									init();//初始化
								}else{
									parent.layer.alert("操作失败!", -1);
								}
					   		},
					   		error: function(){}
						});
					});
				}else{
					parent.layer.alert("请选择要"+title+"的版本!", -1);
				}
			}
			$('#gongkai').click(function(){
				 maskObject('0');
			});
			$('#pingbi').click(function(){
				 maskObject('1');
			});
			//初始化选择项
			init();
		});
		
	</script>
  
  </head>
  
  <body>
    <!-- 实例历史版本列表 -->
	<div class="index_chu" style="display: none;">
		<div class="index_d">
			<div class="index_dan">
				<div class="index_dan_title">
					<b>版本对比</b><a href="javascript:void(0);"><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg"
							width="12" height="12" />
					</a>
				</div>
				<div class="index_dan_con">
					<ul>
						<li class="biaoge">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="biaoti" align="center">
										版本号
									</td>
									<td class="biaoti" align="center">
										版本变更时间
									</td>
									<td class="biaoti" align="center">
										版本变更原因
									</td>
								</tr>
								<%--是否有屏蔽/公开权限 --%>
								<s:set var="maskObject" value="1" ></s:set>
								<s:iterator value="#session.session_user_key.roles" var="va1">
									<s:iterator value="#va1.menus" var="va2">
										<s:if test="#va2.key == 'K-maskObject'"><s:set var="maskObject" value="0" ></s:set></s:if>
									</s:iterator>
								</s:iterator>
								<s:iterator var="sr" value="#request.sntryRevisions" status="i">
									<s:if test='#maskObject=="0"'>
										<tr>
											<td align="center">
												<input type="checkbox" id="${sr.revisionId}" index="<s:property value="#request.sntryRevisions.size() - #i.index"/>"/>
												<s:property value="#request.sntryRevisions.size() - #i.index"/>
												<s:if test="#i.index == 0">
													(当前)
												</s:if>
												<span id="${sr.revisionId}_${sr.status }"><s:if test='#sr.status == "1"'>(屏蔽)</s:if></span>
											</td>
											<td align="center">
												<s:date format="yyyy-MM-dd HH:mm:ss" name="#sr.commitTime"/>
											</td>
											<td align="center">
												${sr.message}
											</td>
										</tr>
									</s:if>
									<s:else>
										<s:if test='#sr.status == "0"'>
											<tr>
												<td align="center">
													<input type="checkbox" id="${sr.revisionId}" index="<s:property value="#request.sntryRevisions.size() - #i.index"/>"/>
													<s:property value="#request.sntryRevisions.size() - #i.index"/>
													<s:if test="#i.index == 0">
														(当前)
													</s:if>
													<span id="${sr.revisionId}_${sr.status }"><s:if test='#sr.status == "1"'>(屏蔽)</s:if></span>
												</td>
												<td align="center">
													<s:date format="yyyy-MM-dd HH:mm:ss" name="#sr.commitTime"/>
												</td>
												<td align="center">
													${sr.message}
												</td>
											</tr>
										</s:if>
									</s:else>
								</s:iterator>
							</table>
						</li>
					</ul>
				</div>
				<div class="xgai">
					<a href="javascript:void(0);" class="tijiao"><input type="button" value="对比" /></a>
					<myTag:input className="button" type="a" id="pingbi"  value="屏蔽"  key="K-maskObject" />
					<myTag:input className="button" type="a" id="gongkai" value="公开"  key="K-maskObject" />
					<%--  
						<a href="javascript:void(0);" class="button">
							<input id="pingbi" type="button" value="屏蔽" />
						</a>
						<a href="javascript:void(0);" class="button">
							<input id="gongkai" type="button" value="公开" />
						</a>
					--%>
				</div>
			</div>
		</div>
	</div>
	
  </body>
</html>
