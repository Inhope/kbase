<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@page import="com.eastrobot.util.WukongUtil"%>
<%@ taglib prefix="myTag" uri="/WEB-INF/kbs-tags.tld" %>

<%
	boolean isGzyd = SystemKeys.isGzyd();
	pageContext.setAttribute("isGzyd", isGzyd);
%>

<!DOCTYPE html PUbLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/quanwen.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/resource/docicons/css/doc-icons.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForFull.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SeniorSearchForm.js"></script>
		<style>
			.quanwen_dan_left a {
				cursor: pointer;
				background:none !important;
			}
			
			.quanwen_dan_left a:hover,.quanwen_dan_left a:active {
				text-decoration: underline;
				cursor: pointer;
				background:none !important;
			}
			
			.quanwen_dan_right a {
				cursor: pointer;
				background:none !important;
			}
			
			.quanwen_dan_right a:hover,.quanwen_dan_right a:active {
				text-decoration: underline;
				cursor: pointer;
				background:none !important;
			}
		</style>
		<script type="text/javascript">
			pageContext = {
				converterPath: '${requestScope.converterPath}'
			}
		
			var searchContent = '${content}';
			var robotContextPath = '${robotContextPath}';
			
			$(function(){
				var _iframe = $(parent.document).find('div.content_content iframe:visible');
				_iframe.height($(document).height());
				$('div.bh').each(function(){
						var bh =$(this).html();
						var category = '<a style="cursor:pointer;text-decoration:underline" bh="'+$(this).attr('bh')+'">'
							+ bh.substr(bh.lastIndexOf('--&gt; ')+7)
							+ '</a>';
						var start = bh.indexOf('--&gt; ')+7;
						var end = bh.lastIndexOf('--&gt; ')+7;
						var text =  bh.substr(0, end);
						text = text.substr(start,text.length);
						text = text+category;
						if($.trim(bh.replace(new RegExp('--&gt;', 'ig'), '')) == ''){
							text = '未找到分类';
						}
						$(this).html(text);
				});
				
				$('div.bh a').click(function(){
					
					/**
					*@lvan.li 20160517 针对广州移动模糊搜索出来二维表格中的知识点，点击目录不能跳转问题优化
					* isCate = true 则为非叶子节点，点击目录跳转到二维表格页面，否则按照正常逻辑跳转到分类展示页面
					*
					* modify by eko.zhan at 2016-07-12 10:58 附件搜索页面路径默认打开实例，如果有question则打开标准问页面，如果是分类展示则打开分类展示页面
					*/
					var bh = $(this).attr('bh');
					var question = $(this).parent('div').attr('question');
					var end = bh.length-1; 
					var url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id=' + $(this).parent().attr('categoryId')+'&isCate=true';
					
					var isGzyd = ${isGzyd };
					
					if (isGzyd){
						if(bh.charAt(end) == 'L'){/*针对广州移动，叶子节点跳转到二维表格*/
							url = $.fn.getRootPath()+"/app/category/category.htm?type=1&categoryId="+$(this).parent().attr('categoryId')+"&categoryName="+$(this).html();
						}
					}else if (question!=''){
						url = $.fn.getRootPath() + '/app/search/search.htm?searchMode=3&askContent=' + question;
					}
					
					
					parent.TABOBJECT.open({
							id : $(this).parent().attr('categoryId'),
							name :  $(this).html(),
							hasClose : true,
							url : url,
							isRefresh : true
					}, this);
				});
			});
		</script>
	</head> 
	
	<body>
		<div class="content_right_bottom">
			<form action="${pageContext.request.contextPath}/app/search/senior-search.htm" method="post">
				<div class="quanwen">
					<div class="quanwen_1">
						<ul>
							<li>
								<p>
									搜索内容
								</p>
								<input type="hidden" value="${content}" />
								<input id="content" name="content" type="text" />
							</li>
							<li>
								<p>
									搜索类型
								</p>
								<label>
									<s:if test="#request.searchType == 1">
										<input type="radio" name="searchType" value="1"
											checked="checked" />
									</s:if>
									<s:else>
										<input type="radio" name="searchType" value="1" />
									</s:else>
									全文
								</label>
								<label>
									<s:if test="#request.searchType == 2">
										<input type="radio" name="searchType" value="2"
											checked="checked" />
									</s:if>
									<s:else>
										<input type="radio" name="searchType" value="2" />
									</s:else>
									实例
								</label>
								<label>
									<s:if test="#request.searchType == 3">
										<input type="radio" name="searchType" value="3"
											checked="checked" />
									</s:if>
									<s:else>
										<input type="radio" name="searchType" value="3" />
									</s:else>
									附件
								</label>
							</li>
							<li>
								<p>
									知识目录
								</p>
								<select id="catgoryDirId" name="catgoryDirId"
									disabled="disabled">
									<option value="" selected="selected">
										全部
									</option>
									<s:iterator var="oc" value="#session.ontologyCategorys">
										<option value="<s:property value="#oc.id"/>"
											<s:if test="#oc.id == #request.catgoryDirId">selected="selected"</s:if>>
											<s:property value="#oc.name" />
										</option>
									</s:iterator>
								</select>
							</li>
						</ul>
					</div>
					<div class="quanwen_2">
						<ul>
							<li>
								<p>
									创建日期
								</p>
								<input id="startDate" name="startDate" type="text"
									value="${startDate}" class="Wdate"/>
							</li>
							<li>
								<p>
									知识时效
								</p>
								<select id="agingType" name="agingType" disabled="disabled">
									<option value="0">
										全部
									</option>
									<s:if test="#request.agingType == 1">
										<option selected="selected" value="1">
											常态
										</option>
									</s:if>
									<s:else>
										<option value="1">
											常态
										</option>
									</s:else>
									<s:if test="#request.agingType == 2">
										<option selected="selected" value="2">
											未过期
										</option>
									</s:if>
									<s:else>
										<option value="2">
											未过期
										</option>
									</s:else>
									<s:if test="#request.agingType == 3">
										<option selected="selected" value="3">
											未过追溯期
										</option>
									</s:if>
									<s:else>
										<option value="3">
											未过追溯期
										</option>
									</s:else>
									<s:if test="#request.agingType == 4">
										<option selected="selected" value="4">
											已过追溯期
										</option>
									</s:if>
									<s:else>
										<option value="4">
											已过追溯期
										</option>
									</s:else>
								</select>
							</li>
						</ul>
					</div>
					<div class="quanwen_3">
						<ul>
							<li>
								<p>
									至
								</p>
								<input id="endDate" name="endDate" type="text"
									value="${endDate}" class="Wdate"/>
							</li>
							<s:if test="#session.locationEnable">
								<li><p></p></li>
							</s:if>
							<s:else>
								<li style="display:<s:if test="#session.locationEnable">none;</s:if>">
									<p>
										地区
									</p>
									<select id="location" name="location">
										<option value="" selected="selected">
											全部
										</option>
										<s:iterator var="dt" value="#session.locasInSearch">
											<option value="<s:property value="#dt.tag"/>"
												<s:if test="#request.location == #dt.tag">selected="selected"</s:if>>
												<s:property value="#dt.name" />
											</option>
										</s:iterator>
									</select>
								</li>
							</s:else>
							<s:if test="#session.brandEnable">
								<li><p></p></li>
							</s:if>
							<s:else>
								<li style="display:">
									<p>品牌</p>
									<select id="brand" name="brand">
										<option value="">全部</option>
										<s:iterator var="dt" value="#session.brandsInSearch">
											<option value="<s:property value="#dt.tag"/>" <s:if test="#request.brand == #dt.tag">selected="selected"</s:if>>
												<s:property value="#dt.name"/>
											</option>
										</s:iterator>
									</select>
								</li>
							</s:else>
							<li>
								<input class="an1" type="button" value="搜索" />
								<input class="an2" type="reset" value="重置" />
							</li>
						</ul>
					</div>
				</div>
			</form>
			<div class="quanwen_title">
				<div class="quanwen_title_left">
					高级搜索，共
					<span>${arrayPage.totalCount}</span>条记录
				</div>
			</div>
			<div class="quanwen_con">
				<div class="quanwen_left">
					<div class="quanwen_con_nr">
						<ul>
							<s:iterator var="ri" value="#request.arrayPage.displayResult">
								<s:set name="objectName" value="" />
								<s:set name="objectValue" value="" />
								<s:if test="#ri.objectName.indexOf('/') > -1">
									<s:set name="objectName" value="#ri.objectName.split('/')[0]" />
									<s:set name="objectValue" value="#ri.objectName.split('/')[1]" />
								</s:if>
								<s:else>
									<s:set name="objectName" value="#ri.objectName" />
								</s:else>
								<li>
									<div class="dangqian_1">
										<h1>
											<!--<a
												href="${searchPath}attachmentDown?objectId=${ri.objectId}&someTime=<%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())%>"><s:property value="#ri.name" />
											</a>
										-->
											<s:if test="#ri.type!='p4'">
												<%
														Map<String,Object>  ri = (Map<String,Object>)pageContext.findAttribute("ri");
												%>
												<div class="kbs-office <%=WukongUtil.getIcon((String)ri.get("name")) %>"></div>
											</s:if>
											<s:else>
												<div class="kbs-office kbs-office-rtf"></div>
											</s:else>
											<s:date var="currentTime" name="new java.util.Date()" format="yyyy-MM-dd HH:mm:ss"/>
											
											<s:if test="#ri.type=='p4'">
												<s:if test="#ri.name==null || #ri.name==''">
													<myTag:input type="a" href="javascript:void('${ri.fileId}');"
												value='未命名p4'  key="K-AttachmentDownload" isDisplay="true" contentDisplay='<a href="javascript:void(0)" style="cursor:default;text-decoration:none;" >未命名p4</a>' />
												</s:if>
												<s:else>
													<myTag:input type="a" href="javascript:void('${ri.fileId}');"
												value='${ri.name}'  key="K-AttachmentDownload" isDisplay="true" contentDisplay='<a href="javascript:void(0)" style="cursor:default;text-decoration:none;" >${ri.name}</a>' />
												</s:else>
											</s:if>
											<s:else>
												<myTag:input type="a" href="${searchPath}attachmentDown?attachmentId=${rfileId}"
												value='${ri.name }'  key="K-AttachmentDownload" isDisplay="true" contentDisplay='<a href="javascript:void(0)" style="cursor:default;text-decoration:none;" >${ri.name }</a>' />
												<a href="javascript:void(0);" _type="${ri.type}" style="color:red;text-decoration:none;" _fileId="${ri.rfileId}">&nbsp;&nbsp;&nbsp;(预览)</a>
											</s:else>
										</h1>
										<h2>
										</h2>
										<h3 style="float: right;">
											<div class ="bh" bh="${ri.bh }" question="<s:property value="#objectValue"/>" categoryId="${ri.categoryId}" style="font-size:14px;font-family:微软雅黑;line-height:28px;margin-left:20px;font-weight:bold;white-space:nowrap; text-overflow:ellipsis;overflow: hidden;">${ri.bhname }</div>
										</h3>
										<h4>

										</h4>
									</div>
								<!-- <div class="dangqian_2" style="position: relative;">
										<a href="javascript:void(0);">附件引用</a>
										<div class="quanwen_dan" style="display: none;">
											<div class="quanwen_dan_left">
												<ul>
													<li>
														<b>实例名</b>
														<br />
														<br />
													<br /></li>
													<li>
														<img class="zhong" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/quanwen_ico1.jpg" />
														<p id="${ri.realObjectId}" title="${objectName }" _bh = "${ri.bh }" _cid="${categoryId }">
															<a>
																
															</a>
														</p>
														<br />
														<br />
													<br /></li>
												</ul>
											</div>
										 
											<div class="quanwen_dan_right">
												<ul>
													<li>
														<b>问题</b>
														<br />
														<br />
													<br /></li>
													<li>:
														<s:if test="#ri.type=='p4'">
															<p id="${ri.valueId}" title='<s:property value="#objectValue" />'>
																<a>
																	<s:property value="#objectValue" />
																</a>
															</p>
														</s:if>
														<s:else>
															<p id="${ri.objectId}" title='<s:property value="#objectValue" />'>
																<a>
																	<s:property value="#objectValue" />
																</a>
															</p>
														</s:else>
													</li>
												</ul>
											</div>
										</div>
									</div>
								-->
									<div class="dangqian_3" style="display: none;">
										<s:property value="#ri.content" />
									</div>
								</li>
							</s:iterator>

						</ul>
						<div class="gaoji_ss" style="display: none;">
							<input class="gaoji_ss_kuang" name="" type="text" />
							<a href="javascript:void(0);" style="margin-left: -3px;">
								<input type="button" class="gaoji_ss_anniu" value="在结果中搜索" /> 
							</a>
						</div>
						<div class="gonggao_con_nr_fenye">
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">首页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == 1">
								<a href="javascript:void(0)" onfocus="this.blur();">上一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">上一页</a>
							</s:else>
							<s:iterator begin="1" end="#request.arrayPage.pageTotalNo" status="i">
								<s:if test="(#i.index + 1) + 4 > #request.arrayPage.pageNo && (#i.index + 1) - #request.arrayPage.pageNo < 5">
									<s:if test="(#i.index + 1) == #request.arrayPage.pageNo">
										<a href="javascript:void(0)" class="dang" onfocus="this.blur();">
											<s:property />
										</a>
									</s:if>
									<s:else>
										<a href="javascript:void(0)" onfocus="this.blur();" start="<s:property value="#i.index * #request.arrayPage.pageSize"/>" limit="${arrayPage.pageSize}">
											<s:property />
										</a>
									</s:else>
								</s:if>
							</s:iterator>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">下一页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">下一页</a>
							</s:else>
							<s:if test="#request.arrayPage.pageNo == #request.arrayPage.pageTotalNo">
								<a href="javascript:void(0)" onfocus="this.blur();">尾页</a>
							</s:if>
							<s:else>
								<a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a>
							</s:else>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>