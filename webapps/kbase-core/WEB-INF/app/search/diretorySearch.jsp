<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link
			href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css"
			rel="stylesheet" type="text/css" />
		<link
			href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yiban.css"
			rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" />
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<link
			href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dirSearch.css"
			rel="stylesheet" type="text/css" />
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/resource/search/js/dirSearch.js"></script>
			
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/log/operationLog.js"></script>
	</head>

	<body>



		<!--******************内容开始***********************-->

		<div class="content_right_bottom">

			<div class="dir_title">
				<div class="dir_title_left">
					目录搜索，共
					<span>${totalCount}</span>条记录：
				</div>
			</div>


			<div class="dir_con">
				<s:if test="#request.total > 0 ">
					<div class="dir_con_title">
						<h1>
							<b>目录路径</b>
						</h1>
						<h6></h6>
					</div>
				</s:if>
				<div class="dir_con_nr">
					<ul>
						<s:iterator value="#request.allDir" var="dir">
							<li class="dangqian">
								<div class="dangqian_1">
									<s:iterator value="#dir" var="dr">
										<a categoryId="${dr.id }" href="javascript:void(0);">
											${dr.name} </a>
									</s:iterator>
								</div>
							</li>
						</s:iterator>
					</ul>
					<s:if test="totalPage>1">
						<div class="gonggao_con_nr_fenye" start="${start}"
							limit="${limit}" currentPage="${currentPage}"
							totalPage="${totalPage}">
							<a href="javascript:void(0);" onfocus="this.blur();">首页</a>
							<a href="javascript:void(0);" onfocus="this.blur();">上一页</a>
							<s:iterator value="new int[totalPage]" status="i">
								<s:if test="#i.index+1 == currentPage">
									<a href="javascript:void(0);" class="dang"
										style="text-decoration: none; cursor: default;"><s:property
											value="#i.index+1" /> </a>
								</s:if>
								<s:elseif test="currentPage < 5 && #i.index<10">
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property
											value="#i.index+1" /> </a>
								</s:elseif>
								<s:elseif
									test="currentPage > totalPage -5 && #i.index < totalPage && #i.index > totalPage-11">
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property
											value="#i.index+1" /> </a>
								</s:elseif>
								<s:elseif
									test="#i.index+6 > currentPage && #i.index-5 < currentPage">
									<a href="javascript:void(0);" onfocus="this.blur();"><s:property
											value="#i.index+1" /> </a>
								</s:elseif>
							</s:iterator>

							<a href="javascript:void(0);" onfocus="this.blur();">下一页></a>
							<a href="javascript:void(0);" onfocus="this.blur();">尾页</a>
						</div>
					</s:if>
					<form id="fenyeForm" method="get" style="display: none;"
						action="${pageContext.request.contextPath}/app/search/diretory-search.htm">
						<input type="text" hidden="hidden" name="start" value="${start}" />
						<input type="text" hidden="hidden" name="dirName"
							value="${dirName}" />
							
						<s:iterator value="#request.dirDims" var="dim">
							<input type="text" hidden="hidden" name="${dim.code }" value="${dim.tag}" />
						</s:iterator>
					</form>
				</div>
			</div>

		</div>

	</body>
</html>

