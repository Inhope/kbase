<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="com.eastrobot.module.object.vo.RqValue"%>
<%@page import="com.eastrobot.module.object.vo.Attachment"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识展示-详情</title>
		<link href="${pageContext.request.contextPath}/library/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/buttons.css" rel="stylesheet" type="text/css" />
		
		<style type="text/css">
			.kbs-avatar{
				padding-left: 2px;
				padding-right: 2px;
			}
			.kbs-title, .kbs-title-helios{
				font-family: Arial,Helvetica,sans-serif;
				font-size: 14px;
				font-weight: 700;
				color: #000;
				margin-bottom:10px;
			}
			.kbs-title-helios{
				color: #666666;
				cursor: pointer;
			}
			.kbs-detail{
				color: #FF0000;
				cursor: pointer;
				text-decoration: underline;
			}
			.nav-tabs li a{
				padding: 5px;
			}
		</style>
				
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
		
		<script type="text/javascript">
			var swfAddress='${swfAddress}';
			var hotWord='${hotWord}';
			var objId = '${objId}';
			var human_id = '${job_id}';
			var faqId = '${rbtResponse_faqId}';
			var robot_context_path = '${robot_context_path}';
			var sessionUId = '${sessionUId}';
			var brand = '${brand}';
			var inner = '${inner}';
			var askContent = '${askContent}';
			var searchCtxPath = '${searchCtxPath}';
			var firstP4 = '<s:property value="#request.p4Data[0].url"/>';
			var categoryId = '${categoryId}';
			
			$(function(){
				//初始化高级预览模式
				$.kbase.convert.init({
					contextPath: swfAddress
				});
				
				//答案中的超链接
				$('div[kbs-answer="1"]').find('a').attr('target', '_blank');
				//纠错
				if ($('#btnErrcorrect').length>0){
					var _this = $('#btnErrcorrect');
					var _score = '${average }';
					_score = _score==''?0:Number(_score);
					if (_score>0){
						_this.text(_this.text() + ' (' + _score + ')');
					}
				}
				//P4Data
				$('iframe[_src!=""]').attr('src', $('iframe[_src!=""]').attr('_src')); 
				
				//加载导航
				var data = {'format':'json', 'async':true, 'nodeid':faqId, 'nodetype':3};
				$.getJSON(robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
					if (arr!=null){
						$(arr.reverse()).each(function(i, item){
							//name id type bh
							if (item.type!=3){
								$('#navCate').prepend('<li _type="'+ item.type +'" _id="' + item.id + '"><a class="kbs-title" herf="javascript:void(0);" style="cursor:pointer;">' + item.name + '</a></li>');
							}
						});
					}
				});
				//导航
				$('#navCate').find('li').live('click', function(){
					var _type = $(this).attr('_type');
					var _id = $(this).attr('_id');
					var _name = $(this).find('a').text();
					
					if (_type==2){
						//实例
						location.href = $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=10&objId=' + _id;
					}else if (_type==1){
						//分类
						parent.selectNodeById('', _id);
						parent.TABOBJECT.open({
							id : 'categoryTree',
							name : '分类展示',
							hasClose : true,
							url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id=' + _id,
							isRefresh : true
						}, this);
					}
					
				});
				
				//Tab
				$('.nav-tabs-panel').hide();
				$('.nav-tabs-panel:first').show();
				//Tab切换
				$('.nav-tabs li').hover(function(){
					$('.nav-tabs li').removeClass('active');
					$(this).addClass('active');
					
					//当前是第几个 li ？
					var _ind = $(this).index();
					$('.nav-tabs-panel').hide();
					$($('.nav-tabs-panel').get(_ind)).show();
					
				});
				
				//单击 预览 触发
				$('div.checkbox input[type="checkbox"]').click(function(){
					if ($(this).attr('checked')!=undefined){
						$('div[_id="kbs-fqa"]').find('.row:last').show();
					}else{
						$('div[_id="kbs-fqa"]').find('.row:last').hide();
					}
				});
				
				//单击 详情 触发
				$('.kbs-detail').click(function(){
					var _question = $(this).prev('.kbs-title-helios').text();
					location.href = $.fn.getRootPath() + '/app/search/search.htm?searchMode=8&askContent=' + _question;
				});
				
				//单击标题展开答案预览
				$('.kbs-title-helios').click(function(){
					var $row = $(this).parent('.row').next('.row');
					if ($row.length>0){
						if ($row.get(0).style.display=='none'){
							$row.show();
						}else{
							$row.hide();
						}
					}
				});
				
				//签读 倒计时
				if ($('#btnMarkread').length>0){
					var limit = 20;
					$('#btnMarkread').attr('disabled', 'disabled');
					var countdownTimer = window.setInterval(function(){
						limit--;
						$('#btnMarkread').text('签读 ' + limit);
						if (limit<1){
							$('#btnMarkread').removeAttr('disabled').text('签读');
							window.clearInterval(countdownTimer);
						}
					}, 1000);
				}
				
				//收藏实例
				$('#btnFav').click(function(){
					parent.__kbs_layer_index = parent.$.layer({
						type : 2,
						border : [10, 0.3, '#000'],
						title : false,
						closeBtn : [0, true],
						iframe : {
							src : $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?objid=' + objId + '&cataid=' + categoryId
						},
						area : ['500', '300']
					});
				});
				
				//纠错
				$('#btnErrcorrect').click(function(){
					var _ques = $('.kbs-title').text();
					var _answer = $('.kbs-title').next('div[kbs-answer="1"]').text();
					$.kbase.errcorrect({
						'objId': objId,
						'faqId': faqId,
						'cateId': categoryId,
						'question': _ques,
						'answer': _answer
					});
				});
				
				//
				$('#valueRelatedPanel li, #objectComparePanel li, #objectRelatedPanel li').hover(function(){
					$(this).addClass('list-group-item-info');
				}, function(){
					$(this).removeClass('list-group-item-info');
				});
				
				//知识关联 单击
				$('#valueRelatedPanel li').click(function(){
					$(this).removeClass('list-group-item-info');
					var _text = $(this).text();
					openValue(_text);
				});
				
				//实例对比
				$('#objectComparePanel li span').click(function(){
					$(this).parent('li').removeClass('list-group-item-info');
					var _text = $(this).text();
					var _id = $(this).attr('_objid');
					openObject(_text, _id);
				});
				
				//实例关联
				$('#objectRelatedPanel li').click(function(){
					$(this).removeClass('list-group-item-info');
					var _text = $(this).text().trim();
					var _id = $(this).attr('_objid');
					openObject(_text, _id);
				});
				
				//选中实例对比
				$('#btnCompareOnto').click(function(){
					var _ids = objId + ',';
					//
					$('#objectComparePanel input[type="checkbox"]:checked').each(function(i, item){
						_ids += $(item).attr('_objid') + ',';
					});
					
					_ids = _ids.substring(0, _ids.length-1);
					
					if (_ids.length==32){
						$(document).hint('至少要选择一个实例');
						return false;
					}
					
					parent.TABOBJECT.open({
						id : 'compareOnto',
						name : '实例对比',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids=' + _ids,
						isRefresh : true
					}, this);
				});
				//添加文章对比
				$('#btnAddOnto').click(function(){
					
				});
				
				/**
				 * 跳转到实例
				 */
				function openObject(text, id){
					parent.TABOBJECT.open({
						id : id,
						name : text,
						hasClose : true,
						url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=8&objId=' + id,
						isRefresh : true
					}, this);
				}
				/**
				 * 跳转到标注问
				 */
				function openValue(text){
					parent.TABOBJECT.open({
						id : _text,
						name : _text,
						hasClose : true,
						url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=5&askContent=' + _text,
						isRefresh : true
					}, this);
				}
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 kbs-avatar">
					<ol class="breadcrumb" id="navCate" style="height:50px;padding-top:12px;margin-bottom:3px;">
						<div class="col-md-offset-6 col-md-6 text-right" style="margin-top:-25px;">
							<div class="btn-group">
								<button class="btn btn-primary" id="btnFav">收藏</button>
								<button class="btn btn-primary" id="btnVersionCheck">版本对比</button>
								<button class="btn btn-primary" id="btnDocList">原文</button>
							</div>
		  				</div>
					</ol>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-9 kbs-avatar">
					<div class="row">
						<!-- 知识明细和相关操作 -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading text-right" style="padding: 2px 3px;">
									<div class="btn-group" role="group">
										<c:choose>
											<c:when test="${requestScope.flag_objectMarkread=='1'}">
												<button class="btn btn-info" disabled="disabled">已读</button>
											</c:when>
											<c:otherwise>
												<button class="btn btn-info" disabled="disabled" id="btnMarkread">签读 20</button>
											</c:otherwise>
										</c:choose>
										<button class="btn btn-info" id="btnErrcorrect">纠错</button>
										<button class="btn btn-info">推荐</button>
										<button class="btn btn-info">收藏</button>
									</div>
								</div>
								<div class="panel-body">
									<div class="kbs-title" kbs-question="1">${question}</div>
									<div kbs-answer="1">${answer}</div>
									<c:forEach items="${requestScope.p4Data}" var="item">
										<a href="javascript:void(0);">[${item.title}]</a><br/>
										<iframe _src="${item.url }" height="300" width="100%" frameborder="no" border="0"></iframe>
									</c:forEach>
									<c:forEach items="${requestScope.attachmentList}" var="item">
										<a href="${item.url }">${item.name }</a>
										<a href="javascript:void(0);" kbsconvert="1" kbsname="${item.name }" kbsid="${item.id }">附件预览</a>
									</c:forEach>
								</div>
							</div>
						</div>
						<!-- 知识列表 -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading text-right" style="padding: 2px 3px;">
									<div class="checkbox">
										<label><input type="checkbox"> 预览&nbsp;&nbsp;</label>
									</div>
								</div>
								<div class="panel-body">
									<c:forEach items="${requestScope.rqValue}" var="item">
										<c:if test="${item.nodeId!=requestScope.rbtResponse_faqId}">
											<div _id="kbs-fqa" style="margin-bottom:12px;border-bottom:1px dotted #CDCDCD;">
												<div class="row">
													<div class="col-md-11 kbs-title-helios" kbs-question="1">
														${item.question }
														<%
															RqValue rqValue = (RqValue) pageContext.findAttribute("item");
															Attachment attachment = rqValue.getAttachment();
															if (attachment!=null){
																Date now = new Date();
																Date timeExpire = attachment.getVtimeExpire();
																Date timeEnd = attachment.getVtimeEnd();
																if (timeExpire!=null && timeExpire.getTime()<now.getTime()){
																	out.print("<span class=\"label label-danger\">已过追溯期</span>");
																}else if (timeEnd!=null && timeEnd.getTime()<now.getTime()){
																	out.print("<span class=\"label label-danger\">未过追溯期</span>");
																}
															}
														%>
													</div>
													<div class="col-md-1 text-right kbs-detail">详情</div>
												</div>
												<div class="row" style="display:none">
													<div class="col-md-12" kbs-answer="1">${item.answer}</div>
												</div>
											</div>
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<ul class="nav nav-tabs" style="margin-bottom:10px;">
						<li role="presentation" class="active"><a href="javascript:void(0);">知识关联</a></li>
						<li role="presentation"><a href="javascript:void(0);">实例对比</a></li>
						<li role="presentation"><a href="javascript:void(0);">实例关联</a></li>
					</ul>
					<div class="nav-tabs-panel" id="valueRelatedPanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.rbtResponse_relatedQuestions}" var="item">
								<li class="list-group-item" style="cursor:pointer;">${item }</li>
							</c:forEach>
						</ul>
					</div>
					<div class="nav-tabs-panel" id="objectComparePanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.cmpareOnto}" var="item">
								<li class="list-group-item" style="cursor:pointer;">
									<input type="checkbox" _objid="${item.objectId }">&nbsp;<span _objid="${item.objectId }">${item.name }</span>
								</li>
							</c:forEach>
							<c:if test="${fn:length(requestScope.cmpareOnto)>0}">
								<li class="list-group-item text-center">
									<button class="btn btn-info" id="btnCompareOnto">选中对比</button>
									<button class="btn btn-info" id="btnAddOnto">添加文章</button>
								</li>
							</c:if>
						</ul>
					</div>
					<div class="nav-tabs-panel" id="objectRelatedPanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.relationOnto}" var="item">
								<li class="list-group-item" style="cursor:pointer;" _objid="${item.objectId }">
									${item.name }
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
