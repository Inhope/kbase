<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<head>
		<base href="<%=basePath%>">

		<title>上一版本</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//your code here ...
			});
		</script>
	</head>

	<body>
		<c:choose>
		<c:when test="${(question==null || question =='') && (answer==null || answer =='')}">
			<p style="margin-top:10px;margin-left:200px;font-weight: bolder;color:red;">没有历史版本</p>
		</c:when>
		<c:otherwise>
		<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%">
				<tr>
					<td width="30%">${question}</td>
					<td width="70%">${answer}</td>
				</tr>
		</table>
		</c:otherwise>
		</c:choose>
	</body>
</html>
