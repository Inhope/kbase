<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>分类-附件-版本</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$(function(){
				var _CateId = '${param.categoryId }', 
					_FileId = '${param.fileId }', 
					_ConverterPath = '${param.converterPath}';
					
				var that = window.CategoryAttachPreview = {}, fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				
				that.extend({
					/*下载*/
					download: function(){
						var node = $('#file-tree').tree('getSelected');
						if(node){
							var fileId = node.id, fileName = node.text;
							if(fileName.lastIndexOf('--') > 0)
								fileName = fileName.substring(0, fileName.lastIndexOf('--'));
							var elemIF = document.createElement('iframe');
				            elemIF.src = $.fn.getRootPath() + '/app/wukong/search!download.htm?fileId=' 
				            	+ fileId + '&fileName=' + fileName;
				            elemIF.style.display = 'none';
				            document.body.appendChild(elemIF);
						}
					},
					/*预览*/
					preview:function(){
						var node = $('#file-tree').tree('getSelected');
						if(node){
							var fileId = node.id, fileName = node.text, fileType;
							if(fileName.lastIndexOf('--') > 0)
								fileName = fileName.substring(0, fileName.lastIndexOf('--'));
							fileType = fileName.substring(fileName.lastIndexOf('.') + 1);
							/*
							fileName = fileName.substring(0, fileName.lastIndexOf('.'));
							var url = $.fn.getRootPath() + '/app/category/category-attach!preview.htm?fileId=' 
								+ fileId + '&fileName=' + fileName + '&fileType=' + fileType + '&categoryId=' + _CateId;
							*/
							var scrHeight = screen.height - 85, 
								scrWidth = screen.width - 20;
							var url = _ConverterPath + '/attachment/preview.do?fileName=' + fileId + '.' + fileType;
							var win = window.open(url , fileName, 
								'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
								+ scrWidth + ',height=' + scrHeight);
							win.focus();
						}
					},
					init: function(){
						if(_CateId && _FileId){
							_FileId = _FileId.substring(0, _FileId.length-1);
							/*附件版本树*/
							$('#file-tree').tree({
								url: $.fn.getRootPath() + '/app/biztpl/article!attamentLoad.htm?objectId=' + _CateId + '&node=' + _FileId,
								onBeforeExpand: function(node) {// 获取该节点下其他数据
				                     $('#file-tree').tree('options').url = $.fn.getRootPath()
				                     	+ '/app/biztpl/article!attamentLoad.htm?objectId=' + _CateId + '&node=' + node.id;
				                },
				                onContextMenu: function(e, node){
									e.preventDefault();
									if(node.leaf){
										// 查找节点
										$(this).tree('select', node.target);
										// 显示快捷菜单
										$('#mm').menu('show', {
											left: e.pageX,
											top: e.pageY
										});
									}
								}
							});
						}
					}
				});
				that.init();
			});
		</script>
	</head>
	<body class="easyui-layout">
		<!-- 版本 -->
	    <ul id="file-tree"></ul>
	    <!-- 版本操作 -->
		<div id="mm" class="easyui-menu" style="width:120px;">
			<div onclick="javascript:CategoryAttachPreview.download();" data-options="iconCls:'icon-add'">下载</div>
			<div onclick="javascript:CategoryAttachPreview.preview();" data-options="iconCls:'icon-remove'">预览</div>
		</div>
	</body>
</html>
