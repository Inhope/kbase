<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="net.sf.json.JSONObject"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>分类-附件</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/CategoryAttach.js"></script>
		<%
			String lastParams = request.getParameter("lastParams");
			JSONObject params = JSONObject.fromObject(lastParams);
			request.setAttribute("categoryId", params.getString("categoryId"));
		%>
		<script type="text/javascript">
			/*列表数据, 分类id*/
			var _CateId = '${categoryId}', 
				_AttType = 2;/*0-实例,1-答案,2-分类*/
			$(function(){
				/*contextPath重新定义*/
				if(typeof($.fn.getRootPath()) == 'undefined') {
					$.extend($.fn, {
						getRootPath : function(){ 
							return '${pageContext.request.contextPath }'; 
						}
					});
				}
				
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid'), /*datagrid对象*/
					getQueryParams: function(){ /*datagrid查询条件*/
						var rst = {
							filename: $('#filename').textbox('getText'),
							topNodeId: _AttType
						};
						return rst;
					}
				});
			});
		</script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'west',title:'已关联文件',split:true" style="width:220px;">
    		<ul id="file-tree"></ul>
    	</div>
		<div data-options="region:'center',title:'待关联文件',split:true,iconCls:'icon-help'">
			<div id="tt" class="easyui-tabs" data-options="fit:true">   
			    <div title="新的文件" data-options="fit:true">
			    	<%--新增文档 --%>
			    	<iframe id="iframepage" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" height="99%"
						src="${pageContext.request.contextPath}/app/category/category-attach!attAdd.htm?targetId=${categoryId }" 
						style="width: 100%"></iframe>
			    </div>   
			    <div title="相关文件">
			        <!--导航栏  -->
					<div id="now-toolbar" style="padding:5px;height:auto;">
						<div>
							相关文件：<input class="easyui-textbox" id="filename" style="width:150px;">&nbsp;&nbsp;
							<a href="javascript:void(0);" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
						</div>
					</div>
					<!-- 列表 -->
					<table id="now-grid" class="easyui-datagrid" data-options="toolbar: '#now-toolbar',
						url:'${pageContext.request.contextPath }/app/biztpl/article!attamentSearch.htm', method:'POST',
						border:true,rownumbers:true,nowrap:false,fit:true,fitColumns:true,singleSelect:true,
						loadFilter: function(data){
							if(data.success)
								return {'rows': data.data};
							else return {'rows': []};
						}">
						<thead>
							<tr>
								<th data-options="field:'name',width:140,align:'left'">名称</th>
								<th data-options="field:'aaa',width:60,align:'center',
									formatter: function(value, row, index){
										var html = '';
										var a = $(document.createElement('a'))[0];
										$(a).attr('href', 'javascript:void(0);');
										$(a).attr('onclick', 'javascript:CategoryAttach.relate(\'' + row.id + '\', \'' + row.name + '\', ' 
											+ '\'/app/custom/gzyd/info-data.htm?mouldId=' + row.id + '\')');
										$(a).text('引用');
										html = a.outerHTML
										
										/*
										html += '&nbsp;&nbsp;';
										
					            		a = $(document.createElement('a'))[0];
										$(a).attr('href', 'javascript:void(0);');
										$(a).attr('onclick', 'javascript:AttachList.attach.download(\'' + row.id + '\', \'' + row.name + '\', ' 
											+ '\'/app/custom/gzyd/info-data.htm?mouldId=' + row.id + '\')');
										$(a).text('下载');
										html = a.outerHTML
										
										html += '&nbsp;&nbsp;';
										
										a = $(document.createElement('a'))[0];
										$(a).attr('href', 'javascript:void(0);');
										$(a).attr('onclick', 'javascript:InfoMould.fn.searchData(\'' + row.id + '\', \'' + row.name + '\', ' 
											+ '\'/app/custom/gzyd/info-data.htm?mouldId=' + row.id + '\')');
										$(a).text('预览');
										html += a.outerHTML
										*/
										
					            		return html;
									}">操作</th>
							</tr>
						</thead>
					</table>
			    </div>   
			</div> 
		</div> 
		
		<!-- 模板编辑、新增  -->
		<div id="attach-dialog"></div>
		<!-- 解除关系 -->
		<div id="mm" class="easyui-menu" style="width:120px;">
			<div onclick="javascript:CategoryAttach.remove();" data-options="iconCls:'icon-remove'">解除关系</div>
			<div onclick="javascript:CategoryAttach.cateAttSync();" data-options="iconCls:'icon-reload'">通知同步</div>
		</div>
	</body>
</html>
