<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="com.eastrobot.module.category.vo.Attachment"%>
<%@page import="com.eastrobot.util.WukongUtil"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">


<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<%-- 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/menu.css"/>
	--%>
		
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<%-- 
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/plugins/jquery.parser.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/plugins/jquery.menu.js"></script>
--%>

<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css?2015123001" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/docicons/css/doc-icons.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	/*html{*overflow:auto;}*/
	.gztable02{width:100%;height:33px;overflow:hidden;background-color:	#4473c2;filter:alpha(Opacity=80);-moz-opacity:0.8;opacity: 0.8;}
	.gztable02 li{width:19%;height:33px;float:left;}
	.gztable02 li a{width:100%;height:33px;display:inline-block;font-size:12px;color:#fff;text-decoration:none;text-align:center;line-height:33px;}
	.gztable02 li a:hover,.gztable02 li a:active{color:#333;background-color:#6d91cf;}
	
	.yulan iframe {height: 300px; width: 100%;}
	
	.gztable01{
		*position: static;
	}
	.gztable01 .gzul02{
		*position: static;
		*float: left;
	} 
	
	.gztable01 .gzform01{
		*float: right;
		*margin-left: 10px;
	}
	.kbs-global-gztable-css{
		position:fixed;
		height:33px;
		right:10px;
		top:300px;
		width: 100%;
		border:1px double #C1CDCD;
		text-align:center;
		cursor:pointer;
		z-index:3333;
	}
	
	.gzcontent .gz-cn2{
		height: 380px;
		overflow: auto;
	}
	
	a.openAttachment , a.showAttachment{
		color: #A1A1A1;
	}
	.easyui-panel{
		border: 1px solid #95b8e7;
		padding: 6px 6px 8px 6px;
		*padding: 3px 3px 6px 3px;
	}
	.easyui-linkbutton{
		border: 1px solid #bbb;
		background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #ffffff 0px, #eeeeee 100%) repeat-x scroll 0 0;
		cursor: pointer;
		padding: 6px;
		*padding: 3px;
	}
</style>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/main.css"/>

<script type="text/javascript">
	//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
	window.__kbs_has_parent = !(window.parent==window);
</script>


<script type="text/javascript">
	window.__kbs_loader = [];
	var swfAddress = '${swfAddress}/attachment/preview.do';
	var human_id = '${job_id}';
	var faqId = '';
	var robot_context_path = '${robot_context_path}';
	var sessionUId = '${sessionUId}';
	var brand = '${sessionScope.brand}';
	var brandDesc = '${brandName}';
	var inner = '${inner}';
	var askLocation = '${sessionScope.location}';
	var askLocationDesc = '${locationName}';
	var askContent = '';
	var categoryId = '${categoryId}';
	var questionId = '${questionId}';
	//点击右键时当前标准问对象(右键发送短信用),Category.js右键事件赋值
	var gzyd_sms_checkbox_obj;
	
</script>
<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
</head>


<body>
    <!--******************操作***********************-->
	<!-- 
	<div class="kbs-global-gztable-css">
		<ul class="gztable02">
			<li><a href="javascript:void(0);" name="gzyd_guidang">归档</a></li>
			<li class="message_send"><a href="javascript:void(0);" name="gzyd_duanxin">发短信</a></li>
			<li><a href="javascript:void(0);">历史版本</a></li>
			<li><a href="javascript:void(0);">知识对比</a></li>
			<li><a href="javascript:void(0);" id="btnFullScreen">全屏显示</a></li>
		</ul>
	</div>
	 -->
	<div id="findbar" class="easyui-panel" title="" style="position:fixed;width:570px;top:32px;right:10px;background-color: #fff;z-index: 99999;">
		<a href="javascript:void(0)" class="easyui-linkbutton" id="btnGoup">回到顶部</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:GzydMappings.filing.guidang();">归档</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:GzydMappings.sms.send(false);">发短信</a>
        <!-- 
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="disabled:true">历史版本</a> -->
        <!-- 
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="disabled:true">知识对比</a> -->
        <a href="javascript:void(0)" class="easyui-linkbutton" id="btnFullScreen">全屏显示</a>
        <a id = "showAllP4" class="easyui-linkbutton" href="javascript:void(0);">一键展开</a>
		<input class="easyui-textbox kbs-global-finder-keyword" type="text" style="width:100px">
		<a href="javascript:void(0)" class="easyui-linkbutton kbs-global-finder-btn" data-options="disabled:false">上一个</a>
		<a href="javascript:void(0)" class="easyui-linkbutton kbs-global-finder-btn" data-options="disabled:false">下一个</a>
		<span class="kbs-global-finder-total"></span>
	</div>
	<!--******************内容开始***********************-->
	<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
	<div class="gzcontent">
		<div class="gz-cn1">
			<!-- <div class="bfb gztitle1">
				<b>业务名称：</b>4G上网套餐、商旅套餐
			</div>
			<div class="bfb gztitle1">
				<font><b>知识分类：</b>语音->语音套餐</font><font><b>更新时间：</b>2014-11-16</font><font><b>点击量：</b>2223</font><font><b>适用品牌：</b>XXXX</font><font><b>使用地市：</b>上海</font>
			</div>
			 -->
			<%-- modify by eko.zhan at 2016-02-22 18:14 公告中访问分类展示页面不通过归属地和品牌过滤 --%>
			<c:if test="${param.key!='BULLETIN'}">
				<div class="bfb gztitle1" style="height: auto;">
					<font><b>知识分类：</b><span id="categoryNv"></span></font>
				</div>
				<div class="gztitle1" style="float: left;">
					<font style="color: red;padding-right: 10px;"><b><s:property value="#request.locationName"/></b></font>
					<font style="color: red;padding-right: 10px;"><b><s:property value="#request.brandName"/></b></font>
				</div>
			</c:if>
			<div class="gztitle1" style="float: left;">
			 更新时间：${editTime}<s:if test ='#request.endtime1_c!=null && #request.endtime1_c!=0'>&nbsp;<font color="red">(已过期)</font></s:if><span style="display:none">${endtime1_c}</span>
			</div>
		</div>
		<p class="clear"></p>

		<ul class="gzul">
			
			<s:iterator value="#request.tagNameList" var="tagName" status="status1">
				<s:if test="#status1.getIndex()==0">
					<li class="active"><a href="javascript:void(0);"><s:property value="#tagName"/></a></li>
				</s:if>
				<s:else>
					<li><a href="javascript:void(0);"><s:property value="#tagName"/></a></li>
				</s:else>
			</s:iterator>
			<%-- 
			<s:iterator value="#request.list" id="all">
				<s:iterator value="#all" id="tag_1"  status="status1">
					<s:if test="#status1.getIndex()==0">
						${status1.index }/
						<s:iterator value="#tag_1" id="tag_2" status="status2">
							&&&${defaultActiveTag }&&&
							${status2.index }
							
							<s:if test="#status2.getIndex()==0">
								<li><a href="javascript:void(0);"><s:property value = "#tag_2.tag1"/></a></li>
							</s:if>
				    	  </s:iterator>
				    </s:if>
			    </s:iterator>
			</s:iterator>--%>
			
			<%-- 短信  先屏蔽掉 --%>
			<s:if test="#request.list.size()>0">
			<li style="display:none"><a href="javascript:void(0);">
					短信
				</a>
			</li>
			</s:if>
			
			<s:if test="#request.list.size()>0">
			<li><a href="javascript:void(0);">
					显示所有
				</a>
			</li>
			</s:if>
			
			<p class="clear"></p>
		</ul>
		
		<s:set var="b_li1" value="0"></s:set>
		<s:iterator value="#request.list" id="all" status="status">
		<s:set var="b_li1" value="#b_li1+1"></s:set>
			<s:set var="b_li2" value="0"></s:set>
			<s:iterator value="#all" id="tag_1" status="status1">
			<s:set var="b_li2" value="#b_li2+1"></s:set>
			<%-- modify by eko.zhan at 2016-04-22 13:30 
				加载时默认隐藏其他的tag
			 --%>
			<s:if test="#b_li1==1 and #b_li2==1">
				<div class="gz-cn2" id="${b_li1 }${b_li2 }" tag1="<s:property value = '#status.getIndex()'/>" tag2="<s:property value = '#status1.getIndex()'/>">
			</s:if>
			<s:else>
				<div class="gz-cn2" style="display:none;" id="${b_li1 }${b_li2 }" tag1="<s:property value = '#status.getIndex()'/>" tag2="<s:property value = '#status1.getIndex()'/>">
			</s:else>
			
				<%-- 
				hidden by eko.zhan at 2015-12-30 11:45 广东移动江门提出去掉分类导航路径
				<h2>
				<s:iterator value="#all" id="tag_1_1" status="status1_1">
				<s:if test="#status1_1.getIndex()==#status1.getIndex()">
				<s:iterator value="#tag_1_1" id="tag_2_1" status="status2_1">
					<s:if test="#status2_1.getIndex()==0">
							<s:property value = "#tag_2_1.tag1"/> -> <s:property value = "#tag_2_1.tag2"/>  
					</s:if>
				</s:iterator>
				</s:if>
				</s:iterator>
				
				</h2>
				--%>
				
				<div class="gz-cn3">
					<div class="gztable01">
						<ul class="gzul02">
							<s:iterator value="#all" id="tag_1_2" status="status1_2">
							<s:iterator value="#tag_1_2" id="tag_2_2" status="status2_2">
								<s:if test="#status2_2.getIndex()==0">
									<s:if test="#status1_2.getIndex()==0">
										<li tag2="<s:property value = '#status1_2.getIndex()'/>" class="active">
									</s:if>
									<s:else>
										<li tag2="<s:property value = '#status1_2.getIndex()'/>">
									</s:else>
										<a href="javascript:void(0);">
											<s:property value = "#tag_2_2.tag2"/>
										</a>
									</li>
									
								</s:if>
							</s:iterator>
							</s:iterator>
						</ul>
	
						<div class="gzform01">
	
							<table width="100%" cellspacing="1" cellpadding="8">
								<tbody>
								<s:iterator value="#tag_1" id="tag_2">
									<tr>
										<td style="display:none;"><s:property value = '#tag_2.id'/></td>
										<td width="15%" height="0" align="left" valign="middle" class="question">
											<span class="kbs-question" id="<s:property value="#tag_2.id"/>"><s:property value="#tag_2.question"/></span>&nbsp;
											<s:if test="#tag_2.isNew"><img src="${pageContext.request.contextPath }/theme/red/resource/category/images/new.gif"></img></s:if>
											<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2.edittime"/></span>
											<%-- modify by eko.zhan at 2016-02-22 18:14 公告中访问分类展示页面不通过归属地和品牌过滤，知识点显示归属地和品牌 --%>
											<c:if test="${param.key=='BULLETIN'}">
												<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2.locationDesc"/> <s:property value="#tag_2.brandDesc"/></span>
											</c:if>
										</td>
										<td width="75%" height="0" align="left" valign="middle" class="answer" _oid="<s:property value="#tag_2.oid"/>">
											<div>
												<s:property value = "#tag_2.answer" escape="false"/>
											</div>
											<div class="yulan">
											<%-- 
											<div><ol>
												<s:iterator value="#tag_2.p4List" var="p4">
													<li>[${p4.name}]&nbsp;<a href="${p4.url}" title="打开 ${p4.name}" target="_blank" >打开p4</a> 
													<a class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览p4</a> 
												</s:iterator>
											</ol></div>
											 --%>
											<div>
												<s:iterator value="#tag_2.p4List" var="p4" status="p4Ind">
													&nbsp;&nbsp;${p4Ind.index+1 }.&nbsp;[${p4.name}]&nbsp;<a href="${p4.url}?time=<%=System.currentTimeMillis() %>" title="打开 ${p4.name}" target="_blank">打开详情</a>
													<a id = "categoryP4" class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览详情</a><br/>
												</s:iterator>
											</div>
											<div>
												<s:iterator value="#tag_2.attachmentList" var="attachment">
													<%
														Attachment att = (Attachment)pageContext.findAttribute("attachment");
														String filename = FilenameUtils.getBaseName(att.getName());
													%>
													<div class="kbs-office <%=WukongUtil.getIcon(att.getName()) %>"></div>
													<a href="${attachment.url}" title="下载 ${attachment.name}"><%=filename %></a>
													<a class="openAttachment" title="打开 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">打开附件</a>
													| <a class="showAttachment" title="预览 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">预览附件</a><br/>
												</s:iterator>
											</div>
											<div></div>
											<div>
												<s:if test="#tag_2.zdyw!=null && #tag_2.zdyw.size()>0">
													<ul class="zdyw">
													<s:iterator value="#tag_2.zdyw" var="_zdyw">
														<li>[${_zdyw}] 
															<span q="${_zdyw}">
																<a href="javascript:void(0);" style="font-size:12px;color:#666666;">相关答案</a> | <a href="javascript:void(0);" style="font-size:12px;color:#666666;">页面展示</a>
															</span>
														</li>
													</s:iterator>
													</ul>
												</s:if>
											</div>
											</div>
										</td>
										<td width="10%" align="center" valign="middle">
											<a class="other_platform" valueId="<s:property value = '#tag_2.id'/>" href="javascript:void(0);">其他平台</a><br/>
											<s:if test='#tag_2.isableSMS==true'>
												<input type="checkbox" class="check" 
													name="gzyd_sms_checkbox_${b_li1 }${b_li2 }" 
													gzyd_sms_question='<s:property value="#tag_2.question" />' 
													gzyd_sms_answer='<s:property value="#tag_2.answer" />'
													gzyd_sms_isableSMS="${tag_2.isableSMS }"
													valueId="<s:property value = "#tag_2.Id"/>"/>
												<!-- <a href="javascript:void(0);" name="gzyd_sms_add_duanxin"
													gzyd_sms_question='<s:property value="#tag_2.question" />' 
													gzyd_sms_answer='<s:property value="#tag_2.answer" />' 
													gzyd_sms_isableSMS="${tag_2.isableSMS }" 
													valueId='<s:property value = "#tag_2.Id"/>' >添加短信</a> -->
											</s:if>
										</td>
									</tr>
								</s:iterator>
								</tbody>
							</table>
	
						</div>
	
					</div>
					<!-- 
					<ul class="gztable02">
						<li><a href="javascript:void(0);" name="gzyd_guidang">归档</a>
						</li>
						<li class="message_send"><a href="javascript:void(0);" name="gzyd_duanxin">发短信</a>
						</li>
						<li><a href="javascript:void(0);">答案反馈</a>
						</li> 
						<li><a href="javascript:void(0);">历史版本</a>
						</li>
						<li><a href="javascript:void(0);">知识对比</a>
						</li>
					</ul> -->
				</div>
				<p class="clear"></p>
			
			</div>
			</s:iterator>
			</s:iterator>
			
			<!-- 短信 -->
			<div class="gz-cn2" id="sms" style="display:none;">
				<div class="gztable01">
					<s:iterator value="#request.list" id="alls" status="status">
						<div class="gzform01" style="margin-left:5px;margin-top:5px;">
							<s:iterator value="#alls" id="tag_1s" status="status1">
								<s:iterator value="#tag_1s" id="tag_2s_all" status="status2_all">
									<s:if test="#status2_all.getIndex()==0">
										<s:property value="#tag_2s_all.tag1" /> -> <s:property
											value="#tag_2s_all.tag2" />
									</s:if>
								</s:iterator>
								<table width="100%" border="0" cellspacing="1" cellpadding="8" bgcolor="#c2c2c2">
									<tbody>
										<s:iterator value="#tag_1s" id="tag_2s">
											<s:if test="#tag_2s.isableSMS">
												<tr gzyd_sms_isableSMS="${tag_2s.isableSMS }">
													<td style="display:none;"><s:property value = '#tag_2s.id'/></td>
													<td width="15%" height="0" align="left" valign="middle" class="question">
														<span id="<s:property value="#tag_2s.id" />" class="kbs-question"><s:property value="#tag_2s.question" />&nbsp;</span>
														<s:if test="#tag_2s.isNew"><img src="${pageContext.request.contextPath }/theme/red/resource/category/images/new.gif"></img></s:if>
														<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2s.edittime"/></span>
														<%-- modify by eko.zhan at 2016-02-22 18:14 公告中访问分类展示页面不通过归属地和品牌过滤，知识点显示归属地和品牌 --%>
														<c:if test="${param.key=='BULLETIN'}">
															<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2s.locationDesc"/> <s:property value="#tag_2s.brandDesc"/></span>
														</c:if>
													</td>
													<td width="75%" height="0" align="left" valign="middle" class="answer" _oid="<s:property value="#tag_2s.oid"/>">
														<div>
															<s:property value="#tag_2s.answer" escape="false"/>
														</div>
														<div class="yulan">
															<%-- 
															<div><ol>
																<s:iterator value="#tag_2s.p4List" var="p4">
																	<li>[${p4.name}]&nbsp;<a href="${p4.url}" title="打开 ${p4.name}" target="_blank">打开p4</a>
																	<a class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览p4</a>
																</s:iterator>
															</ol></div>
															 --%>
															<div>
																<s:iterator value="#tag_2s.p4List" var="p4" status="p4Ind">
																	&nbsp;&nbsp;${p4Ind.index+1 }.&nbsp;[${p4.name}]&nbsp;<a href="${p4.url}?time=<%=System.currentTimeMillis() %>" title="打开 ${p4.name}" target="_blank">打开详情</a>
																	<a id = "categoryP4" class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览详情</a><br/>
																</s:iterator>
															</div>
															<div>
																<s:iterator value="#tag_2s.attachmentList" var="attachment">
																	<%
																		Attachment att = (Attachment)pageContext.findAttribute("attachment");
																		String filename = FilenameUtils.getBaseName(att.getName());
																	%>
																	<div class="kbs-office <%=WukongUtil.getIcon(att.getName()) %>"></div>
																	<a href="${attachment.url}" title="下载 ${attachment.name}"><%=filename %></a>
																	<a class="openAttachment" title="打开 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">打开附件</a>
																	| <a class="showAttachment" title="预览 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">预览附件</a><br/>
																</s:iterator>
															</div>
															<div></div>
														</div>
														<div>
															<s:if test="#tag_2s.zdyw!=null && #tag_2s.zdyw.size()>0">
																<ul class="zdyw">
																<s:iterator value="#tag_2s.zdyw" var="_zdyw">
																	<li>[${_zdyw}] 
																		<span q="${_zdyw}">
																			<a href="javascript:void(0);" style="font-size:12px;color:#666666;">相关答案</a> | <a href="javascript:void(0);" style="font-size:12px;color:#666666;">页面展示</a>
																		</span>
																	</li>
																</s:iterator>
																</ul>
															</s:if>
														</div>
													</td>
													<td width="10%" height="0" align="center" valign="middle">
														<a class="other_platform" valueId="<s:property value = '#tag_2s.id'/>" href="javascript:void(0);">其他平台</a><br>
														<s:if test='#tag_2s.isableSMS==true'>
															 <input type="checkbox" class="check" 
															name="gzyd_sms_checkbox_sms" 
															gzyd_sms_question='<s:property value="#tag_2s.question" />' 
															gzyd_sms_answer='<s:property value="#tag_2s.answer" />' 
															gzyd_sms_isableSMS="${tag_2s.isableSMS }" 
															valueId='<s:property value = "#tag_2s.Id"/>' /> 
															<%--<a href="javascript:void(0);" name="gzyd_sms_add_duanxin"
															gzyd_sms_question='<s:property value="#tag_2s.question" />' 
															gzyd_sms_answer='<s:property value="#tag_2s.answer" />' 
															gzyd_sms_isableSMS="${tag_2s.isableSMS }" 
															valueId='<s:property value = "#tag_2s.Id"/>' >添加短信</a> --%>
														</s:if>
													</td>
												</tr>
											</s:if>
										</s:iterator>
									</tbody>
								</table>
							</s:iterator>
						</div>
					</s:iterator>
				</div>
			</div>
			<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
			<div class="gz-cn2" id="all" style="display:none;">
			
				<div class="gztable01">
					<s:iterator value="#request.list" id="all" status="status">
						<div class="gzform01" style="margin-left:5px;margin-top:5px;">
							<s:iterator value="#all" id="tag_1" status="status1">
								<s:iterator value="#tag_1" id="tag_2_all" status="status2_all">
									<s:if test="#status2_all.getIndex()==0">
										<s:property value="#tag_2_all.tag1" /> -> <s:property
											value="#tag_2_all.tag2" />
									</s:if>
								</s:iterator>
								<!-- 屏蔽不能发短信的标准问 -->
								<s:if test="#status.first && #status1.first">
									<span style="float: right; margin-right: 10px;">
										短信显示 <input type="checkbox" onclick="javascript:GzydMappings.sms.filter(this);" /></span>
								</s:if>
								<table width="100%" border="0" cellspacing="1" cellpadding="8" bgcolor="#c2c2c2">
									<tbody>
										<s:iterator value="#tag_1" id="tag_2">
											<tr gzyd_sms_isableSMS="${tag_2.isableSMS }">
												<td style="display:none;"><s:property value = '#tag_2.id'/></td>
												<td width="15%" height="0" align="left" valign="middle" class="question">
													<span id="<s:property value="#tag_2.id" />" class="kbs-question"><s:property value="#tag_2.question" />&nbsp;</span>
													<s:if test="#tag_2.isNew"><img src="${pageContext.request.contextPath }/theme/red/resource/category/images/new.gif"></img></s:if>
													<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2.edittime"/></span>
													<%-- modify by eko.zhan at 2016-02-22 18:14 公告中访问分类展示页面不通过归属地和品牌过滤，知识点显示归属地和品牌 --%>
													<c:if test="${param.key=='BULLETIN'}">
														<br/><span style="font-size:10px;color:#939393;"><s:property value="#tag_2.locationDesc"/> <s:property value="#tag_2.brandDesc"/></span>
													</c:if>
												</td>
												<td width="75%" height="0" align="left" valign="middle" class="answer" _oid="<s:property value="#tag_2.oid"/>">
													<div>
														<s:property value="#tag_2.answer" escape="false"/>
													</div>
													<div class="yulan">
														<%-- 
														<div><ol>
															<s:iterator value="#tag_2.p4List" var="p4">
																<li>[${p4.name}]&nbsp;<a href="${p4.url}" title="打开 ${p4.name}" target="_blank">打开p4</a>
																<a class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览p4</a>
															</s:iterator>
														</ol></div>
														 --%>
														<div>
															<s:iterator value="#tag_2.p4List" var="p4" status="p4Ind">
																&nbsp;&nbsp;${p4Ind.index+1 }.&nbsp;[${p4.name}]&nbsp;<a href="${p4.url}?time=<%=System.currentTimeMillis() %>" title="打开 ${p4.name}" target="_blank">打开详情</a>
																<a id = "categoryP4" class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览详情</a><br/>
															</s:iterator>
														</div>
														<div>
															<s:iterator value="#tag_2.attachmentList" var="attachment">
																<%
																	Attachment att = (Attachment)pageContext.findAttribute("attachment");
																	String filename = FilenameUtils.getBaseName(att.getName());
																%>
																<div class="kbs-office <%=WukongUtil.getIcon(att.getName()) %>"></div>
																<a href="${attachment.url}" title="下载 ${attachment.name}"><%=filename %></a>
																<a class="openAttachment" title="打开 ${attachment.name}"
																	href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">打开附件</a>
																| <a class="showAttachment" title="预览 ${attachment.name}"
																	href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">预览附件</a><br/>
															</s:iterator>
														</div>
														<div></div>
													</div>
													<div>
														<s:if test="#tag_2.zdyw!=null && #tag_2.zdyw.size()>0">
															<ul class="zdyw">
															<s:iterator value="#tag_2.zdyw" var="_zdyw">
																<li>[${_zdyw}] 
																	<span q="${_zdyw}">
																		<a href="javascript:void(0);" style="font-size:12px;color:#666666;">相关答案</a> | <a href="javascript:void(0);" style="font-size:12px;color:#666666;">页面展示</a>
																	</span>
																</li>
															</s:iterator>
															</ul>
														</s:if>
													</div>
												</td>
												<td width="10%" height="0" align="center" valign="middle">
													<a class="other_platform" valueId="<s:property value = '#tag_2.id'/>" href="javascript:void(0);">其他平台</a><br>
													<s:if test='#tag_2.isableSMS==true'>
														 <input type="checkbox" class="check" 
														name="gzyd_sms_checkbox" 
														gzyd_sms_question='<s:property value="#tag_2.question" />' 
														gzyd_sms_answer='<s:property value="#tag_2.answer" />' 
														gzyd_sms_isableSMS="${tag_2.isableSMS }" 
														valueId='<s:property value = "#tag_2.Id"/>' /> 
														<%--<a href="javascript:void(0);" name="gzyd_sms_add_duanxin"
														gzyd_sms_question='<s:property value="#tag_2.question" />' 
														gzyd_sms_answer='<s:property value="#tag_2.answer" />' 
														gzyd_sms_isableSMS="${tag_2.isableSMS }" 
														valueId='<s:property value = "#tag_2.Id"/>' >添加短信</a> --%>
													</s:if>
												</td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</s:iterator>
						</div>
					</s:iterator>
				</div>
				<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
				
			</div>
	</div>

	<!--******************内容结束***********************-->
	<!--******************弹框开始***********************-->
<div class="increase_d">
<div class="increase_dan">
<div class="increase_dan_title"><b>其他平台</b><a href="#"><img src="${pageContext.request.contextPath}/theme/red/resource/main/images/xinxi_ico1.jpg"></a></div>
<div class="increase_dan_con">
<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#FFF" class="biaoge">
	<tbody>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长00M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
  	</tbody>
</table>
</div>
</div>
</div>
<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
<!--******************弹框结束***********************-->
<!-- 右键菜单start -->
<div id="menu" class="index_dankuang">
	<ul>
		<li><a href="javascript:void(0);">答案反馈</a></li>
		<li><a href="javascript:void(0);" id="navFav">收藏</a></li>
		<li><a href="javascript:void(0);" id="navFavSms">短信收藏</a></li>
		<c:if test="${cateTag=='CATETAG'}">
			<li><a href="javascript:void(0);" id="navFavCatetag">业务收藏</a></li>
		</c:if>
		<c:if test="${kbValue=='KBVALUE'}">
			<li><a href="javascript:void(0);" id="navFavKbval">知识点收藏</a></li>
		</c:if>
		<li><a href="javascript:void(0);" id="navRecommend">推荐</a></li>
		<%--
		<li><a href="javascript:void(0);" id="version">上一版本</a></li>
		 --%>
		<li><a href="javascript:void(0);" id="objVersion">版本对比</a></li>
		<!-- 
		<li><a href="javascript:void(0);" id="navWeixinCode">生成微信码</a></li> --> <!-- 该功能暂不开放 add by eko.zhan at 2016-02-19 14:30 -->
	</ul>
</div>
<%-- 
<div id="mm" class="easyui-menu" style="width:120px;">
	<div id="jNavErrCorrect">答案反馈</div>
	<div>
		<span>收藏</span>
		<div style="width:150px;">
			<div id="jNavFavDefault">收藏至文件夹</div>
			<div id="jNavFavSms">短信收藏</div>
			<!-- 
			<div id="jNavFavCatetag">业务收藏</div>
			<div id="jNavFavKbval">知识收藏</div>
			-->
		</div>
	</div>
	<div id="jNavRecommend">推荐</div>
	<div id="jNavCopy">复制</div>
	<div id="jNavSendSms">发送短信</div>
	<div id="jNavVersion">上一版本</div>
</div>
--%>
<!-- 右键菜单end -->
<script type="text/javascript">window.__kbs_loader.push((new Date()).getTime());</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/Category.js?201610311145"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/findme.js?2015102902"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>

<script type="text/javascript">
	//分类路径加载 modify by eko.zhan at 2015-10-08
	navigate_init('${robot_context_path}','${categoryId}');
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/GzydMappings.js?2016050416"></script> 
<script type="text/javascript">
	//alert(window.__kbs_loader.join(', \r\n'));
</script>
<script type="text/javascript">
   /*
   $(function(){
        $('a[name="gzyd_sms_add_duanxin"]').click(function(){
              var _question = $(this).attr('gzyd_sms_question');
              var _answer = $(this).attr('gzyd_sms_answer');
              if(typeof(GzydMappings.sms.add)=='function'){
                 GzydMappings.sms.add(_question, _answer);
              }
        });
   });*/
</script>
</body>
</html>
