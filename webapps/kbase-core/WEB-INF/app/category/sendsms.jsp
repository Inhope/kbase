<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>发送短信</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		
		<script type="text/javascript">
			$(function(){
				var args = window.dialogArguments;
				var qaArr = [];
				$(args).each(function(i, item){
					$('table:last').append('<tr><td>' + (i+1) + '</td><td>' + item.question + '</td><td>' + item.answer + '</td></tr>');
					qaArr.push({
						q: item.question,
						a: item.answer
					});
				});
				
				//发送短信
				$('#btnSend').click(function(){
					var serialno = $('#serialno').val();
					var recevier = $('#recevier').val();
					
					if (serialno.length==0){
						alert('接触流水号不能为空');
						return false;
					}
					if (receiver.length==''){
						alert('接收短信号码不能为空');
						return false;
					}
					
					var yes = window.confirm('确定发送短信吗');
					if (yes){
						var data = {
							CONTACTID: serialno,
							SENDNUMBER: '10086',
							SERIALNO: serialno,
							SERVICEFULLNAME: 'SERVICEFULLNAME',
							SERVICETYPEID: 'SERVICETYPEID',
							SUBSNUMBER: recevier,
							qa: JSON.stringify(qaArr)
						}
						$.post($.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!duanxin.htm', data, function(json){
							if (json==null){
								alert('发送异常');
							}else if (json.result==-1){
								alert('发送失败');
							}else{
								alert('发送成功');
							}
							window.close();
						}, 'json');
					}
				});
			});
		</script>
	</head>

	<body>
		<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%">
			<tr>
				<td>接触流水号</td>
				<td colspan="3"><input type="text" style="width:72%" id="serialno"></td>
			</tr>
			<tr>
				<td width="15%">接收短信号码</td>
				<td width="35%"><input type="text" style="width:72%" id="receiver"></td>
				<td width="15%">发送短信号码</td>
				<td width="35%">10086</td>
			</tr>
			<tr>
				<td colspan="4" align="right"><button class="button button-tiny" id="btnSend">发送</button></td>
			</tr>
		</table>
		<br>
		<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%">
			<tr>
				<th width="10%">序号</th>
				<th width="30%">标题</th>
				<th width="60%">内容</th>
			</tr>
		</table>
	</body>
</html>
