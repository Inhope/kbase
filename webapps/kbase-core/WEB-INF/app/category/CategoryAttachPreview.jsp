<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>分类-附件-预览</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/auther/css/tab.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
			.box tr{line-height: 24px;}
			.box th {
				vertical-align:top;
				border-right: none;
				border-bottom: none;
			}
			
			.box td {
				border-right: none;
				border-bottom: none;
				color: #333333;
			}
			
			.buttons {text-align: left;margin-top: 15px;color: #333333;}
			.buttons div {width: 80px;float: left;font-size: 14px; }	
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/CategoryAttachPreview.js"></script>
		<script type="text/javascript">
			$(function(){
				CategoryAttachPreview.set({
					fileId: '${param.fileId }',
					fileName: '${param.fileName }',
					fileType: '${param.fileType }',
					categoryId: '${param.categoryId }',
					converterPath: '${requestScope.converterPath}'
				});
			});
		</script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'north'" style="height:65px;">
			<div>
				<s:set var="type" value="#parameters.fileType[0]"></s:set>
				<div style="height: 40px;top: 10px;margin-left: 5px;font-size: 24px;">
					<s:if test="#type == 'docx' || #type == 'doc'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/Word.png"
								width="16" height="15" />
					</s:if>
					<s:elseif test="#type == 'xlsx' || #type == 'xls'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/Excel.png"
								width="16" height="15" />
					</s:elseif>
					<s:elseif test="#type == 'pptx' || #type == 'ppt'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/PowerPoint.png"
								width="16" height="15" />
					</s:elseif>
					<s:elseif test="#type == 'png' || #type == 'bmp' 
						|| #type == 'gif' || #type == 'jpeg'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/picture.png"
								width="16" height="15" />
					</s:elseif>
					<s:elseif test="#type == 'pdf'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/pdf.png"
								width="16" height="15" />
					</s:elseif>
					<s:elseif test="#type == 'html' || #type == 'htm'">
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/p4page.png"
								width="16" height="15" />
					</s:elseif>
					<s:else>
						<img src="${pageContext.request.contextPath}/resource/wukong/imgs/txt.png"
								width="16" height="15" />
					</s:else>
					${param.fileName }
				</div>
				<div style="font-size: 14px;margin-left: 5px;">
					上传人：admin | 上传时间：2016-06-03 | 阅读量：1次 | 下载量：0次 | 评论次数：2次
				</div>
			</div>
		</div>   
	    <div data-options="region:'center'" style="padding:5px;background:#eee;">
	    	<!--附件信息  -->
	    	<iframe frameborder="0" scrolling="auto" marginheight="0" marginwidth="0" height="99%" style="width: 100%" 
				src="${requestScope.converterPath}/attachment/preview.do?fileName=${param.fileId }.${param.fileType }"></iframe>
	    </div>
	    <%--
	    <div data-options="region:'east',iconCls:'icon-reload',title:'文档信息'" style="width:250px;">
	    	<table class="box" cellspacing="0" style="width: 100%; height: auto;">
	    		<tr>
	    			<th style="width: 35%">标题：</th>
	    			<td style="width: 65%">信用卡文档</td>
	    		</tr>
	    		<tr>
	    			<th>发布日期：</th>
	    			<td>2016-07-29</td>
	    		</tr>
	    		<tr>
	    			<th>生效日志：</th>
	    			<td>2016-07-29</td>
	    		</tr>
	    		<tr>
	    			<th>发布单位：</th>
	    			<td>
	    				全国人大<br>
	    				国务院<br>
	    				中国证监会<br>
	    				高法高检<br>
	    				司法解释
	    			</td>
	    		</tr>
	    		<tr>
	    			<th>试用范围：</th>
	    			<td>
	    				深市中小板<br>
						适用所有<br>
						深市主板公司<br>
						深市所有
	    			</td>
	    		</tr>
	    		<tr>
	    			<th>时效性：</th>
	    			<td>现行有效</td>
	    		</tr>
	    	</table>
	    </div>
	    --%>
	    <div data-options="region:'south'" style="height:50px;">
	    	<div class="buttons" style="">
	    		<div style="width: 50px;float: left;">&nbsp;&nbsp;&nbsp;</div>
	    		<%--<div><a href="javascript:void(0);" id="btn-max">最大化</a></div>  --%>
	    		<%--<div><a href="javascript:void(0);" id="btn-recommend">推荐</a></div>  --%>
	    		<div><a href="javascript:void(0);" id="btn-download">下载</a></div>
	    		<%--<div><a href="javascript:void(0);" id="btn-favClip">收藏</a></div>  --%>
	    		<%--<div><a href="javascript:void(0);" id="btn-comment">评论</a></div>  --%>
	    		<div><a href="javascript:void(0);" id="btn-version">版本</a></div>
	    	</div>
	    </div>
	    
	    <%-- 弹窗 --%>
	    <div id="dd_easyui"></div>
	</body>
</html>
