<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>分类-附件-添加</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/dhtmlxVault_v25_std/codebase/fonts/font_roboto/roboto.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/dhtmlxVault_v25_std/skins/terrace/dhtmlxvault.css"/>
		<style>
			html,body{margin:0px;height: 100%;}
			div#vaultObj {
				position: relative;
				width: 100%;
				height: 100%;
				box-shadow: 0 1px 3px rgba(0,0,0,0.05), 0 1px 3px rgba(0,0,0,0.09);
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script src="${pageContext.request.contextPath}/library/dhtmlxVault_v25_std/codebase/dhtmlxvault.js"></script>
		<script src="${pageContext.request.contextPath}/library/dhtmlxVault_v25_std/codebase/swfobject.js"></script>
		<script>
			$(function(){
				dhtmlXVaultObject.prototype.strings = {
					done: '完成', 
					error: '错误', 
					size_exceeded:'文件大小超过 (最大 #size#)', 
					btnAdd: '添加', 
					btnUpload: '新增',
					btnClean: '清空',
					btnCancel: '取消'
				};
				/*创建附件上传对象*/
				var targetId = '${param.targetId}';
				if(targetId){
					var uploadUrl = '${pageContext.request.contextPath}/app/category/category-attach!upload.htm?targetId=' + targetId;
					var myVault = new dhtmlXVaultObject({
						parent: 'vaultObj',
						uploadUrl: uploadUrl,
						swfPath: 'dhxvault.swf',
						swfUrl: uploadUrl,
						slXap: 'dhxvault.xap',
						slUrl: uploadUrl,
						maxFileSize: 2097152,
						autoStart: false, /*是否自动上传*/
						buttonUpload:true /*是否显示上传按钮*/
					});
					/*父页面操作对象*/
					var tree = parent.CategoryAttach.fn.tree;
					var upFiles = new Array();
					/*未找到传递额外参数的地方*/
					myVault.attachEvent('onUploadFile', function(file, extra){
						if(file && tree && tree.getIdByName) {
							file.fileId = tree.getIdByName(file.name);
							upFiles.push(file);
						}
					});
					/*上传完成*/
					myVault.attachEvent('onUploadComplete', function(files){
						$.post('${pageContext.request.contextPath}/app/category/category-attach!add.htm',
							{upFiles: JSON.stringify(upFiles), targetId: targetId},
							function(jsonResult){
								try {
									/*响应操作结果*/
									if(jsonResult.rst) {
										if(tree && tree.refresh) {
											var fileNames = new Array();
											$(upFiles).each(function(){
												fileNames.push(this.name);
											});
											tree.refresh(fileNames);
										}
									}
								} catch (e){
									alert(e);
								} finally {
									/*清空缓存*/
									upFiles = new Array();
								}
							},'json');
					});
				}
			});
		</script>
	</head>
	<body>
		<div id="vaultObj"></div>
	</body>
</html>
