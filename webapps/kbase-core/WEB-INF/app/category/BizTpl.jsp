<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">


<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>

<link href="${pageContext.request.contextPath}/resource/corrections/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/corrections/css/satisfaction.css" rel="stylesheet" type="text/css" />

<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/Category.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
<script type="text/javascript">
	navigate_init('${robot_context_path}','${categoryId}');
	var swfAddress = '${swfAddress}';
	var human_id = '${job_id}';
	var faqId = '';
	var robot_context_path = '${robot_context_path}';
	var sessionUId = '${sessionUId}';
	var brand = '${brand}';
	var inner = '${inner}';
	var askContent = '';
	var categoryId = '${categoryId}';
	
	//点击右键时当前标准问对象(右键发送短信用),Category.js右键事件赋值
	var gzyd_sms_checkbox_obj;
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/GzydMappings.js"></script>
</head>


<body>
	<!--******************内容开始***********************-->
	<div class="gzcontent">
		<div class="gz-cn1">
			<!-- <div class="bfb gztitle1">
				<b>业务名称：</b>4G上网套餐、商旅套餐
			</div>
			<div class="bfb gztitle1">
				<font><b>知识分类：</b>语音->语音套餐</font><font><b>更新时间：</b>2014-11-16</font><font><b>点击量：</b>2223</font><font><b>适用品牌：</b>XXXX</font><font><b>使用地市：</b>上海</font>
			</div>
			 -->
			 
			 <div class="bfb gztitle1">
				<font><b>知识分类：</b><span id="categoryNv"></span></font>
			</div>
			 更新时间：${editTime}
			
		</div>
		<p class="clear"></p>

		<ul class="gzul">
		<s:iterator value="#request.list" id="all">
			<s:iterator value="#all" id="tag_1"  status="status1">
				<s:if test="#status1.getIndex()==0">
					<s:iterator value="#tag_1" id="tag_2" status="status2">
						<s:if test="#status2.getIndex()==0">
							<li>
								<a href="javascript:void(0);">
									<s:property value = "#tag_2.tag1"/>
								</a>
							</li>
						</s:if>
			    	  </s:iterator>
			    </s:if>
		    </s:iterator>
		</s:iterator>
			<s:if test="#request.list.size()>0">
			<li><a href="javascript:void(0);">
					显示所有
				</a>
			</li>
			</s:if>
			
			<p class="clear"></p>
		</ul>
		
		<s:set var="b_li1" value="0"></s:set>
		<s:iterator value="#request.list" id="all" status="status">
		<s:set var="b_li1" value="#b_li1+1"></s:set>
			<s:set var="b_li2" value="0"></s:set>
			<s:iterator value="#all" id="tag_1" status="status1">
			<s:set var="b_li2" value="#b_li2+1"></s:set>
			<div class="gz-cn2" id="${b_li1 }${b_li2 }" tag1="<s:property value = '#status.getIndex()'/>" tag2="<s:property value = '#status1.getIndex()'/>">
				<h2>
				<s:iterator value="#all" id="tag_1_1" status="status1_1">
				<s:if test="#status1_1.getIndex()==#status1.getIndex()">
				<s:iterator value="#tag_1_1" id="tag_2_1" status="status2_1">
					<s:if test="#status2_1.getIndex()==0">
							<s:property value = "#tag_2_1.tag1"/> -> <s:property value = "#tag_2_1.tag2"/>  
					</s:if>
				</s:iterator>
				</s:if>
				</s:iterator>
				
				</h2>
				<div class="gz-cn3">
					<div class="gztable01">
						<ul class="gzul02">
							<s:iterator value="#all" id="tag_1_2" status="status1_2">
							<s:iterator value="#tag_1_2" id="tag_2_2" status="status2_2">
								<s:if test="#status2_2.getIndex()==0">
									<s:if test="#status1_2.getIndex()==0">
										<li tag2="<s:property value = '#status1_2.getIndex()'/>" class="active">
									</s:if>
									<s:else>
										<li tag2="<s:property value = '#status1_2.getIndex()'/>">
									</s:else>
										<a href="javascript:void(0);">
											<s:property value = "#tag_2_2.tag2"/>
										</a>
									</li>
									
								</s:if>
							</s:iterator>
							</s:iterator>
							
						</ul>
	
						<div class="gzform01">
	
							<table width="100%" border="0" cellspacing="1" cellpadding="8"
								bgcolor="#c2c2c2">
								<tbody>
								<s:iterator value="#tag_1" id="tag_2">
									<tr>
										<td style="display:none;"><s:property value = '#tag_2.id'/></td>
										<td width="22%" height="0" align="left" valign="middle" class="question"><s:property value = "#tag_2.question"/></td>
										<td width="58%" height="0" align="left" valign="middle" class="answer">
											<s:property value = "#tag_2.answer" escape="false"/>
											<div class="yulan">
											<div>
												<s:iterator value="#tag_2.p4List" var="p4">
													<a href="${p4.url}" title="打开 ${p4.name}" target="_blank" >打开p4[${p4.name}]</a> 
													<a class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览p4[${p4.name}]</a> 
												</s:iterator>
											</div>
											<div>
												<s:iterator value="#tag_2.attachmentList" var="attachment">
													<a href="${attachment.url}" title="下载 ${attachment.name}">${attachment.name}</a>
													<a class="openAttachment" title="打开 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}">打开附件</a>
													<a class="showAttachment" title="预览 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}">预览附件</a>
												</s:iterator>
											</div>
											<div></div>
											</div>
										</td>
										<td width="10%" height="0" valign="middle">
										${objectName}
										</td>
										<td width="10%" height="0" align="center" valign="middle">
											<a class="other_platform" valueId="<s:property value = '#tag_2.id'/>" href="javascript:void(0);">其他平台</a>
										</td>
										<td>
											<s:if test='#tag_2.isableSMS==true'>
												<input type="checkbox" class="check" 
													name="gzyd_sms_checkbox_${b_li1 }${b_li2 }" 
													gzyd_sms_question='<s:property value="#tag_2.question" />' 
													gzyd_sms_answer='<s:property value="#tag_2.answer" />'
													gzyd_sms_isableSMS="${tag_2.isableSMS }"
													valueId="<s:property value = "#tag_2.Id"/>"/>
											</s:if>
										</td>
									</tr>
								</s:iterator>
								</tbody>
							</table>
	
						</div>
	
					</div>
					
					<ul class="gztable02">
						<li><a href="javascript:void(0);" name="gzyd_guidang">归档</a>
						</li>
						<li class="message_send"><a href="javascript:void(0);" name="gzyd_duanxin">发短信</a>
						</li>
						<!-- <li><a href="javascript:void(0);">答案反馈</a>
						</li> -->
						<li><a href="javascript:void(0);">历史版本</a>
						</li>
						<li><a href="javascript:void(0);">知识对比</a>
						</li>
					</ul>
				</div>
				<p class="clear"></p>
			
			</div>
			</s:iterator>
				</s:iterator>
				
				
			<div class="gz-cn2" id="all">
				<h2>
					<s:if test="#request.list.size()>0">
					显示所有
					</s:if>
					<s:else>
					<font color="red">没有此业务</font>
					</s:else>
				</h2>
				<div class="gz-cn3">
					<div class="gztable01">
						<!--
						            	<ul class="gzul02">
						                	<li class="active"><a href="">全文展示</a></li>
						                    <li><a href="">套内资费</a></li>
						                    <li><a href="">套外资费</a></li>
						                    <li><a href="">资费规则</a></li>
						                </ul>
										-->
			
			
						<s:iterator value="#request.list" id="all" status="status">
							<div class="gzform01">
								<s:iterator value="#all" id="tag_1" status="status1">
									<s:iterator value="#tag_1" id="tag_2_all" status="status2_all">
										<s:if test="#status2_all.getIndex()==0">
											<s:property value="#tag_2_all.tag1" /> -> <s:property
												value="#tag_2_all.tag2" />
										</s:if>
									</s:iterator>
									<table width="100%" border="0" cellspacing="1" cellpadding="8"
										bgcolor="#c2c2c2">
										<tbody>
											<s:iterator value="#tag_1" id="tag_2">
												<tr>
													<td style="display:none;"><s:property value = '#tag_2.id'/></td>
													<td width="22%" height="0" align="left" valign="middle"
														class="question"><s:property value="#tag_2.question" />
													</td>
													<td width="58%" height="0" align="left" valign="middle" class="answer">
														<s:property value="#tag_2.answer" escape="false"/>
														<div class="yulan">
															<div>
																<s:iterator value="#tag_2.p4List" var="p4">
																	<a href="${p4.url}" title="打开 ${p4.name}" target="_blank">打开p4[${p4.name}]</a>
																	<a class="showP4" title="预览 ${p4.name}"
																		href="javascript:void(0);" url="${p4.url}">预览p4[${p4.name}]</a>
																</s:iterator>
															</div>
															<div>
																<s:iterator value="#tag_2.attachmentList" var="attachment">
																	<a href="${attachment.url}" title="下载 ${attachment.name}">${attachment.name}</a>
																	<a class="openAttachment" title="打开 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}">打开附件</a>
																	<a class="showAttachment" title="预览 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}">预览附件</a>
																</s:iterator>
															</div>
															<div></div>
														</div>
													</td>
													<td width="10%" height="0" valign="middle">
													${objectName}
													</td>
													<td width="10%" height="0" align="center" valign="middle">
														<a class="other_platform"
														valueId="<s:property value = '#tag_2.id'/>"
														href="javascript:void(0);">其他平台</a>
													</td>
													<td>
														<s:if test='#tag_2.isableSMS==true'>
															<input type="checkbox" class="check" 
															name="gzyd_sms_checkbox" 
															gzyd_sms_question='<s:property value="#tag_2.question" />' 
															gzyd_sms_answer='<s:property value="#tag_2.answer" />' 
															gzyd_sms_isableSMS="${tag_2.isableSMS }" 
															valueId='<s:property value = "#tag_2.Id"/>' />
														</s:if>
													</td>	
												</tr>
											</s:iterator>
										</tbody>
									</table>
								</s:iterator>
							</div>
						</s:iterator>
					</div>
			
				</div>
			
				<ul class="gztable02">
					<li><a href="javascript:void(0);" id="gzyd_guidang">归档</a>
					</li>
					<li class="message_send"><a href="javascript:void(0);" id="gzyd_duanxin" >发短信</a>
					</li>
					<!-- <li><a href="javascript:void(0);">答案反馈</a>
					</li> -->
					<li><a href="javascript:void(0);">历史版本</a>
					</li>
					<li><a href="javascript:void(0);">知识对比</a>
					</li>
				</ul>
			</div>
			<p class="clear"></p>
			</div>
				
	</div>

	<!--******************内容结束***********************-->
	<!--******************弹框开始***********************-->
<div class="increase_d">
<div class="increase_dan">
<div class="increase_dan_title"><b>其他平台</b><a href="#"><img src="${pageContext.request.contextPath}/theme/red/resource/main/images/xinxi_ico1.jpg"></a></div>
<div class="increase_dan_con">
<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#FFF" class="biaoge">
	<tbody>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长00M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
  	</tbody>
</table>
</div>
</div>
</div>

<!--******************弹框结束***********************-->
<!-- 右键菜单start -->
<div id="menu" class="index_dankuang">
	<ul>
		<li><a href="javascript:void(0);">答案反馈</a></li>
	</ul>
</div>
<!-- 右键菜单end -->
<!--******************弹出框 答案反馈***********************-->
<div class="satisfaction" id="satisfaction" style="position: absolute;z-index:100010;display:none;">
		
		<div class="satisfaction_x"><a href="#" onclick="closesatisfaction()">&nbsp;</a></div>
		  <form id="form1" name="form1" method="post" action="">
		  <input type="hidden" name="obj_id" value="<s:property value = '#tag_2.id'/>" />
		  <input type="hidden" name="category_id" value="${categoryId}" />
		  <div class="satisfaction_title">
		    <div class="satisfaction_title_div1" id="title_div">
		        <ul>
		          <li class="cl_333 fs14 fw ff_wei mt10">知识满意度：</li>
		          <li class="satisfaction_title_div1_fen"><span class="cl_hongse fs36 fw" id="score">5</span>分
	              <s:iterator value="#request.satisfacttype" var="va" status="sta">
	               <s:if test="#sta.index==0">
	                   <label>
	                  <input type="radio" name="satisfaction" value="${va.type}" id="RadioGroup1_0" onclick="changeinfo('${va.type}','${va.name}');" checked="checked"/>${va.name}
	                </label>
	               </s:if>
	               <s:else>
	               <label>
	                  <input type="radio" name="satisfaction" value="${va.type}" id="RadioGroup1_0" onclick="changeinfo('${va.type}','${va.name}');" />${va.name}
	                </label>
	                </s:else>
	              </s:iterator>
		         </li>
		         <li class="satisfaction_title_bottom">共<span class="fs14 cl_hongse ff_english fw" id="commcount"></span>人评价</li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div2" id="satisfaction_type">
		       <ul>
		         <li class="cl_333 fs14 fw ff_wei">选择评论类型：</li>
		         <li id="typelist" style="display: none">
		          <s:iterator value="#request.commenttype" var="va">
	               <label>
	                  <input type="radio" name="type" value="${va.type}" id="RadioGroup1_0" />${va.name}
	                </label>
	               </s:iterator>
		         </li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div3">
		       <ul>
		         <li class="cl_333 fs14 fw ff_wei">选择评论类型：</li>
		         <li>
		           <textarea name="content" cols="" rows="" id="sat_content"></textarea>
		         </li>
		       </ul>
		   </div>
		   <div class="satisfaction_title_div4">
		       <input type="button" class="satisfaction_btn" onclick="satisfactionsubmit();" value="提交评论" />
		   </div>
		 </div>
		 </form>
		 <div class="satisfaction_title1 cl_666">
		   <form id="form2">
		   <s:iterator value="#request.satisfacttype" var="va">
             <label>
                <input type="radio" name="filter" onclick="filterinfo('${va.type}');" value="${va.type}" id="filter" />${va.name}
              </label>
            </s:iterator>
            </form>
		 </div>
		 <div class="satisfaction_title2 cl_333 fw" id="satisfaction_content">
		   <p>评价内容</p>
		   <span>评价人</span> <span>评价时间</span> <span>满意度</span> <span>类型</span> <span>紧急程度</span> </div>
		  
		</div>
<!--******************弹出框 答案反馈***********************-->

<!-- 广州移动归档弹窗 -->
<jsp:include page="../custom/gzyd/Gzyd_guidang.jsp"></jsp:include>
<!-- 广州移动短信弹窗 -->
<jsp:include page="../custom/gzyd/Gzyd_duanxin.jsp"></jsp:include>
</body>
</html>
