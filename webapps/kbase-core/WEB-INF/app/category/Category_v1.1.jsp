<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>分类展示页面</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-title-weight, .kbs-title{
				font-weight: bolder;
				font-size: 14px;
			}
			.kbs-title{
				margin-left: 10px;
			}
			.box td.kbs-question{
				color: #000080;
			    font-family: 微软雅黑;
			    font-size: 14px;
			    line-height: 30px;
			}
			.box td.kbs-answer{
				color: #000000;
			    font-family: 微软雅黑;
			    font-size: 14px;
			    line-height: 28px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<!-- easy ui -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		
		<script type="text/javascript">
			var pageContext = {
				swfAddress: '${swfAddress}/attachment!getSwf.htm',
				human_id: '${job_id}',
				faqId: '',
				robot_context_path: '${robot_context_path}',
				sessionUId: '${sessionUId}',
				brand: '${sessionScope.brand}',
				brandDesc: '${brandName}',
				inner: '${inner}',
				askLocation: '${sessionScope.location}',
				askLocationDesc: '${locationName}',
				askContent: '',
				categoryId: '${categoryId}'
			}
		</script>
		
		<script type="text/javascript">
			$(function(){
				var data = {'format':'json', 'async':true, 'nodeid':pageContext.categoryId, 'nodetype':1};
				$.getJSON(pageContext.robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
					if (arr!=null){
						$(arr).each(function(i, item){
							//name id type bh
							if ($('#catePath').html()==''){
								$('#catePath').append('<a href="javascript:void(0)" categoryId="' + item.id + '">' + item.name + '</a>');
							}else{
								$('#catePath').append(' > <a href="javascript:void(0)" categoryId="' + item.id + '">' + item.name + '</a>');
							}
						});
					}
				});
				
			});
		</script>
	</head>

	<body>
		<div style="margin:5px;">
			<span class="kbs-title-weight">知识分类：</span> <span id="catePath"></span><span class="kbs-title">归属地：</span>${requestScope.locationName }<span class="kbs-title">品牌：</span>${requestScope.brandName }
			<br/>
			更新时间：${editTime}
		</div>
		
		<div class="easyui-tabs"  style="width:100%;">
			<c:forEach items="${requestScope.tagNameList}" var="xTagName">
				<div title="${xTagName }" style="padding:10px;">
					<div class="easyui-tabs" data-options="tabPosition:'left'" style="width:100%;height:400px;">
					
							<c:forEach items="${requestScope.list}" var="rlist">
								<c:forEach items="${rlist}" var="slist" varStatus="status">
									<c:forEach items="${slist}" var="qa" varStatus="xStatus">
										<c:if test="${xTagName==qa.tag1 && yTagName!=qa.tag2}">
											<c:set value="${qa.tag2}" var="yTagName"></c:set>
											<div title="${yTagName }" style="padding:10px;">
												<!-- Content Start -->
												<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%">
													
													<!-- 循环太多，心好累 -->
													<c:forEach items="${requestScope.list}" var="rlist_1">
														<c:forEach items="${rlist_1}" var="slist_1">
															<c:forEach items="${slist_1}" var="qa_1">
																<c:if test="${qa_1.tag1==xTagName && qa_1.tag2==yTagName }">
																	
																	<tr>
																		<td width="30%" class="kbs-question">${qa_1.question }</td>
																		<td width="55%" class="kbs-answer">${qa_1.answer }</td>
																		<td width="10%">其他平台</td>
																		<td width="5%"><input type="checkbox"></td>
																	</tr> <!-- /tr -->
																	
																</c:if>
															</c:forEach>
														</c:forEach>
													</c:forEach>
												
												</table>	<!-- /table -->
												<!-- Content End -->
											</div>	<!-- /yTagName div -->
										</c:if>
									</c:forEach>
								</c:forEach>
							</c:forEach>
						
					</div>	<!-- /y-tabs -->
				</div>	<!-- /xTagName div -->
			</c:forEach>
		</div>	<!-- /x-tabs -->
	</body>
</html>
