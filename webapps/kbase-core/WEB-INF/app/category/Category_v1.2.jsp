<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">


<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>

<link href="${pageContext.request.contextPath}/resource/corrections/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/corrections/css/satisfaction.css" rel="stylesheet" type="text/css" />

<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
	window.__kbs_has_parent = !(window.parent==window);
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/Category.js?20151023"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>
<style type="text/css">
	.gztable02{width:100%;height:33px;overflow:hidden;background-color:	#4473c2;filter:alpha(Opacity=80);-moz-opacity:0.8;opacity: 0.8;}
	.gztable02 li{width:19%;height:33px;float:left;}
	.gztable02 li a{width:100%;height:33px;display:inline-block;font-size:12px;color:#fff;text-decoration:none;text-align:center;line-height:33px;}
	.gztable02 li a:hover,.gztable02 li a:active{color:#333;background-color:#6d91cf;}
</style>
<script type="text/javascript">
	var swfAddress = '${swfAddress}/attachment!getSwf.htm';
	var human_id = '${job_id}';
	var faqId = '';
	var robot_context_path = '${robot_context_path}';
	var sessionUId = '${sessionUId}';
	var brand = '${sessionScope.brand}';
	var brandDesc = '${brandName}';
	var inner = '${inner}';
	var askLocation = '${sessionScope.location}';
	var askLocationDesc = '${locationName}';
	var askContent = '';
	var categoryId = '${categoryId}';
	
	//点击右键时当前标准问对象(右键发送短信用),Category.js右键事件赋值
	var gzyd_sms_checkbox_obj;
	
	//分类路径加载 modify by eko.zhan at 2015-10-08
	$(function(){
		navigate_init('${robot_context_path}','${categoryId}');
	});
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/custom/gzyd/js/GzydMappings.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.finder.js?20150805"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/find.js?20150805"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/category/js/operate.js?20151010"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/icon.css">
</head>


<body>
    <!--******************操作***********************-->
	<div class="kbs-global-gztable-css">
		<ul class="gztable02">
			<li><a href="javascript:void(0);" style="display: none;" name="gzyd_guidang">归档</a></li>
			<li class="message_send"><a href="javascript:void(0);" name="gzyd_duanxin">发短信</a></li>
			<li><a href="javascript:void(0);">历史版本</a></li>
			<li><a href="javascript:void(0);">知识对比</a></li>
			<li><a href="javascript:void(0);" id="btnFullScreen">全屏显示</a></li>
		</ul>
	</div>
	<!--******************内容开始***********************-->
	<div class="gzcontent">
		<div class="gz-cn1">
			<!-- <div class="bfb gztitle1">
				<b>业务名称：</b>4G上网套餐、商旅套餐
			</div>
			<div class="bfb gztitle1">
				<font><b>知识分类：</b>语音->语音套餐</font><font><b>更新时间：</b>2014-11-16</font><font><b>点击量：</b>2223</font><font><b>适用品牌：</b>XXXX</font><font><b>使用地市：</b>上海</font>
			</div>
			 -->
			 
			 <div class="bfb gztitle1">
				<font><b>知识分类：</b><span id="categoryNv"></span>&nbsp;&nbsp;&nbsp;&nbsp;|归属地：<s:property value="#request.locationName"/>&nbsp;&nbsp;|品牌：<s:property value="#request.brandName"/></font>
			</div>
			 更新时间：${editTime}
			
		</div>
		<p class="clear"></p>

		<ul class="gzul">
		<s:iterator value="#request.list" id="all">
			<s:iterator value="#all" id="tag_1"  status="status1">
				<s:if test="#status1.getIndex()==0">
					<s:iterator value="#tag_1" id="tag_2" status="status2">
						<s:if test="#status2.getIndex()==0">
							<li>
								<a href="javascript:void(0);">
									<s:property value = "#tag_2.tag1"/>
								</a>
							</li>
						</s:if>
			    	  </s:iterator>
			    </s:if>
		    </s:iterator>
		</s:iterator>
			<s:if test="#request.list.size()>0">
			<li><a href="javascript:void(0);">
					显示所有
				</a>
			</li>
			</s:if>
			
			<p class="clear"></p>
		</ul>
		
		<s:set var="b_li1" value="0"></s:set>
		<s:iterator value="#request.list" id="all" status="status">
		<s:set var="b_li1" value="#b_li1+1"></s:set>
			<s:set var="b_li2" value="0"></s:set>
			<s:iterator value="#all" id="tag_1" status="status1">
			<s:set var="b_li2" value="#b_li2+1"></s:set>
			<div class="gz-cn2" id="${b_li1 }${b_li2 }" tag1="<s:property value = '#status.getIndex()'/>" tag2="<s:property value = '#status1.getIndex()'/>">
				<h2>
				<s:iterator value="#all" id="tag_1_1" status="status1_1">
				<s:if test="#status1_1.getIndex()==#status1.getIndex()">
				<s:iterator value="#tag_1_1" id="tag_2_1" status="status2_1">
					<s:if test="#status2_1.getIndex()==0">
							<s:property value = "#tag_2_1.tag1"/> -> <s:property value = "#tag_2_1.tag2"/>  
					</s:if>
				</s:iterator>
				</s:if>
				</s:iterator>
				
				</h2>
				<div class="gz-cn3">
					<div class="gztable01">
						<ul class="gzul02">
							<s:iterator value="#all" id="tag_1_2" status="status1_2">
							<s:iterator value="#tag_1_2" id="tag_2_2" status="status2_2">
								<s:if test="#status2_2.getIndex()==0">
									<s:if test="#status1_2.getIndex()==0">
										<li tag2="<s:property value = '#status1_2.getIndex()'/>" class="active">
									</s:if>
									<s:else>
										<li tag2="<s:property value = '#status1_2.getIndex()'/>">
									</s:else>
										<a href="javascript:void(0);">
											<s:property value = "#tag_2_2.tag2"/>
										</a>
									</li>
									
								</s:if>
							</s:iterator>
							</s:iterator>
							
						</ul>
	
						<div class="gzform01">
	
							<table width="100%" border="0" cellspacing="1" cellpadding="8"
								bgcolor="#c2c2c2">
								<tbody>
								<s:iterator value="#tag_1" id="tag_2">
									<tr>
										<td style="display:none;"><s:property value = '#tag_2.id'/></td>
										<td width="22%" height="0" align="left" valign="middle" class="question"><s:property value = "#tag_2.question"/></td>
										<td width="68%" height="0" align="left" valign="middle" class="answer" _oid="<s:property value="#tag_2.oid"/>">
											<div>
												<s:property value = "#tag_2.answer" escape="false"/>
											</div>
											<div class="yulan">
											<div>
												<s:iterator value="#tag_2.p4List" var="p4">
													<a href="${p4.url}" title="打开 ${p4.name}" target="_blank" >打开p4[${p4.name}]</a> 
													<a class="showP4" title="预览 ${p4.name}" href="javascript:void(0);" url="${p4.url}">预览p4[${p4.name}]</a> 
												</s:iterator>
											</div>
											<div>
												<s:iterator value="#tag_2.attachmentList" var="attachment">
													<a href="${attachment.url}" title="下载 ${attachment.name}">${attachment.name}</a>
													<a class="openAttachment" title="打开 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">打开附件</a>
													<a class="showAttachment" title="预览 ${attachment.name}" href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">预览附件</a>
												</s:iterator>
											</div>
											<div></div>
											<div>
												<s:if test="#tag_2.zdyw!=null && #tag_2.zdyw.size()>0">
													<ul class="zdyw">
													<s:iterator value="#tag_2.zdyw" var="_zdyw">
														<li>[${_zdyw}] 
															<span q="${_zdyw}">
																<a href="javascript:void(0);" style="font-size:12px;color:#666666;">相关答案</a> | <a href="javascript:void(0);" style="font-size:12px;color:#666666;">分类展示</a>
															</span>
														</li>
													</s:iterator>
													</ul>
												</s:if>
											</div>
											</div>
										</td>
										<td width="10%" height="0" align="center" valign="middle">
											<a class="other_platform" valueId="<s:property value = '#tag_2.id'/>" href="javascript:void(0);">其他平台</a>
										</td>
										<td>
											<s:if test='#tag_2.isableSMS==true'>
												<input type="checkbox" class="check" 
													name="gzyd_sms_checkbox_${b_li1 }${b_li2 }" 
													gzyd_sms_question='<s:property value="#tag_2.question" />' 
													gzyd_sms_answer='<s:property value="#tag_2.answer" />'
													gzyd_sms_isableSMS="${tag_2.isableSMS }"
													valueId="<s:property value = "#tag_2.Id"/>"/>
											</s:if>
										</td>
									</tr>
								</s:iterator>
								</tbody>
							</table>
	
						</div>
	
					</div>
					<!-- 
					<ul class="gztable02">
						<li><a href="javascript:void(0);" name="gzyd_guidang">归档</a>
						</li>
						<li class="message_send"><a href="javascript:void(0);" name="gzyd_duanxin">发短信</a>
						</li>
						<li><a href="javascript:void(0);">答案反馈</a>
						</li> 
						<li><a href="javascript:void(0);">历史版本</a>
						</li>
						<li><a href="javascript:void(0);">知识对比</a>
						</li>
					</ul> -->
				</div>
				<p class="clear"></p>
			
			</div>
			</s:iterator>
				</s:iterator>
				
				
			<div class="gz-cn2" id="all">
				<h2>
					<s:if test="#request.list.size()>0">
					显示所有
					</s:if>
					<s:else>
					<font color="red">没有此业务</font>
					</s:else>
				</h2>
				<div class="gz-cn3">
					<div class="gztable01">
						<!--
						            	<ul class="gzul02">
						                	<li class="active"><a href="">全文展示</a></li>
						                    <li><a href="">套内资费</a></li>
						                    <li><a href="">套外资费</a></li>
						                    <li><a href="">资费规则</a></li>
						                </ul>
										-->
			
			
						<s:iterator value="#request.list" id="all" status="status">
							<div class="gzform01">
								<s:iterator value="#all" id="tag_1" status="status1">
									<s:iterator value="#tag_1" id="tag_2_all" status="status2_all">
										<s:if test="#status2_all.getIndex()==0">
											<s:property value="#tag_2_all.tag1" /> -> <s:property
												value="#tag_2_all.tag2" />
										</s:if>
									</s:iterator>
									<table width="100%" border="0" cellspacing="1" cellpadding="8"
										bgcolor="#c2c2c2">
										<tbody>
											<s:iterator value="#tag_1" id="tag_2">
												<tr>
													<td style="display:none;"><s:property value = '#tag_2.id'/></td>
													<td width="22%" height="0" align="left" valign="middle"
														class="question"><s:property value="#tag_2.question" />
													</td>
													<td width="68%" height="0" align="left" valign="middle" class="answer" _oid="<s:property value="#tag_2.oid"/>">
														<div>
															<s:property value="#tag_2.answer" escape="false"/>
														</div>
														<div class="yulan">
															<div>
																<s:iterator value="#tag_2.p4List" var="p4">
																	<a href="${p4.url}" title="打开 ${p4.name}" target="_blank">打开p4[${p4.name}]</a>
																	<a class="showP4" title="预览 ${p4.name}"
																		href="javascript:void(0);" url="${p4.url}">预览p4[${p4.name}]</a>
																</s:iterator>
															</div>
															<div>
																<s:iterator value="#tag_2.attachmentList" var="attachment">
																	<a href="${attachment.url}" title="下载 ${attachment.name}">${attachment.name}</a>
																	<a class="openAttachment" title="打开 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">打开附件</a>
																	<a class="showAttachment" title="预览 ${attachment.name}"
																		href="javascript:void(0);" filename="${attachment.id}" _name="${attachment.name }">预览附件</a>
																</s:iterator>
															</div>
															<div></div>
														</div>
														<div>
															<s:if test="#tag_2.zdyw!=null && #tag_2.zdyw.size()>0">
																<ul class="zdyw">
																<s:iterator value="#tag_2.zdyw" var="_zdyw">
																	<li>[${_zdyw}] 
																		<span q="${_zdyw}">
																			<a href="javascript:void(0);" style="font-size:12px;color:#666666;">相关答案</a> | <a href="javascript:void(0);" style="font-size:12px;color:#666666;">分类展示</a>
																		</span>
																	</li>
																</s:iterator>
																</ul>
															</s:if>
														</div>
													</td>
													<td width="10%" height="0" align="center" valign="middle">
														<a class="other_platform"
														valueId="<s:property value = '#tag_2.id'/>"
														href="javascript:void(0);">其他平台</a>
													</td>
													<td>
														<s:if test='#tag_2.isableSMS==true'>
															<input type="checkbox" class="check" 
															name="gzyd_sms_checkbox" 
															gzyd_sms_question='<s:property value="#tag_2.question" />' 
															gzyd_sms_answer='<s:property value="#tag_2.answer" />' 
															gzyd_sms_isableSMS="${tag_2.isableSMS }" 
															valueId='<s:property value = "#tag_2.Id"/>' />
														</s:if>
													</td>	
												</tr>
											</s:iterator>
										</tbody>
									</table>
								</s:iterator>
							</div>
						</s:iterator>
					</div>
			
				</div>
			   <!-- 
				<ul class="gztable02">
					<li><a href="javascript:void(0);" id="gzyd_guidang">归档</a>
					</li>
					<li class="message_send"><a href="javascript:void(0);" id="gzyd_duanxin" >发短信</a>
					</li>
					 <li><a href="javascript:void(0);">答案反馈</a>
					</li> 
					<li><a href="javascript:void(0);">历史版本</a>
					</li>
					<li><a href="javascript:void(0);">知识对比</a>
					</li>
				</ul> -->
			</div>
			<p class="clear"></p>
			</div>
				
	</div>

	<!--******************内容结束***********************-->
	<!--******************弹框开始***********************-->
<div class="increase_d">
<div class="increase_dan">
<div class="increase_dan_title"><b>其他平台</b><a href="#"><img src="${pageContext.request.contextPath}/theme/red/resource/main/images/xinxi_ico1.jpg"></a></div>
<div class="increase_dan_con">
<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#FFF" class="biaoge">
	<tbody>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长50分钟国内通话时长00M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
      <tr>
        <td width="22%" height="0" align="left" valign="middle">58元上网资费和内容</td>
        <td width="78%" height="0" align="left" valign="middle">国内接听免费，含50分钟国内通话时长，200M国内手机上网流量，送来电显示功能。</td>
      </tr>
  	</tbody>
</table>
</div>
</div>
</div>

<!--******************弹框结束***********************-->
<!-- 右键菜单start -->
<div id="menu" class="index_dankuang">
	<ul>
		<li><a href="javascript:void(0);">答案反馈</a></li>
		<li><a href="javascript:void(0);" id="navFav">收藏</a></li>
		<li><a href="javascript:void(0);" id="navRecommend">推荐</a></li>
	</ul>
</div>
<!-- 右键菜单end -->

<!-- 广州移动归档弹窗 -->
<jsp:include page="../custom/gzyd/Gzyd_guidang.jsp"></jsp:include>
<!-- 广州移动短信弹窗 -->
<jsp:include page="../custom/gzyd/Gzyd_duanxin.jsp"></jsp:include>

</body>
</html>
