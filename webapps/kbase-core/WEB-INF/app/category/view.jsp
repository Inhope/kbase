<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>分类展示页面</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-title-weight, .kbs-title{
				font-weight: bolder;
				font-size: 14px;
			}
			.kbs-title{
				margin-left: 10px;
			}
			.box td.kbs-question{
				color: #000080;
			    font-family: 微软雅黑;
			    font-size: 14px;
			    line-height: 30px;
			}
			.box td.kbs-answer{
				color: #000000;
			    font-family: 微软雅黑;
			    font-size: 14px;
			    line-height: 28px;
			}
			.kbs-embed{
				border: 1px solid #666666;
			}
			.kbs-panel-undefined{
				text-align: center;
			}
			a:link {
				color:#004080;
			    text-decoration: none;
			}
			a:visited {
			    color:#004080;
			    text-decoration: none;
			}
			a:hover {
			    color:#004080;
			    text-decoration: underline;
			}
			a:active {
			    color:#004080;
			    text-decoration: none;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<script type="text/javascript">
			//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
			window.__kbs_has_parent = !(window.parent==window);
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<!-- easy ui -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.finder.js"></script>
		
		<script type="text/javascript">
			var pageContext = {
				swfAddress: '${swfAddress}',
				human_id: '${job_id}',
				faqId: '',
				robot_context_path: '${robot_context_path}',
				sessionUId: '${sessionUId}',
				brand: '${sessionScope.brand}',
				brandDesc: '${brandName}',
				inner: '${inner}',
				askLocation: '${sessionScope.location}',
				askLocationDesc: '${locationName}',
				askContent: '',
				categoryId: '${categoryId}'
			}
		</script>
		
		<script type="text/javascript">
			/**
			 * @author eko.zhan
			 * url, width, height, args
			 */
			function _showModalDialog(opts){
				opts = $.extend(true, {
					width: 500,
					height: 400,
					args: window
				}, opts);
				var _diaWidth = opts.width;
				var _diaHeight = opts.height;
				var _scrWidth = screen.width;
				var _scrHegiht = screen.height;
				var _diaLeft = (_scrWidth-_diaWidth)/2;
				var _diaTop = (_scrHegiht-_diaHeight)/2;
				var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
				window.showModalDialog(opts.url, opts.args, params);
			}
		
			$(function(){
				var _record = {};	//右键菜单记录数据
				
				//高度
				if (window.__kbs_has_parent){
					$('.kbs-xtabs').tabs({height: screen.height-368});
				}else{
					$('.kbs-xtabs').tabs({height: screen.height-268});
				}
				
				//只用于windows
				$('#findbar').hide();
				if (!!window.ActiveXObject || "ActiveXObject" in window){
					//快速搜索功能	 
					$.kbase.find('tabAll', false);
					$('#tt').tabs({
						onSelect: function(title, index){
							var tab = $('#tt').tabs('getTab', index);
							var tabid = $(tab).attr('id');
							if (tabid!=undefined && tabid=='tabAll'){
								$('#findbar').show();
							}else{
								$('#findbar').hide();
							}
						}
					});
				}
			
				var data = {'format':'json', 'async':true, 'nodeid':pageContext.categoryId, 'nodetype':1};
				$.getJSON(pageContext.robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
					if (arr!=null){
						$(arr).each(function(i, item){
							//name id type bh
							if ($('#catePath').html()==''){
								$('#catePath').append('<a href="javascript:void(0)" categoryId="' + item.id + '">' + item.name + '</a>');
							}else{
								$('#catePath').append(' > <a href="javascript:void(0)" categoryId="' + item.id + '">' + item.name + '</a>');
							}
						});
					}
				});
				
				//右键菜单事件
				$('table tr td.kbs-answer').bind('contextmenu',function(e){
					var elem = e.target;
					var $tr = $(elem).parents('tr:first');
					_record.id = $tr.attr('_id');
					_record.objid = $tr.attr('_objid');
					_record.enablesms = $tr.attr('_enablesms');
					_record.question = $tr.find('td.kbs-question').text();
					_record.answer = $tr.find('td.kbs-answer span:first').text();
					
					//归档功能暂时屏蔽
					if (_record.enablesms=='true'){
						$('#mm').menu('showItem', $('#menuSendSms'));
	            		//$('#mm').menu('showItem', $('#menuArchive'));
					}else{
						$('#mm').menu('hideItem', $('#menuSendSms'));
	            		//$('#mm').menu('hideItem', $('#menuArchive'));
					}
					
	                e.preventDefault();
	                
	                $('#mm').menu('show', {
	                    left: e.pageX,
	                    top: e.pageY
	                });
	            });
	            
	            //指定业务
	            $('a[name="navRelQues"]').click(function(){
	            	var _text = $(this).text();
	            	var _url = $.fn.getRootPath() + '/app/search/search.htm?searchMode=2&askContent=' + _text;
	            	parent.TABOBJECT.open({
	   					id : _text,
	   					name : _text,
	   					hasClose : true,
	   					url : _url,
	   					isRefresh : true
	   				}, this);
	            });
	            //相关答案
	            $('a[name="navRelAns"]').click(function(){
	            	var _this = this;
	            	var ques = $(this).parent('span').attr('_question');
	            	if ($(_this).attr('_answer')==null){
	            		window.__kbs_layer_index = layer.load('请稍候...');
	            		$.ajax({
	            			async: false,
	            			url: $.fn.getRootPath() + '/app/category/all-ans!zdyw.htm',
	            			data: {question: ques, type:1},
	            			type: 'POST',
	            			dataType: 'text',
	            			success: function(text){
	            				layer.close(window.__kbs_layer_index);
			            		if (text=='null'){
			            			$(_this).attr('_answer', -1);
			            		}else{
			            			$(_this).attr('_answer', 1);
			            			$(_this).parent('span').append('<br><div title="'+ques+'" class="kbs-embed" style="display:none;">' + text + '</div>');
			            		}
	            			}
	            		});
	            	}
	            	if ($(_this).attr('_answer')==-1){
	            		alert('当前地市未关联相应的知识');
		            	return false;
	            	}else{
	            		if ($('div[title="' + ques + '"]:visible').length==0){
	            			$('div[title="' + ques + '"]').show();
	            		}else{
	            			$('div[title="' + ques + '"]').hide();
	            		}
	            		
	            		//jeasyui IE7 bug 隐藏和显示在IE7下存在问题，滚动条高度出错
		            	if ($.browser.msie && ($.browser.version=='7.0' || $.browser.version=='8.0')){
		            		$('.tabs-selected').click();
		            	}
	            	}
	            });
	            //分类展示
	            $('a[name="navCateView"]').click(function(){
	            	var _this = this;
	            	var ques = $(this).parent('span').attr('_question');
	            	if ($(_this).attr('_cateid')==null){
	            		window.__kbs_layer_index = layer.load('请稍候...');
	            		$.ajax({
	            			async: false,
	            			url: $.fn.getRootPath() + '/app/category/all-ans!zdyw.htm',
	            			data: {question: ques, type:2},
	            			type: 'POST',
	            			dataType: 'json',
	            			success: function(json){
	            				layer.close(window.__kbs_layer_index);
			            		if (json==null){
			            			$(_this).attr('_cateid', -1);
			            			
			            		}else{
			            			$(_this).attr('_cateid', json.id);
			            			$(_this).attr('_catename', json.name);
			            		}
	            			}
	            		});
	            	}
	            	if ($(_this).attr('_cateid')==-1){
            			alert('当前地市未关联相应的知识');
		            	return false;
            		}else{
            			var _id = $(_this).attr('_cateid');
            			var _text = $(_this).attr('_catename');
            			var url = $.fn.getRootPath()+"/app/category/category.htm?type=3&categoryId=" + _id;
		   				parent.TABOBJECT.open({
		   					id : _id,
		   					name : _text,
		   					hasClose : true,
		   					url : url,
		   					isRefresh : true
		   				}, this);
            		}
	            });
	            
	            //打开P4 预览P4
	            $('a[name="navOpenP4"], a[name="navReviewP4"]').click(function(){
	            	var _this = this;
	            	var _url = $(_this).attr('_url');
	            	
	            	if ($(_this).attr('name')=='navOpenP4'){
	            		_showModalDialog({url: _url, width: screen.width, height: screen.height});
	            	}else{
	            		var p4area = $(_this).siblings('div[name="p4area"]:visible');
		            	if (p4area.length==1 && p4area.find('iframe').attr('src').indexOf(_url)!=-1){
		            		p4area.hide();
		            	}else{
		            		$(_this).siblings('div[name="p4area"]').find('iframe').attr('src', _url);
		            		$(_this).siblings('div[name="p4area"]').show();
		            	}
		            	
		            	//jeasyui IE7 bug 隐藏和显示在IE7下存在问题，滚动条高度出错
		            	if ($.browser.msie && ($.browser.version=='7.0' || $.browser.version=='8.0')){
		            		$('.tabs-selected').click();
		            	}
	            	}
	            });
	            
	            //打开附件 //预览附件
	            $('a[name="navOpenAtt"], a[name="navReviewAtt"]').click(function(){
	            	var _this = this;
	            	var _host = pageContext.swfAddress;
	            	if (_host.indexOf('attachment!')!=-1){
	            		_host = _host.substring(0, _host.indexOf('attachment!'));	//http://gzyd_swf.demo.xiaoi.com/robot-swf/
	            	}
	            	if (!_host.endWithIgnoreCase('/')){
	            		_host = _host + '/';
	            	}
	            	var _filename = $(_this).attr('_name');
	            	var _fileid = $(_this).attr('_id');
	            	var _filetype = '';
	            	if (_filename.lastIndexOf('.')!=-1){
	            		_filetype = _filename.substring(_filename.lastIndexOf('.'));
	            	}
	            	var _url = _host + 'attachment!getSwf.htm?callbackparam=?';
	            	var _data = {fileName: _fileid + _filetype};
	            	$.getJSON(_url, _data, function(json){
	            		//alert(json);	//返回的是附件路径	robot-swf/swf/swf/1444803852968/201510141725249720.jpg
	            		var _url = _host + json.substring(json.indexOf('/')+1);
	            		if ($(_this).attr('name')=='navOpenAtt'){
	            			//打开
	            			_showModalDialog({url: _url, width: screen.width, height: screen.height});
	            		}else{
	            			//预览
	            			var attarea = $(_this).siblings('div[name="attarea"]:visible');
			            	if (attarea.length==1 && attarea.find('iframe').attr('src').indexOf(_url)!=-1){
			            		attarea.hide();
			            	}else{
			            		$(_this).siblings('div[name="attarea"]').find('iframe').attr('src', _url);
			            		$(_this).siblings('div[name="attarea"]').show();
			            	}
			            	
			            	//jeasyui IE7 bug 隐藏和显示在IE7下存在问题，滚动条高度出错
			            	if ($.browser.msie && ($.browser.version=='7.0' || $.browser.version=='8.0')){
			            		$('.tabs-selected').click();
			            	}
	            		}
	            	}, 'json')
	            });
				
				//其他平台
				$('a[name="btnOtherPlatform"]').click(function(){
					var valueid = $(this).parent().parent('tr').attr('_id');
					_showModalDialog({url: $.fn.getRootPath() + '/app/category/all-ans!view.htm?valueId=' + valueid});
				});
				
				//全屏显示
				$('#btnFullScreen').click(function(){
					if (!window.__kbs_has_parent) return false;
					_showModalDialog({
						url: location.href,
						width: screen.width,
						height:screen.height
					});
				});
				
				//发送短信
				$('#btnSendSms').click(function(){
					var tab = $('.tabs-selected:visible:last');
					var tabHeader = tab.parents('.tabs-header:first');
					var tabContent = tabHeader.siblings('.tabs-panels:first');
					var checkboxarr = tabContent.find('input[name="sms"]:checked');
					if (checkboxarr.length==0){
						alert('请选择待发送的知识');
						return false;
					} //end if
					var _arr = [];
					$(checkboxarr).each(function(i, item){
						var $tr = $(item).parents('tr');
						_arr.push({
							id: $tr.attr('_id'),
							objid: $tr.attr('_objid'),
							enablesms: $tr.attr('_enablesms'),
							question: $tr.find('td.kbs-question').text(),
							answer: $tr.find('td.kbs-answer span:first').text()
						});
					});
					var _url = $.fn.getRootPath() + '/app/category/category!sendSms.htm'
					_showModalDialog({url: _url, width:600, args: _arr});
				});
				
				//归档
				$('#btnArchive').click(function(){
					//
					
				});
				//置顶
				$('#btnGoup').click(function(){
					var tab = $('.tabs-selected:visible:last');
					var tabHeader = tab.parents('.tabs-header:first');
					var tabBody = tabHeader.siblings('.tabs-panels:first').find('.panel-body:visible');
					tabBody.scrollTop(0);
				});
				
				
				////////////////////右键操作//////////////////////////
				$('#menuErrCorrect').click(function(){
					var _params = $.param({
						'objId': _record.objid,
						'faqId': _record.id,
						'cateId': pageContext.categoryId/*,
						'question': _record.question,
						'answer': _record.answer*/
					});
					var _url = $.fn.getRootPath() + "/app/corrections/corrections!send.htm?" + _params;
					_showModalDialog({url: _url, width:835, height:600});
				});
				$('#menuFav').click(function(){
					var _params = $.param({
						'objid': '',
						'faqid': _record.id,
						'cataid': pageContext.categoryId
					});
					var _url = $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?' + _params;
					_showModalDialog({url: _url, width:500, height:300});
				});
				$('#menuCopy').click(function(){
					if (!!window.ActiveXObject || "ActiveXObject" in window){
						window.clipboardData.setData("Text", _record.question + '\r\n' + _record.answer);
					}else{
						alert('当前浏览器不支持复制功能');
					}
				});
				$('#menuRecommand').click(function(){
					var _url = $.fn.getRootPath() + '/app/recommend/recommend!open.htm?cateid=' + pageContext.categoryId + '&valueid=' + _record.id;
					_showModalDialog({url: _url, width: 540, height:480});
				});
				$('#menuSendSms').click(function(){
					var _url = $.fn.getRootPath() + '/app/category/category!sendSms.htm'
					_showModalDialog({url: _url, width:600, args: [_record]});
				});
				$('#menuArchive').click(function(){
					
				});
			});
		</script>
	</head>

	<body>
		<div style="margin:5px;">
			<span class="kbs-title-weight">知识分类：</span> <span id="catePath"></span><span class="kbs-title">归属地：</span>${requestScope.locationName }<span class="kbs-title">品牌：</span>${requestScope.brandName }
			<br/>
			更新时间：${editTime}
		</div>
		
        <c:choose>
        	<c:when test="${fn:length(requestScope.qaList)>0 }">
        		
        		
        		<!-- 主要内容 -->
				<div class="easyui-tabs" id="tt" data-options="tools:'#tab-tools', cache:false" style="width:100%;">
					<c:forEach items="${requestScope.tagNameList}" var="xTagName">
						<div title="${xTagName }" style="padding:10px;">
							<div class="easyui-tabs kbs-xtabs" data-options="tabPosition:'left', cache:false" style="width:100%;">
							
									<c:forEach items="${requestScope.qaList}" var="qa">
										<c:if test="${xTagName==qa.tag1 && yTagName!=qa.tag2}">
											<c:set value="${qa.tag2}" var="yTagName"></c:set>
											<div title="${yTagName }" style="padding:10px;">
												<!-- Content Start -->
												<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%">
													
													<!-- 循环太多，心好累 -->
													<c:forEach items="${requestScope.qaList}" var="qaobj">
														<c:if test="${qaobj.tag1==xTagName && qaobj.tag2==yTagName }">
															
															<tr _id="${qaobj.id }" _enableSms="${qaobj.isableSMS}" _objid="${qaobj.oid }">
																<td width="20%" class="kbs-question">${qaobj.question }</td>
																<td width="65%" class="kbs-answer">
																	<span>${qaobj.answer }</span>
																	<br>
																	<div style="font-size: 12px;">
																		<c:forEach items="${qaobj.p4List}" var="p4">
																			${p4.name }
																			<a href="javascript:void(0)" _url="${p4.url }" name="navOpenP4">[打开P4]</a> | 
																			<a href="javascript:void(0)" _url="${p4.url }" name="navReviewP4">[预览P4]</a>
																			<br>
																		</c:forEach>
																		<c:if test="${fn:length(qaobj.p4List)>0}">
																			<div class="kbs-embed" name="p4area" style="display:none">
																				<iframe frameborder="0" src="" style="width:100%;height:300px;"></iframe>
																			</div>
																		</c:if>
																		<c:forEach items="${qaobj.attachmentList}" var="att">
																			<a href="${att.url }" target="_blank">${att.name }</a>
																			<a href="javascript:void(0)" name="navOpenAtt" _name="${att.name }" _id="${att.id }">[打开]</a> | 
																			<a href="javascript:void(0)" name="navReviewAtt" _name="${att.name }" _id="${att.id }">[预览]</a>
																			<br>
																		</c:forEach>
																		<c:if test="${fn:length(qaobj.attachmentList)>0}">
																			<div class="kbs-embed" name="attarea" style="display:none">
																				<iframe frameborder="0" src="" style="width:100%;height:300px;"></iframe>
																			</div>
																		</c:if>
																		<c:forEach items="${qaobj.zdyw}" var="item">
																			<span _question="${item }"><a href="javascript:void(0)" name="navRelQues">${item}</a> | <a href="javascript:void(0)" name="navRelAns">[相关答案]</a> | <a href="javascript:void(0)" name="navCateView">[分类展示]</a></span>
																			<br>
																		</c:forEach>
																	</div>
																</td>
																<td width="10%"><a href="javascript:void(0);" name="btnOtherPlatform">其他平台</a></td>
																<td width="5%">
																	<c:if test="${qaobj.isableSMS}">
																		<input type="checkbox" name="sms">
																	</c:if>
																	&nbsp;
																</td>
															</tr> <!-- /tr -->
															
														</c:if>
													</c:forEach>
												
												</table>	<!-- /table -->
												<!-- Content End -->
											</div>	<!-- /yTagName div -->
										</c:if>
									</c:forEach>
								
							</div>	<!-- /y-tabs -->
						</div>	<!-- /xTagName div -->
					</c:forEach>
					<div title="显示所有" id="tabAll" style="padding:10px;">
						<div class="easyui-tabs kbs-xtabs" data-options="tabPosition:'left', cache:false" style="width:100%;">
							<div title="详情明细" style="padding:10px;">
								
								<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:100%;">
									<c:forEach items="${requestScope.qaList}" var="qaobj">
										<c:set value="${qaobj.tag1}  --> ${qaobj.tag2 }" var="curQaNav"></c:set>
										<c:if test="${curQaNav!=tmpQaNav}">
											<c:set value="${curQaNav }" var="tmpQaNav"></c:set>
											<tr>
												<td colspan="4" class="kbs-title" style="height:30px;">${curQaNav }</td>
											</tr>
										</c:if>
										<tr _id="${qaobj.id }" _enableSms="${qaobj.isableSMS}" _objid="${qaobj.oid }">
											<td width="20%" class="kbs-question">${qaobj.question }</td>
											<td width="65%" class="kbs-answer">
												<span>${qaobj.answer }</span>
												<br>
												<div style="font-size: 12px;">
													<c:forEach items="${qaobj.p4List}" var="p4">
														${p4.name }
														<a href="javascript:void(0)" _url="${p4.url }" name="navOpenP4">[打开P4]</a> | 
														<a href="javascript:void(0)" _url="${p4.url }" name="navReviewP4">[预览P4]</a>
														<br>
													</c:forEach>
													<c:if test="${fn:length(qaobj.p4List)>0}">
														<div class="kbs-embed" name="p4area" style="display:none">
															<iframe frameborder="0" src="" style="width:100%;height:300px;"></iframe>
														</div>
													</c:if>
													<c:forEach items="${qaobj.attachmentList}" var="att">
														<a href="${att.url }" target="_blank">${att.name }</a>
														<a href="javascript:void(0)" name="navOpenAtt" _name="${att.name }" _id="${att.id }">[打开]</a> | 
														<a href="javascript:void(0)" name="navReviewAtt" _name="${att.name }" _id="${att.id }">[预览]</a>
														<br>
													</c:forEach>
													<c:if test="${fn:length(qaobj.attachmentList)>0}">
														<div class="kbs-embed" name="attarea" style="display:none">
															<iframe frameborder="0" src="" style="width:100%;height:300px;"></iframe>
														</div>
													</c:if>
													<c:forEach items="${qaobj.zdyw}" var="item">
														<span _question="${item }"><a href="javascript:void(0)" name="navRelQues">${item}</a> | <a href="javascript:void(0)" name="navRelAns">[相关答案]</a> | <a href="javascript:void(0)" name="navCateView">[分类展示]</a></span>
														<br>
													</c:forEach>
												</div>
											</td>
											<td width="10%"><a href="javascript:void(0);" name="btnOtherPlatform">其他平台</a></td>
											<td width="5%">
												<c:if test="${qaobj.isableSMS}">
													<input type="checkbox" name="sms">
												</c:if>
												&nbsp;
											</td>
										</tr> <!-- /tr -->
									</c:forEach>
								</table>
								
							</div>	<!-- /div 详情明细 -->
						</div>	<!-- /div y-tabs -->
					</div>	<!-- /div x-tabs -->
				</div>	<!-- /x-tabs -->
        		
        		
        		<!-- 搜索框 -->
				<div id="findbar" class="easyui-panel" title="" style="position:fixed;width:340px;top:5px;right:10px;padding:2px;">
					<input class="easyui-textbox kbs-global-finder-keyword" type="text">
					<a href="javascript:void(0)" class="easyui-linkbutton kbs-global-finder-btn">上一个</a>
					<a href="javascript:void(0)" class="easyui-linkbutton kbs-global-finder-btn">下一个</a>
					<span class="kbs-global-finder-total"></span>
				</div>
				
				<!-- Tab工具条 -->
				<div id="tab-tools" style="display:none;">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-goup'" id="btnGoup" title="回到顶部"></a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-archive'" id="btnArchive" title="归档" style="display:none;"></a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mail-send'" id="btnSendSms" title="发送短信"></a>
		        	<a href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-full-screen'" id="btnFullScreen" title="全屏显示"></a>
				</div>
				<!-- 右键菜单工具条 -->
				<div id="mm" class="easyui-menu" style="width:120px;">
		        	<div onclick="javascript:void(0)" id="menuErrCorrect">答案反馈</div>
		        	<div onclick="javascript:void(0)" id="menuFav">收藏</div>
		        	<div onclick="javascript:void(0)" id="menuCopy">复制</div>
		        	<div onclick="javascript:void(0)" id="menuRecommand">推荐</div>
		        	<div onclick="javascript:void(0)" style="display:none" id="menuSendSms">发送短信</div>
		        	<div onclick="javascript:void(0)" style="display:none" id="menuArchive">归档</div>
		        </div>
		        
        		
        	</c:when>
        	<c:otherwise>
        		<div class="easyui-panel kbs-panel-undefined" title="" style="width:100%;height:200px;padding:10px;">
			        <p class="kbs-title-weight">没有此业务<p>
			    </div>
        	</c:otherwise>
        </c:choose>
        
	</body>
</html>
