<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识库</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/individual/js/jquery.form.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/library/jquery/js/jquery.ajaxfileupload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jQuery.md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
<script type="text/javascript">

function selecttag(id,url){
   $("#tags li").removeClass();
   $("#"+id+"").addClass("selectTag");
   if(url!=""){
     $("#tagContent").css("display","none");
     $("#menuframe").css("display","block");
     $("#menuframe").attr("src",$.fn.getRootPath() +url);
   }else{
       $("#menuframe").css("display","none");
       $("#menuframe").attr("src","");
       $("#tagContent").css("display","block");
   }
}



function upload(){
   $("#imgload").click();
}

function reset(){
   /**
   $("#img_info").attr("src","${pageContext.request.contextPath}/resource/individual/images/individual/head_ico.jpg");
   $("#userChineseName_user").attr("value","");
   $("#sex_user").attr("value","");
   $("#birthday_user").attr("value","");
   $("#mobile_user").attr("value","");
   $("#email_user").attr("value","");
   $("#descContent_user").attr("value","");
   **/
   window.location.href=$.fn.getRootPath() +"/app/individual/individual!getUserInfo.htm";
}


function imginfo(self){
	var imgName =  $("#imgload").val();
	var  path1 = imgName.lastIndexOf("\\");
    var  name = imgName.substring(path1+1);
    var  type=name.substring(name.lastIndexOf("."));
	if (name==""){
	    parent.$(document).hint("请选择文件");
		return false;
	}else{
		if(name.toLocaleLowerCase().match("^.+\\.(jpg|png|bmp|ico|jpeg|gif)$")==null) {
			parent.$(document).hint("目前只支持jpg|png|bmp|ico|jpeg|gif等格式的图片");
			return false;
		}
	}
    var _url = $.fn.getRootPath() + '/app/individual/individual!upload.htm';
	$.ajaxFileUpload({
		url : _url,
		secureuri : false,
		fileElementId : "imgload",
		data : {"imgFieldName": "imgload", "imgName": name},
		dataType : 'json',
		timeout : 5000,
		success : function(data, status) {
			$("#img_info").attr("src",'${pageContext.request.contextPath}/upload/'+$("#id_user").val()+''+type+'?index='+Math.random()+'');
		}
	});
  $("#img_infos").attr("src",name);
}

function addOrEditUser(){
		var bl = true;
		var jobNumber_user =  $("#jobNumber_user").val();
		var userChineseName_user =  $("#userChineseName_user").val();
		var username_user =  $("#username_user").val();
		var stationId_deputy_user =  $("#stationId_deputy_user").val();
		var email_user =  $("#email_user").val();
		var mobile_user =  $("#mobile_user").val();	
		var errorMessage = $("#errorMessage");				
		if(jobNumber_user==''){
			parent.$(document).hint("工号不能为空!");
			return;
		}
		if(userChineseName_user==''){
			parent.$(document).hint("姓名不能为空!");
			return;
		}
		if(username_user==''){
			parent.$(document).hint("账号不能为空!");
			return;
		}		
		
		if(mobile_user!=''){//手机号码不为空验证格式
			var filter  = /^1[3|4|5|7|8][0-9]\d{4,8}$/;
			if (!filter.test(mobile_user)) {
			    parent.$(document).hint("手机号码不符合标准!")
				return;
			}
		}
		var name=$("#img_infos").attr("src");
		var  type=name.substring(name.lastIndexOf("."));
        var params=$("#form1").serialize();
		$.ajax({
			   		type: "POST",
			   		url : $.fn.getRootPath() + "/app/individual/individual!addOrEditUserDo.htm?"+params,
			   		data:{imgsrc:$("#img_infos").attr("src")},
			   		async: false,
			   		dataType : 'text',
			   		success: function(msg){
			   		     if(name!=""){
			   		       parent.$("#imgheadinfo").attr("src",'${pageContext.request.contextPath}/upload/'+$("#id_user").val()+''+type+'?index='+Math.random()+'');
			   		     }
			   		     parent.$(document).hint(msg);
			   		}
			   　});			
	}　

</script>

</head>

<body>

      <ul id=tags>
        <li id="userset" class="selectTag"><a href="javascript:void(0)" onclick="selecttag('userset','')">用户设置</a> </li>
        <li id="userpassword"><a href="javascript:void(0)" onclick="selecttag('userpassword','/app/auther/user!pwdEditTo.htm')">密码设置</a> </li>
      </ul>
       <iframe id="menuframe" src="" frameborder="0"  height="500px" width="100%" style="display: none"></iframe>
      <div id="tagContent">
	  
        <div class="tagContent selectTag" id="tagContent0">
          <div class="user_settings">
          <form  method="post" id="form1" enctype="multipart/form-data">
            <div class="user_settings_line c666 fw"><span>头像信息</span></div>
            <div class="user_settings_head">
            <s:if test="individualimg.img_name !=null">
               <img id="img_info" src="${pageContext.request.contextPath}/app/individual/individual!getImg.htm" />
            </s:if>
            <s:else>
               <img id="img_info" src="${pageContext.request.contextPath}/resource/individual/images/individual/head_ico.jpg" />
            </s:else> 
            <a class="user_settings_btn" href="javascript:void(0);" onclick="upload();">上传头像</a> </div>
            <img id="img_infos" src="" style="display: none"/>
            <input type="file"  id="imgload" name="imgload"  onchange="imginfo()" style="display: none"/>
            <div class="user_settings_line c666 fw mb10"><span>个人信息</span></div>
            <div class="user_settings_personal">
                <div class="user_settings_personalleft fl">
                  <ul>
                    <li>
                      <p>您的工号</p>
                      <input type="hidden" id="id_user" name="id_user" value="${userModel.id }"/>
                      <input name="jobNumber_user" id="jobNumber_user" type="text" value="${userModel.userInfo.jobNumber }" readonly="readonly"/>
                    </li>
                    <li>
                      <p>您的姓名</p>
                      <input name="userChineseName_user" id="userChineseName_user" type="text" value="${userModel.userInfo.userChineseName }" readonly="readonly"/>
                    </li>
                    <li>
                      <p>您的性别</p>
                      <s:select id="sex_user" name="sex_user" list="#{'1':'男','2':'女'}" headerKey="" headerValue="---请选择---"
									listKey="key" listValue="value" value="#request.userModel.userInfo.sex"></s:select>
                    </li>
                    <li>
                      <p>您的生日</p>
                      <input id="birthday_user"  name="birthday_user" onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly" class="Wdate"
									value='<s:date  name="userModel.userInfo.birthday" format="yyyy-MM-dd"/>' 
									type="text"  />
                    </li>
                    <li>
                      <p>您的电话</p>
                      <input name="mobile_user" id="mobile_user" type="text" value="${userModel.userInfo.mobile }"/>
                    </li>
                    <li>
                      <p>您的邮件</p>
                      <input name="email_user" id="email_user" type="text" value="${userModel.userInfo.email }"/>
                    </li>
                    <li>
                      <p>所在岗位</p>
                      <span style="padding-top:7px;position:absolute;font-size:12px;color:#666;">
                      <s:iterator value="#request.userModel.stationsStatus0" var="va1">
                      	<s:if test="#va1.id==#request.userModel.mainStationId">
                      		${va1.name }<br><span style="color:red;font-size:12px">如有错误请联系管理员！</span>
                      	</s:if>
                      </s:iterator>
                      </span>
                    </li>
                  </ul>
                </div>
                <div class="user_settings_personalright fl">
                  <p>个人描述</p>
                  <div class="pright">
                    <textarea cols="" rows="" id="descContent_user" name="descContent_user">${userModel.userInfo.descContent }</textarea>
                  </div>
                </div>
              </form>
            </div>
            <div class="clear"></div>
            <div class="user_settingsbtn">
              <label style="float: left;color: red;" id="errorMessage"></label>
              <input class="an2" type="button" onclick="reset();" value="重置" />
              <input class="an1" type="button" onclick="addOrEditUser()" value="确认" />
            </div>
          </div>
        </div>

</body>
</html>
