<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@page import="com.eastrobot.util.WorkflowUtils"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	boolean isOpenWorkflow = WorkflowUtils.isUsable();
	String isOpentask = PropertiesUtil.value("showpetask");
	pageContext.setAttribute("isOpentask", Boolean.valueOf(isOpentask));
	pageContext.setAttribute("isOpenWorkflow", isOpenWorkflow);
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识库</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css" />
		<script type="text/javascript">
			//add by eko.zhan at 2015-04-21
			var SystemKeys = parent.SystemKeys;
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/individual/js/individual.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
		<style>
			.content_content {
				border: none;
				border-top: 1px solid #dfdfdf;
			}
		</style>
		<script type="text/javascript">
			$(function(){
			    $(".individual_ico1").find(".individual_circle").css({"background":"#c22a2c","color":"#fff"});
			    $(".individual_ico1").find(".individual_circle1").css({"background":"#c22a2c","color":"#fff"});
			    $(".individual_ico1").css({"background":"url(${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico1hover.png) no-repeat top center","color":"#c22a2c"}); 
			    if($(".individual_left ul li").length<6){
			          $(".individual_left ul li").eq(1).find("a").click();
			          $("#tagContent").hide();
			    }else{
			       $(".tags1 li a").eq(0).click();  
			    }
			    $('body').click (function (e){
			      e = e || window.event;
			      if(e.target.id!="showchange1"){
			         $("#rationalize_select1").hide();
			      }
			    })
			   
			});

			//查看流程
			function workFlowLook(id, type, method){
					//mode 流程引擎启用版本(1新版本,默认就版本)
					//取消 mode
					if($.workflow.isUsable()){
						//modify by eko.zhan at 2015-04-21 15:00
						//var targeturl = '/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid=' + id;
						//$.workflow.open($.workflow.format(targeturl), parent.parent.$);
						$.workflow.editDocument(id, parent.$);
						/*
						var workflow_path  = '<%=PropertiesUtil.value("kbase.workflow.context_path")%>';
						var knowledgeCreate  = '<%=PropertiesUtil.value("knowledgeCreate")%>';
						
						var _url = workflow_path + "/j_acegi_security_check?j_username="+'${sessionScope.session_user_key.username}'+"&j_password=1&targeturl=" + workflow_path + "/workflow/request/workflow.jsp?reqtype=kbase&isclose=1&requestid=" + id;
						var _index = parent.parent.$.layer({
						    type: 2,
						    border: [10, 0.3, '#000'],
						    title: "合理化建议",
						    closeBtn: [0, true],
						    //shade: [0],
						    iframe: {src : _url},
						    area: ['1000px', '560px']
						});*/
					}else{
						
						$.ajax({
						    url:$.fn.getRootPath()+'/app/auther/work-flow!knowledgeCreateLook.htm?type='+type+'&method='+method,
							type:"post",
							data:{"id":id},
							timeout:5000,
							async:false,
							dataType:"html",
							success:function(data){
								if (data!=null&&data!="") {
								    $("#workFlowShadeCotent").html(data);
								    openShade("workFlowShade");
								 } else {
								    alert("网络错误，请稍后再试");
								 }
							},
							error:function(){
								 alert("网络错误,请稍后再试");
							}
						});	
					}
				}
			
			
			function showchange(){
			   $("#rationalize_select1").toggle();
			}
		</script>
	</head>
	
	<body>
		<div class="content_right_bottom">
			<div id="workFlowShadeCotent"></div>
			<div class="individual">
				<div class="individual_left">
					<ul>
						<li>
							<s:if test="individualimg.img_name !=null">
								<img id="imgheadinfo" src="${pageContext.request.contextPath}/app/individual/individual!getImg.htm" />
							</s:if>
							<s:else>
								<img id="imgheadinfo" src="${pageContext.request.contextPath}/resource/individual/images/individual/head_ico.jpg" />
							</s:else>
						</li>

						<!--是否显示（个人中心-待办事物，个人中心（待办数目），合理化建议）  -->
						<!-- 
				        	modify by eko.zhan at 2015-04-21 15:30
				        	待办事宜模块配置繁琐，简化后根据是否配置 kbase.workflow.context_path 来确定是否开启待办事宜，如果不为空则开启
				         -->
						<c:if test="${isOpenWorkflow }">
							<li>
								<a class="individual_ico1" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico1hover.png','flag','/app/individual/individual.htm')" href="javascript:void(0)">待办事务 
									<s:if test="workflowdata['workflow1count']>99">
										<div class="individual_circle">
											99+
										</div>
									</s:if>
									<s:elseif test="workflowdata['workflow1count']>0 && workflowdata['workflow1count']<=9">
										<div class="individual_circle1">
											<s:property value="workflowdata['workflow1count']" />
										</div>
									</s:elseif>
									<s:elseif test="workflowdata['workflow1count']>=10">
										<div class="individual_circle">
											<s:property value="workflowdata['workflow1count']" />
										</div>
									</s:elseif>
								</a>
							</li>
						</c:if>
						<c:if test="${isOpentask }">
						<li>
							<a class="individual_ico6" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico6hover.png','','/app/task/pe-task.htm')" href="#">我的任务 
								<s:if test="workflowdata['taskcount']>99">
										<div class="individual_circle">
											99+
										</div>
									</s:if>
									<s:elseif test="workflowdata['taskcount']>0 && workflowdata['taskcount']<=9">
										<div class="individual_circle1">
											<s:property value="workflowdata['taskcount']" />
										</div>
									</s:elseif>
									<s:elseif test="workflowdata['taskcount']>=10">
										<div class="individual_circle">
											<s:property value="workflowdata['taskcount']" />
										</div>
									</s:elseif>
							</a>
						</li>
						</c:if>
						<!-- 
				          <li><a class="individual_ico2" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico2hover.png','correction','/app/corrections/corrections!getcorrections.htm?status=1')" href="javascript:">知识反馈
				             
				            <s:if test="workflowdata['correctioncount']>99">
				               <div class="individual_circle">99+</div>
				            </s:if>
				            <s:if test="workflowdata['correctioncount']<=0">
				             
				            </s:if>
				            <s:if test="workflowdata['correctioncount']>0 && workflowdata['correctioncount']<=9">
				               <div class="individual_circle1"><s:property value="workflowdata['correctioncount']"/></div>
				            </s:if>
				            <s:if test="workflowdata['correctioncount']>0 && workflowdata['correctioncount']>=10">
				              <div class="individual_circle1"><s:property value="workflowdata['correctioncount']"/></div>
				            </s:if>
				            
				          </a></li>
				           -->
						<li>
							<a class="individual_ico2" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico2hover.png','','/app/recommend/recommend.htm?flag=owner')" href="#">推荐知识 
								<s:if test="workflowdata['recommendcount']>99">
									<div class="individual_circle">
										99+
									</div>
								</s:if>
								<s:elseif test="workflowdata['recommendcount']>0 && workflowdata['recommendcount']<=9">
									<div class="individual_circle1">
										<s:property value="workflowdata['recommendcount']" />
									</div>
								</s:elseif>
								<s:elseif test="workflowdata['recommendcount']>=10">
									<div class="individual_circle">
										<s:property value="workflowdata['recommendcount']" />
									</div>
								</s:elseif>
							</a>
						</li>
						<li>
							<a class="individual_ico5" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico5hover.png','','/app/notice/notice!list.htm','/app/individual/individual.htm')" href="#">公告列表 
								<s:if test="workflowdata['noticecount']>99">
									<div class="individual_circle">
										99+
									</div>
								</s:if>
								<s:elseif test="workflowdata['noticecount']>0 && workflowdata['noticecount']<=9">
									<div class="individual_circle1">
										<s:property value="workflowdata['noticecount']" />
									</div>
								</s:elseif>
								<s:elseif test="workflowdata['noticecount']>=10">
									<div class="individual_circle">
										<s:property value="workflowdata['noticecount']" />
									</div>
								</s:elseif>
							</a>
						</li>
						<li>
							<a class="individual_ico3" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico3hover.png','','/app/auther/index-setting.htm','/app/individual/individual.htm')" href="#">主题设置 </a>
						</li>
						<li>
							<a class="individual_ico4" onclick="menuselect(this,'${pageContext.request.contextPath}/resource/individual/images/individual/individual_ico4hover.png','','/app/individual/individual!getUserInfo.htm','/app/individual/individual.htm')" href="#">用户设置</a>
						</li>
					</ul>
				</div>
				<div class="individual_right">
					<c:if test="${isOpenWorkflow }">
						<ul id="tags" class="tags1">
							<!-- 设置对多显示8条数据 -->
							<s:if test="workflowdata['workflowindividual'].size<=9">
								<s:set name="nkSize" value="workflowdata['workflowindividual'].size - 1"></s:set>
							</s:if>
							<s:else>
								<s:set name="nkSize" value="8"></s:set>
							</s:else>
							<!-- 遍历显示的业务模块 -->
							<s:iterator value="workflowdata['workflowindividual']" var="va" begin="0" end="#nkSize" status="index">
								<s:if test="#index.index==0">
									<li class="selectTag">
										<a onclick="selectTag('${va.id}',this)" href="javascript:void(0)">
											<span>${va.name}</span>
											<s:if test="#va.count!=0">(${va.count})</s:if>
										</a>
									</li>
								</s:if>
								<s:else>
									<li>
										<a onclick="selectTag('${va.id}',this)" href="javascript:void(0)"><span>${va.name}</span> <s:if test="#va.count!=0">(${va.count})</s:if> </a>
									</li>
								</s:else>
							</s:iterator>
						</ul>
						<ul id=tags class="tags3" style="display: none">
							<li class="selectTag" id="waitAuditing">
								<a onclick="more_info('knowledgeCreate','waitAuditing','/app/auther/work-flow!workFlow.htm','tags3');" href="#">待我审核<span></span> </a>
							</li>
							<li id="myCreate">
								<a onclick="more_info('knowledgeCreate','myCreate','/app/auther/work-flow!workFlow.htm','tags3');" href="#">申请待审批<span></span> </a>
							</li>
							<li id="myCreate1">
								<a onclick="more_info('knowledgeCreate','myCreate1','/app/auther/work-flow!workFlow.htm','tags3');" href="#">申请已归档<span></span> </a>
							</li>
							<li id="concluded">
								<a onclick="more_info('knowledgeCreate','concluded','/app/auther/work-flow!workFlow.htm','tags3');" href="#">处理待审批<span></span> </a>
							</li>
							<li id="concluded1">
								<a onclick="more_info('knowledgeCreate','concluded1','/app/auther/work-flow!workFlow.htm','tags3');" href="#">处理已归档<span></span> </a>
							</li>
							<li id="individual_select">
								<div class="rationalize">
									<div class="rationalize_select">
										<div class="rationalize_select_left"></div>
										<div class="rationalize_select_center" onclick="showchange();">
											<p class="rationalize_select_ico">
												<a class="rationalize_select_btn" href="#" id="showchange1"></a>
											</p>
										</div>
										<div class="rationalize_select_right"></div>
										<div>
											<div class="rationalize_option" id="rationalize_select1" style="display: none">
												<ul>
													<s:if test="workflowdata['modulelist'].size<=9">
														<s:set name="Size" value="workflowdata['modulelist'].size - 1"></s:set>
													</s:if>
													<s:else>
														<s:set name="Size" value="8"></s:set>
													</s:else>
													<s:iterator value="workflowdata['modulelist']" var="va" begin="0" end="#Size" status="index">
														<li>
															<a href="#" onclick="selectchange('tags3','/app/auther/work-flow!workFlow.htm?type=knowledgeCreate&method=waitAuditing','${va.id }','${va.objname}')">${va.objname}</a>
														</li>
													</s:iterator>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</c:if>	<!-- end isOpenWorkflow -->
					<iframe id="menuframe" src="" frameborder="0" height="550px" width="100%" style="display: none"></iframe>
				</div>
			</div>
		</div>
	</body>
</html>