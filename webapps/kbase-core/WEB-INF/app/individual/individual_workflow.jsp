<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
   <div id="tagContent">
        
        <div class="tagContent selectTag" id="tagContent0">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="individual_table">
            <input type="hidden" id="module_id" value="${module_id }" /> 
            <tr>
              <td><div class="content_news1">
                  <div class="content_index_news_left"></div>
                  <div class="content_index_news_center">
                    <div class="content_news1_title"> <a href="#">待我审核</a> </div>
                    <div class="content_news1_con">
                      <ul>
                         <s:if test="workflowdata['workflow1'].size <= 4">
							<s:set name="nkSize" value="workflowdata['workflow1'].size - 1"></s:set>
						</s:if>
						<s:else>
							<s:set name="nkSize" value="3"></s:set>
						</s:else>
                         <s:iterator value="workflowdata['workflow1']" var="va" begin="0" end="#nkSize">
                                <li><img src="${pageContext.request.contextPath}/resource/individual/images/index_dian.jpg" /><a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')"><s:if test="#va.requestname.length() > 12"><s:property value="#va.requestname.substring(0,12) + '...'"/></s:if><s:else><s:property value="#va.requestname"/></s:else></a><span>${va.createdate}</span></li>
                         </s:iterator> 
                        <li class="content_news1_more">
                           <s:if test="workflowdata['workflow1'].isEmpty == false">
								<a onclick="more_info('knowledgeCreate','waitAuditing','/app/auther/work-flow!workFlow.htm','tags3');" href="#">更多</a>
							</s:if>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="content_index_news_right"></div>
                </div></td>
              <td><div class="content_news1">
                  <div class="content_index_news_left"></div>
                  <div class="content_index_news_center">
                    <div class="content_news1_title"> <a href="#">申请待审批</a> </div>
                    <div class="content_news1_con">
                      <ul>
                        <s:if test="workflowdata['workflow2'].size <= 4">
							<s:set name="Size2" value="workflowdata['workflow2'].size - 1"></s:set>
						</s:if>
						<s:else>
							<s:set name="Size2" value="3"></s:set>
						</s:else>
                        <s:iterator value="workflowdata['workflow2']" var="va" begin="0" end="#Size2">
                                <li><img src="${pageContext.request.contextPath}/resource/individual/images/index_dian.jpg" /><a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')"><s:if test="#va.requestname.length() > 12"><s:property value="#va.requestname.substring(0,12) + '...'"/></s:if><s:else><s:property value="#va.requestname"/></s:else></a><span>${va.createdate}</span></li>
                         </s:iterator>
                        <li class="content_news1_more">
                           <s:if test="workflowdata['workflow2'].isEmpty == false">
								<a onclick="more_info('knowledgeCreate','myCreate','/app/auther/work-flow!workFlow.htm','tags3');" href="#">更多</a>
							</s:if>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="content_index_news_right"></div>
                </div></td>
              <td><div class="content_news1">
                  <div class="content_index_news_left"></div>
                  <div class="content_index_news_center">
                    <div class="content_news1_title"> <a href="#">申请已归档</a> </div>
                    <div class="content_news1_con">
                      <ul>
                         <s:if test="workflowdata['workflow3'].size <= 4">
							<s:set name="Size3" value="workflowdata['workflow3'].size - 1"></s:set>
						</s:if>
						<s:else>
							<s:set name="Size3" value="3"></s:set>
						</s:else>
                        <s:iterator value="workflowdata['workflow3']" var="va" begin="0" end="#Size3">
                                <li><img src="${pageContext.request.contextPath}/resource/individual/images/index_dian.jpg" /><a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')"><s:if test="#va.requestname.length() > 12"><s:property value="#va.requestname.substring(0,12) + '...'"/></s:if><s:else><s:property value="#va.requestname"/></s:else></a><span>${va.createdate}</span></li>
                         </s:iterator>
                        <li class="content_news1_more">
                           <s:if test="workflowdata['workflow3'].isEmpty == false">
								<a onclick="more_info('knowledgeCreate','myCreate1','/app/auther/work-flow!workFlow.htm','tags3');" href="#">更多</a>
							</s:if>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="content_index_news_right"></div>
                  
                </div></td>
            </tr>
              
            <tr>
              <td><div class="content_news1">
                  <div class="content_index_news_left"></div>
                  <div class="content_index_news_center">
                    <div class="content_news1_title"> <a href="#">处理待审批</a> </div>
                    <div class="content_news1_con">
                      <ul>
                         <s:if test="workflowdata['workflow4'].size <= 4">
							<s:set name="Size4" value="workflowdata['workflow4'].size - 1"></s:set>
						</s:if>
						<s:else>
							<s:set name="Size4" value="3"></s:set>
						</s:else>
                        <s:iterator value="workflowdata['workflow4']" var="va" begin="0" end="#Size4">
                                <li><img src="${pageContext.request.contextPath}/resource/individual/images/index_dian.jpg" /><a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')"><s:if test="#va.requestname.length() > 12"><s:property value="#va.requestname.substring(0,12) + '...'"/></s:if><s:else><s:property value="#va.requestname"/></s:else></a><span>${va.createdate}</span></li>
                         </s:iterator>
                         
                        <li class="content_news1_more">
                           <s:if test="workflowdata['workflow4'].isEmpty == false">
								<a onclick="more_info('knowledgeCreate','concluded','/app/auther/work-flow!workFlow.htm','tags3');" href="#">更多</a>
							</s:if>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="content_index_news_right"></div>
                </div></td>
              <td><div class="content_news1">
                  <div class="content_index_news_left"></div>
                  <div class="content_index_news_center">
                    <div class="content_news1_title"> <a href="#">处理已归档</a> </div>
                    <div class="content_news1_con">
                      <ul>
                        <s:if test="workflowdata['workflow5'].size <= 4">
							<s:set name="Size5" value="workflowdata['workflow5'].size - 1"></s:set>
						</s:if>
						<s:else>
							<s:set name="Size5" value="3"></s:set>
						</s:else>
                        <s:iterator value="workflowdata['workflow5']" var="va" begin="0" end="#Size5">
                                <li><img src="${pageContext.request.contextPath}/resource/individual/images/index_dian.jpg" /><a href="#" onclick="workFlowLook('${va.id }','${type }','${method }')"><s:if test="#va.requestname.length() > 12"><s:property value="#va.requestname.substring(0,12) + '...'"/></s:if><s:else><s:property value="#va.requestname"/></s:else></a><span>${va.createdate}</span></li>
                         </s:iterator>
                        <li class="content_news1_more">
                           <s:if test="workflowdata['workflow5'].isEmpty == false">
								<a onclick="more_info('knowledgeCreate','concluded1','/app/auther/work-flow!workFlow.htm','tags3');" href="#">更多</a>
							</s:if>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="content_index_news_right"></div>
                </div></td>
              <td>&nbsp;</td>
            </tr>
            
          </table>
        </DIV>
      </div>


