<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
/**
 * 命中知识详情
 */
%>
<%
	/*业务模板功能开启——请求转向*/
	if(request.getAttribute("articleId") != null){/*是业务模板的数据*/
		String articleId = request.getAttribute("articleId").toString();
		if (PropertiesUtil.valueToBoolean("bizTpl.enable") 
				&& StringUtils.isNotBlank(articleId)) {
			String faqId = request.getParameter("faqId");/*标准问id*/
			String isUnMatch = request.getAttribute("isUnMatch").toString();
			if (StringUtils.isNotBlank(articleId) && isUnMatch.equals("false")) {
				String targetUrl = "/app/biztpl/article-search.htm?&articleId="
						+ articleId;
				if (StringUtils.isNotBlank(faqId)) /*锚点位置*/
					targetUrl += "&spotId=" + faqId;
				response.sendRedirect(request.getContextPath() + targetUrl);
			}
		}
	}
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识展示-详情</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/wukong/wukong-luna.css">
		<style type="text/css">
			.kbs-cate{
				font-size: 16px;
				font-weight: bolder;
				color: #35bdb2;
			}
			.btn{
				padding: 4px 6px;
				border: 1px solid #fff;
				cursor: pointer;
				color: #fff;
			}
			.btn:hover{
				border: 1px solid #000;
			}
			.btn-red{
				background-color: #fe6262;
			}
			.btn-yellow{
				background-color: #ff971d;
			}
			.btn-green{
				background-color: #35bdb2;
			}
			.kbs-row-btn{
				border-bottom: 1px dotted #D4D1D1;
				padding: 6px 0;
			}
			.kbs-row-btn .kbs-office{
				float: right;
			}
			.kbs-nav td{
				line-height:30px;
				border-bottom: 1px dotted #D4D1D1;
				padding-left: 10px;
			}
			.kbs-row-question, .kbs-answer-helios{
				padding-left: 10px;
				color: #000;
			}
			.kbs-answer-helios{
				background-color: #fff;
			}
			ul, li{
				list-style: outside none none; 
			}
			.kbs-tabs{
				border-bottom: 1px solid #ddd;
			}
			
			.kbs-tabs tr th{
				border: 1px solid #eee;
				padding: 10px;
				cursor: pointer;
			}
			.kbs-tabs tr th:hover{
				background: #eee;
			}
			.kbs-tabs tr th.checked{
				color: #ff0000;
				border-bottom: 1px solid #fff;
			}
			nav-tabs-panel{
				
			}
			ul.list-group{
				margin: 0;
				padding-left: 0px;
			}
			.list-group-item{
				margin: 6px 0;
				padding-left: 3px;
			}
			.list-group-item:hover{
				color: #ff0000;
			}
			.kbs-att-title, .kbs-p4-title{
				width:27em; 
				white-space:nowrap; 
				text-overflow:ellipsis; 
				-o-text-overflow:ellipsis; 
				overflow:hidden;
			}
			
		</style>
		
		<jsp:include page="/resource/wukong/head.jsp" flush="true" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/SearchJump.js"></script>
		
		<!--<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FaultTree.js"></script>
		
		--><script type="text/javascript">
			//window.faqId = pageContext.question_id;
		
			$(function(){
				//刷新前台保存维护的实例对比信息
				refreshObjectComparison();
				//搜索结果页面下的a标签都应该用一个新的标签页打开
				$('.kbs-answer a[href^="http"],.kbs-answer-helios a[href^="http"]').attr('target', '_blank');
				
				$(parent).scrollTop(0);
				
				//Tab
				$('.nav-tabs-panel').hide();
				$('.nav-tabs-panel:first').show();
				//Tab切换
				$('.kbs-tabs tr th').hover(function(){
					$('.kbs-tabs tr th').removeClass('checked');
					$(this).addClass('checked');
					
					//当前是第几个 li ？
					var _ind = $(this).index();
					$('.nav-tabs-panel').hide();
					$($('.nav-tabs-panel').get(_ind)).show();
					
				}, function(){});
				
				//单击 预览 触发
				$('input[type="checkbox"][name="preview"]').click(function(){
					if ($(this).attr('checked')!=undefined){
						$('.kbs-row-answer').show();
					}else{
						$('.kbs-row-answer').hide();
					}
					//刷新iframe高度
					parent.setCurrentIframeHeight($(parent.document).find('.content_content iframe:visible'));
				});
				
				//单击 详情 触发
				$('a[name="kbs-detail"]').click(function(){
					var _id = $(this).attr('_id');
					layer.load('加载中，请稍候...');
					location.href = pageContext.contextPath + '/app/wukong/search!detail.htm?id=' + _id;
				});
				
				//单击标题展开答案预览
				$('.kbs-row-question').click(function(){
					var $row = $(this).parent('tr').next('tr');
					if ($row.length>0){
						if ($row.get(0).style.display=='none'){
							$row.show();
						}else{
							$row.hide();
						}
					}
				});
				
				//签读 倒计时
				if ($('#btnMarkread').length>0){
					var desc = '签读';
					var limit = 20;
					
					var countdownTimer = window.setInterval(function(){
						limit--;
						$('#btnMarkread').text(desc + ' ' + limit);
						if (limit<1){
							$('#btnMarkread').text(desc);
							window.clearInterval(countdownTimer);
						}
					}, 1000);
				}
				
				//单击签读
				$('#btnMarkread').click(function(){
					var _this = this;
					if ($(_this).text()=='签读'){	//用中文不太好的样纸
						//签读倒计时完毕后才阔以执行markread
						var param = {
							"valueId": pageContext.question_id,
							"objectId": pageContext.object_id,
							"categoryId": pageContext.cateid,
							"catePath": pageContext.catePath
						};
						$.post('${pageContext.request.contextPath}/app/search/search!markreadObject.htm', param, function(data){
							if (data==1){
								alert('签读成功');
								$(_this).text('已读');
							}
						}, 'text');
					}
				});

				//收藏实例
				$('#btnFavObject').click(function(){
					var url = '${pageContext.request.contextPath}/app/fav/fav-clip-object!pick.htm?objid=' + pageContext.object_id + '&cataid=' + pageContext.cateid;
					pageContext.open({url: url});
				});

				//收藏标准问知识
				$('#btnFavQa').click(function(){
					var url = '${pageContext.request.contextPath}/app/fav/fav-clip-object!pick.htm?faqid=' + pageContext.question_id + '&cataid=' + pageContext.cateid;
					pageContext.open({url: url});
				});


				//推荐
				$('#btnRecommend').click(function(){
					var url = '${pageContext.request.contextPath}/app/recommend/recommend!open.htm?cateid=' + pageContext.cateid + '&valueid=' + pageContext.question_id;
					pageContext.open({
						url: url, 
						width: '800px',
						height: '600px'
					})
					
				});

				//纠错
				$("#btnErrcorrect").click(function (){
					var url = '${pageContext.request.contextPath}/app/corrections/corrections!send.htm?objId='+pageContext.object_id+'&faqId='+pageContext.question_id+'&cateId='+pageContext.cateid;
					pageContext.open({
						url: url, 
						width: '840px',
						height: '600px'
					})
				});
				
				//故障树
				$('#btnFaultTree').click(function(){
					parent.TABOBJECT.open({
						id : '故障树-${faq.question}',
						name : '故障树-${faq.question}',
						title : '故障树-${faq.question}',
						hasClose : true,
						url : '${pageContext.request.contextPath}/app/search/fault-graph!read.htm?questionId=${faq.question_id}',
						isRefresh : true
					}, this);
				});


				//选中对比
				$("#btnCompareOnto").click(function(){

					var objIds = new Array();

					//添加知识实例id
					objIds.push(pageContext.object_id);
					
					$("input:checkbox[name='objnameCheck']:checked").each(function(){
						objIds.push($(this).attr('_objid'));
					});

					if(objIds.length < 2){
						parent.$(document).hint("至少要选择一个实例");
						return false;
					}else if(objIds.length > 5){
						parent.$(document).hint("最多只能选择四个实例");
						return false;
					}else{
						parent.TABOBJECT.open({
							id : 'sldb',
							name : '实例对比',
							hasClose : true,
							url : '${pageContext.request.contextPath}/app/comparison/business-contrast.htm?oids='+objIds,
							isRefresh : true
						}, this);
					}
				});


				//添加文章
				$("#btnAddOnto").click(function(){
					var url = '${pageContext.request.contextPath}/app/wukong/object-compare!view.htm?objId=' + pageContext.object_id;
					
					pageContext.open({
						url: url,
						width: '810px',
						height: '445px'
					});
					return false;
					
				});


				//实例关联，实例对比下的--实例点击事件
				//$('.objname').click(function(){
				$('#objectRelatedPanel').on('click', '.list-group-item', function(){
					var objId = $(this).attr("_objid");
					var objectName = $(this).text(); 
					objectName = $.trim(objectName);
					openObject(objId,objectName);
				});
				

				//知识关联--知识点击事件
				$('#valueRelatedPanel .list-group-item').click(function(){
					//alert($(this).text());
					
					var _question = $(this).text();
					var _url = '${pageContext.request.contextPath}/app/wukong/search!detail.htm?question=' + encodeURI(_question);
					parent.TABOBJECT.open({
						id : _question,
						name : _question,
						title : _question,
						hasClose : true,
						url : _url,
						isRefresh : true
					}, this);
					
				});


				//原文--打开原文弹框
				$("#btnDocList").click(function(){
					var title = "原文";
					var url = "${pageContext.request.contextPath}/app/wukong/search!docList.htm?objId=" + pageContext.object_id;
					pageContext.open({url: url});
				});

				//版本对比
				$("#btnVersionCheck").click(function(){
					var url = "${pageContext.request.contextPath}/app/wukong/version-compare!view.htm?objId=" + pageContext.object_id;
					pageContext.open({url: url, width: '600px'});
				});
				
				
				//实例对比-打开实例
				$('#objectComparePanel').on('click', '.list-group-item span', function(){
					openObject($(this).attr('_objid'), $(this).text());
				});
				
			});
			
			
			
			/**
			 * 获取对比实例信息
			 * disposeDataFuns：数据处理函数数组
			 */
			function refreshObjectComparison(){
				$.ajax({
					url : $.fn.getRootPath() + "/app/search/object-search!getObjectComparison.htm",
					type : 'POST',
					data : {
					    objectId : pageContext.object_id
					},
					dataType:'json',
					success : function(data) {
						if(data && data.success){
							showCmpareOnto(data.data);
						}
					}
				});
			}
			
			
			/**
			 * 显示实例对比数据
			 * @param datas json 数组对象
			 * @author baidengke
			 * @date 2016年3月8日
			 */
			function showCmpareOnto(datas){
				if(!datas || datas.length == 0){
					return;
				}
				var showCmpareOntoDiv = $("#showCmpareOntoDiv");
				showCmpareOntoDiv.html('');
				for (var i = 0; i < datas.length; i++) {
					var row = datas[i];
					var saveInManager = row.saveInManager;
					if(saveInManager && saveInManager == 1){//后台数据跳过
						continue;
					}
					var liObj = $('<li class="list-group-item" style="cursor:pointer;">' 
					+ '<input type=\"checkbox\" _objid="' + row.compareObjectId + '" name="objnameCheck">&nbsp;' 
					+ '<span _objid="' + row.compareObjectId + '" class="objname">' + row.compareObjectName + '</span>'
					+ '</li>');
					
					$(liObj).children('span').click(function(){
						openObject(row.compareObjectId,row.compareObjectName);
					});
					
					showCmpareOntoDiv.append(liObj);
				}
			}
			
			/**
				open打开实例详情信息。
				新开一个tab打开实例详情页面
			*/
			function openObject(objId, objectName){
				var _url = '${pageContext.request.contextPath}/app/search/object-search.htm?searchMode=8&objId='+objId;
				parent.TABOBJECT.open({
					id : objectName,
					name : objectName,
					title : objectName,
					hasClose : true,
					url : _url,
					isRefresh : true
				}, this);
			}
		</script>
	</head>
	
	<body>
		<table class="" width="100%" cellpadding="1" cellspacing="1">
			<colgroup>
				<col width="75%"/>
				<col width="25%"/>
			</colgroup>
			<tr>
				<td>
					<c:if test="${empty faq }">
						<span class="kbs-cate">${msg }</span>
					</c:if>
					<c:if test="${not empty faq }">
						<span class="kbs-cate">${faq.bh_name } > ${faq.object_name }</span>
					</c:if>
				</td>
				<td align="right">
					<button class="btn btn-red" id="btnFavObject">收藏文章</button>
					<button class="btn btn-yellow" id="btnVersionCheck">版本对比</button>
					<button class="btn btn-green" id="btnDocList">原文</button>
				</td>
			</tr>
			<tr>
				<td>
					<table class="kbs-nav" width="100%" cellpadding="0" cellspacing="0">
						<colgroup>
							<col width="25%"/>
							<col width="25%"/>
							<col width="25%"/>
							<col width="25%"/>
						</colgroup>
						<tr>
							<td align="right" class="kbs-row-btn" colspan="4">
								<span class="kbs-office kbs-office-btn kbs-office-correct" id="btnErrcorrect">纠错</span>
								<span class="kbs-office kbs-office-btn kbs-office-resolve" id="btnRecommend">推荐</span>
								<span class="kbs-office kbs-office-btn kbs-office-fav" id="btnFavQa">收藏</span>
								<kbs:if key="faultTree">
									<c:if test="${requestScope.faultTreeCount>0 }">
										<span class="kbs-office kbs-office-btn kbs-office-eflow-read" id="btnFaultTree">故障树</span>
									</c:if>
								</kbs:if>
								<c:choose>
									<c:when test="${requestScope.isRead}">
										<span class="kbs-office kbs-office-btn kbs-office-markread" style="display:block;">已读</span>
									</c:when>
									<c:otherwise>
										<span class="kbs-office kbs-office-btn kbs-office-markread" id="btnMarkread" style="display:block;">签读 20</span>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<td class="kbs-question" colspan="4">${faq.question}</td>
						</tr>
						<tr>
							<td class="kbs-answer" style="padding-left: 12px;" colspan="4">${faq.answer}</td>
						</tr>
						
						<%-- 附件和P4数据 --%>
						<jsp:include page="attachment-plugin.jsp"></jsp:include>
						
					</table>
				</td>
				<td rowspan="2" valign="top">
					
					
					<table class="kbs-tabs" width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<th width="33.3%" class="checked">知识关联</th>
							<th width="33.4%">实例对比</th>
							<th width="33.3%">实例关联</th>
						</tr>
						<tr>
							<td colspan="3">
								<div class="nav-tabs-panel" id="valueRelatedPanel">
									<ul class="list-group">
										<c:forEach items="${requestScope.relatedQuestions}" var="item">
											<li class="list-group-item" style="cursor:pointer;" content="${item}">${item }</li>
										</c:forEach>
									</ul>
								</div>
								<div class="nav-tabs-panel" id="objectComparePanel" style="display:none;">
									<ul class="list-group">
										<c:forEach items="${requestScope.cmpareOnto}" var="item">
											<li class="list-group-item" style="cursor:pointer;">
												<input type="checkbox" _objid="${item.objectId }" name="objnameCheck">&nbsp;<span _objid="${item.objectId }" class="objname">${item.name }</span>
											</li>
										</c:forEach>
										<!--  kbase-core中维护添加的实例对比信息  s-->
										<div id="showCmpareOntoDiv">
										</div>
										<li class="list-group-item text-center">
											<button class="btn btn-info" id="btnCompareOnto">选中对比</button>
											<button class="btn btn-info" id="btnAddOnto">添加文章</button>
										</li>
									</ul>
								</div>
								<div class="nav-tabs-panel" id="objectRelatedPanel" style="display:none;">
									<ul class="list-group">
										<c:forEach items="${requestScope.relationOnto}" var="item">
											<li class="list-group-item objname" style="cursor:pointer;" _objid="${item.objectId }">
												${item.name }
											</li>
										</c:forEach>
									</ul>
								</div>
							</td>
						</tr>
					</table>
					
				</td>
			</tr>
			<tr>
				<td>
					<table class="kbs-nav" width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td colspan="2" align="right">
								<input type="checkbox" name="preview"> 预览
							</td>
						</tr>
						<c:forEach items="${qaList}" var="item">
							<c:if test="${item.question_id!=param.id}">
								<tr>
									<td width="90%" class="kbs-row-question">${item.question }</td>
									<td width="10%" align="center">
										<a href="javascript:void(0)" _id="${item.question_id }" class="kbs-detail" name="kbs-detail">详情</a>
									</td>
								</tr>
								<tr style="display:none" class="kbs-row-answer">
									<td colspan="2" class="kbs-answer-helios">${item.answer}</td>
								</tr>
							</c:if>
						</c:forEach>
						
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>