<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%--  
	@author eko.zhan
	@date 2016-06-08 09-45
	附件和P4界面插件，内嵌在 jsp 页面中使用
	
	格式固定：
	
	<table>
		<tr>
			<td colspan="2"></td>
			<td colspan="2"></td>
		</tr>
	</table>
	
 --%>
<c:if test="${fn:length(p4s)>0 or fn:length(atts)>0}">
<tr class="kbs-intro-att">
	<td colspan="4">
		<table width="100%" cellpadding="0" cellspacing="0">

	<%-- 遍历附件开始 --%>
	<c:forEach items="${atts}" var="item" varStatus="status">
		<c:choose>
			<c:when test="${status.index%2==0}">
				<tr>
					<td class="kbs-detail" colspan="2">
						<div class="kbs-office ${item.icon }"></div>
						<span class="kbs-att-title" _filename="${item.filename }" style="width:20em;">${item.title }</span>
						<a style="color:red;" href="javascript:void(0);" _addr="${item.addr }" _title="${item.title }">(下载)</a>
					</td>
			</c:when>
			<c:otherwise>
					<td class="kbs-detail" colspan="2">
						<div class="kbs-office ${item.icon }"></div>
						<span class="kbs-att-title" _filename="${item.filename }" style="width:20em;">${item.title }</span>
						<a style="color:red;" href="javascript:void(0);" _addr="${item.addr }" _title="${item.title }">(下载)</a>
					</td>
				</tr>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<c:if test="${fn:length(atts)%2==1}">
				<td class="kbs-detail" colspan="2">
					&nbsp;
				</td>
			</tr>
	</c:if>
	<%-- 遍历附件结束 --%>
	<%-- 遍历p4开始 --%>
	<c:forEach items="${p4s}" var="item" varStatus="status">
		<c:choose>
			<c:when test="${status.index%2==0}">
				<tr>
					<td class="kbs-detail" colspan="2">
						<div class="kbs-office ${item.icon }"></div>
						<span class="kbs-p4-title" _addr="${item.addr }">[${item.title }]</span>
					</td>
			</c:when>
			<c:otherwise>
					<td class="kbs-detail" colspan="2">
						<div class="kbs-office ${item.icon }"></div>
						<span class="kbs-p4-title" _addr="${item.addr }">[${item.title }]</span>
					</td>
				</tr>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<c:if test="${fn:length(p4s)%2==1}">
				<td class="kbs-detail" colspan="2">
					&nbsp;
				</td>
			</tr>
	</c:if>
	<%-- 遍历p4结束 --%>
	
			</table>
		</td>
	</tr>
</c:if>
<script type="text/javascript">
	//附件预览
	$('.kbs-att-title').click(function(){
		var _filename = $(this).attr('_filename');
		pageContext.preview(_filename);
	});
	
	//附件下载
	$('.kbs-att-title+a').click(function(){
		var _addr = $(this).attr('_addr');
		var _title = $(this).attr('_title');
		
		pageContext.download(_addr, _title);
	});
	
	//P4预览
	$('.kbs-p4-title').click(function(){
		var _addr = $(this).attr('_addr');
		window.open(_addr);
	});
</script>