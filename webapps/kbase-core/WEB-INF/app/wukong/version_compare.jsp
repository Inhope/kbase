<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>版本对比</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/wukong/wukong-luna.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/theme/table-form.css">
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/laypage/laypage.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/base.js"></script>
		<script type="text/javascript">
			pageContext = {
				contextPath: '${pageContext.request.contextPath }',
				converterPath: '${requestScope.converterPath}',
				
				preview: function(filename){
					parent.$.layer({
						type: 2,
						title: false,
						shade: [0.3, '#000'],
						border: [0],
						area: ['1000px', '500px'],
						iframe: {src: pageContext.converterPath + '/attachment/preview.do?fileName=' + filename}
					});
				}
			}
		</script>
		
		<script type="text/javascript">
			$(function(){
				$('#btnEnable').click(function(){
					var _checked = $('input[type="checkbox"][_isenable="1"]:checked');	//1-屏蔽 0-公开
					
					
					var _ids = [];
					$(_checked).each(function(i, item){
						_ids.push($(item).attr('_id'));
					});
					
					
					if (_ids.length==0){
						alert('请选择待公开的项');
						return false;
					}
					
				
					var param = {
						objectId : '${param.objId}',
						versionIds: _ids.join(','),
						flag: 0	//公开
					}
					
					
					$.post('${pageContext.request.contextPath}/app/search/search!maskObject.htm', param, function(data){
						location.reload();
					}, 'json');
					
				});
				
				$('#btnDisable').click(function(){
					var _checked = $('input[type="checkbox"][_isenable="0"]:checked');	//1-屏蔽 0-公开
					
					
					var _ids = [];
					$(_checked).each(function(i, item){
						_ids.push($(item).attr('_id'));
					});
					
					if (_ids.length==0){
						alert('请选择待屏蔽的项');
						return false;
					}
					
				
					var param = {
						objectId : '${param.objId}',
						versionIds: _ids.join(','),
						flag: 1	//屏蔽
					}
					
					
					$.post('${pageContext.request.contextPath}/app/search/search!maskObject.htm', param, function(data){
						location.reload();
					}, 'json');
					
				});
				
				
				$('#btnCompare').click(function(){
					var _checked = $('input[type="checkbox"][_isenable="0"]:checked');	//1-屏蔽 0-公开
					
					var _ids = [];
					$(_checked).each(function(i, item){
						_ids.push($(item).attr('_id') + ';' + + $(item).attr('_index'));
					});
					
					if (_ids.length!=2){
						alert('请选择两项未屏蔽的版本对比');
						return false;
					}
					
					var url = '${pageContext.request.contextPath}/app/search/object-search!comparisonObject.htm?objId=${param.objId}&versionIds=' + _ids.join(',');
					
					parent.TABOBJECT.open({
	   					id : 'versionCompare',
	   					name : '版本对比',
	   					hasClose : true,
	   					url : url,
	   					isRefresh : true
	   				}, this);
	   				
	   				parent.layer.close(parent.__kbs_layer_index);
				});
			});
		</script>
	</head>

	<body>
		<table class="box" width="100%">
			<colgroup>
				<col width="5%"/>
				<col width="15%"/>
				<col width="30%"/>
				<col width="50%"/>
			</colgroup>
			<tr>
				<th>&nbsp;</th>
				<th>版本号</th>
				<th>版本变更时间</th>
				<th>版本变更原因</th>
			</tr>
			<c:set var="listSize" value="${fn:length(list)}"></c:set>
			<c:forEach items="${requestScope.list}" var="versionCompare" varStatus="status">
				<tr>
					<td>
						<input type="checkbox" _id="${versionCompare.revisionId }" _isenable="${versionCompare.status }" _index="${listSize-status.index }" />
					</td>
					<td>
						${listSize-status.index }
						<c:if test="${status.index==0}"> (当前)</c:if>
						<c:if test="${versionCompare.status=='1'}"> (屏蔽)</c:if>
					</td>
					<td><fmt:formatDate value="${versionCompare.commitTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td>${versionCompare.message }</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="4" align="right">
					<input type="button" value="公开" id="btnEnable">
					<input type="button" value="屏蔽" id="btnDisable">
					<input type="button" value="对比" id="btnCompare">
				</td>
			</tr>
		</table>
	</body>
</html>