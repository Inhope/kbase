<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>原文</title>

<link href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" language="javascript">

		pageContext = {
				objId: '${requestScope.objId}',
				searchCtxPath:'${requestScope.searchCtxPath}'
		}

		var setting = {
			 view: {                                  
				dblClickExpand: false,
				showLine: true,
				selectedMulti: false
			 },
			 async: {
					enable: true,
					url: "${pageContext.request.contextPath}/app/search/attachment!getNode.htm",
					autoParam: ["id"]
			 },                        
			 data: {                                  
				  simpleData: {   
					   enable: true,
					   idKey: "id",  
					   pIdKey: "pId",
					   rootPId: 0   
				  }  
			 },                         
			 callback: {    
				  beforeClick: zTreeBeforeClick                               
			 }
		};
		function zTreeBeforeClick(treeId, treeNode, clickFlag) {
			if(treeNode.level==1){
				try{ 
					var fileName = treeNode.name;
					if(fileName){
						fileName = fileName.replace(/\(.*\)/,"");
					}
		            var elemIF = document.createElement("iframe");   
		            elemIF.src = '${pageContext.request.contextPath}/app/wukong/search!download.htm?fileId=' + treeNode.id + '&fileName=' + fileName;   
		            elemIF.style.display = "none";   
		            document.body.appendChild(elemIF);   
		        }catch(e){ 
		        	alert('文件异常，下载失败' + e);
		        } 
		        
// 				window.open(pageContext.searchCtxPath+"attachmentDown?attachmentId="+treeNode.id);
			}
		};
		
		$(document).ready(function(){
			$.post(
				"${pageContext.request.contextPath}/app/search/attachment!getNode.htm", 
				{"objId": pageContext.objId}, 
				function(data) {
					if(!data || data.length == 0){
						$("#notDocs").show();
					}else{
						$("#notDocs").hide();
		    			var zTreeDemo = $.fn.zTree.init($("#attachTree"),setting, data);
					}
 				},
 				"json"
	 		);
		});
</script>  
</head>
<body>
	<ul id="attachTree" class="ztree"></ul> 
	<div id="notDocs" >没有原文</div>
</body>
</html>