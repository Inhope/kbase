<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%--  
	@author eko.zhan
	@date 2016-04-14 14:25
	搜索界面插件，内嵌在 jsp 页面中使用
 --%>

<div class="kbs-search-result">
	搜索 
	<span class="kbs-i">${param.keyword }</span> 
	<span class="kbs-i-checked kbs-param-cateid" style="display:none;"></span>
	<span class="kbs-i-checked kbs-param-aging" style="display:none;"></span>
	<span class="kbs-i-checked kbs-param-interval" style="display:none;"></span>
	<span class="kbs-i-checked kbs-param-location" style="display:none;"></span>
	<span class="kbs-i-checked kbs-param-brand" style="display:none;"></span>
	<span class="kbs-i-checked kbs-param-prop" style="display:none;"></span>
	找到 <span class="kbs-i kbs-total-hit">${totalHits }</span> 条记录，
	本次搜索耗时 <span class="kbs-i kbs-total-elapsed">0</span> 秒
</div>
<!-- 收起筛选/显示筛选 -->
<div class="kbs-btn-right" id="btnSearchCondition"><span class="kbs-item">收起筛选</span><span class="kbs-icon kbs-icon-up kbs-text-right"></span></div><div class="kbs-clear"></div>

<!-- 搜索条件 -->
<table id="searchConditionPanel" class="kbs-nav" cellpadding="1" cellspacing="1" style="margin-top:10px;">
	<colgroup>
		<col width="10%"></col>
		<col width="90%"></col>
	</colgroup>
	<tr>
		<td class="kbs-detail">搜索方式：</td>
		<td class="kbs-detail" id="modeOpts">
			<span class="kbs-item-checked" _isfuzzy="1">模糊匹配</span>
			<span class="kbs-item" _isfuzzy="0">精确搜索</span>
		</td>
	</tr>
	<tr>
		<td class="kbs-detail">搜索范围：</td>
		<td class="kbs-detail" id="rangeOpts">
			<span class="kbs-item-checked" _rangetype="0">
				<span class="kbs-text-left">标准问答知识</span>
				<span id="memoQa" class="kbs-icon kbs-icon-help"></span>
			</span>
			<%-- 暂时隐藏
			<span class="kbs-item">
				<span class="kbs-text-left">扩展问知识</span>
				<span id="memoObj" class="kbs-icon kbs-icon-help"></span>
			</span>--%>
			<span class="kbs-item" _rangetype="1">
				<span class="kbs-text-left">附件文档知识</span>
				<span id="memoAtt" class="kbs-icon kbs-icon-help"></span>
			</span>
		</td>
	</tr>
	<tr style="display:none;">
		<td class="kbs-detail">筛选条件：</td>
		<td class="kbs-detail kbs-condition" id="conditionOpts">
			<span class="kbs-item kbs-item-first" id="kbValCate">
				<span class="kbs-text-left">知识目录</span>
				<span class="kbs-icon kbs-icon-down"></span>
			</span>
			
			<span class="kbs-item" id="kbValAging">
				<span class="kbs-text-left">知识时效</span>
				<span class="kbs-icon kbs-icon-down"></span>
			</span>
			
			<span class="kbs-item" id="kbValInterval">
				<span class="kbs-text-left">创建日期</span>
				<span class="kbs-icon kbs-icon-down"></span>
			</span>
			
			<c:if test="${requestScope.levelArr!=null}">
				<span class="kbs-item" id="kbValProp">
					<span class="kbs-text-left">文章属性</span>
					<span class="kbs-icon kbs-icon-down"></span>
				</span>
			</c:if>
			
			<c:if test="${locationAbled }">
				<span class="kbs-item" id="location">
					<span class="kbs-text-left">归属地</span>
					<span class="kbs-icon kbs-icon-down"></span>
				</span>
			</c:if>
			<c:if test="${brandAbled }">
				<span class="kbs-item" id="brand">
					<span class="kbs-text-left">品牌</span>
					<span class="kbs-icon kbs-icon-down"></span>
				</span>
			</c:if>
		</td>
	</tr>
</table>

<!-- 相关问 -->
<%--
<table class="kbs-nav" cellpadding="1" cellspacing="1" style="margin-top:10px;">
	<tr>
		<td class="kbs-detail" width="10%">
			您是不是想找：
		</td>
		<td class="kbs-detail" width="90%">
			<span class="kbs-item">全球通套餐</span>
			<span class="kbs-item">全球通积分兑换</span>
			<span class="kbs-item">全球通58元套餐简介</span>
			<span class="kbs-item">中国移动网上营业厅</span>
		</td>
	</tr>
</table>
 --%>
<table class="kbs-nav-body" cellpadding="5" cellspacing="5">
	<thead>
		<tr class="kbs-intro-result">
			<th width="89%" align="left">&nbsp;</th>
			<th width="1%" align="left">&nbsp;</th>
			<th width="1%" align="left">&nbsp;</th>
			<th width="1%" align="left">&nbsp;</th>
			<th width="1%" align="left">&nbsp;</th>
			<th width="1%" align="left">&nbsp;</th>
			<th width="6%"><a href="javascript:void(0)" id="expandQa" style="float:right;" title="展开所有">预览</a></th>
		</tr>
	</thead>
	<tbody>
		
		<tr class="kbs-info" id="pageBar">
			<td colspan="7" align="center">
				<div id="pagePanel" _url="${pageContext.request.contextPath }/app/wukong/search!loadData.htm" _keyword="${param.keyword }" _categoryDirId = "${param.categoryDirId }"></div>
			</td>
		</tr>
	</tbody>
</table>

<div id="kbValCatePanel" class="kbs-panel-tip" style="width:360px;">
	<c:forEach items="${cateArr }" var="cate">
		<span class="kbs-item" _id="${cate.id }">${cate.name }</span>
	</c:forEach>
	<%-- 
	<table style="width:100%;">
		<tr>
    		<td class="kbs-detail"><span class="kbs-item kbs-item-detail">充值服务 > 缴费方式 > 缴费业务 > 常用介绍 > 全球通 > 计费周期</span></td>
    		<td class="kbs-detail"><span class="kbs-item kbs-item-detail">国际业务 >  国际服务 > 国际漫游 > 出访 > 下发短信 > 国际漫游出访 > 下发短信</span></td>
    	</tr>
    	<tr>
    		<td class="kbs-detail"><span class="kbs-item kbs-item-detail">其他服务 > 品牌介绍 >  全球通 > 全球通</span></td>
    		<td class="kbs-detail">&nbsp;</td>
    	</tr>
	</table>
	--%>
</div>

<div id="kbValAgingPanel" class="kbs-panel-tip" style="width:360px;">
	<span class="kbs-item" _type="1">常态</span>
	<span class="kbs-item" _type="2">未过期</span>
	<span class="kbs-item" _type="4">已过期</span>
	<span class="kbs-item" _type="8">已过追溯期</span>
</div>

<div id="kbValIntervalPanel" class="kbs-panel-tip" style="width:380px;height:36px;line-height:36px;padding-left:8px;">
	<input class="kbs-input" style="width:120px;" id="startDate" type="text" onclick="WdatePicker({'maxDate':'#F{$dp.$D(\'endDate\')}'})"/> ~ <input class="kbs-input" style="width:120px;margin-left:2px;" id="endDate" type="text" onclick="WdatePicker({'minDate':'#F{$dp.$D(\'startDate\')}'})"/>
	<button class="btn btn-primary" id="btnIntervalOk">确定</button>
	<button class="btn btn-primary" id="btnIntervalCancel">取消</button>
</div>

<div id="kbValPropPanel" class="kbs-panel-tip" style="width:360px;">
	<c:forEach items="${requestScope.levelArr}" var="prop">
		<span class="kbs-item" _id="${prop.id }" _prop="0">${prop.name }</span>
	</c:forEach>
</div>
<div id="kbValPropLevel0Panel" class="kbs-panel-tip" style="width:360px;"></div>
<div id="kbValPropLevel1Panel" class="kbs-panel-tip" style="width:360px;"></div>

<div id="locationPanel" class="kbs-panel-tip" style="width:600px;">
<%-- 
	<span class="kbs-item">广州</span>
	<span class="kbs-item">深圳</span>
	<span class="kbs-item">茂名</span>
	<span class="kbs-item">江门</span>
	<span class="kbs-item">河源</span>
	<span class="kbs-item">惠州</span>
	<span class="kbs-item">云浮</span>
	<span class="kbs-item">中山</span>
	<span class="kbs-item">珠海</span>
	<span class="kbs-item">潮州</span>
	--%>
	<c:forEach items="${sessionScope.dimTags}" var="item">
		<span class="kbs-item" _id="${item.tag }">${item.name }</span>
	</c:forEach>
</div>

<div id="brandPanel" class="kbs-panel-tip" style="width:360px;">
	<c:forEach items="${sessionScope.brands}" var="item">
		<span class="kbs-item" _id="${item.tag }">${item.name }</span>
	</c:forEach>
<%-- 
	<span class="kbs-item">全部</span>
	<span class="kbs-item">神州行</span>
	<span class="kbs-item">全球通</span>
	<span class="kbs-item">动感地带</span> --%>
</div>

