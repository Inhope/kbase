<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/shili_dan.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<!--<script type="text/javascript" src="${pageContext.request.contextPath}/resource/wukong/ObjectSearchForComparison.js"></script>
		-->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ui/jquery-ui-sortable.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript">
			window.objId = '${requestScope.objectId}';
			var setting = {
				async: {
					enable: true,
					url: '${pageContext.request.contextPath}/app/search/object-search!getCatalogObject.htm',
					autoParam : ["id", "name=n", "level=lv","bh"],
					dataFilter : function(treeId, parentNode, responseData){
						for(var i =0; i < responseData.length; i++) {
							if(responseData[i].isParent == "true"){
					    		responseData[i].nocheck = true;
							}
					    }
					    return responseData;
				   }
				},
				check: {
					enable: true,
					chkboxType : { "Y" : "ps", "N" : "ps" }
				},
				callback: {
					onCheck : function(event, treeId, treeNode){
						if (treeNode.checked == true) {
							appendSelectObject(treeNode.id,treeNode.name,treeNode.parentId);
						}else{
							
						}
					}
				}
			};
			
			/**
				追加选中的实例对比
			 */
			function appendSelectObject(objectId,objectName,parentId){
				if(!appendValid(objectId,objectName)){
					return;
				}
				$('#compareSortable').append('<li _id="' + objectId + '" _pid="' + parentId + '"><p>' + objectName + '</p><img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico5.jpg" /></li>')
			}
			
			/**
				追加的实例验证是否重复
			 */
			function appendValid(objectId,objectName){
				if(!objectId){
					return false;
				}
				
				if(objectId == window.objId) {
					parent.layer.alert('不能与自己对比,对象:' + objectName, -1);
					return false;
				} 
				
				if ($('.shili_dan_right1_con').find('li[_id="' + objectId + '"]').length > 0){
					parent.layer.alert('请勿重复添加,对象:' + objectName, -1);
					return false;
				}
				return true;
			}
			
			/**
			         初始化实例对比信息
			 */
			function initCompareObject(){
				$.ajax({
					url : $.fn.getRootPath() + "/app/search/object-search!getObjectComparison.htm",
					type : 'POST',
					dataType:'json',
					data : {
					    objectId : window.objId
					},
					success : function(data) {
						if(!data){
							return;
						}
						var rows = data.data;
						if(!rows || rows.length == 0){
							return;
						}
						
						for (var i = 0; i < rows.length; i++) {
							var row = rows[i];
							if(row.saveInManager && row.saveInManager == 1){//如果来自后台则不生成li
								continue;
							}
							appendSelectObject(row.compareObjectId,row.compareObjectName,"-NONE-");
						}
						
					}
				});
			}
		
			$(function(){
				//初始化实例选择树
				$.fn.zTree.init($("#treeDemo"), setting);
				
				//已选中的对比文章取消选中
				$('#compareSortable').on('click', 'img', function(){
					$(this).parent('li').remove();
				});
				
				//初始化 grid
				$('#grid').datagrid({
					onDblClickRow: function(index, row){
						appendSelectObject(row.objectId,row.objectName,"-NONE-");
					}
				});
				
				//初始化实例对比信息
				initCompareObject();
				
				//将实例对比列表转为sortable，可拖动
				$('#compareSortable').sortable();
				
				//查询实例
				$('#btnSearch').click(function(){
					var keyword = $('input[name="keyword"]').val();
					if ($.trim(keyword)==''){
						alert('关键词不能为空');
						return false;
					}
					
					//
					$('#grid').datagrid({
						url: '${pageContext.request.contextPath}/app/wukong/object-compare!loadData.htm?objectName=' + encodeURI(keyword)
					});
				});
				
				//保存
				$('#btnSave').click(function(){
					var $iframe = $(parent.document).find('.content_content iframe:visible');
					var $panel = $iframe.contents().find('#objectComparePanel');
					var $extPanel = $iframe.contents().find('#showCmpareOntoDiv');
					
					$extPanel.html("");
					var objectComparisions = [];
					$('#compareSortable').find('li').each(function(i, item){
						if ($panel.find('input[_objid="' + $(item).attr('_id') + '"]').length==0){
							$extPanel.append('<li class="list-group-item" style="cursor:pointer;"><input type="checkbox" _objid="' + $(item).attr('_id') + '" name="objnameCheck">&nbsp;<span _objid="' + $(item).attr('_id') + '" class="objname">' + $(item).find('p').text() + '</span></li>');
						}
						
						var objectComparision = {};
						objectComparision.objectId = window.objId;
						objectComparision.compareObjectId = $(item).attr('_id');
						objectComparision.compareObjectName = $(this).find("p").text();
						objectComparision.saveInManager = 0;//来源非后台
						objectComparision.dataIndex = i + 1;//数据位置
						objectComparisions[i] = objectComparision;
					});
					
					//ajax 保存或删除关联文章
					$.ajax({
						url : $.fn.getRootPath() + "/app/search/object-search!saveObjectComparison.htm",
						type : 'POST',
						async:false,
						data : {
							objectComparision:JSON.stringify(objectComparisions),
							objectId:window.objId
						},
						success : function(data) {
						}
					});
					
					parent.layer.close(parent.__kbs_layer_index);
				});
				
			});
		</script>
	</head>
	<body>
		<div style="border:1px solid #CCC;">
			<div class="fkui_dan_title">
				<b>实例对比文件添加</b>
			</div>
			<div class="shili_dan_con">
				<div class="shili_dan_left">
					实例选择				  
				</div>
				<div class="shili_dan_center">
					实例名：
					<input name="keyword" type="text" />
					<a href="javascript:void(0);"><input type="button" id="btnSearch" class="anniu1" value="查询" /></a>
				</div>
				<div class="shili_dan_right">
					对比文章列表
				</div>
			</div>
			<div class="shili_dan_con1">
				<div class="shili_dan_left1">
					<div class="xinxi_con_nr_left">
						<ul id="treeDemo" class="ztree"></ul>
					</div>
				</div>
				<div class="shili_dan_center1">
					
					<table id="grid" style="height:368px;"
				        rownumbers="true" pagination="true" 
				        data-options="singleSelect:true">
						<thead>
							<tr>
								<th field="objectName" width="35%">文章名称</th>
								<th field="startTime" width="20%">开始时间</th>
								<th field="endTime" width="20%">结束时间</th>
								<th field="editTime" width="20%">修改时间</th>
							</tr>
						</thead>
					</table>
					<!-- 
					<div class="shili_dan_center1_top">

						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<thead>
								<td>
									<b>实例名</b>
								</td>
								<td>
									<b>开始时间</b>
								</td>
								<td>
									<b>结束时间</b>
								</td>
								<td>
									<b>修改时间</b>
								</td>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="shili_dan_center1_bottom">

						<div class="gonggao_con_nr_fenye">
							<a href="javascript:void(0);" onfocus="this.blur();">首页</a>
							<a href="javascript:void(0);" onfocus="this.blur();">上一页</a>
							<a href="javascript:void(0);" onfocus="this.blur();">下一页></a>
							<a href="javascript:void(0);" onfocus="this.blur();">尾页</a>
						</div>

					</div>
					 -->
				</div>
				<div class="shili_dan_right1" style="width:167px;">
					<div class="shili_dan_right1_top" style="width:167px;">
						<div class="shili_dan_right1_con" style="width:167px;">
							<ul style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
								<s:iterator value="#request.cmpareOnto" var="st">
									<li _id="${st.objectId}">
										<p style="color:red;" >${name}</p>
									</li>
								</s:iterator>
							</ul>
							<ul id="compareSortable">
							
							</ul>
						</div>
						<div class="shili_dan_right1_an">
<!-- 								    <a class="delete" href="javascript:void(0);">删除</a> -->
<!-- 									<a href="javascript:void(0);" class="duibi">对比</a> -->
							<a href="javascript:void(0);" class="duibi" id="btnSave">保存</a>
						</div>
					</div>
					<!--<div class="shili_dan_right1_bottom">
						共 <span id="totlePageSize">0</span> 条记录
					</div>
				--></div>
			</div>
		</div>
	</body>
</html>
