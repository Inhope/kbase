<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
/**
 * 命中知识详情
 */
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/wukong/wukong-luna.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/intro/introjs.min.css">
		<style type="text/css">
			body{
				padding: 10px;
			}
		</style>
		<script type="text/javascript">
			window.__kbs_enable_merge = '${requestScope.enableMerge }';
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery.cookie.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/laypage/laypage.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/intro/intro.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/base.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/search-plugin.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/wukong.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/SearchJump.js"></script>
		
		<jsp:include page="/resource/wukong/head.jsp" flush="true" />
	</head>

	<body style="min-width: 1024px;">
		<c:if test="${requestScope.isHit}">
			<table class="kbs-nav">
				<tr>
					<td width="25%"></td>
					<td width="25%"></td>
					<td width="25%"></td>
					<td width="25%"></td>
				</tr>
				<tr>
					<td class="kbs-detail" colspan="3" style="padding:10px 5px;" >
						<span class="kbs-question" _id="${faq.question_id }" _title="${faq.question_w }">${faq.question }</span>
					</td>
					<td class="kbs-detail" align="right" style="padding-right:10px;">
						<a href="javascript:void(0)" id="kbsIntro">新手引导</a>&nbsp;
						<%-- 
						<span class="kbs-office kbs-office-thumbs-up"></span>
						<span class="kbs-thumbs-text">21</span>
						<span class="kbs-office kbs-office-thumbs-down"></span>
						<span class="kbs-thumbs-text">3</span>
						--%>
					</td>
				</tr>
				<tr>
					<td class="kbs-detail" colspan="4">
						<div class="kbs-answer">
							${faq.answer }
						</div>
					</td>
				</tr>
				
				<%-- 附件和P4数据 --%>
				<jsp:include page="attachment-plugin.jsp"></jsp:include>
				
				<c:if test="${fn:length(relationQa)>0 }">
					<tr class="kbs-intro-relatefaq">
						<td class="kbs-detail">
							相关问题：
						</td>
						<!-- add by lwp 2016.05.13 -->
						<td class="kbs-detail" colspan="2" id="related-question">
							<c:forEach items="${relationQa }" var="item" begin="0" end="3">
								<span class="kbs-item">${item }</span>
							</c:forEach>
						</td>
					</tr>
				</c:if>
			</table>
		</c:if>
	
		<jsp:include page="search-plugin.jsp"></jsp:include>
	</body>
</html>