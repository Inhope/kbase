<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!-- 
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		 -->
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/h-ui/lib/html5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/h-ui/lib/respond.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/h-ui/lib/PIE_IE678.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/h-ui/static/h-ui/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/h-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/h-ui/lib/jquery/1.9.1/jquery.min.js"></script>
		<!--[if IE 6]>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/h-ui/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
		<script>DD_belatedPNG.fix('.pngfix,.iconpic,.list-icon');</script>
		<![endif]-->
		
		<script type="text/javascript">
			$(function(){
				$('.btn-group').on('click', 'span', function(){
					$(this).siblings().removeClass('btn-primary').addClass('btn-default');
					$(this).addClass('btn-primary');
				});
			});
		</script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4>
						全球通有什么业务
						<small style="margin-left:10px;">更新时间: 2016-04-06 12:23</small>
						<input class="btn btn-secondary radius" type="button" value="详情">
						<i class="Hui-iconfont Hui-iconfont-zan"></i>22
						<i class="Hui-iconfont Hui-iconfont-cai"></i>1
					</h4>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12" style="background-color:#F5F5F5;">
					<h5 style="line-height:1.5;">
						1. 标题栏显示的排序规则为用户选择的排序项目，如点击量被选中 （参见页面 搜索结果列表页-

按照点击量排序（合并目录显示））

 

2. 当勾选了“知识目录合并”的选项时：

    A. 目录的排序规则，仍然保持按照知识目录下匹配的知识点的匹配度之和进行排序

    B. 目录内知识点的排序，按照被选中的排序项目（如点击量）进行排序

 

3. 当取消“知识目录合并”的选中状态时，知识点列表的排序规则为选中的排序项目（如点击量）

 

目前全球通支持的业务有：各种智能和移动数据新业务如主叫显示，呼叫等待，呼叫前转，数据传真，留言信箱，短信，秘书服务等；如需了解更多全球通业务信息，请您登陆我公司网站http://gd.10086.cn/gotone/operation/
					</h5>
					<table style="margin-bottom:5px;">;
						<tr>
							<td width="10%"><li class="Hui-iconfont Hui-iconfont-news"></li></td>
							<td width="25%">全球通VIP高尔夫俱乐部报名流程</td>
							<td width="15%">2016-04-06</td>
							<td width="10%"><li class="Hui-iconfont Hui-iconfont-news"></li></td>
							<td width="25%">全球通VIP高尔夫俱乐部报名流程</td>
							<td width="15%">2016-04-06</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					在归属地 <strong>广州</strong> 品牌 <strong>神州行</strong> 中搜索 <strong>全球通的业务</strong> ，找到 <strong>1249</strong> 条记录
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<table class="table table-border table-bordered radius">
					 	<tbody>
					 		<tr>
								<td width="10%">搜索方式</td>
								<td>
									<div class="btn-group" id="searchStyleOpt">
									  <span class="btn btn-primary radius">模糊匹配</span>
									  <span class="btn btn-default radius">精确搜索</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>搜索范围</td>
								<td>
									<div class="btn-group" id="searchScopeOpt">
									  <span class="btn btn-primary radius">标准问答知识</span>
									  <span class="btn btn-default radius">扩展问知识</span>
									  <span class="btn btn-default radius">附件文档知识</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>筛选条件</td>
								<td>
									<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    知识目录
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
									知识时效
									创建日期
									归属地
									品牌
								</td>
							</tr>
					 	</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</body>
</html>