<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
/**
 * 命中知识详情
 */
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识展示-详情</title>
		<link href="${pageContext.request.contextPath}/library/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/buttons.css" rel="stylesheet" type="text/css" />
		
		<style type="text/css">
			.kbs-avatar{
				padding-left: 2px;
				padding-right: 2px;
			}
			.kbs-title, .kbs-title-helios{
				font-family: Arial,Helvetica,sans-serif;
				font-size: 14px;
				font-weight: 700;
				color: #000;
				margin-bottom:10px;
			}
			.kbs-title-helios{
				color: #666666;
				cursor: pointer;
			}
			.kbs-detail{
				color: #FF0000;
				cursor: pointer;
				text-decoration: underline;
			}
			.nav-tabs li a{
				padding: 5px;
			}
		</style>
		<script type="text/javascript">
			pageContext = {
				contextPath: '${pageContext.request.contextPath }'
			}
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/kbase.convert.js"></script>
		
		<script type="text/javascript">
			$(function(){
				
				//Tab
				$('.nav-tabs-panel').hide();
				$('.nav-tabs-panel:first').show();
				//Tab切换
				$('.nav-tabs li').hover(function(){
					$('.nav-tabs li').removeClass('active');
					$(this).addClass('active');
					
					//当前是第几个 li ？
					var _ind = $(this).index();
					$('.nav-tabs-panel').hide();
					$($('.nav-tabs-panel').get(_ind)).show();
					
				});
				
				//单击 预览 触发
				$('div.checkbox input[type="checkbox"]').click(function(){
					if ($(this).attr('checked')!=undefined){
						$('div[_id="kbs-fqa"]').find('.row:last').show();
					}else{
						$('div[_id="kbs-fqa"]').find('.row:last').hide();
					}
				});
				
				//单击 详情 触发
				$('.kbs-detail').click(function(){
					var _id = $(this).attr('_id');
					location.href = pageContext.contextPath + '/app/wukong/search.htm?id=' + _id;
				});
				
				//单击标题展开答案预览
				$('.kbs-title-helios').click(function(){
					var $row = $(this).parent('.row').next('.row');
					if ($row.length>0){
						if ($row.get(0).style.display=='none'){
							$row.show();
						}else{
							$row.hide();
						}
					}
				});
				
				//签读 倒计时
				if ($('#btnMarkread').length>0){
					var desc = '签读';
					var limit = 20;
					$('#btnMarkread').attr('disabled', 'disabled');
					var countdownTimer = window.setInterval(function(){
						limit--;
						$('#btnMarkread').text(desc + ' ' + limit);
						if (limit<1){
							$('#btnMarkread').removeAttr('disabled').text(desc);
							window.clearInterval(countdownTimer);
						}
					}, 1000);
				}	
				
			});
		</script>
	</head>
	
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 kbs-avatar">
					<ol class="breadcrumb" id="navCate" style="height:50px;padding-top:12px;margin-bottom:3px;">
						<li _type="" _id=""><a class="kbs-title" herf="javascript:void(0);" style="cursor:pointer;">${faq.bh_name }</a></li>
						<div class="col-md-offset-6 col-md-6 text-right" style="margin-top:-25px;">
							<div class="btn-group">
								<button class="btn btn-primary" id="btnFav">收藏</button>
								<button class="btn btn-primary" id="btnVersionCheck">版本对比</button>
								<button class="btn btn-primary" id="btnDocList">原文</button>
							</div>
		  				</div>
					</ol>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-9 kbs-avatar">
					<div class="row">
						<!-- 知识明细和相关操作 -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading text-right" style="padding: 2px 3px;">
									<div class="btn-group" role="group">
										<c:choose>
											<c:when test="${requestScope.flag_objectMarkread=='1'}">
												<button class="btn btn-info" disabled="disabled">已读</button>
											</c:when>
											<c:otherwise>
												<button class="btn btn-info" disabled="disabled" id="btnMarkread">签读 20</button>
											</c:otherwise>
										</c:choose>
										<button class="btn btn-info" id="btnErrcorrect">纠错</button>
										<button class="btn btn-info">推荐</button>
										<button class="btn btn-info">收藏</button>
									</div>
								</div>
								<div class="panel-body">
									<div class="kbs-title" kbs-question="1">${faq.question}</div>
									<div kbs-answer="1">${faq.answer}</div>
									<c:forEach items="${p4s}" var="item">
										<a href="javascript:void(0);">[${item.title}]</a><br/>
										<iframe src="${item.addr }" height="300" width="100%" frameborder="no" border="0"></iframe>
									</c:forEach>
									<c:forEach items="${atts}" var="item">
										<a href="${item.addr }">${item.title }</a>
										<a href="javascript:void(0);" kbsconvert="1" kbsname="${item.name }" kbsid="${item.addr }">[附件预览]</a><br>
									</c:forEach>
								</div>
							</div>
						</div>
						<!-- 知识列表 -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading text-right" style="padding: 2px 3px;">
									<div class="checkbox">
										<label><input type="checkbox"> 预览&nbsp;&nbsp;</label>
									</div>
								</div>
								<div class="panel-body">
									<c:forEach items="${qaList}" var="item">
										<c:if test="${item.question_id!=param.id}">
											<div _id="kbs-fqa" style="margin-bottom:12px;border-bottom:1px dotted #CDCDCD;">
												<div class="row">
													<div class="col-md-11 kbs-title-helios" kbs-question="1">
														${item.question }
													</div>
													<div class="col-md-1 text-right kbs-detail" _id="${item.question_id }">详情</div>
												</div>
												<div class="row" style="display:none">
													<div class="col-md-12" kbs-answer="1">${item.answer}</div>
												</div>
											</div>
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<ul class="nav nav-tabs" style="margin-bottom:10px;">
						<li role="presentation" class="active"><a href="javascript:void(0);">知识关联</a></li>
						<li role="presentation"><a href="javascript:void(0);">实例对比</a></li>
						<li role="presentation"><a href="javascript:void(0);">实例关联</a></li>
					</ul>
					<div class="nav-tabs-panel" id="valueRelatedPanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.rbtResponse_relatedQuestions}" var="item">
								<li class="list-group-item" style="cursor:pointer;">${item }</li>
							</c:forEach>
						</ul>
					</div>
					<div class="nav-tabs-panel" id="objectComparePanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.cmpareOnto}" var="item">
								<li class="list-group-item" style="cursor:pointer;">
									<input type="checkbox" _objid="${item.objectId }">&nbsp;<span _objid="${item.objectId }">${item.name }</span>
								</li>
							</c:forEach>
							<c:if test="${fn:length(requestScope.cmpareOnto)>0}">
								<li class="list-group-item text-center">
									<button class="btn btn-info" id="btnCompareOnto">选中对比</button>
									<button class="btn btn-info" id="btnAddOnto">添加文章</button>
								</li>
							</c:if>
						</ul>
					</div>
					<div class="nav-tabs-panel" id="objectRelatedPanel">
						<ul class="list-group">
							<c:forEach items="${requestScope.relationOnto}" var="item">
								<li class="list-group-item" style="cursor:pointer;" _objid="${item.objectId }">
									${item.name }
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>