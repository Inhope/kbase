<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/wukong/wukong.css">
		
		<style type="text/css">
			.kbs-tab-header{
				
			}
			.kbs-tab{
				width:100%;
			}
			.kbs-tab-header .kbs-tab-item, .kbs-tab-header .kbs-tab-item-checked{
				float: left;
				padding: 2px 10px;
				margin: 1px;
				border: 1px solid #fff;
				cursor: pointer;
				font-size: 14px;
				font-weight: bolder;
			}
			.kbs-tab-item-checked{
				color: #ff0000;
			}
			.kbs-tab-header .kbs-tab-item:hover, .kbs-tab-header .kbs-tab-item-checked:hover{
				border: 1px solid #000;
			}
			.kbs-tab-body .kbs-tab-content{
				width: 100%;
				height: 200px;
				border: 1px solid #000;
				display: none;
			}
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/laypage/laypage.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/base.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/search-plugin.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/wukong/wukong.js"></script>
	
		<script type="text/javascript">
			$(function(){
				var tabIndex = $('.kbs-tab-header .kbs-tab-item-checked').index();
				$('.kbs-tab-body .kbs-tab-content:eq(' + tabIndex + ')').show();
				
				
			});
		</script>
	</head>

	<body>
		<table class="kbs-tab">
			<tr>
				<td class="kbs-tab-header">
					<div class="kbs-tab-item-checked">活动介绍</div>
					<div class="kbs-tab-item">活动办理</div>
					<div class="kbs-tab-item">活动规则</div>
				</td>
			</tr>
			<tr>
				<td class="kbs-tab-body">
					<div class="kbs-tab-content">
						
					</div>
					<div class="kbs-tab-content">
						
					</div>
					<div class="kbs-tab-content">
						
					</div>
				</td>
			</tr>
		</table>
	
		<jsp:include page="search-plugin.jsp"></jsp:include>
	</body>
</html>