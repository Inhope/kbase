<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%--  
	@author eko.zhan
	@date 2016-05-31 14:25
	目录合并显示标准问
 --%>
<c:forEach items="${requestScope.items}" var="item">
	<c:set var="tmpCateId" value=""></c:set>
	<c:if test="${item.category_id!=tmpCateId}">
		<c:set var="tmpCateId" value="${item.category_id}"></c:set>
		<tr class="kbs-row-cate"><td colspan="7">${item.bh_name }</td></tr>
	</c:if>
	<tr class="kbs-row-question">
		<td class="kbs-cell-question">${item.question }</td>
		<td>${item.create_time}</td>
		<td>${item.edit_time}</td>
		<td>${item.editor}</td>
		<td>${item.visit_num}</td>
		<td>&nbsp;</td>
		<td>
			<span class="kbs-item" _name="detail" _id="${item.question_id}" _title="${item.question_w}">详情</span>
		</td>
	</tr>
	<tr class="kbs-row-answer">
		<td colspan="7">${item.answer}</td>
	</tr>
</c:forEach>