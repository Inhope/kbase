<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>知识详情</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/bootstrap/css/bootstrap.min.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 14px;
			}
			highlight{
				color: #FF0000;
			}
			.kbs-answer-panel{
				background-color: #F5F5F5;
			}
			.btn{
				font-size: 12px;
			}
			.table{
				margin-bottom: 8px;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				$('.btn-group').on('click', 'button', function(){
					$(this).siblings().removeClass('btn-primary').addClass('btn-default');
					$(this).addClass('btn-primary');
				});
			});
		</script>
	</head>

	<body>
		<div class="container-fluid">
			<div class="row">
				<h4>
					<div class="col-md-5"><a href="javascript:void(0)">全球通有什么业务</a></div>
					<div class="col-md-3">
						<small style="margin-left:10px;">更新时间: 2016-04-06 12:23</small>
					</div>
					<div class="col-md-2">
						
					</div>
					<div class="col-md-2">
						<i class="glyphicon glyphicon-thumbs-up"></i> 22
						<i class="glyphicon glyphicon-thumbs-down"></i> 1
					</div>
				</h4>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<table class="table kbs-answer-panel" style="margin:8px 0px;">
						<tr>
							<td colspan="6">
								<h5 style="line-height: 1.5;">
						1. 标题栏显示的排序规则为用户选择的排序项目，如点击量被选中 （参见页面 搜索结果列表页-

按照点击量排序（合并目录显示））

 

2. 当勾选了“知识目录合并”的选项时：

    A. 目录的排序规则，仍然保持按照知识目录下匹配的知识点的匹配度之和进行排序

    B. 目录内知识点的排序，按照被选中的排序项目（如点击量）进行排序

 

3. 当取消“知识目录合并”的选中状态时，知识点列表的排序规则为选中的排序项目（如点击量）

 

目前全球通支持的业务有：各种智能和移动数据新业务如主叫显示，呼叫等待，呼叫前转，数据传真，留言信箱，短信，秘书服务等；如需了解更多全球通业务信息，请您登陆我公司网站http://gd.10086.cn/gotone/operation/
					</h5>
							</td>
						</tr>
						<tr>
							<td width="50%">
								<a href="javascript:void(0)">
									<li class="glyphicon glyphicon-list-alt 2x"></li>
									全球通VIP高尔夫俱乐部报名流程
									2016-04-06
								</a>
							</td>
							<td width="50%">
								<a href="javascript:void(0)">
									<li class="glyphicon glyphicon-list-alt 2x"></li>
									全球通VIP高尔夫俱乐部报名流程
									2016-04-06
								</a>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<strong>相关问题</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-link" value="全球通套餐分钟数的使用">
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					在归属地 <strong>广州</strong> 品牌 <strong>神州行</strong> 中搜索 <strong>全球通的业务</strong> ，找到 <strong>1249</strong> 条记录
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<table class="table">
					 	<tbody>
					 		<tr>
								<td width="10%">搜索方式</td>
								<td>
									<div class="btn-group" id="searchStyleOpt">
									  <button class="btn btn-primary">模糊匹配</button>
									  <button class="btn btn-default">精确搜索</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>搜索范围</td>
								<td>
									<div class="btn-group" id="searchScopeOpt">
									  <button class="btn btn-primary">标准问答知识</button>
									  <button class="btn btn-default">扩展问知识</button>
									  <button class="btn btn-default">附件文档知识</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>筛选条件</td>
								<td>
									<div class="dropdown col-md-2">
										<button class="btn btn-default dropdown-toggle" type="button" id="kbValCatePanel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											知识目录
											<span class="caret"></span>
										</button>
										<div class="dropdown-menu" aria-labelledby="kbValCatePanel" style="width: 600px;padding: 10px;">
										    <table class="table table-bordered">
										    	<tr>
										    		<td><a href="javascript:void(0)">充值服务 > 缴费方式 > 缴费业务 > 常用介绍 > 全球通 > 计费周期</a></td>
										    		<td><a href="javascript:void(0)">国际业务 >  国际服务 > 国际漫游 > 出访 > 下发短信 > 国际漫游出访 > 下发短信</a></td>
										    	</tr>
										    	<tr>
										    		<td><a href="javascript:void(0)">其他服务 > 品牌介绍 >  全球通 > 全球通</a></td>
										    		<td>&nbsp;</td>
										    	</tr>
										    </table>
										</div>
									</div>
									
									<div class="dropdown col-md-2">
										<button class="btn btn-default dropdown-toggle" type="button" id="kbValAgingPanel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											知识时效
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="kbValAgingPanel">
										    <li><a href="javascript:void(0)">全部</a></li>
										    <li><a href="javascript:void(0)">常态</a></li>
										    <li><a href="javascript:void(0)">未过期</a></li>
										    <li><a href="javascript:void(0)">未过追溯期</a></li>
										    <li><a href="javascript:void(0)">已过追溯期</a></li>
										</ul>
									</div>
									
									<div class="dropdown col-md-2">
										<button class="btn btn-default dropdown-toggle" type="button" id="kbValIntervalPanel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											创建日期
											<span class="caret"></span>
										</button>
										<div class="dropdown-menu form-inline" aria-labelledby="kbValIntervalPanel" style="width:400px;padding:10px;">
										    &nbsp;<input class="form-control" style="width:120px;" id="fromDate" type="text" onclick="WdatePicker()"/> ~ <input class="form-control" style="width:120px;margin-left:2px;" id="toDate" type="text" onclick="WdatePicker()"/>
											<button class="btn btn-primary">确定</button>
											<button class="btn btn-primary">取消</button>
										</div>
									</div>
									
									<div class="dropdown col-md-2">
										<button class="btn btn-default dropdown-toggle" type="button" id="locationPanel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											归属地
											<span class="caret"></span>
										</button>
										<div class="dropdown-menu form-inline" aria-labelledby="locationPanel" style="width:400px;padding: 0px 6px;">
											<h4>
												<c:forEach items="${sessionScope.dimTags}" var="dimTag">
													<button class="btn btn-info" style="margin-bottom:2px;">${dimTag.name }</button>
												</c:forEach>
											</h4>
										</div>
									</div>
									
									<div class="dropdown col-md-2">
										<button class="btn btn-default dropdown-toggle" type="button" id="brandPanel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											品牌
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="brandPanel">
										    <li><a href="javascript:void(0)">全部</a></li>
										    <li><a href="javascript:void(0)">神州行</a></li>
										    <li><a href="javascript:void(0)">全球通</a></li>
										    <li><a href="javascript:void(0)">动感地带</a></li>
										</ul>
									</div>
								</td>
							</tr>
							<!-- 筛选条件结束 -->
					 	</tbody>
					</table>
				</div>
			</div>
			<!-- 查询条件结束 -->
			
			<div class="row">
				<div class="col-md-12">
					<table class="table table-hover">
						<thead>
							<tr class="active">
								<th width="40%">&nbsp;</th>
								<th width="10%">创建时间</th>
								<th width="10%">修改时间</th>
								<th width="10%">修改人</th>
								<th width="10%">点击量</th>
								<th width="10%">&nbsp;</th>
								<th width="10%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr class="warning">
								<td colspan="7">
									<div class="btn-group" id="searchStyleOpt">
										<button class="btn btn-primary">目录合并</button>
										<button class="btn btn-default">知识预览</button>
									</div>
								</td>
							</tr>
							<tr class="info">
								<td colspan="7">
									在 充值服务 > 缴费方式 > 缴费业务 > 常用介绍 > 全球通 > 计费周期 中找到 9 条记录
								</td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>的计费周期介绍</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>客户办理手机流量10元套餐的生效规则</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>怎样更改套餐？</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr class="info">
								<td colspan="7">
									在  国际业务 >  国际服务 > 国际漫游 > 出访 > 下发短信 > 国际漫游出访 > 下发短信 中找到 7 条记录 >
								</td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>的计费周期介绍</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>客户办理手机流量10元套餐的生效规则</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>怎样更改套餐？</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr class="info">
								<td colspan="7">
									在 其他服务 > 品牌介绍 >  全球通 > 全球通 > 中找到 3 条记录 >
								</td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>的计费周期介绍</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>客户办理手机流量10元套餐的生效规则</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
							<tr>
								<td><highlight>全球通</highlight>怎样更改套餐？</td>
								<td>2015-04-20</td>
								<td>2015-04-20</td>
								<td>tingting</td>
								<td>20</td>
								<td></td>
								<td><a href="javascript:void(0)">详情</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					您是不是想找 <a href="javascript:void(0)">全球通套餐</a> 
					<a href="javascript:void(0)">全球通积分兑换</a>
					<a href="javascript:void(0)">全球通58元套餐介绍</a>
					<a href="javascript:void(0)">中国移动网上营业厅</a>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 text-center">
					<nav>
					  <ul class="pagination">
					    <li>
					      <a href="javascript:void(0)" aria-label="Previous">
					        <span aria-hidden="true">&laquo;</span>
					      </a>
					    </li>
					    <li><a href="javascript:void(0)">1</a></li>
					    <li><a href="javascript:void(0)">2</a></li>
					    <li><a href="javascript:void(0)">3</a></li>
					    <li><a href="javascript:void(0)">4</a></li>
					    <li><a href="javascript:void(0)">5</a></li>
					    <li>
					      <a href="javascript:void(0)" aria-label="Next">
					        <span aria-hidden="true">&raquo;</span>
					      </a>
					    </li>
					  </ul>
					</nav>
				</div>
			</div>
			
		</div>
	</body>
</html>