<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>请选择收藏夹</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<script type="text/javascript">
			//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
			window.__kbs_has_parent = !(window.parent==window);
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		
		<script type="text/javascript">
			$(function(){
				$('#btnSave').click(function(){
					var text = $('#favOption').combobox('getText');
					var val = $('#favOption').combobox('getValue');
					
					if (text==''){
						layer.alert('收藏夹不能为空', -1);
						return false;
					}
					
					var _data = {
						'text': text,
						'value': val,
						'objid': '${param.objid}',
						'faqid': '${param.faqid}',
						'cataid': '${param.cataid}'
					};
					
					$.post('${pageContext.request.contextPath}/app/fav/fav-clip-object!saveme.htm', _data, function(data){
						if (window.__kbs_has_parent){
							layer.alert(data.message, -1, function(){
								parent.layer.close(parent.__kbs_layer_index);
							});
						}else{
							alert('收藏成功');
							window.close();
						}
					}, 'json');
					
				});
			});
		</script>
	</head>

	<body>
		<table class="box" cellpadding="1" cellspacing="1" align="center" style="width:98%;margin-top:20px;">
			<tr>
				<td>
					请选择收藏夹：
				</td>
				<td>
					<select id="favOption" class="easyui-combobox" style="width:200px;">
						<c:forEach items="${list}" var="item">
							<option value="${item.id }">${item.favClipName }</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<a id="btnSave" href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-save'">收藏</a>
				</td>
			</tr>
		</table>
	</body>
</html>
