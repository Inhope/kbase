<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >  
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
	  		table.kbs-dialog{
	  			font-size:12px;table-layout:fixed;margin:10px;border:1px solid #eee;
	  		}
	  		table.kbs-dialog tr td{
	  			border:1px dotted #eee;
	  		}
	  		table.kbs-dialog tr td input.text{
	  			border:1px solid #eee;height:25px;line-height:25px;width:280px;
	  		}
	  		table.kbs-dialog tr td input.button{
	  			padding-left:5px;padding-right:5px;padding-top:3px;padding-bottom:3px;border:1px solid #7E7E7E;background-color:#eee;cursor:pointer;
	  		}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/fav/js/FavClip.js"></script>
		<style type="text/css">
			button{
				cursor: pointer;
			}
		</style>
	</head>

	<body>
		<!-- 
		<div class="content_right_bottom">

			<div class="zhishi_titile">
				<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/images/zhishi_ico.jpg" width="16" height="15" />
				<a href="${pageContext.request.contextPath}/app/fav/fav-clip.htm">收藏夹列表</a>
				<button>新建</button>&nbsp;<button>修改</button>&nbsp;<button>删除</button>
			</div>
			
			<div class="zhishi_toolbar" style="display: none;">
				<button>新建</button>&nbsp;<button>修改</button>&nbsp;<button>删除</button>
			</div>
	
			<div class="zhishi_con">
				<div class="zhishi_con_title">
					<h1>
						<input type="checkbox">
					</h1>
					<h2>
						<b>名称</b>
					</h2>
					<h3>
						创建时间
					</h3>
					<h4>
						上次修改时间
					</h4>
					
				</div>
				<div class="zhishi_con_nr">
					<ul>
						<s:iterator var="fc" value="#request.faviconClips">
							<li>
								<h1>
									<input id="<s:property value="#fc.id"/>" type="checkbox">
								</h1>
								<h2>
									<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/images/zhishi_ico2.jpg" width="16" height="15" />
									<b id="<s:property value="#fc.id"/>"><s:property value="#fc.favClipName"/></b>
								</h2>
								<h3><s:date name="#fc.createTime" format="yyyy-MM-dd HH:mm:ss"/></h3>
								<h4><s:date name="#fc.lastUpdateTime" format="yyyy-MM-dd HH:mm:ss"/></h4>
							</li>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>

		<div class="f-div" style="display: none;">
			<div class="f-diva">
				<div class="f-div-t"><b>新增收藏夹</b>
				<a href="javascript:void(0);">
							<img width="12" height="12" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/images/xinxi_ico1.jpg">
						</a></div>
				<div class="f-div-c">
					<span>文件夹名：</span>
					<input type="text"/>
					<button>提交</button>
				</div>
			</div>
		</div>
		 -->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="5%"></td>
						<td width="55%"></td>
						<td width="20%"></td>
						<td width="20%"></td>
					</tr>
					<tr>
						<td colspan="4" align="right" valign="middle" style="text-align:right;">
							<div class="gonggao_titile_right">
								<a href="javascript:void(0);" id="btnDel">删除</a>
								<a href="javascript:void(0);" id="btnEdit">修改</a>
								<a href="javascript:void(0);" id="btnAdd">新建</a>
							</div>
						</td>
					</tr>
					<tr class="tdbg">
						<td><input type="checkbox" id="selAll" /></td>
						<td>名称</td>
						<td>创建时间</td>
						<td>最后修改时间</td>
					</tr>
					<s:iterator var="fc" value="#request.faviconClips">
						<tr>
							<td><input id="<s:property value="#fc.id"/>" type="checkbox" xname='childCheckBox' xfoldername='<s:property value="#fc.favClipName"/>'></td>
							<td valign="middle">
								<a href="${pageContext.request.contextPath }/app/fav/fav-clip-object.htm?fid=<s:property value="#fc.id"/>" target="_self">
									<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/images/zhishi_ico2.jpg" width="16" height="15" style="padding-top:8px;"/>
									<b id="<s:property value="#fc.id"/>"><s:property value="#fc.favClipName"/></b>
								</a>
							</td>
							<td>
								<s:date name="#fc.createTime" format="yyyy-MM-dd HH:mm:ss"/>
							</td>
							<td>
								<s:date name="#fc.lastUpdateTime" format="yyyy-MM-dd HH:mm:ss"/>
							</td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
		
		<!-- 弹出层 -->
		<table width="420" height="80" cellpadding="0" cellspacing="2" class="kbs-dialog" style="display:none">
			<input type="hidden" id="folderId">
			<tr>
				<td width="25%"><b>收藏夹名称</b></td>
				<td width="75%"><input type="text" id="folderName" class="text"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="button" id="btnSave" value="提交" class="button"/></td>
			</tr>
		</table>
	</body>
</html>