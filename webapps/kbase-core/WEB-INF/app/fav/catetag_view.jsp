<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>收藏详情</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css"/>
		
		<style type="text/css">
			body{
				padding: 3px;
				font-size: 12px;
			}
			.panel-header, .panel-body{
				border: 0px;
			}
			.datagrid-row{
				height: 30px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->

		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>

		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/plugins/easyui.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/custom/gzyd/js/GzydMappings.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/sms/js/SMSLog.js"></script>

		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-zclip/jquery.zclip.js"></script>
		
		<script type="text/javascript">
			window.__kbs_fav_sms = 1;
			window.__kbs_fav_sms_data = '';
			categoryId = '';
			
			var locationList = {
				'chaozhou': '潮州',
				'dongguan': '东莞',
				'guangzhou': '广州',
				'heyuan': '河源',
				'huizhou': '惠州',
				'foshan': '佛山',
				'jiangmen': '江门',
				'jieyang': '揭阳',
				'maoming': '茂名',
				'meizhou': '梅州',
				'qingyuan': '清远',
				'shantou': '汕头',
				'shanwei': '汕尾',
				'shaoguan': '韶关',
				'shenzhen': '深圳',
				'yangjiang': '阳江',
				'yunfu': '云浮',
				'zhanjiang': '湛江',
				'zhaoqing': '肇庆',
				'zhongshan': '中山',
				'zhuhai': '珠海'
			};
			
			/**
			 * 删除选中的grid record
			 */
			function removeSelections(){
				var _records = $('#grid').datagrid('getSelections');
				$(_records).each(function(i, item){
					$('#grid').datagrid('deleteRow', $('#grid').datagrid('getRowIndex', item));
				});
			}
			
			$(function(){
				//your code here ...
				
				var _type = '${param.type}'; //'param.type=='SMS_PICK''
				
				$('#remarkPanel').window({
					title: '请输入备注',
				    width: 400,
				    height: 150,
				    modal: false,
				    collapsible: false,
				    minimizable: false,
				    closed: true,
				    maximizable: false
				});
				
				$('#folderPanel').window({
					title: '请输入收藏夹名称',
				    width: 400,
				    height: 150,
				    modal: false,
				    collapsible: false,
				    minimizable: false,
				    closed: true,
				    maximizable: false
				});
				
				
				var key = '${param.key}';
				
				$(document).on('contextmenu', function(e){
					e.preventDefault();
				});
				
				//主面板右键
				var westPanel = $('#cc').layout('panel', 'west');
				$(westPanel).on('mouseup', function(e){
					if (e.button==2){
						$('#mm').menu('show', {
						    left: e.pageX,
						    top: e.pageY
						});
					}
				});
				
				var _timeout = null;
				
				$('#tree').tree({
					url: '${pageContext.request.contextPath }/app/fav/fav-folder!loadData.htm?key=${param.key }',
					onContextMenu: function(e, node){
						e.preventDefault();
						$('#tree').tree('select', node.target);
						$('#mm').menu('show', {
							left: e.pageX,
							top: e.pageY
						});
					},
					onClick: function(node){
					
						
						if (_timeout!=null){
							window.clearTimeout(_timeout);
							_timeout = null;
						}
					
						_timeout = window.setTimeout(function(){
							
							//刷新右侧的grid
							$('#grid').datagrid({
								url: '${pageContext.request.contextPath }/app/fav/fav-detail-catetag!loadData.htm?key=${param.key }&id=' + node.id
							});
						}, 300);
					},
					onDblClick: function(node){
						if (_timeout!=null){
							window.clearTimeout(_timeout);
							_timeout = null;
						}
						$('#tree').tree('expand', node.target);
						//刷新右侧的grid
						$('#grid').datagrid({
							url: '${pageContext.request.contextPath }/app/fav/fav-detail-catetag!loadData.htm?key=${param.key }&id=' + node.id
						});
						
					},
					onExpand: function(node){
						var children = $('#tree').tree('getChildren', node.target);
						if (children.length==0){
							$('#tree').tree('update', {
								target: node.target,
								state: 'open'
							});
						}
					}/*,
					onLoadSuccess: function(){
						//隐藏分页条中每页显示的记录条数select控件
						$('.pagination-page-list').hide();
					}*/
				});
				
				$('#grid').datagrid({
					//fixed: true,
					checkOnSelect: true,
					selectOnCheck: true,
				    columns:[[
						{field:'id', title:'', width: '5%', checkbox: true},
						{field:'location', title:'地市', width:'5%', formatter: function(value,row,index){
							return locationList[row.location];
						}},
				        {field:'name', title:'业务名称及路径', width:'65%'},
				        {field:'createDate', title:'收藏时间', width:'10%'},
				        {field:'remark', title:'备注', width:'15%'}
					]],
					onDblClickRow: function(rowIndex, rowData){
						var title = rowData.title;
						var categoryId = rowData.cateId;
						var url = encodeURI(encodeURI('${pageContext.request.contextPath }/app/category/category.htm?type=1&categoryId='+categoryId+'&categoryName='+title));
						parent.TABOBJECT.open({
							id : categoryId,
							name : title,
							hasClose : true,
							url : url,
							isRefresh : true
						}, this);
					}
				});
				
				//收藏信息删除
				$('#btnDel').click(function(){
					var _records = $('#grid').datagrid('getSelections');
					if (_records.length==0){
						alert('请选择数据');
						return false;
					}
					if (!window.confirm('确定删除吗')){
						return false;
					}
					var _ids = [];
					$(_records).each(function(i, item){
						_ids.push(item.id);
					});
					$.post('${pageContext.request.contextPath}/app/fav/fav-detail-catetag!delete.htm', {ids: _ids}, function(data){
						if (data.success){
							$(_records).each(function(i, item){
								var rowIndex = $('#grid').datagrid('getRowIndex', item);
								$('#grid').datagrid('deleteRow', rowIndex);
							});
						}
					}, 'json');
				});
				//移动收藏数据
				$('#btnMove').click(function(){
					var _records = $('#grid').datagrid('getSelections');
					if (_records.length==0){
						alert('请选择数据');
						return false;
					}
					var ids = [];
					
					$(_records).each(function(i, item){
						ids.push(item.id);
					});
					window.__kbs_fav_params = ids;
					var _url = '${pageContext.request.contextPath }/app/fav/fav-folder!pick.htm?key=${param.key }';
					window._open(_url, '选择收藏夹', 500, 300)
				});
				//添加备注
				$('#btnRemark').click(function(){
					var _records = $('#grid').datagrid('getSelections');
					if (_records.length==0){
						alert('请选择数据');
						return false;
					}
					
					$('#remarkPanel').window('open');
				});
				
				$('#btnOk').click(function(){
					var remark = $('input[name="name"]').val();
					if (remark==''){
						alert('备注不能为空');
						return false;
					}
					var _records = $('#grid').datagrid('getSelections');
					var ids = [];
					$(_records).each(function(i, item){
						ids.push(item.id);
					});
					
					$.post('${pageContext.request.contextPath}/app/fav/fav-detail-catetag!update.htm', {ids: ids, remark: remark, key: '${param.key}'}, function(data){
						if (data.success){
							$(_records).each(function(i, item){
								$('#grid').datagrid('updateRow',{
									index: $('#grid').datagrid('getRowIndex', item),
									row: {
										remark: remark
									}
								});
							});
							
							$('#btnCancel').click();
						}else{
							alert(data.message);
						}
					}, 'json');
				});
				$('#btnCancel').click(function(){
					$('#remarkPanel').window('close');
				});
				
				
				//收藏夹
				$('#btnNewFolder').click(function(){
					$('#btnFolderOk').attr('_id', '');
					$('input[name="folderName"]').val('');
					$('#folderPanel').window('open');
				});
				$('#btnEditFolder').click(function(){
					var node = $('#tree').tree('getSelected');
					if (node==null){
						alert('请选择收藏夹');
						return false;
					}
					$('#btnFolderOk').attr('_id', node.id);
					$('input[name="folderName"]').val(node.text);
					$('#folderPanel').window('open');
				});
				$('#btnDelFolder').click(function(){
					var node = $('#tree').tree('getSelected');
					if (node==null){
						alert('请选择待删除的收藏夹');
						return false;
					}
					
					$.post('${pageContext.request.contextPath}/app/fav/fav-folder!check.htm', {id: node.id}, function(data){
						if (data.result==1){
							if (window.confirm(data.msg)){
								$.post('${pageContext.request.contextPath}/app/fav/fav-folder!delete.htm', {id: node.id}, function(data){
									if (data.result==1){
										$('#tree').tree('remove', node.target);
									}else{
										alert(data.msg);
									}
								}, 'json');
							}
						}else{
							alert(data.msg);
						}
					}, 'json');
				});
				$('#btnFolderOk').click(function(){
					var _id = $(this).attr('_id'); //如果为空，则表示新增，否则为编辑当前节点
					var folderName = $('input[name="folderName"]').val();
					if (folderName==''){
						alert('收藏夹名称不能为空');
						return false;
					}
					var node = $('#tree').tree('getSelected')
					var nodeId = '';
					var _parent = null;
					if (node==null){
						nodeId = '';
					}else{
						nodeId = node.id;
						_parent = node.target;
					}
					$.post('${pageContext.request.contextPath}/app/fav/fav-folder!save.htm', {parentId: nodeId, id: _id, name: folderName, key: '${param.key}'}, function(data){
						var id = data.message;
						//编辑当前收藏夹名称
						if (_id!=''){
							$('#tree').tree('update', {
								target: _parent,
								text: folderName
							})
							return false;
						}
						//新增收藏夹
						$('#tree').tree('append', {
							parent: _parent,
							data: [{
								id: id,
								text: folderName
							}]
						});
					}, 'json');
					
					$('#btnFolderCancel').click();
				});
				//取消新增收藏夹
				$('#btnFolderCancel').click(function(){
					$('#folderPanel').window('close');
				});
				//确定新建收藏夹
				$('input[name="folderName"]').keyup(function(e){
					if (e.keyCode==13){
						$('#btnFolderOk').click();
					}
				});
				//刷新
				$('#btnRefresh').click(function(){
					location.reload();
				});
			});
		</script>
	</head>

	<body>
		<div id="cc" class="easyui-layout" data-options="fit:true">
		    <div data-options="region:'west',title:false,split:true" style="width:200px;">
		    	<div id="tree"></div>
		    </div>
		    <div data-options="region:'center',title:false" style="padding:1px">
		    	<div style="width:100%;position:fixed;z-index:999;background-color:#ffffff;">
		    		<div>
		    			<a id="btnRefresh" href="javascript:void(0);" class="easyui-linkbutton">刷新</a>
		    			<a id="btnRemark" href="javascript:void(0);" class="easyui-linkbutton">修改备注</a>
		    			<a id="btnMove" href="javascript:void(0);" class="easyui-linkbutton">移动</a>
		    			<a id="btnDel" href="javascript:void(0);" class="easyui-linkbutton">删除</a>
		    		</div>
		    	</div>
		    	<!--
		    	<div id="grid" rownumbers="true" pagination="true" data-options="toolbar:'#tb-outbox',singleSelect:false">
		    	-->
		    	<div style="margin-top:30px;">
		    		<div id="grid">
			    	</div>
		    	</div>
		    </div>
		</div>
		
		<div id="remarkPanel">
        	<table width="100%" height="100%" class="box" cellpadding="1" cellspacing="1">
        		<tr>
        			<td><input type="text" name="name" style="width:92%;"/></td>
        		</tr>
        		<tr>
        			<td align="right">
						<a id="btnOk" href="javascript:void(0);" class="easyui-linkbutton">确定</a>
			    		<a id="btnCancel" href="javascript:void(0);" class="easyui-linkbutton">取消</a>
					</td>
        		</tr>
        	</table>
        </div>
        
        <div id="mm" class="easyui-menu" style="width:120px;">
        	<div id="btnNewFolder">新建</div>
        	<div id="btnEditFolder">编辑</div>
        	<div id="btnDelFolder">删除</div>
        </div>
        
        <div id="folderPanel">
        	<table width="100%" height="100%" class="box" cellpadding="1" cellspacing="1">
        		<tr>
        			<td><input type="text" name="folderName" style="width:92%;"/></td>
        		</tr>
        		<tr>
        			<td align="right">
						<a id="btnFolderOk" href="javascript:void(0);" class="easyui-linkbutton">确定</a>
			    		<a id="btnFolderCancel" href="javascript:void(0);" class="easyui-linkbutton">取消</a>
					</td>
        		</tr>
        	</table>
        </div>
	</body>
</html>
