<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>请选择收藏夹</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css"/>
		<style type="text/css">
			body{
				padding: 3px;
				font-size: 12px;
			}
			.panel-header, .panel-body{
				border: 0px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->

		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//your code here ...
				var _timeout = null;
				$('#tree').tree({
					url: '${pageContext.request.contextPath }/app/fav/fav-folder!loadData.htm?key=${param.key }',
					onDblClick: function(node){
						$('#tree').tree('expand', node.target);
					}
				});
				
				//保存
				$('#btnOk').click(function(){
					var node = $('#tree').tree('getSelected');
					if (node==null){
						alert('请选择收藏夹');
						return false;
					}
					var params = opener.__kbs_fav_params;
					var ids = [];
					$(params).each(function(i, item){
						ids.push(item);
					});
					var url = '${pageContext.request.contextPath}/app/fav/fav-detail!move.htm';
					if('${param.key }' == 'CATETAG'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-catetag!move.htm'
					}else if('${param.key }' == 'KBVALUE'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-kb-value!move.htm'
					}else if('${param.key }' == 'BULLETIN'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-bulletin!move.htm'
					}
					$.post(url, {pid: node.id, ids: ids, key: '${param.key}'}, function(data){
						alert(data.message);
						opener.removeSelections();
						$('#btnCancel').click();
					}, 'json');
				});
				$('#btnCancel').click(function(){
					window.close();
				});
			});
		</script>
	</head>

	<body>
		<table width="100%" class="box" cellpadding="1" cellspacing="1">
			<tr>
				<td><b>请选择收藏夹</b></td>
			</tr>
			<tr>
				<td>
					<div id="tree" style="height:200px;overflow:auto;"></div>
				</td>
			</tr>
			<tr>
				<td align="right">
					<a id="btnOk" href="javascript:void(0);" class="easyui-linkbutton">确定</a>
		    		<a id="btnCancel" href="javascript:void(0);" class="easyui-linkbutton">取消</a>
				</td>
			</tr>
		</table>
	</body>
</html>
