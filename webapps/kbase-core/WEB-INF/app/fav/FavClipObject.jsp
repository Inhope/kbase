<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE >
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip-object.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >  
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/fav/js/FavClipObject.js"></script>
	</head>

	<body>
		<!-- 
		<div class="content_right_bottom">
			<div class="zhishi_titile">
				<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/images/zhishi_ico.jpg" width="16" height="15" />
				<a href="${pageContext.request.contextPath}/app/fav/fav-clip.htm">收藏夹列表</a>
				<a href="javascript:void(0);" onclick="javascript:window.location.reload();">
					<s:property value="#request.faviconClip.favClipName"/>
				</a>
				<button>删除</button>
			</div>


			<div class="zhishi_con">
				<div class="zhishi_con_title">
					<h1>
						<input type="checkbox">
					</h1>
					<h2>
						<b>名称</b>
					</h2>
					<h3>
						收藏时间
					</h3>
				</div>
				<div class="zhishi_con_nr">
					<ul>
						<s:iterator var="fco" value="#request.faviconClipObjects">
							<s:if test="#fco.kbObject != null">
								<li>
									<h1>
										<input id="<s:property value="#fco.id"/>" type="checkbox">
									</h1>
									<h2>
										<b id="${fco.kbObject.objectId}" title='<s:property value="#fco.kbObject.name"/>'><s:property value="#fco.kbObject.name"/></b>
									</h2>
									<h3>
										<s:date name="#fco.createTime" format="yyyy-MM-dd HH:mm:ss"/>
									</h3>
								</li>
							</s:if>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>
		 -->
		 
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="5%"></td>
						<td width="15%"></td>
						<td width="60%"></td>
						<td width="20%"></td>
					</tr>
					<tr>
						<td colspan="4" align="right" valign="middle" style="text-align:right;">
							<b style="float:left;padding-left: 20px;">当前位置：${faviconClip.favClipName }</b>
							<div class="gonggao_titile_right">
								<a href="javascript:void(0);" id="btnDel">删除</a>
								<a href="javascript:void(0);" id="btnBack">返回</a>
							</div>
						</td>
					</tr>
					<tr class="tdbg">
						<td><input type="checkbox" id="selAll" /></td>
						<td width="50px;">类型</td>
						<td>名称</td>
						<td>收藏时间</td>
					</tr>
					<s:iterator var="fco" value="#request.faviconClipObjects">
						<tr>
							<td><input id="<s:property value="#fco.id"/>" type="checkbox" xname='childCheckBox'></td>
							<td>
								<s:if test="#fco.type == 1">标准问</s:if><s:else>实例</s:else>
							</td>
							<td>
								<s:if test="#fco.name != null && #fco.name != ''">
									<b id="${fco.objectId}" title='<s:property value="#fco.name"/>' 
										<s:if test="#fco.type == 1">xname="kbQuestion"</s:if><s:else>xname="kbObject"</s:else> >
										<s:property value="#fco.name"/>
									</b>
								</s:if>
								<s:else>
									<b style="color:red;" title="收藏的实例已被删除，建议删除此条数据！">已被删除</b>
								</s:else>
							</td>
							<td>
								<s:date name="#fco.createTime" format="yyyy-MM-dd HH:mm:ss"/>
							</td>
						</tr>
					</s:iterator>
				</table>
				
			</div>
		</div>
	</body>
</html>
