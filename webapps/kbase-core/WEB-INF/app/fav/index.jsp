<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>请选择收藏夹</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/default/easyui.css"/>
		<style type="text/css">
			body{
				padding: 3px;
				font-size: 12px;
			}
			.panel-header, .panel-body{
				border: 0px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->

		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//your code here ...
				
				$('#folderName').window({
					title: '请输入收藏夹名称',
				    width: 400,
				    height: 150,
				    modal: false,
				    collapsible: false,
				    minimizable: false,
				    closed: true,
				    maximizable: false
				});
				
				var _timeout = null;
				$('#tree').tree({
					url: '${pageContext.request.contextPath }/app/fav/fav-folder!loadData.htm?key=${param.key }',
					onContextMenu: function(e, node){
						e.preventDefault();
						// select the node
						$('#tree').tree('select', node.target);
						// display context menu
						$('#mm').menu('show', {
							left: e.pageX,
							top: e.pageY
						});
					},
					onClick: function(node){
						if (_timeout!=null){
							window.clearTimeout(_timeout);
							_timeout = null;
						}
					
						_timeout = window.setTimeout(function(){
							
							//刷新右侧的grid
							
						}, 300); 
					},
					onDblClick: function(node){
						if (_timeout!=null){
							window.clearTimeout(_timeout);
							_timeout = null;
						}
						
						$('#tree').tree('expand', node.target);
					}
				});
				
				//新增收藏夹
				$('#btnNewRoot, #btnNew, #btnEdit').click(function(){
					if ($(this).attr('id')=='btnEdit'){
						var node = $('#tree').tree('getSelected');
						if (node!=null){
							$('input[name="name"]').val(node.text);
							$('#btnFolderOk').attr('_id', node.id);
						}
					}else{
						$('input[name="name"]').val('');
						$('#btnFolderOk').attr('_id', '');
					}
					$('#folderName').window('open');
				});
				
				//删除收藏夹
				$('#btnDel').click(function(){
					var node = $('#tree').tree('getSelected');
					if (node==null){
						alert('请选择待删除的收藏夹');
						return false;
					}
					
					$.post('${pageContext.request.contextPath}/app/fav/fav-folder!check.htm', {id: node.id}, function(data){
						if (data.result==1){
							if (window.confirm(data.msg)){
								$.post('${pageContext.request.contextPath}/app/fav/fav-folder!delete.htm', {id: node.id}, function(data){
									if (data.result==1){
										$('#tree').tree('remove', node.target);
									}else{
										alert(data.msg);
									}
								}, 'json');
							}
						}else{
							alert(data.msg);
						}
					}, 'json');
				});
				
				//保存
				$('#btnOk').click(function(){
					var node = $('#tree').tree('getSelected');
					if (node==null){
						alert('请选择收藏夹');
						return false;
					}
					var params = opener.__kbs_fav_params;
					if (params.length==0){
						alert('请选择待收藏的数据');
						return false;
					}
					
					var datas = '';
					//IE7 下 JSON.stringify 解析数组存在问题
					if (navigator.isLowerBrowser()){
						var tmpString = '';
						$(params).each(function(i, item){
							//tmpString += JSON.stringify(item) + ',';
							var question = item.question.replace(/\"/gi, '');
							var answer = item.answer.replace(/\"/gi, '').replace(/\n/g, '');
							var name = item.name.replace(/\"/gi, '').replace(/\n/g, '');
							var title = item.title.replace(/\"/gi, '').replace(/\n/g, '');
							var content = item.content.replace(/\"/gi, '').replace(/\n/g, '');
							var locations = item.locations.replace(/\"/gi, '').replace(/\n/g, '');
							tmpString += '{"id":"' + item.id + '", "cateid":"' + item.cateid + '", "question":"' +  question + '", "locations":"' +  locations + 
										 '", "content":"' +  content + '", "name":"' +  name + '", "title":"' +  title + '", "answer": "' + answer + '"},';
						});
						tmpString = tmpString.substring(0, tmpString.length-1);
						tmpString = '[' + tmpString + ']';
						datas = tmpString;
					}else{
						datas = JSON.stringify(params);
					}
					var url = '${pageContext.request.contextPath}/app/fav/fav-detail!save.htm';
					if('${param.key}' == 'CATETAG'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-catetag!save.htm'
					}else if('${param.key}' == 'KBVALUE'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-kb-value!save.htm'
					}else if('${param.key}' == 'BULLETIN'){
						url = '${pageContext.request.contextPath}/app/fav/fav-detail-bulletin!save.htm'
					}
					$.post(url, {id: node.id, datas: datas, key: '${param.key}'}, function(data){
						if (data.success){
							if (data.message!=''){
								alert('已经收藏，本次不做重复收藏');
							}else{
								alert('收藏成功！');
							}
							$('#btnCancel').click();
						}else{
							alert("收藏失败！");
						}
					}, 'json');
				});
				$('#btnCancel').click(function(){
					window.close();
				});
				
				//确定新建收藏夹
				$('input[name="name"]').keyup(function(e){
					if (e.keyCode==13){
						$('#btnFolderOk').click();
					}
				});
				//确定新建收藏夹
				$('#btnFolderOk').click(function(){
				
					var _id = $(this).attr('_id'); //如果为空，则表示新增，否则为编辑当前节点
					
					var folderName = $('input[name="name"]').val();
					if (folderName==''){
						alert('收藏夹名称不能为空');
						return false;
					}
					
					var node = $('#tree').tree('getSelected')
					var nodeId = '';
					var _parent = null;
					if (node==null){
						nodeId = '';
					}else{
						nodeId = node.id;
						_parent = node.target;
					}
					
					$.post('${pageContext.request.contextPath}/app/fav/fav-folder!save.htm', {parentId: nodeId, id: _id, name: folderName, key: '${param.key}'}, function(data){
						var id = data.message;
						//编辑当前收藏夹名称
						if (_id!=''){
							$('#tree').tree('update', {
								target: _parent,
								text: folderName
							})
							return false;
						}
						//新增收藏夹
						$('#tree').tree('append', {
							parent: _parent,
							data: [{
								id: id,
								text: folderName
							}]
						});
					}, 'json');
					
					
					$('#btnFolderCancel').click();
				});
				
				//新建收藏夹关闭
				$('#btnFolderCancel').click(function(){
					$('#folderName').window('close');
				});
			});
		</script>
	</head>

	<body>
		<table width="100%" class="box" cellpadding="1" cellspacing="1">
			<tr>
				<td><b>请选择收藏夹</b></td>
			</tr>
			<tr>
				<td>
					<div id="tree" style="height:300px;overflow:auto;"></div>
				</td>
			</tr>
			<tr>
				<td align="right">
					<a id="btnNewRoot" href="javascript:void(0);" class="easyui-linkbutton">新建收藏夹</a>
					<a id="btnOk" href="javascript:void(0);" class="easyui-linkbutton">确定</a>
		    		<a id="btnCancel" href="javascript:void(0);" class="easyui-linkbutton">取消</a>
				</td>
			</tr>
		</table>
		<div id="mm" class="easyui-menu" style="width:120px;">
        	<div id="btnNew">新建</div>
        	<div id="btnEdit">编辑</div>
        	<div id="btnDel">删除</div>
        </div>
        <div id="folderName">
        	<table width="100%" height="100%" class="box" cellpadding="1" cellspacing="1">
        		<tr>
        			<td><input type="text" name="name" style="width:92%;"/></td>
        		</tr>
        		<tr>
        			<td align="right">
						<a id="btnFolderOk" href="javascript:void(0);" class="easyui-linkbutton">确定</a>
			    		<a id="btnFolderCancel" href="javascript:void(0);" class="easyui-linkbutton">取消</a>
					</td>
        		</tr>
        	</table>
        </div>
	</body>
</html>
