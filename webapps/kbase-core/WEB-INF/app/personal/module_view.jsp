<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String key = request.getParameter("key");
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>业务模块</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			table.box {
				border: 0px;
			}
			table.box tr td{
				border-right: 0px;
				color: #666666;
			}
			.hander{
				cursor: pointer;
			}
			.hander:HOVER {
				color: red;
			}
			.gonggao_con_nr input{
				border: 1px solid #DDDDDD;
				height: 18px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/jquery.easyui.portal.css"/>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.portal.js"></script>
		<script type="text/javascript">
			var SystemKeys = parent.parent.SystemKeys;
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
		
		<script type="text/javascript">
			var keyArr = ['approveList', 'applyList', 'applyFinishList', 'handleList', 'handleFinishList'];
		
			var _key = '${param.key}';
			var _moduleId = '${param.moduleid}';
			
			//分页跳转
			function pageClick(pageNo){
				_forward(_key, _moduleId, '&pageNo=' + pageNo + _getQueryString());
			}
			
			$(function(){
			
				//jeasyui tab select event
				$('#tt').tabs({
					onSelect: function(title, index){
						var _tab = $('#tt').tabs('getTab', title);
						var key = $(_tab).attr('id');
						_forward(key, _moduleId);
					}
				});
				
				//点击标题打开文档链接
				$('span[_eflowdoc=1]').live('click', function(){
					var _this = this;
					var id = $(_this).attr('_requestid');
					$.workflow.editDocument(id, parent.parent.$);
				});
				
				//doQuery
				$('#btnQuery').live('click', function(){
					_forward(_key, _moduleId, _getQueryString());
				});
				//doReset
				$('#btnReset').live('click', function(){
					$('#create_time').val('');
					$('#end_time').val('');
					$('#user_name').val('');
					$('#eflowIdPicker').combobox('setValue', '');
				});
				
				
				//default tab content
				/*
				var _content = $('div.gonggao_con').html();
				$('div.gonggao_con').remove();
				$('#'+_key).html(_content);
				*/
				$('#'+_key).append($('div.gonggao_con'));
			});
			
			/**
			 * module combobox change select event
			 */
			function _onSelect(record){
				_forward(_key, record.value);
			}
			/**
			 * forward
			 */
			function _forward(key, moduleId, param){
				param = param==undefined?'':param;
				layer.load('请稍候...');
				location.href = $.fn.getRootPath() + '/app/auther/work-flow!moduleview.htm?key=' + key + '&moduleid=' + moduleId + param;
			}
			/**
			 * get query condition
			 */
			function _getQueryString(){
				var createTime = $('#create_time').val();
				var endTime = $('#end_time').val();
				var userName = $('#user_name').val();
				var eflowId = $('#eflowIdPicker').combobox('getValue');
				return '&createtime=' + createTime + '&endtime=' + endTime + '&username=' + userName + '&eflowid=' + eflowId;
			}
		</script>
	</head>

	<body>
		<div id="tt" class="easyui-tabs" style="height:560px;" data-options="tools:'#modulePicker'">
			<div title="待我审核${requestScope.approveCount }" id="approveList" data-options="selected:${param.key eq 'approveList'}"></div>
			<div title="申请待审批${requestScope.applyCount }" id="applyList" data-options="selected:${param.key eq 'applyList'}"></div>
			<div title="申请已归档${requestScope.applyFinishCount }" id="applyFinishList" data-options="selected:${param.key eq 'applyFinishList'}"></div>
			<div title="处理待审批${requestScope.handleCount }" id="handleList" data-options="selected:${param.key eq 'handleList'}"></div>
			<div title="处理已归档${requestScope.handleFinishCount }" id="handleFinishList" data-options="selected:${param.key eq 'handleFinishList'}"></div>
		</div>
		
		<!-- 主内容 -->
		<div class="gonggao_con" style="margin:0px;">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="3" style="text-align: left;">
							&nbsp;开始时间：<input type="text" readonly="readonly" onclick="WdatePicker({maxDate:'%y-%M-%d'})" value="${requestScope.createtime }" name="create_time" id="create_time" class="Wdate" style="width:100px;">
							结束时间：<input type="text" readonly="readonly" onclick="WdatePicker({maxDate:'%y-%M-%d',minDate:'#F{$dp.$D(\'create_time\')}'})" value="${requestScope.endtime }" name="end_time" id="end_time" class="Wdate" style="width:100px;">
							创建者：<input type="text" name="user_name" value="${requestScope.username }" id="user_name" style="width:100px;">
							流程类型：
								<select class="easyui-combobox" id="eflowIdPicker" data-options="editable:false">
									<option value="">--请选择--</option>
									<c:forEach items="${requestScope.eflowIdJsonArr}" var="item">
										<c:choose>
											<c:when test="${item.id eq requestScope.eflowid}">
												<option value="${item.id}" selected="selected">${item.name}</option>
											</c:when>
											<c:otherwise>
												<option value="${item.id}">${item.name}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
						</td>
						<td colspan="2" valign="middle">
							<div class="gonggao_titile_right" style="margin-top:-6px;">
								<a href="javascript:void(0);" id="btnReset">重置</a>
								<a href="javascript:void(0);" id="btnQuery">查询</a>
							</div>
						</td>
					</tr>
					<tr class="tdbg">
						<td width="40%">知识标题</td>
						<td width="25%">流程类型</td>
						<td width="15%">当前节点</td>
						<td width="10%">开始时间</td>
						<td width="10%">创建者</td>
					</tr>
					<c:forEach items="${requestScope.list}" var="item">
						<tr>
							<td><span class="hander" _eflowdoc="1" _requestid="${item.id }">${item.requestname }</span></td>
							<td>${item.workflowname }</td>
							<td>${item.currentnodename }</td>
							<td>${item.createdate }</td>
							<td>${item.createrName }</td>
						</tr>
					</c:forEach>
					<tr class="trfen">
						<td colspan="5">
							<jsp:include page="../util/part_fenye.jsp" flush="true"></jsp:include>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<!-- 主内容 -->
		
		<!-- 模块选项 -->
		<div id="modulePicker" >
			<select class="easyui-combobox" data-options="editable:false,onSelect:_onSelect">
				<c:forEach items="${requestScope.moduleList}" var="item">
					<c:choose>
						<c:when test="${item.id eq param.moduleid}">
							<option value="${item.id }" selected>${item.objname }</option>
						</c:when>
						<c:otherwise>
							<option value="${item.id }">${item.objname }</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</div>
		<!-- 模块选项 -->
	</body>
</html>
