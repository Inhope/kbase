<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>待办事宜模块</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			table.box {
				border: 0px;
			}
			table.box tr td{
				border-right: 0px;
				color: #666666;
			}
			.hander{
				cursor: pointer;
			}
			.hander:HOVER {
				color: red;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/jquery.easyui.portal.css"/>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.portal.js"></script>
		<script type="text/javascript">
			var SystemKeys = parent.parent.SystemKeys;
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.workflow.js"></script>
		
		<script type="text/javascript">
			$(function(){
				//jeasyui portal initialize
				$('#pp').portal({
					border: false,
					fit: false
				});
				//jeasyui tab select event
				$('#tt').tabs({
					onSelect: function(title, index){
						var _tab = $('#tt').tabs('getTab', title);
						var moduleId = $(_tab).attr('_moduleid');
						layer.load('请稍候...');
						location.href = $.fn.getRootPath() + '/app/auther/work-flow!todoview.htm?moduleid=' + moduleId;
					}
				});
				//点击标题打开文档链接
				$('span[_eflowdoc=1]').click(function(){
					var _this = this;
					var id = $(_this).attr('_requestid');
					$.workflow.editDocument(id, parent.parent.$);
				});
				//单击更多
				$('span[_key$="List"]').click(function(){
					var _key = $(this).attr('_key');
					var _moduleId = $(this).attr('_moduleid');
					layer.load('请稍候...');
					location.href = $.fn.getRootPath() + '/app/auther/work-flow!moduleview.htm?key=' + _key + '&moduleid=' + _moduleId;
				});
			});
		</script>
	</head>

	<body>
		<div id="tt" class="easyui-tabs" style="height:560px;">
			<c:forEach items="${requestScope.moduleList}" var="item">
				<c:choose>
					<c:when test="${requestScope.moduleId eq item.id}">
						<div title="${item.objname }" _moduleid="${item.id }" style="padding:10px" data-options="selected:true">
							<div id="pp" style="overflow: hidden;">
								<div style="width:33%;">
							    	<div title="待我审核" style="height:230px;text-align:center;">
							    		<table class="box" style="width:100%;">
											<c:forEach items="${requestScope.approveList}" var="var">
										        <tr>
									            	<td width="70%" align="left"><span class="hander" _eflowdoc="1" _requestid="${var.id }">${var.requestname }</span></td>
									            	<td width="30%">${var.createdate }</td>
									            </tr>
											</c:forEach>
											<c:if test="${fn:length(requestScope.approveList)>0}">
												<tr>
									            	<td colspan="2" align="right" style="border-bottom: 0px;"><span class="hander" _key="approveList" _moduleid="${item.id }">更多</span></td>
									            </tr>
											</c:if>
										</table>
									</div>
									<div title="处理待审批" style="height:230px;text-align:center;">
										${var.createdate }
										<table class="box" style="width:100%;">
											<c:forEach items="${requestScope.handleList}" var="var">
												 <tr>
									            	<td width="70%" align="left"><span class="hander" _eflowdoc="1" _requestid="${var.id }">${var.requestname }</span></td>
									            	<td width="30%">${var.createdate }</td>
									            </tr>
											</c:forEach>
											<c:if test="${fn:length(requestScope.handleList)>0}">
												<tr>
									            	<td colspan="2" align="right" style="border-bottom: 0px;"><span class="hander" _key="handleList" _moduleid="${item.id }">更多</span></td>
									            </tr>
											</c:if>
										</table>
									</div>
							    </div>
				          		<div style="width:33%;">
				    				<div title="申请待审批" style="height:230px;text-align:center;">
										<table class="box" style="width:100%;">
											<c:forEach items="${requestScope.applyList}" var="var">
												 <tr>
									            	<td width="70%" align="left"><span class="hander" _eflowdoc="1" _requestid="${var.id }">${var.requestname }</span></td>
									            	<td width="30%">${var.createdate }</td>
									            </tr>
											</c:forEach>
											<c:if test="${fn:length(requestScope.applyList)>0}">
												<tr>
									            	<td colspan="2" align="right" style="border-bottom: 0px;"><span class="hander" _key="applyList" _moduleid="${item.id }">更多</span></td>
									            </tr>
											</c:if>
										</table>
									</div>
									<div title="处理已归档" style="height:230px;text-align:center;">
										<table class="box" style="width:100%;">
											<c:forEach items="${requestScope.handleFinishList}" var="var">
												 <tr>
									            	<td width="70%" align="left"><span class="hander" _eflowdoc="1" _requestid="${var.id }">${var.requestname }</span></td>
									            	<td width="30%">${var.createdate }</td>
									            </tr>
											</c:forEach>
											<c:if test="${fn:length(requestScope.handleFinishList)>0}">
												<tr>
									            	<td colspan="2" align="right" style="border-bottom: 0px;"><span class="hander" _key="handleFinishList" _moduleid="${item.id }">更多</span></td>
									            </tr>
											</c:if>
										</table>
									</div>
							    </div>
							    <div style="width:34%;">
							    	<div title="申请已归档" style="height:230px;text-align:center;">
										<table class="box" style="width:100%;">
											<c:forEach items="${requestScope.applyFinishList}" var="var">
												 <tr>
									            	<td width="70%" align="left"><span class="hander" _eflowdoc="1" _requestid="${var.id }">${var.requestname }</span></td>
									            	<td width="30%">${var.createdate }</td>
									            </tr>
											</c:forEach>
											<c:if test="${fn:length(requestScope.applyFinishList)>0}">
												<tr>
									            	<td colspan="2" align="right" style="border-bottom: 0px;"><span class="hander" _key="applyFinishList" _moduleid="${item.id }">更多</span></td>
									            </tr>
											</c:if>
										</table>
									</div>
							    </div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div title="${item.objname }" _moduleid="${item.id }" style="padding:10px"></div>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</body>
</html>
