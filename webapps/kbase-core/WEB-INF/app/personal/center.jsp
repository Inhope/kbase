<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>个人中心</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css" />
		
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		
		<style type="text/css">
			body{
				margin: 1px;
			}
			.tooltip{
				padding: 1px;
			}
			.l-btn-text{
				color: #fff;
			}
			#points{
				background: #44BE78;
			}
			.m-btn-plain-active,
			.s-btn-plain-active {
			  background-color: #44BE78;
			}
			.l-btn-plain:hover {
			  background: #44BE78;
			}
			.m-btn-downarrow,
			.s-btn-downarrow {
			  background: url('${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/images/menu_arrows_green.png') no-repeat 0 center;
			}
			.menu-active {
			  background: #44BE78;
			}
		</style>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript">
		   //////////////////////////////////////////////////////////////
		   //针对"我的空间"模块关闭时提示用户保存，重写TABOBJECT的close方法  modify by heart.cao 2016-02-19
		   var EXT_TABOBJECT = $.extend(parent.TABOBJECT,{
		       'close' : function(obj) {
					if(!(obj.index > -1)) {
						obj.index = this.exists(obj.id);
					}
					var exists_unsaved = this.tabContent.children(':eq(' + obj.index + ')')[0].contentWindow.exists_unsaved;
					if(exists_unsaved==1){
				        if(!confirm('是否强行关闭，当前未保存内容将丢失!'))return false;       
				    }
					if(obj.index > -1) {
						this.tabTitle.children(':eq(' + obj.index + ')').remove();
						this.tabContent.children(':eq(' + obj.index + ')').remove();
						this.activity({
							index : obj.index - 1				
						});
						this.existTabs.splice(obj.index, 1);
					}
				}
		   });
		   //////////////////////////////////////////////////////////////
			/**
			 * 导航扩展方法
			 * 在 individual_left ul 下增加 <li>
			 * 在 document.ready 中增加入口地址即可
			 */
			$(function(){
			
				//导航条单击样式切换
				$('div.individual_left ul li a').click(function(){
					//修改导航Logo样式
					var _this = this;
					var _id = $(_this).attr('id');
					var _class = $(_this).attr('class');
					if (!_class.endWithIgnoreCase('-active')){
						$(_this).removeClass().addClass(_class + '-active');
					}
					//修改数字显示样式
					if ($(_this).find('div').length>0){
						var _numClass = $(_this).find('div').attr('class');
						if (!_numClass.endWithIgnoreCase('-active')){
							$(_this).find('div').removeClass().addClass(_numClass + '-active');
						}
					}
					//修改其他的导航的样式
					var _active = $('div.individual_left ul li a[class$="-active"][id!="'+_id+'"]');
					if (_active.length>0){
						var _activeClass = _active.attr('class');
						//修改数字显示样式
						if (_active.find('div').length>0){
							var _numClass = _active.find('div').attr('class');
							if (_numClass.endWithIgnoreCase('-active')){
								_active.find('div').removeClass().addClass(_numClass.substring(0, _numClass.length-7));
							}
						}
						//修改导航Logo
						_active.removeClass().addClass(_activeClass.substring(0, _activeClass.length-7));
					}
				});
				
				
				//待办事宜
				$('#navTodo').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/auther/work-flow!todoview.htm');
				});
				//我的任务
				$('#navMyTask').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/task/pe-task.htm');
				});
				//推荐知识
				$('#navKb').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/recommend/recommend.htm?flag=owner');
				});
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				//////////////公告、便签 分 入口  modify by heart.cao 2016.1.4  ////////////////////////////////////
				var _tObj = parent.TABOBJECT;
		        try{
		           if(parent.parent.TABOBJECT!=undefined && parent.parent.TABOBJECT!=null)_tObj=parent.parent.TABOBJECT;
		        }catch(e){}
				//公告列表
				$('#navNotice').click(function(){
					//modify by eko.zhan at 2016-01-19
					var enableCateTag = '${model.enableCateTag}';
					var _isopen = $('#navNotice').attr('_isopen');
					//modify by eko.zhan at 2016-01-27 广东移动模式启用新公告管理
					if (enableCateTag=='true'){
						$('#navNotice').attr('_isopen', 1);
					/*
						var _diaWidth = screen.width;
						var _diaHeight = screen.height;
						var _scrWidth = screen.width;
						var _scrHegiht = screen.height;
						var _diaLeft = (_scrWidth-_diaWidth)/2;
						var _diaTop = (_scrHegiht-_diaHeight)/2;
						var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0';
						window.open('${pageContext.request.contextPath}/app/notice/bulletin.htm', '_blank', params);
						*/
						//modify by eko.zhan at 2016-08-04 13:43 
						if ($(parent.document).find('#navBulletin').length>0){
							$(parent.document).find('#navBulletin').get(0).click();
						}else{
							var _diaWidth = screen.width-50;
							var _diaHeight = screen.height-100;
							var _scrWidth = screen.width;
							var _scrHegiht = screen.height;
							var _diaLeft = (_scrWidth-_diaWidth)/2;
							var _diaTop = (_scrHegiht-_diaHeight)/2;
							var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0, resizable=1, status=0, fullscreen=0';
							window.open($.fn.getRootPath() + '/app/notice/bulletin.htm', '_blank', params);
						}
					}else{
						_tObj.open({
							id : 'notice',
							name : '公告管理',
							hasClose : true,
							url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isVirgin=1&isNote=0',
							isRefresh : true
						}, this);
					}
				});
				//便签列表
				$('#navNote').click(function(){
				    _tObj.open({
						id : 'note',
						name : '便签管理',
						hasClose : true,
						url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote=1',
						isRefresh : true
					}, this);
				});		
				//////////////////////////////////////////////////////////////////////////////////////////////////////////		
				//我的空间
				$('#navNotepad').click(function(){
					//$('#content').attr('src', $.fn.getRootPath() + '/app/notice/notepad!view.htm');
					EXT_TABOBJECT.open({
						id : 'notepad',
						name : '我的空间',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/notice/notepad!view.htm',
						isRefresh : true
					}, this);
				});				
				//主题设置
				$('#navTheme').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/auther/index-setting.htm');
				});
				//用户设置
				$('#navUserCenter').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/individual/individual!getUserInfo.htm');
				});
				
				
				//首次进入默认选中(a标签标记为first->待办事务->用户设置)
				var first = $('#cc').find('a[first]');
				if(first != null && first.length>0) {
					$(first).click();
				} else {
					if ($('#navTodo').length > 0){
						$('#navTodo').click();
					}else{
						$('#navUserCenter').click();
					}
				}
				
				//积分模块显示
				var enablePoints = '${model.enablePoints}';
				if (enablePoints=='true'){
					//
					$('#imgheadinfo').tooltip({
						hideEvent: 'none',
	                    content: function(){
	                        return $('#points');
	                    },
	                    onShow: function(){
	                        var t = $(this);
	                        t.tooltip('tip').focus().unbind().bind('blur',function(){
	                            t.tooltip('hide');
	                        });
	                        ///*
	                        window.setTimeout(function(){
	                        	t.tooltip('hide');
	                        }, 5*1000);
	                        //*/
	                    }
					});
					$('#imgheadinfo').tooltip('show');
					
					///*
					$('#points a, #points-detail div[_detail="1"]').click(function(){
						parent.__kbs_layer_ind = parent.$.layer({
							type: 2,
						    shadeClose: false,
						    title: false,
						    closeBtn: [0, false],
						    shade: [0.3, '#000'],
						    border: [0],
						    area: ['1000px', '800px'],
						    iframe: {
						    	src: $.fn.getRootPath() + "/app/point/p-log!finddetailinfo.htm?log.userid=${sessionScope.session_user_key.id}",
						    	scrolling: 'no'
						    }
						});
						
					});
					//*/
				}
				
				//我的学习
				$('#navLearning').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/learning/learn.htm');
				});
				
			});	//end document.ready
		</script>
	</head>

	<body>
		
		<div style="display:none">
			<div id="points" class="easyui-panel" style="padding:0px;" data-options="fit:true">
        		<a href="#" class="easyui-menubutton" data-options="menu:'#points-detail'" _id="${model.points[0].id }">${model.points[0].name }： ${model.points[0].points }</a>
			</div>
			<div id="points-detail">
		        <c:forEach items="${model.points}" var="item" begin="1">
		        	<div _id="${item.id }" _detail="1">${item.name }： ${item.points }</div>
		        </c:forEach>
	        </div>
	    </div>
	    
	    
		
		<div class="easyui-layout" data-options="fit:false" id="cc" style="height:640px;">
			<div data-options="region:'west',split:true,title:false" style="width:78px;">
				<div class="individual_left">
					<ul>
						
						<li>
							<c:choose>
								<c:when test="${model.headImg==null}">
									<img id="imgheadinfo" src="${pageContext.request.contextPath}/resource/individual/images/individual/head_ico.jpg" />
								</c:when>
								<c:otherwise>
									<img id="imgheadinfo" src="${pageContext.request.contextPath}/app/individual/individual!getImg.htm" />
								</c:otherwise>
							</c:choose>
						</li>
						<kbs:if code="gzyd">
							<li>
							    <a id="navNotepad" class="individual_ico6" href="javascript:void(0);">我的空间</a>		
							</li>
						</kbs:if>
						<c:if test="${model.enableLearning}">
							<li>
								<a id="navLearning" class="individual_ico4" href="javascript:void(0);">我的学习</a>
							</li>
						</c:if>
						<c:if test="${model.isOpenWorkflow }">
							<li>
								<a id="navTodo" class="individual_ico1" href="javascript:void(0);">
									待办事务
									<c:choose>
										<c:when test="${model.countTodo==0}"></c:when>
										<c:when test="${model.countTodo>99}"><div class="individual_circle">99+</div></c:when>
										<c:when test="${model.countTodo>9}"><div class="individual_circle">${model.countTodo }</div></c:when>
										<c:otherwise><div class="individual_circle1">${model.countTodo }</div></c:otherwise>
									</c:choose>
								</a>
							</li>
						</c:if>
						<c:if test="${model.isOpenTask }">
							<li>
								<a id="navMyTask" class="individual_ico6" href="javascript:void(0);">
									我的任务 
									<c:choose>
										<c:when test="${model.countMyTask==0}"></c:when>
										<c:when test="${model.countMyTask>99}"><div class="individual_circle">99+</div></c:when>
										<c:when test="${model.countMyTask>9}"><div class="individual_circle">${model.countMyTask }</div></c:when>
										<c:otherwise><div class="individual_circle1">${model.countMyTask }</div></c:otherwise>
									</c:choose>
								</a>
							</li>
						</c:if>
						<li>
							<a id="navKb" class="individual_ico2" href="javascript:void(0);" 
								<kbs:if code="gzyd">first</kbs:if>>
								推荐知识 
								<c:choose>
									<c:when test="${model.countKb==0}"></c:when>
									<c:when test="${model.countKb>99}"><div class="individual_circle">99+</div></c:when>
									<c:when test="${model.countKb>9}"><div class="individual_circle">${model.countKb }</div></c:when>
									<c:otherwise><div class="individual_circle1">${model.countKb }</div></c:otherwise>
								</c:choose>
							</a>
						</li>
						<li>
							<a id="navNotice" class="individual_ico5" href="javascript:void(0);" _isopen="1">
								公告列表
								<c:choose>
									<c:when test="${model.countNotice==0}"></c:when>
									<c:when test="${model.countNotice>99}"><div class="individual_circle">99+</div></c:when>
									<c:when test="${model.countNotice>9}"><div class="individual_circle">${model.countNotice }</div></c:when>
									<c:otherwise><div class="individual_circle1">${model.countNotice }</div></c:otherwise>
								</c:choose>
							</a>
						</li>
						<s:if test="#session.activate_note==1">
							<li>
								<a id="navNote" class="individual_ico6" href="javascript:void(0);">
									便签列表
									<c:choose>
										<c:when test="${model.countNote==0}"></c:when>
										<c:when test="${model.countNote>99}"><div class="individual_circle">99+</div></c:when>
										<c:when test="${model.countNote>9}"><div class="individual_circle">${model.countNote }</div></c:when>
										<c:otherwise><div class="individual_circle1">${model.countNote }</div></c:otherwise>
									</c:choose>
								</a>
							</li>	
						</s:if>								
						<li>
							<a id="navTheme" class="individual_ico3" href="javascript:void(0);">主题设置 </a>
						</li>
						<li>
							<a id="navUserCenter" class="individual_ico4" href="javascript:void(0);">用户设置</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div data-options="region:'center',title:false">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border:0px;">
					<iframe id="content" src="" scrolling="no" frameborder="0" style="height:99%;width:99%;"></iframe>
				</div>
			</div>
	</body>
</html>
