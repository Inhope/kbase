<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%@page import="com.eastrobot.util.WorkflowUtils"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>个人中心</title>
		<!-- 另一种积分展示样式，勿删 by eko.zhan at 2015-07-29 -->

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/wheelmenu.css" />
		
		<style type="text/css">
			body{
				margin: 1px;
			}
			
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript">
			/**
			 * 导航扩展方法
			 * 在 individual_left ul 下增加 <li>
			 * 在 document.ready 中增加入口地址即可
			 */
			$(function(){
			
				//导航条单击样式切换
				$('div.individual_left ul li a').click(function(){
					//修改导航Logo样式
					var _this = this;
					var _id = $(_this).attr('id');
					var _class = $(_this).attr('class');
					if (!_class.endWithIgnoreCase('-active')){
						$(_this).removeClass().addClass(_class + '-active');
					}
					//修改数字显示样式
					if ($(_this).find('div').length>0){
						var _numClass = $(_this).find('div').attr('class');
						if (!_numClass.endWithIgnoreCase('-active')){
							$(_this).find('div').removeClass().addClass(_numClass + '-active');
						}
					}
					//修改其他的导航的样式
					var _active = $('div.individual_left ul li a[class$="-active"][id!="'+_id+'"]');
					if (_active.length>0){
						var _activeClass = _active.attr('class');
						//修改数字显示样式
						if (_active.find('div').length>0){
							var _numClass = _active.find('div').attr('class');
							if (_numClass.endWithIgnoreCase('-active')){
								_active.find('div').removeClass().addClass(_numClass.substring(0, _numClass.length-7));
							}
						}
						//修改导航Logo
						_active.removeClass().addClass(_activeClass.substring(0, _activeClass.length-7));
					}
				});
				
				
				//待办事宜
				$('#navTodo').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/auther/work-flow!todoview.htm');
				});
				//我的任务
				$('#navMyTask').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/task/pe-task.htm');
				});
				//推荐知识
				$('#navKb').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/recommend/recommend.htm?flag=owner');
				});
				//公告列表
				$('#navNotice').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/notice/notice!list.htm');
				});
				//主题设置
				$('#navTheme').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/auther/index-setting.htm');
				});
				//用户设置
				$('#navUserCenter').click(function(){
					$('#content').attr('src', $.fn.getRootPath() + '/app/individual/individual!getUserInfo.htm');
				});
				
				
				//首次进入默认选中用户设置导航
				if ($('#navTodo').length>0){
					$('#navTodo').click();
				}else{
					$('#navUserCenter').click();
				}
				
				//
				$('#points').click(function(){
					var _this = this;
					if ($('#points-detail:hidden').length>0){
						//隐藏
						$('#points-detail').show(300);
						$('#points-detail').focus().unbind().bind('blur',function(){
                            $('#points-detail').hide();
                        });
					}else{
						$('#points-detail').hide(300);
					}
				});
				
				///*
				$('#points-detail div').hover(function(){
					$(this).css('background-color', '#44BE78');
				}, function(){
					$(this).css('background-color', '#fff');
				});
				//*/
				
				$('#points-detail div').click(function(){
					parent.__kbs_layer_ind = parent.$.layer({
						type: 2,
					    shadeClose: false,
					    title: false,
					    closeBtn: [0, false],
					    shade: [0.3, '#000'],
					    border: [0],
					    area: ['1000px', '800px'],
					    iframe: {
					    	src: $.fn.getRootPath() + "/app/point/p-log!finddetailinfo.htm?log.userid=${sessionScope.session_user_key.id}",
					    	scrolling: 'no'
					    }
					});
					$('#points-detail').hide();
				});
			});	//end document.ready
		</script>
	</head>

	<body>
		
		
		<div id="points-detail" style="position:absolute;padding-top:3px;padding-bottom:3px;top:96px;color:#000;border:1px solid #ddd;border-top:0px;background:#fff;z-index: 99999;display:none;font-size:10px;cursor: pointer;">
			<c:forEach items="${model.points}" var="item" begin="1">
	        	<div style="background:#fff;padding:3px;width:66px;">${item.name }： ${item.points }</div>
	        </c:forEach>
		</div>
	
		<div class="easyui-layout" data-options="fit:false" id="cc" style="height:580px;">
			<div data-options="region:'west',split:true,title:false" style="width:78px;">
				<div class="individual_left">
					<ul>
						<li style="${model.enablePoints?'height:98px;':'' }">
							<c:choose>
								<c:when test="${model.headImg==null}">
									<img id="imgheadinfo" src="${pageContext.request.contextPath}/resource/individual/images/individual/head_ico.jpg" />
								</c:when>
								<c:otherwise>
									<img id="imgheadinfo" src="${pageContext.request.contextPath}/app/individual/individual!getImg.htm" />
								</c:otherwise>
							</c:choose>
							<c:if test="${model.enablePoints}">
								<div id="points" style="margin:1px;border:0px solid red;font-size:12px;background: #44BE78;color:#fff;cursor: pointer;">
									<span style="font-size:10px;">${model.points[0].name }</span><br><span style="font-family:Arial;">${model.points[0].points }</span>
								</div>
							</c:if>
						</li>
						<c:if test="${model.isOpenWorkflow }">
							<li>
								<a id="navTodo" class="individual_ico1" href="javascript:void(0);">
									待办事务
									<c:choose>
										<c:when test="${model.countTodo==0}"></c:when>
										<c:when test="${model.countTodo>99}"><div class="individual_circle">99+</div></c:when>
										<c:when test="${model.countTodo>9}"><div class="individual_circle">${model.countTodo }</div></c:when>
										<c:otherwise><div class="individual_circle1">${model.countTodo }</div></c:otherwise>
									</c:choose>
								</a>
							</li>
						</c:if>
						<c:if test="${model.isOpenTask }">
							<li>
								<a id="navMyTask" class="individual_ico6" href="javascript:void(0);">
									我的任务 
									<c:choose>
										<c:when test="${model.countMyTask==0}"></c:when>
										<c:when test="${model.countMyTask>99}"><div class="individual_circle">99+</div></c:when>
										<c:when test="${model.countMyTask>9}"><div class="individual_circle">${model.countMyTask }</div></c:when>
										<c:otherwise><div class="individual_circle1">${model.countMyTask }</div></c:otherwise>
									</c:choose>
								</a>
							</li>
						</c:if>
						<li>
							<a id="navKb" class="individual_ico2" href="javascript:void(0);">
								推荐知识 
								<c:choose>
									<c:when test="${model.countKb==0}"></c:when>
									<c:when test="${model.countKb>99}"><div class="individual_circle">99+</div></c:when>
									<c:when test="${model.countKb>9}"><div class="individual_circle">${model.countKb }</div></c:when>
									<c:otherwise><div class="individual_circle1">${model.countKb }</div></c:otherwise>
								</c:choose>
							</a>
						</li>
						<li>
							<a id="navNotice" class="individual_ico5" href="javascript:void(0);">
								公告列表
								<c:choose>
									<c:when test="${model.countNotice==0}"></c:when>
									<c:when test="${model.countNotice>99}"><div class="individual_circle">99+</div></c:when>
									<c:when test="${model.countNotice>9}"><div class="individual_circle">${model.countNotice }</div></c:when>
									<c:otherwise><div class="individual_circle1">${model.countNotice }</div></c:otherwise>
								</c:choose>
							</a>
						</li>
						<li>
							<a id="navTheme" class="individual_ico3" href="javascript:void(0);">主题设置 </a>
						</li>
						<li>
							<a id="navUserCenter" class="individual_ico4" href="javascript:void(0);">用户设置</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div data-options="region:'center',title:false">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border:0px;">
					<iframe id="content" src="" scrolling="no" frameborder="0" style="height:99%;width:99%;"></iframe>
				</div>
			</div>
	</body>
</html>
