<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jquery.jqplot.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/satisfaction/css/satisfaction.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/satisfaction/css/css.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
			if(!window.USER_THEME)
				window.USER_THEME = '${sessionScope.session_user_key.userInfo.userTheme}';
		</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
<!--[if lte IE 8]><script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/excanvas.js"></script><![endif]-->
<!-- jqplot -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jquery.jqplot.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.highlighter.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.cursor.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.barRenderer.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.categoryAxisRenderer.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.highlighter.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.pieRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.dateAxisRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/satisfaction/js/jqplot/jqplot.canvasAxisTickRenderer.js"></script>
<!-- layer -->
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/themes/default/easyui.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/1.3.1/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
<style type= "text/css" >  
	td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
</style>
<script type="text/javascript">
		var formparams;
		var _index;
		var color=["#F75000","#3399FF","#ADADAD"];
		$(document).ready(function(){
		   $("#content").live("mouseover",function(){
		      _index=layer.tips($(this).html(),$(this), {maxWidth:500, style: ['background-color:#78BA32; color:#fff', '#78BA32'], guide: 1, time: 10, closeBtn:[0, true]});
		   });
		   var totalcount=${sa.satis_count+sa.general_count+sa.dissatis_count};
		   if(totalcount>0){
			   for(var i=0;i<color.length;i++){
				   $("#category p").eq(i).css("background",color[i]);
			   }
			   var data = [[$("#category span").eq(0).html(), ${sa.satis_count}],[$("#category span").eq(1).html(), ${sa.general_count}], [$("#category span").eq(0).html(), ${sa.dissatis_count}]];
		       var plot1 = jQuery.jqplot ("chartdiv", [data], 
			    { 
			      grid: { 
			         background: '#f6f6f6', // 设置整个图表区域的背景色 
					 borderWidth: 0,
					 shadow: false
			      },
			      title: {  
				       text: '满意度占比',   // 设置当前图的标题  
				       show: true,//设置当前标题是否显示  
				       fontSize:12,
				       fontFamily:'Microsoft YaHei' 
				   },  
			      seriesColors: color,
			      //seriesColors:$.jqplot.config.defaultColors,
			      seriesDefaults: {
			        renderer: jQuery.jqplot.PieRenderer, 
			        rendererOptions: {
			          shadow:true,
			          shadowDepth: 0,
			          shadowOffset: 2,
			          shadowAlpha: 0.07,
			          sliceMargin:1.5,
			          showDataLabels: true
			        }
			      }, 
			      legend: { show:false},
		          highlighter:{
		              show:true,
		              showTooltip: true,      	  
		              tooltipOffset: -50,
		              tooltipSeparator: '',
		              sizeAdjust: 5,	 
		              tooltipAxes: 'both',
		              useAxesFormatters: false,	
		              formatString:'<span style="color:#4F4F4F;">%s:</span> <span style="font-weight:bold;color:#000000">%s</span>'
		              //tooltipFormatString: '<span style="color:blue;">满意度</span> %.4P'
		          }
			    });
			 }else{
                 $("#category").hide();
              }      
		   if($("#satis_type").val()=='5' || $("#satis_type").val()==''){
		     $("#comm_type").hide();
		     $("#showcomm_type").show(); 
		   }else{
		      $("#comm_type").show();
		      $("#showcomm_type").hide();
		   }
		   
		   $('#menue').bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$('#obj_id').val("");
				}
			});
		   
		   
		   $.fn.zTree.init($("#workflowTypeTree"), setting);
		   formparams=$("#forms").serialize();
		   
		   $(document).click(function(e){
		      if(($("#workflowTypeTree").offset().left <= e.pageX && e.pageX <= ($("#workflowTypeTree").offset().left + $("#workflowTypeTree").width())) && (($("#workflowTypeTree").offset().top - $("#menue").height()) <= e.pageY && e.pageY <= ($("#workflowTypeTree").offset().top + $("#workflowTypeTree").height()))) {
			  }else{
			     if(!$("#workflowTypeContent").is(":hidden")){
			        $("#workflowTypeContent").hide();
			     }
			  }
			 
		   });
		});
		var setting = {
			view: {
					expandSpeed: "fast"
			},
			async : {
				enable : true,
				url : $.fn.getRootPath()+"/app/main/ontology-category!list.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam : {},
				dataFilter: ajaxDataFilter
			},
			callback : {
				onClick : onClick
			}
			
		};
		
		function ajaxDataFilter(treeId, parentNode, responseData) {
		    if (responseData) {
		      for(var i =0; i < responseData.length; i++) {
		      	if(responseData[i].isParent=="false")
		        	responseData[i].icon = $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/left_ico5.jpg';
		      }
		    }
		    return responseData;
		};
		
		function onClick(event,treeId, treeNode) {
		    $("#menue").val(treeNode.name);
		    $("#obj_id").val(treeNode.id);
		    $("#workflowTypeContent").hide();
		}
	
	
	//分页跳转
	function pageClick(pageNo){
	    var _index = parent.layer.load("正在查询数据...");
		//$('.content_right_bottom').ajaxLoading('正在查询数据...');
		var params=$("#forms").serialize();
		$.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!correctionsreport.htm?"+params,
			type:"post",
			data:{"pageNo":pageNo},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
			    parent.layer.close(_index);
				if (data!=null&&data!="") {
					//$('.xinxi_con_nr_right').html(data);
					$("body").html(data);
				} else {
					parent.layer.alert("网络错误，请稍后再试", -1);
				}
				//$('.content_right_bottom').ajaxLoadEnd();
			},
				error:function(){
					parent.layer.alert("网络错误，请稍后再试", -1);
				  	$('.content_right_bottom').ajaxLoadEnd();
				}
		});		
	}
	
	function showInfo(index,id,isread,status){
	   if(isread==0&&status==1&&$('#imginfo'+index+'').attr("src")!=""){
	      $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/corrections!findcorrectionsbyid.htm",
			type:"post",
			data:{"id":id},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
			      var count=$('#totalcounts').text();
			      var processcount=$('#processcount').text();
			      if(count>1)
			      {
			        $('#totalcounts').text(count-1);
			        $('#processcount').text(count-1);
			      }
			      if(count<=1)
			      {
			        $('#flags').text("");
			        $('#flag').text("");
			      }
			      if(processcount<=1)
			      {
			        $('#processflag').text("");
			      }
			   $('#imginfo'+index+'').hide();
			},
			error:function(){
				parent.layer.alert("网络错误，请稍后再试", -1);
			  	$('.content_right_bottom').ajaxLoadEnd();
			}
		});
		
	   }
	   $('#message'+index+'').toggle();
	  
	}
	
	function showmessage(val,e)
	{  
	   $('#divshow').show();
	   $('#divshow').html(val);
       $('#divshow').text($(this).attr('alt')).css({ top: (e.pageY + 3), left: (e.pageX + 5) }).show();
	}
	function leave()
	{
	  $('#divshow').hide();
	}
	
	
	function questionInfo(id,cate_id,indexid,imgid){
	   if($('#div'+indexid+'').html()!=''){
	       $('#div'+indexid+'').toggle();
	       if($('#div'+indexid+'').is(":visible")){
	          $('#'+imgid+'').attr('src','${pageContext.request.contextPath}/resource/satisfaction/images/report_title_ico1.jpg'); 
	       }else{
	          $('#'+imgid+'').attr('src','${pageContext.request.contextPath}/resource/satisfaction/images/report_title_ico2.jpg'); 
	       }
	   }else{
	       $('#'+imgid+'').attr('src','${pageContext.request.contextPath}/resource/satisfaction/images/report_title_ico1.jpg');
	       var data = [[$("#category span").eq(0).html(), parseInt($('#li'+indexid+' font').eq(1).html())],[$("#category span").eq(1).html(), parseInt($('#li'+indexid+' font').eq(2).html())], [$("#category span").eq(2).html(), parseInt($('#li'+indexid+' font').eq(3).html())]];
	       var plot1 = jQuery.jqplot ("chartdiv", [data], 
		    { 
		      grid: { 
		         background: '#f6f6f6', // 设置整个图表区域的背景色 
				 borderWidth: 0,
				 shadow: false
		      },
		      title: {  
			       text: '满意度占比',   // 设置当前图的标题  
			       show: true,//设置当前标题是否显示  
			       fontSize:12,
			       fontFamily:'Microsoft YaHei' 
			   },  
		      seriesColors: color,
		      //seriesColors:$.jqplot.config.defaultColors,
		      seriesDefaults: {
		        renderer: jQuery.jqplot.PieRenderer, 
		        rendererOptions: {
		          shadow:true,
		          shadowDepth: 0,
		          shadowOffset: 2,
		          shadowAlpha: 0.07,
		          sliceMargin:1.5,
		          showDataLabels: true
		        }
		      }, 
		      legend: { show:false},
	          highlighter:{
	              show:true,
	              showTooltip: true,      	  
	              tooltipOffset: -50,	 
	              useAxesFormatters: false,	
	              formatString:'<span style="color:#4F4F4F;">%s:</span> <span style="font-weight:bold;color:#000000">%s</span>'
	          }
		    });
		   var _index = parent.layer.load("请稍候...");
		   $.ajax({
				url:$.fn.getRootPath()+"/app/corrections/satisfaction!satisfactionbyobjid.htm?"+formparams,
				type:"post",
				data:{"obj_id":id,"category_id":cate_id},
				timeout:5000,
				async:false,
				dataType:"json",
				success:function(data){
				    parent.layer.close(_index);
		            for(var i=0;i<data.length;i++){
		               var totalcount=parseInt(data[i].satis_count)+parseInt(data[i].general_count)+parseInt(data[i].dissatis_count)
		               //$('#'+trid+'').append('<tr class="individual_moretr"><td>'+data[i].question+'</td><td>'+data[i].average+'</td><td>'+data[i].satis_count+'</td><td>'+data[i].general_count+'</td><td>'+data[i].dissatis_count+'</td><td>'+totalcount+'</td></tr>');	               
		               $('#div'+indexid+'').append('<li><p><a href="javascript:void(0)"><img src="${pageContext.request.contextPath}/resource/satisfaction/images/report_title_ico0.jpg" /></a><span title='+data[i].question+'>'+data[i].question+'</span></p><font>'+data[i].average+'</font> <font>'+data[i].satis_count+'</font> <font>'+data[i].general_count+'</font> <font>'+data[i].dissatis_count+'</font> <font>'+totalcount+'</font> <font><a href="javascript:void(0)" onclick=\'detailinfo("'+data[i].value_id+'")\'>详情</a></font> </li>');
		            }
		            $('#objcount').html("1");
		            $('#quescount').html(data.length);
		        }
		  }); 
		}     
	}
	
	function reportinfo(){
	   if($('#menue').val()==''){
	       $('#obj_id').val("");
	   }
	   var _index = parent.layer.load("请稍候...");
	   var params=$("#forms").serialize();
	   $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!correctionsreport.htm?"+params,
			type:"post",
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
			    parent.layer.close(_index);
	            $('body').html(data);
	            
	        }
	  });
	}
	
	function showtreeinfo(){
	   var cityObj = $("#menue");
	   var cityOffset = $(cityObj).offset();
	   $("#workflowTypeContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	   $("#workflowTypeContent").show();
	}
	
	function detailinfo(valueid){
		PageUtils.dialog.open('#detailinfo-dialog', {
	   		title: '评论明细', 
	   		params: {'value_id': valueid},
	   		width: $(window).width()*0.95, 
	   		height: $(window).height()*0.98,
	   		url: '/app/corrections/satisfaction!detailInfo.htm?' + formparams,
	   		buttons:[{
				text:'导   出',
				handler:function(){
					/*调用子页面方法*/
					var DetailInfo = window.frames['detailinfo-dialog-iframe'].DetailInfo;
					DetailInfo.exportData();
				}
			}]
	   	});
	}
	
	
	function changesattis(val){
	   if(val=='5' || val==''){
	     $("#comm_type").hide();
	     $("#showcomm_type").show(); 
	   }else{
	      $("#comm_type").show();
	      $("#showcomm_type").hide();
	   }
	}
	
	function exportdata(){
		var _index = parent.layer.load("数据准备中，请稍候...");
	   	var params=$("#forms").serialize();
	   	$.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!processData1.htm?"+params,
			type:"post",
			timeout:5000,
			async:false,
			dataType:"json",
			success:function(data){
	            if(data.success) {
	            	parent.layer.close(_index);
					var iframe = document.createElement("iframe");
		            iframe.src = $.fn.getRootPath()+"/app/corrections/satisfaction!downLoad.htm?fileName=" + data.message;
		            iframe.style.display = "none";
		            document.body.appendChild(iframe);
				}
	        }
	  }); 
	
	}
	
</script>
</head>

<body>
<div class="report">
<div class="report_title mt10">
<form id="forms" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><p>统计周期</p>
        <input name="start_time" type="text" value="${satisfaction.start_time }"  onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly"/>
        <font class="mr5 ml5">至</font>
        <input name="end_time" type="text" value="${satisfaction.end_time }" onclick="WdatePicker({maxDate:'%y-%M-%d'})" readonly="readonly"/></td>
      <td><font class="mr5">统计目录</font>
         <input style="width: 164px" id="menue" type="text" value="${satisfaction.obj_name }" name="obj_name" readonly="readonly" onclick="showtreeinfo();"/> 
         <input id="obj_id" name="node_id" value="${satisfaction.node_id}" type="hidden"/>
        </td>
      <td><font class="mr5">满意度</font>
        <select name="satis_type" id="satis_type" onchange="changesattis(this.value)">
             <option value="">-请选择-</option>
    　　　　　　  <s:iterator value="#request.satisfacttype" var="va">
                 <option value="${va.type }" <s:if test="#va.type==#request.satisfaction.satis_type"> selected='selected'</s:if>>${va.name }</option>
              </s:iterator>
         </select>
      </td>
      <td><font class="mr5">评论类型</font>
        <select id="showcomm_type">
             <option value="">-请选择-</option>
        </select>
        <select name="comment_type" id="comm_type" style="display:none">
             <option value="">-请选择-</option>
   　　　　　　  <s:iterator value="#request.commenttype" var="va">
                <option value="${va.type }" <s:if test="#va.type==#request.satisfaction.comment_type"> selected='selected'</s:if>>${va.name }</option>
             </s:iterator>
        </select>
       </td>
       <!-- 
      <td><font class="mr5">是否有评论</font>
        <select name="flag">
             <option value="">-请选择-</option>
             <option value="1" <c:if test="${satisfaction.flag ==1 }">selected="selected"</c:if>>否</option>
             <option value="0" <c:if test="${satisfaction.flag ==0 }">selected="selected"</c:if>>是</option>
         </select>
      </td> -->
      <td class="report_none"><input class="report_title_btn" type="button" value="查询" onclick="reportinfo();"/></td>
    </tr>
  </table>
  </form>
</div>
<div class="report_con mt10">
<div class="report_con_title">
<div class="report_con_title_left">
<ul>
<li>
  <p><span>业务名称</span></p>
  <font>平均分</font> <font>满意</font> <font>一般</font> <font>不满意</font> <font>评论总数</font> <font>&nbsp;</font>
</li>
</div>
<div class="report_con_title_right">&nbsp;</div>
</div>
<div class="report_con_con">
  <div class="report_con_left">
    <div class="report_con_left_top">
      <ul>
        <s:iterator value="#request.page.result" var="va" status="sta">
        
        
			<li class="bgcolor" id="li${sta.index}">
	          	<p>
	          		<a href="javascript:void(0)" onclick="questionInfo('${va.obj_id }','${va.category_id }','${sta.index}','img${sta.index}');">
	          			<img id="img${sta.index}" src="${pageContext.request.contextPath}/resource/satisfaction/images/report_title_ico2.jpg" />
	          		</a>
	          		<span title="${va.obj_name }" style="cursor:pointer" onclick="questionInfo('${va.obj_id }','${va.category_id }','${sta.index}','img${sta.index}');">${va.obj_name }</span>
	          	</p>
	      		<font>${va.average }</font>
	      		<font>${va.satis_count }</font>
	      		<font>${va.general_count }</font>
				<font>${va.dissatis_count }</font>
				<font>${va.satis_count+va.general_count+va.dissatis_count }</font>
				<font>&nbsp;</font>
			</li>
			<div id="div${sta.index}"></div>
		</s:iterator>
</ul>
    </div>
    <div class="report_con_left_bottom fl">
      <div class="report_bottom_export"><a href="javascript:void(0)" onclick="exportdata();">导出EXCEL</a></div>
      <div class="report_bottom_page"  style="background: white;"><div style="margin-top: 20px;background: white;"><jsp:include page="../util/part_fenye.jsp"></jsp:include></div></div>
    </div>
  </div>
    <div class="report_con_right">
  	<div class="report_con_right_top">
    <div class="report_con_right_circle1"><p><span>业务</span><span id="objcount">${page.totalCount }</span></p></div>
    <div class="report_con_right_circle2"><p><span>标准问</span><span id="quescount">${quescount }</span></p></div>
    </div>
    <div class="report_con_right_bottom" id="chartdiv"　style="height: 1000px"></div>
    <div class="report_con_right_bottom_wz" id="category">
       <s:iterator value="#request.satisfacttype" var="va">
             <p></p><span>${va.name }</span>
       </s:iterator>
    </div>
  </div>
</div>
</div>
</div>
<%-- 纠错详情 --%>
<div id="detailinfo-dialog"></div>
<div id="workflowTypeContent" class="workflowTypeContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:170px;">
	<ul id="workflowTypeTree" class="ztree" style="margin-top:0;"></ul>
</div>
</body>
</html>
