<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript">
			if(!window.USER_THEME)
				window.USER_THEME = '${sessionScope.session_user_key.userInfo.userTheme}';
		</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
<style type= "text/css" >  
	td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
</style>
<script type="text/javascript">
		var formparams;
		$(document).ready(function(){
		   $.fn.zTree.init($("#workflowTypeTree"), setting);
		   formparams=$("#forms").serialize();
		});
		var setting = {
			view: {
					expandSpeed: "fast"//空不显示动画
			},
			async : {
				enable : true,
				url : $.fn.getRootPath()+"/app/main/ontology-category!list.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam : {},
				dataFilter: ajaxDataFilter
			},
			callback : {
				onClick : onClick
			}
			
		};
		
		function ajaxDataFilter(treeId, parentNode, responseData) {
		    if (responseData) {
		      for(var i =0; i < responseData.length; i++) {
		      	if(responseData[i].isParent=="false")
		        	responseData[i].icon = $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/left_ico5.jpg';
		      }
		    }
		    return responseData;
		};
		
		function onClick(event,treeId, treeNode) {
		    $("#menue").val(treeNode.name);
		    $("#obj_id").val(treeNode.id);
		}
	
	
	//分页跳转
	function pageClick(pageNo){
		//$('.content_right_bottom').ajaxLoading('正在查询数据...');
		$.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!correctionsreport.htm",
			type:"post",
			data:{"pageNo":pageNo},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
				if (data!=null&&data!="") {
					//$('.xinxi_con_nr_right').html(data);
					$("body").html(data);
				} else {
					parent.layer.alert("网络错误，请稍后再试", -1);
				}
				//$('.content_right_bottom').ajaxLoadEnd();
			},
				error:function(){
					parent.layer.alert("网络错误，请稍后再试", -1);
				  	$('.content_right_bottom').ajaxLoadEnd();
				}
		});		
	}
	
	function showInfo(index,id,isread,status){
	   if(isread==0&&status==1&&$('#imginfo'+index+'').attr("src")!=""){
	      $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/corrections!findcorrectionsbyid.htm",
			type:"post",
			data:{"id":id},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
			      var count=$('#totalcounts').text();
			      var processcount=$('#processcount').text();
			      if(count>1)
			      {
			        $('#totalcounts').text(count-1);
			        $('#processcount').text(count-1);
			      }
			      if(count<=1)
			      {
			        $('#flags').text("");
			        $('#flag').text("");
			      }
			      if(processcount<=1)
			      {
			        $('#processflag').text("");
			      }
			   $('#imginfo'+index+'').hide();
			},
			error:function(){
				parent.layer.alert("网络错误，请稍后再试", -1);
			  	$('.content_right_bottom').ajaxLoadEnd();
			}
		});
		
	   }
	   $('#message'+index+'').toggle();
	  
	}
	
	function showmessage(val,e)
	{  
	   $('#divshow').show();
	   $('#divshow').html(val);
       $('#divshow').text($(this).attr('alt')).css({ top: (e.pageY + 3), left: (e.pageX + 5) }).show();
	}
	function leave()
	{
	  $('#divshow').hide();
	}
	
	
	function questionInfo(id,trid){
	   $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!satisfactionbyobjid.htm?"+formparams,
			type:"post",
			data:{"obj_id":id},
			timeout:5000,
			async:false,
			dataType:"json",
			success:function(data){
	            for(var i=0;i<data.length;i++){
	               var totalcount=parseInt(data[i].satis_count)+parseInt(data[i].general_count)+parseInt(data[i].dissatis_count)
	               $('#'+trid+'').after('<tr class="individual_moretr"><td>'+data[i].question+'</td><td>'+data[i].average+'</td><td>'+data[i].satis_count+'</td><td>'+data[i].general_count+'</td><td>'+data[i].dissatis_count+'</td><td>'+totalcount+'</td></tr>');	               
	            }
	        }
	  });    
	}
	
	function reportinfo(){
	   var params=$("#forms").serialize();
	   $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/satisfaction!correctionsreport.htm?"+params,
			type:"post",
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
	            $('body').html(data);
	        }
	  });
	}
	
	function showtreeinfo(){
	   var cityObj = $("#menue");
	   var cityOffset = $(cityObj).offset();
	   $("#workflowTypeContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
	   $("#workflowTypeContent").show();
	}
</script>
</head>
<body>
<div id="tagContent">
     <form id="forms" method="post" action="">
         目录:<input id="menue" type="text" value="${satisfaction.obj_name }" name="obj_name" onclick="showtreeinfo();"/> 
         <input id="obj_id" name="node_id" value="${satisfaction.node_id}" type="hidden"/>   
         满意度:<select name="satis_type">
                <option value="">-${satisfaction.satis_type }-</option>
       　　　　　　  <s:iterator value="#request.satisfacttype" var="va">
                    <option value="${va.type }" <s:if test="#va.type==#request.satisfaction.satis_type"> selected='selected'</s:if>>${va.name }</option>
                 </s:iterator>
            </select>
       评论类型:<select name="comment_type">
                 <option value="">-请选择-</option>
       　　　　　　  <s:iterator value="#request.commenttype" var="va">
                    <option value="${va.type }" <s:if test="#va.type==#request.satisfaction.comment_type"> selected='selected'</s:if>>${va.name }</option>
                 </s:iterator>
            </select>
       评论内容:<select name="flag">
                <option value="">-请选择-</option>
                <s:if test="#request.satisfaction.flag==0">
                   <option value="0" selected="selected">否</option>
                   <option value="1">是</option>
                </s:if>
                <s:if test="#request.satisfaction.flag==1">
                   <option value="0">否</option>
                   <option value="1" selected="selected">是</option>
                </s:if>
                <s:if test="#request.satisfaction.flag==null">
                   <option value="0">否</option>
                   <option value="1">是</option>
                </s:if>
            </select>  
            <input type="button" onclick="reportinfo();" value="确定" />         
     </form>
     <div class="tagContent selectTag" id="tagContent0">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="individual_more">
			<tr>
			　　<td class="individual_tdbg pl10 fw individual_tdbg1" width="30%">
				     业务名称
				</td>
				<td class="individual_tdbg pl10 fw individual_tdbg1" width="10%">
				     平均分
				</td>
				<td class="individual_tdbg fw individual_tdbg2" width="10%">
					满意
				</td>
				<td class="individual_tdbg fw individual_tdbg3" width="10%">
					一般
				</td>
				
				<td class="individual_tdbg fw individual_tdbg4" width="10%">
					不满意
				</td>
				
				<td class="individual_tdbg fw individual_tdbg4" width="10%">
					评论总数
				</td>
				
				<s:if test="#request.status==1">
					<td width="5%" class="individual_tdbg fw individual_tdbg4" align="center">
						操作
					</td>
				</s:if>
			</tr>
		  <s:iterator value="#request.page.result" var="va" status="sta">
				<tr class="individual_moretr" id="obj${sta.index}">
					<td onclick="questionInfo('${va.obj_id }','obj${sta.index}');">${va.obj_name }</td>
					<td>${va.average }</td>
					<td>${va.satis_count }</td>
					<td>${va.general_count }</td>
					<td>${va.dissatis_count }</td>
					<td>${va.satis_count+va.general_count+va.dissatis_count }</td>
				</tr>
			</s:iterator>
			
			<tr class="trfen">
				<td colspan="5">
					<jsp:include page="../util/part_fenye.jsp"></jsp:include>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id="workflowTypeContent" class="workflowTypeContent" style="display:none;position: fixed;z-index:10002;background-color: white;border: 1px solid #ccc; _position:absolute;overflow: auto; max-height: 200px;width:298px;">
	<ul id="workflowTypeTree" class="ztree" style="margin-top:0;"></ul>
</div>
</body>
</html>
