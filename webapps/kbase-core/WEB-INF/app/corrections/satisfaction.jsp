<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>     
     
     <div class="satisfaction_con" id="satisfact_content">
       <input type="hidden" value="<s:property value="#request.page.totalCount"/>" id="satisfactioncount" />
		   <ul>
		     <s:iterator  value="#request.page.result" var="va">
		       <li>
			       <p title="<s:property value="#va.content"/>"><s:property value="#va.content"/>&nbsp;</p>
			       <span><s:property value="#va.user.userInfo.userChineseName"/></span>
			       <span> <fmt:formatDate  value="${va.create_time }" pattern="yyyy-MM-dd" /></span>
			       <span>
			       <s:iterator value="#request.satisfacttype" var="vac">
	                  <s:if test="#va.satisfaction==#vac.type">
	                     ${vac.name}
	                  </s:if>
	               </s:iterator>
			       </span>
			       <span>
			        <s:iterator value="#request.commenttype" var="vac">
	                  <s:if test="#vac.type==#va.type">
	                     ${vac.name}
	                  </s:if>
	               </s:iterator>
                   </span>
			       <span>&nbsp;</span>
		       </li>
		     </s:iterator>
		    </ul>
	</div>
	<div id="fenye">
	<s:if test="#request.page.totalCount >10">
	  <jsp:include page="../util/part_fenye.jsp"></jsp:include>
	</s:if>
	<div>
	