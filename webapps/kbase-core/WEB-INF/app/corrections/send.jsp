<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>    

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>知识评论-满意度调查</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gonggao.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/resource/corrections/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/resource/corrections/css/satisfaction.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript">
			//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
			window.__kbs_has_parent = !(window.parent==window);
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery.flash.js"></script>
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>			
		<!-- eko.zhan modify at 2015-08-18 13:20 -->
		<script type="text/javascript">
			/*
			var _data = {
				'objId': '${param.objId}',
				'faqId': '${param.faqId}',
				'cateId': '${param.cateId}',
				'question': '${param.question}',
				'answer': '${param.answer}'
			}
			var _data = {
				'value_id': '${param.faqId}',
				'category_id': '${param.cateId}',
				'obj_id': '${param.objId}',
				'question': '${param.question}',
				'answer': '${param.answer}'
			}*/
			var _data = {
				'value_id': '${param.faqId}',
				'category_id': '${param.cateId}',
				'obj_id': '${param.objId}'
			}
		</script>

		<script type="text/javascript">
			$(function() {
				$("#score").html($("input[name='satisfaction']:checked").val());
				$.ajax({
					type: "POST",
					url: '${pageContext.request.contextPath}/app/corrections/corrections!getsatisfaction.htm',
					data: {
						'value_id': _data.value_id
					},
					async: true,
					dataType: 'html',
					success: function(data) {
						$("#fenye").remove();
						$("#satisfact_content").remove();
						$("#satisfaction").append(data);
						$('#commcount').html($('#satisfactioncount').val());
					},
					error: function() {
						if (window.__kbs_has_parent){
							parent.layer.alert('获取失败');
						}else{
							alert('获取失败');
						}
					}
				});
			
				//提交评论
				$('#btnSend').click(function() {
					///////////////////////////////////////////////////////////////////
					//如果后台获取标准问失败，提示用户关闭当前页面重新进行纠错  modify by heart.cao 2016-08-02
					if($('#question').val().trim()=='标准问获取失败，请联系管理员'){
					     alert('标准问获取失败，请关闭当前页面重新执行纠错操作');
					     return false;
					}	
					///////////////////////////////////////////////////////////////////					
					//知识满意度 5-满意 3-一般 0-不满意
					var satisRank = $('input[name="satisfaction"]:checked').val();
					//评论类型 0-答案错误 1-答案过期 2-答案遗漏 3-其他
					var satisType = $('input[name="type"]:checked').val();
					
					if ($("#sat_content").val().trim()==''){
						if (window.__kbs_has_parent){
							$(document).hint('请填写评价内容');
						}else{
							alert('请填写评价内容');
						}
						return false;
					}
					//未选择满意 则必须选择评论类型
					if (satisRank!=5){
						//非满意的情况下必须选择 评论类型
						if (satisType==undefined){
							if (window.__kbs_has_parent){
								$(document).hint('请选择评论类型');
							}else{
								alert('请选择评论类型');
							}
							return false;
						}
					}
					var params = $("#form1").serializeArray();
					// var params = $("#form1").serialize();
				    $.each(params, function() {
				      _data[this.name] = this.value;
				    });
					//
					window.__kbs_layer_index = layer.load('正在提交，请稍候...');	
					var _url = '${pageContext.request.contextPath }/app/corrections/corrections!save.htm';
					$.ajax({
				   		url:  _url,
				   		type: 'POST',
				   		data: _data,
				   		async: true,
				   		dataType : 'json',
				   		success: function(data){
				   		    layer.close(window.__kbs_layer_index);	
				   			if (window.__kbs_has_parent){
								$(document).hint('评分成功');
								parent.layer.close(parent.__kbs_layer_index);
							}else{
								alert('评分成功');
								window.close();
							}
				   		},
				   		error :function(){
				   		    layer.close(window.__kbs_layer_index);	
				   			if (window.__kbs_has_parent){
								$(document).hint('提交失败');
							}else{
								alert('提交失败');
							}
				   		}
				   });
				});
				
				//知识满意度radio选择
				$('input[name="satisfaction"]').click(function(){
					var satisRank = $('input[name="satisfaction"]:checked').val();
					$("#score").html(satisRank);
					if (satisRank==5){
						$("#typelist").hide();
					}else{
						$("input[name='type']").attr("checked", false);
						$("#typelist").show();
					}
				});
				//如果后台获取标准问失败，提示用户关闭当前页面重新进行纠错  modify by heart.cao 2016-08-02
				if($('#question').val().trim()=='标准问获取失败，请联系管理员'){
				     alert('标准问获取失败，请关闭当前页面重新执行纠错操作');
				}				
			}); //end document.ready
			
			//分页跳转
			function pageClick(pageNo){
				var check = $("input[name='filter']:checked").val();
				$.ajax({
					url:"${pageContext.request.contextPath }/app/corrections/corrections!getsatisfaction.htm",
					type:"post",
					data:{"pageNo":pageNo,'value_id':_data.value_id,'satisfaction':check},
					timeout:5000,
					async:false,
					dataType:"html",
					success:function(data){
					    $("#fenye").remove();
						$("#satisfact_content").remove();
			   			$("#satisfaction").append(data);
					}
				})
			}
			
			//过滤信息
			function filterinfo(type){
			   $.ajax({
			   		type: "POST",
			   		url:  '${pageContext.request.contextPath }/app/corrections/corrections!getsatisfaction.htm',
			   		data:{'value_id': _data.value_id, 'satisfaction':type},
			   		async: true,
			   		dataType : 'html',
			   		success: function(data){
			   		    $("#fenye").remove();
			   		    $("#satisfact_content").remove();
			   			$("#satisfaction").append(data);
			   		},
			   		error :function(){
			   			parent.layer.alert('获取失败');
			   		}
				});
			}	
		</script>
	</head>
	<body>
		<div id="satisfaction">
		
			<form id="form1" name="form1" method="post" action="">
			    <div style="display:none">
			        <textarea name="category_name" cols="" rows="" id="answer" >${category_name}</textarea>
			        <textarea name="object_name" cols="" rows="" id="answer" >${object_name}</textarea>			    
			        <textarea name="question" cols="" rows="" id="question" >${question}</textarea>
			        <textarea name="answer" cols="" rows="" id="answer" >${answer}</textarea>
			    </div>			
				<div class="satisfaction_title">
				  <div class="satisfaction_title_div1" id="title_div">
				      <ul>
				        <li class="cl_333 fs14 fw ff_wei mt10">知识满意度：</li>
				        <li class="satisfaction_title_div1_fen"><span class="cl_hongse fs36 fw" id="score">5</span>分
				           <s:iterator value="#request.satisfacttype" var="va" status="sta">
				            <s:if test="#va.type==0">
				                <label>
				                  <input type="radio" name="satisfaction" value="${va.type}" id="satisrank" checked="checked"/>${va.name}
				                </label>
				            </s:if>
				            <s:else>
					            <label>
					               <input type="radio" name="satisfaction" value="${va.type}" id="satisrank" />${va.name}
					            </label>
				             </s:else>
				           </s:iterator>
				       </li>
				       <li class="satisfaction_title_bottom">共<span class="fs14 cl_hongse ff_english fw" id="commcount"></span>条评价</li>
				     </ul>
				 </div>
				 <div class="satisfaction_title_div2" id="satisfaction_type">
				     <ul>
				       <li class="cl_333 fs14 fw ff_wei">选择评论类型：</li>
				       <li id="typelist">
					        <s:iterator value="#request.commenttype" var="va">
					            <label>
					               <input type="radio" name="type" value="${va.type}" id="satistype" />${va.name}
					             </label>
					        </s:iterator>
				        </li>
				      </ul>
				  </div>
				  <div class="satisfaction_title_div3">
				      <ul>
				        <li class="cl_333 fs14 fw ff_wei">填写评论内容：</li>
				        <li>
				          <textarea name="content" cols="" rows="" id="sat_content"></textarea>
				        </li>
				      </ul>
				  </div>
				  <div class="satisfaction_title_div4">
				      <input type="button" class="satisfaction_btn" id="btnSend" value="提交评论" />
				  </div>
				</div>
			</form>
			<div class="satisfaction_title1 cl_666">
			   <form id="form2">
			       <label><input type="radio" name="filter" onclick="filterinfo();" value="" id="filter" checked="true"/> 全部 </label>
				   <s:iterator value="#request.satisfacttype" var="va">
					    <label>
					       <input type="radio" name="filter" onclick="filterinfo('${va.type}');" value="${va.type}" id="filter" />${va.name}
					     </label>
				   </s:iterator>
			   </form>
			</div>
			<div class="satisfaction_title2 cl_333 fw" id="satisfaction_content">
			   <p>评价内容</p>
			   <span>评价人</span> <span>评价时间</span> <span>满意度</span> <span>类型</span> <span>紧急程度</span>
			</div>
		  
		</div>
	</body>
</html>