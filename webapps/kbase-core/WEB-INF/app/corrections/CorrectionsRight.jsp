<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/individual.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/individual/css/css.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
<style type= "text/css" >  
	td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
</style>
<script type="text/javascript">
	//分页跳转
	function pageClick(pageNo){
		//$('.content_right_bottom').ajaxLoading('正在查询数据...');
		$.ajax({
			url:$.fn.getRootPath()+"/app/corrections/corrections!getcorrections.htm",
			type:"post",
			data:{"pageNo":pageNo,"status":${status}},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
				if (data!=null&&data!="") {
					//$('.xinxi_con_nr_right').html(data);
					$("body").html(data);
				} else {
					parent.layer.alert("网络错误，请稍后再试", -1);
				}
				//$('.content_right_bottom').ajaxLoadEnd();
			},
				error:function(){
					parent.layer.alert("网络错误，请稍后再试", -1);
				  	$('.content_right_bottom').ajaxLoadEnd();
				}
		});		
	}
	
	function showInfo(index,id,isread,status){
	   if(isread==0&&status==1&&$('#imginfo'+index+'').attr("src")!=""){
	      $.ajax({
			url:$.fn.getRootPath()+"/app/corrections/corrections!findcorrectionsbyid.htm",
			type:"post",
			data:{"id":id},
			timeout:5000,
			async:false,
			dataType:"html",
			success:function(data){
			      var count=$('#totalcounts').text();
			      var processcount=$('#processcount').text();
			      if(count>1)
			      {
			        $('#totalcounts').text(count-1);
			        $('#processcount').text(count-1);
			      }
			      if(count<=1)
			      {
			        $('#flags').text("");
			        $('#flag').text("");
			      }
			      if(processcount<=1)
			      {
			        $('#processflag').text("");
			      }
			   $('#imginfo'+index+'').hide();
			},
			error:function(){
				parent.layer.alert("网络错误，请稍后再试", -1);
			  	$('.content_right_bottom').ajaxLoadEnd();
			}
		});
		
	   }
	   $('#message'+index+'').toggle();
	  
	}
	
	function showmessage(val,e)
	{  
	   $('#divshow').show();
	   $('#divshow').html(val);
       $('#divshow').text($(this).attr('alt')).css({ top: (e.pageY + 3), left: (e.pageX + 5) }).show();
	}
	function leave()
	{
	  $('#divshow').hide();
	}
</script>


<div id="tagContent">
     <div class="tagContent selectTag" id="tagContent0">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="individual_more">
			<tr>
				<td class="individual_tdbg pl10 fw individual_tdbg1" width="35%">
				     标准问
				</td>
				<td class="individual_tdbg fw individual_tdbg2" width="35%">
					纠错原因
				</td>
				<td class="individual_tdbg fw individual_tdbg3" width="15%">
					创建时间
				</td>
				
				<td class="individual_tdbg fw individual_tdbg4" width="10%">
					类型
				</td>
				
				<s:if test="#request.status==1">
					<td width="5%" class="individual_tdbg fw individual_tdbg4" align="center">
						操作
					</td>
				</s:if>
			</tr>
		  <s:iterator value="#request.page.result" var="va" status="sta">
				<tr class="individual_moretr">
					<td title="${va.question}"  class="pl10 individual_40" style="width:">
					    <div class="w100">
						 <s:if test="#va.question.length() > 12">
							<s:property value="#va.question.substring(0,12) + '...'"/>
						 </s:if>
						 <s:else>
							<s:property value="#va.question"/>
						 </s:else>
						 <s:if test="#va.isread==0 && #va.status==1 ">
						 <img id="imginfo${sta.index+1 }" src="${pageContext.request.contextPath}/resource/individual/images/individual/newicon.gif" />
						 </s:if>
					    </div>
					</td>
					
					<td title="${va.content}" class="individual_15">
						<s:if test="#va.content.length() > 12">
							<s:property value="#va.content.substring(0,12) + '...'"/>
						</s:if>
						<s:else>
							<s:property value="#va.content"/>
						</s:else>
						<div id="mesage" style="display: none">${va.content }</div>
					</td>
					<td class="individual_15">
					    <fmt:formatDate  value="${va.create_time }" pattern="yyyy-MM-dd HH:mm:ss" />
					</td>
					
					<td class="individual_15">
						${va.typelabel }
					</td>
					
					<s:if test="#request.status==1">
						<td align="center" class="individual_15">                                                                        
							<a href="javascript:void(0)" onclick="showInfo('${sta.index+1 }','${va.id}','${va.isread}','${va.status }')">详情</a>
						</td>
					</s:if>
				</tr>
				<tr style="border: opx;display: none" id="message${sta.index+1 }">
				   <td colspan="6" height="70px" align="center">
							<s:if test="#va.status==1">${va.remarks }</s:if>
					</td>
				</tr>
			</s:iterator>
			<div id="divshow" style="border: 1px;background:#fff9f9; width:200px; position:absolute; display:none"></div>
			<tr class="trfen">
				<td colspan="5">
					<jsp:include page="../util/part_fenye.jsp"></jsp:include>
				</td>
			</tr>
		</table>
	</div>
</div>