<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
<style>
.gonggao_con_info{background:#FFF;width:100%;height:426px;}
.gonggao_con_info table{border-right:1px solid #edecec;}
.gonggao_con_info .tdbg{background:#fff9f9;font-weight:bold;color:#333333;}
.gonggao_con_info .tdbg td{color:#333333;font-weight:bold;}
.gonggao_con_info td{word-break:break-all;border:1px solid #edecec;border-top:none;border-right:none;color:#666;line-height:33px;font-size:14px }
.gonggao_con_info .trfen{background:#fff9f9;}
</style>
<div class="gonggao_con">
	<div class="gonggao_con_info">
		<table width="100%" border="1" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;background:#fff9f9">
			<tr>
			   <td align="right" bgcolor="white">紧急程度:</td>
			   <td>${corrections.statuslabel }</td>
			   <td align="right" bgcolor="white">类 型:</td>
			   <td>${corrections.typelabel }</td>
			</tr>
			
			<tr>
			   <td align="right" bgcolor="white">标 准 问:</td>
			   <td>${corrections.question }</td>
			   <td align="right" bgcolor="white">纠错原因:</td>
			   <td>${corrections.content }</td>
			</tr>
			
			<tr>
			   <td align="right" bgcolor="white">说    明:</td>
			    <td>${corrections.remarks }</td>
			   <td align="right" bgcolor="white">处理时间:</td>
			   <td><fmt:formatDate  value="${corrections.process_time }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
			</tr>
			
		</table>
	</div>
</div>