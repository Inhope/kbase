<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>纠错详情</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$(function(){
				var that = window.DetailInfo = {}, 
					fn = that.fn = {};
				that.extend = fn.extend = $.extend;
				
				DetailInfo.extend({
					/*数据导出*/
					exportData: function(){
						$.ajax({
							url: $.fn.getRootPath() + '/app/corrections/satisfaction!processdetaildata.htm?' + parent.formparams,
							type: 'post',
							data: {'value_id': $('#detail_id').val()},
							timeout: 5000,
							async: false,
							dataType: 'json',
							success:function(data){
					            if(data.success) {
						            window.open($.fn.getRootPath() + 
						            	'/app/corrections/satisfaction!downLoad.htm?fileName=' + data.message);
								}
					        }
					  }); 
					}
				});
			});
		</script>
	</head>
	<body class="easyui-layout">
		<input type="hidden" value="${param.value_id }" id="detail_id"/>
    	<div data-options="region:'center',split:true">
			<!-- 列表 -->
			<table id="now-grid" class="easyui-datagrid" data-options="
				border:false,rownumbers:true,nowrap:false,fit:true,fitColumns:true,singleSelect:true">
				<thead>
					<tr>
						<th data-options="field:'satisfactType',width:80,align:'left'">满意度</th>
						<th data-options="field:'commentType',width:80,align:'left'">评价类型</th>
						<th data-options="field:'content',width:300,align:'left'">评价内容</th>
						<th data-options="field:'createDate',width:120,align:'left'">评价时间</th>
						<th data-options="field:'createUser',width:200,align:'left'">评价人</th>
						<th data-options="field:'role',width:100,align:'left'">角色</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="#request.rows" var="va">
						<tr>
							<td>${va.satisfactType }</td>
						 	<td>${va.commentType }</td>
						 	<td>${va.content }</td>
						 	<td>${va.createDate }</td>
							<td>${va.createUser }</td>
						 	<td>${va.role }</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
		</div> 
	</body>
</html>
