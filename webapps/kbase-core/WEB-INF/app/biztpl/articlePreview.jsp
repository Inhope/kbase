<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>预览</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/article_see.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/js/ArticleSearch.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-scroll.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript">
			function jump(value){
				return false;
			}
			$(function(){
				ArticleSearch.setting({
					objId: '${json.id}', /*实例id(与文章id共用)*/
					objName: '${json.name}', /*实例名称*/
					cateId: '${json.categoryId}', /*分类id*/
					cateName: '${json.categoryName}', /*分类名称*/
					cateBh: '', /*分类路径*/
					cateBhName: '', /*分类路径（中文）*/
					spotId: '', /*锚点id*/
					converterPath: '',/*附件转换地址*/
					tplId: '${json.tplId}', /*文章模板id*/
					doDownload: '${keyMenus['K-AttachmentDownload']!=null}',/*知识附件下载权限*/
					robotPath: '${robot_context_path}',/*robot地址*/
					groupList: ${groupList},
					pageType: 'preview'
				});
				
				//1024的屏幕，截取字符串
				if(screen.width == 1024){
					$("[class='catalog-list']").find("li a").each(function(){
						var text = $(this).attr("title");
						if(text.length > 10){
							text = text.substring(0,10) + '...';
							$(this).text(text);
						}
					});
				}
				
				$(window).bind('resize',function(){
					//location.reload();
					//重新判断是否显示导航
					var bar = $('#navigationBar'), 
						hgBar = $(bar).height();
					ArticleSearch.timeline.navBar(hgBar, bar);
				});
				
			});
		</script>
	</head>
	<body class="easyui-layout" data-options="fit:true" >
    	<div data-options="region:'center', border:false">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:white;">
				<tr>
					<td>
						<div class="pl20 position" id="categoryNv"></div>
					</td>
                    <td style="vertical-align: top;text-align: right;padding-right: 5px;">
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-ckwd'" id="refer">参考文档</a>
						<%-- 
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-bb'"   id="edition">版本</a>
						--%>
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-sc1'"  id="favClip">收藏</a>
			        	<a href="javascript:void(0);" class="easyui-linkbutton" 
			        		data-options="iconCls:'icon-bj'"   id="edit">编辑</a>
			        	<a href="javascript:void(0);" class="easyui-linkbutton" 
			        		data-options="iconCls:'icon-bb'"  id="open">最大化</a>
			        	<%-- add by eko.zhan at 2016-09-12 17:10 --%>
			        	<a href="javascript:void(0);" class="easyui-linkbutton" 
			        		data-options="iconCls:'icon-view'" id="btn2d">二维展示</a>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tab_cont" style="background:white">
				<tr>
					<td style="vertical-align: top;">
						<div class="detail-a">
							<div class="a-title pl20" style="margin-top:auto;">
								<h1>${json.name}</h1>
							</div>
							<div class="about">
								<c:if test="${json.createUser != ''}">
									<span>创建人</span>
		    						<span>${json.createUser }</span>
								</c:if>
  								<span>${json.date }</span>
	    					</div>
							<div class="about pl20">
	    						<s:if test="#request.article.articleAbs != null && #request.article.articleAbs != ''">
									<div class="textarea_div">${json.abstract }</div>
								</s:if>
	    					</div>
							<div class="a-con" style="width: 100%; margin:0 auto">
								<div class="con-content">
									<div class="main_ml" id="spotRoot">
										<div class="left_ml">
											<s:set var="totalCount" value="#request.groupList.size"/>
											<s:set var="columnSize" value="(#totalCount%3)==0?(#totalCount/3):(#totalCount/3+1)"/>
											<h2 class="block-title">目录</h2>
										</div>
										<div class="right_ml">
											<ul class="right_u">
												<c:forEach items="1,2,3" var="columnNum">
													<li>
														<div class="catalog-list">
															<ol>
															<c:if test="${fn:length(groupList) > 0 }">
															<c:forEach items="${groupList }"
																begin="${(columnNum-1)*columnSize }" end="${columnNum*columnSize-1 }" var="va">
																<li class="${va.level == 0 ? 'level1' : 'level2'}">
																	<%-- <span class="index"></span> --%>
																	<span class="text">
																		<font style="margin-right: 5px;">${va.level == 0 ? '' : '▪'}</font>
																		<a href="#spot${va.groupId }" title="${va.name }">
																			<c:if test="${fn:length(va.name) > 15}">
																				${fn:substring(va.name,0,15)}...
																			</c:if>
																			<c:if test="${fn:length(va.name) <= 15}">
																				${va.name }
																			</c:if>
																		</a>
																	</span>
																</li>
															</c:forEach>
															</c:if>
															</ol>
														</div>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="detail-c">
							<div class="a-title pl20">
								<div class="title-b">
									<h2 class="title-h2">
										正文
									</h2>
								</div>
							</div>
							<div class="a-con">
								<%-- 一级标题 ********************************************--%>
								<s:iterator value="#request.json.groups" var="va1" status="st1">
									<div class="con-title mt20">
										<div class="ab-title" id="spot${va1.groupId }">
											<div class="title-h">
												<h2>${va1.name }</h2>
												<c:if test="${va1.abs != null && va1.abs != '' }">
													<span class="showorhideabs" name="showOrHideAbs">
														<a href="javascript:void(0);">折叠↑</a>
													</span>
												</c:if>
											</div>
											<div class="contain">
												<s:if test="#va1.abs != null && #va1.abs != ''">
													${va1.abs }
												</s:if>
											</div>
										</div>
										<%-- 标准问 --%>
										<s:if test="#request.json.questions != null && #request.json.questions.size > 0">
											<s:set var="isTrue" value="false"/>
											<s:iterator value="#request.json.questions" var="ques"><%--标准问 --%>
												<s:if test="#ques.groupid==#va1.groupId">
													<s:set var="isTrue" value="true"/>
												</s:if>
											</s:iterator>
											<s:if test="#isTrue==true">
											<div class="wenda_list">
												<h2>相关问题</h2>
												<ul>
													<s:iterator value="#request.json.questions" var="ques"><%--标准问 --%>
													<s:if test="#ques.groupid==#va1.groupId">
														<li class="blue" id="spot${ques.id }">
															<div class="c-text-ask c-text">问</div>
															<div class="c-text-ans">
															${ques.name }
															</div>
														</li>
														<s:iterator value="#ques.answers" var="ans"><%--答案 --%>
															<li class='orange'>
																<div class="c-text-answer c-text">答</div>
																<div class="c-text-ans">
																	${ans.content }
																</div>
															</li>
															<!-- 附件 -->
															<%-- <s:if test="#va4.attachments != null && #va4.attachments.size > 0">
																<li class="file">
																	<ul>
																		<s:iterator value="#va4.attachments" var="va5">
																			<li <s:if test="#va5.value.indexOf('.xls')>-1 || #va5.value.indexOf('.xlsx')>-1">class="name-b"</s:if>
																				<s:elseif test="#va5.value.indexOf('doc')>-1 || #va5.value.indexOf('.docx')>-1">class="name-c"</s:elseif>
																				<s:else>class="name-a"</s:else>>
																				<a href="javascript:void(0);"><span></span>
																					<div class="file-con" options="fileId:'${va5.key }',fileName:'${va5.value }'">
																						<p>${va5.value }</p>
																						<div class="file-preview"></div>
																					</div>
																				</a>
																			</li>
																		</s:iterator>
																	</ul>
																</li>
															</s:if> --%>
														</s:iterator>
														<li>
															<div class="c-meaubar" options="faqId:'${ques.id }',faq:'${ques.name }'">
																<ul>
																	<li class="c-icona">
																		<a href="javascript:void(0);"><span></span>签读</a>
																	</li>
																	<li class="c-iconb">
																		<a href="javascript:void(0);"><span></span>评论</a>
																	</li>
																	<li class="c-iconc">
																		<a href="javascript:void(0);"><span></span>收藏</a>
																	</li>
																	<li class="c-icond">
																		<a href="javascript:void(0);"><span></span>分享</a>
																	</li>
																</ul>
															</div>
														</li>
													</s:if>
													</s:iterator>
												</ul>
											</div>
											</s:if>
										</s:if>
									</div>
									<div style="clear: both"></div>
									<%-- 二级级标题 ********************************************--%>
									<s:if test="#va1.children != null && #va1.children.size > 0">
									<s:iterator value="#va1.children" var="va2" status="st2">
										<s:if test="#va2.groupId!=''">
										<div <s:if test="#st2.first">class="con-content"</s:if><s:else>class="con-content pt10"</s:else>>
											<div class="conb-title" id="spot${va2.groupId }">
												<div class="title-h">
													<h2>${va2.name }</h2>
													<c:if test="${va2.abs != null && va2.abs != '' }">
														<span class="showorhideabs" name="showOrHideAbs">
															<a href="javascript:void(0);">折叠↑</a>
														</span>
													</c:if>
												</div>
												<div class="contain">
													<s:if test="#va2.abs != null && #va2.abs != ''">
														${va2.abs }
													</s:if>
												</div>
											</div>
											<%-- 标准问 --%>
												<s:if test="#request.json.questions != null && #request.json.questions.size > 0">
													<s:set var="isTrue" value="false"/>
													<s:iterator value="#request.json.questions" var="ques"><%--标准问 --%>
														<s:if test="#ques.groupid==#va2.groupId">
															<s:set var="isTrue" value="true"/>
														</s:if>
													</s:iterator>
													<s:if test="#isTrue==true">
														<div class="wenda_list">
															<h2>相关问题</h2>
															<s:iterator value="#request.json.questions" var="ques"><%--标准问 --%>
																<s:if test="#ques.groupid==#va2.groupId"><!-- 注意大小写哦 -->
																<ul>
																	<li class="blue" id="spot${ques.id }">
																		<div class="c-text-ask c-text">问</div>
																		<div class="c-text-ans">
																		${ques.name }
																		</div>
																	</li>
																	<s:iterator value="#ques.answers" var="ans"><%--答案 --%>
																		<li class='orange'>
																			<div class="c-text-answer c-text">答</div>
																			<div class="c-text-ans">
																				${ans.content }
																			</div>
																		</li>
																		<!-- 附件 -->
																		<%-- <s:if test="#va4.attachments != null && #va4.attachments.size > 0">
																			<li class="file">
																				<ul>
																					<s:iterator value="#va4.attachments" var="va5">
																						<li <s:if test="#va5.value.indexOf('.xls')>-1 || #va5.value.indexOf('.xlsx')>-1">class="name-b"</s:if>
																							<s:elseif test="#va5.value.indexOf('doc')>-1 || #va5.value.indexOf('.docx')>-1">class="name-c"</s:elseif>
																							<s:else>class="name-a"</s:else>>
																							<a href="javascript:void(0);"><span></span>
																								<div class="file-con" options="fileId:'${va5.key }',fileName:'${va5.value }'">
																									<p>${va5.value }</p>
																									<div class="file-preview"></div>
																								</div>
																							</a>
																						</li>
																					</s:iterator>
																				</ul>
																			</li>
																		</s:if> --%>
																	</s:iterator>
																	<li>
																		<div class="c-meaubar" options="faqId:'${va3.id }',faq:'${va3.question }'">
																			<ul>
																				<li class="c-icona">
																					<a href="javascript:void(0);"><span></span>签读</a>
																				</li>
																				<li class="c-iconb">
																					<a href="javascript:void(0);"><span></span>评论</a>
																				</li>
																				<li class="c-iconc">
																					<a href="javascript:void(0);"><span></span>收藏</a>
																				</li>
																				<li class="c-icond">
																					<a href="javascript:void(0);"><span></span>分享</a>
																				</li>
																			</ul>
																		</div>
																	</li>
																</ul>
																</s:if>
															</s:iterator>
														</div>
													</s:if>
												</s:if>
											</div>
										</s:if>
									</s:iterator>
									</s:if>
								</s:iterator>
							</div>
					</td>
					<td style="width: 250px; vertical-align: top;">
						<div id="easyui-tabs">
						<div class="easyui-tabs" data-options="plain:true" style="width: 245px;height: 260px;">
							<div title="文章对比" style="padding:5px;overflow: hidden;" 
								data-options="iconCls:'icon-comp'" >
								<div class="slzs_right_con" style="width: 100%;">
					        		<ul>
					        			<div style="overflow-y: auto; max-height: 180px;">
					        				<div id="cmpareArticle">
					        					
					        				</div>
										</div>
										<div>
											<li class="slzs_anniu">
												<a href="javascript:void(0);" id="cmpObject" style="font-size: 13px;">选中对比</a>
												<a href="javascript:void(0);" id="addObject" style="font-size: 13px;">添加文章</a>
											</li>
									    </div>
					        		</ul>
					        	</div>
					        </div>
					        <div title="文章关联" style="padding:5px;overflow: hidden;" 
					        	data-options="iconCls:'icon-rela'" >
					        	<div class="slzs_right_con" style="width: 100%;">
					        		<ul>
					        			<div style="overflow-y: auto; max-height: 220px;">
											<div id="relationArticle">
												
											</div>
										</div>
					        		</ul>
					        	</div>
					        </div>
						</div>
						</div>
						
						<div class="right-sidebar">
						</div>
						
						<div class="detail-d mt20" id="navigationBar">
							<div class="box">
								<div id="timeline" class="timeline">
									<div class="kbs-circle"></div>
									<div class="kbs_w" style="width: 210px;">
										<div class="kbs_n"> 
											<ul class="kbs-one">
												<s:iterator value="#request.json.groups" var="va1" status="st1"><%--一级标题 --%>
													<s:if test="#va1.level==0">
														<li>
															<div class="one-button">
																<a href="#spot${va1.groupId }" style="color: black;" title="${va1.name }">
																	<span></span>
																	<b>
																	<c:if test="${fn:length(va1.name) > 12}">
																		${fn:substring(va1.name,0,11)}...
																	</c:if>
																	<c:if test="${fn:length(va1.name) <= 12}">
																		${va1.name }
																	</c:if>
																	</b>
																</a>
															</div>
															<ul class="kbs-two">
																<s:iterator value="#va1.children" var="va2" status="st2">
																	<s:if test="#va2.level==1"><%--二级标题 --%>
																		<li>
																			<a href="#spot${va2.groupId }"  title="${va2.name }">
																			<em><span></span>
																			<c:if test="${fn:length(va2.name) > 12}">
																				${fn:substring(va2.name,0,11)}...
																			</c:if>
																			<c:if test="${fn:length(va2.name) <= 12}">
																				${va2.name }
																			</c:if>
																			</em>
																			</a>
																		</li>
																	</s:if>
																</s:iterator>
															</ul>
														</li>
													</s:if>
												</s:iterator>
											</ul>
		                             	</div>
									</div>
								</div>
							</div>
							<div class="kbs-fl">
								<li class="d-icona">
									<a href="javascript:void(0);">
										<span id="timeline-show"></span>
									</a>
								</li>
								<li class="d-iconb">
									<a href="#spotRoot">
										<span id="backTop"></span>
									</a>
								</li>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>

