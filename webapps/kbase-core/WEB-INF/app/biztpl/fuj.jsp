<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<base href="<%=basePath%>">
	<title>Document</title>
	<style>
		.fj	{
			width:650px;
			height:auto;
			background:white;
			position:absolute;
			z-index:9999;
		}
		.fj h2{
			height:40px;
			line-height:40px;
			text-indent:2em;
			font-size:13px;
			color:#555;
			background:#F2F2F2;
		}
		.fj_con{
			height:100%;
			padding:6px;
		}
		.fj_con_l{
			width:200px;
			text-indent:10px;
			border:1px solid #333;
			background:#f2f2f2;
			height:430px;
			float:left;
		}
		.fj_con_l h3{
			height:30px;
			line-height:30px;
			font-size:13px;
			color:#444;
			background:#f1f1f1;
			border-bottom:1px solid #acbccb;
		}
		.fj_con_l ul{
			
			padding:8px 0;
		}
		.fj_con_l li{
			height:auto;
			line-height:28px;
			font-size:13px;
		}
		.fj_button{
			clear:both;
			width:auto;
			height:55px;
			text-align:center;
			line-height:55px;
			margin: 0 auto;

		}
		.fj_button button{
			width:92px;
			height:28px;
			line-height:28px;
			background:#d6d6d6;
			border:1px solid #ddd;
			overflow: visible;
			margin-right:50px;
			border-radius:3px;
			font-size:14px;
			cursor:pointer;
		}
		.fj_con_r{
			width:426px;
			height:100%;
			border:1px solid #333;
			background:#f2f2f2;
			float:left;
			margin-left:6px;
			position:relative;
			min-height:355px;
		}
		.fj_con_r li{
			height:auto;
			margin-top:10px;
		}
		.fj_con_r li span{
			display: inline-block;
			width:78px;
			height:26px;
			line-height:26px;
			text-align:right;
			font-size:14px;
			color:#444;
			vertical-align: top
		}
		.fj_con_r li input[type=text]{
			margin-left: 10px;
		    width:250px;
		    padding: 4px;
		    outline: 0;
		    display: inline-block;
		    -webkit-border-radius: 2px;
		    border: 1px #8aa8ca solid;
		}
		.fj_con_r li:first-child input[type=text]{
		    width:190px; 
		}
		.file{
			 position:absolute; 
			 top:12px; 
			 right:60px;
			 height:24px; 
			 filter:alpha(opacity:0);
			 opacity:0;width:70px 
		}
		.fj_con_r li textarea{
			margin-left:5px;
		    width:250px;
		    height:130px;
		    display: inline-block;
		    -webkit-border-radius: 2px;
		    border: 1px #8aa8ca solid;
		}
		.fj_con_r li input[type=button]{
			display: inline-block;
			vertical-align: top;
		    cursor: pointer;
		    background: #eef6ff;
		    text-align: center;
		    height:26px;
		    line-height:22px;
		    border-radius: 2px;
		    font-size: 13px;
		    padding:0 8px;
		    overflow: visible;
		    border: 1px #8aa8ca solid
		}
		.fj_con_r li label{
			width:auto;
			margin-left:12px;
			font-size:13px;
			color:#333;	
		}
		.fj_con_r li input[type=radio]{
			height:26px;
			line-height:26px;
			vertical-align: middle;
			margin-right:3px;
			margin-right:1px\9;
			/*width:14px;	*/
			}
		.fj_con_r table{
			border-collapse:collapse;
			text-align:center;
			width:96%;
			border:1px solid #ccc;
			background:white;
			margin:0 auto 10px;
		}
		.fj_con_r table td{
			height:26px;
			line-height:26px;
			font-size:13px;

		}
		.fj_con_r table thead td{
			height:26px;
			line-height:26px;
			font-size:13px;
			border:1px solid #ccc;

		}
		.fj_con_r table thead td:first-child{
			background:#f2f2f2;
			width:125px;
			border:1px solid #ccc;
		}
		table tr{
			border: 1px solid #ccc;
		}
		table tr td{
			border: 1px solid #ccc;
		}
		table tr td a{
			color: #388eed;
		}
		.icon-newVersion{
			 background: rgba(0, 0, 0, 0) url(resource/biztpl/images/star.png) no-repeat scroll 0 0;
		}
		.icon-oldVersion{
			 background: rgba(0, 0, 0, 0) url(resource/biztpl/images/leaf.png) no-repeat scroll 0 0;
		}
		.tree-title {
    		display: inline;
   	 	}
   	 	.l-btn-left {
   	 		display: inline;
   	 	}
   	 	.l-btn-icon-left .l-btn-text {
		    margin: 0 4px 0 14px;
		}
		.icon-delete {
			background: rgba(0, 0, 0, 0) url(resource/biztpl/images/att_delete.png) no-repeat scroll 0 0;
		}
		.icon-look {
			background: rgba(0, 0, 0, 0) url(resource/biztpl/images/att_look.png) no-repeat scroll 0 0;
		}
		.l-btn-text {
			line-height: 30px;
		}
	</style>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/attament.js"></script>
	<script type="text/javascript">
		var type = '${type}';//0实例上的附件||1答案上的附件
		var ckId = '${ckId}';
		var objectId = '${id}';//实例id或者答案id
		var fileTr = "";//上传附件的html组合代码
		var flg = false;
		var nameArr = new Array();
		var p = $('<p><a href="javascript:void(0)" class="wz_fj" _type="newUpload"></a><i class="wz_del"></i></p>');
		
		function changeFile(_this){
			var fileName = $(_this).val();
			if(fileName){
				fileName = fileName.replace(/^.*[\/\\]/, '');
				searchByName(fileName);
			}
		}
		
		$('body').on('click','#lookAttment',function(){
			var node = $('#attamentDatas').tree('getSelected');
			if(node == null){
				return false;
			}
			loadData(node.id);
		})
		
		function loadData(id){
			$.ajax({
                url: pageContext.contextPath + '/app/biztpl/article!relation.htm?id=' + id,
                type: "get",
                dataType: "json",
                success: function (data) {
                   $("#attmentTable").datagrid("loadData", data.data);
                }
           });
			$('#attmentList').dialog('open');
			$("#attmentList").parent().css('z-index',10000);
		}
		
		$('body').on('click','#deleteAttment',function(){
			if (window.confirm('确定解除吗')){
				var node = $('#attamentDatas').tree('getSelected');
				if(node == null){
					return false;
				}
				if(node.leaf){
					alert('只能解除主文件（顶级节点）的引用，请重新选择');
					return false;
				}
				var msg = delAttament(type,objectId,node.id);
				if(msg){
					if(msg.result){
						alert('解除成功');
						loadTree(0);
					}else{
						alert('解除失败');
					}
				}
			}
		})
		
		$('body').on('click','#search',function(){
			var fileName = $(this).prev('input').val();
			searchByName(fileName);
		})
		
		function searchByName(fileName){
			if (fileName && fileName.length>0){
				$.ajax({
						type: "POST",
						url : pageContext.contextPath + '/app/biztpl/article!attamentSearch.htm',
						data : {'filename': fileName,'topNodeId':type},	
						dataType:'json',
						async: false,
						success: function(data){
							if (data.success) {
								var files = data.data
								 $.each(files,function(n,f) {
								 	nameArr.push(f.name);
								 	var tr = $('<tr id="'+f.id+'"><td>'+f.name+'</td><td><a href="javascript:void(0);" type="quote">引用</a>   <a href="javascript:void(0);" type="add">新增</a>  <a type="look" href="javascript:void(0);">查看引用</a></td></tr>');
								 	$('table[name="search"]').append(tr)
						         }); 
							}
						},
						error :function(data, status, e){
								
						}
				});
			}
		}
		
		$(function(){
			$('button#submit').click(function(){
				if(flg){
					var remark = $('#remark').val().replace(/^\s+|\s+$/g,"");
					if(remark && remark.length > 0){
						if(type == '0'){
							fileTr += '<td>'+$('#remark').val()+'</td>';
							fileTr += '</tr>';
							$('#list').append(fileTr);
						}else if(type == '1'){
							var textarea = $('textarea[id="'+ckId+'"]');
							var div = $(textarea).siblings('div.wd_bj').children('div.drop_down').find('ul li[id="attrachment"] div.wz_r');
							$(p).children('a').attr('remark',$('#remark').val());
							$(div).append(p);
						}
						$('div#mask').css('display','none');
						$('div.fj').hide();
						$('body').css('overflow','auto');
					}else{
						alert('您尚未正确完整的输入表单内容，请检查！');
					}
				}else{
					alert('请先上传附件');
				}
			})
		})
		function loadTree(nodeId){
			$('#attamentDatas').tree({
				url:pageContext.contextPath + '/app/biztpl/article!attamentLoad.htm?objectId=' + objectId + '&node='+nodeId,
				onBeforeExpand: function(node) {// 获取该节点下其他数据
                     $('#attamentDatas').tree('options').url = pageContext.contextPath + '/app/biztpl/article!attamentLoad.htm?objectId=' + objectId + '&node='+node.id;
                }
			});
		}
	</script>
	<script type="text/javascript">
		$(function(){
			loadTree(0);
		})
	</script>
</head>
<body>
	<div class="fj">
		<h2>上传原文附件</h2>
		<div class="fj_con">
			<div class="fj_con_l" style="overflow:auto;">
				<h3>已关联的附件</h3>
				<h3>
					 <a id="lookAttment" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-look" plain="true">查看引用</a>
					 <a id="deleteAttment" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-delete" plain="true">解除引用</a>
				</h3>
				<ul id="attamentDatas" class="easyui-tree">
				</ul>
			</div>
			<div class="fj_con_r">
				<ul>
					<li><span>文件路径:</span><input type="text" id='textfield' onchange="changeFile(this);"> <input type="button" value="浏览文件"  /><input id="upload" type="button" value="上传"  >
					<input type="file" name="file" class="file" id="file" onchange="document.getElementById('textfield').value=this.value;changeFile(this);" />
					</li>
                    <li>
                    	<!-- 法规的附件有:附件列表、废止文件、征求意见稿、其他
						案例的附件有:相关公告 -->
						<kbs:if test="<%=SystemKeys.isShxg() %>">
							<span>文件类型</span>
                    		<label><input type="radio" name='fj' value="5">正文</label>
	                    	<c:choose>
	                    		<c:when test="${categoryName=='法规' }">
	                    			<label><input type="radio" name='fj' value="6"  checked="checked">附件</label>
			                    	<label><input type="radio" name='fj' value="2">废止文件</label>
			                    	<label><input type="radio" name='fj' value="3">征求意见稿</label>
			                    	<label><input type="radio" name='fj' value="4">其他</label>
	                    		</c:when>
	                    		<c:when test="${categoryName=='案例' }">
	                    			<label><input type="radio" name='fj' value="1" checked="checked">相关公告</label>
	                    		</c:when>
	                    		<c:otherwise>
	                    			<label><input type="radio" name='fj' value="6" checked="checked">附件</label>
	                    		</c:otherwise>
	                    	</c:choose>
						</kbs:if>
						<kbs:if test="<%=!SystemKeys.isShxg() %>">
						<!--  
							<label><input type="radio" name='fj' value="6"  checked="checked">附件</label>
	                    	<label><input type="radio" name='fj' value="1">相关公告</label>
	                    	<label><input type="radio" name='fj' value="2">废止文件</label>
	                    -->
						</kbs:if>
                    </li>
					<li><span>备注:</span> <textarea name="" id="remark" cols="30" rows="10"></textarea></li>
				<li><span>搜索:</span><input type="text" placeholder="输入模板名称"> <input id="search" type="button" value="搜索"></li>
				<li><div style="overflow-x:hide; overflow-y:auto;height: 200px;"><table name="search">
						<thead>
							<td>附件名称</td>
							<td>操作</td>
						</thead>
					</table></div>
				</li>
				</ul>
			</div>
			<div id="attmentList" resizable="true" modal="true" closed="true" class="easyui-dialog" title="附件引用关系" style="width:530px;min-height:300px;height:400px;overflow: hidden;z-index: 10000;">
					<table id="attmentTable" class="easyui-datagrid"style="width:700px;height:250px"
						data-options="singleSelect:true,collapsible:true,method:'post'">
						<thead>
							<tr>
								<th data-options="field:'objectName',width:350">被关联对象名称</th>
								<th data-options="field:'typeName',width:150">附件类型</th>
							</tr>
						</thead>
					</table>
			</div>
			<div class="fj_button">
				<button id="submit">提交</button>
				<button id='cancel'>取消</button>
			</div>
		</div>
	</div>
</body>
</html>