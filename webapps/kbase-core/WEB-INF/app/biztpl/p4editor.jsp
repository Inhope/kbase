<%@page import="com.eastrobot.robotface.util.Simp2TranUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta charset="utf-8" />
	<!--用户自定义CSS引入-->
	<style type="text/css">
		*{font-family:Tahoma, Verdana, Georgia; font-size:12px;}
	</style>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor4/ckeditor.js"></script>
	<script type="text/javascript">
		var pageContext = {
			contextPath : '${pageContext.request.contextPath }'
		}
		var p4id = '${p4id}';
		var urlid = '${urlid}';
		if(p4id && p4id.length > 0){
			parent.setP4Id(p4id,urlid);
		}
		function check() {
			CKEDITOR.instances.p4content.updateElement();
			var pcontent = document.getElementById("p4content").value;
			if(!pcontent) {
				alert("<%=Simp2TranUtils.simp2tran("p4设计内容不能为空！")%>");
				document.getElementById("p4content").focus();
				return false;
			}
			return true;
		}
	</script>
</head>
<body style="overflow:hidden;margin: 0">
		<form name="designP4" method="post" action="${pageContext.request.contextPath }/app/biztpl/article-instruction!p4editor.htm" onsubmit="return check();" style="margin-bottom:0;">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
			<input type="hidden" name="p4id" value="${p4id }" />
			<input type="hidden" name="url" value="${url }" />
			<input type="hidden" name="urlid" value="${urlid}" />
		<tr><td align="center">
		<div style="width:100%;height:100%">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="0" align="center">
				<colgroup>
					<col style="width: 50px;">
					<col style="">
				</colgroup>
				<tr>
					<td colspan="2" width="100%" height="100%">
						<textarea id="p4content" name="p4content" style="width: 100%; height:100%;visibility:hidden;">${p4content }</textarea>
						<script>
						CKEDITOR.replace('p4content',
						{
							height:0,width:'100%',
							toolbar:
							[
							    { name: 'document',    items : [ 'Source','-','NewPage','DocProps','-','Templates' ] },
							    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
							    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll'] },
							    '/',
							    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
							    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
							    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
							    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'] },
							    '/',
							    { name: 'styles',      items : [ 'Format','Font','FontSize' ] },
							    { name: 'colors',      items : [ 'TextColor','BGColor'] }   
							],
							extraPlugins : 'word2html',
							filebrowserBrowseUrl : pageContext.contextPath + '/app/biztpl/article-instruction!fileManager.htm?p4id=${p4id}',
							filebrowserUploadUrl : pageContext.contextPath + '/app/biztpl/article-instruction!uploadImage.htm?p4id=${p4id}'
						});
						CKEDITOR.on( 'instanceReady', function( ev )
						{
							var editor = ev.editor;
							var editorContainer = document.getElementById('p4content').parentNode;
							var resizeFn = function(){
								editor.resize('100%',0);
								editor.resize('100%',document.documentElement.clientHeight-25);
							}
							if (editorContainer.addEventListener)
							 window.addEventListener('resize',resizeFn,false)
							else
							 window.attachEvent('onresize',resizeFn)
							resizeFn()
							editor.focus()
						});
						</script>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="submit" name="button" value="<%=Simp2TranUtils.simp2tran("保存设置")%>" />&nbsp;&nbsp;&nbsp;&nbsp;
						<script type="text/javascript">
						function viewp4() {
							var p4url = pageContext.contextPath + '/biztpl/p4data/${p4id}/index.html';
							window.open(p4url);
						}
						function closeP4(){
							parent.closeP4();
						}
						</script>
						<c:if test="${not empty p4id}">
							<input type="button" value="<%=Simp2TranUtils.simp2tran("预览效果")%>" onclick="viewp4();"/>
						</c:if>
						<input id="close" type="button" value="<%=Simp2TranUtils.simp2tran("关闭")%>" onclick="closeP4();"/>
					</td>
				</tr>
			</table>
		
	</div>
	</td></tr></table>
	</form>
	<iframe id="callbackIframe" width="0" height="0" style="display: none;"></iframe>
</body>
</html>
<script type="text/javascript">
	var word2htmlCallback = function(id) {
		if (!id)
			alert('转换失败，请换其他文档重试');
		else 
			p4Win.editP4(id);
	}
</script>