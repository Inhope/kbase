<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>文章管理—二维表格展示</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/article_see.css" />
		<style type="text/css">
			.tabs{
				padding-left: 180px;
			}
			/* 一级标题增加下划线 */
			.tabs li.tabs-selected a.tabs-inner{
				font-size: 18px;
				color: #0076a3;
				font-weight: bolder;
				text-decoration: underline;
			}
			.tabs li a.tabs-inner{
				font-size: 18px;
				font-weight: bolder;
				color: #000;
			}
			/* 二级标题选中的字体样式 */
			.tabs li.tabs-selected a.tabs-inner span.tabs-title .kbs-tabs-title{
				font-size: 12px;
				color: #FF0000;
				font-weight: bolder;
			}
			.tabs li a.tabs-inner .kbs-tabs-title{
				font-size: 12px;
				font-weight: bolder;
				color: #000;
			}
			.kbs-tabs-title{
				word-break:normal;
				width:auto;
				display:block;
				white-space:pre-wrap;
				word-wrap : break-word ;
				overflow: hidden ;
			}
			.kbs-sub-level ul.tabs a.tabs-inner{
				height: auto;
				text-decoration: none;
			}
			.box tr td{
				padding: 10px;
			}
			.kbs-sub-level .tabs{
				background: #f4f8f9;
			}
			.kbs-title{
				height: 50px;
				line-height: 50px;
				font-size: 16px;
				font-weight: bolder;
				float:left;
				margin-left: 20px;
			}
			.kbs-sub-title{
				height: 50px;
				line-height: 50px;
				float: right;
				margin-right: 20px;
			}
			.kbs-ques{
				font-weight: bolder;
			}
			td.kbs-summary{
				font-size: 14px;
				font-weight: bolder;
				color: #0076a3;
			}
			td.kbs-ques{
				color: #lclclc;
				font-size: 14px;
				min-width: 180px;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript">
			$(function(){
				var valueId = '';
				var objectId = '${article.objectId}';
				var cateId = '${article.inCategoryId}';
				var catePath = '${article.inBh}';
				
				$(document).on('contextmenu', function(e){
					e.preventDefault();
				});
			
				$('#btnArticle').click(function(){
					location.href = $.fn.getRootPath() + '/app/biztpl/article-search.htm?articleId=${param.articleId}';
				});
				$('#btnRefresh').click(function(){
					location.reload();
				});
				
				$('.kbs-ans').on('mouseup', function(e){
					if (e.button==2){
						valueId = $(this).attr('_valueid');
						$('#mm').menu('show', {
						    left: e.pageX,
						    top: e.pageY
						});
					}
				});
				//签读
				$('#jNavMarkread').click(function(){
					var _url = $.fn.getRootPath() + '/app/search/search!markreadObject.htm';
					var _params = {
						'valueId': valueId,
						'objectId': objectId,
						'categoryId': cateId,
						'catePath': catePath
					}
					$.post(_url, _params, function(data){
						if (data==1){
							alert('知识签读成功');
						}else{
							alert('知识已签读');
						}
					}, 'text');
				});
				//纠错
				$('#jNavErrcorrect').click(function(){
					var _url = $.fn.getRootPath() + '/app/corrections/corrections!send.htm?objId=' + objectId + '&faqId=' + valueId + '&cateId=' + cateId;
					window._open(_url, '知识反馈', 840, 500);
				});
				//收藏
				$('#jNavFav').click(function(){
					var _url = $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?faqid=' + valueId + '&cateid=' + cateId;
					window._open(_url, '知识收藏', 840, 500);
				});
				//推荐
				$('#jNavShare').click(function(){
					var _url = $.fn.getRootPath() + '/app/recommend/recommend!open.htm?valueid=' + valueId + '&cateid=' + cateId;
					window._open(_url, '知识推荐', 840, 500);
				});
				
				
				$('.kbs-level0').tabs({
					onSelect: function(title, index){
						$('.kbs-sub-level').tabs('select', 0);
					}
				});
			});
		</script>
	</head>
	
	<body>
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'north'" style="height:50px;overflow-y: hidden;background: #fdfdfd;">
				<div class="kbs-title">${article.inBhName } > ${article.objectName }</div>
				<div class="kbs-sub-title">
					<a id="btnRefresh" href="javascript:void(0)" class="easyui-linkbutton">刷新</a>
					<a id="btnArticle" href="javascript:void(0)" class="easyui-linkbutton">文章展示</a>
					${article.modifyDate }
				</div>
			</div>
			<div data-options="region:'center',split:true">
				<%-- 横向 tabs 加载 --%>
				<div class="easyui-tabs kbs-level0">
					<s:iterator value="#request.groups['root']" var="level0" status="status0">
						<div title="${level0.name }" style="padding:10px;">
							<%-- 纵向 tabs 加载 --%>
							<div class="easyui-tabs kbs-sub-level" data-options="tabPosition:'left',tabHeight:'auto'">
								<div style="padding:10px;">
									<%--
										 一级标题
										 1、先显示一级标题的摘要
										 2、显示一级标题下的qaList
									 --%>
									<s:set var="tmpQaList" value="#request.quess[#level0.groupId]"></s:set>
									<s:if test="(#level0.groupAbs!=null && #level0.groupAbs!='') || (#tmpQaList!=null && #tmpQaList.size>0)">
										<table class="box kbs-level0-${status0.index }" style="width:100%;margin-bottom: 10px;background: #f4f8f9;">
											<s:if test="#level0.groupAbs!=null && #level0.groupAbs!=''">
												<tr>
													<td colspan="2" class="kbs-summary">${level0.groupAbs }</td>
												</tr>
											</s:if>
											<s:iterator value="#tmpQaList" var="qa">
												<tr>
													<td width="15%" class="kbs-ques">${qa.question }</td>
													<td width="85%" class="kbs-ans" _valueid='${qa.id }'>
														<s:iterator value="#qa.answers" var="ans">
															${ans.answer }
														</s:iterator>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
								</div>
								<s:iterator value="#request.groups[#level0.groupId]" var="level1" status="status1">
									<div title="<div class='kbs-tabs-title'>${level1.name }</div>" style="padding:10px;">
										<%-- 
											二级标题
											1、先显示二级标题下的摘要
											2、显示二级标题下的qaList
										 --%>
										<s:set var="tmpQaList" value="#request.quess[#level1.groupId]"></s:set>
										<s:if test="(#level1.groupAbs!=null && #level1.groupAbs!='') || (#tmpQaList!=null && #tmpQaList.size>0)">
											<table class="box" style="width:100%;background:#FCFEFD;">
												<s:if test="#level1.groupAbs!=null && #level1.groupAbs!=''">
													<tr>
														<td colspan="2">${level1.groupAbs }</td>
													</tr>
												</s:if>
												<s:iterator value="#tmpQaList" var="qa">
													<tr>
														<td width="15%" class="kbs-ques">${qa.question }</td>
														<td width="85%" class="kbs-ans">
															<s:iterator value="#qa.answers" var="ans">
																${ans.answer }
															</s:iterator>
														</td>
													</tr>
												</s:iterator>
											</table>
										</s:if>
									</div>
								</s:iterator>
							</div>
						</div>
					</s:iterator>
				</div>
			</div>
		</div>
		
		<div id="mm" class="easyui-menu" style="width:120px;">
			<div id="jNavMarkread">签读</div>
			<div id="jNavErrcorrect">评论</div>
			<div id="jNavFav">收藏</div>
			<div id="jNavShare">分享</div>
		</div>
	</body>
</html>
