<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>文章管理—查看</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/article_see.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/js/ArticleSearch.js"></script>
		<script type="text/javascript">
			var _objId, /*实例id(与文章id共用)*/
				_objName, /*实例名称*/
				_cateId, /*分类id*/
				_cateName, /*分类名称*/
				_cateBh, /*分类路径*/
				_cateBhName;/*分类路径（中文）*/
		</script>
	</head>
	<body style="overflow-y: auto;">
		<%--页面主体 --%>
	    <div class="easyui-layout" data-options="fit:true">
	    	<div data-options="region:'west', border:false" style="width:20px;">&nbsp;</div>
	    	<div data-options="region:'center', border:false">
				<table width="80%" border="0" cellspacing="0" cellpadding="0" class="tab_cont" style="background:white">
					<tr>
						<td>
							<div class="detail-a">
								<div class="a-title pl20">
									<h1>${article.name }<a class="title-a">编辑</a></h1>
								</div>
								<div class="about pl20">
		    						<s:if test="#request.article.articleAbs != null && #request.article.articleAbs != ''">
										${article.articleAbs1 }
									</s:if>
		    					</div>
								<div class="a-con" style="width: 100%; margin:0 auto">
									<div class="con-content">
										<div class="catalog">
											<div class="catalog-h">
												<h2 class="block-title">目录</h2>
												<div class="catalog-list column-4">
													<ol>
														<s:iterator value="#request.groups['root']" var="va1" status="st1">
															<li class="level1">
																<span class="index">${st1.index+1 }.</span>
																<span class="text"><a href="#spotRoot${st1.index+1 }">${st1.index+1 }.&nbsp;&nbsp;${va1.name }</a></span>
															</li>
															<s:iterator value="#request.groups[#va1.groupId]" var="va2" status="st2">
					    										<li class="level2">
																	<span class="index">▪</span>
																	<span class="text"><a href="#spotRoot${st1.index+1 }.${st2.index+1 }">${va2.name }</a></span>
																</li>
					    									</s:iterator>
					    								</s:iterator>
													</ol>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
						<td style="width: 320px; margin-left: 40px;">
							<div class="detail-b">
								<div class="b-title">
									<ul>
										<li class="b-icona">
											<a href="javascript:void(0);" class="current"><div>
													<span></span>文章对比
												</div>
											</a>
										</li>
										<li class="b-iconb">
											<a href="javascript:void(0);"><div>
													<span></span>文章关联
												</div>
											</a>
										</li>
									</ul>
								</div>
								<div style="clear: both;"></div>
								<div class="b-con">
									<ul class="b-choose">
										<li>
											<span><input type="checkbox">
											</span>借记卡
										</li>
										<li>
											<span><input type="checkbox">
											</span>借记卡交易
										</li>
										<li>
											<span><input type="checkbox">
											</span>信用卡交易
										</li>
										<li>
											<span><input type="checkbox">
											</span>信用卡
										</li>
									</ul>
									<div style="height: 20px; clear: both;"></div>
									<ul class="b-button">
										<li>
											<a href="javascript:void(0);">选中对比</a>
										</li>
										<li>
											<a href="javascript:void(0);">添加文章</a>
										</li>
									</ul>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="detail-c">
								<div class="a-title pl20" id="spotRoot">
									<div class="title-b">
										<h2 class="title-h2">
											正文
										</h2>
									</div>
								</div>
								<div class="a-con">
									<%-- 一级标题 ********************************************--%>
									<s:iterator value="#request.groups['root']" var="va1" status="st1">
										<div class="con-title mt20">
											<div class="ab-title" id="spotRoot${st1.index+1 }">
												<div class="title-h">
													<h2>${va1.name }</h2>
												</div>
                                                <div class="contain">
												${va1.groupAbs1 }
                                                </div>
											</div>
											<%-- 标准问 --%>
											<div class="wenda_list">
												<h2>相关问题</h2>
												<ul>
													<li class="blue">
														<i class="c-text-ask c-text">问</i>这是一个问题这是一个问题这是一个问题这是一个问题这是一个问题
													</li>
													<li class='orange'>
														<i class="c-text-answer c-text">答</i>这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难这是一个大难
													</li>
												</ul>
											</div>
										</div>
										<div style="clear: both"></div>
										<%-- 二级级标题 ********************************************--%>
										<s:iterator value="#request.groups[#va1.groupId]" var="va2" status="st2">
											<div <s:if test="#st2.first">class="con-content"</s:if><s:else>class="con-content pt10"</s:else>>
												<div class="conb-title" id="spotRoot${st1.index+1 }.${st2.index+1 }">
													<div class="title-h">
														<h2>${va2.name }</h2>
													</div>
                                                    <div class="contain">
													${va2.groupAbs1 }
                                                    </div>
												</div>
												<%-- 标准问 --%>
												<div class="wenda_list" style="float: right;margin-bottom: 20px;">
													<s:iterator value="#request.quess[#va2.groupId]" var="va3"><%--标准问 --%>
														<h2>相关问题</h2>
														<ul>
															<li class="blue">
																<i class="c-text-ask c-text">问</i>
																${va3.name }
															</li>
															<li class='orange'>
																<i class="c-text-answer c-text">答</i>
																<s:iterator value="#va3.answers" var="va4"><%--答案 --%>
																	${va4.content }
																</s:iterator>
															</li>
														</ul>
													</s:iterator>
												</div>
												<div style="height: 10px; width: 100%;"></div>
											</div>
										</s:iterator>
									</s:iterator>
								</div>
							</div>
						</td>
						<td style="margin-left: 40px;" valign="top">
							<div class="detail-d mt20">
								<div class="box">
									<div id="timeline" class="timeline">
										<div class="kbs-circle"></div>
										<ul class="kbs-one">
											<s:iterator value="#request.groups['root']" var="va1" status="st1"><%--一级标题 --%>
												<li>
													<div class="one-button">
														<a href="#spotRoot${st1.index+1 }" style="color: black;">
															<span></span>${st1.index+1 }&nbsp;&nbsp;${va1.name }</a>
													</div>
													<ul class="kbs-two">
														<s:iterator value="#request.groups[#va1.groupId]" var="va2" status="st2"><%--二级标题 --%>
															<li>
																<a href="#spotRoot${st1.index+1 }.${st2.index+1 }">
																	<span></span>${st1.index+1 }.${st2.index+1 }&nbsp;&nbsp;${va2.name }</a>
															</li>
														</s:iterator>
													</ul>
												</li>
											</s:iterator>
										</ul>
									</div>
								</div>
								<div class="kbs-fl">
									<li class="d-icona">
										<a href="javascript:void(0);">
											<span id="timeline-show"></span>
										</a>
									</li>
									<li class="d-iconb">
										<a href="#spotRoot">
											<span id="backTop"></span>
										</a>
									</li>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
	    </div>
	</body>
</html>
