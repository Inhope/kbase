<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>文章管理—查看</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/article_see.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/css/right-sidebar.css" />
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/FavBall.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/kbase.errcorrect.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/js/ArticleSearch.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-scroll.js"></script>
		<script type="text/javascript">
			$(function(){
				$.extend(true, window.ArticleSearch, {
					/*标准问*/
					req: function(objId, cateId, cateBh){
						var fn = ArticleSearch.fn;
						/*标准问-签读、评论、收藏*/
						$('div[class="c-meaubar"]').each(function(){
							var options = fn.parseJSON($(this).attr('options')), 
								faqId = options.faqId;
							
							/*签读*/
							$(this).find('li[class="c-icona"] a:eq(0)')
								.click(function(){
								var url = '${pageContext.request.contextPath }/app/category/all-ans!view.htm?valueId=' + faqId;
								window._open(url, '其它平台', '700', '500');
							});
							
							/*评论*/
							$(this).find('li[class="c-iconb"] a:eq(0)')
								.click(function(){
									$.kbase.errcorrect({
										'objId': objId,
										'faqId': faqId,
										'cateId': cateId
									});
							});
							
							/*收藏*/
							$(this).find('li[class="c-iconc"] a:eq(0)')
								.click(function(){
									FavBallExt.f_openPanel(null, faqId, cateId);
							});
							
							/*推荐*/
							$(this).find('li[class="c-icond"] a:eq(0)')
								.click(function(){
								
									fn.layer({
										title: '分享', height: $(window).height()*0.90, width: '640', 
										url: '/app/recommend/recommend!open.htm',
										params: {
											cateid: cateId,
											valueid: faqId
										}
									});
							});
							
						});
					}
				});
				
				ArticleSearch.setting({
					objId: '${article.objectId}', /*实例id(与文章id共用)*/
					objName: '${article.objectName}', /*实例名称*/
					cateId: '${article.inCategoryId}', /*分类id*/
					cateName: '${article.inCategoryName}', /*分类名称*/
					cateBh: '${article.inBh}', /*分类路径*/
					cateBhName: '${article.inBhName}', /*分类路径（中文）*/
					spotId: '${spotId}', /*锚点id*/
					converterPath: '${requestScope.converterPath}',/*附件转换地址*/
					tplId: '${tplId}', /*文章模板id*/
					doDownload: '${keyMenus['K-AttachmentDownload']!=null}',/*知识附件下载权限*/
					robotPath: '${robot_context_path}',/*robot地址*/
					groupList: ${groupList}
				});
				
				$(window).bind('resize',function(){
					//location.reload();
					//重新判断是否显示导航
					var bar = $('#navigationBar'), 
						hgBar = $(bar).height();
					ArticleSearch.timeline.navBar(hgBar, bar);
				});
				
				//点击（正文摘要、标题摘要、答案）图片放大展示
				$("body table[class='tab_cont']").on("dblclick",
					"div[class='detail-a'] [calss='textarea_div'] img,div[class='detail-c'] [class='contain'] img,div[class='detail-c'] [class='c-text-ans'] img",
					function(){
						var choose_image_toBig = $("#choose_image_toBig");
						var imageDom = $("#choose_image_toBig").find("img");
						var _height = $(window).height();
						var _width = $(window).width();
						choose_image_toBig.css({
							height:_height,
							width:_width
						});
						imageDom.css({
							'max-height':_height-4,
							'max-width':_width-4
						});
						imageDom.attr("src",$(this).attr("src"));
						choose_image_toBig.show();
					}
				);
			});
		</script>
	</head>
	<body class="easyui-layout" data-options="fit:true" >
    	<div data-options="region:'center', border:false">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:white;">
				<tr>
					<td>
						<div class="pl20 position" id="categoryNv"></div>
					</td>
                    <td style="vertical-align: top;text-align: right;padding-right: 5px;">
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-ckwd'" id="refer">参考文档</a>
						<%-- 
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-bb'"   id="edition">版本</a>
						--%>
						<a href="javascript:void(0);" class="easyui-linkbutton" 
							data-options="iconCls:'icon-sc1'"  id="favClip">收藏</a>
						<kbs:if key="ArticleEdit">
							<a href="javascript:void(0);" class="easyui-linkbutton" 
			        			data-options="iconCls:'icon-bj'"   id="edit">编辑</a>
						</kbs:if>
			        	<a href="javascript:void(0);" class="easyui-linkbutton" 
			        		data-options="iconCls:'icon-bb'"  id="open">最大化</a>
			        	<%-- add by eko.zhan at 2016-09-12 17:10 --%>
			        	<a href="javascript:void(0);" class="easyui-linkbutton" 
			        		data-options="iconCls:'icon-view'" id="btn2d">二维展示</a>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tab_cont" style="background:white">
				<tr>
					<td style="vertical-align: top;">
						<div class="detail-a">
							<div class="a-title pl20" style="margin-top:auto;">
								<h1>${article.objectName }</h1>
							</div>
							<div class="about">
								<c:if test="${article.createUser != ''}">
									<span>创建人</span>
		    						<span>${article.createUser }</span>
								</c:if>
  								<span>${article.createDate }</span>
	    					</div>
							<div class="about pl20">
	    						<s:if test="#request.article.articleAbs != null && #request.article.articleAbs != ''">
									<div class="textarea_div">${article.articleAbs }</div>
								</s:if>
	    					</div>
							<div class="a-con" style="width: 100%; margin:0 auto">
								<div class="con-content">
									<div class="main_ml" id="spotRoot">
										<div class="left_ml">
											<s:set var="totalCount" value="#request.groupList.size"/>
											<s:set var="columnSize" value="(#totalCount%3)==0?(#totalCount/3):(#totalCount/3+1)"/>
											<h2 class="block-title">目录</h2>
										</div>
										<div class="right_ml">
											<ul class="right_u">
												<c:forEach items="1,2,3" var="columnNum">
													<li>
														<div class="catalog-list">
															<ol>
															<c:if test="${fn:length(groupList) > 0 }">
															<c:forEach items="${groupList }" 
																begin="${(columnNum-1)*columnSize }" end="${columnNum*columnSize-1 }" var="va">
																<li class="${va.level_ == 0 ? 'level1' : 'level2'}">
																	<%-- <span class="index"></span> --%>
																	<span class="text">
																		<font style="margin-right: 5px;">${va.level_ == 0 ? '' : '▪'}</font>
																		<a href="#spot${va.groupId }" title="${va.name }">
																			<c:if test="${fn:length(va.name) > 12}">
																				${fn:substring(va.name,0,12)}...
																			</c:if>
																			<c:if test="${fn:length(va.name) <= 12}">
																				${va.name }
																			</c:if>
																		</a>
																	</span>
																</li>
															</c:forEach>
															</c:if>
															</ol>
														</div>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="detail-c">
							<div class="a-title pl20">
								<div class="title-b">
									<h2 class="title-h2">
										正文
									</h2>
								</div>
							</div>
							<div class="a-con">
								<%-- 一级标题 ********************************************--%>
								<s:iterator value="#request.groups['root']" var="va1" status="st1">
									<div class="con-title mt20">
										<div class="ab-title" id="spot${va1.groupId }">
											<div class="title-h">
												<h2>${va1.name }</h2>
												<c:if test="${va1.groupAbs != null && va1.groupAbs != '' }">
													<span class="showorhideabs" name="showOrHideAbs">
														<a href="javascript:void(0);">折叠↑</a>
													</span>
												</c:if>
											</div>
											<div class="contain">
												<s:if test="#va1.groupAbs != null && #va1.groupAbs != ''">
													${va1.groupAbs }
												</s:if>
											</div>
										</div>
										<%-- 标准问 --%>
										<s:set var="quess1" value="#request.quess[#va1.groupId]"></s:set>
										<s:if test="#quess1 != null && #quess1.size > 0">
											<div class="wenda_list">
												<h2>相关问题</h2>
												<ul>
													<s:iterator value="#quess1" var="va3"><%--标准问 --%>
														<li class="blue" id="spot${va3.id }">
															<div class="c-text-ask c-text">问</div>
															<div class="c-text-ans">
															${va3.question }
															</div>
														</li>
														<s:iterator value="#va3.answers" var="va4"><%--答案 --%>
															<li class='orange'>
																<div class="c-text-answer c-text">答</div>
																<div class="c-text-ans">
																	${va4.answer }
																</div>
															</li>
															<s:set var="hasAttachments" value="false"/><!-- 是否有附件 -->
															<s:if test="#va4.attachments != null && #va4.attachments.size > 0">
																<s:set var="hasAttachments" value="true"/>
																<li class="file">
																	<ul>
																		<s:iterator value="#va4.attachments" var="va5">
																			<li <s:if test="#va5.value.indexOf('.xls')>-1 || #va5.value.indexOf('.xlsx')>-1">class="name-b"</s:if>
																				<s:elseif test="#va5.value.indexOf('doc')>-1 || #va5.value.indexOf('.docx')>-1">class="name-c"</s:elseif>
																				<s:else>class="name-a"</s:else>>
																				<a href="javascript:void(0);"><span></span>
																					<div class="file-con" options="fileId:'${va5.key }',fileName:'${va5.value }'">
																						<p>${va5.value }</p>
																						<div class="file-preview"></div>
																					</div>
																				</a>
																			</li>
																		</s:iterator>
																	</ul>
																</li>
															</s:if>
															<s:if test="#va4.p4s != null && #va4.p4s.size > 0">
																<li class="file" <s:if test="#hasAttachments">style="margin-top:-15px;"</s:if> ><!-- 如果前面已有附件，为了样式，此处设置maring-top -->
																	<ul>
																		<s:iterator value="#va4.p4s" var="va5">
																			<li class="name-a">
																				<a href="${va5.key }" target="block"><span></span>
																					<div class="file-con">
																						<p>${va5.value }</p><p>打开p4</p>
																					</div>
																				</a>
																			</li>
																		</s:iterator>
																	</ul>
																</li>
															</s:if>
														</s:iterator>
														<li>
															<div class="c-meaubar" options="faqId:'${va3.id }',faq:'${va3.question }'">
																<ul>
																	<!-- 
																	<li class="c-icona">
																		<a href="javascript:void(0);"><span></span>签读</a>
																	</li>
																	 -->
																	<li class="c-icona">
																		<a href="javascript:void(0);"><span></span>其它平台</a>
																	</li>
																	<li class="c-iconb">
																		<a href="javascript:void(0);"><span></span>评论</a>
																	</li>
																	<li class="c-iconc">
																		<a href="javascript:void(0);"><span></span>收藏</a>
																	</li>
																	<li class="c-icond">
																		<a href="javascript:void(0);"><span></span>分享</a>
																	</li>
																</ul>
															</div>
														</li>
													</s:iterator>
												</ul>
											</div>
										</s:if>
									</div>
									<div style="clear: both"></div>
									<%-- 二级级标题 ********************************************--%>
									<s:iterator value="#request.groups[#va1.groupId]" var="va2" status="st2">
										<s:if test="#va2.groupId != ''">
										<div <s:if test="#st2.first">class="con-content"</s:if><s:else>class="con-content pt10"</s:else>>
											<div class="conb-title" id="spot${va2.groupId }">
												<div class="title-h">
													<h2>${va2.name }</h2>
													<c:if test="${va2.groupAbs != null && va2.groupAbs != '' }">
														<span class="showorhideabs" name="showOrHideAbs">
															<a href="javascript:void(0);">折叠↑</a>
														</span>
													</c:if>
												</div>
												<div class="contain">
													<s:if test="#va2.groupAbs != null && #va2.groupAbs != ''">
														${va2.groupAbs }
													</s:if>
												</div>
											</div>
											<%-- 标准问 --%>
											<s:set var="quess2" value="#request.quess[#va2.groupId]"></s:set>
											<s:if test="#quess2 != null && #quess2.size > 0">
												<div class="wenda_list">
													<h2>相关问题</h2>
													<ul>
														<s:iterator value="#quess2" var="va3"><%--标准问 --%>
															<li class="blue" id="spot${va3.id }">
																<div class="c-text-ask c-text">问</div>
																<div class="c-text-ans">
																	${va3.question }
																</div>
															</li>
															<s:iterator value="#va3.answers" var="va4"><%--答案 --%>
																<li class='orange'>
																	<div class="c-text-answer c-text">答</div>
																	<div class="c-text-ans">
																		${va4.answer }
																	</div>
																</li>
																<s:set var="hasAttachments" value="false"/><!-- 是否有附件 -->
																<s:if test="#va4.attachments != null && #va4.attachments.size > 0">
																	<s:set var="hasAttachments" value="true"/>
																	<li class="file">
																		<ul>
																			<s:iterator value="#va4.attachments" var="va5">
																				<li <s:if test="#va5.value.indexOf('.xls')>-1 || #va5.value.indexOf('.xlsx')>-1">class="name-b"</s:if>
																					<s:elseif test="#va5.value.indexOf('doc')>-1 || #va5.value.indexOf('.docx')>-1">class="name-c"</s:elseif>
																					<s:else>class="name-a"</s:else>>
																					<a href="javascript:void(0);"><span></span>
																						<div class="file-con" options="fileId:'${va5.key }',fileName:'${va5.value }'">
																							<p>${va5.value }</p>
																							<div class="file-preview"></div>
																						</div>
																					</a>
																				</li>
																			</s:iterator>
																		</ul>
																	</li>
																</s:if>
																<s:if test="#va4.p4s != null && #va4.p4s.size > 0">
																	<li class="file" <s:if test="#hasAttachments">style="margin-top:-15px;"</s:if> ><!-- 如果前面已有附件，为了样式，此处设置maring-top -->
																		<ul>
																			<s:iterator value="#va4.p4s" var="va5">
																				<li class="name-a">
																					<a href="${va5.key }" target="block"><span></span>
																						<div class="file-con">
																							<p>${va5.value }</p><p>打开p4</p>
																						</div>
																					</a>
																				</li>
																			</s:iterator>
																		</ul>
																	</li>
																</s:if>
															</s:iterator>
															<li>
																<div class="c-meaubar" options="faqId:'${va3.id }',faq:'${va3.question }'">
																	<ul>
																		<!-- 
																		<li class="c-icona">
																			<a href="javascript:void(0);"><span></span>签读</a>
																		</li>
																		 -->
																		<li class="c-icona">
																			<a href="javascript:void(0);"><span></span>其它平台</a>
																		</li>
																		<li class="c-iconb">
																			<a href="javascript:void(0);"><span></span>评论</a>
																		</li>
																		<li class="c-iconc">
																			<a href="javascript:void(0);"><span></span>收藏</a>
																		</li>
																		<li class="c-icond">
																			<a href="javascript:void(0);"><span></span>分享</a>
																		</li>
																	</ul>
																</div>
															</li>
														</s:iterator>
													</ul>
												</div>
											</s:if>
										</div>
										</s:if>
									</s:iterator>
								</s:iterator>
							</div>
						</div>
						<div style="height: 20px; width: 100%;"></div>
					</td>
					<td style="width: 250px; vertical-align: top;">
						<div id="easyui-tabs">
						<div class="easyui-tabs" data-options="plain:true" style="width: 245px;height: 260px;">
							<div title="文章对比" style="padding:5px;overflow: hidden;" 
								data-options="iconCls:'icon-comp'" >
								<div class="slzs_right_con" style="width: 100%;">
					        		<ul>
					        			<div style="overflow-y: auto; max-height: 180px;">
					        				<div id="cmpareArticle">
					        					
					        				</div>
										</div>
										<div>
											<li class="slzs_anniu">
												<a href="javascript:void(0);" id="cmpObject" style="font-size: 13px;">选中对比</a>
												<a href="javascript:void(0);" id="addObject" style="font-size: 13px;">添加文章</a>
											</li>
									    </div>
					        		</ul>
					        	</div>
					        </div>
					        <div title="文章关联" style="padding:5px;overflow: hidden;" 
					        	data-options="iconCls:'icon-rela'" >
					        	<div class="slzs_right_con" style="width: 100%;">
					        		<ul>
					        			<div style="overflow-y: auto; max-height: 220px;">
											<div id="relationArticle">
												
											</div>
										</div>
					        		</ul>
					        	</div>
					        </div>
						</div>
						</div>
						
						<div class="right-sidebar">
							<c:if test="${fn:length(propertyJa) > 0 }">
							<div class="wzsx wzmt">
								<h2 class="title-wzsx">文章属性</h2>
								<ul class="ul-wzsx">
									<c:forEach items="${propertyJa }" var="property">
										<c:if test="${property.type == 'single' }">
											<li><span>${property.propertyName }:</span>${property.propertyValue }</li>
										</c:if>
										<c:if test="${property.type == 'multi' }">
											<li><span>${property.propertyName }:</span>${property.propertyValue }</li>
										</c:if>
									</c:forEach>
								</ul>
							</div>
							</c:if>
							<c:if test="${fn:length(labelJa) > 0 }">
								<div class="wztag wzmt">
									<h2 class="title-wzsx">标签</h2>
									<div id="main_demo">
										<div id="slider">
											<a class="buttons prev" href="#">left</a>
											<div class="viewport">
												<ul class="overview">
													<c:set var="maxNum" value="4"/>
													<c:forEach items="${labelJa }" var="label" varStatus="labelIndex">
														<c:set value="" var="jaIndex"/>
														<c:if test="${labelIndex.index%maxNum==0 }">
															<c:set value="${labelIndex.index/maxNum }" var="jaIndex"/>
															<li>
																<div class="view-span">
														</c:if>
																	<span>${label }</span>
														<c:if test="${labelIndex.index%maxNum==0 && labelIndex.index/maxNum > jaIndex }">
																</div>
															</li>
														</c:if>
													</c:forEach>
												</ul>
											</div>
											<a class="buttons next" href="#">right</a>
										</div>
									</div>
								</div>
							</c:if>
						</div>
						
						<div class="detail-d mt20" id="navigationBar">
							<div class="box">
								<div id="timeline" class="timeline">
									<div class="kbs-circle"></div>
                                        <div class="kbs_w" style="width: 210px;">
                                        	<div class="kbs_n"> 
											<ul class="kbs-one">
												<s:iterator value="#request.groups['root']" var="va1" status="st1"><%--一级标题 --%>
													<li>
														<div class="one-button">
															<a href="#spot${va1.groupId }" style="color: black;" title="${va1.name }">
																<span></span>
																<b>
																<c:if test="${fn:length(va1.name) > 12}">
																	${fn:substring(va1.name,0,11)}...
																</c:if>
																<c:if test="${fn:length(va1.name) <= 12}">
																	${va1.name }
																</c:if>
																</b>
															</a>
														</div>
														<ul class="kbs-two">
															<s:iterator value="#request.groups[#va1.groupId]" var="va2" status="st2"><%--二级标题 --%>
																<s:if test="#va2.groupId != ''">
																<li>
																	<a href="#spot${va2.groupId }" title="${va2.name }">
																		<em><span></span>
																		<c:if test="${fn:length(va2.name) > 12}">
																			${fn:substring(va2.name,0,11)}...
																		</c:if>
																		<c:if test="${fn:length(va2.name) <= 12}">
																			${va2.name }
																		</c:if>
																		</em>
																	</a>
																</li>
																</s:if>
															</s:iterator>
														</ul>
													</li>
												</s:iterator>
											</ul>
                                         </div>
                                        </div>
								</div>
							</div>
							<div class="kbs-fl">
								<li class="d-icona">
									<a href="javascript:void(0);">
										<span id="timeline-show"></span>
									</a>
								</li>
								<li class="d-iconb">
									<a href="#spotRoot">
										<span id="backTop"></span>
									</a>
								</li>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	    <%-- 图片放大 --%>
		<div id="choose_image_toBig" style="display: none;position: absolute;background-color: #DFDFDF;text-align: center;top:0;z-index: 3001;">
			<span style="height: 100%;display: inline-block;vertical-align: middle;"></span>
			<img src="" ondblclick="$(this).parent().hide();" style="vertical-align: middle;"/>
			<a href="javascript:void(0);" onclick="$(this).parent().hide();" style="vertical-align: top; margin-left: 20px;">关闭</a>
		</div>
	</body>
</html>
