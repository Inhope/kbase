<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>业务模板搜索-文章添加</title>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/shili_dan.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<STYLE type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;}
			.shili_dan_con1>div, .shili_dan_con1>div>div{height: 335px;}
		</STYLE>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript">
			$(function(){
				window.CmpareUL = {
					getLi: function(id){
						return $('div[class="shili_dan_right1_con"]').find('li[objId="' + id + '"]');
					},
					addLi: function (id, name) {
						var lis = CmpareUL.getLi(id);
						if(lis && lis.length == 0){
							/*添加当前页面对比实例元素*/
							var li = $('<li></li>');
							li.attr('objId', id);
							li.append('<p>' + name +'</p>');
							li.append('<img src="' + $.fn.getRootPath() + '/theme/green/resource/search/images/xinxi_ico5.jpg"/>');
							$(li).children('img').click(function(){
								CmpareUL.rmLi(id);
							});
							$('#compareSortable').append(li);
						} else 
							$.messager.alert('信息', '请勿重复添加,对象:' + name + '!');
					},
					rmLi: function(id){
						/*移除当前页面对比实例元素*/
						var lis = CmpareUL.getLi(id);
						$(lis).each(function(){
							$(this).remove(); 
						});
					},
					init: function(){
						$('#compareSortable').find('li[objId]').each(function(i, o){
							$(o).find('img').click(function(){
								CmpareUL.rmLi($(o).attr('objId'));
							});
						});
					}
				};
				CmpareUL.init();
				
				/*知识树*/
				var catalogTree = {
					zTree : null,
					setting : {
						view: {
							expandSpeed: "fast"//空不显示动画
						},
						async : {
							enable : true,
							url : $.fn.getRootPath() + "/app/search/object-search!getCatalogObject.htm",
							autoParam : ["id", "name=n", "level=lv","bh"],
							dataFilter : function(treeId, parentNode, responseData){
								for(var i =0; i < responseData.length; i++) {
									if(responseData[i].isParent == "true"){
							    		responseData[i].nocheck = true;
									}
							    }
							    return responseData;
						   }
						},
						check: {
							enable: true,
							chkboxType : { "Y" : "ps", "N" : "ps" }
						},
						callback: {
							onCheck : function(event, treeId, treeNode){
								if(treeNode.checked == true) {
									var id = treeNode.id;
								    var name = treeNode.name;
								   	CmpareUL.addLi(id, name);
								}
							}
						}
					},
					setZtreeParam : function(params) {
						this.setting.async.otherParam = params;
					},
					refreshTree : function(params) {
						this.setZtreeParam(params);
						$.fn.zTree.init($("#catalogTree"), this.setting);
					},
					init : function(){
						$.fn.zTree.init($("#catalogTree"), this.setting);
						this.zTree=$.fn.zTree.getZTreeObj('catalogTree');
					}
				}
				catalogTree.init();
				
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid'), /*datagrid对象*/
					getQueryParams: function(){
						return { objectName: $('#srCon').val() }
					}
				});
				
				
				/*将选择的对比实例保存到数据库中*/
				$('#sureBtn').click(function() {
					var dataLis = $('#compareSortable').find('li[objId]');
					var objectComparisions = new Array(dataLis.length);
					$.each(dataLis, function(i, n){
					  	var objectComparision = {};
						objectComparision.objectId = '${param.objId}';
						objectComparision.compareObjectId = $(this).attr('objId');
						objectComparision.compareObjectName = $(this).find('p').text();
						objectComparision.saveInManager = 0;//来源非后台
						objectComparision.dataIndex = i + 1;//获取现有元素的个数 + 1
						objectComparisions[i] = objectComparision;
					});
					$.ajax({
						url : $.fn.getRootPath() + '/app/search/object-search!saveObjectComparison.htm',
						type : 'POST',
						data : {
							objectComparision: JSON.stringify(objectComparisions),
							objectId: '${param.objId}'
						},
						success : function(data) {
							data = JSON.parse(data);
							if(data.success && data.success == true) {
								$.messager.alert('信息', '数据保存成功!', 'info', function(){
									var cmpare = parent.ArticleSearch.obj.cmpare;
									/*移除手动添加的数据*/
									var lis = parent.$('#cmpareArticle').find('li[saveinmanager!=1]');
									$(lis).each(function(i, o){
										cmpare.rmRow($(o).attr('objId'));
									});
						   			/*添加手动添加的数据*/
									$(objectComparisions).each(function(i, o){
										cmpare.addRow(o);
									});
									/*关闭弹窗*/
									parent.PageUtils.dialog.close('${param.dialogId}');
						   		});
							} else {
								$.messager.alert('信息', '数据保存失败!');
							}
							
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<div class="fkui_dan" style="width:810px;height: 370px;margin-left:5px;">
			<div class="shili_dan_con">
				<div class="shili_dan_left">
					实例选择				  
				</div>
				<div class="shili_dan_center">
					实例名：
					<input name="" type="text" id="srCon"/>
					<a href="javascript:void(0);"><input type="button" class="anniu1" value="查询" 
						onclick="PageUtils.datagrid.reload(1);"/>
					</a>
				</div>
				<div class="shili_dan_right">
					对比文章列表
				</div>
			</div>
			<div class="shili_dan_con1">
				<div class="shili_dan_left1">
					<div class="xinxi_con_nr_left">
						<ul id="catalogTree" class="ztree">
						</ul>
					</div>
				</div>
				<div class="shili_dan_center1">
					<div class="shili_dan_center1_top">
						<table class="easyui-datagrid" id="now-grid" 
							data-options="url:'${pageContext.request.contextPath }/app/comparison/business-contrast!expertSearch.htm',
				    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,pagination:true,
				    		loadFilter: function(data){
								if(data) {
									data.total = data.totlePageSize;
								} else {
									data = { total:0, rows:[] };
								}
								return data;
							},
							onSelect: function(rowIndex, rowData){
								CmpareUL.addLi(rowData.objectId, rowData.objectName);
							}">
						    <thead>
						        <tr>
						        	<th data-options="field:'objectId',checkbox:true"></th>
						        	 <th data-options="field:'objectName',width:100">实例名</th>   
						            <th data-options="field:'startTime',width:100">开始时间</th>   
						            <th data-options="field:'endTime',width:100">结束时间</th>
						            <th data-options="field:'editTime',width:100">修改时间</th>
						        </tr>   
						    </thead>
						</table>
					</div>
				</div>
				<div class="shili_dan_right1" style="width:187px;">
					<div class="shili_dan_right1_top" style="width:187px;">
						<div class="shili_dan_right1_con" style="width:187px;">
							<ul style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
								<s:iterator value="#request.cmpareOnto">
									<s:if test="saveInManager == 1">
										<li objId="${compareObjectId }">
											<p style="color:red;">${compareObjectName}</p>
										</li>
									</s:if>
								</s:iterator>
							</ul>
							<ul id="compareSortable">
								<s:iterator value="#request.cmpareOnto">
									<s:if test="saveInManager != 1">
										<li objId="${compareObjectId }">
											<p>${compareObjectName}</p>
											<img src="${pageContext.request.contextPath}/theme/green/resource/search/images/xinxi_ico5.jpg">
										</li>
									</s:if>
								</s:iterator>
							</ul>
						</div>
						<div class="shili_dan_right1_an">
							<a href="javascript:void(0);" class="duibi" id="sureBtn">保存</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
