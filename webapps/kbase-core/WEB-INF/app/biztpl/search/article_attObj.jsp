<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>业务模板搜索-原文</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	</head>
	<body class="easyui-layout">
	    <div data-options="region:'center'">
	    	<ul class="easyui-tree" id="att-article" 
	    		data-options="url:'${pageContext.request.contextPath }/app/search/attachment!getNode.htm?objId=${param.objId }',
    			loadFilter:function(data, parent){
	   				if(data && data.length > 0){
	   					$(data).each(function(i, o){
	   						o.text = o.name;
	   						if(o.isParent == 'false')
	   							o.state = 'open'
	   						else o.state = 'closed'
		   				});
	   				} else {
	   					$.messager.alert('信息','无数据!');
	   				}
	   				return data;
	   			},
    			onClick: function(node){
    				if(node.isParent == 'false') {
    					window.open('${rsPath }attachmentDown?attachmentId=' + node.id);
    				}
				}"></ul> 
	    </div>
	</body>
</html>
