<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>文章管理—二维表格展示</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/biztpl/2d/tab2.css" />
		<style type="text/css">
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/2d/jquery-scroll.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/2d/divscroll.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/search/js/SearchJump.js"></script>
		<script type="text/javascript">
			var pageContext = {
				contextPath: '${pageContext.request.contextPath }',
				converterPath: '${requestScope.converterPath}',
				robotContextPath: '${robotContextPath}',
				enableCateTag: '${requestScope.enableCateTag}'=='true',
				location: '${requestScope.location}',
				brand: '${requestScope.brand}',
				
				objId: '${article.objectId}', /*实例id(与文章id共用)*/
				objName: '${article.objectName}', /*实例名称*/
				cateId: '${article.inCategoryId}', /*分类id*/
				cateName: '${article.inCategoryName}', /*分类名称*/
				cateBh: '${article.inBh}', /*分类路径*/
				cateBhName: '${article.inBhName}', /*分类路径（中文）*/
				spotId: '${spotId}', /*锚点id*/
				converterPath: '${requestScope.converterPath}',/*附件转换地址*/
				tplId: '${tplId}', /*文章模板id*/
				
				//打开页面
				open: function(opts){
					var param = {width: '500px', height: '400px'};
					param = $.extend(param, opts);
					parent.__kbs_layer_index = parent.$.layer({
						type: 2,
						border: [5, 0.3, '#000'],
						title: false,
						iframe: {src : param.url},
						area : [param.width, param.height]
					});
				},
				
				//预览
				preview: function(fileName){
					var fileType = fileName.right(".");
					var fileId = fileName.replace(".p4","");
					
					var url = "";
					if(fileType && fileType == "p4"){
						url = pageContext.contextPath + '/app/wukong/search!p4.htm?id=' + fileId;
					}else{
						url = pageContext.converterPath + '/attachment/preview.do?fileName=' + fileName;
					}
					
					/**
					 * 预览修改成window.open modify by eko.zhan at 2016-06-07 14:20
					 */
					window.open(url);
					return false;
					parent.$.layer({
						type: 2,
						title: false,
						shade: [0.3, '#000'],
						border: [0],
						area: ['1000px', '500px'],
						iframe: {src: url}
					});
				}, 
				
				//下载附件
				download: function(fileid, filename){
					try{ 
			            var elemIF = document.createElement("iframe");
			            elemIF.src = pageContext.contextPath + '/app/wukong/search!download.htm?fileId=' + fileid + '&fileName=' + filename;   
			            elemIF.style.display = "none";
			            document.body.appendChild(elemIF);   
			        }catch(e){ 
			        	alert('文件异常，下载失败' + e);
			        }
				}
			}
		</script>
		<script type="text/javascript">
			$(function(){
				$(window).bind('load resize scroll',function(){
					$('.tab_head').find('.viewport').css('width',($('.tab_head').width()-20)+'px')
					var item = $('.tabnav').height();
				    var win =  $(window).height()-$('.tabnav').offset().top;
			    	if(item > win){
				      $('.tabnav').css({
				      	'height':win+'px',
				      	'overflow-y':'hidden'
				      })
				    }else{
			    		$('.tabnav').removeAttr('style');
				    }
				})
				
				$(".tab_con").first().show().siblings('.tab_con').hide(); 
				$(".w_table").first().show().siblings('.w_table').hide();
				$(".tabnav,.tab_head .overview").children("li").each(function(){
					var text = $(this).text();
					if(text.length > 9){
						text = text.substring(0,9) + '...';
						$(this).text(text);
					}
				});
				$(window).bind('load resize',function(){
					$('.w_con').click(function(e){
						e.stopPropagation();
						if($(e.target).is('.tab_head ul li')){
							var target = $(e.target);
							var _index = target.index();
							$('.tabnav li').removeClass("on"); 
							target.addClass("on").siblings().removeClass("on"); 
							$(".tab_con").eq(_index).show().siblings('.tab_con').hide();
							$(".tab_con").eq(_index).find('.w_table').css('background','#fff');
							$(".tab_con").eq(_index).find('.w_table,.w_table_con').first().show().siblings('.w_table').hide();
							var w_table_con_h = target.parents('.w_con').find('.tabnav').eq(_index).height();
							if(target.parents('.w_con').find('.tabnav > li').length !=0){
								$('.w_table_con').css({
									height : w_table_con_h + 'px',
									lineHeight: w_table_con_h +'px'
								});
							}
							var tabnav_ = target.parents('.w_con').find('.tabnav').eq(_index);
							$(window).bind('resize load scroll',function(){
								var item = target.parents('.w_con').find('.tabnav').eq(_index).children('li').length*37;
		    					var win =  $(window).height()-$('.tabnav').offset().top;
								if(item > win){
							      tabnav_.css({
							      	'height':win+'px',
							      	'overflow-y':'hidden'
							      })
							      $('div[id^=ascrail]').css('display','block')
							    }else{
						    		tabnav_.removeAttr('style');
						    		$('div[id^=ascrail]').css('display','none')
							    }
							})
						}else if($(e.target).is('.tabnav li')){
							var target = $(e.target);
							var _ind = target.index();
							target.addClass("on").siblings().removeClass("on"); 
							target.parents('.tab_con').find('.w_table').css('background','#F7F8FA');
							target.parents('.tab_con').find('.w_table').eq(_ind+1).show().siblings('.w_table,.w_table_con').hide();
							$('.tab_head').find('.viewport').css('width',($('.tab_head').width()-20)+'px');
						}
					});	
				});
				
				// $('#slider').jqueryScroll();
				$('.tab_head').jqueryScroll();
				$('.ul-wzsx').perfectScrollbar();
				$('.tabnav').perfectScrollbar();
				
				//本页面业务代码由此开始 eko.zhan at 2016-10-20
				//签读
				$('.kbs-btn-markread').click(function(){
					var _this = this;
					var _url = $.fn.getRootPath() + '/app/search/search!markreadObject.htm';
					var _params = {
						'valueId': $(_this).attr('_id'),
						'objectId': pageContext.objId,
						'categoryId': pageContext.cateId,
						'catePath': pageContext.cateBh
					}
					$.post(_url, _params, function(data){
						if (data==1){
							alert('知识签读成功');
						}else{
							alert('知识已签读');
						}
					}, 'text');
				});
				
				//其它平台
				$('.kbs-btn-platform').click(function(){
					var url = pageContext.contextPath + '/app/category/all-ans!view.htm?valueId=' + $(this).attr('_id');
					window._open(url, '其它平台', '700', '500');
				});
				
				//纠错
				$('.kbs-btn-errcorrect').click(function(){
					var _url = $.fn.getRootPath() + '/app/corrections/corrections!send.htm?objId=' + pageContext.objId + '&faqId=' + $(this).attr('_id') + '&cateId=' + pageContext.cateId;
					window._open(_url, '知识反馈', 840, 500);
				});
				//收藏
				$('.kbs-btn-fav').click(function(){
					var _url = $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?faqid=' + $(this).attr('_id') + '&cateid=' + pageContext.cateId;
					window._open(_url, '知识收藏', 840, 500);
				});
				//推荐
				$('.kbs-btn-share').click(function(){
					var _url = $.fn.getRootPath() + '/app/recommend/recommend!open.htm?valueid=' + $(this).attr('_id') + '&cateid=' + pageContext.cateId;
					window._open(_url, '知识推荐', 840, 500);
				});
				
				
				//参考文档
				$('#btnDoc').click(function(){
					var url = "${pageContext.request.contextPath}/app/wukong/search!docList.htm?objId=" + pageContext.objId;
					pageContext.open({url: url});
				});
				//收藏文章
				$('#btnFavArticle').click(function(){
					var url = '${pageContext.request.contextPath}/app/fav/fav-clip-object!pick.htm?objid=' + pageContext.objId + '&cataid=' + pageContext.cateId;
					pageContext.open({url: url});
				});
				//编辑文章
				$('#btnEditArticle').click(function(){
					var url = '${pageContext.request.contextPath}/app/biztpl/article!index.htm?articleId=' + pageContext.objId;
					window.open(url);
				});
				//最大化
				$('#btnMax').click(function(){
					var url = location.href;
					window._open(url, '二维展示-' + pageContext.objName, screen.width, screen.height);
				});
				//文章展示
				$('#btnReadArticle').click(function(){
					var url = '${pageContext.request.contextPath}/app/biztpl/article-search.htm?&articleId=' + pageContext.objId;
					location.href = url;
				});
				
				//页面加载时执行
				//文章关联
				$.post(pageContext.contextPath + '/app/biztpl/article!getObjectRelationOn.htm', {objId: pageContext.objId, 'pageNo':1, tplId: pageContext.tplId}, function(result){
					if (result && result.success){
						var data = result.data;
						$(data).each(function(i, o){
							if(o.name != pageContext.objName) {/*不是当前实例*/
								$('#relationArticlePanel').append('<li><a href="javascript:void(0)" onclick="readArticle(\'' + o.object_id + '\', \'' + o.name + '\');" title="' + o.name + '">' + o.name + '</a></li>');
							}
						});
					}
				}, 'json');
				
				$('.tab-share').hide();
				$('.w_table').find('table tbody tr').each(function(i, tr){
					$(tr).find('td:last').hover(function(){
						$(this).find('.tab-share').show();
					}, function(){
						$(this).find('.tab-share').hide();
					});
				});
				
				$('[_preview="1"]').click(function(){
					var _fileid = $(this).attr('_fileid');
					var _filename = $(this).text();
					var _filetype = '';
					if (_filename.indexOf('.')!=-1){
						_filetype = _filename.substring(_filename.lastIndexOf('.'));
					}
					pageContext.preview(_fileid + _filetype);
				});
				
				$('.file-name').click(function(){
					var _fileid = $(this).attr('_fileid');
					var _filename = $(this).text();
					var _filetype = '';
					if (_filename.indexOf('.')!=-1){
						_filetype = _filename.substring(_filename.lastIndexOf('.'));
					}
					pageContext.preview(_fileid + _filetype);
				});
				
				$('.file-load').click(function(){
					var $file = $(this).parent().find('.file-name');
					var _fileid = $file.attr('_fileid');
					var _filename = $file.text();
					pageContext.download(_fileid, _filename);
				});
				
				$(window).bind("scroll", function(){
					var height=document.documentElement.scrollHeight - document.documentElement.clientHeight;
					if($(window).scrollTop()>100 && height>170){
						$("#backTop").click(function(){
						  $(window).scrollTop("");
						});
						$("#backTop").show();
						
					}else if($(window).scrollTop()<100){
						$("#backTop").hide();
					}
				}); 
				
			}) //end document load end
		
			/**
			 * 访问关联文章
			 */
			function readArticle(id, name){
				var url = pageContext.contextPath + '/app/search/object-search.htm?searchMode=8&objId=' + id;
				if(parent.TABOBJECT) {
					parent.TABOBJECT.open({
						id : id,
						name : name,
						hasClose : true,
						url : url,
						isRefresh : true
					}, this);
				} else {
					window._open(url, id, screen.width, screen.height);
				}
			}
		</script>
	</head>
	
	<body>
		<div class="tab_top">
			<a href="javascript:void(0);" class="l-btn">
				<span class="l-btn-left l-btn-icon-left" id="btnDoc">
					<span class="l-btn-text">参考文档</span>
					<span class="l-btn-icon icon-ckwd">&nbsp;</span>
				</span>
			</a>				
			<a href="javascript:void(0);" class="l-btn">
				<span class="l-btn-left l-btn-icon-left" id="btnFavArticle">
					<span class="l-btn-text">收藏</span>
					<span class="l-btn-icon icon-sc1">&nbsp;</span>
				</span>
			</a>
			<!-- 
	    	<a href="javascript:void(0);" class="l-btn">
		    	<span class="l-btn-left l-btn-icon-left" id="btnEditArticle">
			    	<span class="l-btn-text">编辑</span>
			    	<span class="l-btn-icon icon-bj">&nbsp;</span>
		    	</span>
	    	</a> -->
	    	<a href="javascript:void(0);" class="l-btn">
		    	<span class="l-btn-left l-btn-icon-left" id="btnMax">
			    	<span class="l-btn-text">最大化</span>
			    	<span class="l-btn-icon icon-bb">&nbsp;</span>
		    	</span>
	    	</a>
	    	<a href="javascript:void(0);" class="l-btn">
		    	<span class="l-btn-left l-btn-icon-left" id="btnReadArticle">
			    	<span class="l-btn-text">文章展示</span>
			    	<span class="l-btn-icon icon-view">&nbsp;</span>
		    	</span>
	    	</a>
		</div>
		
		<div class="tab_position">
			<span class="tab_pos_title">${article.inBhName } &gt; <em>${article.objectName }</em> </span>
			<span class="tab_pos_time"><%-- 更新时间&nbsp;${article.modifyDate } 暂时屏蔽 --%></span>
		</div>
	
		<div class="w_box">
			<div class="right-sidebar">
				<div class="wzgl">
					<h2 class="title-wzgl">文章关联</h2>
					<ul class="ul-wzgl" id="relationArticlePanel">
						
					</ul>
				</div>
				
				<!-- 文章属性开始 -->
				<c:if test="${fn:length(propertyJa) > 0 }">
					<div class="wzsx">
						<h2 class="title-wzsx">文章属性</h2>
						<ul class="ul-wzsx">
							<c:forEach items="${propertyJa }" var="property">
								<c:if test="${property.type == 'single' }">
									<li><span>${property.propertyName }:</span>${property.propertyValue }</li>
								</c:if>
								<c:if test="${property.type == 'multi' }">
									<li><span>${property.propertyName }:</span>${property.propertyValue }</li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</c:if>
				<!-- 文章属性结束 -->
				
				<!-- 标签开始 -->
				<c:if test="${fn:length(labelJa) > 0 }">
					<div class="wztag">
						<h2 class="title-wztag">标签</h2>
						<div id="main_demo">
							<div id="slider"> 
								<a class="buttons prev" href="javascript:void(0)">left</a>
								<div class="viewport">
									<ul class="overview">
										<c:forEach items="${labelJa }" var="label" varStatus="status">
											<c:if test="${status.index%6==0 }">
												<li><div class="view-span">
											</c:if>
											<span>${label }</span>
											<c:if test="${status.index%6==5 }">
												<div class="view-span"></li>
											</c:if>
										</c:forEach>
									</ul>
								</div>
								<a class="buttons next" href="javascript:void(0)">right</a> 
							</div>
						</div>
					</div>
				</c:if>
				<!-- 标签结束 -->
				
			</div> <!-- end right-sidebar -->
			
			<div class="w_con">
				<div class="tab_head">
					<a class="buttons prev disable" href="javascript:void(0)" ></a>
					<div class="viewport">
						<ul class="overview">
						  	<s:iterator value="#request.groups['root']" var="level0" status="status0">
						  		<s:if test="#status0.index==0"><li class="on">${level0.name }</li></s:if>
						  		<s:else><li>${level0.name }</li></s:else>
						  	</s:iterator>
						</ul> 
					</div>
					<a class="buttons next" href="javascript:void(0)"></a>
				</div> <!-- end tab_head -->
				
				
				<s:iterator value="#request.groups['root']" var="level0" status="status0">
					<div class="tab_con">
						<ul class="tabnav">
							<s:iterator value="#request.groups[#level0.groupId]" var="level1" status="status1">
								<li>${level1.name }</li>
							</s:iterator>
						</ul>
						
						<s:set var="tmpQaList" value="#request.quess[#level0.groupId]"></s:set>
						<s:if test="(#level0.groupAbs!=null && #level0.groupAbs!='') || (#tmpQaList!=null && #tmpQaList.size>0)">
							<div class="w_table">
								<table>
									<thead>
										<tr>
											<th colspan="2">
												<s:if test="#level0.groupAbs!=null && #level0.groupAbs!=''">
													<div class="w_about">介绍</div>
													<div class="w_about_con">
														${level0.groupAbs }
													</div>
												</s:if>
												<s:if test="#tmpQaList!=null && #tmpQaList.size>0">
													<div class="w_about">相关问题</div>
												</s:if>
											</th>
										</tr>
									</thead>
									<s:iterator value="#tmpQaList" var="qa">
										<tr>
											<td class="td_title"><s:property value="#rowLen"/>${qa.question }</td>
											<td>
												<s:iterator value="#qa.answers" var="ans">
													${ans.answer }
												</s:iterator>
												<div class="tab-share">
													<ul>
														<!-- 
														<li class="c-icona">
															<a href="javascript:void(0);" class="kbs-btn-markread" _id="${qa.id }"><span></span>签读</a>
														</li>
														-->
														<li class="c-icona">
															<a href="javascript:void(0);" class="kbs-btn-platform" _id="${qa.id }"><span></span>其它平台</a>
														</li>
														<li class="c-iconb">
															<a href="javascript:void(0);" class="kbs-btn-errcorrect" _id="${qa.id }"><span></span>评论</a>
														</li>
														<li class="c-iconc">
															<a href="javascript:void(0);" class="kbs-btn-fav" _id="${qa.id }"><span></span>收藏</a>
														</li>
													</ul>
												</div>
											</td>
										</tr>
									</s:iterator>
								</table> <!-- end table -->
							</div> <!-- end div.w_table -->
						</s:if>
						<s:else>
							<div class="w_table">
								<table>
									<thead>
										<tr>
											<td>
												<div class="w_table_con">该分类下面没有内容，请查看相关二级内容</div>
											</td>
										</tr>
									</thead>
								</table>
							</div>
						</s:else>
						
						<s:iterator value="#request.groups[#level0.groupId]" var="level1" status="status1">
							<s:set var="tmpQaList" value="#request.quess[#level1.groupId]"></s:set>
							<s:if test="(#level1.groupAbs!=null && #level1.groupAbs!='') || (#tmpQaList!=null && #tmpQaList.size>0)">
								<div class="w_table">
									<table>
										<thead>
											<tr>
												<th colspan="2">
													<s:if test="#level1.groupAbs!=null && #level1.groupAbs!=''">
														<div class="w_about">介绍</div>
														<div class="w_about_con">
															${level1.groupAbs }
														</div>
													</s:if>
												
													<s:if test="#tmpQaList!=null && #tmpQaList.size>0">
														<div class="w_about">相关问题</div>
													</s:if>
												</th>
											</tr>
										</thead>
										<s:iterator value="#tmpQaList" var="qa">
											<%-- 一个问题对应多个答案的情况 --%>
											<s:set var="rowLen" value="#qa.answers.size"></s:set>
											<tr>
												<td class="td_title" rowspan="${rowLen }">${qa.question }</td>
												<td>
													${qa.answers[0].answer }
													<s:if test="#qa.answers[0].attachments != null && #qa.answers[0].attachments.size > 0">
														<div class="tab_fj">
															<ul>
																<s:iterator value="#qa.answers[0].attachments" var="att">
																	<li >
																		<s:if test="#att.value.indexOf('.xls')>-1 || #att.value.indexOf('.xlsx')>-1"><span class="file-xls"></span></s:if>
																		<s:elseif test="#att.value.indexOf('.doc')>-1 || #att.value.indexOf('.docx')>-1"><span class="file-doc"></span></s:elseif>
															            <s:else><span class="file-p4"></span></s:else>
															            <a class="file-name" _fileid="${att.key }">${att.value }</a>
															            <a class="file-load">下载</a>
														            </li>
																</s:iterator>
															</ul>
														</div>
													</s:if>
													<div class="tab-share">
														<ul>
															<!-- 
															<li class="c-icona">
																<a href="javascript:void(0);" class="kbs-btn-markread" _id="${qa.id }"><span></span>签读</a>
															</li>
															-->
															<li class="c-icona">
																<a href="javascript:void(0);" class="kbs-btn-platform" _id="${qa.id }"><span></span>其它平台</a>
															</li>
															<li class="c-iconb">
																<a href="javascript:void(0);" class="kbs-btn-errcorrect" _id="${qa.id }"><span></span>评论</a>
															</li>
															<li class="c-iconc">
																<a href="javascript:void(0);" class="kbs-btn-fav" _id="${qa.id }"><span></span>收藏</a>
															</li>
															<li class="c-icond">
																<a href="javascript:void(0);" class="kbs-btn-share" _id="${qa.id }"><span></span>分享</a>
															</li>
															
														</ul>
													</div>
												</td>
											</tr>
											<s:iterator value="#qa.answers" var="ans" begin="1">
												<tr>
													<td>
														${ans.answer }
														<s:if test="#ans.attachments != null && #ans.attachments.size > 0">
															<div class="tab_fj">
																<ul>
																	<s:iterator value="#ans.attachments" var="att">
																		<li >
																			<s:if test="#att.value.indexOf('.xls')>-1 || #att.value.indexOf('.xlsx')>-1"><span class="file-xls"></span></s:if>
																			<s:elseif test="#att.value.indexOf('.doc')>-1 || #att.value.indexOf('.docx')>-1"><span class="file-doc"></span></s:elseif>
																            <s:else><span class="file-p4"></span></s:else>
																            <a class="file-name" _fileid="${att.key }">${att.value }</a>
																            <a class="file-load">下载</a>
															            </li>
																	</s:iterator>
																</ul>
															</div>
														</s:if>
														<div class="tab-share">
															<ul>
																<!-- 
																<li class="c-icona">
																	<a href="javascript:void(0);" class="kbs-btn-markread" _id="${qa.id }"><span></span>签读</a>
																</li>
																-->
																<li class="c-icona">
																	<a href="javascript:void(0);" class="kbs-btn-platform" _id="${qa.id }"><span></span>其它平台</a>
																</li>
																<li class="c-iconb">
																	<a href="javascript:void(0);" class="kbs-btn-errcorrect" _id="${qa.id }"><span></span>评论</a>
																</li>
																<li class="c-iconc">
																	<a href="javascript:void(0);" class="kbs-btn-fav" _id="${qa.id }"><span></span>收藏</a>
																</li>
																<li class="c-icond">
																	<a href="javascript:void(0);" class="kbs-btn-share" _id="${qa.id }"><span></span>分享</a>
																</li>
															</ul>
														</div>
													</td>
												</tr>
											</s:iterator>
										</s:iterator>
									</table>
								</div> <!--  end div.w_table -->
							</s:if>
						</s:iterator>
					</div>
				</s:iterator>
				
	       </div> <!-- end w_con -->
		</div>
		
		<div id="backTop"></div>
	</body>
</html>
