<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>指令编辑</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/reset.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/zl.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<style>
			ul{	background: white ;
			    border: 1px solid #ccc;
			    height: 190px;
			    list-style: outside none none;
			    margin-left: 80px;
			    overflow-y: auto;
			    position: absolute;
			    width: 180px;
			    z-index: 9998;
				
			 } 
		</style>
		<script type="text/javascript">
			var editid = '${editid}';
			var cmds1 = '${cmds1}';
			var pageContext = {
				contextPath : '${pageContext.request.contextPath }'
			}
			$(function(){
				$("body").on('click','.add',function (e) {
					var list = $('div#list').eq(0).clone();
					var span = $('<div class="zhil"><span><label for="zl001" >指令:</label><em><i class="add"></i><i class="remove"></i><input type="text" id="zl001" readonly></em></span></div>');
			  		$(span).find('em').after(list);
			  		$('.button').before(span);
				})
				$("body").on("click",".remove", function(e){
					var div = $('div.zhil');
					if(div && div.length > 1){
						$(this).parents('div.zhil').remove();
					}else{
						$(this).parents('div.zhil').find('span').not(':first').remove();
						$(this).siblings('input').val('');
					}
				})
				
				$('body').on('click','i.url',function(){
					var urlid = 'p4_' + new Date().getTime();
					var p4id = $(this).next('input').val();
					$(this).attr('urlid', urlid)
					parent.openP4(p4id,urlid);
				})
				$('body').on('click','input#zl001',function(){
					$(this).parent().next('div').css('display','block');
				})
				$('body').on('click','ul li a',function(){
					$(this).parents('div.zhil').find('span').not(':first').remove();
					var txt = $(this).text();
					var _type = $(this).attr('rel');
					$(this).parents('div#list').prev('em').children('input').val(txt);
					$(this).parents('div#list').prev('em').children('input').attr('_type',_type);
					if(_type == 'p4'){
						var p = $('<span><label for="zl002" >URL地址:</label><em><i class="url"></i><input type="text" id="zl002"></em></span><span><label for="zl003" >P4标题:</label><em><input type="text" id="zl003" ></em></span>');
					}else{
						var  p = $('<span><label for="zl004" >指定相关问:</label><em><input type="text" id="zl004"></em></span>');
					}
					$(this).parents('span').after(p);
					$(this).parents('div#list').css('display','none');
				})
				
				$('#ok').click(function(){
					var cmds = '';
					var div = $('div.zhil');
					if(div){
						div.each(function(i, item){
							var cmd = '';
							var code = $(item).find('input#zl001').attr('_type');
							var param1 = $(item).find('input#zl002').val();
							var param2 = $(item).find('input#zl003').val();
							if(code && code.length > 0){
								cmd = code + '(' + param1 + ',' + param2 + ');';
								cmds += cmd;
							}
						})
						
					}
					cmds += cmds1;
					parent.setCmds(editid,cmds);
					parent.closeZhiling();
				})
				$('#cancel').click(function(){
					parent.closeZhiling();
				})
			})
			function setP4Id(p4Id,urlid){
				$('i[urlid="'+urlid+'"]').next('input').val(p4Id);				
			}
		</script>
</head>
<body>
	<div class="content">
		<c:if test="${empty cmds}">
			<div class="zhil">
				<span><label for="zl001" >指令:</label>
					<em><i class="add"></i><i class="remove"></i>
						<input type="text" id="zl001" value="" _type="" readonly>
					</em>
					<div id="list" style="display:none;overflow-y: auto;height: auto;">
						<ul>
							<c:forEach items="${ins}" var="in">
								<li><a href="javascript:void(0);" rel="${in.code }">${in.name }</a></li>
							</c:forEach>
						</ul> 
					</div>
				</span>
			</div>
		</c:if>
		<c:forEach items="${cmds}" var="cmd">
			<div class="zhil">
				<span><label for="zl001" >指令:</label>
					<em><i class="add"></i><i class="remove"></i>
						<input type="text" id="zl001" value="打开p4" _type="${cmd.code }" readonly>
					</em>
					<div id="list" style="display:none;overflow-y: auto;height: auto;">
						<ul>
							<c:forEach items="${ins}" var="in">
								<li><a href="javascript:void(0);" rel="${in.code }">${in.name }</a></li>
							</c:forEach>
						</ul> 
					</div>
				</span>
				<span><label for="zl002" >URL标题</label><em><i class="url"></i><input type="text" id="zl002" value="${cmd.params[0] }"></em></span>
				<span><label for="zl003" >P4标题:</label><em><input type="text" id="zl003" value="${cmd.params[1] }"></em></span>
			</div>
		</c:forEach>
	
		<p class="button">
			<span id="ok">确定</span>
			<span id="cancel">取消</span>
		</p>
	</div>
</body>
</html>