<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.domain.biztpl.BizTplGroup"%>
<%@page import="com.eastrobot.module.biztpl.service.BizTplQuestionService"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>编辑文章</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css01.css?t=2">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			input[type="text"]{
				border: 1px solid #cdcdcd;
				height: 30px;
				line-height: 30px;
				border-radius: 4px;
				margin: 4px 0;
				width: 72%;
			}
			.btn{
				margin-bottom: 2px;
			}
			button{
				border: 1px solid #cdcdcd;
				margin: 2px;
				padding: 2px;
				cursor: pointer;
			}
			span.cke_button__embed_label{
				display: inline;
			}
			span.cke_button__embed_icon{
				width: 0px;
			}
			span.cke_button__ansattr_label{
				display: inline;
			}
			span.cke_button__ansattr_icon{
				width: 0px;
			}
			span.cke_button__ansdel_label{
				display: inline;
			}
			span.cke_button__ansdel_icon{
				width: 0px;
			}
			span.cke_button__anssave_label{
				display: inline;
				font-weight: bold;
			}
			span.cke_button__anssave_icon{
				width: 0px;
			}
			.textarea_div{
				background-color: #f6f6fe;
			    border: 1px solid #cdcdcd;
			    border-radius: 4px;
			    color: #555;
			    display: inline-block;
			    line-height: 24px;
			    margin: 8px 0;
			    min-height: 30px;
			    padding: 2px 8px;
			    width: 82%;
			    word-break: break-all;
			}
			.img_width{
				width: 30px;
			}
			.img_style{
				cursor: pointer;
				vertical-align: top;
				width: 30px;
				float: right;
			}
			
			.move{
				width:385px;
				height:300px;
				/* border:1px solid #ccc; */
				padding:0 5px;
				text-indent:12px;
				line-height:22px;
				background: white;
				position:absolute;
				z-index:1;
				overflow: auto;
			}
			.move h3{
				height:40px;
				line-height:50px;
				font-size:14px;
				color:#444;
		
			}
			.move h3 i.close{
				line-height:12px;
				background:url(images/big-close.png) no-repeat center center;
				float:right;
				width:24px;
				height:24px;
				background-size:12px 12px;
				cursor:pointer;
			}
			.move dl{
				margin-bottom:10px;
			}
			.move dl dt{
				font-size:14px;
			}
			.move dl dd{
				text-indent:2em;
				font-size:13px;
			}
			.move dl dd p{
				text-indent:40px;
				font-size:11px;
				width:98%;
				height:16px;
				line-height:16px;
				color:#888;
				overflow: hidden;
			}
			
			.title-checked{
				background-color: #ffe6b0;
			    border: 1px solid #ffb951;
			    color: black;
			    height: 16px;
			    opacity: 0.8;
			    padding-top: 0;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-embed.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansattr.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansdel.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-anssave.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/CheckInput.js"></script>
		<script type="text/javascript">
			var pageContext = {
				contextPath : '${pageContext.request.contextPath }'
			}
			
			var localConfig = {
				customConfig: '${pageContext.request.contextPath }/library/ckeditor/config-cell.js'
			}
			var map = {};
			var deleteMap = [];
			var questionMap = [];
			var answerMap = [];
			var flag = true;
		</script>
		
		<script type="text/javascript">
			function _getWindowParams(opts){
				var height = (opts && opts.height) || 300;
				var width = (opts && opts.width) || 500;
				
				var _scrHeight = screen.height;
				var _scrWidth = screen.width;
				var top = (_scrHeight - height)/2;
				var left = (_scrWidth - width)/2;
				
				return 'height=' + height + ', width=' + width + ', top=' + top + ', left=' + left + ',location=0, menubar=0, resizable=0, status=0, toolbar=0';
				
			}
			
			//所有ckeditor转为div模式
			function editorToDiv(){
				for (var id in map){
					var _this = $("div#cke_"+id);//富文本对象
					var _div = $("div[_name='"+id+"']");//文本div对象
					//判断是否已保存过：未保存过的会生成对应div
					if(_div.length == 0){
						var _divhtml = "<div _name='"+id+"' class='textarea_div'>"+map[id].getData()+"</div>"; 
						_this.parent().append(_divhtml);
						$("div[_name='"+id+"']").on("dblclick",function(){
							editorToDiv();
							$("div#cke_"+id).show();
							$(this).hide();
						});
					}else{
						_div.text("");//清空数据
						_div.append(map[id].getData())
					}
					_div.show();
					_this.hide();
				}
			}
			
			$(function(){
				$('body').on('dblclick', '[type="readContent"]', function(){
						var id = $(this).attr('aid');
						var type = $(this).attr('_type');
						editorToDiv();
						var _id =''
						if(type=='Answer'){
							_id = 'Answer'+id
						}else if(type=='abs'){
							_id = 'group'+id
						}
						if(!CKEDITOR.instances[_id]){
							var _editor = CkEditImage.create.replace(_id);
							map[_id] = _editor;
						}else{
							$("div#cke_"+_id).show();
						}
						$(this).hide();
				});
			
				//新增标准问
				$('.kbs-new-qa').click(function(){
					var _this = this; 
					
					$(_this).next('button').click();
				});
				//新增标准问
				$('.kbs-new-qa').next('button').click(function(){
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					//增加ckeditor之前将其他所有ckeditor转为div模式
					editorToDiv();
				
					var _this = this; 
					var _targetPanel = $(_this).attr('_targetPanel');
					
					var _table = $('table[_panel="' + _targetPanel + '"]');
					$(_table).parent('td').css('display','');
					if (_table.length==1){
						var _id = new Date().getTime();//Math.random();
						_id = 'Draft' + _id;
					
						var _textarea = document.createElement('textarea');
						_textarea.id = _id;
						$(_textarea).attr('tid',_id);
						//_dummyID 虚拟ID，新增的标准问没有attrID，通过_dummyID和答案关联
						_table.append('<tr _id="' + _id + '" _name="kbs-tr-question"><td><span class="ico-ask ico-text">问</span><input style="margin-right:4px;" type="text" _attrid="" _dummyID='+_id+' _name="kbs-question" /><font _name="attrAsterisk" style="color: red;font-size: 16px;">*</font><img _id="'+_id+'" _name="btnNewAnswer" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_add.png" title="新增答案" class="img_style"><img _id="'+_id+'" _name="btnRemove" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_remove.png" title="删除" class="img_style" style="margin-right:10px;"></td></tr>');
						var _tr = document.createElement('tr');
						var _td = document.createElement('td');
						var imgHtml = '<span _answer = "'+_id +'" class="ico-answer ico-text">答</span>';
						$(_td).append(imgHtml);
						$(_td).append(_textarea);
						$(_tr).attr('_id', _id).attr('_dimtags', $(_this).attr('_dimtags')).append(_td);
						_table.append(_tr);
						
						var _editor = CkEditImage.create.replace(_textarea);
						map[_id] = _editor;
					}
				});
				
				//tr hover
				$('input[_name="kbs-question"]').parent('td').parent('tr').hover(function(){
					var _btns = $(this).find('input[_name="kbs-question"]').siblings('button[_ishidden!="1"]');
					$(_btns).show();
				}, function(){
					var _btns = $(this).find('input[_name="kbs-question"]').siblings('button');
					$(_btns).hide();
				});
				
				//删除
				$('body').on('click', '[_name="btnRemove"]', function(){
					var qid = $(this).attr('_qid');
					if (window.confirm('确定删除吗')){
						if(qid){
							questionMap.push(qid);
						}else{
							qid = $(this).attr('_id');
						}
						
						$($('tr[_id="' + qid + '"]').get(1)).find('textarea').each(function(i, item){
								delete map[item.id];
						})
						
						$('tr[_id="' + $(this).attr('_id') + '"]').remove();
						delete map[$(this).attr('_id')];
					}
				})
				//编辑
				$('body').on('click', '[_name="btnEdit"]', function(){
					var _id = $(this).attr('_id');
					$($('tr[_id="' + _id + '"]').get(1)).find('textarea').each(function(){
						var aid = $(this).attr('aid');
						var tid = $(this).attr('tid');
						if (!CKEDITOR.instances[tid]){
							$('div[id="AnswerRead' + aid + '"]').hide();
							$('textarea[id="Answer' + aid + '"]').show();
							var _editor = CkEditImage.create.replace('Answer' + tid);
							map[tid] = _editor;
						}
					})
					$(this).attr('_ishidden', '1').hide();
					$(this).next('button[_name="btnCancelEdit"]').attr('_ishidden', '0').show();
					
				})
				//取消编辑
				$('body').on('click', 'button[_name="btnCancelEdit"]', function(){
					var _id = $(this).attr('_id');
					
					if (window.confirm('确定取消编辑吗')){
						$($('tr[_id="' + _id + '"]').get(1)).find('textarea').each(function(){
							var aid = $(this).attr('aid');
							var tid = $(this).attr('tid');
							if (CKEDITOR.instances['Answer' + tid]){
								CKEDITOR.instances['Answer' + tid].destroy();
								$('div[id="AnswerRead' + aid + '"]').show();
								$('textarea[id="Answer' + aid + '"]').hide();
							}
						});
						$(this).attr('_ishidden', '1').hide();
						$(this).prev('button[_name="btnEdit"]').attr('_ishidden', '0').show();
					}
				})
				//新增介绍
				$('img[_name="abstract"]').click(function(){
					
					var _this = this;
					var _id = $(_this).attr('_id');
					var _divId = 'group' + _id
					var _div=$('div[_name='+_divId+']');
					
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					
					if(!CKEDITOR.instances[_id] && _div.length == 0){
						
						//增加ckeditor之前将其他所有ckeditor转为div模式
						editorToDiv();
						
						var _newId = new Date().getTime();//Math.random();
						_newId = 'Draft' + _newId
					
						var _textarea = document.createElement('textarea');
						_textarea.id = 'group'+_id;
						
						if($($('tr[_id="' + _id + '"]').get(0)).find("td").length == 0){
							$($('tr[_id="' + _id + '"]').get(0)).append("<td></td>");
						}
						$($('tr[_id="' + _id + '"]').get(0)).find('td').append(_textarea);
						
						//var _editor = CKEDITOR.replace(_textarea, localConfig);
						var _editor = CkEditImage.create.replace(_textarea);
						map['group'+_id] = _editor;
					}else{
						if($(_this).attr('_abstractType') == 'abstract'){
							alert("每篇文章只能创建一个摘要!");
						}else{
							alert("每个标题只能创建一个介绍!");
						}
						return false;
					}
					
				});
				//新增答案
				$('body').on('click', '[_name="btnNewAnswer"]', function(){
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					
					//增加ckeditor之前将其他所有ckeditor转为div模式
					editorToDiv();
				
					var _this = this;
					var _id = $(_this).attr('_id');
					
					var _newId = new Date().getTime();//Math.random();
					_newId = 'Draft' + _newId
				
					var _textarea = document.createElement('textarea');
					_textarea.id = _newId;
					$(_textarea).attr('tid',_newId);
					
					var imgHtml = '<span _answer = "'+_newId +'" class="ico-answer ico-text">答</span>';
					$($('tr[_id="' + _id + '"]').get(1)).find('td').append(imgHtml);
					$($('tr[_id="' + _id + '"]').get(1)).find('td').append(_textarea);
					var _editor = CkEditImage.create.replace(_textarea);
					map[_newId] = _editor;
				})
				
				
				//获取业务模版的结构
				function getGroup(group){
					var  _json = [];
					group.each(function(i,item){
						var json = {'id':'','name':'','attrId':'','groupId':'','parentId':'','level':'','priority':'','tplId':'','abs':'','children':''};
						var id = $(item).attr('id');;
						json.id =  id;
						json.name =  $(item).attr('_name');
						json.attrId = $(item).attr('attrId');
						json.parentId = $(item).attr('parentId');
						json.priority = $(item).attr('priority');
						json.groupId = $(item).attr('groupId');
						json.tplId = $('input[name="tplId"]').val();
						json.level = $(item).attr('level');
						if(!$(item).attr('attrId')){
							var div =  $('#group'+id);
							if(CKEDITOR.instances['group'+id]){
								json.abs = map['group'+id].getData();
							}else if(div && div.length > 0){
								json.abs = $('#group'+id).val();
							}			
						}
						var chil = getGroup($('a[parentId="'+$(item).attr('groupId')+'"]'));
						json.children = chil;
						_json.push(json);
					})
					return _json;
				}
				
				//文章入库
				$('#btnStore').click(function(){
					var article = getData();
					
					$.post(pageContext.contextPath + '/app/biztpl/article!save.htm?op=store', {data: JSON.stringify(article)}, function(data){
						var result = data.data;
						var code = result.code;
						if(code == '0'){
							alert('入库成功');
						}else{
							alert(result.reason);
						}
					}, 'json');
				});
				//文章保存
				$('#btnSave').click(function(){
					var article = getData();
					if(flag){
						flag = _checkDataBatch();
					}
					if(flag){
						$('#btnSave').attr('disabled',true); 
						var group = $('a[parentId=""]');
						var groupList = getGroup(group);
						var deleteData = {};
						$.extend(deleteData, {
							question: questionMap,
							answer:answerMap
						})
						
						var property  = $('#textarea').text();
						console.log(property);
						
						$.post(pageContext.contextPath + '/app/biztpl/article!save.htm?op=save', {data: JSON.stringify(article),deleteData: JSON.stringify(deleteData),groupData:JSON.stringify(groupList),property:property}, function(data){
							var result = data.data;
							var code = result.code;
							if(code == '0'){
								alert('保存成功');
								location.href = pageContext.contextPath + '/app/biztpl/article!edit.htm?id=' + result.id;
							}else{
								alert(result.reason);
							}
							$('#btnSave').attr('disabled',false);
						}, 'json');
					}
					flag = true;
					
				});
				
				function getData(){
					var absId = $('button[name="abstract"]').attr('_id');
					var abstract = '';
					var articleId= '${bizTplArticle.id }';
					abstract = $('div[_name="group'+articleId+'"]').html()
					var name = $('input[name="articleName"]').val();
					var semantic = $('input[name="semantic"]').val();
					if(name == ''){
						alert("文章名称不能为空!");
						flag = false;
						$('input[name="articleName"]').focus();
						return false;
					}
					if(semantic == ''){
						alert("语义块不能为空!");
						$('input[name="semantic"]').focus();
						flag = false;
						return false;
					}
					
					var $textarea = $('textarea[id="group' + articleId + '"]')
					var _attids = $textarea.attr('_attidsNew');	//附件ids
					var _attNames = $textarea.attr('_attNamesNew');	//附件names
					
					var article = {
						id: $('input[name="id"]').val(),
						tplId: $('input[name="tplId"]').val(),
						name: name,
						label: $('input[name="label"]').attr('nodeIds'),
						traceDate: $('input[name="traceDate"]').val(),
						startDate: $('input[name="startDate"]').val(),
						endDate: $('input[name="endDate"]').val(),
						categoryName: $('#categoryId').val(),
						categoryId: $('#categoryId').attr('_id'),
						semantic: semantic,
						abstract:abstract,
						attids:_attids,
						attNames:_attNames
					}
					
					var quesArr = [];
					var $quesArr = $('input[_name="kbs-question"]');
					$quesArr.each(function(i, item){
						if($(item).val()==null||$(item).val()==''||$(item).val()==undefined){
							alert('标准问不能为空');
							flag = false;
							return false;
						}
						var _id = $(item).attr('_qid');
						if(!_id){//如果没有_qid 则表示为新增的知识点，通过_dummyID去关联对应的答案
							_id = $(item).attr('_dummyID');
						}
						var answerArr = [];
						$($('tr[_id="' + _id + '"]').get(1)).find('textarea').each(function(i, item){
							var _editor = map[$(item).attr('tid')];
							var _content ;
							if(!_editor){
								_content = $.trim($(item).prev().text());
								
							}else {
								 _content = _editor.getData();
							}
							
							if(_content ==null || _content == ''){
								flag = false;
								return false;							
							}
							var _attids = $(item).attr('_attidsNew');	//附件ids
							var _attNames = $(item).attr('_attNamesNew');	//附件names
							var _dimtags = $(item).attr('_dimtags');	//答案维度属性ids
							var _dimnames = $(item).attr('_dimnames');
							
							answerArr.push({
								id: $(item).attr('aid'),
								content: _content,
								attids: _attids,
								attNames:_attNames,
								dimtags: _dimtags,
								dimsnames: _dimnames
							})
						});
					
						if(answerArr == null ||answerArr.length ==0 || !flag){;
							flag = false;
							alert('[' + $(item).val() + ']的答案不能为空');
							return false;
						}
					
						quesArr.push({
							id: $(item).attr('_qid'),
							attrid: $(item).attr('_attrid'),
							attrname: $(item).attr('_attrname'),
							groupid: $($(item).parents('table').get(0)).attr('_panel'),
							name: $(item).val(),
							answers: answerArr
						})
					});
					
					//待匹配标准问信息封装 add by wilson.li at 2016-07-25
					var questions = [];
					$("[name='unMatchQuestion']").each(function(i,item){
						var answers = [];
						$(item).find("[name='unMatchAnswer']").each(function(i,aItem){
							var aId = $(item).find("[name='unMatchAnswerId']").val();
							var aContent = $(item).find("[name='unMatchAnswerContent']").val();
							var aDimIds = $(item).find("[name='answerDimIds']").val();
							var aDimNames = $(item).find("[name='answerDimNames']").val();
							var aAttId = $(item).find("[name='answerAttIds']").val();
							var aAttNames = $(item).find("[name='answerAttNames']").val();
							answers.push({
								id: aId,
								content: aContent,
								attids: aAttId,
								attNames:aAttNames,
								dimtags: aDimIds,
								dimsnames: aDimNames
							});
						});
						var qId = $(item).find("[name='unMatchQuestionId']").val();
						var qName = $(item).find("[name='unMatchQuestionName']").val();
						var qAttrId = $(item).find("[name='unMatchQuestionAttrId']").val();
						var qAttrName = $(item).find("[name='unMatchQuestionAttrName']").val();
						var qGroupId = $(item).find("[name='unMatchQuestionGroupId']").val();
						quesArr.push({
							id: qId,
							attrid: qAttrId,
							attrname: qAttrName,
							groupid: qGroupId,
							name: qName,
							answers: answers
						})
					});
					
					$.extend(article, {
						questions: quesArr
					});
					return article;
				}
				
				
				var setting = {
					view: {
						showIcon: function(){return false}
					},
					callback: {
						onClick: function(event, treeId, treeNode, clickFlag){
							if(treeNode.attrId == ""){//标题
								location.hash = '#' + treeNode.id;
							}else{//属性
								var _this = $("input[_attrid='"+treeNode.attrId+"']");
								$(_this).focus();
								//上移
								var moveTo = $(_this).offset().top-5;
								$("html,body").animate({scrollTop:moveTo},10);
							}
						}
					}
	
				};
	
				var zNodes = ${requestScope.groupNodes };
				
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				
				//左边模板浮动
				var _height = 80;//预留空间
				var scrollTop = $(window).scrollTop();
				var moveTop = scrollTop+_height;
				$("#biztplOperate").animate({top:scrollTop},0);
				$("#treeDemo").animate({top:moveTop},0);
				$(window).scroll(function(){
					moveTop = $(window).scrollTop()+_height;
					$("#treeDemo").animate({top:moveTop},0);
					scrollTop = $(window).scrollTop();
					$("#biztplOperate").animate({top:scrollTop},0);
				});
				$('input[name=property]').click(function(){
					var id = '${bizTplArticle.id  }';
					$("#openEditPropIframe")[0].src = $.fn.getRootPath() + "/app/biztpl/article!property.htm?id="+id;
					$("#openEditPropDiv").dialog("open");
				});
				function _moveTop(_thisDiv,_height){
					$(_thisDiv).window("move",{top:$(window).scrollTop() + _height});
				}
				
				//点击更换模板
				$("#changeBiztpl").on("click",function(){
					if(confirm("请确保文章已保存，否则修改的数据将丢失")){
						var articleName = $("[name='articleName']").val();
						var _url = $.fn.getRootPath() + "/app/biztpl/article!changeBiztplPage.htm?articleName="+articleName;
						//打开编辑页面所在iframe
						$("#openChangeBiztplIframe")[0].src = _url;
						$("#openChangeBiztplDiv").dialog("open");
						_moveTop($("#openChangeBiztplDiv"),10);
					}
				});
				//修改模板
				$("#modifyBiztpl").on("click",function(){
					if(confirm("请确保文章已保存，否则修改的数据将丢失")){
						var articleId = $("[name='id']").val();
						var tplId = $("[name='tplId']").val();
						var _url = $.fn.getRootPath() + "/app/biztpl/article!modifyBiztplPage.htm?articleId="+articleId+"&tplId="+tplId;
						//打开编辑页面所在iframe
						$("#openModifyBiztplIframe")[0].src = _url;
						$("#openModifyBiztplDiv").dialog("open");
						_moveTop($("#openModifyBiztplDiv"),10);
					}
				});
				
				//点击标准问名称，显示答案
				$("[class='pipei']").on("click","[name='qName']",function(){
					if($(this).parent().find("div").is(":hidden")){
						$(this).parent().find("div").show();
					}else{
						$(this).parent().find("div").hide();
					}
				});
				//删除标准问
				$("[class='pipei']").on("click","[class='ico-delete2']",function(){
					if(confirm("确定删除吗?")){
						var _this = this;
						var id = $(_this).attr("_id");
						$.ajax({
							type: "POST",
							url: $.fn.getRootPath()+"/app/biztpl/article!deleteQuestion.htm",
							data: "id="+id,
							dataType:"json",
							success: function(data) {
								if(data.status == 0){
									$(_this).parent().remove();
									alert("操作成功");
								}else{
									alert("操作失败");
								}
							}
						});
					}
				});
				//打开移动到标题页面，记录标准问id
				$("[class='pipei']").on("click","[class='ico-move']",function(){
					$("#moveQuestionId").val($(this).attr("_id"));
					$("#openMoveQuestionDiv").dialog("open");
					_moveTop($("#openMoveQuestionDiv"),100);
				});
				//移动到标题页面，记录标题id
				$("[class='move']").on("click","[name='moveGroup']",function(){
					$("#moveGroupId").val($(this).attr("_id"));
					$("[name='moveGroup']").removeClass("title-checked");
					$(this).addClass("title-checked");
				});
				//确定将标准问移动到标题下
				$("#btnSure").on("click",function(){
					var questionId = $("#moveQuestionId").val();
					var groupId = $("#moveGroupId").val();
					if(groupId == ""){
						alert("请选择标题");
						return false;
					}else{
						var param = {
							articleId: $("#id").val(),
							questionId: questionId,
							groupId: $("#moveGroupId").val()
						};
						$.ajax({
							type: "POST",
							url: $.fn.getRootPath()+"/app/biztpl/article!moveQuestion.htm",
							data: param,
							dataType:"json",
							success: function(data) {
								if(data.status == 0){
									$("[_id='"+questionId+"']").parent().hide();
									alert("操作成功,刷新有效");
								}else if(data.status == 0){
									alert(data.msg);
								}else{
									alert("操作失败");
								}
							}
						});
						$("#openMoveQuestionDiv").dialog("close");
					}
				});
			});
			function closeProIframe(){
				$("#openEditPropDiv").dialog("close");
			}
			//关闭更换模板页面
			function closeIframe(){
				$("#openChangeBiztplDiv").dialog("close");
			}
			//关闭修改模板页面
			function closeModifyIframe(){
				$("#openModifyBiztplDiv").dialog("close");
			}
		</script>
	</head>

	<body>
		<input type="hidden" name="id" value="${bizTplArticle.id }"/>
		<input type="hidden" name="tplId" value="${bizTplArticle.tplId }"/>
		
		<!-- 更换模板页面 -->
		<div id="openChangeBiztplDiv" class="easyui-dialog" closed="true" modal="true" title="更换模板" 
			style="width:530px;min-height:300px;height:600px;overflow: hidden;">
			<iframe scrolling="auto" id='openChangeBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
		<!-- 修改模板页面 -->
		<div id="openModifyBiztplDiv" class="easyui-dialog" closed="true" modal="true" title="修改模板" 
			style="width:800px;min-height:300px;height:600px;overflow: hidden;">
			<iframe scrolling="auto" id='openModifyBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
		<!-- 移动标准问页面 -->
		<div id="openMoveQuestionDiv" class="easyui-dialog" closed="true" modal="true" title="移动标准问" 
			style="width:400px;min-height:300px;height:400px;overflow: hidden;">
			<div style="width: 100%;float: left;height: 88%;">
				<div class="move">
					<input type="hidden" id="moveQuestionId" value="">
					<input type="hidden" id="moveGroupId" value="">
					<h3>移动到</h3>
					<dl>
						<!-- 暂时支持二级，更多级时可继续循环，最好是后台将数据处理好之后一次遍历 -->
						<c:forEach items="${groupNodes }" var="group" varStatus="status">
							<c:if test="${group.attrId==''}">
								<dt><span name="moveGroup" _id="${group.groupId }" style="cursor: pointer;">${status.index+1 }.${group.name }</span></dt>
							</c:if>
							<c:if test="${fn:length(group.children) > 0 }">
								<c:forEach items="${group.children }" var="child" varStatus="childStatus">
									<c:if test="${child.attrId==''}">
										<dd><span name="moveGroup" _id="${child.groupId }" style="cursor: pointer;">${status.index+1 }.${childStatus.index+1 } ${child.name }</span></dd>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
					</dl>
				</div>
			</div>
			<div style="float: right;margin-right: 10px;">
				<button class="btn btn-default" id="btnSure">确定</button>
			</div>
		</div>
		
		<table width="100%">
			<tr>
				<td width="25%" style="padding: 0px 10px;">
					<!-- 左边模板浮动区域 -->
					<div id="biztplOperate" style="width: 20%;z-index: 1000;height: 80px;position: absolute;top:0px;">
						<h4>文章模板</h4>
						<h5>
							<a id="changeBiztpl" href="javascript:void(0)" style="margin-right: 15px;">
								<img src="${pageContext.request.contextPath }/resource/biztpl/images/ico-8.png" style="width: 18px;margin-right: 3px;">更换文章模板
							</a>
							<a id="modifyBiztpl" href="javascript:void(0)">
								<img src="${pageContext.request.contextPath }/resource/biztpl/images/ico-2.png" style="width: 18px;margin-right: 3px;">修改模板
							</a>
						</h5>
						
					</div>
					<!-- 左边模板浮动区域 -->
					<div id="treeDemo" class="ztree" style="width: 25%;position: absolute;z-index: 1000;"></div>
				</td>
				<td width="75%">
					<table width="100%" class="table">
						<colgroup>
							<col width="15%">
							<col width="35%">
							<col width="15%">
							<col width="35%">
						</colgroup>
						<tr>
							<td>文章路径</td>
							<td colspan="3"><input type="text" name="" value="${bizTplArticle.categoryName }" _id="${bizTplArticle.categoryId }" disabled="disabled"/></td>
						</tr>
						<tr>
							<td>文章名称</td>
							<td colspan="3">
								<input type="text" name="articleName" value="${bizTplArticle.name }" _aid="${bizTplArticle.id }"/>
								<font style="color: red;font-size: 16px;">*</font>
							</td>
						</tr>
						<tr>
							<td>标签</td>
							<td><input type="text" name="label" nodeIds="${bizTplArticle.label }" value=""/></td>
							<td>追溯期</td>
							<td><input type="text" name="traceDate" 
							value="<fmt:formatDate value="${bizTplArticle.traceDate }" pattern="yyyy-MM-dd"/>"
							onclick="WdatePicker()"/>
							</td>
						</tr>
						<tr>
							<td>有效期</td>
							<td>
								<input id="startDate" type="text" name="startDate" style="width:120px;" 
									value="<fmt:formatDate value="${bizTplArticle.startDate }" pattern="yyyy-MM-dd"/>"
									onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})"/>
								 ~ 
								<input id="endDate" type="text" name="endDate" style="width:120px;" 
									value="<fmt:formatDate value="${bizTplArticle.endDate }" pattern="yyyy-MM-dd"/>"
									onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd'})"/>
							</td>
							<td>语义块</td>
							<td>
								<input type="text" name="semantic" value="${bizTplArticle.semantic }" _aid="${bizTplArticle.id }"/>
								<font style="color: red;font-size: 16px;">*</font>
							</td>
						</tr>
						<tr>
							<td>属性</td>
							<td>
								<input type="button" name="property" value="添加">
							</td>
						</tr>
						<tr>
							<td colspan="4" align="right">
								<button class="btn btn-default" id="btnSave" style="background-color: #e1e1e1;">保存</button>
								<!--  <button class="btn btn-default" id="btnStore">入库</button>-->
							</td>
						</tr>
					</table>
					<div class="con-a">
		                 <h2 >正文</h2>
		                 <div class="con-b">
							<table width="100%" class="table">
								<tr>
									<td class="con-c"><a href="javascript:void(0)" _name="编辑介绍" class="f-size"><i class="ico-t1"></i>文章介绍</a>
										 <img _id="${bizTplArticle.id  }" _abstractType="abstract" _name="abstract" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_edit2.png" title="编辑介绍" style="cursor: pointer;vertical-align:top;width: 30px;display:inline-block;float:right;">
									</td>
								</tr>
								<tr _id="${bizTplArticle.id }">
								<c:if test="${not empty bizTplArticle.articleAbs}">
									<td>
										<div type="readContent" style="width:930px;" class="textarea_div" aid="${bizTplArticle.id }" _name="group${bizTplArticle.id }" _type="abs">${bizTplArticle.articleAbs }</div>
										<textarea id="group${bizTplArticle.id }" tid="${bizTplArticle.id }" aid="${bizTplArticle.id }" style="height: 80px;width:100%; hidden;display:none;" _attids="${attIds}" _attNames="${attNames}">
																		${bizTplArticle.articleAbs }
										</textarea>
									</td>
								</c:if>
								</tr>
								${htmlData }
							
							</table>
							<c:if test="${fn:length(unMatchList) > 0 }">
							<div class="content">
								<dl class="pipei">
									<dt> 等待匹配的问题</dt>
									<c:forEach items="${unMatchList }" var="question" varStatus="status">
										<dd name="unMatchQuestion">
											<font name="qName" style="cursor: pointer;">${status.index+1 }.${question.name }</font>
											<i class="ico-delete2" _id="${question.id }"></i>
											<i class="ico-move" _id="${question.id }"></i>
											<input type="hidden" name="unMatchQuestionId" value="${question.id }">
											<input type="hidden" name="unMatchQuestionName" value="${question.name }">
											<input type="hidden" name="unMatchQuestionAttrId" value="${question.attrId }">
											<input type="hidden" name="unMatchQuestionAttrName" value="${question.attrName }">
											<input type="hidden" name="unMatchQuestionGroupId" value="${question.groupId }">
											<c:if test="${fn:length(question.answers) > 0 }">
												<c:forEach items="${question.answers }" var="answer">
													<div name="unMatchAnswer" style="margin-bottom: 5px;">
														<p>${answer.content }</p>
														<input type="hidden" name="unMatchAnswerId" value="${answer.id }">
														<input type="hidden" name="unMatchAnswerContent" value="${answer.content }">
														<input type="hidden" name="answerDimIds" value="${answer.dimIds }">
														<input type="hidden" name="answerDimNames" value="${answer.dimIds }">
														<input type="hidden" name="answerAttIds" value="">
														<input type="hidden" name="answerAttNames" value="">
													</div>
												</c:forEach>
											</c:if>
										</dd>
									</c:forEach>
								</dl>
							</div>
							</c:if>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<div id="toolbar-question" style="display:none;">
			<button class="btn btn-default" _id="{0}" _name="btnRemove">删除</button> 
			<button class="btn btn-default" _id="{0}" _name="btnEdit">编辑答案</button>
			<button class="btn btn-default" _id="{0}" _name="btnCancelEdit" style="display:none;">取消编辑</button>
			<button class="btn btn-default" _id="{0}" _name="btnNewAnswer">新增答案</button>
		</div>
		<div id="openEditPropDiv" class="easyui-window" closed="true" modal="true" title="编辑属性" style="width:70%;height:50%;overflow: hidden;">
		    <iframe scrolling="auto" id='openEditPropIframe' frameborder="0"  src="" style="width:100%;height:100%;overflow: hidden;"></iframe>
		</div>
		<div style="display: none;">
				<textarea id = "textarea"></textarea>
		</div>
		<%-- 图片插入 --%>
		<jsp:include page="../util/ckEdit-myImage.jsp"></jsp:include>
		<%-- 标签选择 --%>
		<jsp:include page="article_labels.jsp"></jsp:include>
	</body>
</html>
