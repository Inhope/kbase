<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>文章列表</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<style type="text/css">
		table{
			border-collapse:collapse;
			text-align:center;
			background:white;
			width:100%;
			overflow:hidden;
		}
		table tr:nth-child(2n+1){
			background-color:#F8F8F8;
		}
		table td{
			font-size:13px;
			border:1px solid #ddd;
			padding:5px;
		}
		table td a{
			color:blue;
			text-decoration:underline;
		}
		table td:nth-child(2){
			text-align:left;
		}
		table td font{
			cursor: pointer;
		}
		.more{
			background: rgba(0, 0, 0, 0) url('${pageContext.request.contextPath }/resource/biztpl/images/more.gif') no-repeat scroll right center;
			color: #777; 
			cursor: pointer;
			float: right;
			font-size: 13px;
			padding-right: 12px;
		}
	</style>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript">
		$.extend($.fn, {
			getRootPath : function(){
			    return "${pageContext.request.contextPath }";
			}
		});
		var tplId = '${tplId}';
		var pageNo = 1;
		var dataNum = 0;
		$(function(){
			loadData = function(){
				var param = {
					tplId: tplId,
					pageNo: pageNo
				};
				$.ajax({
					type: "POST",
					dataType : 'json',
					url: $.fn.getRootPath() + '/app/biztpl/biz-tpl!findArticles.htm',
					//async: false,
					data: param,
					success: function(data){
						if(data.length == 0){
							$("#msgShow").show();
						}else{
							$("#msgShow").hide();
							pageNo += 1;
							$(data).each(function(i,item){
								dataNum += 1;
								var html = "";
								html 	+= "<tr>";
								html 	+= "	<td>"+dataNum+"</td>";
								html 	+= "	<td><font name='articleName' id='"+item.id+"'>"+item.name+"</font></td>";
								html 	+= "	<td>"+(item.createUser==undefined?"":item.createUser)+"</td>";
								html 	+= "	<td>"+item.createDate+"</td>";
								html 	+= "</tr>";
								$("#datalist").append(html);
							});
						}
					}
				});
			}
			$("[class='more']").on("click",function(){
				loadData();
			});
			loadData();
			$("#datalist").on("click","[name='articleName']",function(){
				window.open($.fn.getRootPath()+"/app/biztpl/article-search.htm?articleId="+$(this).attr("id"));
			});
		});
	</script>
</head>
<body style="overflow-y: auto;margin: 0px;">
	<div>
		<table>
			<thead>
				<tr style="background-color:#DADADA;">
					<td width="10%" style="text-align: center;"><b>序号</b></td>
					<td width="60%" style="text-align: center;"><b>文章名称</b></td>
					<td width="15%" style="text-align: center;"><b>创建人</b></td>
					<td width="15%" style="text-align: center;"><b>创建时间</b></td>
				</tr>
			</thead>
			<tbody id="datalist">
			</tbody>
		</table>
		<div style="width: 100%;text-align: center;">
			<font id="msgShow" style="color: red;font-size: 12px;">没有更多数据</font>
			<i class="more" style="margin-top: 5px;">更多</i>
		</div>
	</div>
</body>
</html>