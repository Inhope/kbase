<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
function funCallback(funcNum,fileUrl){  

    var parentWindow = ( window.parent == window ) ? window.opener : window.parent;  

    parentWindow.CKEDITOR.tools.callFunction(funcNum, fileUrl);  

    window.close();  

}  

</script>
<c:forEach items="${fileList}" var="file">
	<img width='110px' height='70px' src='${file.src }'	alt='${file.filename }'	onclick='funCallback("${callback}","${file.src }" )' />
</c:forEach>
