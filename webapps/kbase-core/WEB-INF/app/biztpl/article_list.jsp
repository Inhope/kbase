<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>文章列表</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/biztpl/js/ArticleList.js"></script>
  	</head>
  	<body class="easyui-layout">   
	    <div data-options="region:'west',title:'文章分类',split:true" style="width:220px;">
	    	<div style="width: 100%;">
	    		<ul class="easyui-tree" id="cate-article" data-options="url:'${pageContext.request.contextPath }/app/main/ontology-category!list.htm',
	    			loadFilter:function(data, parent){
		   				if(data){
		   					$(data).each(function(i, o){
		   						o.text = o.name;
			   					if(o.isParent == 'true')
			   						o.state = 'closed';
			   					else o.state = 'open';
								try{
									/*查询该分类下文章数量*/
									$.ajax({
										type: 'POST',
										async: false,
										url: $.fn.getRootPath()+'/app/biztpl/article!listDataCount.htm',
										data: {categoryBh: o.bh },
										dataType:'json',
										success: function(result) {
											if(result.count > 0){
												var font = $(document.createElement('font'))[0];
												$(font).css({'color': 'red'});
												$(font).text('(' + result.count + ')');
				   								o.text = o.text + font.outerHTML;
											}
										}
									});
								} catch(e){alert(e);}
			   				});
		   				}
		   				return data;
		   			},
	    			onClick: function(node){
	    				/*刷新列表*/
	    				ArticleList.fn.reloadList();
					}"></ul> 
	    	</div>
	    </div>
	    <div data-options="region:'center',title:'列表',iconCls:'icon-ok'" style="background:#eee;">
	    	<!--导航栏  -->
			<div id="now-toolbar" style="padding:5px;height:auto">
				<div>
					文章名：<input class="easyui-textbox" id="name" style="width:200px">
					&nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
				</div>
				<div>
				    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"    id="btn-add">新增</a>
				    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"   id="btn-edit">编辑</a>
				    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="btn-remove">删除</a>
				    <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" id="btn-search">查看</a>
			    </div>
			</div>
			<!-- 列表 -->
	    	<table class="easyui-datagrid" id="now-tab" data-options="url:'${pageContext.request.contextPath }/app/biztpl/article!listData.htm',
	    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,toolbar:'#now-toolbar',pagination:true">
			    <thead>
			        <tr>
			        	<th data-options="field:'id',checkbox:true">文章名</th>
			            <th data-options="field:'name',width:200">文章名</th>   
			            <th data-options="field:'startDate',width:200">开始时间</th>
			            <th data-options="field:'endDate',width:200">结束时间</th>
			            <th data-options="field:'traceDate',width:200">追溯时间</th>
			        </tr>   
			    </thead>
			</table>
	    </div>
	    
	    <%-- 文章新增 --%>
	    <div id="dd_add-article"></div>
	</body>
</html>
