<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
	<style>
		.text{
			display: inline-block;
			vertical-align: top;
			width:90px;
			height:21px;
			line-height:21px;
			border:0;
			outline:none;
			background:url(${pageContext.request.contextPath }/resource/biztpl/images/ss_2.png) no-repeat scroll 10px center;
			padding-left:30px;
		} 
		.bzw{
			width:100%;
			border:0px solid #ccc;
			background:#f2f2f2;
		}
		.bzw_ss{
			height:29px;
			font:12px/29px "宋体";
			padding:0 15px;
			background:#E5EAFE;
			border-bottom:1px solid #ccc;
		}
		.bzw_ss_l{
			display: inline-block; 
			vertical-align: top;
			width:120px; 
			height:21px; 
			line-height: 21px;
			margin-top:3px;
			border-radius: 3px;
			border: 1px #ccc solid;
			background:#fff;
			overflow: hidden;
		}
		.bzw_ss_r{
			display: inline-block;
			width:auto;
			margin-left:12px;
			padding-left:24px;
			height:29px;
			background:url(${pageContext.request.contextPath }/resource/biztpl/images/ico-29.png) no-repeat left center;
			color:#555;
			font:13px/32px "tahoma, arial, sans-serif";
			cursor:pointer;
			overflow: hidden;
		}
		.bzw_con{
			min-height:150px;
			height:100px;
			height:auto;
			overflow:hidden;
			word-break: break-all;
			padding:10px;
			background:white;
		}
	</style>
	<script type="text/javascript">
	$(function(){
		var _value = "";
		var _attrname = "";
		var _attrid = "";
		var _this_q_id = '${param._this_q_id}';
		var articleName = '${param.articleName}';
		onClick = function(event, treeId, treeNode, clickFlag){
			//只能点击本体类的属性
			if(treeNode && treeNode.level > 0){
				var li = $(window.parent.document).find('li[attrid="'+treeNode.id+'"]:hidden');
				var delflag = $(li).attr('delflag');
				if(li && li.length > 0 && delflag == '0'){
					_attrname = '';
					_attrid = '';
					alert('该本体已被引用，不能重复引用！');
				}else{
					var param = {
							attrId: treeNode.id,
							articleName: articleName
					};
					$.ajax({
						type: "POST",
						dataType : 'json',
						url: '${pageContext.request.contextPath }/app/biztpl/article!findRuleName.htm',
						async: false,
						data: param,
						success: function(data){
							_value = data.name;
						}
					});
					//_value = articleName + treeNode.name;
					_attrname = treeNode.name;
					_attrid = treeNode.id;
				}
			}
		}
		/** 初始化属性树 start */
		var setting = {
			view: {
				//showIcon: false
			},
			callback: {
				onClick: onClick
			}
		};
		var zNodes = ${requestScope.bizTplAttr };
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		/** 初始化属性树 end */
		
		/** 搜索栏浮动  */
		var scrollTop = $(window).scrollTop();
		$('[class="bzw_ss"]').animate({top:scrollTop},0);
		$(window).scroll(function(){
			scrollTop = $(window).scrollTop();
			$('[class="bzw_ss"]').animate({top:scrollTop},0);
		});
		$("#btnOk").on("click",function(){
			if(_attrid){
				var question = $(window.parent.document).find('input[_this_q_id="'+_this_q_id+'"]').siblings('input[name="kbs-question"]');
				var attrId = $(question).attr('attrid');
				if(attrId && attrId.length > 0){
					$(window.parent.document).find('li[attrid="'+attrId+'"]').attr('delflag','1');
				}
				var li = $('<li id="" name="" attrId="" parentId="" groupId="" priority="" tplId=""	level="" delFlag=""	 style="display:none;"></li>');
				$(li).attr('id','');
				$(li).attr('attrId',_attrid);
				$(li).attr('name',_attrname);
				$(li).attr('groupId','');
				var parents = $(question).parents('li.q_ask').prev('li.q_ask');
				$(li).attr('parentId',$(parents).attr('groupid'));
				$(li).attr('priority',Number($(parents).attr('priority'))+1);
				$(li).attr('tplId',$(window.parent.document).find("[name='tplId']").val());
				$(li).attr('level','99');
				$(li).attr('delFlag','0');
				
				var oldli = $(window.parent.document).find('div.content').find('li');
				oldli.each(function(i,item){
					var priority = $(item).attr('priority');
					if(priority && priority.length > 0){
						if(Number(priority) > Number($(parents).attr('priority'))){
							$(item).attr('priority',Number($(item).attr('priority'))+1);
						}
					}
				});
							
				var content = $(window.parent.document).find('div.content').append(li);
			}
			
			var thisObj = $(window.parent.document).find("[_this_q_id='"+_this_q_id+"']").parent().find("[name='kbs-question']");
			if(_attrid != ""){
				thisObj.attr("value",_value);
				thisObj.attr("attrname",_attrname);
				thisObj.attr("attrid",_attrid);
			}
			window.parent.closeSelectAttrIframe();
		});
	});
	</script>
</head>
<body style="overflow-x: hidden">
	<div class="bzw">
		<h3 class="bzw_ss" style="z-index:1000;position:absolute;top:0px;width:100%;">
			<!-- <span class="bzw_ss_l">
				<input type="text" placeholder="搜索" autocomplete="off" class="text" value="">
			</span>
			<span class="bzw_ss_r">自定义</span> -->
			<a id="btnOk" href="javascript:void(0);" style="color: blue;float: right;margin-right: 30px;">确定</a>
		</h3>
		<div class="bzw_con" style="overflow: auto;">
			<div id="treeDemo" class="ztree" style="margin: 20px;height: 100%;"></div>
		</div>
	</div>
</body>
</html>