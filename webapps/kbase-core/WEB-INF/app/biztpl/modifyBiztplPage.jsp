<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<title>业务模板</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<style type="text/css">
			body{margin: 3px;font-size: 12px;}
			span.cke_button__level0_label{display: inline;}
			span.cke_button__level0_icon{width: 0px;}
			span.cke_button__level1_label{display: inline;}
			span.cke_button__level1_icon{width: 0px;}
			span.cke_button__level2_label{display: inline;}
			span.cke_button__level2_icon{width: 0px;}
			span.cke_button__level3_label{display: inline;}
			span.cke_button__level3_icon{width: 0px;}
			span.cke_button__biztpldel_label{display: inline;}
			span.cke_button__biztpldel_icon{width: 0px;}
		</style>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/content.css"/>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level0.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level1.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript">
			$(function(){
				var editor = CKEDITOR.replace('editor', {
					customConfig: '${pageContext.request.contextPath}/library/ckeditor/config-biz.js',height:340
				});
				onClick = function(event, treeId, treeNode, clickFlag){
					//只能点击本体类的属性
					if(treeNode && treeNode.level > 0){
						var hasThis = false;//是否已经包含该属性
						$("#bizTplPanel").html(editor.getData());
						var allInput = $("#bizTplPanel").find("input");
						allInput.each(function(i,item){
							if($(item).attr("attrid") == treeNode.id){
								hasThis = true;
								return false;
							}
						});
						if(hasThis){
							alert("已包含该属性");
						}else{
							if($('#bizTplPanel').find("input[xlevel=0]").length > 0){
								var baseHtml = '<input type="text" xlevel="99" attrid="' + treeNode.id + '" class="kbs-title-cell" value="' + treeNode.name + '">';
								var selectObj = editor.getSelection().getSelectedElement();
								if(selectObj){
									if($(selectObj).attr("xlevel") < 99){//选中的是标题
										baseHtml = selectObj.getOuterHtml()+"<br/>"+baseHtml;
									}
								}else{
									baseHtml += "<br/>";
								}
								window.__kbs_editor.editable().insertHtml(baseHtml);
							}else{
								alert("属性只能添加到标题下面");
							}
						}
					}
				}
				
				window.__kbs_editor = editor;
			
				//初始化属性树
				var setting = {
					view: {
						//showIcon: false
					},
					callback: {
						onClick: onClick
					}
				};
				var zNodes = ${requestScope.bizTplAttr };
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				
				//获取对象属性
				getObjectAttr = function(_obj,_index){
					var attrs;
					if(_obj){
						attrs = {
							id: $(_obj).attr("xid")==undefined?"":$(_obj).attr("xid"),
							name: $(_obj).val(),
							delflag: $(_obj).attr("xdelflag")==undefined?0:$(_obj).attr("xdelflag"),
							level: $(_obj).attr("xlevel"),
							attrid: $(_obj).attr("attrid")==undefined?"":$(_obj).attr("attrid"),
							groupid: $(_obj).attr("groupid")==undefined?"":$(_obj).attr("groupid")
						}
					}else{
						attrs = {
							id: "",
							name: "",
							delflag: 0,
							level: 0,
							attrid: "",
							groupid: ""
						}
					}
					attrs.priority = _index;
					return attrs;
				};
				
				//校验标题是否符合规则
				function checkData(iarr){
					var levelMap = {
						0:"一级标题",
						1:"二级标题",
						2:"三级标题",
						3:"四级标题"
					};
					var bo = true;//默认不符合规则
					var max = 0;//最顶级
					var min = 0;//最低级
					//var iarr [] = {2,3,4,5,6,2,3,4,5,6,3,4,5,4,5,5,2,3,3,4,4,4,4,5,5,3,4,5};
					//提取标题级别
					for(var x=1;x<iarr.length;x++){
						if(iarr[x] > min){
							min = iarr[x];
						}
						if(iarr[x] < max){
							max = iarr[x];
						}
					}
					if(iarr.length > 0 && iarr[0] == max){
						var lastLevel = max;//上一级，默认为最高级
						var haslevel = [];
						for(var i=0;i<iarr.length;i++){
							if(iarr[i] == max){//为顶级时，置空对象并重新填充
								haslevel = [];
							}
							haslevel.push(iarr[i]);
							//1、当前为最高级，2、当前为二级并且上一级是最高级
							if(iarr[i] == max || iarr[i] == max + 1 || lastLevel == iarr[i] || lastLevel == iarr[i]-1 || (lastLevel > iarr[i]-1 && haslevel.indexOf(iarr[i]) > -1)){
								lastLevel = iarr[i];
							}else{
								alert(levelMap[lastLevel]+"下不能直接跟"+levelMap[iarr[i]]);
								bo = false;
								break;
							}
						}
					}else{
						alert("最顶级只能是一级标题");
						bo = false;
					}
					return bo;
				}
				
				//确定
				$('#btnSure').click(function(){
					var _html = editor.getData();
					$('#bizTplPanel').html(_html);
					var allInput = $("#bizTplPanel").find("input");//所有input对象
					//第一个不能是属性
					if(allInput.length > 0 && $(allInput[0]).attr("xlevel") == 99){
						alert("属性只能添加到标题下面 ");
						return false;
					}
					//标题级别数组
					var levelArr = [];
					var attrs = [];
					allInput.each(function(i,item){
						if($(item).attr("attrid") == undefined || $(item).attr("attrid") == ''){
							if($(item).attr("xdelflag") != 1){
								levelArr.push($(item).attr("xlevel"));
							}
						}
						attrs.push(getObjectAttr(this,i+1));
					});
					if(levelArr.length > 0 && !checkData(levelArr)){
						return false;
					}
					var items = JSON.stringify(attrs);//模板数据
					var tplId = $('[name="tplId"]').val();//模板id
					var articleId = $('[name="articleId"]').val();//文章id
					var param = {
						tplId: tplId,
						articleId: articleId,
						items:items
					};
					if(articleId == ""){//新增
						var thisForm = $(window.parent.document).find("#indexForm");
						thisForm.find("[name='items']").val(items);
						thisForm.find("[name='artiName']").val($("[name='articleName']").val());
						thisForm.submit();
						//window.parent.closeModifyIframe();
					}else{//修改
						$.ajax({
							type: "POST",
							url: "${pageContext.request.contextPath }/app/biztpl/article!modifyBiztpl.htm",
							data: param,
							//async: true,
							dataType:"json",
							success: function(data) {
								if(data.status == 0){
									parent.location.href = '${pageContext.request.contextPath }/app/biztpl/article!index.htm?articleId='+articleId;
								}else{
									alert(data.msg);
								}
							}
						});
					}
				});
				$("#bizTplPanel").html(editor.getData());
			});
		</script>
	</head>

	<body>
		<input type="hidden" name="tplId" value="${tplId }">
		<input type="hidden" name="articleId" value="${articleId }">
		<input type="hidden" name="articleName" value="${articleName }">
		<table width="100%">
			<tr>
				<td ${fn:length(requestScope.bizTplAttr)==0?"width='2%'":"width='25%'"} valign="top">
					<div id="treeDemo" class="ztree" style="margin: 20px;max-height: 420px;overflow: auto;"></div>
				</td>
				<td width="73%">
					<div id="editor" style="height: 300px;overflow: hidden;">
						${requestScope.htmlContent}
					</div>
					<div style="padding: 5px;text-align: right;">
						<button class="btn btn-default" id="btnSure">确定</button>
					</div>
				</td>
				<td width="2%">
					&nbsp;
				</td>
			</tr>
		</table>
		<div style="display:none;" id="bizTplPanel"></div>
	</body>
</html>
