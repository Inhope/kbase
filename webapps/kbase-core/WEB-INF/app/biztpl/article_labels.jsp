<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<style type="text/css">
		.tag{
			width:100%;
			height:100%;
			position: absolute;
			z-index:2;
			overflow: hidden
		}
		.tag h2{
			height:36px;
			line-height:36px;
			text-indent:32px;
			font-size:14px;
			width:100%;
			color:#333;
			background:#ddd url(${pageContext.request.contextPath }/resource/biztpl/images/ico-13.png) no-repeat 10px center;
			background-size:16px 16px;
			border-bottom:1px solid #ccc;
		}
		.tag h3{
			height:36px;
			line-height:36px;
			text-indent:32px;
			font-size:14px;
			width:100%;
			color:#333;
			background:#eee;
			border-bottom:1px solid #ccc;
		}
		.tag h3 select{
			width:150px;
			height:24px;
		}
		.tag h2 a.tag_close{
			display: inline-block;
			background: url(${pageContext.request.contextPath }/resource/biztpl/images/big-close.png) no-repeat 60% center;
			float:right;
			width:40px;
			height:30px;
			background-size:12px 12px;
			cursor:pointer;
		}
		.tag_con{
			height:132px;
			padding:0px 6px;
			background:white;
			border-bottom:1px solid #ccc;
		}
		.tag_con span{
			display: inline-block;
			width:auto;
			padding:2px 0 2px 8px;
			margin:8px 8px 0 0;
			background-color:#FFB5B5;
		}
		.tag_con span i{
			display: inline-block;
			background:url(${pageContext.request.contextPath }/resource/biztpl/images/ico-20.png) no-repeat center center;
			background-size:14px 14px;
			width:16px;
			height:14px;
			cursor: pointer;
		}
		.tag_con2{
			height:120px;
			padding:0px 6px;
			background:white;
			border-bottom:1px solid #ccc;
		}
		.tag_con2 span{
			display: inline-block;
			width:auto;
			padding:2px 8px;
			margin:8px 8px 0 0;
			background-color:#FFB5B5;
			border:1px solid white;
			cursor: pointer;
		}
		.tag_con2 span:hover{
			border:1px solid #5C87B1;
		}
	</style>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript">
	$(function(){
		var nodeIds = '${param.nodeIds}';
		var _this_q_id = '${param._this_q_id}';
		//标签数据
		var childLabels = [];
		//显示在已选择标签区域
		showSelectLabel = function(id,name){
			var html = "<span id='"+id+"' name='label'>"+name+"<i></i></span>";
			$("#selectLabelList").append(html);
		}
		//写入父页面
		setLabelVal = function(){
			var tagIds = "";
			$("#selectLabelList span").each(function(i,item){
				if(tagIds == ""){
					tagIds += $(item).attr("id");
				}else{
					tagIds += "," + $(item).attr("id");
				}
			});
			if(_this_q_id != ""){//文章标签和标准问标签
				$(window.parent.document).find("[_this_q_id='"+_this_q_id+"']").val(tagIds);
			}else{
				$(window.parent.document).find("[name='label']").attr("nodeIds",tagIds);
			}
		}
		//标签类型
		$.ajax({
			type: "POST",
			async: false,
			url:'${pageContext.request.contextPath}/app/util/choose!labelData.htm',
			dataType:"json",
			success: function(data) {
				if(data) {
					$(data).each(function(i,item){
						if(this.isParent){
							var html = "<option value='"+item.id+"'>"+item.name+"</option>";//"<span>"+this.name+"<i></i></span>";
							$("#labelTypes").append(html);
						}else{
							childLabels.push(this);
							//如果有已选中，则默认显示
							if(nodeIds != ""){
								var ids = nodeIds.split(",");
								$.each(ids,function(j,value){
									if(value == item.id){
										showSelectLabel(item.id,item.name);
									}
								});
							}
						}
					});
				}
			}
		});
		//点击下拉列表
		$("#labelTypes").on("change",function(){
			$("#labelList").empty();
			//console.info(childLabels);
			var selectId = $("#labelTypes option:selected").val();
			if(selectId != ""){
				$(childLabels).each(function(){
					if(this.pId == selectId){
						var html = "<span id='"+this.id+"' name='label'>"+this.name+"</span>";
						$("#labelList").append(html);
					}
				});
			}
		});
		//选择标签事件
		$("#labelList").on("click","span[name='label']",function(){
			$("#msg").text("");
			var _this = this;
			var isNotExist = true;
			$("#labelList span[name='label']").css("border","");
			$(this).css("border","1px solid #5C87B1");
			$("#selectLabelList span[name='label']").each(function(i,item){
				if($(item).attr("id") == $(_this).attr("id")){
					isNotExist = false;
					return;
				}
			});
			if(isNotExist){
				showSelectLabel($(_this).attr("id"),$(_this).text());
				setLabelVal();
			}else{
				$("#msg").text('提示:'+'"'+$(_this).text()+'"已存在');
			}
		});
		//取消选择标签
		$("#selectLabelList").on("click","span i",function(){
			if(confirm("确定移除该标签?")){
				$(this).parent().remove();
				//写入父页面
				setLabelVal();
			}
		});
	});
</script>
</head>
<body>
	<div class="tag">
		<h2>选择的标签<!-- <a class="tag_close" javascript:void(0)></a> --></h2>
		<div class="tag_con" id="selectLabelList">
			<!-- <span id="" name="label">test2<i></i></span> -->
		</div> 
		<h2>待选择的标签</h2>
		<h3>标签类别:
			<select name="labelTypeList" id="labelTypes" class="tag_w">
				<option value=""></option>
			</select>
			<span style="color: red;font-size: 12px;" id="msg"></span>
		</h3>
		<div class="tag_con2" id="labelList">
			<!-- 标签列表 -->
		</div> 
	</div>
</body>
</html>