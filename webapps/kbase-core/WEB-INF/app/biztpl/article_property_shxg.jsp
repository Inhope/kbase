<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="UTF-8">
		<title>属性展示</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
		<style>
			.property {
				width: 428px;
				height: auto;
				border: 1px solid #ccc;
				background: white;
				z-index: 99;
				font-size: 12px;
			}
			
			.property h2 {
				height: 28px;
				line-height: 28px;
				background: #f2f2f2 url(${pageContext.request.contextPath }/resource/biztpl/images/ico-11.png) no-repeat
					10px center;
				background-size: 24px 24px;
				font-size: 16px;
				padding: 5px;
				color: #555;
				text-indent: 2em;
				border-bottom: 1px solid #ccc;
			}
			
			.property i.close {
				background: url(${pageContext.request.contextPath }/resource/biztpl/images/big-close.png) no-repeat;
				display: inline-block;
				float: right;
				width: 12px;
				height: 12px;
				background-size: 12px 12px;
				cursor: pointer;
			}
			
			.prop_con .level1 {
				height: auto;
				padding: 10px 8px;
			}
			
			.level1>li {
				height: 28px;
				line-height: 28px;
				margin-top: 5px;
				position: relative;
				margin-left: 100px;
				vertical-align: middle
			}
			
			.prop_con  select {
				border: 1px solid #ccc;
				width: 100px;
			}
			
			.prop_con input[type="checkbox"] {
				margin-right: 3px;
				vertical-align: middle;
			}
			
			.level1>li span.title {
				width: 100px;
				font-size: 13px;
				text-align: right;
				float: left;
				margin-left: -100px;
				line-height: 34px;
			}
			
			.prop_con i.add {
				background: url(${pageContext.request.contextPath }/resource/biztpl/images/biztpl_add2.png) no-repeat;
				display: inline-block;
				vertical-align: middle;
				width: 18px;
				height: 18px;
				margin-left: 3px;
				background-size: 15px 15px;
				cursor: pointer;
			}
			
			.prop_con i.remove_click {
				background: url(${pageContext.request.contextPath }/resource/biztpl/images/biztpl_remove.png) no-repeat;
				display: inline-block;
				vertical-align: middle;
				width: 18px;
				height: 18px;
				margin-left: 3px;
				background-size: 15px 15px;
			}
			
			.prop_con_b {
				width: 100%;
				height: 50px;
				line-height: 50px;
				text-align: center;
			}
			
			.prop_con_b button {
				width: 60px;
				height: 28px;
				line-height: 28px;
				border: 1px solid #ccc;
				margin-right: 20px;
				border-radius: 2px;
				cursor: pointer;
			}
			
			.one_level {
				border: 1px solid #ccc;
				padding: 2px 5px;
				height: auto;
				background: white;
				margin-left: 8px;
				display: none
			}
			
			.one_level_2 {
				border: 1px solid #ccc;
				padding: 2px 1%;
				height: auto;
				width: 95%;
				position: absolute;
				top: 0;
				right: 0;
				left: 8px;
				background: white;
				z-index: 9;
				display: none
			}
			
			.one_level2 p {
				height: 28px;
				line-height: 28px;
				font-size: 13px;
			}
			
			.one_level_r {
				float: right;
				position: relative;
			}
			
			.one_level>input[type="text"] {
				border: 1px solid #cdcdcd;
				border: 0;
				height: 24px;
				line-height: 24px;
				border-radius: 4px;
				width: 98%;
				padding: 0 1%;
				outline: 0;
				color: #333;
			}
			
			.one_level>select {
				width: 100%;
				height: 24px;
				line-height: 24px;
			}
			
			.select_checkBox {
				position: relative;
				display: inline-block;
			}
			
			.check_xl_1 a {
				line-height: 20px;
				padding: 0px 5px;
				color: #313131;
				text-decoration: none;
				cursor: pointer;
			}
			
			.check_xl {
				background-color: white;
				border: 1px solid gray;
				display: none;
				position: absolute;
				left: 0;
				top: 25px;
				width: 220px;
				height: 310px;
				border-right: none;
				overflow-y: auto;
				overflow-x: hidden;
				z-index: 9999 !important;
			}
			
			.check_xl>ul {
				float: left;
				margin: 0 8px;
			}
			
			.check_xl>ul>li {
				float: left;
				text-align: left;
				width: 197px;
				overflow: hidden;
				white-space: nowrap;
				text-overflow: ellipsis;
				display: block;
				left: 0px;
				clear: both;
			}
		</style>
		<script type="text/javascript">
			$(function(){
				/**数据赋值**/
				var val = $('textarea#textarea').text();
				if(val && val.length > 0){
					var data = eval(val);
					for(var i in data){  
						var tagId = data[i].tagId;
						var tagVal = data[i].tagVal;
						var obj = $('#'+tagId);
						if(tagVal &&  tagVal.length > 0){
							$(obj).find('input').val(tagVal);
						}else{
							var _type = $(obj).attr('_type');
							if(_type == 'select_choices'){
								$(obj).attr("selected",true);
							}else if(_type == 'multiple_choices'){
								$(obj).attr("checked",true);
							}
						}
					}
				}
			
				var li = $('ul#allProperty').children('li[type="multiple_choices"]')
				if(li && li.length > 0){
					li.each(function(i, item){
						var title = $(item).children('span.title').attr('title');
						if(title == '分组'){
							var inputs = $(item).find('div > input[_type="multiple_choices"]:checked');
							if(!inputs || inputs.length == 0){
								$(item).find('div > input[_type="multiple_choices"]').attr("checked",'true');
							}
						}
					})
				}
			})
		</script>
	</head>
	<body>


		<div class="property">
			<h2>
				选择属性22
				<i class="close"></i>
			</h2>
			<div class="prop_con">
				<ul class="level1" id="allProperty">
					<c:forEach items="${list}" var="property">
						<li id="${property.id }" type="${property.dataType }"
							_id="<c:if test="${property.dataType == 'date_text' || property.dataType == 'single_row_text'}">${property.articleProperty.id }</c:if>">
							<span class="title" title="${property.name }">${fn:substring(property.name, 0, 6)}<i class="add"></i></span>
							<c:if test="${property.dataType == 'multiple_choices'}">
								<br><div class="one_level_2">
									<c:forEach items="${property.propertyTags}" var="tag">
											<div>
											   <input _type="${property.dataType }" _id="<c:if test="${not empty tag.articleProperty}">${tag.articleProperty.id }</c:if>" id = "${tag.id }" type="checkbox" <c:if test="${not empty tag.articleProperty}">checked="checked"</c:if>/>${tag.name }
												<span class="one_level_r">
													<c:if test="${tag.dataType == 'multiple_choices'}">
														<div class="check_xl_1">
																<a>选择指标</a>
														</div>
														<div class="check_xl">
														  <ul>
															  	<c:forEach items="${tag.children}" var="children">
																	<li><input _type="${tag.dataType }"  id="${children.id }" _id="<c:if test="${not empty children.articleProperty}">${children.articleProperty.id }</c:if>" <c:if test="${not empty children.articleProperty}">checked="checked"</c:if> title="${children.name }" type="checkbox" value="1"/>${children.name }<br></li> 
																</c:forEach>
															</ul>
														</div>
													</c:if>
													<c:if test="${tag.dataType == 'select_choices'}">
														<c:if test="${not empty tag.children}">
															<select name="liebie" id="">
																<option value="-1" >全部</option>
																<c:forEach items="${tag.children}" var="children">
																	   <option _type="${tag.dataType }" id="${children.id }" value="${children.id }" <c:if test="${not empty children.articleProperty}">selected</c:if>>${children.name }</option>
																</c:forEach>
															</select>
														</c:if>
													</c:if>
												</span>
											
											</div>
									</c:forEach>
								</div>
							</c:if>
							<c:if test="${property.dataType == 'select_choices'}">
								<div class="one_level_2">
									<c:if test="${not empty property.propertyTags}">
										<select name="liebie" id="" style="width: 100%;">
											<c:forEach items="${property.propertyTags}" var="tag">
												<option _type="${property.dataType }" id="${tag.id }" value="${tag.id }" <c:if test="${not empty tag.articleProperty}">selected</c:if>>${tag.name }</option>
											</c:forEach>
										</select>
									</c:if>
								</div>
							</c:if>
							<c:if test="${property.dataType == 'single_row_text'}">
								<div class="one_level">
									<input type="text" value="${property.articleProperty.propertyVal }" _type="${property.dataType }">
								</div>
							</c:if>
							
							<c:if test="${property.dataType == 'date_text'}">
								<div class="one_level">
									<input type="text" name="date"  onclick="WdatePicker()"
												value="${property.articleProperty.propertyVal }" _type="${property.dataType }"/>
								</div>
							</c:if>
						</li>
					</c:forEach>
				</ul>
				<div class="prop_con_b">
					<button id="ok">
						确定
					</button>
					<button id='cancel'>
						取消
					</button>
				</div>
			</div>
		</div>
	</body>
</html>