<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>答案属性选择</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		
		<style type="text/css">
			body{
				margin: 10px;
				font-size: 12px;
			}
			input[type="text"]{
				border: 1px solid #cdcdcd;
				height: 30px;
				line-height: 30px;
				border-radius: 4px;
				margin: 4px 0;
				width: 72%;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		
		<script type="text/javascript">
		
			$(function(){
				var $textarea = $(parent.document).find('textarea[id="' + parent.__kbs_editor_id + '"]');
				
				//勾选 checkbox
				var _dimtags = $textarea.attr('_dimtags');
				if (_dimtags!=undefined){
					var ids_name = [];
					var _dimtagsArr = _dimtags.split(',');
					
					$(_dimtagsArr).each(function(i, item){
						$('input[name="' + item + '"]').attr('checked', 'checked');
					});
				}
			
				//单击确定
				$('#btnOk').click(function(){
					
					var ids = [];
					var ids_name = [];
					$('input[type="checkbox"]:checked').each(function(i, item){
						ids.push($(item).attr('name'));
						ids_name.push($(item).attr('_name'));
					});
					$textarea.attr('_dimtags', ids);
					$textarea.attr('_dimnames', ids_name);
					
					$('#btnCancel').click();
				});
				
				//单击取消
				$('#btnCancel').click(function(){
					parent.layer.close(parent.__kbs_layer_index);
				});
			});
		</script>
	</head>

	<body>
		<div class="easyui-tabs" style="margin:10px;height: 200px;">
	        <c:forEach items="${requestScope.dimList}" var="dim">
	        	<div title="${dim.name }" style="padding:10px;">
	        		<c:forEach items="${dim.dimTags}" var="dimTag">
	        			<input type="checkbox" name="${dimTag.id }" value="${dimTag.tag }" _name="${dimTag.name }">&nbsp;${dimTag.name }
	        		</c:forEach>
	        	</div>
	        </c:forEach>
	    </div>
	    
	    <div style="margin: 0px 12px;float: right;">
	    	<button class="btn btn-default" id="btnOk">确定</button>
	    	<button class="btn btn-default" id="btnCancel">取消</button>
	    </div>
	</body>
</html>
