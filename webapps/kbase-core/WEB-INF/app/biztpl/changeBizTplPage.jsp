<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>选择业务模板</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<style type="text/css">
		html,body{margin:0px;height: 100%;}
	</style>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/cssChangeBizTpl.css">
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
	
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		$.extend($.fn, {
			getRootPath : function(){
			    return "${pageContext.request.contextPath }";
			}
		});
	</script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/biztpl/js/biztplCate.js"></script>
	<script type="text/javascript">
		var articleId = '${articleId}';
		$(function(){
			var hasParent = '${hasParent}';//window.parent.location.href.indexOf("app/biztpl/article!index.htm") > 0;
			if(hasParent=="false"){
				$("#makeCancelBiztpl").hide();
			}
			//根据页面其他元素高度设置模板列表区域高度
			$("[name='autoHeight']").css("height",$(window).height()-$("[class='dialog-f']").outerHeight()-$("[class='dialog-title']").outerHeight()-$("[class='dialog-search']").outerHeight()-15+"px");
			//确定选择模板
			$("#makeSureBiztpl").on("click",function(){
				var articleName = $("[name='articleName']").val();
				articleName = $.trim(articleName);
				var selectBiztplId = $("#selectBiztplId").val();
				var url = "";
				var makeSure = false;
				if(articleName == ""){
					alert("请填写文章名称");
					return false;
				}
				if(selectBiztplId == ""){
					alert("请选择业务模板");
					return false;
				}
				if(articleId == ''){//新建业务或者更换模板
					if(hasParent!="false"){
						articleId = $(window.parent.document).find("[name='articleId']").val();
						if(articleId == undefined){
							articleId = "";
						}
					}
					if(articleId == ''){
						//校验文章名
						$.ajax({
							type: "POST",
							async: false,
							url:"${pageContext.request.contextPath}/app/biztpl/article!checkByName.htm",
							data: {articleName: articleName},
							dataType:"json",
							success: function(data) {
								if(data.status != 0){
									makeSure = false;
									if(data.status == 1){
										alert("文章名称[" + articleName + "]已存在");
									}else{
										alert("操作失败");
									}
								}else{
									makeSure = true;
								}
							}
						});
					}else{
						makeSure = true;
					}
				}else{//旧实例套用模板
					//if (confirm("该实例没有文章，是否新建业务文章？")){
						makeSure = true;
						url = "${pageContext.request.contextPath}/app/biztpl/article!applyBizTpl.htm?";
					//}
				}
				if(!makeSure){
					return false;
				}
				if(url != ""){
					var param = "tplId="+selectBiztplId+"&categoryId=${categoryId}&articleId="+articleId+"&articleName="+articleName;
					window.location.href = url + param;
				}else{
					if(articleId == ""){//换模板时未保存
						var tplId = selectBiztplId;
						if(hasParent!="false"){
							var categoryId = $(window.parent.document).find("[name='categoryId']").val();
							var categoryName = $(window.parent.document).find("[name='categoryName']").val();
							if(categoryId == undefined){
								categoryId = $("#categoryId").val();
							}
							if(categoryName == undefined){
								categoryName = $("#categoryName").val();
							}
							var param = "tplId="+tplId+"&categoryId="+categoryId+"&categoryName="+categoryName+"&articleName="+articleName;
							window.parent.location.href = '${pageContext.request.contextPath}/app/biztpl/article!index.htm?' + param;
						}else{
							var categoryId = $("#categoryId").val();
							var categoryName = $("#categoryName").val();
							var param = "tplId="+tplId+"&categoryId="+categoryId+"&categoryName="+categoryName+"&articleName="+articleName;
							window.location.href = '${pageContext.request.contextPath}/app/biztpl/article!index.htm?' + param;
						}
					}else{//换模板时已保存
						//更换模板
						$.ajax({
							type: "POST",
							dataType : 'json',
							url: $.fn.getRootPath() + '/app/biztpl/article!changeBiztpl.htm',
							async: false,
							data: "biztplId="+selectBiztplId+"&articleId="+articleId,
							success: function(data){
								if(data.status == 0){
									alert("操作成功!");
									var param = "articleId="+articleId+"&articleName="+articleName;
									window.parent.location.href = '${pageContext.request.contextPath}/app/biztpl/article!index.htm?' + param;
								}else{
									alert("操作失败!");
								}
							}
						});
					}
				}
			});
			//搜索
			$("#type-search").on("click",function(){
				var name = $("#card-search-input").val();
				showBiztplList("",name);
			});
			//自定义
			$("#addZdy").on("click",function(){
				window.open($.fn.getRootPath() + "/app/biztpl/biz-tpl!edit.htm");
			});
			//模板点击选中事件
			$("#biztplList").on("click","[name='biztplRadio']",function(){
				$("#selectBiztplId").val($(this).attr("_id"));
			});
			//模板点击查看详情
			$("#biztplList").on("click","[name='showBiztplInfo']",function(){
				var _url = $.fn.getRootPath() + "/app/biztpl/biz-tpl!showBizTplAttrs.htm?id="+$(this).attr("_id");
				//打开编辑页面所在iframe
				$("#openShowBiztplIframe")[0].src = _url;
				$("#openShowBiztplDiv").dialog("open");
			});
		});
		//显示模板列表
		function showBiztplList(id,name){
			var param = {};
			if(id != null && id != ""){
				param["id"] = id;
			}
			if(name != ""){
				param["name"] = name;
				$.fn.zTree.getZTreeObj("treeDemo").cancelSelectedNode();
			}
			$("#biztplList").empty();
			$.ajax({
				type: "POST",
				dataType : 'json',
				url: $.fn.getRootPath() + '/app/biztpl/biz-tpl!biztplListByCateId.htm',
				async: false,
				data: param,
				success: function(data){
					var html = "";
					$(data).each(function(i){
						html += '<div>';
						html += '<input type="radio" name="biztplRadio" _id="'+this.id+'" style="cursor: pointer;">';
						html += '<em title="' + this.name + '" style="cursor: pointer;">' + (this.name.length > 15 ? (this.name.substring(0,15)+"...") : this.name) + '</em>';
						html += '<i name="showBiztplInfo" _id="'+this.id+'" style="cursor: pointer;"></i>';
						html += '</div>';
					});
					$("#biztplList").append(html);
				}
			});
		}
		//加载模板列表数据
		function loadData(){
			var ztree = $.fn.zTree.getZTreeObj("treeDemo");
			if(ztree != null){
				var node = ztree.getSelectedNodes()[0];
				if(node != null){
					showBiztplList(node.id,"");
				}
			}
		}
	</script>
</head>
<body style="overflow: hidden;">
	<div class="easyui-layout" style="width:100%;height:100%;border: 0px;">
		<div data-options="region:'north'" style="height:130px;border: 0px;overflow: hidden;">
			<div class="dialog-title">
				<div class="dialog-title-span">
					<input type="hidden" id="selectBiztplId" value="">
					<input type="hidden" id="categoryId" value="${categoryId }">
					<input type="hidden" id="categoryName" value="${categoryName }">
					<span>文章名称:</span><input type="text" name="articleName" value="${articleName }"><em>*</em>
				</div>
				<span class="dialog-title-inner">
					选择业务模板
				</span>
				<!-- 查看模板属性页面 -->
				<div id="openShowBiztplDiv" class="easyui-dialog" closed="true" modal="true" title="模板详情" 
					style="width:450px;min-height:250px;height:450px;overflow: hidden;">
					<iframe scrolling="auto" id='openShowBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
				</div>
			</div>
			<div class="dialog-search">
				<span>模板目录</span>
				<label>搜索模板<input type="text" placeholder="输入模板名称" value="" id="card-search-input"> 
				<input class="type-search" type="button" value="搜索" id="type-search"></label>
			</div>
		</div>
		<div data-options="region:'center'" style="border: 0px;border-top: 1px solid #E5E5E5;">
			<div class="dialog-content" style="height: 100%;">
				<div class="con_">
					<div class="con_right">
						<div id="card-div-2" class="card-div" style="height: 250px;min-height: 200px;overflow-y:auto;" name="autoHeight">
							<ul class="card-ul">
								<li id="biztplList">
								</li>
							</ul> 
						</div>
					</div>
					<div class="con_left">
						<div style="height: 250px;overflow-y:auto;min-height: 200px;" name="autoHeight">
							<ul id="treeDemo" class="ztree"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-options="region:'south'" style="height:80px;border: 0px;">
			<div class="dialog-f" style="overflow: hidden;">
				<span id="addZdy" style="margin-left: 10px;width: 120px;"><i class="ico-zdy"></i>添加自定义</span>
				<div id="makeSureBiztpl" class="dialog-b dialog-b-l">确定</div>
				<div id="makeCancelBiztpl" onclick="window.parent.closeIframe();" class="dialog-b dialog-b-l-2">取消</div>
			</div>
		</div>
	</div>
</body>
</html>