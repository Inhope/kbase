<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="UTF-8">
	<title>列表</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/form.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
	<script>
		$(function(){
			$('.wz_title a').tab();
			$('.absolute').find('button:nth-child(3)').click(function(){
				$('.wz_form').find('.none').fadeToggle(400);
			});
		})
		var pageContext = {
			contextPath : '${pageContext.request.contextPath }'
		}
		var searchType = '${searchType}';
	</script>
	<style>
		.wz_mb{
			width:100%;
			margin:0px auto ;
			font-size:14px;
			font-family:"宋体";
		}
		.wz_form{
			margin:8px auto;
		}
		.wz_form > ul{
			height:100%; 
			margin:0 auto; 
			*padding-bottom:10px;
			overflow: hidden;
			margin-right:200px;
		}
		.wz_form > ul > li{
			width:33.3%;
		    float: left;
			height:28px;
			line-height:28px;
			margin:6px 0;
		}
		.wz_form label{
			width:70px;
			height:26px;
			color:#666;
			font:13px/26px "宋体";
			margin-top:1px;
			white-space: pre-wrap;
			text-align:center;
			text-indent:6px;
			float:left;
		}	
		.wz_form > ul > li > div{
			border:1px solid #ccc;
			height: 26px;
			line-height: 26px;
			position:relative;
			text-align:center;
			margin-left:70px;
		}
		.wz_form > ul > li > div > i{
			display: inline-block;
			width:19px;
			height:20px;
			position: absolute;
			top:3px;
			right:2px;
			background: url(../../resource/biztpl/images/ico-date.png) no-repeat  center center
		}
		.wz_form input[type="text"]{
			width:80%;
			height: 24px;
			line-height: 24px;
			color:#388EED;
			padding:0px 5% 0px 0%;
			border:0;
			outline: 0;
			background:transparent;
		}
		.none{
			display: none
		}
		.absolute{
			float:right;
			width:250px;
			text-align:center;
			margin:6px auto;
		}
		.absolute button{
			border: 1px solid #999;
			width:auto;
			background:white;
			font:12px/22px '宋体';
			height:28px;
			margin:0 2px;
			padding:0 6px;
			color:#666;
			border-radius: 4px;
			cursor:pointer;
			overflow:visible;
			outline:none;
			filter:chroma(color=#000000);
		}
		button:hover{
			background:#3FC6BF;
			color:white;
			border:1px solid #3FC6BF;
		}
		button.red_wz{
			color:white;
			background:red;
			border:1px solid red;
		}
		button.red_wz:hover{
			color:white;
			background:red;
			border:1px solid red;
		}
		button.green_wz{
			color:white;
			background:#3FC6BF;
			border:1px solid #3FC6BF;
		}
		
		.wz_date{
			width:78%!important;
			padding:0px 17% 0px 5%!important;
		}
		.wz_mb table{
			width:100%;
			color:#444;
			text-align:center;
			border-collapse:collapse;
		}
		.wz_mb>table>thead th{
			font-size:14px;
			height:36px;
			line-height:36px;
			font-weight:normal;
			color:white;
			background:#3FC6BE;
		}
		.wz_mb>table td{
			width:auto;
			height:32px;
			line-height:32px;
			font-size:14px;
			border-top:1px solid #ccc;
			border-bottom:1px solid #ccc;
		}
		.wz_mb>table td.title{
			text-align:left;
			cursor: pointer;
		}
		.wz_mb>table td i{
			display: inline-block;
			vertical-align: middle;
			background:url(../../resource/biztpl/images/ico-w2.png) no-repeat center center;]
			width:auto;
			height:100%;
			padding:0 14px;
			margin: -3px -8px 0 0;
			cursor:pointer;
		}
		.wz_mb>table td input{
			display: inline-block;
			vertical-align:middle;
			margin-right:5px;
			width:14px;
			height:14px;
		}
		.wz_fy{
			width:100%;
			height:100%;
			padding:15px 0;
			text-align:center;
			overflow: hidden;
		}
		.wz_fy li{
			display: inline-block;
			*display:inline;
			*zoom:1;
			width:auto;
			height:28px;
			line-height:28px;
			font-size:14px;
			margin:0 5px;
			border:1px solid #C2DCF9;
		}
		.wz_fy li a{
			display: block;
			width:auto;
			height:28px;
			line-height:28px;
			font-size:14px;
			padding:0 15px;
			color:#4A74BA;
		}
		li.on a {
		    background: #3fc6be;
		    color: white;
		}
		li.disabled a{
			border:none;
		    background-color: #F5F5F5;
		    color:#ACA899;
		    cursor:default;
		    text-decoration:none;
		}

		.wz_title{
			width:100%;
			background:#f6f6f6; 
			height:36px;
			line-height:36px; 
			float:left;
		}
		.wz_title > a{
			display:inline-block; 
			font:14px/22px "ו"; 
			margin:9px 30px 0; 
			padding:2px 8px; 
			color:#333; 
			float:left;
			outline:none;
			text-decoration:none;
		}
		a:hover { 
			color:#CC6600;
		}
		.wz_mb a:focus{
			outline:none;
		}
	</style>
</head>
<body>
	<div class="wz_mb">
		<form action="${pageContext.request.contextPath }/app/biztpl/article!list.htm" method="post">
			<div class="wz_form">
				<div class="absolute">
					<button class="green_wz" type="submit">搜索</button><button id="reset">重置</button><button id="gaoji" type="button">高级</button><button id="relieve" type="button" class="red_wz">解除绑定</button>
				</div>
				<ul style="position: relative;">
					<li><label>文章名称</label><div><input name="name" type="text" value="${name }"/></div></li>
					<li><label>开始时间</label><div><input id="startDate" name="startDate" type="text" class="wz_date" 
						value="<fmt:formatDate value="${startDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})"/><i></i></div></li>
					<li><label>结束时间</label><div><input id="endDate" name="endDate" type="text" class="wz_date" 
						value="<fmt:formatDate value="${endDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd'})"/><i></i></div></li>
	              	<li class="none"><label>文章标签</label><div><input name="label" type="text" value="${label }" /></div></li> 
	              	<li class="none"><label>语 义 块</label><div><input name="semantic" type="text" value="${semantic }" type="text" /></div></li>
					<li class="none"><label>追 溯 期</label><div><input name="traceDate" type="text" class="wz_date" 
						value="<fmt:formatDate value="${traceDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({minDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})"/><i></i></div></li>
				</ul>
				<input name="start" type="hidden" value=""/>
				<input name="limit" type="hidden" value=""/>
				<input name="username" type="hidden" value="${username}"/>
				<input name="categoryId" type="hidden" value="${categoryId}"/>
				<input name="searchType" type="hidden" value="${searchType}"/>
			</div>
		</form>
		<div class="wz_title">
        	<a href="javascript:void(0)">全部</a><a href="javascript:void(0)">实例</a><a href="javascript:void(0)">文章</a>
         </div>
		<table border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr><th></th>
					<th width="20%" style="text-align: left;min-width: 250px;">文章标题</th>
					<th width="15%">类型</th>
					<th width="15%">创建人</th>
					<th width="15%">创建时间</th>
					<th >有效期</th>
					<th width="15%">追溯期</th>
				</tr>	
			</thead>
			<tbody>
				<c:forEach items="${requestScope.list}" var="article">
					<tr id="${article.id }" tplId="${article.tplId }">
						<td style="text-align: center;"><input type="checkbox" name="check_list"/></td>
						<td width="20%" name="articleName" class="title" title="${article.name }" style="text-align: left;min-width: 250px;">
							<c:choose>
								<c:when test="${fn:length(article.name) > 16}">
									${fn:substring(article.name,0,16)}...
								</c:when>
								<c:otherwise>
									${article.name }
								</c:otherwise>
							</c:choose>
						</td>
						<td width="15%">
							<c:choose>
							   <c:when test="${not empty article.tplId}">
							   		文章
							   </c:when>
							   <c:otherwise>
							   		实例
							   </c:otherwise>
							</c:choose>
						</td>
						<td width="15%">${article.createUser }</td>
						<td  width="15%"><fmt:formatDate value="${article.createDate }" type="date" pattern="yyyy-MM-dd"/></td>
						<td width="15%"><fmt:formatDate value="${article.startDate }" type="date" pattern="yyyy-MM-dd"/>-<fmt:formatDate value="${article.endDate }" type="date" pattern="yyyy-MM-dd"/>
						</td>
						<td width="15%"><fmt:formatDate value="${article.traceDate }" type="date" pattern="yyyy-MM-dd"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<ul class="wz_fy">
			<c:choose>
			   <c:when test="${requestScope.arrayPage.pageNo == 1}">
			   		<li class="disabled"><a href="javascript:void(0)" onfocus="this.blur();">首页</a></li>
			   </c:when>
			   <c:otherwise>
			   		<li><a href="javascript:void(0)" onfocus="this.blur();" start="0" limit="${arrayPage.pageSize}">首页</a></li>
			   </c:otherwise>
			</c:choose>
			
			<c:choose>
			   <c:when test="${requestScope.arrayPage.pageNo == 1}">
			   		 <li class="disabled"><a href="javascript:void(0)" onfocus="this.blur();">&laquo;</a></li>
			   </c:when>
			   <c:otherwise>
			   		 <li><a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.previousPageStart}" limit="${arrayPage.pageSize}">&laquo;</a>
			   		 </li>
			   </c:otherwise>
			</c:choose>
			
			<c:forEach  begin="1" end="${requestScope.arrayPage.pageTotalNo}" varStatus="i" var="info">
				<c:choose>
					<c:when test="${(i.index) + 4 > requestScope.arrayPage.pageNo && (i.index) - requestScope.arrayPage.pageNo < 5}">
						<c:choose>
							<c:when test="${(i.index) == requestScope.arrayPage.pageNo}">
								<li class="on"><a href="javascript:void(0)" onfocus="this.blur();">${info}</a></li>
							</c:when>
							 <c:otherwise>
								<li><a href="javascript:void(0)" onfocus="this.blur();" start="${(i.index - 1) * requestScope.arrayPage.pageSize }" limit="${arrayPage.pageSize}">${info}</a></li>
							 </c:otherwise>
						</c:choose>
			   		</c:when>
				</c:choose>
					
		  	</c:forEach>
		  	
		  	
			
			
			<c:choose>
			   <c:when test="${requestScope.arrayPage.pageNo == requestScope.arrayPage.pageTotalNo}">
			   		 <li class="disabled"><a href="javascript:void(0)" onfocus="this.blur();">&raquo;</a></li>
			   </c:when>
			   <c:otherwise>
			   		 <li><a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.nextPageStart}" limit="${arrayPage.pageSize}">&raquo;</a>
			   		 </li>
			   </c:otherwise>
			</c:choose>
			
		    
		    <c:choose>
			   <c:when test="${requestScope.arrayPage.pageNo == requestScope.arrayPage.pageTotalNo}">
			   		<li class="disabled"><a href="javascript:void(0)" onfocus="this.blur();">尾页</a></li>
			   </c:when>
			   <c:otherwise>
			   		<li><a href="javascript:void(0)" onfocus="this.blur();" start="${arrayPage.lastPageStart}" limit="${arrayPage.pageSize}">尾页</a></li>
			   </c:otherwise>
			</c:choose>
		</ul>
	</div>
</body>
</html>