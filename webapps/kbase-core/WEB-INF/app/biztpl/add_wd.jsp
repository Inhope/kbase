<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.domain.biztpl.BizTplGroup"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>维度选择</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
		<style>
		.wd_tab{
			width:545px;
			height:410px;
			color:#333;
			background-color:#f2f2f2;
			z-index: 9999;
		}
		.wd_tab_title{
			height:42px;
			line-height:52px;
			font-size:15px;
			padding-left:15px;
		}
		i.wd_tab_close{
			float:right;
			width:26px;
			height:26px;
			background:url(images/big-close.png)no-repeat center center;
			background-size:12px 12px;
			cursor:pointer;
		}

		.tabs_ul{
			width:100%;
			height:32px;
			text-align:center;
			font-size:0;
		}
		.tabs_ul li{
			width:auto;
			display: inline-block;
			margin:0 15px;
		}
		.tabs_ul li a{
			display: block;
			font:12px/27px "宋体";
			border-bottom:5px solid #f2f2f2;
		}
		.tabs_ul li a:hover{
			color:#6484D9;
			text-decoration: none;
			border-bottom:5px #6484D9 solid ;
		}
		.tabs_ul2{
			width:100%;
			height:100%;
			background:#fff;
			border-top:1px #ADADAD solid;
			border-bottom:1px #ADADAD solid;
		}
		.tabs_ul2>li{
			height:100%;
			display: none;
		}
		.tabs_ul2 label{
			width:auto;
			display: inline-block;;
			margin:0 15px;
			word-break: break-all;
			word-wrap: break-word;
			font-size:12px;
		}
		.tabs_ul2 label input{
			margin-right:5px;
			vertical-align: middle;
		}
		.tabs_ul_level3{
			width:100%;
			height:272px;
			overflow: hidden
		}
		.tabs_ul_level3 li{
			float:left;
			height:32px;
			line-height:32px;
		}
		.tabs_ul_level3 li.first-child{
			height:100%;
			float:left;
			width:150px;
			text-align:center;
			background:#f2f2f2;
			padding-top:100px;
		}
		.tabs_ul_level3 li.last-child{
			width:395px;
			height:100%;
		}
		.label{
			width:94%;
			margin:15px auto;
			height: 250px;
			overflow-y:auto
		}
		.button{
			clear:both;
			text-align:center;
			line-height:62px;
		}
		.button button{
			display: inline-block;
			width:78px;
			height:32px;
			font:14px/32px "宋体";
			text-align:center;
			background:#fff;
			border:1px #adadad solid;
			border-radius:3px;
			margin-right:10px;
			cursor:pointer;
		}
		.msg{
			color: red;
			font-size: 12px;
			margin-left: 100px;
		}
	</style>
		<script>
			$(function(){
				$('.tabs_ul2 li').first().css('display','block');
				$('.tabs_ul').find('a').click(function(){
					var _index = $('.tabs_ul').find('a').index(this);
					$('.tabs_ul2').children('li').eq(_index).css('display','block').siblings().css('display','none');
				})
			})
		</script>
	</head>
<body>
	<div class="wd_tab">
		<div class="wd_tab_title">
			选择维度<span class="msg">维度不选默认为全维度</span>
		</div>
		<div class="wd_tab_c">
			<div class="wd_tab_c">
				<ul class="tabs_ul">
					<c:forEach items="${requestScope.dimList}" var="dim">
						<li><a href="javascript:void(0)">${dim.name }</a></li>
					</c:forEach>
				</ul>
			</div>
			<div class="wd_tab_label">
				<ul class="tabs_ul2">
				   <c:forEach items="${requestScope.dimList}" var="dim">
						<li>
							<ul class="tabs_ul_level3">
								<li class="first-child"><label><input type="checkbox" name="" id="" code="${dim.code }" _type="all" <c:if test="${attr_kbs == true && dim.code == 'platform'}">disabled="disabled"</c:if>>全部${dim.name }</label></li>
								<li class="last-child">
									<div class="label">
										<c:forEach items="${dim.dimTags}" var="dimTag">
											<label><input type="checkbox" name="${dimTag.id }" value="${dimTag.tag }" _name="${dimTag.name }" code="${dimTag.dim.code }" 
												<c:if test="${attr_kbs == true && dim.code == 'platform' &&  dimTag.tag != 'kbase'}">disabled="disabled"</c:if>
											/>${dimTag.name }</label>
										</c:forEach>
									</div>
								</li>
							</ul>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="button">
			<button id ="ok">确定</button>
			<button id="cancel">取消</button>
		</div>
	</div>
</body>
</html>