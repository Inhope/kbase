<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>业务模板选择</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
		</style>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		
		<script type="text/javascript">
			var articleId = '${articleId}';
			function showBody(){
				if(articleId != ''){
					if (confirm("该实例没有文章，是否新建业务文章？")){
						$("body #showOrHide").show();
					}
				}else{
					$("body #showOrHide").show();
				}
			}
			$(function(){
				/*初始化树的高度*/
				$('#treeDemo').css({
					width:$('body').width()-20, 
					height: $('body').height()-$("div[name='operateDiv']").height()
				});
				//初始化模板树
				var setting = {
					data: {
						simpleData: {
							enable: true
						}
					},
					check: {
						enable: true,
						chkStyle: "radio",
						radioType: "all"
					}
				};
				var zNodes = ${requestScope.bizTpls };
				var $ztree = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
				//新增文章-确定
				$('#btnOk').click(function(){
					var checkPass = true;
					var articleName = $("[name='articleName']").val();
					//校验文章名
					$.ajax({
						type: "POST",
						async: false,
						url:"${pageContext.request.contextPath}/app/biztpl/article!checkByName.htm",
						data: {articleName: articleName},
						dataType:"json",
						success: function(data) {
							if(data.status == 0){
							}else if(data.status == 1){
								checkPass = false;
								alert("文章名称[" + articleName + "]已存在");
							}else{
								checkPass = false;
								alert("操作失败");
							}
						}
					});
					if(!checkPass){
						return false;
					}
					var nodes = $ztree.getCheckedNodes(true);
					if (nodes.length==0){
						alert('请选择业务模板');
						return false;
					}
					var selectedNode = nodes[0];
					var param = "tplId="+selectedNode.id+"&categoryId=${categoryId}&categoryName=${categoryName}&articleId="+articleId+"&articleName="+articleName;
					if(articleId != ''){
						window.location.href = '${pageContext.request.contextPath}/app/biztpl/article!applyBizTpl.htm?' + param;
					}else{
						window.location.href = '${pageContext.request.contextPath}/app/biztpl/article!index.htm?' + param;// + '&articleName=' + encodeURI(articleName));
					}
				});
			});
		</script>
	</head>

	<body style="overflow: hidden;" onload="showBody();">
	<div style="display: none;" id="showOrHide">
		<input type="hidden" id="categoryId" value="${categoryId }">
		<input type="hidden" id="categoryName" value="${categoryName }">
		<input type="hidden" id="articleId" value="${articleId }">
		<div name="operateDiv" style="width: 100%;margin-left: 10px;">
			<label style="">文章名称:</label>
			<input type="text" name="articleName" value="${articleName }" style="width: 200px;margin-bottom:10px;margin-top:10px;margin-right: 50px;">
			<button class="btn btn-default" id="btnOk" style="margin-right: 10px;">确定</button>
		</div>
		<div id="treeDemo" class="ztree" style="overflow: auto;"></div>
	</div>
	</body>
</html>
