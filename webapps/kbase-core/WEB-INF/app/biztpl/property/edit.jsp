<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<title>编辑属性</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			.link-btn-select{
				background: #e6e6e6 none repeat scroll 0 0;
				border: 1px solid #ddd;
				color: #444;
				filter:none;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript">
			$.extend($.fn, {
				getRootPath : function(){
				    return "${pageContext.request.contextPath }";
				}
			});
			var defaultDataType = "${bizTplProperty.dataType}";
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/biztpl/js/biztplPropertyEdit.js"></script>
	</head>

	<body style="margin: 0;overflow-x: hidden;">
		<div class="easyui-panel" title="" style="width:100%;height:130px;border: 0px 0px 1px 0px;padding: 10px;">
			<input id="id" type="hidden" value="${bizTplProperty.id }">
			<label>属性名称:</label>
			<input id="name" style="width:200px;border: 1px solid #DDDDDD;margin-bottom: 5px;" value="${bizTplProperty.name }"><br>
			<label>是否启用:</label>
			<select id="status" style="border: 1px solid #DDDDDD;margin-bottom: 5px;margin-right: 10px;">
				<option value="1" ${bizTplProperty.status==1?"selected":"" }>否</option>
				<option value="0" ${bizTplProperty.status==0?"selected":"" }>是</option>
			</select>
			<label>是否搜索:</label>
			<select id="isSearch" style="border: 1px solid #DDDDDD;margin-bottom: 5px;margin-right: 10px;">
				<option value="1" ${bizTplProperty.isSearch==1?"selected":"" }>否</option>
				<option value="0" ${bizTplProperty.isSearch==0?"selected":"" }>是</option>
			</select>
			<!-- <label>是否必填:</label>
			<select id="required" style="border: 1px solid #DDDDDD;margin-bottom: 5px;margin-right: 10px;">
				<option value="1" ${bizTplProperty.required==1?"selected":"" }>否</option>
				<option value="0" ${bizTplProperty.required==0?"selected":"" }>是</option>
			</select> --><br>
			<label>属性类型:</label>
			<select id="dataType" style="border: 1px solid #DDDDDD;">
				<c:forEach var="item" items="${dataTypeList}">
					<option value="${item.key }" ${bizTplProperty.dataType==item.key?"selected":"" }>${item.value }</option>
				</c:forEach>
			</select><br>
		</div>
		<div id="prop_tag" class="easyui-panel" title="属性值" style="width:100%;height:auto;padding:5px;min-height: 280px;"
				data-options="collapsible:true,closed:true"><!-- minimizable:true,maximizable:true,closable:true -->
			<div style="width: 100%;float: left;">
				<div style="width: 100%;">
					<a id="btnAddTag" href="javascript:void(0)" class="easyui-linkbutton" 
						data-options="iconCls:'icon-add',plain:true">添加</a>
				</div>
				<table style="width: 100%;">
					<tr>
						<td style="width: 50%;vertical-align: top;">
							<table id="valList">
								<!-- 属性值区域 -->
								<c:forEach items="${bizTplProperty.propertyTags }" var="object">
									<tr id="${object.id }" name="row_tr">
										<td>
											<input name="prop_tag_name" childNames='${object.childrenJSON}' style="width:200px;border: 1px solid #DDDDDD;" value="${object.name }" id="${object.id }" dataTypeChild="${object.dataType }">
										</td>
										<td>
											<a name="btnDelTag" href="javascript:void(0);" class="easyui-linkbutton">删除</a>
										</td>
										<td>
											<a name="btnRelateData" href="javascript:void(0);" class="easyui-linkbutton">关联数据</a>
										</td>
									</tr>
								</c:forEach>
							</table>
						</td>
						<td style="width: 50%;vertical-align: top;">
							<div style="width:330px;float: left;">
								<div id="prop_tag_child" class="easyui-panel" title="编辑关联数据" style="width:100%;height:auto;padding:5px;"
										data-options="closable:true,closed:true">
									<div>
										数据类型:
										<select id="data_type_child" style="border: 1px solid #DDDDDD;">
											<option value="select_choices">下拉列表</option>
											<option value="multiple_choices">复选列表</option>
										</select>
										<a id="btnAddTag_child" href="javascript:void(0)" class="easyui-linkbutton" 
											data-options="iconCls:'icon-add',plain:true"></a>
										<a id="btnDelTag_child" href="javascript:void(0)" class="easyui-linkbutton" 
											data-options="iconCls:'icon-remove',plain:true"></a>
										<!-- <a id="btnSaveTag_child" href="javascript:void(0)" class="easyui-linkbutton" 
											data-options="iconCls:'icon-ok',plain:true"></a> -->
									</div>
									<table id="valList_child">
										<!-- 关联数据区域 -->
									</table>
									<!-- 关联数据模板 -->
									<table id="relateTemplet" style="display: none;">
										<tr name="row_relate">
											<td>
												<input type="checkbox">
											</td>
											<td>
												<input name="childName" style="width:200px;border: 1px solid #DDDDDD;">
											</td>
										</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<!-- 属性值模板 -->
				<table id="propTagTemplet" style="display: none;">
					<tr name="row_tr">
						<td>
							<input name="prop_tag_name" childNames="{}" style="width:200px;border: 1px solid #DDDDDD;">
						</td>
						<td>
							<a name="btnDelTag" href="javascript:void(0);" class="easyui-linkbutton">删除</a>
						</td>
						<td>
							<a name="btnRelateData" href="javascript:void(0);" class="easyui-linkbutton">关联数据</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div style="width: 100%;text-align: right;">
			<a id="saveDo" href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" style="margin: 10px;">保存</a>
		</div>
	</body>
</html>
