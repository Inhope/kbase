<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<title>属性管理</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/js/My97DatePicker/skin/WdatePicker.css"/>
		<style type="text/css">
			.moveStyle{
				color: blue;
				cursor: pointer;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$.extend($.fn, {
				getRootPath : function(){
				    return "${pageContext.request.contextPath }";
				}
			});
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/biztpl/js/biztplProperty.js"></script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'center',title:false,iconCls:'icon-ok'" style="background:#eee;">
			<div id="now-toolbar" style="padding:5px;height:auto">
				属性名称：
				<input id="name" style="width:200px;border: 1px solid #DDDDDD;">
				状态:
				<select id="status" style="border: 1px solid #DDDDDD;">
					<option value="">--全部--</option>
					<option value="0">启用</option>
					<option value="1">停用</option>
				</select>
				创建日期:
				<input id="startDate" type="text" style="width:90px;border: 1px solid #DDDDDD;" 
					onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})" readonly="readonly">
				~
				<input id="endDate" type="text" style="width:90px;border: 1px solid #DDDDDD;" 
					onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd'})" readonly="readonly">
				<a id="btnSearch" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search">查询</a>
				<a id="btnReset" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear">重置</a>
			    <a id="btnAdd" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>
			    <a id="btnEdit" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</a>
			    <a id="btnDel" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
			</div>
	    	<table id="dataList" class="easyui-datagrid" data-options="url:'${pageContext.request.contextPath }/app/biztpl/biz-tpl-property!dataList.htm',
	    		fitColumns:true,singleSelect:false,rownumbers:true,fit:true,toolbar:'#now-toolbar',pagination:true">
			    <thead>
			        <tr>
			        	<th data-options="field:'id',checkbox:true,width:'4%'"></th>
			            <th data-options="field:'name',width:'26%'">属性名称</th>
			            <th data-options="field:'dataType',width:'18%'">属性类型</th>
			            <th data-options="field:'status',width:'18%',formatter:function(value,row,index){return value==1?'停用':'启用';}">状态</th>
			            <th data-options="field:'createDate',width:'18%',
							formatter:function(value,row,index){return fmtDate(value);}">创建时间</th>
			            <th data-options="field:'null',width:'16%',
			            	formatter:function(value,row,index){return operateFn(value,row,index);}">操作</th>
			        </tr>
			    </thead>
			</table>
	    </div>
		<!-- 编辑页面 -->
		<div id="openEditPropDiv" class="easyui-dialog" closed="true" modal="true" title="编辑属性" style="width:750px;min-height:300px;height:100%;overflow: hidden;">
			<iframe scrolling="auto" id='openEditPropIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
	</body>
</html>