<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.shl{
			width:628px;
			height:auto;
			background:#F1F1F1;
			overflow: hidden;
			position:absolute;
			z-index:2;
		}
		.shl h2{

			height:50px;
			line-height:60px;
			text-indent:2em;
			font-size:13px;
			color:#333;
		}
		.shl h2 a.shl_close{
			display: inline-block;
			background: url(${pageContext.request.contextPath}/resource/biztpl/images/big-close.png) no-repeat 60% center;
			float:right;
			width:40px;
			height:30px;
			background-size:12px 12px;
			cursor:pointer;
		}
		.shl h2 em{
			display: inline-block;
			vertical-align: bottom;
			float:right;
			margin-right:35px;
			width:auto;
			height:50px;
			line-height: 60px;
			background:url(${pageContext.request.contextPath}/resource/biztpl/images/ico-12.png) no-repeat 5px 18px;
			background-size:18px 18px;
			cursor:pointer;
		}
		.shl_con{
			height:100%;
			padding:0px 6px;
		}
		.shl_con_l{
			width:180px;
			height:100%;
			text-indent:10px;
			border:1px solid #aeaeae;
			background:white;
			min-height:300px;
			float:left;
		}
		.shl_con_l ul{
			clear:both;
			padding:8px 0;
		}
		/*.shl_con_l li{*/
			/*height:28px;*/
			/*line-height:28px;*/
			/*font-size:13px;*/
			/*font-weight:bold;*/
		/*}*/
		.shl_con_l input{
			margin-top:5px;
			margin-left:5px;
			outline: 0;
			float:left;
		}

		.shl_con_l input[type=text]{
			width:110px;
			padding: 4px;
			display: inline-block;
			-webkit-border-radius: 2px;
			border: 1px #8aa8ca solid;
		}
		.shl_con_l input[type=button]{
			display: inline-block;
			vertical-align: top;
			cursor: pointer;
			background: #eef6ff;
			text-align: center;
			width:45px;
			height:26px;
			line-height:22px;
			border-radius: 2px;
			font-size: 13px;
			border: 1px #8aa8ca solid;
		}
		.shl_con_r{
			width:426px;
			height:100%;
			float:left;
			margin-left:6px;
			position:relative;
		}
		table{
			border-collapse:collapse;
			text-align:center;
			width:100%;
			background:white;
		}
		table td{
			height:18px;
			line-height:18px;
			font-size:13px;
			color:#444;
			border-left:1px solid #ccc;
			border-top:1px solid #ccc;
			padding:5px;
		}

		table td a{
			color:blue;
			text-decoration:underline;
		}
		.table_h thead th{
			background:#f2f2f2;
			color:#333;
			height:32px;
			line-height:32px;
			font-size:14px;
			font-weight:normal;
			border-top:1px solid #ccc;
			border-left:1px solid #ccc;
			font-family:'宋体';
			padding-right:17px;
		}
		.table_b{
			width:100%;
			height:145px;
			overflow-y:scroll;
			overflow-x:hidden;
		}
		.table_h table,.table_b table{
			width:100%;
		}
		.table_b table tr:nth-child(2n+1){
			background-color:#F8F8F8;
		}
		.table_b table td:nth-child(2){
			text-align:left;
		}
		table tr:nth-child(n+1) td:nth-child(1){
			border-right:1px solid #ccc;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
	<%--<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>--%>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/datagrid-scrollview.js"></script>
	<script type="text/javascript">
		//relatedObjects 取自 index.jsp数据,每次添加删除修改他的值
		var objectId = articleId;//取自 index.jsp数据
		var compare = new Array();
		var related = new Array();
		$(function () {
//			console.log(relatedObjects);
			$('a.shl_close').click(function(){
//				console.log(relatedObjects);
				$('div.shl').remove();
			});

			initTable();
			/**
			 * 初始化
			 */
			function initTable(){

				for (var i = 0; i < relatedObjects.length; i++) {
					if (relatedObjects[i].type == 2){
						addNode(relatedObjects[i] ,2);

					} else if (relatedObjects[i].type == 1) {
						addNode(relatedObjects[i],1);
					}
				}
				reloadTable(compare, 'compare');
				reloadTable(related, 'related');
			}
			/**
			 * 加载左侧树
			 */
			$('#tree').tree({
				url : '${pageContext.request.contextPath }/app/biztpl/article!slqlTree.htm',
				method : 'post'
			});
			/**
			 * 对比实例点击事件
			 */
			$('#compare').click(function(){
				var node = selectNode();
				if(node){
					node.type = 1;
					if (hasNode(compare, node)){
						alert('该实例已经存在对比列表')
						return false;
					}
					addNode(node,1);
					reloadTable(compare, 'compare');
				}
			});
			/**
			 * 相关实例点击事件
			 */
			$('#related').click(function(){
				var node = selectNode();
				if(node){
					node.type = 2;
					if (hasNode(related, node)){
						alert('该实例已经存在关联列表')
						return false;
					}
					addNode(node ,2);
					reloadTable(related, 'related');
				}
			});

			/**
			 * 判断是否包含node
			 */
			function hasNode(nodes, node){

				if(!nodes || nodes == null || nodes.length < 1){
					return false;
				}
				for(var i = 0; i < nodes.length; i++) {
					if(nodes[i].id == node.id && nodes[i].type == node.type){
						return true;
					}
				}
				return false;
			}
			/**
			 * 添加节点到数组
			 * @param nodes
			 * @param node
			 */
			function addNode(node, type){
				var n = new Object();
				n.id = node.id;
				n.text = node.text;
				n.type = type;
				if(type == 2) {
					related.push(n);
				} else if (type == 1){
					compare.push(n);
				}

				if(!hasNode(relatedObjects, n)){
					relatedObjects.push(n);
				}
			}


			/**
			 * 获取树的选择节点
			 * @returns {*}
			 */
			function selectNode() {
				if (objectId=='' || objectId == null) {
					alert('未获取到当前实例id,无法完成操作');
					return false;
				}
				var select = $('#tree').tree('getSelected');
				if(!select.objectId || select.objectId == null || select.objectId == ''){
					alert('请选择实例');
					return false;
				}
				return select;
			}
		});
		/**
		 * 删除
		 */
		function del(id,type){
			if(type == 2){
				removeNode(related,id,type);
				reloadTable(related,'related');
			}else if(type == 1){
				removeNode(compare,id,type);
				reloadTable(compare,'compare');
			}
			removeNode(relatedObjects,id,type);
		}
		function removeNode(nodes, id, type){
			var dels = new Array();
			for(var i = 0; i < nodes.length; i++) {
				if(nodes[i].id == id && nodes[i].type == type){
					dels.push(i);
				}
			}
			for(var i = dels.length-1; i >= 0; i--) {
				nodes.splice(dels[i],1);
			}
		}
		/**
		 * 重新加载table数据
		 * @param nodes
		 * @param type
		 */
		function reloadTable(nodes,type){
			if (type == 'compare'){
				var content = '';
				for (var i = 0; i< nodes.length; i++){
					content += '<tr><td>' + (i+1) + '</td>'
							+ '<td>' + nodes[i].text + '</td>'
							+ '<td><a href="javascript:del(\'' + nodes[i].id + '\', 1)">删除</a></td></tr>'
				}
				for (var i = 0; i< 4-nodes.length; i++){
					content += '<tr><td></td><td></td><td></td></tr>'
				}
				$('#compare_table tbody').html(content);
			} else if (type == 'related') {
				var content = '';
				for (var i = 0; i< nodes.length; i++){
					content += '<tr><td>' + (i+1) + '</td>'
							+ '<td>' + nodes[i].text + '</td>'
							+ '<td><a href="javascript:del(\'' + nodes[i].id + '\', 2)">删除</a></td></tr>'
				}
				for (var i = 0; i< 4-nodes.length; i++){
					content += '<tr><td></td><td></td><td></td></tr>'
				}
				$('#related_table tbody').html(content);
			}
		}



	</script>
</head>
<body>
<div class="shl">
	<h2>实例关联 <a class="shl_close" href="javascript:void(0);"></a><em id="compare">添加实例对比</em><em id="related">添加实例关联</em></h2>
	<div class="shl_con">
		<div class="shl_con_l">
			<div style="float:left;">
			<input type="text" placeholder="输入模板名称"> <input type="button" value="搜索">
			</div>
			<div class="easyui-panel" style="float:left;width:180px;height: 260px;border: none;">
			<ul id="tree"></ul>
			</div>
		</div>
		<div title="添加实例对比" data-options="fit:true" style="width:434px;height:300px;float:left;" >
				<table id="dataList" class="easyui-datagrid" title="实例对比"
					data-options="view:scrollview,rownumbers:true,singleSelect:true,
                    queryParams: {tplId : '0000000056f9b8470156fd9a3eca0305',
		  						 articleId :'0000000056fd40040157079c70e2000d'},
					url:'${pageContext.request.contextPath }/app/biztpl/example!exampleListDatas.htm',
                    autoRowHeight:false,pageSize:10
                    ">
					<thead>
					 <tr>
	                    <th data-options="field:'id',width:30,align:'center',checkbox:true"></th>
							<th data-options="field:'name',width:190,align:'center'">实例名称</th>
							<th data-options="field:'_operate',width:190,align:'center',
								formatter:function(value,row,index){return formatOper(value,row,index)}">操作</th>
	                </tr>
					</thead>
				</table>
		</div>
	</div>
</div>
</body>
</html>