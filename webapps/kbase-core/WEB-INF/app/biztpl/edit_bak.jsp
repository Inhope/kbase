<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>业务模板</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			span.cke_button__level0_label{
				display: inline;
			}
			span.cke_button__level0_icon{
				width: 0px;
			}
			span.cke_button__level1_label{
				display: inline;
			}
			span.cke_button__level1_icon{
				width: 0px;
			}
			span.cke_button__biztpldel_label{
				display: inline;
			}
			span.cke_button__biztpldel_icon{
				width: 0px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level0.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level1.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript">
			
		
			var localConfig = {
				customConfig: '${pageContext.request.contextPath }/library/ckeditor/config-cell.js'
			}
		</script>
		
		<script type="text/javascript">
			
			function onClick(event, treeId, treeNode, clickFlag){
				//console.log(window.__kbs_editor.editable().getPrevious());
				
				window.__kbs_editor.editable().insertHtml('<input type="text" xid="' + uuid(8) + '" attrid="' + treeNode.tId + '" class="kbs-title-cell" value="' + treeNode.name + '"><br/>');
			}
			
			/**
			 * @author eko.zhan 
			 * @since 2016-05-10 20:38 
			 * 生成指定位uuid，默认为32位
			 */
			function uuid(len) {
				len = len==undefined?32:len;
				var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
				var res = "";
				for(var i = 0; i < len ; i ++) {
					var id = Math.ceil(Math.random()*35);
					res += chars[id];
				}
				return res;
			}
		
			$(function(){
				var editor = CKEDITOR.replace('editor', {
					customConfig: '${pageContext.request.contextPath}/library/ckeditor/config-biz.js'
				});
				
				
				
				window.__kbs_editor = editor;
			
				//初始化属性树
				var setting = {
					view: {
						showIcon: false
					},
					callback: {
						onClick: onClick
					}
				};
				var zNodes = ${requestScope.bizTplAttr };
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				
				
				//保存
				$('#btnParser, #btnSaveDraft').click(function(){
					var _html = editor.getData();
					//alert(_html);
					$('#bizTplPanel').html(_html);
					
					var attributes = [];
					$('#bizTplPanel').find('.kbs-title-level0').each(function(i, item){
						
						//alert($(item).attr('xdelflag')==undefined?0:$(item).attr('xdelflag'));
						
						var _level0 = {
							id: $(item).attr('xid'),
							name: $(item).val(),
							delflag: $(item).attr('xdelflag')==undefined?0:$(item).attr('xdelflag'),
							children: []
						};
						
						var _level1Arr = $(item).nextUntil('.kbs-title-level0', '.kbs-title-level1');
						//console.log(_level1Arr);
						//解析第二层标题
						$(_level1Arr).each(function(i, item1){
							var _level1 = {
								id: $(item1).attr('xid'),
								delflag: $(item1).attr('xdelflag')==undefined?0:$(item1).attr('xdelflag'),
								name: $(item1).val(),
								children: []
							}
							
							//解析属性
							var _attrArr = $(item1).nextUntil('.kbs-title-level1', '.kbs-title-cell');
							//console.log(_attrArr)
							
							$(_attrArr).each(function(i, attrItem){
								_level1.children.push({
									id: $(attrItem).attr('xid'),
									attrid: $(attrItem).attr('attrid'),
									name: $(attrItem).val(),
									delflag: $(attrItem).attr('xdelflag')==undefined?0:$(attrItem).attr('xdelflag'),
									priority: i
								});
							});
							
							_level0.children.push(_level1);
						});
						
						attributes.push(_level0)
						
					});
					
					var bizTpl = {
						id: $('input[name="id"]').val(),
						items: JSON.stringify(attributes)
					}
					
					$.post('${pageContext.request.contextPath}/app/biztpl/biz-tpl!save.htm', bizTpl, function(data){
						if ($('input[name="id"]').val()==''){
							location.href = '${pageContext.request.contextPath}/app/biztpl/biz-tpl!edit.htm?id=' + data.id;
							//$('input[name="id"]').val(data.id);
						}
						window.__kbs_editor.setData(data.htmlContent);
						alert('业务模板保存成功');
					}, 'json');
				});
			});
		</script>
	</head>

	<body>
		<input type="hidden" name="id" value="${bizTpl.id }">
		<table width="100%">
			<tr>
				<td width="25%" valign="top">
					<div id="treeDemo" class="ztree"></div>
				</td>
				<td width="75%">
					<div style="padding: 5px;">
						<button class="btn btn-default" id="btnSaveDraft">保存为草稿</button>
						<button class="btn btn-default" id="btnParser">保存</button>
					</div>
					<div id="editor" style="height: 300px;overflow: hidden;">
						${requestScope.htmlContent}
					</div>
				</td>
			</tr>
		</table>
		
		<div style="display:none;" id="bizTplPanel"></div>
	</body>
</html>
