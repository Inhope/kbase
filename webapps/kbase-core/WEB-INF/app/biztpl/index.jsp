<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.eastrobot.util.SystemKeys"%>
<%@ taglib prefix="kbs" uri="/WEB-INF/kbs-tags.tld" %>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<base href="<%=basePath%>">

	<title>文章编辑</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/jquery-tab.js"></script>
	<script type="text/javascript">
		var pageContext = {
			contextPath : '${pageContext.request.contextPath }'
		}
		$.extend($.fn, {
			getRootPath : function(){
			    return "${pageContext.request.contextPath }";
			}
		});
		//东航要求默认宋体，此处config可重写，若其他环境和此默认字体不一致，可新建config，并在此判断引用 add by wilson.li at 20161017
		if('<%=SystemKeys.isCeair()%>'){
			var customConfig = pageContext.contextPath+'/library/ckeditor/config_full.js';
		}
	</script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/base.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-embed.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansattr.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansdel.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-anssave.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-zhiling.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/CheckInput.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.tree.js"></script>
	

	<script type="text/javascript">
		var map = {};
		var _absId = new Date().getTime();//Math.random();
		var flag = true;
		var articleId = '${bizTplArticle.id}';
		var tplId  = '${tplId }';
		var relatedObjects = eval('(' + '${relatedObjects}' + ')');
		var type='${type}';
		var swf_address = '${requestScope.converterPath}';
		var username = '${username}';
		//是否显示QA
		var showQa = '<%=PropertiesUtil.valueToString("bizTpl.show_qa")%>';
	</script>
</head>
<body>

<!-- 更换模板页面 -->
<div id="openChangeBiztplDiv" class="easyui-dialog" closed="true" modal="true" title="选择业务模板" style="width:650px;min-height:300px;height:500px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openChangeBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
<!-- 修改模板页面 -->
<div id="openModifyBiztplDiv" class="easyui-dialog" closed="true" modal="true" title="修改业务模板" style="width:800px;min-height:300px;height:500px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openModifyBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
<!-- 选择标签页面 -->
<div id="openSelectLabelDiv" class="easyui-dialog" closed="true" modal="true" title="选择标签" style="width:530px;min-height:300px;height:400px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openSelectLabelIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
<!-- 实例关联页面 -->
<div id="openExampleDiv" class="easyui-dialog" closed="true" modal="true" title="实例关联" style="width:640px;min-height:300px;height:410px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openExampleIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
<!-- 选择本体页面 -->
<div id="openSelectAttrDiv" class="easyui-dialog" closed="true" modal="true" title="选择本体" style="width:530px;min-height:300px;height:400px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openSelectAttrIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
<!-- 编辑指令 -->
<div id="editzhiling" class="easyui-dialog" closed="true" modal="true" title="指令编辑" style="width:320px;height:300px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe id='openZhiLingIframe' frameborder="0"  src="" style="width:100%;height: 100%;"></iframe>
</div>
<!-- 编辑p4 -->
<div id="openp4Div" class="easyui-dialog" closed="true" modal="true" title="p4编辑" resizable="true" style="width:1000px;min-height:100px;height:450px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe scrolling="auto" id='openp4Iframe' name="openp4Iframe" frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>

<!-- 移动标准问页面 -->
<div id="openMoveQuestionDiv" class="easyui-dialog" closed="true" modal="true" title="移动标准问" 
	style="width:400px;min-height:300px;height:400px;overflow: hidden;">
	<div style="width: 100%;float: left;height: 88%;">
		<div class="move">
			<input type="hidden" id="moveQuestionId" value="">
			<input type="hidden" id="moveGroupId" value="">
			<h3><b>移动到</b></h3>
			<dl>
				<!-- 暂时支持二级，更多级时可继续循环，最好是后台将数据处理好之后一次遍历 -->
				<c:forEach items="${groupNodes }" var="group" varStatus="status">
					<c:if test="${group.attrId==''}">
						<dt><span name="moveGroup" _id="${group.groupId }" style="cursor: pointer;"><%-- ${status.index+1 }. --%>${group.name }</span></dt>
					</c:if>
					<c:if test="${fn:length(group.children) > 0 }">
						<c:forEach items="${group.children }" var="child" varStatus="childStatus">
							<c:if test="${child.attrId==''}">
								<dd><span name="moveGroup" _id="${child.groupId }" style="cursor: pointer;"><%-- ${status.index+1 }.${childStatus.index+1 }  --%>${child.name }</span></dd>
							</c:if>
						</c:forEach>
					</c:if>
				</c:forEach>
			</dl>
		</div>
	</div>
	<div style="float: right;margin-right: 10px;">
		<button class="btn btn-default" id="btnSure">确定</button>
	</div>
</div>
<form id="iframeForm" method="post" target="_blank"
	action="${pageContext.request.contextPath }/app/biztpl/article!articlePreview.htm">
	<input type="hidden" name="articleInfo"/>
</form>
<!-- start 	预览页面-打开预览页面时post提交并传参-->
<%-- <form id="iframeForm" method="post" target="iframeForm" 
	action="${pageContext.request.contextPath }/app/biztpl/article!articlePreview.htm">
	<input type="hidden" name="articleInfo"/>
</form>
<div id="articlePreviewDiv" class="easyui-dialog" closed="true" modal="true" title="预览" style="width:100%;min-height:400px;height:600px;overflow: hidden;"
	 data-options="onClose:function(){showOverflow();},onOpen:function(){hideOverflow();}">
	<iframe name="iframeForm" scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div> --%>
<!-- end 	预览页面 -->
<div class="container">
	<div class="sidebar">
		<h2>文章模板</h2>
		<h3 class="ico-2">
			<a id="changeBiztpl" href="javascript:void(0)">更换文章模板</a>
		</h3>
		<h3 class="ico-3">
			<a id="modifyBiztpl" href="javascript:void(0)">修改模板</a>
		</h3>
		<div class="sidebar_cata">
			<div class="sidebar-list">
				<c:forEach items="${groupNodes }" var="group" varStatus="status">
					<ol>
						<c:if test="${group.attrId==''}">
							<li class="level1">
								<%-- <span class="index">${status.index+1 }</span> --%>
								<span class="text"><a href="javascript:void(0);" id="${group.groupId }" title="${group.title }">${group.name }</a></span>
							</li>
						</c:if>
						<c:forEach items="${group.children }" var="child">
							<c:if test="${child.attrId==''}">
								<li class="level2">
									<span class="index">▪</span>
									<span class="text"><a href="javascript:void(0);" id="${child.groupId }" title="${child.title }">${child.name }</a></span>
								</li>
							</c:if>
						</c:forEach>
					</ol>
				</c:forEach>
			</div>
		</div>
	</div>
    <div style="clear:both"></div>
	<div class="right_sidebar">
		<div class="head_top" >
			<p class="butt"><button class="head_top_tj">提交</button><button onclick="articlePreview();">预览</button></p>
			<!-- <p><input type="checkbox" >保存版本</p> -->

			<form id="indexForm" action="${pageContext.request.contextPath }/app/biztpl/article!index.htm" method="post">
				<input type="hidden" name="articleId" value="${bizTplArticle.id }"/>
				<input type="hidden" name="artiName" value=""/>
				<input type="hidden" name="tplId" value="${tplId }"/>
				<input type="hidden" name="categoryId" value="${categoryId }"/>
				<input type="hidden" name="categoryName" value="${categoryName }"/>
				<input type="hidden" name="items" value="">
			</form>

		</div>
		<div class='fix-height'></div>
		<div class="head_form m-top" id="valForm">
			<ul>
				<li class="small">
					<label class="form-font1"><em>*</em>文章名称</label>
					<input name="articleName" type="text" class="full" value="${bizTplArticle.name }" _aid="${bizTplArticle.id }"/>
				</li>
				<li class="small">
					<label class="form-font1"><!-- <em>*</em> -->文章路径</label>
					<input name="category" value="${categoryName }" type="text" class="full" disabled="disabled" value="${categoryName }">
				</li>
				<li class="small">
					<label class="form-font1">开始日期</label>
					<input  id="startDate" name="startDate" type="text" value="<fmt:formatDate value="${bizTplArticle.startDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',onpicking:function(dq){changeDate(dq,this);} ,dateFmt:'yyyy-MM-dd'})"/>
					<!-- <i></i> -->
				</li>
				<li class="small">
					<label class="form-font1">结束日期</label>
					<input id="endDate" name="endDate" type="text" value="<fmt:formatDate value="${bizTplArticle.endDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',onpicking:function(dq){changeDate(dq,this);},dateFmt:'yyyy-MM-dd'})"/>
					<!-- <i></i> -->
					<button id="high" >高级</button>
				</li>
				<li class="small">
					<label class="form-font1">追 溯 期</label>
					<input type="text" name="traceDate" value="<fmt:formatDate value="${bizTplArticle.traceDate }" pattern="yyyy-MM-dd"/>" 
						onclick="WdatePicker({minDate:'#F{$dp.$D(\'endDate\')}',onpicking:function(dq){changeDate(dq,this);},dateFmt:'yyyy-MM-dd'})"/>
					<!-- <i></i> -->
				</li>
				<li class="small">
					<label class="form-font1">语 义 块</label>
					<input name="semantic" type="text" value="${bizTplArticle.semantic }" _aid="${bizTplArticle.id }"/>
				</li>
				<li class="small none">
					<label class="form-font1">关 键 字</label>
					<input name="keywords" type="text" class="full" value="${(bizTplArticle.keyWords=='' || bizTplArticle.keyWords==null )?'多个关键字请用逗号隔开':(bizTplArticle.keyWords) }" 
						onfocus="$(this).css('color','');if($(this).val() == '多个关键字请用逗号隔开'){$(this).val('');}"
						onblur="if($(this).val() == ''){$(this).val('多个关键字请用逗号隔开');$(this).css('color','gray');}if($(this).val() == '多个关键字请用逗号隔开'){$(this).css('color','gray');}"
					/>
				</li>
				<li class="small none">
					<label class="form-font1">实例关联</label>
					<input type="button" value="添加" name="slgl"/>
				</li>
				<li class="small none">
					<label class="form-font1">标    签</label>
					<input type="button" value="添加" name="label" nodeIds="${bizTplArticle.label }"/>
				</li> 
				<!--
                <li class="small none"><label class="form-font1">历史版本</label><input type="button" value="查看" name="version"/></li>
                -->
				<li class="small none">
					<label class="form-font1">属    性</label>
					<input type="button" value="添加" name="property"/>
				</li>
			</ul>
		</div>
		<kbs:if test="<%=SystemKeys.isShxg() %>">
			<div class="about">
				<div class="q_ask_show2">
					<c:if test="${not empty bizTplArticle.articleAbs}">
						<div class="q_js">
							<a href="javascript:void(0)" style="display:none;"><i class="ico_bj"></i>编辑</a>
							<i class="ico-pic"></i>
							<div class="q_js_con">${bizTplArticle.articleAbs }</div>
							<i class="ico-js" style="display:none;"></i>
						</div>
					</c:if>
						<div class="q_ask_ans" <c:if test="${not empty bizTplArticle.articleAbs}">style="display:none;"</c:if>>
							<a href="javascript:void(0)" ><i class="ico-add"></i>添加摘要</a>
						</div>
				</div>
			</div>
		</kbs:if>
		<!---->
		<div class="content">
			<h2 class="title">正文</h2>
			<div class="content_inner">
				<ul class="sortable content_ul">
					<c:forEach items="${requestScope.attrNodes}" var="group">
						<li id="${group.id }" groupId="${group.groupId }" name="${group.name }" parentId="${group.parentId }" attrId="${group.attrId }" priority="${group.priority }" level="${group.level }" delFlag="${group.delFlag }" class="q_ask" style="overflow:hidden">
							<h2 class="title_${group.level +1 }"><a href="javascript:void(0);" _id="${group.groupId }">${group.name }</a></h2>
							<c:set var="isShowQa" value='<%=PropertiesUtil.valueToString("bizTpl.show_qa")%>'></c:set>
							<c:if test="${isShowQa==true}">
								<span class="add_zs"><i class="ico-title_2"></i>新增知识</span>
							</c:if>
						</li>
						<li class="q_ask">
							 <div class="q_ask_show2">
								<div class="q_ask_ans" <c:if test="${not empty group.groupAbs }"> style="display:none;"</c:if>>
									<a href="javascript:void(0)" ><i class="ico-add"></i>添加介绍</a>
							    </div>
							    <c:if test="${not empty group.groupAbs }">
									<div class="q_js">
											<a href="javascript:void(0)" style="display:none;"><i class="ico_bj"></i>编辑</a>
											<i class="ico-pic"></i>
											<div class="q_js_con">${group.groupAbs }</div>
											<i class="ico-js" style="display: none;"></i>
									</div>
								</c:if>
							</div>
							<c:forEach items="${group.question}" var="question">
								<div class="q_ask_show" style="display:${ question.display}">
									<div class="q_as">
										<em class="question-answer question"></em><!--  <i class="ico-move"></i>-->
		                                <span class="q-ask-span">
		                                	<input id="${question.id }" attrId="${question.attrId }" attrName="${question.attrName }" type="text" value="${question.name }" name="kbs-question"/>
		                                	<i class="ico-more"></i>
		                                	<input type="hidden" value="${question.tagIds }" name="tagIds" _this_q_id="${question.attrId }" >
		                                </span>
		                                <span class="q-ask-ico" style="display: none;">
		                                	<i class="ico-delete"></i> 
		                                    <i class="ico-add"></i> 
		                                 </span>
									</div>
	                                <div class="q_ans">
	                                	<c:if test="${not empty question.answer}">
	                                		<em class="question-answer answer"></em>
	                                     	<c:forEach items="${question.answer}" var="an" varStatus="status">
												<div class="wz_more <c:if test="${an.isFist }"> mt</c:if>" id="${an.id }" dimname="${an.dimName }" dimid="${an.dimId }" style="display:${an.display }" isKbase="${an.isKbase }">
														<h2 class="wz_title gray">维度:<em class="wz_wd" title="${an.dimName }">
															<c:if test="${fn:length(an.dimName) > 40}">
																${fn:substring(an.dimName,0,40)}...
															</c:if>
															<c:if test="${fn:length(an.dimName) <= 40}">
																${not empty an.dimName ? an.dimName:"所有维度"}
															</c:if>
														</em><i class="wz_zk">展开</i></h2>
														<div class="wz_more_c">${an.content }</div>
														<div class="wz_down">
															<ul class="wz">
																	<li>
																		<div class="wz_l"></div>
																		<div class="wz_r">
																			<i class="wz_up">收起</i>
																		</div>
																	</li>
																	<li id="dim">
																		<div class="wz_l">
																			维&nbsp;&nbsp;&nbsp;&nbsp;度:
																		</div>
																		<div class="wz_r">
																			<em title="${an.dimName }">
																				<c:if test="${fn:length(an.dimName) > 40}">
																					${fn:substring(an.dimName,0,40)}...
																				</c:if>
																				<c:if test="${fn:length(an.dimName) <= 40}">
																					${not empty an.dimName ? an.dimName:"所有维度"}
																				</c:if>
																			</em>    
																		</div>
																	</li>
																	
																	<li id="cmds">
																		<div class="wz_l">
																			指&nbsp;&nbsp;&nbsp;&nbsp;令:
																		</div>
																		<div class="wz_r">
																			<em>${an.cmds }</em>    
																		</div>
																	</li>		
																	<li id="validDate">
																		<div class="wz_l">
																			有效期:
																		</div>
																		<div class="wz_r">
																			<input type="text" id="ans_startDate" _name="startDate" class="wz_date" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'ans_endDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.startDate }" disabled="disabled"> 
																		  ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ans_startDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.endDate }" disabled="disabled">
																		</div>
																	</li>
																	<li id="traceDate">
																		<div class="wz_l">
																			追溯期:
																		</div>
																		<div class="wz_r">
																			<input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ans_endDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.traceDate }" disabled="disabled">
																		</div>
																	</li>		
																	<li id="attrachment">
																		<div class="wz_l">
																			附&nbsp;&nbsp;&nbsp;&nbsp;件:
																		</div>
																		<div class="wz_r">
																			<c:forEach items="${an.attments}" var="att">
																				<p>
																					<a href="javascript:void(0)" class="wz_fj" fId="${att.pid }" id="${att.attId }" atttype="${att.attType }" remark="${att.remark }">${att.attName }</a>
																					<a href="javascript:void(0)" class="wz_xz">下载 </a>
																					<i class="wz_del"></i>
																				</p>
																			</c:forEach>
																		</div>
																	</li>
															</ul>
														</div>
													<c:if test="${status.first && question.anCount > 1}">
														<div class="ico_wz_m">更多<div class="num"><a href="javascript:void(0)" class="em_num">${question.anCount}</a></div></div>
													</c:if>
														<div class="ico_wz_d" style="display:none;"></div>							   
												</div>
											</c:forEach>
										</c:if>
									</div>
								</div>
							</c:forEach>
						</li>
						<c:forEach items="${group.children}" var="children">
								<li id="${children.id }" name="${children.name }" attrId="${children.attrId }" 
									parentId="${children.parentId }" groupId="${children.groupId }" priority="${children.priority}" tplId="${children.tplId }"
									level="${children.level }" delFlag="${children.delFlag }"
									 style="display:none;"></li>
						</c:forEach>
					</c:forEach>

                    </ul>
					<c:if test="${fn:length(unMatchList) > 0 }">
					<div class="wait-pipei">
						<div class="title-wait-pipei">
							等待匹配的问题
							<div class="title-pipei">
								<span name="moveQues"><i class="ico-move"></i><a href="javascript:void(0)">移动知识</a></span>
								<span name="deleteQues"><i class="ico-delete2"></i><a href="javascript:void(0)">删除知识</a></span>
							</div>
						</div>
						<dl>
							<dt class="title-dt">
								<label for=""><input type="checkbox" name="checkAll" style="vertical-align: middle;"></label>
								<label style="vertical-align: middle;">全部</label>
							</dt>
							<c:set value="0" var="sum" />
							<c:forEach items="${unMatchList }" var="question" varStatus="status">
							<c:if test="${question.display == 'block' }">
								<c:set value="${sum + 1}" var="sum" />
							</c:if>
							<dd name="unMatchQuestion" style="display:${ question.display}">
								<label for=""><input type="checkbox" name="checkQues" value="${question.id }" style="vertical-align: middle;"></label>
								<font name="qName" style="cursor: pointer;vertical-align: middle;">
									${sum}.${question.name }
								</font>
								<!-- <i class="arrows-move"></i> -->
								<input type="hidden" name="unMatchQuestionId" value="${question.id }">
								<input type="hidden" name="unMatchQuestionName" value="${question.name }">
								<input type="hidden" name="unMatchQuestionAttrId" value="${question.attrId }">
								<input type="hidden" name="unMatchQuestionAttrName" value="${question.attrName }">
								<input type="hidden" name="unMatchQuestionGroupId" value="">
								<c:if test="${fn:length(question.answers) > 0 }">
									<c:forEach items="${question.answers }" var="answer">
										<div name="unMatchAnswer" style="margin-bottom: 5px;">
											<p>${answer.content }</p>
											<input type="hidden" name="unMatchAnswerId" value="${answer.id }">
											<span style="display:none!important;" name="unMatchAnswerContent">${answer.content }</span>
											<input type="hidden" name="answerDimIds" value="${answer.dimIds }">
											<input type="hidden" name="answerDimNames" value="${answer.dimNames }">
											<input type="hidden" name="answerAttIds" value="">
											<input type="hidden" name="answerAttNames" value="">
											<input type="hidden" name="answerCmds" value="${answer.cmds }">
										</div>
									</c:forEach>
								</c:if>
								<div style="display: none;" name="qid_${question.id }" isKbase="${question.isKbase }">
									<div class="q_ask_show" style="display:${ question.display}">
										<div class="q_as">
											<em class="question-answer question"></em><!--  <i class="ico-move"></i>-->
			                                <span class="q-ask-span">
			                                	<input id="${question.id }" attrId="${question.attrId }" attrName="${question.attrName }" type="text" value="${question.name }" name="kbs-question"/>
			                                	<i class="ico-more"></i>
			                                	<input type="hidden" value="" name="tagIds" _this_q_id="" >
			                                </span>
			                                <span class="q-ask-ico" style="display: none;">
			                                	<i class="ico-delete"></i> 
			                                    <i class="ico-add"></i> 
			                                 </span>
										</div>
		                                <div class="q_ans">
		                                	<c:if test="${not empty question.answers}">
		                                		<em class="question-answer answer"></em>
		                                     	<c:forEach items="${question.answers}" var="an" varStatus="status">
													<div class="wz_more <c:if test="${an.isFist }"> mt</c:if>" id="${an.id }" dimname="${an.dimNames }" dimid="${an.dimIds }" style="display:${an.display }" isKbase="${an.isKbase }">
															<h2 class="wz_title gray">维度:<em class="wz_wd" title="${an.dimNames }">
																<c:if test="${fn:length(an.dimNames) > 40}">
																	${fn:substring(an.dimNames,0,40)}...
																</c:if>
																<c:if test="${fn:length(an.dimNames) <= 40}">
																	${not empty an.dimNames ? an.dimNames:"所有维度"}
																</c:if>
															</em><i class="wz_zk">展开</i></h2>
															<div class="wz_more_c">${an.content }</div>
															<div class="wz_down">
																<ul class="wz">
																		<li>
																			<div class="wz_l"></div>
																			<div class="wz_r">
																				<i class="wz_up">收起</i>
																			</div>
																		</li>
																		<li id="dim">
																			<div class="wz_l">
																				维&nbsp;&nbsp;&nbsp;&nbsp;度:
																			</div>
																			<div class="wz_r">
																				<em title="${an.dimNames }">
																					<c:if test="${fn:length(an.dimNames) > 40}">
																						${fn:substring(an.dimNames,0,40)}...
																					</c:if>
																					<c:if test="${fn:length(an.dimNames) <= 40}">
																						${not empty an.dimNames ? an.dimNames:"所有维度"}
																					</c:if>
																				</em>    
																			</div>
																		</li>
																		
																		<li id="cmds">
																			<div class="wz_l">
																				指&nbsp;&nbsp;&nbsp;&nbsp;令:
																			</div>
																			<div class="wz_r">
																				<em>${an.cmds }</em>    
																			</div>
																		</li>		
																		<li id="validDate">
																			<div class="wz_l">
																				有效期:
																			</div>
																			<div class="wz_r">
																				<input type="text" id="ans_startDate" _name="startDate" class="wz_date" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'ans_endDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.startDate }" disabled="disabled"> 
																			  ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ans_startDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.endDate }" disabled="disabled">
																			</div>
																		</li>
																		<li id="traceDate">
																			<div class="wz_l">
																				追溯期:
																			</div>
																			<div class="wz_r">
																				<input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ans_endDate\')}',dateFmt:'yyyy-MM-dd'})" value="${an.traceDate }" disabled="disabled">
																			</div>
																		</li>		
																		<li id="attrachment">
																			<div class="wz_l">
																				附&nbsp;&nbsp;&nbsp;&nbsp;件:
																			</div>
																			<div class="wz_r">
																			</div>
																		</li>
																</ul>
															</div>
															<c:if test="${status.first && question.anCount > 1}">
																<div class="ico_wz_m">更多<div class="num"><a href="javascript:void(0)" class="em_num">${question.anCount}</a></div></div>
															</c:if>
															<div class="ico_wz_d" style="display:none;"></div>							   
													</div>
												</c:forEach>
											</c:if>
										</div>
									</div>
								</div>
							</dd>
							</c:forEach>
						</dl>
					</div>
					</c:if>
					<div class="content-tab">
						<h2 class="title-table">附件</h2>
						<table border="0" cellspacing="0" cellpadding="0" id="list">
							<tr>
								<td colspan="7">
									<div class="table_head">
										<span><input type="checkbox" name="all">全部</span>
										<em id="fj_del">删除</em>
										<em id="fj_look">查看</em>
										<em id="upload">上传</em>
										<em id="fj_down">下载</em>
									</div>
								</td>
							</tr>
							<tr>
								<td>名称</td>
								<td>操作者</td>
								<td>时间</td>
								<!-- <td>屏蔽/公开</td>
								<td>版本</td> -->
								<td>类型</td>
								<td>备注</td>
							</tr>
							<c:forEach items="${requestScope.files}" var="file">
								<c:set var="f" value=""/>
								<c:forEach items="${file.physicalRFiles}" var="rf" varStatus="status">
									<c:if test="${status.first}">
										<c:set var="f" value="${rf}"/>
									</c:if>
									<c:if test="${rf.createTime > f.createTime}">
										<c:set var="f" value="${rf}"/>
									</c:if>
								</c:forEach>
								<tr type="${file.attType }" id="${file.id }" pid="${f.id }">
									<td> <div class="fileName"><input id="${file.id }" type="checkbox" name="check_list">${file.name }</div></td>
									<td></td>
									<td>
										<fmt:formatDate value="${file.createTime }" pattern="yyyy-MM-dd"/>
									</td>
									<!-- <td>:</td>
									<td>x</td> -->
									<td>
										<c:if test="${file.attType==1 }">相关文件</c:if>
										<c:if test="${file.attType==2 }">废止文件</c:if>
										<c:if test="${file.attType==3 }">征求意见稿</c:if>
										<c:if test="${file.attType==4 }">其他</c:if>
										<c:if test="${file.attType==5 }">正文</c:if>
										<c:if test="${file.attType==6 }">附件</c:if>
										<c:if test="${file.attType==0 }">附件</c:if>
									</td>
									<td>${f.remark }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none;">
	<textarea id = "textarea"></textarea>
</div>
<%-- 图片插入 --%>
<jsp:include page="../util/ckEdit-myImage.jsp"></jsp:include>
<%-- 标签选择 --%>
<%-- <jsp:include page="article_labels.jsp"></jsp:include> --%>
</body>
</html>