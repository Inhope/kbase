<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>附件上传</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			input[type="text"]{
				border: 1px solid #cdcdcd;
				height: 30px;
				line-height: 30px;
				border-radius: 4px;
				margin: 4px 0;
				width: 72%;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.ajaxfileupload.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		
		<script type="text/javascript">
			var pageContext = {
				contextPath : '${pageContext.request.contextPath }'
			}
		</script>
		
		<script type="text/javascript">
		
			$(function(){
				var $textarea = $(parent.document).find('textarea[id="' + parent.__kbs_editor_id + '"]');
				var objectId = parent.__kbs_editor_id;
				var _attids = $textarea.attr('_attids');
				var _attNames = $textarea.attr('_attNames');
				var _attidsNew = $textarea.attr('_attidsNew');
				var _attNamesNew = $textarea.attr('_attNamesNew');
				var _attidArrNew = [];
				var _attNamesArrNew = [];
				var _attidArr = [];
				var _attNamesArr = [];
				if (_attids!=undefined && _attids.length>0){
					_attidArr = _attids.split(',');
					_attidArrNew = _attidsNew.split(',');
				}
				if (_attNames!=undefined && _attNames.length>0){
					_attNamesArr = _attNames.split(',');	
					_attNamesArrNew	= _attNamesNew.split(',');
				}
				
				//填充 attList
				var $attPanel = $(parent.document).find('div[id="Att' + $textarea.attr('id') + '"]');
				if ($attPanel.length==0){
					$(parent.document.body).append('<div id="Att' + $textarea.attr('id') + '" style="display:none;"></div>');
				}else{
					$('#attList').html($attPanel.html());
				}
				
				$('#btnUpload').on('click', function(){
					$('input[type="file"]').click();
					return false;
				});
				$('#attList>li').remove();
				for(x in _attidArr){
					$('#attList').append('<li _id="' + _attidArr[x] + '">' + _attNamesArr[x] + '<a  name="deleteAtt" id="' + _attidArr[x] + '" href = "javascript:void(0);"> 删除 </a></li>');
				}
				
				
			
				//解决type=file框onchange监听第二次失效的问题
				$('input[type="file"]').parent('td').on('change', 'input[type="file"]', function(){
					var fileName = $(this).val();
					if (fileName.length>0){
						$.ajaxFileUpload({
							url : pageContext.contextPath + '/app/biztpl/article!doUpload.htm',		//待上传文件的路径
							secureuri : false,
							fileElementId : 'file',		//文件上传字段id
							data : {"filename": fileName},	//参数
							dataType : 'json',
							timeout : 5*60*1000,
							success : function(data, status) {
								if (data.success) { 
									//alert(data.message);
									//alert(data.data);
									
									var result = JSON.parse(data.data);
									_attidArr.push(result.id);
									_attNamesArr.push(result.filename);
									_attidArrNew.push(result.id);
									_attNamesArrNew.push(result.filename);
									$('#attList').append('<li _id="' + result.id + '">' + result.filename + '<a name="deleteAtt" id="' + result.id + '" href = "javascript:void(0);"> 删除 </a></li>');
									
								} else {
									alert(data.message);
								}
							},
							error : function(data, status, e){// 服务器响应失败处理函数
								alert('处理超时,请重新上传!');
							}
						});
					}
				});
			
				$('#btnOk').click(function(){
					$textarea.attr('_attids', _attidArr);
					$textarea.attr('_attNames', _attNamesArr);
					$textarea.attr('_attidsNew', _attidArrNew);
					$textarea.attr('_attNamesNew', _attNamesArrNew);
					
					$attPanel = $(parent.document).find('div[id="Att' + $textarea.attr('id') + '"]');
					$attPanel.html($('#attList').html());
					
					$('#btnCancel').click();
				});
				
				$('#btnCancel').click(function(){
					parent.layer.close(parent.__kbs_layer_index);
				});
				
				$('a[name="deleteAtt"]').click(function(){
					 var attId = $(this).attr('id');
					 var topNodeId = '3';
					 var oid = '';
					 if(objectId.indexOf('Answer') != -1){
						 oid = objectId.replace('Answer','');
					 }else if(objectId.indexOf('group') != -1){
					 	oid = objectId.replace('group','');
					 	topNodeId = '1';
					 }
					$.ajax({
				   		type: "POST",
				   		url: pageContext.contextPath + '/app/biztpl/article!relieveRelation.htm',
				   		data: {attachmentId:attId,topNodeId:topNodeId,objectIds:oid},
				   		dataType:'json',
				   		async: true,
				   		success: function(msg){
				   			if(msg){
					   			var attachmentId = msg.attachmentId;
					   			$('ul#attList > li[_id="'+attachmentId+'"]').remove();
					   			_attNamesArr.splice(_attidArr.indexOf(attachmentId),1);
					   			_attidArr.splice(_attidArr.indexOf(attachmentId),1);
					   			$textarea.attr('_attids', _attidArr);
								$textarea.attr('_attNames', _attNamesArr);
					   			alert('解除成功');
				   			}else{
				   				alert('解除超时');
				   			}
				   		},
				   		error :function(){
				   			alert('解除失败');
				   		}
					});
					 
				});
			});
		</script>
	</head>

	<body>
	    <table class="table">
	    	<tr>
	    		<td>
	    			<button class="btn btn-default" id="btnUpload">上传</button>
	    			<input type="file" id="file" name="file" style="display:none;"/>
	    		</td>
	    	</tr>
	    	<tr>
	    		<td>
	    			<ul id="attList">
	    				
	    			</ul>
	    		</td>
	    	</tr>
	    	<tr>
	    		<td align="right">
	    			<button class="btn btn-default" id="btnOk">确定</button>
					<button class="btn btn-default" id="btnCancel">取消</button>
	    		</td>
	    	</tr>
	    </table>
	    
	</body>
</html>
