<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.eastrobot.domain.biztpl.BizTplGroup"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>新增文章</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css01.css?t=2">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 14px;
			}
			input[type="text"]{
				border: 1px solid #cdcdcd;
				height: 30px;
				line-height: 30px;
				border-radius: 4px;
				margin: 4px 0;
				width: 72%;
			}
			.btn{
				margin-bottom: 2px;
			}
			button{
				border: 1px solid #cdcdcd;
				margin: 2px;
				padding: 2px;
				cursor: pointer;
			}
			span.cke_button__embed_label{
				display: inline;
			}
			span.cke_button__embed_icon{
				width: 0px;
			}
			span.cke_button__ansattr_label{
				display: inline;
			}
			span.cke_button__ansattr_icon{
				width: 0px;
			}
			span.cke_button__ansdel_label{
				display: inline;
			}
			span.cke_button__ansdel_icon{
				width: 0px;
			}
			span.cke_button__anssave_label{
				display: inline;
				font-weight: bold;
			}
			span.cke_button__anssave_icon{
				width: 0px;
			}
			.cke_button__myimage_icon {background: url(${pageContext.request.contextPath }/library/ckeditor/skins/moono/icons.png) no-repeat 0 -936px !important;}  
    		.cke_button__myimage_label {display: inline !important;}
			.button_fr{
				float: right;
			}
			.textarea_div{
				background-color: #f6f6fe;
			    border: 1px solid #cdcdcd;
			    border-radius: 4px;
			    color: #555;
			    display: inline-block;
			    line-height: 24px;
			    margin: 8px 0;
			    min-height: 30px;
			    padding: 2px 8px;
			    width: 82%;
			    word-break: break-all;
			}
			.marign_right{
				margin-right: 4px;
			}
			.img_width{
				width: 30px;
			}
			.img_style{
				cursor: pointer;
				vertical-align: top;
				width: 30px;
				float: right;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/prototype.extend.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-embed.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansattr.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-ansdel.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-anssave.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/json/json2.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/biztpl/js/CheckInput.js"></script>
		<script type="text/javascript">
			var pageContext = {
				contextPath : '${pageContext.request.contextPath }'
			}
			
			var localConfig = {
				customConfig: '${pageContext.request.contextPath }/library/ckeditor/config-cell.js'
			}
			
			var map = {};
			var _absId = new Date().getTime();//Math.random();
			var flag = true;
		</script>
		
		<script type="text/javascript">
			function _getWindowParams(opts){
				var height = (opts && opts.height) || 300;
				var width = (opts && opts.width) || 500;
				
				var _scrHeight = screen.height;
				var _scrWidth = screen.width;
				var top = (_scrHeight - height)/2;
				var left = (_scrWidth - width)/2;
				
				return 'height=' + height + ', width=' + width + ', top=' + top + ', left=' + left + ',location=0, menubar=0, resizable=0, status=0, toolbar=0';
				
			}
			
			//所有ckeditor转为div模式
			function editorToDiv(){
				for (var id in map){
					var _this = $("div#cke_"+id);//富文本对象
					var _div = $("div[_name='"+id+"']");//文本div对象
					//判断是否已保存过：未保存过的会生成对应div
					if(_div.length == 0){
						var _divhtml = "<div _name='"+id+"' class='textarea_div'>"+map[id].getData()+"</div>"; 
						_this.parent().append(_divhtml);
						$("div[_name='"+id+"']").on("dblclick",function(){
							editorToDiv();
							$("div#cke_"+id).show();
							$(this).hide();
						});
					}else{
						_div.text("");//清空数据
						_div.append(map[id].getData())
					}
					_div.show();
					_this.hide();
				}
			}
			
			$(function(){
				//新增标准问
				$('[name="kbs-new-qa"]').click(function(){
					var _this = this; 
					var _targetPanel = $(_this).next('button').attr('_targetPanel');
					
					$(_this).next('button').click();
					
				});
				//新增标准问
				$('[name="kbs-new-qa"]').next('button').click(function(){
					
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					//增加ckeditor之前将其他所有ckeditor转为div模式
					editorToDiv();
					
					var _this = this; 
					var _targetPanel = $(_this).attr('_targetPanel');
					var _table = $('table[_panel="' + _targetPanel + '"]');
					$(_table).parent('td').css('display','');
					if (_table.length==1){
						var _id = new Date().getTime();//Math.random();
						_id = 'Draft' + _id;
					
						var _textarea = document.createElement('textarea');
						_textarea.id = _id;
						//_dummyID 虚拟ID，新增的标准问没有attrID，通过_dummyID和答案关联
						_table.append('<tr _id="' + _id + '" _name="kbs-tr-question"><td><span class="ico-ask ico-text">问</span><input style="margin-right:4px;" type="text" _attrid="" _dummyID='+_id+' _name="kbs-question" /><font _name="attrAsterisk" style="color: red;font-size: 16px;">*</font><img _id="'+_id+'" _name="btnNewAnswer" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_add.png" title="新增答案" class="img_style"><img _id="'+_id+'" _name="btnRemove" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_remove.png" title="删除" class="img_style" style="margin-right:10px;"></td></tr>');
						var _tr = document.createElement('tr');
						var _td = document.createElement('td');
						var imgHtml = '<span _answer = "'+_id +'" class="ico-answer ico-text">答</span>';
						$(_td).append(imgHtml);
						$(_td).append(_textarea);
						$(_tr).attr('_id', _id).attr('_dimtags', $(_this).attr('_dimtags')).append(_td);
						_table.append(_tr);
						
						var _editor = CkEditImage.create.replace(_textarea);
						map[_id] = _editor;
					}
				});
				
				//删除
				$('body').on('click', 'img[_name="btnRemove"]', function(){
					var _id = $(this).attr('_id');
					if (window.confirm('确定删除吗')){
						$($('tr[_id="' + _id + '"]').get(1)).find('textarea').each(function(i, item){
							delete map[item.id];
						})
						$('tr[_id="' + $(this).attr('_id') + '"]').remove();
						delete map[_id];
					}
				})
				//新增答案
				$('body').on('click', 'img[_name="btnNewAnswer"]', function(){
					
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					
					//增加ckeditor之前将其他所有ckeditor转为div模式
					editorToDiv();
					
					var _this = this;
					var _id = $(_this).attr('_id');
					
					var _newId = new Date().getTime();//Math.random();
					_newId = 'Draft' + _newId
				
					var _textarea = document.createElement('textarea');
					_textarea.id = _newId;
					$(_textarea).css('margin-left','60px');
					var imgHtml = '<span _answer = "'+_newId +'" class="ico-answer ico-text">答</span>';
					$($('tr[_id="' + _id + '"]').get(1)).find('td').append(imgHtml);
					$($('tr[_id="' + _id + '"]').get(1)).find('td').append(_textarea);
					
					var _editor = CkEditImage.create.replace(_textarea);
					map[_newId] = _editor;
				})
				
				//新增摘要
				$('img[_name="abstract"]').click(function(){
					
					var _this = this;
					var _id = $(_this).attr('_id');
					
					//判断答案不能为空
					for (var id in map){
						if(id.indexOf('Draft') > -1){
							if(map[id].getData() == ''){
								alert("答案不能为空!");
								return  false;
							}
						}
					}
					
					if(!CKEDITOR.instances[_id]){
						
						//增加ckeditor之前将其他所有ckeditor转为div模式
						editorToDiv();
						
						var _newId = new Date().getTime();//Math.random();
						_newId = 'Draft' + _newId
					
						var _textarea = document.createElement('textarea');
						_textarea.id = _id;
						
						if($($('tr[_id="' + _id + '"]').get(0)).find("td").length == 0){
							$($('tr[_id="' + _id + '"]').get(0)).append("<td></td>");
						}
						$($('tr[_id="' + _id + '"]').get(0)).find('td').append(_textarea);
						
						var _editor = CkEditImage.create.replace(_textarea);
						map[_id] = _editor;
					}else{
						if($(_this).attr('_abstractType') == 'abstract'){
							alert("每篇文章只能创建一个摘要!");
						}else{
							alert("每个标题只能创建一个介绍!");
						}
						return false;
					}
					
				});
				//获取业务模版的结构
				function getGroup(group){
					var  _json = [];
					group.each(function(i,item){
						var json = {'id':'','name':'','attrId':'','parentId':'','groupId':'','priority':'','tplId':'','level':'','abs':'','children':''};
						var id = $(item).attr('id');;
						//json.id =  id;
						json.name =  $(item).attr('_name');
						json.attrId = $(item).attr('attrId');
						json.parentId = $(item).attr('parentId');
						json.priority = $(item).attr('priority');
						json.level = $(item).attr('level');
						json.groupId = id;
						json.tplId = $('input[name="tplId"]').val();
						if(!$(item).attr('attrId')){
							if(CKEDITOR.instances['group_'+id]){
								json.abs = map['group_'+id].getData();
							}						
						}
						
						var chil = getGroup($('a[parentId="'+id+'"]'));
						json.children = chil;
						_json.push(json);
					})
					return _json;
				}
				
				//文章入库
				$('#btnStore').click(function(){
					var article = getData();
					$.post(pageContext.contextPath + '/app/biztpl/article!save.htm?op=store', {data: JSON.stringify(article)}, function(data){
						var result = data.data;
						var code = result.code;
						if(code == '0'){
							alert('入库成功');
						}else{
							alert(result.reason);
						}
					}, 'json');
				});
				//文章保存
				$('#btnSave').click(function(){
					var article = getData();
					if(flag){
						flag = _checkDataBatch();
					}
					if(flag){
						var property  = $('#textarea').text();
						console.log(property);
						
						$('#btnSave').attr('disabled',true);
						var group = $('a[parentId=""]');
						var groupList = getGroup(group);
						console.log(JSON.stringify(article));
						$.post(pageContext.contextPath + '/app/biztpl/article!save.htm?op=save', {data: JSON.stringify(article),groupData:JSON.stringify(groupList),property:property}, function(data){
							var result = data.data;
							var code = result.code;
							if(code == '0'){
								alert('保存成功');
								location.href = pageContext.contextPath + '/app/biztpl/article!edit.htm?id=' + result.id;
							}else{
								alert(result.reason);
							}
							$('#btnSave').attr('disabled',false);
						}, 'json');
					}
					flag = true;
				});
				
				//数据封装
				function getData(){
					var absId = $('img[_name="abstract"]').attr('_id');
					var abstract = '';
					if(absId){
						if(CKEDITOR.instances[absId]){
							abstract = map[absId].getData();// 文章摘要
						}	
					}
					var $textarea = $('textarea[id="' + absId + '"]')
					var _attids = $textarea.attr('_attids');	//附件ids
					var _attNames = $textarea.attr('_attNames');	//附件names
					
					var name = $('input[name="articleName"]').val();
					var semantic = $('input[name="semantic"]').val();
					if(name == ''){
						alert("文章名称不能为空!");
						$('input[name="articleName"]').focus();
						flag = false;
						return false;
					}
					if(semantic == ''){
						alert("语义块不能为空!");
						flag = false;
						$('input[name="semantic"]').focus();
						return false;
					}
					var article = {
						id: $('input[name="id"]').val(),
						tplId: $('input[name="tplId"]').val(),
						name: name,
						label: $('input[name="label"]').attr('nodeIds'),
						traceDate: $('input[name="traceDate"]').val(),
						startDate: $('input[name="startDate"]').val(),
						endDate: $('input[name="endDate"]').val(),
						categoryName: $('#categoryId').val(),
						categoryId: $('#categoryId').attr('_id'),
						semantic: semantic,
						abstract:abstract,
						attids:_attids,
						attNames:_attNames
					}
					
					var quesArr = [];
					var $quesArr = $('input[_name="kbs-question"]');
					$quesArr.each(function(i, item){
						if($(item).val()==null||$(item).val()==''||$(item).val()==undefined){
							alert('标准问不能为空');
							flag = false;
							return false;
						}
						var _id = $(item).attr('_attrid');
						if(!_id){//如果没有_attrid 则表示为新增的知识点，通过_dummyID去关联对应的答案
							_id = $(item).attr('_dummyID');
						}
						var answerArr = [];
						$($('tr[_id="' + _id + '"]').get(1)).find('textarea').each(function(i, item){
							var _editor = map[item.id];
							var _content = _editor.getData();
							
							if(_content ==null || _content == ''){
								flag = false;
								return false;							
							}
							
							var _attids = $(item).attr('_attids');	//附件ids
							var _attNames = $(item).attr('_attNames');	//附件names
							var _dimtags = $(item).attr('_dimtags');	//答案维度属性ids
							var _dimnames = $(item).attr('_dimnames');
							
							answerArr.push({
								id: '',
								content: _content,
								attids: _attids,
								attNames:_attNames,
								dimtags: _dimtags,
								dimsnames: _dimnames
							})
						});
						if(answerArr == null ||answerArr.length ==0 || !flag){
							flag = false;
							alert('[' + $(item).val() + ']的答案不能为空');
							return false;
						}
						quesArr.push({
							id: '',
							attrid: $(item).attr('_attrid'),
							attrname: $(item).attr('_attrname'),
							groupid: $($(item).parents('table').get(0)).attr('_panel'),
							name: $(item).val(),
							answers: answerArr
						})
						
					});	//end each quesArr
					
					$.extend(article, {
						questions: quesArr
					})
					return article;
				}
				
				var setting = {
					view: {
						showIcon: function(){return false}
					},
					data: {
						simpleData: {
							enable: true
						},
						showTitle:true, //是否显示节点title信息提示 默认为true
						key: {
							title:"title" //设置title提示信息对应的属性名称 也就是节点相关的某个属性
						}
					},
					callback: {
						onClick: function(event, treeId, treeNode, clickFlag){
							if(treeNode.attrId == ""){//标题
								location.hash = '#' + treeNode.id;
							}else{//属性
								var _this = $("input[_attrid='"+treeNode.attrId+"']");
								$(_this).focus();
								//上移
								var moveTo = $(_this).offset().top-5;
								$("html,body").animate({scrollTop:moveTo},10);
							}
						}
					}
				};
	
				var zNodes = ${requestScope.groupNodes };
				
				$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				
				//左边模板浮动
				var _height = 80;//预留空间
				var scrollTop = $(window).scrollTop();
				var moveTop = scrollTop+_height;
				$("#biztplOperate").animate({top:scrollTop},0);
				$("#treeDemo").animate({top:moveTop},0);
				$(window).scroll(function(){
					moveTop = $(window).scrollTop()+_height;
					$("#treeDemo").animate({top:moveTop},0);
					scrollTop = $(window).scrollTop();
					$("#biztplOperate").animate({top:scrollTop},0);
				});
				
				
				$('input[name=property]').click(function(){
					var id = '${bizTplArticle.id  }';
					$("#openEditPropIframe")[0].src = $.fn.getRootPath() + "/app/biztpl/article!property.htm?id="+id;
					$("#openEditPropDiv").dialog("open");
				});
				
				
				//更换模板
				$("#changeBiztpl").on("click",function(){
					if(confirm("请确保文章已保存，否则修改的数据将丢失")){
						var articleName = $("[name='articleName']").val();
						var _url = $.fn.getRootPath() + "/app/biztpl/article!changeBiztplPage.htm?articleName="+articleName;
						//打开编辑页面所在iframe
						$("#openChangeBiztplIframe")[0].src = _url;
						$("#openChangeBiztplDiv").dialog("open");
					}
				});
				
				//修改模板
				$("#modifyBiztpl").on("click",function(){
					if(confirm("请确保文章已保存，否则修改的数据将丢失")){
						var articleId = $("[name='id']").val();
						var tplId = $("[name='tplId']").val();
						var _url = $.fn.getRootPath() + "/app/biztpl/article!modifyBiztplPage.htm?articleId="+articleId+"&tplId="+tplId;
						//打开编辑页面所在iframe
						$("#openModifyBiztplIframe")[0].src = _url;
						$("#openModifyBiztplDiv").dialog("open");
					}
				});
			});
			function closeProIframe(){
					$("#openEditPropDiv").dialog("close");
			}
			
			//关闭更换模板页面
			function closeIframe(){
				$("#openChangeBiztplDiv").dialog("close");
			}
			
			//关闭修改模板页面
			function closeModifyIframe(){
				$("#openModifyBiztplDiv").dialog("close");
			}
		</script>
	</head>

	<body>
		<input type="hidden" name="id" value="${param.id }"/>
		<input type="hidden" name="tplId" value="${param.tplId }"/>
		<input type="hidden" name="categoryId" value="${param.categoryId }"/>
		<input type="hidden" name="categoryName" value="${param.categoryName }"/>
		
		<!-- 更换模板页面 -->
		<div id="openChangeBiztplDiv" class="easyui-window" closed="true" modal="true" title="更换模板" style="width:530px;min-height:300px;height:600px;overflow: hidden;">
			<iframe scrolling="auto" id='openChangeBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
		<!-- 修改模板页面 -->
		<div id="openModifyBiztplDiv" class="easyui-window" closed="true" modal="true" title="修改模板" style="width:800px;min-height:300px;height:600px;overflow: hidden;">
			<iframe scrolling="auto" id='openModifyBiztplIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
		</div>
		
		<table width="100%">
			<tr>
				<td width="20%" style="padding: 0px 10px;">
					<!-- 左边模板浮动区域 -->
					<div id="biztplOperate" style="width: 20%;z-index: 1000;height: 80px;position: absolute;top:0px;">
						<h4>文章模板</h4>
						<h5>
							<a id="changeBiztpl" href="javascript:void(0)" style="margin-right: 10px;">
								<img src="${pageContext.request.contextPath }/resource/biztpl/images/ico-8.png" width="16">更换文章模板
							</a>
							<a id="modifyBiztpl" href="javascript:void(0)">
								<img src="${pageContext.request.contextPath }/resource/biztpl/images/ico-2.png" width="16">修改模板
							</a>
						</h5>
					</div>
					<!-- 因为是绝对定位，保持百分比和td百分比一致 -->
					<div id="treeDemo" class="ztree" style="width: 20%;position: absolute;z-index: 1000;">
					</div>
				</td>
				<td width="80%" style="padding: 0px 50px;">
					<table width="100%" class="table">
						<colgroup>
							<col width="15%">
							<col width="35%">
							<col width="15%">
							<col width="35%">
						</colgroup>
						<tr>
							<td>文章路径</td>
							<td colspan="3"><input type="text" id="categoryId" value="${categoryName }" _id="${categoryId }" disabled="disabled"/></td>
						</tr>
						<tr>
							<td>文章名称</td>
							<td colspan="3">
								<input type="text" name="articleName" value="${param.articleName }"/>
								<font style="color: red;font-size: 16px;">*</font>
							</td>
						</tr>
						<tr>
							<td>标签</td>
							<td><input type="text" name="label" nodeIds=""/></td>
							<td>追溯期</td>
							<td><input type="text" name="traceDate" onclick="WdatePicker()"/></td>
						</tr>
						<tr>
							<td>有效期</td>
							<td>
								<input id="startDate" type="text" name="startDate" style="width:120px;" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',dateFmt:'yyyy-MM-dd'})"/>
								 ~ 
								<input id="endDate" type="text" name="endDate" style="width:120px;" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd'})"/>
							</td>
							<td>语义块</td>
							<td>
								<input type="text" name="semantic"/>
								<font style="color: red;font-size: 16px;">*</font>
							</td>
						</tr>
						
						<tr>
							<td>属性</td>
							<td>
								<input type="button" name="property" value="添加">
							</td>
						</tr>
						
						<tr>
							<td colspan="4" align="right">
								<button class="btn btn-default" style="background-color: #e1e1e1;">预览</button>
								<button class="btn btn-default" id="btnSave" style="background-color: #e1e1e1;">保存</button>
								<!--  <button class="btn btn-default" id="btnStore">入库</button>-->
							</td>
						</tr>
					</table>
					<!--  
					<br>
					<span style="font-size: 16px;font-weight: bolder;">摘要</span>
					<table width="100%" class="table">
						<tr>
							<td>
								<img _id="${param.tplId  }" _abstractType="abstract" _name="abstract" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_edit2.png" 
									title="添加摘要" class="img_style">
							</td>
						</tr>
						<tr _id="${param.tplId }">
						</tr>
					</table>
					-->
					<br>
					<!-- <span style="font-size: 16px;font-weight: bolder;">正文</span> -->
					<div class="con-a">
		                 <h2 >正文</h2>
		                 <div class="con-b">
							<table width="100%" class="table">
								<tr>
									<td class="con-c"><a href="javascript:void(0)" _name="增加介绍" class="f-size"><i class="ico-t1"></i>添加介绍</a>
											<!--<img _id="group_3428a92f557d88d101558016a8720020" _name="abstract" src="/kbase-core/resource/biztpl/images/biztpl_edit2.png" 
												title="新增介绍" style="cursor: pointer;vertical-align: top;width: 40px;float: right;">-->
		                                        <img _id="${param.tplId  }" _abstractType="abstract" _name="abstract" src="${pageContext.request.contextPath }/resource/biztpl/images/biztpl_edit2.png" title="新增介绍" style="cursor: pointer;vertical-align: top;width: 30px;float: right;">
									</td>
								</tr>
								<tr _id="${param.tplId }">
								</tr>
									${htmlData }
							</table>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<div id="openEditPropDiv" class="easyui-window" closed="true" modal="true" title="编辑属性" style="width:50%;min-height:300px;height:50%;overflow: hidden;">
		    <iframe scrolling="auto" id='openEditPropIframe' frameborder="0"  src="" style="width:100%;height:100%;overflow: hidden;"></iframe>
		</div>
		
		<div style="display: none;">
				<textarea id = "textarea"></textarea>
		</div>
		
		<!-- <div id="toolbar-question" style="display:none;">
			<button class="btn btn-default" _id="{0}" _name="btnRemove">删除</button> 
			<button class="btn btn-default" _id="{0}" _name="btnEdit">编辑答案</button>
			<button class="btn btn-default" _id="{0}" _name="btnCancelEdit" style="display:none;">取消编辑</button>
			<button class="btn btn-default" _id="{0}" _name="btnNewAnswer">新增答案</button>
		</div> -->
		
		<%-- 图片插入 --%>
		<jsp:include page="../util/ckEdit-myImage.jsp"></jsp:include>
		<%-- 标签选择 --%>
		<jsp:include page="article_labels.jsp"></jsp:include>
		
		
	</body>
</html>
