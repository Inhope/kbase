<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
		<title>业务模板</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<style type="text/css">
			
		</style>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/js/My97DatePicker/skin/WdatePicker.css"/>
		<style type="text/css">
			.input_style{
				border: 1px solid #DDDDDD;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level0.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ckeditor/plugins/biztpl/plugin-level1.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript">
			$.extend($.fn, {
				getRootPath : function(){
				    return "${pageContext.request.contextPath }";
				}
			});
		
			var tableObject;//因为在biztplCate.js中用到tableObject，故声明于此
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/biztpl/js/biztplCate.js"></script>
		<script type="text/javascript">
			
			//获取
			function getValues(){
				var ztree = $.fn.zTree.getZTreeObj("treeDemo");
				if(ztree == null){
					return null;
				}
				var node = ztree.getSelectedNodes()[0];
				var cateId = "";
				if(node != null && node.id != 'root'){
					cateId = node.id;
				}
				var param = {
					name:$('#name').val(),
					createDate : $('#createDate').val(),
					endDate : $('#endDate').val(),
					status : $('#status').val(),
					isDraft : $('#isDraft').val(),
					cateId : cateId,
					sort : $('#dataList').datagrid("options").sortName,
					order : $('#dataList').datagrid("options").sortOrder
				}
				return param;
			}
			
			function loadData(){
				$('#dataList').datagrid({
					url : $.fn.getRootPath() + "/app/biztpl/biz-tpl!dataJson.htm",
					queryParams :getValues()
				})
			}
			
			$(function(){
				//重置查询条件
				$('#searchBtn').on("click",function(){
					loadData();
				});
				
				//重置查询条件
				$('#resetBtn').on("click",function(){
					$('#name').val("");
					$('#createDate').val("");
					$('#endDate').val("");
					$('#status').val("");
					$.fn.zTree.getZTreeObj("treeDemo").cancelSelectedNode();
				});
				
				//初始化表格数据-左边展开时已自动初始化
				//$('#dataList').datagrid();
				//loadData();
				
				//新增、修改模板
				$("a[name='addBizTpl'],a[name='editBizTpl']").on("click",function(){
					var url = "/app/biztpl/biz-tpl!edit.htm";
					var title = "编辑模板";
					if($(this).attr("name")=="addBizTpl"){
						var ztree = $.fn.zTree.getZTreeObj("treeDemo");
						if(ztree != null){
							var node = ztree.getSelectedNodes()[0];
							var cateId = "";
							var cateName = "";
							if(node != null && node.id != 'root'){
								url = url+"?cateId="+node.id+"&cateName="+node.title+"&cateBh="+node.bh;
							}
						}
					}else if($(this).attr("name")=="editBizTpl"){
						var row = $('#dataList').datagrid('getSelections');
						if (row.length == 1){
							url = url+"?id="+row[0].id;
						}else{
							alert("请选择一条数据",-1);
							return false;
						}
					}
					url = encodeURI(url);
					window.open($.fn.getRootPath() + url);
				});
				
				//删除模板
				$("a[name='delBizTpl']").on("click",function(){
					var rows = $('#dataList').datagrid('getSelections');
					if (rows.length > 0){
						var ids = [];
						$(rows).each(function(i,item){
							ids.push(item.id);
						});
						if (confirm("确定要删除选中的数据吗？")){
							$.ajax({
								type: "POST",
								url: $.fn.getRootPath()+"/app/biztpl/biz-tpl!delete.htm",
								data: "ids="+ids,
								dataType:"json",
								success: function(data) {
									if(data.status){
										loadData();
									}else{
										alert("操作失败");
									}
								}
							});
						}else{
							
						}
					}else{
						alert("请选择数据",-1);
						return false;
					}
				});
				
			});
			function fmtDate(value){
				if(value != undefined){
					return $.fn.datebox.defaults.formatter(new Date(value.time));
				}else{
					return "";
				}
			}
			//格式化状态
			function formatValue(value,row,index){
				var img = $.fn.getRootPath()+"/resource/biztpl/images/" + (value==0?"switch_on.png":"switch_off.png");
				return '<img name="changeStatus" status="'+value+'" rowid="'+row.id+'" style="cursor: pointer;" src="'+img+'" width="40px;">';
			}
			$(function(){
				$("#dataListTable").on("click","[name='changeStatus']",function(){
					var _this = $(this);
					var img = $.fn.getRootPath()+"/resource/biztpl/images/" + (_this.attr("status")==0?"switch_off.png":"switch_on.png");
					var param = {
						id: _this.attr("rowid"),
						status: _this.attr("status")==0?1:0,
						img: img
					};
					$.ajax({
						type: "POST",
						url: $.fn.getRootPath()+"/app/biztpl/biz-tpl!changeStatus.htm",
						data: param,
						dataType:"json",
						success: function(data) {
							_this.attr("status",param.status);
							_this.attr("src",param.img);
							//alert(data);
						}
					});
				});
			});
			function fmtArticleCount(value,row,index){
				if(value == 0){
					return value;
				}else{
					return "<span id='"+row.id+"' style='color:blue;cursor:pointer;' onclick='showArticlesByTplId(\""+row.id+"\")'>"+value+"</span>";
				}
			}
			function showArticlesByTplId(tplId){
				$("#openArticlesIframe")[0].src = "${pageContext.request.contextPath }/app/biztpl/biz-tpl!findArticlesPage.htm?tplId="+tplId;
				$("#openArticlesDiv").dialog("open");
			}
		</script>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'west',split:false" title="" style="width:20%;overflow: hidden;">
			<div class="easyui-accordion" style="width:100%;height:100%;">
				<div title="模板目录" data-options="onExpand:function(){if($('#isDraft').val()==1){$('#isDraft').val(0);loadData();}}" style="overflow:auto;padding:5px;">
					<div style="padding: 5px 10px;">
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnNewCate" >添加</a>  
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnEditCate" >修改</a>  
						<a href="javascript:void(0);" class="easyui-linkbutton" style="width: 40px; border-radius:5px;" id="btnDelCate" >删除</a>  
					</div>
					<div>
						<ul id="treeDemo" class="ztree"></ul>
					</div>
				</div>
				<div title="草稿箱" data-options="onExpand:function(){if($('#isDraft').val()==0){$('#isDraft').val(1);loadData();}}"></div>
			</div>
		</div>
		<div data-options="region:'center',iconCls:'icon-ok'">
			<div class="easyui-layout" style="width:100%;height:100%;">
				<div data-options="region:'north'" style="height:50px;overflow: hidden;">
					<div id="now-toolbar" style="padding:5px;height:auto;">
						<input id="isDraft" type="hidden" value="1">
						模板名称:
						<input id="name" type="text" style="width:120px;" class="input_style">
						创建日期:
						<input id="createDate" type="text" class="input_style" style="width:90px;" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly">
						~
						<input id="endDate" type="text" class="input_style" style="width:90px;" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly">
						状态:
						<select id="status" style="width:90px;" class="input_style">
							<option value="">--全部--</option>
							<option value="0">启用</option>
							<option value="1">停用</option>
						</select>
						<a href="javascript:void(0)" class="easyui-linkbutton" id="searchBtn" iconCls="icon-search">查询</a>
						<a href="javascript:void(0)" class="easyui-linkbutton" id="resetBtn" iconCls="icon-clear">重置</a>
						<a name="addBizTpl" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>
						<a name="editBizTpl" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
						<a name="delBizTpl" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
					</div>
				</div>
				<div data-options="region:'center'" id="dataListTable">
					<div id="openArticlesDiv" class="easyui-dialog" closed="true" modal="true" title=" " style="width:600px;min-height:200px;height:450px;overflow: hidden;">
						<iframe scrolling="auto" id='openArticlesIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
					</div>
					<table id="dataList" data-options="
						method : 'POST',title : '',loadMsg:'加载中...',
						width : '100%',height : '100%',
						nowrap : true,autoRowHeight : false,striped : true,
						rownumbers:true,//行号
						singleSelect: false,//单选
						pagination:true,
						onSortColumn: function (sort, order) {
							loadData();
						}">
						<thead>
							<tr>
								<th data-options="field:'id',width:'2.5%',align:'center',checkbox:true"></th>
								<th data-options="field:'cateName',width:'12.5%',align:'center'">模板路径</th>
								<th data-options="field:'name',width:'20%',align:'center',
									formatter:function(value,row,index){
										return '<span title=\''+value+'\'>'+value+'</span>';
									}">模板名称</th>
								<th data-options="field:'createUserName',width:'12.5%',align:'center'">创建人</th>
								<th data-options="field:'status',width:'12.5%',align:'center',sortable:'true',
									formatter:function(value,row,index){return formatValue(value,row,index);}">状态</th>
								<th data-options="field:'attrCount',width:'5%',align:'center',sortable:'true'">属性</th>
								<th data-options="field:'classCount',width:'5%',align:'center',sortable:'true',
									formatter:function(value,row,index){return fmtArticleCount(value,row,index);}">文章</th>
								<th data-options="field:'createDate',width:'10%',align:'center',sortable:'true',
									formatter:function(value,row,index){return fmtDate(value);}">创建时间</th>
								<th data-options="field:'startDate',width:'10%',align:'center',sortable:'true',
									formatter:function(value,row,index){return fmtDate(value);}">开始日期</th>
								<th data-options="field:'endDate',width:'10%',align:'center',sortable:'true',
									formatter:function(value,row,index){return fmtDate(value);}">结束日期</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>