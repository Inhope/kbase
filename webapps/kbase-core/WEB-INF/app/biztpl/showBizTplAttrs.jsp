<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>模板详情</title>
	<base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<style type="text/css">
		body{margin: 3px;font-size: 12px;}
		span.cke_button__level0_label{display: inline;}
		span.cke_button__level0_icon{width: 0px;}
		span.cke_button__level1_label{display: inline;}
		span.cke_button__level1_icon{width: 0px;}
		span.cke_button__level2_label{display: inline;}
		span.cke_button__level2_icon{width: 0px;}
		span.cke_button__level3_label{display: inline;}
		span.cke_button__level3_icon{width: 0px;}
		span.cke_button__biztpldel_label{display: inline;}
		span.cke_button__biztpldel_icon{width: 0px;}
	</style>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/ztree/css/zTreeStyle/zTreeStyle.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/content.css"/>
	<style type="text/css">
		.kbs-title-level0{
			width: 300px;
			border: 1px solid #eee;
			height: 30px;
			line-height: 30px;
			margin: 1px;
			font-size: 20px;
			font-weight: bolder;
		}
		.kbs-title-level1{
			width: 300px;
			border: 1px solid #eee;
			height: 26px;
			line-height: 26px;
			margin: 1px;
			margin-left: 30px;
			font-size: 16px;
			font-weight: bolder;
		}
		.kbs-title-level2{
			width: 300px;
			border: 1px solid #eee;
			height: 26px;
			line-height: 26px;
			margin: 1px;
			margin-left: 60px;
			font-size: 16px;
			font-weight: bolder;
		}
		.kbs-title-level3{
			width: 300px;
			border: 1px solid #eee;
			height: 26px;
			line-height: 26px;
			margin: 1px;
			margin-left: 90px;
			font-size: 16px;
			font-weight: bolder;
		}
		.kbs-title-cell{
			width:300px;border:1px solid #eee;height:30px;line-height:30px;margin:1px;margin-left:60px;
		}
		body{
			font-family: sans-serif, Arial, Verdana, "Trebuchet MS";
			font-size: 12px;
			color: #333;
			background-color: #fff;
			margin: 20px;
		}
	</style>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/2.3/layer.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/library/ztree/js/jquery.ztree.all-3.5.min.js"></script>
	
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		$(function(){
			var param = {
				id: $("#biztplId").val()
			};
			$.ajax({
				type: "POST",
				dataType : 'json',
				url: '${pageContext.request.contextPath }/app/biztpl/biz-tpl!info.htm',
				data: param,
				async: false,
				success: function(data){
					$("body").append(data);
				}
			});
		});
		
	</script>
</head>
<body>
	<input type="hidden" value="${id }" id="biztplId">
	
</body>
</html>