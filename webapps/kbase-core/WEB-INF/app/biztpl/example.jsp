<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="en">
<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.shl{
			width:628px;
			height:auto;
			background:#F1F1F1;
			overflow: hidden;
			position:absolute;
			z-index:2;
		}
		.shl h2{

			height:30px;
			line-height:40px;
			text-indent:2em;
			font-size:13px;
			color:#333;
		}
		.shl h2 a.shl_close{
			display: inline-block;
			float:right;
			width:40px;
			height:30px;
			background-size:12px 12px;
			cursor:pointer;
		}
		.shl h2 em{
			display: inline-block;
			vertical-align: bottom;
			float:right;
			margin-right:35px;
			width:auto;
			height:30px;
			line-height: 40px;
			background:url(../../resource/biztpl/images/ico-12.png) no-repeat 5px 15px;
			background-size:15px 15px;
			cursor:pointer;
		}
		.shl_con{
			height:100%;
			padding:0px 6px;
		}
		.shl_con_l{
			width:180px;
			height:100%;
			text-indent:10px;
			border:1px solid #aeaeae;
			background:white;
			min-height:300px;
			float:left;
		}
		.shl_con_l ul{
			clear:both;
			padding:8px 0;
		}
		/*.shl_con_l li{*/
			/*height:28px;*/
			/*line-height:28px;*/
			/*font-size:13px;*/
			/*font-weight:bold;*/
		/*}*/
		.shl_con_l input{
			margin-top:5px;
			margin-left:5px;
			outline: 0;
			float:left;
		}

		.shl_con_l input[type=text]{
			width:110px;
			padding: 4px;
			display: inline-block;
			-webkit-border-radius: 2px;
			border: 1px #8aa8ca solid;
		}
		.shl_con_l input[type=button]{
			display: inline-block;
			vertical-align: top;
			cursor: pointer;
			background: #eef6ff;
			text-align: center;
			width:45px;
			height:26px;
			line-height:22px;
			border-radius: 2px;
			font-size: 13px;
			border: 1px #8aa8ca solid;
		}
		.shl_con_r{
			width:426px;
			height:100%;
			float:left;
			margin-left:6px;
			position:relative;
		}
		
	</style>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/biztpl/css/css02.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/gray/easyui.css">
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/datagrid-scrollview.js"></script>
	<script type="text/javascript"><!--
		//relatedObjects 取自 index.jsp数据,每次添加删除修改他的值
		var tplId = '${tplId }';
		var objectId = '${articleId }';
		var compare = new Array();//type ==2
		var related = new Array();//type==1
		$(function () {
			/**
			 * 加载左侧树
			 */
			$('#tree').tree({
				url : '${pageContext.request.contextPath }/app/biztpl/article!slqlTree.htm',
				method : 'post'
			});
			/**
			 * 对比实例点击事件
			 */
			$('#compare').click(function(){
				var node = selectNode();
				if(node){
					node.type = 2;
					if (hasNode(compare, node)){
						$.messager.alert('消息提醒','该文章已经存在!','info');
						return false;
					}
					addNode(node,2);
				}
			});
			/**
			 * 相关实例点击事件
			 */
			$('#related').click(function(){
				var node = selectNode();
				if(node){
					node.type = 1;
					if (hasNode(related, node)){
						$.messager.alert('消息提醒','该文章已经存在!','info');
						return false;
					}
					addNode(node ,1);
				}
			});
			
			//tab切换事件
			$('#tt').tabs({    
    			border:false,    
    			onSelect:function(title){   
        			if(title=='实例对比'){
        				$("#compare").show();
        				$("#related").hide();
        				
        			}else{
        				$("#compare").hide();
        				$("#related").show();
        			}   
    			}    
			});
		});
		
		/**
		 * 判断是否包含node
		 */
		function hasNode(nodes, node){

			if(!nodes || nodes == null || nodes.length < 1){
				return false;
			}
			for(var i = 0; i < nodes.length; i++) {
				if(nodes[i].id == node.id && nodes[i].type == node.type){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 获取树的选择节点
		 * @returns {*}
		 */
		function selectNode() {
			if (objectId=='' || objectId == null) {
				alert('未获取到当前实例id,无法完成操作');
				return false;
			}
			var select = $('#tree').tree('getSelected');
			if(!select.objectId || select.objectId == null || select.objectId == ''){
				alert('请选择实例');
				return false;
			}
			return select;
		}
		
		/**
		 * 添加节点到数组
		 * @param nodes
		 * @param node
		 */
		function addNode(node, type){
			var n = new Object();
			n.id = node.id;
			n.name = node.text;
			n.type = type;
			n.index = "";
			n.article_id =objectId;
			if(type == 1&&!hasNode(related,n)) {
				related.push(n);
				$('#relatedDataList').datagrid('insertRow',{
					index: 0,	
					row: {
					id: n.id,
					name: n.name,
					_operate:"<a href='javascript:void(0)' onclick=\"delExample('"+n.id+"','"+n.type+"',this)\">删除</a>||<a href='javascript:void(0)' onclick='putTop(this)' type='" + n.type+"'_id='" + n.id+"'_name='"+n.name+"'>置顶</a>",
					_dataSource:"<a href='javascript:void(0)' name='手动' >手动</a>"
					}
				});
			} else if (type == 2&&!hasNode(compare,n)){
				compare.push(n);
				$('#compareDataList').datagrid('insertRow',{
					index: 0,	
					row: {
					id: n.id,
					name: n.name,
					_operate:"<a href='javascript:void(0)' onclick=\"delExample('"+n.id+"','"+n.type+"',this)\">删除</a>||<a href='javascript:void(0)' onclick='putTop(this)' type='" + n.type+"'_id='" + n.id+"'_name='"+n.name+"'>置顶</a>",
					_dataSource:"<a href='javascript:void(0)' name='手动' >手动</a>"
					}
				});
			}else{
				$.messager.alert('消息提醒','该文章已经存在!','info');
			}
		}
		/**
		 * 删除
		 */
		function del(id,type){
			if(type == 1){
				removeNode(related,id,type);
			}else if(type == 2){
				removeNode(compare,id,type);
			}
		}
		
		function removeNode(nodes, id, type){
			var dels = new Array();
			for(var i = 0; i < nodes.length; i++) {
				if(nodes[i].id == id && nodes[i].type == type){
					dels.push(i);
				}
			}
			for(var i = dels.length-1; i >= 0; i--) {
				nodes.splice(dels[i],1);
			}
		}
		

		//置顶按钮
		function putTop(obj){
			var _id = $(obj).attr("_id");
			var _name = $(obj).attr("_name");
			var type = $(obj).attr("type");
			var object = {id:_id,name:_name,flag:1,article_id :objectId,type:type};
			if(type=="2"){
				$('#compareDataList').datagrid('insertRow',{
					index: 0,	
					row: {
						id: _id,
						name: _name,
						type:type,
						_operate:"<a href='javascript:void(0)' onclick=\"delExample('"+_id+"','"+type+"',this)\">删除</a>||<a href='javascript:void(0)' onclick='putTop(this)' type='" + type+"'_id='" + _id+"'_name='"+_name+"'>置顶</a>",
						_dataSource:"<a href='javascript:void(0)' name='手动' >手动</a>"
					}
				});
				delShowView(_id,type);
				compare.push(object);
			}else{
				$('#relatedDataList').datagrid('insertRow',{
					index: 0,	
					row: {
						id: _id,
						name: _name,
						type:type,
						_operate:"<a href='javascript:void(0)' onclick=\"delExample('"+_id+"','"+type+"',this)\">删除</a>||<a href='javascript:void(0)' onclick='putTop(this)' type='" + type+"'_id='" + _id+"'_name='"+_name+"'>置顶</a>",
						_dataSource:"<a href='javascript:void(0)' name='手动' >手动</a>"
					}
				});
				delShowView(_id,type);
				related.push(object);
			}
		}
		
		//操作的按钮设置
		function formatOper(val,row,index){  
			
			if (typeof(val) == "undefined"&&row.flag=="0") { 
   				return "<a href='javascript:void(0)' onclick='putTop(this)' type='" + row.type+"'_id='" + row.id+"'_name='"+row.name+"'>置顶</a>";
			}else if(typeof(val) == "undefined"&&row.flag=="1"){
				return "<a href='javascript:void(0)' onclick=\"delExample('"+row.id+"','"+row.type+"',this)\">删除</a>||<a href='javascript:void(0)' onclick='putTop(this)' type='" + row.type+"'_id='" + row.id+"'_name='"+row.name+"'>置顶</a>";
			}else{
				return val;
			}
		} 
		
		//数据来源项
		function formatDataSource(val,row,index){
			if(typeof(val) == "undefined"&&row.flag=="0"){
				return "<a href='javascript:void(0)' >自动</a>";
			}else if(typeof(val) == "undefined"&&row.flag=="1"){
				return "<a href='javascript:void(0)' >手动</a>";
			}else{
				return val;
			}
		}
		
		//删除实例（删除数据库）
		function delExample(id,type){
			var index = -1;
			var rows ;
			var object ={id:id,type:type,article_id:objectId};
			$.messager.confirm('消息提醒', "确定要删除么？", function (data) {
				if(data){
					if(type=="2"){
						rows = $('#compareDataList').datagrid('getRows');
						//取到当前点击删除的行号，这里比较难一点
						for(var i =0;i<rows.length;i++){
							if(rows[i].id==id){
								index = Math.max($('#compareDataList').datagrid('getRowIndex',rows[i]),index);
							}
						}
						$('#compareDataList').datagrid('deleteRow',index);
						//删除对比数组里面的数据
						del(id,2);
				    }else{
						  rows = $('#relatedDataList').datagrid('getRows');
						  for(var i =0;i<rows.length;i++){
							if(rows[i].id==id){
								index = Math.max($('#relatedDataList').datagrid('getRowIndex',rows[i]),index);
							}
						 }
						 $('#relatedDataList').datagrid('deleteRow',index);
						 del(id,1);
					}
		            $.ajax({
					type: "POST",
					url: '${pageContext.request.contextPath }/app/biztpl/example!delExample.htm',
					data: object,
					dataType:"json",
					async:false, 
					success: function(data) {
						if(data.status == 0){
							$.messager.alert('消息提醒','删除成功','info');
						}else{
							$.messager.alert('消息提醒','删除失败','error');
						}
					}
				  });
				}
		     });
		}
		
		//界面删除
		function delShowView(id,type){
			var index = -1;
			var rows ;
			if(type=="2"){
				rows = $('#compareDataList').datagrid('getRows');
				//取到当前点击删除的行号，这里比较难一点
				for(var i =0;i<rows.length;i++){
					if(rows[i].id==id){
						index = Math.max($('#compareDataList').datagrid('getRowIndex',rows[i]),index);
					}
				}
				$('#compareDataList').datagrid('deleteRow',index);
				//删除对比数组里面的数据
				del(id,2);
		    }else{
				  rows = $('#relatedDataList').datagrid('getRows');
				  for(var i =0;i<rows.length;i++){
					if(rows[i].id==id){
						index = Math.max($('#relatedDataList').datagrid('getRowIndex',rows[i]),index);
					}
				 }
				 $('#relatedDataList').datagrid('deleteRow',index);
				 del(id,1);
			}
		}
		//保存
		function save_example(){
		 var _data ={"compare":JSON.stringify(compare),"related":JSON.stringify(related)};
		 var _url = '${pageContext.request.contextPath }/app/biztpl/example!saveExampleData.htm';
		 $.ajax({
				type: "POST",
				url: _url,
				data: _data,
				dataType:"json",
				success: function(result) {
					$.messager.alert('消息提醒','保存成功!','info');
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
				 	alert(XMLHttpRequest.status);
 					alert(XMLHttpRequest.readyState);
 					alert(textStatus);
				}
			  });
		}
		
		//按钮的模糊搜索
		function likeSearch(){
			var articleName = $.trim($("#articleName").val());
			if(articleName !=""){
			$.ajax({
					type : 'post',
					url : '${pageContext.request.contextPath }/app/biztpl/article!searchArticleLikes.htm', 
					data : { articleName: articleName },
					async: false,
					dataType : 'json',
					success : function(jsonResult){
						 if(jsonResult!="0"){
						 	var node = $('#tree').tree('find', "check");
						 	if(node){
						 		$('#tree').tree('remove', node.target);
						 	 	$('#tree').tree('append', {
									data: [{
										id: "check",
										text: '查询结果',
										children:jsonResult
									}]
								});
							}else{
								$('#tree').tree('append', {
									data: [{
										id: "check",
										text: '查询结果',
										children:jsonResult
									}]
								});
							}
						 }else{
						 	$.messager.alert('消息提醒','没有找到相关文章!','info');
						 	$('#tree').tree({
								url : '${pageContext.request.contextPath }/app/biztpl/article!slqlTree.htm',
								method : 'post'
							});
						 }
					},
					error : function(msg){
						alert('网络错误，请稍后再试!');
					}
				});
			}else{
				$('#tree').tree({
					url : '${pageContext.request.contextPath }/app/biztpl/article!slqlTree.htm',
					method : 'post'
				});
			}
		}
</script>
</head>
<body class="easyui-layout">
<input type="hidden" name="tplId" value="${tplId }">
<input type="hidden" name="articleId" value="${articleId }">
<div class="shl" id="toolbar">
	<h2><a class="shl_close" javascript:void(0)></a><em id="save_example" onclick="save_example()">保存</em>
	<em id="compare">添加实例对比</em><em id="related" style="display:none">添加实例关联</em>
	</h2>
	<div class="shl_con">
		<div class="shl_con_l" >
			<div style="float:left;">
			<input id="articleName" type="text" placeholder="输入文章名称"> <input type="button" onclick="likeSearch()" value="搜索">
			</div>
			<div class="easyui-panel" style="float:left;width:180px;height: 300px;border: none;">
			<ul id="tree"></ul>
			</div>
		</div>
		
		<div id="tt" class="easyui-tabs" style="width:434px;height:auto">
	     	<div title="实例对比"  style="width:434px;height:300px;float:left">
				<table id="compareDataList" class="easyui-datagrid" title="" style="width:434px;height:300px"
					data-options="view:scrollview,rownumbers:true,singleSelect:true,
                    queryParams: {tpl_id:'${tplId }',article_id:'${articleId }'},
					url:'${pageContext.request.contextPath }/app/biztpl/example!exampleCompareListData.htm',
                    autoRowHeight:false,pageSize:10">
					<thead>
					 <tr>
	                    <th data-options="field:'id',width:30,align:'center',checkbox:true"></th>
							<th data-options="field:'name',width:170,align:'center'">实例名称</th>
							<th data-options="field:'_operate',width:110,align:'center',
								formatter:function(value,row,index){return formatOper(value,row,index)}">操作</th>
							<th data-options="field:'_dataSource',width:80,align:'center',
								formatter:function(value,row,index){return formatDataSource(value,row,index)}">数据来源</th>
	                </tr>
					</thead>
				</table>
			</div>
			<div title="实例关联"  style="width:434px;height:300px;float:left;">
				<table id="relatedDataList" class="easyui-datagrid" title="" style="width:434px;height:300px"
					data-options="view:scrollview,rownumbers:true,singleSelect:true,
                    queryParams: {tpl_id:'${tplId }',article_id:'${articleId }'},
					url:'${pageContext.request.contextPath }/app/biztpl/example!exampleRelatedListData.htm',
                    autoRowHeight:false,pageSize:10">
					<thead>
					 <tr>
	                    <th data-options="field:'id',width:30,align:'center',checkbox:true"></th>
							<th data-options="field:'name',width:170,align:'center'">实例名称</th>
							<th data-options="field:'_operate',width:110,align:'center',
								formatter:function(value,row,index){return formatOper(value,row,index)}">操作</th>
							<th data-options="field:'_dataSource',width:80,align:'center',
								formatter:function(value,row,index){return formatDataSource(value,row,index)}">数据来源</th>
	                </tr>
					</thead>
				</table>
			</div>
		</div>
		
	</div>
</div>


</body>
</html>