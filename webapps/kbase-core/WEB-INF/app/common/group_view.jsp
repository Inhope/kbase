<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>群组列表</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.datagrid-body{
				overflow: hidden;
			}
			.datagrid-row{
				height: 30px;
			}
			.datagrid-toolbar {
			  height: auto;
			  padding: 1px 2px;
			  border-width: 0 0 1px 0;
			  border-style: solid;
			  height: 25px;
			}			
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/plugins/easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>		
		<!-- layer -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.extend.js"></script>
		
		<script type="text/javascript">
			$(function(){
			    //刷新
			    $('a[name="btnRefresh"]').on('click',function(){
			        $('#groupGrid').datagrid('reload');
			    });
			    //新增
			    $('a[name="btnNew"]').on('click',function(){
					$.layer({
						    id: 'edit_group',
							type: 2,
						    title: '群组',
						    shade: [0.3, '#000'],
						    border: [5, 0.3, '#000'],
						    area: ['820px', '500px'],
						    iframe: {src: '${pageContext.request.contextPath}/app/common/group!edit.htm'}
						});			    
			    });			
			    //删除
			    $('a[name="btnDel"]').on('click',function(){
			        var ids = '';
			        var rows = $('#groupGrid').datagrid('getSelections');
			        if(rows.length==0){
			            alert('请选择需要删除的群组!');
			            return false;
			        }
			        for(i=0;i<rows.length;i++){
			            var row = rows[i];
			            ids += row.id + ',';
			        }
			        if(confirm('确定要删除选中的群组信息?')){
				        var _win_kbs_index = layer.load('请稍等...');
				        $.get('${pageContext.request.contextPath}/app/common/group!delete.htm?ids='+ids,function(data){
				            layer.close(_win_kbs_index);
				            if(data.success){
				               alert('删除成功!');
				               $('#groupGrid').datagrid('reload');
				            }else{
				               alert('删除失败!');
				            }
				        },'json')			        
			        }
			    });	
			    //		
				$('#groupGrid').datagrid({
					onDblClickRow: function(index, row){
					     $.layer({
						    id: 'edit_group',
							type: 2,
						    title: '群组',
						    shade: [0.3, '#000'],
						    border: [5, 0.3, '#000'],
						    area: ['820px', '500px'],
						    iframe: {src: '${pageContext.request.contextPath}/app/common/group!read.htm?id='+row.id}
						});							   
					}
				});
			});
		</script>
	</head>

	<body>
        <div id="tb-groupview" style="padding:5px;">
        	<div style="float:right;">
        		<a href="javascript:void(0)" class="easyui-linkbutton" name="btnRefresh">刷新</a> 
				<a href="javascript:void(0)" class="easyui-linkbutton" name="btnNew">新增</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" name="btnDel">删除</a>
        	</div>
		</div>	
		<table id="groupGrid" style="overflow:hidden;height:400px;width: 100%;" 
		    url="${pageContext.request.contextPath }/app/common/group!list.htm"
	        rownumbers="true" pagination="true" data-options="toolbar:'#tb-groupview', singleSelect:false, pageList: [10]">
			<thead>
				<tr>
					<th field="groupname" width="40%">群组名称</th>
					<th field="createuser" width="15%">创建人</th>
					<th field="createtime" width="15%">创建时间</th>
					<th field="edituser" width="15%">最后修改人</th>	
					<th field="edittime" width="15%">最后修改时间</th>				
				</tr>
			</thead>
		</table>
	</body>
</html>
