<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>分类审核人维护</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-important{
			    color:red;
			    margin-left: 3px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js?20161011"></script>
		<script type="text/javascript">
		$(function(){
		    //保存
		    $('#btnSave').on('click', function(){
		        var id = $('#id').val();
		        var category = $('#category').val();
		        var categoryId = $('#categoryId').val();
		        var users = $('#users').val();
		        var userIds = $('#userIds').val();
		        var users1 = $('#users1').val();
		        var userIds1 = $('#userIds1').val();		        
		        if($.trim(users)==''){
		            alert('请选择专家坐席!');
		            return false;
		        }	
		        if($.trim(users1)==''){
		            alert('请选择业务知识管理员!');
		            return false;
		        }			        
		        var params = {
		            'categoryAuditor.id' : id,
		            'categoryAuditor.category' : category,
		            'categoryAuditor.categoryId' : categoryId,
		            'categoryAuditor.users' : users,
		            'categoryAuditor.userIds' : userIds,
		            'categoryAuditor.users1' : users1,
		            'categoryAuditor.userIds1' : userIds1		            
		        }	
		        var _win_kbs_index = layer.load('请稍等...');
		        $.post('${pageContext.request.contextPath}/app/common/category-auditor!save.htm',params,
		            function(data){
		                layer.close(_win_kbs_index);
		                if(data.success){ 
		                   alert('保存成功');
		                   location.href = '${pageContext.request.contextPath}/app/common/category-auditor!edit.htm?id=' + data.message;
		                }else{
		                   alert(data.message);
		                }
		            }, 'json');	        		               
		    });
		    
			//////////////////////////////////////////////////
			//选择专家坐席
		    $('#users').click(function(){
		          $.kbase.picker.multiUserByDept({returnField:"users|userIds",diaWidth:540,diaHeight:400});
		    });
			///////////////////////////////////////////////////////////////////	
			//////////////////////////////////////////////////
			//选择业务知识管理员
		    $('#users1').click(function(){
		          $.kbase.picker.multiUserByDept({returnField:"users1|userIds1",diaWidth:540,diaHeight:400});
		    });
			///////////////////////////////////////////////////////////////////					    
		});
		</script>
		
	</head>

	<body>
		<input type="hidden" id="id" value="${categoryAuditor.id }" />
        <input id="userIds" type="hidden" value="${categoryAuditor.userIds }"/>
        <input id="userIds1" type="hidden" value="${categoryAuditor.userIds1 }"/>
		<input id="category" type="hidden" value="${categoryAuditor.category }"/>	
		<input id="categoryId" type="hidden" value="${categoryAuditor.categoryId }"/>			
		
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">分类</td>
				<td width="75%" >${categoryAuditor.category}</td>
			</tr>
			<tr>
				<td>专家坐席<span class="kbs-important">*</span></td>
				<td colspan="2">
				    <textarea id="users" rows="5" style="width:550px;border:1px solid #cdcdcd;" readOnly="readOnly">${categoryAuditor.users}</textarea>
				</td>
			</tr>	
			<tr>
				<td>业务知识管理员<span class="kbs-important">*</span></td>
				<td colspan="2">
				    <textarea id="users1" rows="5" style="width:550px;border:1px solid #cdcdcd;" readOnly="readOnly">${categoryAuditor.users1}</textarea>
				</td>
			</tr>						
			<tr>
				<td align="right" colspan="3">
					<input type="button" value="保存" id="btnSave">
				</td>
			</tr>
		</table>
	</body>
</html>