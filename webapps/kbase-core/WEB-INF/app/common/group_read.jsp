<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>编辑群组</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-important{
			    color:red;
			    margin-left: 3px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/plugins/easyui.min.js"></script>
		<script type="text/javascript">
		$(function(){
		    $('#btnEdit').on('click',  function(){
		         location.href = '${pageContext.request.contextPath}/app/common/group!edit.htm?id=' + $('#id').val();
		    })
		});
		</script>
		
	</head>

	<body>
		<input type="hidden" id="id" value="${group.id }" />
		
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">群组名称</td>
				<td width="75%" style="border-right-style:hidden">${group.groupName }</td>
			</tr>
			<tr>
				<td>可用本群组角色</td>
				<td colspan="2">${group.authorizeScopeDesc }</td>
			</tr>			
			<tr>
				<td>发送目标成员架构</td>
				<td colspan="2">${group.groupScopeDesc }</td>
			</tr>			
			<tr>
				<td align="right" colspan="3">
					<input type="button" value="编辑" id="btnEdit">
				</td>
			</tr>
		</table>
	</body>
</html>