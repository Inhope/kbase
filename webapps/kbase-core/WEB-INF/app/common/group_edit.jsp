<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>编辑群组</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
        <style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
			.kbs-important{
			    color:red;
			    margin-left: 3px;
			}
			.bc_btn{
	  		    color: blue;
	  		    cursor: pointer;
	  		}
	  		
	  		.ke-container{
	  		    float:left;
	  		}
		</style>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js?20161011"></script>
		<script type="text/javascript">
		$(function(){
		    //群组成员选择
		    $('#groupScopeDesc').on('click', function(){
		        $.kbase.picker.multiDeptForGroup({returnField:"groupScopeDesc|groupScope", title:"请选择部门", diaHeight:450}); 
		    });
		    //清空群组成员
		    $('#btnClearGroupScope').on('click',function(){
		        $('#groupScope').val('');
		        $('#groupScopeDesc').val('');
		    })
		    //保存
		    $('#btnSave').on('click', function(){
		        var id = $('#id').val();
		        var groupType = $('#groupType').val();
		        var groupName = $('#groupName').val();
		        var authorizeScope = $('#authorizeScope').val();
		        var authorizeScopeDesc = $('#authorizeScopeDesc').val();
		        var groupScope = $('#groupScope').val();
		        var groupScopeDesc = $('#groupScopeDesc').val();	
		        
		        if($.trim(groupName)==''){
		            alert('请输入群组名称!');
		            return false;
		        }	 
		        if($.trim(authorizeScope)==''){
		            alert('请选择可用本群组角色!');
		            return false;
		        }	
		        if($.trim(groupScope)==''){
		            alert('请选择发送目标成员架构!');
		            return false;
		        }	
		        var params = {
		            'group.id' : id,
		            'group.groupType' : groupType,
		            'group.groupName' : groupName,
		            'group.authorizeScope' : authorizeScope,
		            'group.authorizeScopeDesc' : authorizeScopeDesc,
		            'group.groupScope' : groupScope,
		            'group.groupScopeDesc' : groupScopeDesc
		        }	
		        var _win_kbs_index = layer.load('请稍等...');
		        $.post('${pageContext.request.contextPath}/app/common/group!save.htm',params,
		            function(data){
		                layer.close(_win_kbs_index);
		                if(data.success){ 
		                   alert('保存成功');
		                   location.href = '${pageContext.request.contextPath}/app/common/group!read.htm?id=' + data.message;
		                }else{
		                   alert('保存失败');
		                }
		            }, 'json');	        		               
		    });
			// 角色回显
			$('#role_select').combotree('setValues', $('#authorizeScope').val().split(','));
		});
		</script>
		
	</head>

	<body>
		<input type="hidden" id="id" value="${group.id }" />
		<input type="hidden" id="groupType" value="1" /><!-- 群组类型，暂定只有部门群组 -->
		<input type="hidden" id="groupScope" value="${group.groupScope }" />
        <input id="authorizeScopeDesc" type="hidden" value="${group.authorizeScopeDesc }"/>
		<input id="authorizeScope" type="hidden" value="${group.authorizeScope }"/>		
		
		<table cellspacing="0" cellpadding="0" class="box" style="width:100%">
			<tr>
				<td width="15%">群组名称<span class="kbs-important">*</span></td>
				<td width="75%" style="border-right-style:hidden">
				    <input id="groupName" type="text" style="width:550px;" value="${group.groupName }">
				</td>
			</tr>
			<tr>
				<td>可用本群组角色<span class="kbs-important">*</span></td>
				<td colspan="2">
						<ul class="easyui-combotree" id="role_select" name="authorizeScope" style="width: 550px;" 
							data-options="url:'${pageContext.request.contextPath}/app/util/choose!roleData.htm',
							lines: true, multiple: true,
			    			loadFilter:function(data, parent){
	                    	 		if(data) {
	                    	 			$(data).each(function(i, item){
	                    	 				item.text = item.name;
	                   	 					item.state = 'open';
	                     	 		});
	                    	 		}
	                    	 		return data;
				   			},
				   			onCheck:function(node, checked){
				   				var roleIds = '';
				   				var roleDescs = '';
				   				var t = $('#role_select').combotree('tree');	/*获取树对象*/
								var nodes = t.tree('getChecked');	/*获取选择的节点*/
				   				if(nodes) $.each(nodes, function(i, e){
				   					roleIds += ',' + e.id;
				   					roleDescs += ',' + e.text;
				   				});
				   				roleIds = roleIds.replace(/^,|,$/g, '');
				   				$('#authorizeScope').val(roleIds);
				   				$('#authorizeScopeDesc').val(roleDescs);
				   			}"></ul>
				</td>
			</tr>			
			<tr>
				<td>发送目标成员架构<span class="kbs-important">*</span></td>
				<td colspan="2">
					<textarea id="groupScopeDesc" rows="5" style="width:550px;border:1px solid #cdcdcd;" readOnly="readOnly">${group.groupScopeDesc }</textarea>
					<input type="button" value="清空" id="btnClearGroupScope">
				</td>
			</tr>			
			<tr>
				<td align="right" colspan="3">
					<input type="button" value="保存" id="btnSave">
				</td>
			</tr>
		</table>
	</body>
</html>