<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>分类审核人员</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
		<style type="text/css">
			body{
				margin: 2px;
				font-size: 12px;
			}
			.easyui-layout{
			    width:100%; 
			    height:500px;
			}
		</style>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>

		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript">
				var setting = {
					async : {
						enable : true,
						url : '${pageContext.request.contextPath}/app/main/ontology-category!list.htm',
						autoParam : ['id', 'name=n', 'level=lv','bh'],
						otherParam : {},
						dataFilter: ajaxDataFilter
					},
					callback : {
						onClick : onClick
					}
				};
				
				function ajaxDataFilter(treeId, parentNode, responseData) {
				    return responseData;
				};
				
				function onClick(event, treeId, treeNode, clickFlag) {
				    var _url = '${pageContext.request.contextPath}/app/common/category-auditor!edit.htm?cid='+treeNode.id+"&cname="+encodeURI(treeNode.name);
				    $('#category_auditor_edit').attr('src',_url)
		        }		
			$(document).ready(function(){
			    var ztreeObj = $.fn.zTree.init($('#treeDemo'), setting);
			});
		</script>	
	</head>

	<body>
		<div class="easyui-layout" >
		    <div region="west" split="true" title="知识分类" style="width:240px;">
			    <div class="ztree" id="treeDemo"></div>
		    </div>  
		    <div region="center" title="审核人" style="padding:1px;">
		    <iframe id="category_auditor_edit" src="" style="width:99%;height:450px;" frameborder="no"></iframe>
		    </div> 
		</div>
	</body>
</html>
