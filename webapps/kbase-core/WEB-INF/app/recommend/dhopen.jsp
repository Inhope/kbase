<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>知识推荐</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/buttons.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/table-form.css"/>
		<style type="text/css">
			body{
				margin: 3px;
				font-size: 12px;
			}
		</style>
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<script type="text/javascript">
			//add by eko.zhan at 2015-10-10 13:16 判断当前页面是否内嵌在iframe中，true 为内嵌，false为不内嵌
			window.__kbs_has_parent = !(window.parent==window);
		</script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/layer/layer.min.js"></script>
		<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery.picker.js"></script>
		
		<script type="text/javascript">
			$(function(){
				var _id = '${param.id}';
				if (_id.length==32){
					var _time = '${requestScope.time}';
					$('input[name="recommendtime"][value="' + _time + '"]').attr('checked', 'checked');
				}
			
				$('#recommendscope').click(function(){
					$.kbase.picker.recommenddeptsearch({returnField: 'recommendscope|recommendscopeid'});
				});
				
				$('#btnClear').click(function(){
					$('#recommendscope').val('');
					$('#recommendscopeid').val('');
				});
				
				$('#btnRecommand').click(function(){
					var endtime = $('input[name="recommendtime"]:checked').val();
					var deptid = $('#recommendscopeid').val();
					
					var _data = {
						categoryId: '${rquestScope.cateid}',
						deptId: deptid,
						endTime: endtime,
						objId: '${requestScope.valueid}',
						recomId: ''
					};
					$.post('${pageContext.request.contextPath }/app/recommend/recommend!save.htm', _data, function(data){
						parent.layer.alert(data.message,-1);
						if (window.__kbs_has_parent){
							parent.layer.close(parent.__kbs_layer_index);
						}else{
							window.close();
						}
					}, 'json');
				});
			});
		</script>
	</head>

	<body>
		<table class="box" style="width:100%;height:100%;" cellpadding="1" cellspacing="1" align="center">
			<tr>
				<td width="15%">推荐范围</td>
				<td width="85%">
					<input type="text" id="recommendscope" value="${requestScope.deptdesc }" style="width:72%;"/>
					<input type="hidden" id="recommendscopeid" value="${requestScope.deptid }" />
					<input type="button" value="清空" id="btnClear">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>推荐周期</td>
				<td>
					<input type="radio" name="recommendtime" id="day" value="day" checked="checked" />日
					<input type="radio" name="recommendtime" id="week" value="week" />周
					<input type="radio" name="recommendtime" id="mooth" value="mooth" />月
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right"><input type="button" value="推荐" id="btnRecommand">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
