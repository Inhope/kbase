<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>知识库</title>
    
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/recommend/css/recom.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/recommend/css/css.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<SCRIPT type="text/javascript">
		$(function() {
			$('div.zhishi_con_nr ul li h1 b').click(function() {
				var askContent = $(this).attr('categoryId').split(',')[0];
				var searchMode = $(this).attr('categoryId').split(',')[1];
				parent.TABOBJECT.open({
					id : 'values',
					name : '知识展示',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/search.htm?searchMode='+searchMode+'&&askContent=' + askContent,
					isRefresh : true
				}, this);
		
			});
		});
	</SCRIPT>
  
  </head>
  
  <body>
    

<!--******************内容开始***********************-->

<div class="content_right_bottom">

<div class="zhishi_titile">
<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico.jpg" width="16" height="15" />
<span id="categoryNv"></span>
</div>


<div class="zhishi_con">

<div class="zhishi_con_title">
<table>
<tr>
<td class="biaoti"><a href="javascript:void(0);"><b>名称</b></a></td>
	<td><a href="javascript:void(0);">开始时间</a></td>
	<td><a href="javascript:void(0);">结束时间</a></td>
	<td><a href="javascript:void(0);">推荐时间</a></td>
</tr>
</table>
</div>


<div class="zhishi_con_nr">
<ul>
	
<s:iterator var="rems" value="#request.recommend" status='st'>
	<s:if test="#st.last">
		<li>
	</s:if>
	<s:else>
		<li style="background:url(${pageContext.request.contextPath}/theme/red/resource/search/images/right_line.jpg) repeat-x bottom;">
	</s:else>
		<h1 style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;text-align:left;width:39%;">
			<img src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/main/images/zhishi_ico3.jpg" width="16" height="15" />
			<b style="cursor:pointer" categoryId="${rems.kbVal.question},${searchMode}">
				${rems.kbVal.question}
			</b>
		</h1>
		<h2><s:date format="yyyy-MM-dd" name="#rems.startTime"/><s:if test="#rems.startTime==null">/</s:if></h2>
		<h3><s:date format="yyyy-MM-dd" name="#rems.endTime"/><s:if test="#rems.endTime==null">/</s:if></h3>
		<h4><s:date format="yyyy-MM-dd" name="#rems.createTime"/><s:if test="#rems.createTime==null">/</s:if></h4>
	</li>
</s:iterator>

</ul>
</div>
</div>

</div>




<!--******************内容结束***********************-->
  </body>
</html>
