<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/css.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/fav-clip.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/fav/css/custom.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/css.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/css/yonghu.css" rel="stylesheet" type="text/css" />
		<style type= "text/css" >  
			td{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
			.gonggao_titile_right a{
	  			margin: 5px;
	  		}
	  		table.kbs-dialog{
	  			font-size:12px;table-layout:fixed;margin:10px;border:1px solid #eee;
	  		}
	  		table.kbs-dialog tr td{
	  			border:1px dotted #eee;
	  		}
	  		table.kbs-dialog tr td input.text{
	  			border:1px solid #eee;height:25px;line-height:25px;width:280px;
	  		}
	  		table.kbs-dialog tr td input.button{
	  			padding-left:5px;padding-right:5px;padding-top:3px;padding-bottom:3px;border:1px solid #7E7E7E;background-color:#eee;cursor:pointer;
	  		}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.excheck-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exedit-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.exhide-3.5.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/recommend/js/recommend.js"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	</head>

	<body>
		<!--******************内容开始***********************-->
		<div class="gonggao_con">
			<div class="gonggao_con_nr">
				<table width="100%" border="0" id="corrtable" cellspacing="0" cellpadding="0" style="font-size: 12px;table-layout:fixed;">
					<tr>
						<td width="5%"></td>
						<td width="30%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="10%"></td>
						<td width="35%"></td>
					</tr>
					<tr>
						<td colspan="6" align="right" valign="middle" style="text-align:right;">
							<div class="gonggao_titile_right">
								<a href="javascript:void(0);" id="del">删除</a>
								<a href="javascript:void(0);" id="edit">修改</a>
							</div>
						</td>
					</tr>
					<tr class="tdbg">
						<td><input type="checkbox" class="sel_all" /></td>
						<td>名称</td>
						<td>开始时间</td>
						<td>结束时间</td>
						<td>推荐时间</td>
						<td>推荐范围</td>
					</tr>
					<s:set name="nowTime" value="new java.util.Date()"></s:set>
					<s:iterator var="rems" value="#request.recommend">
						<tr>
							<td><input type="checkbox" class="sel_one" id="${rems.id}" objId="${rems.kbVal.valueId}" /></td>
							<td>
								<b>
									<s:if test="%{#rems.kbVal==null || #rems.kbVal.question == null || #rems.kbVal.question == '' 
										|| #rems.kbVal.kbObject == null || #rems.kbVal.kbObject.objectId == null || #rems.kbVal.kbObject.objectId == ''}">
										标准问或者文章不存在
									</s:if>
									<s:else>
										<span title="${rems.kbVal.question}">${rems.kbVal.question}</span>
										<s:if test="#rems.endTime!=null  && #rems.endTime.getTime() < #nowTime.getTime()">
											<span style='color: red;'>(已失效)</span>
										</s:if>
									</s:else>
								</b>
							</td>
							<td>
								<s:date format="yyyy-MM-dd" name="#rems.startTime" />
								<s:if test="#rems.startTime==null">/</s:if>
							</td>
							<td>
								<s:date format="yyyy-MM-dd" name="#rems.endTime" />
								<s:if test="#rems.endTime==null">/</s:if>
							</td>
							<td>
								<s:date format="yyyy-MM-dd" name="#rems.createTime" />
								<s:if test="#rems.createTime==null">/</s:if>
							</td>
							<td>
								<span title="${rems.deptNames}">
									${ rems.deptNames}
								</span>
							</td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>

		<!--******************内容结束***********************-->
		<jsp:include page="/WEB-INF/app/search/Recommend.jsp"></jsp:include>
	</body>
</html>