<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分配置项-新增、编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: auto; }
			table.box table.box th,table.box table.box td{text-align: center; }
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/ConfigSave.js"></script>
		<script type="text/javascript">
			$(function(){
				ConfigSave.set({
					dialogId: '${param.dialogId }' /*当前窗口id*/
				});
			});
		</script>
	</head>
	<body>
		<form id="form1" action="" method="post">
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th>
							配置名称：
						</th>
						<td>
							<input class="easyui-textbox" name="pointsname" 
								value="${config.pointsname }" 
								data-options="required:true" style="width:300px" >
							<input type="hidden" name="id" value="${config.id }">
						</td>
					</tr>
					<tr>
						<th>
							配置单位：
						</th>
						<td>
							<input class="easyui-textbox" name="pointsunit" 
								value="${config.pointsunit }"
								data-options="required:true" style="width:50px" >
						</td>
					</tr>
					<tr>
						<th>
							初始化分数：
						</th>
						<td>
							<input class="easyui-textbox" value="${config.pointsinit }" name="pointsinit"
								data-options="required:true, validType:['digits']" style="width:50px" >
						</td>
					</tr>
					<tr>
						<th>
							积分上限、下限：
						</th>
						<td>
							<input class="easyui-textbox" value="${config.pointsmax }" id="pointsmax" name="pointsmax"
								data-options="required:true, validType:['digits', 'min[0]']" style="width:50px" >（上限）
							-
							<input class="easyui-textbox" value="${config.pointsmin }" id="pointsmin" name="pointsmin"
								data-options="required:true, validType:['digits', 'min[0]']" style="width:50px" >（下限）
						</td>
					</tr>
					<tr>
						<th>
							备注：
						</th>
						<td>
							<input class="easyui-textbox" id="remark" name="remark" 
								value="${config.remark }"
								data-options="multiline:true,value:''" style="width:300px;height:50px">
						</td>
					</tr>
				</table>
				<div style="height: 28px; width: 99%; padding: 10px 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="btn-save" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
