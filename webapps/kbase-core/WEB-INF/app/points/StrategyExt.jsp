<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分自定义策略</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/StrategyExt.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid') /*datagrid对象*/
				});
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar">
			<div>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"    id="btn-add">新增</a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"   id="btn-edit">编辑</a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="btn-remove">删除</a>
		    </div>
		</div>
		<!-- 列表 -->
    	<table class="easyui-datagrid" id="now-grid" data-options="url:'${pageContext.request.contextPath }/app/points/p-strategy-ext!loadData.htm',
    		queryParams:{strategyId: '${param.strategyid }'},border:false,fitColumns:true,singleSelect:true,
    		rownumbers:true,fit:true,toolbar:'#now-toolbar',nowrap:false,
    		onLoadSuccess:function(data){
    			var sgExtDatas = {};
    			if(data) $(data.rows).each(function(){
    				sgExtDatas[this.id] = {
    					name: this.name,
    					rangIds: this.rangIds,
    					rangNames: this.rangNames,
    					rangBhs: this.rangBhs
    				};
    			});
    			/*已有自定策略部门、分类勾选数据（用于当前自定义策略的冲突判断）*/
    			StrategyExt.fn.datas.set('sgExtDatas', sgExtDatas);
    		}">
		    <thead>
		        <tr>
		        	<th data-options="field:'id',checkbox:true"></th>
		        	<th data-options="field:'name',width:100">自定义策略名称</th>
		        	<th data-options="field:'rangNames',width:400, 
		        		formatter: function(value,row,index){
							var val = '';
							val += '<span style=font-weight:bold>分类：</span>' + PageUtils.fn.arrayToString(value['CATALOG'], ',');
							val += '<br/>';
							val += '<span style=font-weight:bold>部门：</span>' + PageUtils.fn.arrayToString(value['DEPT'], ',');
							return val;
					}">规则</th>
		        	<th data-options="field:'createUser',width:100">创建人</th>
		        	<th data-options="field:'createDate',width:100">创建时间</th>
		        </tr>   
		    </thead>
		</table>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>
