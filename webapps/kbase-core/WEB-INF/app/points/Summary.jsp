<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>小i知识库积分-积分概要</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/resource/point/css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/superslide.2.1.js"></script>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			.table_a {width: 32%;margin-right:9px;}
			.table_all {padding: 15px 10px;}
			.jifen_pic{padding: 15px 0 10px 80px}
			.graph{height: 195px}
		</style>
		<script type="text/javascript">
			//下拉框事件
			function pointsChange(pRequest, configKey){
				var cycleType;
				if(pRequest == 'sortActkey'){
					$("li[id^=actkey_]").each(function(index,element){
						var clazz = $(this).attr("class");
						if(clazz == "b_focus"){
							cycleType  = index+1;
						}
					});
				}else if (pRequest == 'sortUser'){
					$("li[id^=user_]").each(function(index,element){
						var clazz = $(this).attr("class");
						if(clazz == "b_focus"){
							cycleType  = index+1;
						}
					});
				}
				if(cycleType != null){
					pointsRequest(pRequest, cycleType);
				}
			}
			
			//日、周、月切换事件
			function pointsRequest(pRequest, cycleType){
				var url,configKey,handleResult = '';
				if(pRequest == 'sortActkey'){//积分动作
					url = '/app/point/p-manager!sortActkey.htm';
					configKey = $('#configActkey  option:selected').val();
					handleResult = function(data){
						var tab_Actkey = "";
						var i = 0
						for(; i<data.length; i++){ 
					       tab_Actkey += '<tr><td class="num_0'+(i+1)+'"></td><td>'+data[i].actdesc+'</td><td><a href="###">'+data[i].points+'</a></td><td><a href="###">'+data[i].exenumber+'</a></td></tr>'
					    }
				     	for(; i<5; i++){
				     		tab_Actkey += '<tr><td class="num_0'+(i+1)+'"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>'
				     	}
				     	$('#tab_Actkey tr').each(function(index,element){
				     		if(index>0)$(element).remove();
				     	});
				     	$("#tab_Actkey").append(tab_Actkey);
				     	
				     	//修改按钮样式
						$("li[id^=actkey_]").each(function(){
							$(this).removeClass();
						});
						$("#actkey_"+cycleType).attr("class","b_focus");
					}
				}else if (pRequest == 'sortUser'){//积分用户
					url = '/app/point/p-manager!sortUser.htm';
					configKey = $('#configUser  option:selected').val();
					handleResult = function(data){
						var tab_User = "";
						var i = 0
						for(; i<data.length; i++){  
					         tab_User += '<tr><td class="num_0'+(i+1)+'"></td><td>'+data[i].username+'</td><td><a href="###">'+data[i].points+'</a></td><td><a href="###">'+data[i].exenumber+'</a></td></tr>'
					    }
					    for(; i<5; i++){
					    	tab_User += '<tr><td class="num_0'+(i+1)+'"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>'
					    }
					    $('#tab_User tr').each(function(index,element){
				     		if(index>0)$(element).remove();
				     	});
					    $("#tab_User").append(tab_User);
					    
					    //修改按钮样式
						$("li[id^=user_]").each(function(){
							$(this).removeClass();
						});
						$("#user_"+cycleType).attr("class","b_focus");
					}
				}
				/*
				else if (pRequest == 'selectAll'){//总积分
					url = '/app/point/p-manager!selectAll.htm';
					handleResult = function(data){
						var ul_All = "";
						for(var i = 0; i<data.length; i++){
							ul_All += '<li><div class="jifen_a a_bg"><h1>总'+data[i].pointsname+'</h1><h2>'+data[i].points+'</h2></div></li>';
						}
						$('#ul_All li').each(function(){
				     		$(this).remove();
				     	});
				     	$("#ul_All").append(ul_All);
					}
				}else if (pRequest == 'selectToday'){//今日积分
					url = '/app/point/p-manager!selectToday.htm';
					handleResult = function(data){
						var ul_Today = "";
						for(var i = 0; i<data.length; i++){
							ul_Today += '<li><div class="jifen_a a_bg"><h1>今日'+data[i].pointsname+'</h1><h2>'+data[i].points+'</h2></div></li>';
						}
						$('#ul_Today li').each(function(){
				     		$(this).remove();
				     	});
				     	$("#ul_Today").append(ul_Today);
					}
				}
				*/
				
				if(url != ''){
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + url, 
						data : {'configKey':configKey,'cycleType':cycleType},
						dataType : 'json',
						async: true,
						success : function(data){
							if(data.result!='-1'){
								handleResult(data);
								return true;
							}
						},
						error : function(msg){
							parent.layer.alert("网络错误，请稍后再试!", -1);
						}
					});
				}
			}
			
			
			$(document).ready(function(){
				pointsRequest('sortActkey', 1);
				pointsRequest('sortUser', 1);
				//pointsRequest('selectAll');
				//pointsRequest('selectToday');
				
				//轮播效果
				$(".fullSlide").each(function(){
					$(this).find(".prev,.next").hide();
					$(this).hover(function() {
					    $(this).find(".prev,.next").stop(true, true).fadeTo("show", 0.5);
					},
					function() {
					    $(this).find(".prev,.next").fadeOut();
					});
					
					$(this).slide({
					    titCell: ".hd ul",
					    mainCell: ".bd ul",
					    effect: "fold",
					    autoPlay: true,
						autoPage: true,
					    trigger: "click",
					    startFun: function(i, c, slider) {
					    	/*
					    	var curLi = $(slider).find(".bd li").eq(i);
					        if (!!curLi.attr("_src")) {
					            curLi.css("background-image", curLi.attr("_src")).removeAttr("_src");
					        }
					    	*/
					        $(slider).parent().find(".yellow").each(function(index){
					        	if(index == i){
					        		$(this).show();
					        	}else{
					        		$(this).hide();
					        	}
					        });
					    }
					});
				});
			});
		</script>
	</head>
	<body>
		<div class="mainr_box">
			<div class="graph">
				<div class="jifen_pic">
					<div class="jifen_all">
						<div class="fullSlide">
							<!--轮播效果开始 -->
							<div class="bd">
								<ul id="ul_All">
									<%--  
									<li>
										<div class="jifen_a a_bg"><h1>总积分</h1><h2>7845888</h2></div>
									</li>
									<li>
								      	<div class="jifen_a a_bg"><h1>比赛积分</h1><h2>788</h2></div>
								    </li>
									<li>
								    	<div class="jifen_a a_bg"><h1>积分</h1><h2>7888</h2></div>
								    </li>
									--%>
									<s:iterator value="#request.list_All" var="va">
										<li>
									    	<div class="jifen_a a_bg"><h1>总${va.pointsname }</h1><h2>${va.points }</h2></div>
									    </li>
									</s:iterator>
								</ul>
							</div>
							<span class="prev"><img src="${pageContext.request.contextPath}/resource/point/images/btn_back.png"/></span>
							<span class="next"><img src="${pageContext.request.contextPath}/resource/point/images/btn_next.png"/></span>
						</div>
						<!--轮播效果结束 -->
						<!--banner结束 -->
						<div class="jifen_b">
							<span>自<s:date name="#request.startDate" format="yyyy年MM月dd日" /> 起<br /> 到现在为止<br />
								<s:iterator value="#request.list_All" var="va">
									<span style="display:none;" class="yellow">总${va.pointsname } <b class="blue">${va.points }</b></span>
								</s:iterator>
							</span>
						</div>
					</div>

				</div>
				<div class="jifen_pic">
					<div class="jifen_all">
						<div class="fullSlide">
							<!--轮播效果开始 -->
							<div class="bd">
								<ul id="ul_Today">
									<%--  
									<li>
										<div class="jifen_a a_bg"><h1>总积分</h1><h2>7845888</h2></div>
									</li>
									<li>
								      	<div class="jifen_a a_bg"><h1>比赛积分</h1><h2>788</h2></div>
								    </li>
									<li>
								    	<div class="jifen_a a_bg"><h1>积分</h1><h2>7888</h2></div>
								    </li>
									--%>
									<s:iterator value="#request.list_Today" var="va">
										<li>
									    	<div class="jifen_a a_bg"><h1>今日${va.pointsname }</h1><h2>${va.points }</h2></div>
									    </li>
									</s:iterator>
								</ul>
							</div>
							<span class="prev"><img src="${pageContext.request.contextPath}/resource/point/images/btn_back.png"/></span>
							<span class="next"><img src="${pageContext.request.contextPath}/resource/point/images/btn_next.png"/></span>
						</div>
						<!--轮播效果结束 -->

						<div class="jifen_b">
							<span>自<s:date name="#request.nowDate" format="yyyy年MM月dd日" />起<br /> 到现在为止<br />
								<s:iterator value="#request.list_Today" var="va">
									<span style="display:none;" class="yellow">今日${va.pointsname } <b class="blue">${va.points }</b></span>
								</s:iterator>
							</span>
						</div>
					</div>

				</div>
			</div>
			<div style="border-bottom: #dddddd solid 1px; margin: 0px 30px;"></div>

			<div class="jifen_table">
				<div class="table_all">
				
					<div class="table_a">
						<div class="title_h">
							<div class="table_a_title">
								积分动作&nbsp;
								<s:select list="#request.list_PConfig" onchange="pointsChange('sortActkey',this.value)" id="configActkey" listValue="pointsname" listKey="configKey"></s:select>
							</div>
							<ul class="table_data">
								<li id="actkey_1" class="b_focus">
									<a href="###" onclick="pointsRequest('sortActkey', 1)">日</a>
								</li>
								<li id="actkey_2">
									<a href="###" onclick="pointsRequest('sortActkey', 2)">周</a>
								</li>
								<li id="actkey_3">
									<a href="###" onclick="pointsRequest('sortActkey', 3)">月</a>
								</li>
							</ul>
						</div>
						<div class="table_b">
							<table id="tab_Actkey" cellpadding="0" cellspacing="0" border="0" width="90%"
								style="margin: 0 auto;" class="table_jifen">
								<tr>
									<th width="10%">
										序号
									</th>
									<th width="45%">
										动作
									</th>
									<th width="25%">
										得分
									</th>
									<th width="10%">
										次数
									</th>
								</tr>
								<tr>
									<td class="num_01"></td>
									<td>
										积分动作
									</td>
									<td>
										<a href="###">150</a>
									</td>
									<td>
										<a href="###">20</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="table_a">
						<div class="title_h">
							<div class="table_a_title">
								积分用户&nbsp;
								<s:select list="#request.list_PConfig" onchange="pointsChange('sortUser',this.value)" id="configUser" listValue="pointsname" listKey="configKey"></s:select>
							</div>
							<ul class="table_data">
								<li id="user_1" class="b_focus">
									<a href="###" onclick="pointsRequest('sortUser', 1)">日</a>
								</li>
								<li id="user_2">
									<a href="###" onclick="pointsRequest('sortUser', 2)">周</a>
								</li>
								<li id="user_3">
									<a href="###" onclick="pointsRequest('sortUser', 3)">月</a>
								</li>
							</ul>
						</div>
						<div class="table_b">
							<table id="tab_User" cellpadding="0" cellspacing="0" border="0" width="90%"
								style="margin: 0 auto;" class="table_jifen">
								<tr>
									<th width="10%">
										序号
									</th>
									<th width="45%">
										用户
									</th>
									<th width="25%">
										得分
									</th>
									<th width="10%">
										次数
									</th>
								</tr>
								<tr>
									<td class="num_01"></td>
									<td>
										积分用户
									</td>
									<td>
										<a href="###">150</a>
									</td>
									<td>
										<a href="###">20</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="table_a" style="margin-right: 0px;">
						<div class="title_h">
							<div class="table_a_title">
								积分栏目
							</div>
							<ul class="table_data">
								<li class="b_focus">
									<a href="###">日</a>
								</li>
								<li>
									<a href="###">周</a>
								</li>
								<li>
									<a href="###">月</a>
								</li>
							</ul>
						</div>
						<div class="table_b">
							<table cellpadding="0" cellspacing="0" border="0" width="90%"
								style="margin: 0 auto;" class="table_jifen">
								<tr>
									<th width="10%">
										序号
									</th>
									<th width="45%">
										动作
									</th>
									<th width="25%">
										得分
									</th>
									<th width="10%">
										次数
									</th>
								</tr>
								<tr>
									<td class="num_01"></td>
									<td>
										知识库
									</td>
									<td>
										<a href="###">150</a>
									</td>
									<td>
										<a href="###">20</a>
									</td>
								</tr>
								<tr><td class="num_02"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>
								<tr><td class="num_03"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>
								<tr><td class="num_04"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>
								<tr><td class="num_05"></td><td></td><td><a href="###"></a></td><td><a href="###"></a></td></tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
