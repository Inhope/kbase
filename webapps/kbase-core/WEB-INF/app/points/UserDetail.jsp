<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分设置-用户积分详情</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid'), /*datagrid对象*/
					getQueryParams: function(){ /*datagrid查询条件*/
						return {
							configkey: $('#configkey').combobox('getValue'),
							stDate: $('#stDate').val(),
							edDate: $('#edDate').val(),
							userid: '${param.userid}'
						};
					}
				});
				
				/*查询*/
				$('#now-search').linkbutton({
				    onClick: function(){
				    	/*查询跳转到第一页*/
				    	PageUtils.datagrid.reload(1);
				    }
				});
				
				/*重置*/
				var stDate = $('#stDate').val(),
					edDate = $('#edDate').val();/*其实日期的初始值*/
				$('#now-clear').linkbutton({
				    onClick: function(){
				    	$('#configkey').combobox('clear');
				    	$('#stDate').val(stDate);
				    	$('#edDate').val(edDate);
				    }
				});
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto">
			<div>
				积分配置:<select class="easyui-combobox" id="configkey" style="width:150px;">
							<option value="" selected="selected">--请选择--</option>
							<s:iterator value="#request.configs" var="va">
								<option value="${va.configKey.key }">${va.configKey.desc }</option>
							</s:iterator>
						</select>
				&nbsp;
				日期:<input id="stDate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false,readOnly:true,
						onpicked:function(dp){
							$('#edDate').val('');
							$('#edDate').focus(function(){
								WdatePicker({
									el:'edDate',
									isShowClear:false,readOnly:true,isShowToday:false,
									dateFmt:'yyyy-MM-dd',
									minDate:'#F{$dp.$D(\'stDate\')}',
									maxDate:dp.cal.date.y + '-' + dp.cal.getP('M', 'MM') + '-%ld'
								})
							});
							$('#edDate').focus();
						}})" 
					style="width:100px;" readonly="readonly" class="Wdate" type="text" value='${stDate}' />
				至
					<input id="edDate" style="width:100px;" readonly="readonly" class="Wdate" type="text" value='${edDate}' />
				&nbsp;
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
			</div>
		</div>
		<!-- 列表 -->
    	<table class="easyui-datagrid" id="now-grid" 
    	data-options="url:'${pageContext.request.contextPath }/app/points/p-user!detailData.htm',
    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,toolbar:'#now-toolbar',pagination:true,
    		queryParams:{ stDate: $('#stDate').val(), edDate: $('#edDate').val(), userid: '${param.userid}' }">
		    <thead>
		        <tr>
		        	<th data-options="field:'id',checkbox:true"></th>
		        	<th data-options="field:'createDate',width:120">日期</th>
		        	<th data-options="field:'actKeyDesc',width:80">动作</th>
		            <th data-options="field:'points',width:50">得分</th>
					<th data-options="field:'remark',width:250">备注</th>
		        </tr>   
		    </thead>
		</table>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>
