<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分策略</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			
			.datagrid-header-row td{
				background-color:#888888;
				color:#000000;
				font-weight:bold;
			}
			.datagrid-header td, .datagrid-body td, .datagrid-footer td {
			    border-style: solid;
			    border-width: 0 1px 1px 0;
			    margin: 0;
			    padding: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/Strategy.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid') /*datagrid对象*/
				});
				/*列表初始化-私有*/
				Strategy.set({
					cycleTypes: eval('(' + '${cycleTypes1 }' + ')') /*策略执行周期类型*/
				});
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto">
			<div>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"    id="btn-add">新增</a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"   id="btn-edit">编辑</a>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="btn-remove">删除</a>
		    </div>
		</div>
		<!-- 列表 -->
    	<table class="easyui-datagrid" id="now-grid" data-options="url:'${pageContext.request.contextPath }/app/points/p-strategy!loadData.htm',
    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,toolbar:'#now-toolbar',
    		rowStyler:function(index,row){
				if (index%2!=0){
					return 'background-color:#DDDDDD;color:#000000;';
				}
			},
			onCheck:function(rowIndex,rowData){
				$('.datagrid-btable tr:even').css('background-color','#fff');
				$('.datagrid-btable tr:odd').css('background-color','#DDDDDD');
				$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#cce6ff');
			}">
		    <thead>
		        <tr>
		        	<th data-options="field:'id',checkbox:true"></th>
		        	<th data-options="field:'strategyName',width:200">策略名称</th>
		        	<th data-options="field:'actDesc',width:100">动作资源</th>
		            <th data-options="field:'actCyc',width:50,
		            	formatter: function(value, row, index){
		            		/*策略执行周期类型*/
		            		var cycleTypes = Strategy.fn.datas.get('cycleTypes');
							return cycleTypes[value];
						}">执行周期</th>
					<th data-options="field:'validDate',width:300,
		            	formatter: function(value, row, index){
		            		/*策略执行周期类型*/
		            		if(row.actCyc == '5') return '不限';
							return value;
						}">有效期</th>
		            <th data-options="field:'actTimes',width:50,
		            	formatter:function(value, row, index){
		            		if(row.actCyc == '5') return '不限';
		            		else if (row.actCyc == '1') return 1;
		            		return value;
		            	}">执行次数</th>
		            <th data-options="field:'status',width:50, align:'center',
		            	formatter:function(value,row,index){
		            		var url = '/app/points/p-strategy!setStatus.htm';
		            		return PageUtils.datagrid.formatStatus(value,row,url); 
		            	}">状态</th>
		            <th data-options="field:'null',width:100, align:'center',
		            	formatter:function(value,row,index){
		            		var a = $(document.createElement('a'))[0];
							$(a).attr('href', 'javascript:void(0);');
							$(a).attr('onclick', 'Strategy.strategyExt(\'' + row.id + '\')');
							$(a).text('自定义');
		            		return a.outerHTML; 
		            	}">操作</th>
		        </tr>   
		    </thead>
		</table>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	    
	    <%-- 新增、编辑-自定义 --%>
	    <div id="dd_save_ext"></div>
	</body>
</html>
