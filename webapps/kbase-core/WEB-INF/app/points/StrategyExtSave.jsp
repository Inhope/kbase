<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分自定义策略-新增、编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: auto; }
			table.box table.box th,table.box table.box td{text-align: center; }
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/StrategyExtSave.js"></script>
		<script type="text/javascript">
			$(function(){
				StrategyExtSave.set({
					dialogId: '${param.dialogId }', /*当前窗口id*/
					strategyExtId: '${strategyExt.id }', /*自定义策略id*/
					rangCates: ${rangCates},/*所选目录CATALOG所有涉及的父子部门id*/
					rangDepts: ${rangDepts} /*所选部门DEPT所有涉及的父子部门id*/
				});
			});
		</script>
	</head>
	<body>
		<form id="form1" action="" method="post">
			<input type="hidden" name="strategyId" value="${param.strategyId }"/>
			<input type="hidden" name="strategyExtId" value="${strategyExt.id }"/>
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th style="width: 15%">
							分类：
						</th>
						<td style="width: 85%">
							<ul class="easyui-combotree" id="cc" style="width: 99%;" 
								data-options="url:'${pageContext.request.contextPath }/app/points/p-strategy-ext!asyncCategory.htm',
								lines: true, multiple: true, cascadeCheck:false,
				    			loadFilter:function(data, parent){
					   				if(data){
					   					var rangCateNodes = StrategyExtSave.fn.datas.get('rangCateNodes');
	                     	 			if(!rangCateNodes) rangCateNodes = {};
					   					$(data).each(function(i, o){
					   						o.text = o.name;
						   					o.state = 'closed';
						   					/*子节点数据*/
						   					var children = rangCateNodes[o.id];
						   					if(children) {
						   						o.state = 'opne';
						   						o.children = children;
						   					}
						   				});
					   				}
					   				return data;
					   			},
					   			onCheck:function(node, checked){
					   				var cateBhs = '';
					   				var t = $('#cc').combotree('tree');	/*获取树对象*/
									var nodes = t.tree('getChecked');	/*获取选择的节点*/
					   				if(nodes) $.each(nodes, function(i, e){
					   					cateBhs += ',' + e.bh;
					   				});
					   				cateBhs = cateBhs.replace(/^,|,$/g, '');
					   				$('#cateBhs').val(cateBhs);
					   			}"></ul>
					   		<input type="hidden" id="cateBhs" name="cateBhs"/>
						</td>
					</tr>
					<tr>
						<th>
							部门：
						</th>
						<td>
							<ul class="easyui-combotree" id="cd" style="width: 99%;" 
								data-options="url:'${pageContext.request.contextPath}/app/auther/dept!asyncData.htm',
								lines: true, multiple: true, cascadeCheck:false,
				    			loadFilter:function(data, parent){
					   				var data_ = new Array();
	                     	 		if(data) {
	                     	 			/*获取已选择的节点的所有子数据*/
	                     	 			var rangDeptNodes = StrategyExtSave.fn.datas.get('rangDeptNodes');
	                     	 			if(!rangDeptNodes) rangDeptNodes = {};
	                     	 			$(data).each(function(i, item){
	                     	 				if(item.type == 'dept') {
	                     	 					/*当前节点id，当前节点的所有子数据*/
	                     	 					var id = item.id;
	                     	 					var node = { id: id, text: item.name, state: 'closed', bh: item.bh };
	                     	 					/*子节点数据*/
	                     	 					var children = rangDeptNodes[id]
	                     	 					if(children) {
	                     	 						node.state = 'open'; /*打开节点*/
	                     	 						node.children = children; /*能获取到子数据*/
	                     	 					}
	                     	 					data_.push(node);
	                     	 				}
		                     	 		});
	                     	 		}
	                     	 		return data_;
					   			},
					   			onCheck:function(node, checked){
					   				var deptBhs = '';
					   				var t = $('#cd').combotree('tree');	/*获取树对象*/
									var nodes = t.tree('getChecked');	/*获取选择的节点*/
					   				if(nodes) $.each(nodes, function(i, e){
					   					deptBhs += ',' + e.bh;
					   				});
					   				deptBhs = deptBhs.replace(/^,|,$/g, '');
					   				$('#deptBhs').val(deptBhs);
					   			}"></ul>
					   		<input type="hidden" id="deptBhs" name="deptBhs"/>
						</td>
					</tr>
					<tr>
						<th colspan="2" style="background-color: gray; text-align: center;">
							积分配置
						</th>
					</tr>
					<tr>
						<td colspan="2"  class="choose_ques">
							<table cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<th>序号</th>
									<th>积分项名称</th>
									<th>积分分值</th>
								</tr>
								<s:iterator value="#request.configs" var="va" status="st">
									<tr>
										<td>${st.index + 1 }</td>
										<td>${va.pointsname }</td>
										<td>
											<s:set var="val" value="0"></s:set>
											<s:iterator value="#request.strategyExt.strategyConfigs" var="va1">
												<s:if test="#va.configKey.key == #va1.config.configKey.key">
													<s:set var="val" value="#va1.points"></s:set>
												</s:if>
											</s:iterator>
											<input class="easyui-textbox" value="${val }" name="configPoints"
												data-options="required:true, validType:'digits'" style="width:50px" >
											<input type="hidden" name="configIds" value="${va.id }">
										</td>
									</tr>
								</s:iterator>
							</table>
						</td>
					</tr>
				</table>
				<div style="height: 28px; width: 99%; padding: 10px 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="btn-save" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
