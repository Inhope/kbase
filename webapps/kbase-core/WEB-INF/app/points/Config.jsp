<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分配置项</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			
			.datagrid-header-row td{
				background-color:#888888;
				color:#000000;
				font-weight:bold;
			}
			.datagrid-header td, .datagrid-body td, .datagrid-footer td {
			    border-style: solid;
			    border-width: 0 1px 1px 0;
			    margin: 0;
			    padding: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/Config.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid') /*datagrid对象*/
				});
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto">
			<div>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"   id="btn-edit">编辑</a>
		    </div>
		</div>
		<!-- 列表 -->
    	<table class="easyui-datagrid" id="now-grid" data-options="url:'${pageContext.request.contextPath }/app/points/p-config!loadData.htm',
    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,toolbar:'#now-toolbar',
    		rowStyler:function(index,row){
				if (index%2!=0){
					return 'background-color:#DDDDDD;color:#000000;';
				}
			},
			onCheck:function(rowIndex,rowData){
				$('.datagrid-btable tr:even').css('background-color','#fff');
				$('.datagrid-btable tr:odd').css('background-color','#DDDDDD');
				$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#cce6ff');
			}">
		    <thead>
		        <tr>
		        	<th data-options="field:'id',checkbox:true"></th>
		        	<th data-options="field:'pointsname',width:200">配置名称</th>
		        	<th data-options="field:'pointsunit',width:100">配置单位</th>
		        	<th data-options="field:'pointsinit',width:100">初始化分数</th>
		        	<th data-options="field:'pointsmax',width:100">积分上限</th>
		        	<th data-options="field:'pointsmin',width:100">积分下限</th>
		            <th data-options="field:'status',width:50, align:'center',
		            	formatter:function(value,row,index){
		            		return PageUtils.datagrid.formatStatus(value, row, 
								'/app/points/p-config!setStatus.htm',
		            			function(id, status){/*进行行数据验证，不允许全部积分配置项都停用*/
		            				if(status == '0') return true; /*允许启用*/
			            			var bl = false, /*是否允许发送请求*/
			            				rows = $('#now-grid').datagrid('getRows');
			            			$(rows).each(function(){
			            				if(this.id != id && this.status == '0') {
			            					return bl = true;
			            				}
			            			});
			            			if(!bl) $.messager.alert('警告', '不允许全部积分配置项都停用!');
			            			return bl;
		            		}); 
		            	}">状态</th>
		        </tr>   
		    </thead>
		</table>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>
