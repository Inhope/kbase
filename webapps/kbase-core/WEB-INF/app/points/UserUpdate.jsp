<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分设置-用户积分修改</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: auto; }
			table.box table.box th,table.box table.box td{text-align: center; }
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript">
			$(function(){
				/*表单保存*/
				$('#btn-save').click(function(){
					PageUtils.submit('#form1', {
						url: '/app/points/p-user!doSave.htm',
			    		after: function(jsonResult){
			    			parent.$.messager.alert('信息', jsonResult.msg, 'info', function(){
				   				if (jsonResult.rst){
				   					/*关闭弹窗*/
				    				parent.PageUtils.dialog.close('${param.dialogId }');
				    				/*操作成功-刷新列表*/
				    				parent.PageUtils.datagrid.reload();
				   				}
				   			});
			    		}
					});
				});
			});
		</script>
	</head>
	<body>
		<form id="form1" action="" method="post">
			<input type="hidden" name="userid" value="${user.id }"/>
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th style="width: 10%">
							工号：
						</th>
						<td style="width: 40%">
							${user.userInfo.jobNumber }
						</td>
					</tr>
					<tr>
						<th style="width: 10%">
							姓名：
						</th>
						<td style="width: 40%">
							${user.userInfo.userChineseName }
						</td>
					</tr>
					<tr>
						<th colspan="2" style="background-color: gray; text-align: center;">
							积分修改
						</th>
					</tr>
					<tr>
						<td colspan="2"  class="choose_ques">
							<table cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<th>序号</th>
									<th>积分项名称</th>
									<th>积分范围</th>
									<th>修改后分值</th>
								</tr>
								<s:iterator value="#request.configs" var="va" status="st">
									<tr>
										<td>${st.index + 1 }</td>
										<td>${va.pointsname }</td>
										<td>
											${va.pointsmin }-${va.pointsmax }
										</td>
										<td>
											<s:set value="configKey" var="#va.configKey.key"></s:set>
											<input class="easyui-textbox" value="${va.points }" name="configPoint"
												data-options="required:true, validType:['digits', 'min[${va.pointsmin }]', 'max[${va.pointsmax }]']" style="width:100px" >
											<input type="hidden" name="configkey" value="${configKey }">
										</td>
									</tr>
								</s:iterator>
							</table>
						</td>
					</tr>
				</table>
				<div style="height: 28px; width: 99%; padding: 10px 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="btn-save" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
