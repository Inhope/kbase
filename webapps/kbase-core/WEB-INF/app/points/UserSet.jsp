<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分设置</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			
			.datagrid-header-row td{
				background-color:#888888;
				color:#000000;
				font-weight:bold;
			}
			.datagrid-header td, .datagrid-body td, .datagrid-footer td {
			    border-style: solid;
			    border-width: 0 1px 1px 0;
			    margin: 0;
			    padding: 0;
			}
		</style>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/ExportUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/UserSet.js"></script>
		<script type="text/javascript">
			$(function(){
				/*列表初始化-公共*/
				PageUtils.init({
					datagrid: $('#now-grid'), /*datagrid对象*/
					getQueryParams: function(){ /*datagrid查询条件*/
						return {
							jobNumber: $('#jobNumber').textbox('getValue'),
							userChineseName: $('#userChineseName').textbox('getValue'),
							depIds: PageUtils.fn.arrayToString($('#depIds').combotree('getValues'), ','),
							roleId: $('#roleId').combotree('getValue')
						};
					}
				});
			});
		</script>
	</head>
	<body>
		<!--导航栏  -->
		<div id="now-toolbar" style="padding:5px;height:auto">
			<div>
				工号：<input class="easyui-textbox" id="jobNumber" style="width:100px;">&nbsp;&nbsp;
				姓名：<input class="easyui-textbox" id="userChineseName" style="width:100px;">&nbsp;&nbsp;
				部门：<ul class="easyui-combotree" id="depIds" style="width:200px;" 
						data-options="url:'${pageContext.request.contextPath}/app/auther/dept!asyncData.htm',
						lines: true, multiple: true,
		    			loadFilter:function(data, parent){
			   				var data_ = new Array();
                    	 		if(data) {
                    	 			$(data).each(function(i, item){
                    	 				if(item.type == 'dept') 
                    	 					data_.push({
	                     	 				id: item.id,
	                     	 				text: item.name,
	                     	 				state: 'closed'
	                     	 			});
                     	 		});
                    	 		}
                    	 		return data_;
			   			}"></ul>&nbsp;&nbsp;
				角色：<input class="easyui-combotree" id="roleId" style="width:200px;"
						data-options="data: ${roles }" />  
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-search" iconCls="icon-search">查询</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" id="now-clear" iconCls="icon-clear">重置</a>
			</div>
			<div>
			    <a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true"   id="btn-expSel">导出选择</a>
		    	<a href="#" class="easyui-linkbutton" iconCls="icon-redo" plain="true"   id="btn-expAll">导出全部</a>
		    </div>
		</div>
		<!-- 列表 -->
    	<table class="easyui-datagrid" id="now-grid" data-options="url:'${pageContext.request.contextPath }/app/points/p-set!loadData.htm',
    		fitColumns:true,singleSelect:true,rownumbers:true,fit:true,toolbar:'#now-toolbar',pagination:true,
    		rowStyler:function(index,row){
				if (index%2!=0){
					return 'background-color:#DDDDDD;color:#000000;';
				}
			},
			onCheck:function(rowIndex,rowData){
				$('.datagrid-btable tr:even').css('background-color','#fff');
				$('.datagrid-btable tr:odd').css('background-color','#DDDDDD');
				$('.datagrid-btable tr[datagrid-row-index = '+rowIndex+']').css('background-color','#cce6ff');
			}">
		    <thead>
		        <tr>
		        	<th data-options="field:'id',checkbox:true"></th>
		        	<th data-options="field:'jobNumber',width:100">工号</th>
		        	<th data-options="field:'userChineseName',width:80">姓名</th>
		            <th data-options="field:'mainStation',width:200">岗位</th>
					<th data-options="field:'roles',width:150">角色</th>
					<s:iterator value="#request.configs" var="va">
						<th data-options="field:'${va.configKey.key }',width:50">${va.pointsname }</th>
					</s:iterator>
		            <th data-options="field:'null',width:100, align:'center',
		            	formatter:function(value,row,index){
		            		var html = '';
		            		var a = $(document.createElement('a'))[0];
							$(a).attr('href', 'javascript:void(0);');
							$(a).attr('onclick', 'UserSet.userSet(\'' + row.id + '\')');
							$(a).text('编辑');
							html = a.outerHTML
							
							a = $(document.createElement('a'))[0];
							$(a).attr('href', 'javascript:void(0);');
							$(a).attr('onclick', 'UserSet.userDetail(\'' + row.id + '\')');
							$(a).text('积分明细');
							html  +=  '&nbsp;&nbsp;' + a.outerHTML
		            		return html; 
		            	}">操作</th>
		        </tr>   
		    </thead>
		</table>
		
		<%-- 新增、编辑 --%>
	    <div id="dd_save"></div>
	</body>
</html>
