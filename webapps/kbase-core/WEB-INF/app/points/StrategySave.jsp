<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>积分策略-新增、编辑</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/category/css/gz.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/learning/css/paper.css" />
		<style type="text/css">
			html, body{ margin:0;padding:0;border:0;width:100%;height:100%;} 
			a:focus{outline:none;}
			.box th{width: auto; }
			table.box table.box th,table.box table.box td{text-align: center; }
		</style>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/util/js/PageUtils.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resource/point/js/StrategySave.js"></script>
		<script type="text/javascript">
			$(function(){
				StrategySave.set({
					dialogId: '${param.dialogId }', /*当前窗口id*/
					actCyc: '${strategy.actCyc }' /*周期设置,用于对‘执行次数’、‘有效期’进行控制*/
				});
			});
		</script>
	</head>
	<body>
		<form id="form1" action="" method="post">
			<div id="sharedshade" class="unsolve_xinxi_dan" style="width: 99%; height: auto;top: 2px;left: 2px;border: 0;">
				<table cellspacing="0" class="box" style="width: 100%;">
					<tr>
						<th>
							策略名称：
						</th>
						<td colspan="3">
							<input class="easyui-textbox" id="actName" name="actName" 
								value="${strategy.actName }"
								data-options="required:true" style="width:300px" >
							<input type="hidden" name="id" value="${strategy.id }">
						</td>
					</tr>
					<tr>
						<th>
							动作资源：
						</th>
						<td colspan="3">
							<select class="easyui-combobox" id="actKey" name="actKey"
								style="width:150px;" data-options="required:true,value:'${strategy.actKey }'">
								<s:iterator value="#request.pActKeys" var="va">
									<option value="${va.key }">${va.value.desc }</option>
								</s:iterator>
							</select>
						</td>
					</tr>
					<tr>
						<th>
							周期设置：
						</th>
						<td>
							<select class="easyui-combobox" id="actCyc" name="actCyc"
								style="width:150px;" data-options="required:true,
								onSelect: function(rec){
									/*执行次数*/
									if(rec.value == '5' || rec.value == '1') {
										$('#actTimes').textbox('clear');/*清空组件*/
										$('#actTimes').textbox('disable');/*停用组件*/
										$('#actTimes').textbox('disableValidation');/*关闭验证*/
									} else {
										$('#actTimes').textbox('enable');/*启用组件*/
										$('#actTimes').textbox('enableValidation');/*启用验证*/
									}
									/*有效期*/
									if(rec.value == '5') {
										$('#cycFromDate').datebox('clear');/*清空组件*/
										$('#cycFromDate').datebox('disable');/*停用组件*/
										$('#cycFromDate').datebox('disableValidation');/*关闭验证*/
										
										$('#cycEndDate').datebox('clear');/*清空组件*/
										$('#cycEndDate').datebox('disable');/*停用组件*/
										$('#cycEndDate').datebox('disableValidation');/*关闭验证*/
									} else {
										$('#cycFromDate').datebox('enable');/*启用组件*/
										$('#cycFromDate').datebox('enableValidation');/*启用验证*/
										
										$('#cycEndDate').datebox('enable');/*启用组件*/
										$('#cycEndDate').datebox('enableValidation');/*启用验证*/
									}
						        }">
								<s:iterator value="#request.cycleTypes" var="va">
									<s:if test="#va.key == '2' || #va.key == '3' || #va.key == '4'">
										<option value="${va.key }">每${va.value.label }</option>
									</s:if>
									<s:else>
										<option value="${va.key }">${va.value.label }</option>
									</s:else>
								</s:iterator>
							</select>
						</td>
						<th>
							执行次数：
						</th>
						<td>
							<input class="easyui-textbox" value="${strategy.actTimes }" id="actTimes" name="actTimes"
								data-options="required:true, validType:['digits', 'min[0]']" style="width:50px" >
						</td>
					</tr>
					<tr>
						<th>
							有效期：
						</th>
						<td colspan="3">
							<input class="easyui-datebox" id="cycFromDate" name="cycFromDate"
								 value="<s:date name="#request.strategy.cycFromDate" format="yyyy-MM-dd"/>"
	        					data-options="editable:false" style="width:150px">
	        				-
	        				<input class="easyui-datebox" id="cycEndDate" name="cycEndDate"
	        				 	value="<s:date name="#request.strategy.cycEndDate" format="yyyy-MM-dd"/>"
	        					data-options="editable:false" style="width:150px">
						</td>
					</tr>
					<tr>
						<th>
							备注：
						</th>
						<td colspan="3">
							<input class="easyui-textbox" id="remark" name="remark" 
								value="${strategy.remark }"
								data-options="multiline:true,value:''" style="width:300px;height:50px">
						</td>
					</tr>
					<tr>
						<th colspan="4" style="background-color: gray; text-align: center;">
							积分配置
						</th>
					</tr>
					<tr>
						<td colspan="4"  class="choose_ques">
							<table cellspacing="0" class="box" style="width: 100%;">
								<tr>
									<th>序号</th>
									<th>积分项名称</th>
									<th>积分分值</th>
								</tr>
								<s:iterator value="#request.configs" var="va" status="st">
									<tr>
										<td>${st.index + 1 }</td>
										<td>${va.pointsname }</td>
										<td>
											<s:set var="val" value="0"></s:set>
											<s:iterator value="#request.strategy.strategyConfigs" var="va1">
												<s:if test="#va.configKey.key == #va1.config.configKey.key">
													<s:set var="val" value="#va1.points"></s:set>
												</s:if>
											</s:iterator>
											<input class="easyui-textbox" value="${val }" name="configPoints"
												data-options="required:true, validType:'digits'" style="width:50px" >
											<input type="hidden" name="configKeys" value="${va.configKey.key }">
										</td>
									</tr>
								</s:iterator>
							</table>
						</td>
					</tr>
				</table>
				<div style="height: 28px; width: 99%; padding: 10px 0 7px 0; float: none;" class="unsolve_xinxi_dan_con">
					<input type="button" id="btn-save" style="float: right; margin-right: 10px;" class="unsolve_xinxi_an1">
				</div>
			</div>
		</form>
	</body>
</html>
