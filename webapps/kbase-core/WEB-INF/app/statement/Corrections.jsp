<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/yibancss.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/dkfj.css" rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/search/css/gonggao.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/statement/css/ca.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/library/ztree/js/jquery.ztree.core-3.5.js"></script>
  	<script type="text/javascript" src="${pageContext.request.contextPath}/resource/corrections/js/Corrections.js"></script>
  </head>
  
  <body>
    <table>
    </table>
    <div id="searchCondition" style="padding: 5px;height: 30px;">
		<span>时间:</span>
		从<input type="text" id="startTime" style="width:150px"/>
		到&nbsp;<input type="text" id="endTime" value="" style="width:150px"/>
		<span>标准问:</span>
		<input type="text" id="question" maxlength="30" style="width:150px;"/>
		<span>状态:</span>
		<select id="status">
		   <option value=""></option>
		   <s:iterator value="#request.statuslist" var="status">
		      <option value="${status.key }">${status.label }</option>
		   </s:iterator>
		</select>
		<span>类型:</span>
		<select id="type">
		   <option value=""></option>
		   <s:iterator value="#request.typelist" var="type">
		      <option value="${type.key }">${type.label }</option>
		   </s:iterator>
		</select>
		<!-- 
		<span>操作人员:</span>
		<input type="text" id="person" maxlength="30" style="width:100px;"/>
		-->
		<a id="searchBtn" href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
	
	<div style="position: absolute;z-index:100010;height:530px;display:none;" id="unSolvedPanel">
	<div class="unsolve_xinxi_d">
	  <form action="${pageContext.request.contextPath}/app/corrections/corrections!save.htm" id="corrform" method="post">	
		<div style="height: 297px;" class="unsolve_xinxi_dan">
			<div class="unsolve_xinxi_dan_title">
				<b>纠错:</b>
				<a href="javascript:void(0);"> <img width="12" height="12" src="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/auther/images/xinxi_ico1.jpg"> </a>
			</div>
			<input type="hidden" id="id_val" value="" />
			<div style="height:20px;width:381px;padding:0 20px;padding-top:5px;float:left">
				<b>标&nbsp;&nbsp;准&nbsp;&nbsp;问:</b>
				<input type="text" name="question" id="question_val" value="" readonly="readonly" />
			</div>
			<div style="height:20px;width:381px;padding:0 20px;padding-top:5px;float:left">
				<b>类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型:</b>
				<input type="text" name="type" id="type_val" value="" readonly="readonly" />
			</div>
			<div style="height:20px;width:381px;padding:0 20px;padding-top:5px;float:left">
				<b>紧急程度:</b>
				<input type="text" name="level" id="level_val" value="" readonly="readonly" />
			</div>
			<div style="height: 30px;" class="unsolve_xinxi_dan_con">
				<b>纠错原因:</b><textarea rows="" cols="" style="height:45px;resize:none;" id="content_val" readonly="readonly" name="content"></textarea>
				<b>回&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;复:</b>
				<textarea rows="" cols="" style="height:45px;resize:none;" id="rmmarks_val" name="remarks"></textarea>
				<a href="javascript:void(0);" id="issubmit"><input type="button" class="unsolve_xinxi_an1"></a>
			</div>
		</div>
		</form>
	</div>
  </div>
</div>

</body>
</html>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>