<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
    
    <title>404</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<style type="text/css">
		.site-notfound{
		    background: #fff url("${pageContext.request.contextPath}/resource/error/images/T1BkxXFtVcXXaK8jso-320-320.png") no-repeat scroll center top;
		    line-height: 32px;
		    margin: 0 auto 60px;
		    overflow: hidden;
		    padding-top: 328px;
		    text-align: center;
		    width: 320px;
		}
		.site-notfound a {
			font-size: 12px;
			color: gray;
		}
		.site-notfound span {
			font-weight:bold;
			font-size: 14px;
			color: red;
		}
	</style>
  </head>
  
  <body>
    <div style="text-align: center;">
    	<dir class="site-notfound">
    		<span style="color: ">
    			<s:if test="#request.message !=null && #request.message !=''">
    				${message }
    			</s:if>
    			<s:else>
    				404警告！ 很不幸，您探索了一个未知领域！
    			</s:else>
    		</span>
    		<br>
    		<a href="${pageContext.request.contextPath}/main.htm">返回知识库首页</a>
    	</dir>
    </div>
  </body>
</html>
