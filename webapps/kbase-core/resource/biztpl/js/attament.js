/**  
 * 搜索结果集中引用、新增、查看引用按钮事件
  */  
$('body').on('click','td a',function(){
	var fileId = $(this).parents('tr').attr('id');
	var fileName = $('#file').val().replace(/^.*[\/\\]/, '');
	var op = $(this).attr('type');
	var attType = '';
	var arrTypeName = '';
	if(op=='add'){
		upload(fileName,'add',fileId);
	}else if(op=='quote'){
		try{
				var fname = $(this).parent('td').prev('td').text();
				var roots = $('#attamentDatas').tree('getRoots');
				if (roots && roots.length>0) {  
	                var node = null;  
	                for (var i=0; i<roots.length; i++) {
	                    node = roots[i];  
	                    if (isMatch(fname, node.text)) {
	                    	return false;
	                    }  
	                } 
				}
				var json = {'id':'','filename':''};
				json.id = fileId;
				json.filename=fname;
				handleData(json,'ref',fileId);
				flg = true;
				alert('引用成功');
		}catch(e){
			console.log(e);
				alert('引用失败');
		}
		
	}else if(op == 'look'){
		loadData(fileId);
	}
})
/**
 * 实例上附件结果处理
 */
function dataByObject(d,op,fileId){
	var attType = $('input[name="fj"]:checked').val();
	var arrTypeName = "";
	if(attType == 1){
		arrTypeName = "相关文件";
	}else if(attType == 2){
		arrTypeName = "废止文件";
	}else if(attType == 3){
		arrTypeName = "征求意见稿";
	}else if(attType == 4){
		arrTypeName = "其他";
	}else if(attType == 5){
		arrTypeName = "正文";
	}else if(attType == 6){
		arrTypeName = "附件";
	}else{
		attType = 0;
		arrTypeName = "附件";
	}
	var date = new Date();
	var nowTime = date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+(date.getDate());
	fileTr = '';
	fileTr += '<tr id="'+d.id+'" _name="att" _type="new" type="'+attType+'">';
	fileTr += '<td><div class="fileName"><input type="checkbox" name="check_list" id='+d.id+' fileId="'+fileId+'" op="'+op+'">'+d.filename+'</div></td>';
	fileTr += '<td>'+username+'</td>';
	fileTr += '<td>'+nowTime+'</td>';
	fileTr += '<td>'+arrTypeName+'</td>';
}
/**
 * 答案上附件结果处理
 */
function dataByAnswer(d,op,fileId){
	$(p).children('a').attr('id',d.id);
	$(p).children('a').text(d.filename);
	$(p).children('a').attr('attType','0');
	$(p).children('a').attr('fileId',fileId);
	$(p).children('a').attr('op',op);
}
/**
 * 附件上传成功后数据处理
 */
function handleData(d,op,fileId){
	if(type == '0'){
		dataByObject(d,op,fileId);
	}else if(type == '1'){
		dataByAnswer(d,op,fileId);
	}
	flg = true;
}

$('input#upload').click(function(){
	var fileName = $('#file').val().replace(/^.*[\/\\]/, '');
	var index = $.inArray(fileName, nameArr);
	if(index != -1){
		alert('不能上传文件名相同的附件');
		return false;
	}
	upload(fileName,'','');
})
/**
 * 附件上传
 */
function upload(fileName,op,fileId){
	if (fileName.length>0){
		$.ajaxFileUpload({
				url : pageContext.contextPath + '/app/biztpl/article!doUpload.htm',		//待上传文件的路径
				secureuri : false,
				fileElementId : 'file',		//文件上传字段id
				data : {"filename": fileName},	//参数
				dataType : 'json',
				timeout : 5*60*1000,
				success : function(data, status) {
					if (data.success) {
						var d  = eval('(' + data.data + ')');
						handleData(d,op,fileId);
						if(op && op == 'add'){
							alert('新增成功');
						}else{
							alert('上传成功');
						}
					}
				},
				error : function(data, status, e){
					alert('处理超时,请重新上传ee!');
				}
			});
	}else{
		alert('您尚未正确完整的输入表单内容，请检查！');
	}
}
/**  
     * 判断searchText是否与targetText匹配  
     * @param searchText 检索的文本  
     * @param targetText 目标文本  
     * @return true-检索的文本与目标文本匹配；否则为false.  
     */  
function isMatch(searchText, targetText) {  
      return $.trim(targetText)!="" && targetText.indexOf(searchText)!=-1;
}