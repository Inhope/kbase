$(function(){
	var zTree;
	var cateId = "";
	_setting = {
		edit: {
			enable: true,
			showRemoveBtn: false,
			showRenameBtn: false
		},
		view: {
			dblClickExpand: false,
			//showIcon: false,
			showTitle: true,
			nameIsHTML: true
		},
		data: {
			key: {
				title: "title"
			},
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick: function(e, treeId, treeNode){
				loadData();
			},
			beforeDrag:function(){return true;},
			//拖动事件
			onDrop:function(event, treeId, treeNodes, targetNode, moveType){
				//如果拖拽改变了父子级关系，则需要修改数据库数据
				var id = zTree.getSelectedNodes()[0].id;
				var pId;
				if(moveType == 'inner'){
					pId = targetNode.id;
				}else{
					pId = targetNode.pId;
				}
				$.ajax({
					url : $.fn.getRootPath() + '/app/biztpl/biz-tpl!updateRelation.htm',
					type : 'POST',
					dataType : 'json',
					data:{id:id,pId:pId},
					success : function(result){
					}
				});
			}
		}
	};
	_loadTree = function(){
		$.ajax({
			type: "POST",
			dataType : 'json',
			url: $.fn.getRootPath() + '/app/biztpl/biz-tpl!cateJson.htm',
			async: false,
			success: function(data){
				zTree=$.fn.zTree.init($("#treeDemo"), _setting, data);
			}
		});
	}
	//加载树
	_loadTree();
	//保存节点
	saveOrUpdate = function(id,name){
		var nodes = zTree.getSelectedNodes();
		var node;//选中的节点，若没选中，默认为根节点
		if(nodes.length > 0){
			node = nodes[0];
		}else{
			node = zTree.getNodes()[0];
		}
		var pId = node.id;
		//新节点
		var newNode = {id:id,name:name,pId:pId};
		//判断同级分类下不能存在相同分类
		var doFlag = true;
		var childNodes;
		if(id == ''){
			childNodes = node.children;
		}else{
			childNodes = node.getParentNode().children;
		}
		$(childNodes).each(function(){
			if($.trim(this.title) == $.trim(name)){
				doFlag = false;
			}
		});
		if(!doFlag){
			alert("同级分类下不应存在相同分类");
			return false;
		}
		//保存节点
		$.ajax({
			type : 'post',
			url : $.fn.getRootPath() + '/app/biztpl/biz-tpl!saveCate.htm', 
			data : newNode,
			async: true,
			dataType : 'json',
			success : function(data){
				if(id == ""){
					zTree.addNodes(node, data);
				}else{
					_loadTree();
				}
				alert("操作成功!");
			},
			error : function(msg){
				alert("网络错误，请稍后再试!");
			}
		});
	}
	
	//点击新增
	$("#btnNewCate,#btnEditCate").click(function(){
		var defaultName = "";
		if($(this).attr("id") == "btnNewCate"){
			//清空数据
			cateId = "";
		}else if($(this).attr("id") == "btnEditCate"){
			var nodes = zTree.getSelectedNodes();
			if(nodes.length == 0){
				alert("请选择节点");
				return false;
			}
			if(nodes[0].id == 'root'){
				alert("无法编辑根节点");
				return false;
			}
			cateId = nodes[0].id;
			defaultName = nodes[0].title;
		}
		layer.prompt({
			title: '请输入分类名称',
			formType: 0, //prompt风格，支持0-2
			value:defaultName
		}, function(text, index, elem){
			//保存节点
			text = $.trim(text);
			if(text == ""){
				alert("分类名称不能为空",-1);
			}else{
				layer.close(index);
				saveOrUpdate(cateId,text);
			}
		});
	});
	
	//点击删除
	$('#btnDelCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if(nodes.length == 0){
			alert("请选择节点");
			return false;
		}
		if(nodes[0].id == 'root'){
			alert("无法删除根节点");
			return false;
		}
		var node = nodes[0];
		var msg = "确定要删除吗？";
		if (node.children && node.children.length > 0) {
			msg = "删除的是父节点，将连同子节点一起删掉，分类下所有数据存入草稿箱。\n\n请确认！";
		}
		//数据库删除操作-分类下所有数据存入草稿箱
		if(confirm(msg)){
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/biztpl/biz-tpl!delCate.htm', 
				data : {'id': node.id},
				async: true,
				dataType : 'json',
				success : function(data){
					if(data.status == 1){
						zTree.removeNode(node);
					}
					alert("操作成功");
				},
				error : function(msg){
					newNode = null;
					alert("网络错误，请稍后再试!");
				}
			});
		}
	});
});