$(function(){
	
	//批量校验文章名称、语义、标准问
	_checkDataBatch = function(){
		var checkResult = [];
		checkResult.push(_checkData($("input[name='articleName']")));
		if(showQa == 'true'){
			checkResult.push(_checkData($("input[name='semantic']")));
		}
		var question = $("input[name='kbs-question']");
		question.each(function(i,item){
			checkResult.push(_checkData(this));
		});
		return $.inArray(false, checkResult) < 0;
	}
	
	//新-单个校验文章名称、语义、标准问
	$("div#valForm,[class='content']").on("blur","input[name='articleName'],input[name='semantic'],input[name='kbs-question']",function(){
		_checkData(this);
	});
	
	//校验
	_checkData = function(_this){
		var checkPass = true;
		var id = $(_this).attr("_aid") ==  undefined ? "" : $(_this).attr("_aid");
		var name = $(_this).attr("name");
		var value = $(_this).val();
		value = value.replace(/\s+/g,"");
		var type = "";
		var typeVal = "";
		if(name == "articleName"){//文章名称
			type = "articleName";
			typeVal = "文章名称";
		}else if(name == "semantic"){//语义
			var hasAttrId = false;
			$('div[class="q_ask_show"]').find('[name="kbs-question"]').each(function(i,item){
				if($(this).attr("attrid") != undefined && $(this).attr("attrid") != ""){
					hasAttrId = true;
				}
			});
			if(!hasAttrId && value == ""){
				$(_this).css("border","");
				return true;
			}
			type = "semantic";
			typeVal = "语义块";
		}else if($(_this).attr("name") == "kbs-question"){//标准问
			id = $(_this).attr("id") ==  undefined ? "" : $(_this).attr("id");
			type = "question";
			typeVal = "标准问";
		}
		var param = {
			id: id,
			type: type,
			value: value
		};
		if(value == ""){
			if(type == "semantic" || type == "articleName"){
				$(_this).css("border","1px dotted red");
			}else{
				$(_this).parent().css("border","1px solid red");
			}
			checkPass = false;
			alert(typeVal + "不能为空");
		}else{
			$.ajax({
				type: "POST",
				async: false,
				url:pageContext.contextPath+"/app/biztpl/article!isExist.htm",
				data: param,
				dataType:"json",
				success: function(data) {
					if(data.status){
						if(type == "semantic" || type == "articleName"){
							$(_this).css("border","1px solid red");
						}else{
							$(_this).parent().css("border","1px solid red");
						}
						checkPass = false;
						alert(typeVal + "[" + value + "]已存在");
					}else{
						if(type == "semantic" || type == "articleName"){
							$(_this).css("border","");
						}else{
							$(_this).parent().css("border","");
						}
					}
				}
			});
		}
		return checkPass;
	}
	
});