/**
 * 文章展示
 * @author Gassol.Bi
 * @date Jun 5, 2016 9:52:26 AM
 *
 */
$(function () {
	var that = window.ArticleSearch = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*公共方法*/
	fn.extend({
		/*有弹窗*/
		layer: function(options){
			var onBefore = options.onBefore;
			/*事件不存在，或者事件存在但是返回true*/
			if(!onBefore || (onBefore && onBefore.call(that, options))){
				var h = options.height, w = options.width;
				var	title = options.title, url = options.url, params = options.params;
				if(params){
					url += '?'; 
					for(var key in params){
						url += '&' + key + '=' + params[key];
					}
				}
				if(!h) h = '400px';
				if(!w) w = '700px';
				__kbs_layer_index = $.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ title, 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + url
					},
					area : [ w, h ]
				});
			}
			
		},
		/*参数转换*/
		parseJSON: function(str){
			//add by eko.zhan at 2016-11-14 18:40 str为null时导致报错
			if (str==null) str = '';
			str = '{' + str + '}';
			return eval('(' + str + ')');
		},
		/*实例查看*/
		lookObject: function(articleId, articleName){
			var url = $.fn.getRootPath() + 
				'/app/search/object-search.htm?searchMode=8&objId=' + articleId;
			if(parent.TABOBJECT) {
				parent.TABOBJECT.open({
					id : articleId,
					name : articleName,
					hasClose : true,
					url : url,
					isRefresh : true
				}, this);
			} else {
				var win = window.open(url, articleId);
				win.focus();
			}
		}
	});
	/*功能方法*/
	that.extend({
		/*时光轴*/
		timeline: {
			navBar: function(hgBar, bar){
				/*"正文"标题距离浏览器底部的距离*/
				//var h = $(window).height() - $('#spotRoot').offset().top;
				//当右边空白区域足够时显示导航 modify by wilson.li at 2016-09-13 pm 19:10
				var h = $(window).height() - $('[class="right-sidebar"]').offset().top - $('[class="right-sidebar"]').height() - 20;
   				if(hgBar < h){
   					$(bar).show();
   					that.timeline.initPosition();
   				}else{
   					$(bar).hide();
   				}
			},
			initPosition: function(){
				//滑动页面时，定位右边导航当前位置 add by wilson.li at 20161014
				var middleHeight = Math.round($(window).height()/2);//可视区域一半高度
				var controlHeight = Math.round($(window).height()/4);//可视区域可控高度
				var spotArr = $(".detail-c div[id^='spot']");
				spotArr.each(function(i,item){
					var distanceHeight = $(item).offset().top;
					if(distanceHeight > 0 && distanceHeight > (middleHeight - controlHeight) && distanceHeight < (middleHeight + controlHeight)){
						$("#timeline .kbs-one span").removeClass("span-new");//其他图标还原
						$("#timeline a[href='#"+$(item).attr("id")+"']").focus();
						$("#timeline a[href='#"+$(item).attr("id")+"'] span").addClass("span-new");//当前图标修改新样式
					}
				});
			},
			init: function(){
				/************手动影藏显示时光轴*******************/
				$('#timeline-show').click(function(){
					var obj = $('#timeline'), 
						display = $(obj).css('display');
					$(obj).css('display', 
						display == 'none' ? '' : 'none');
					});
				/************滚动条监听*******************/
				/*刷新文章展示滚动条最大高度.kbs_w*/
				var maxHeight = 300, nowHeight = $(window).height()*0.9 - 130;
				if(maxHeight < nowHeight) nowHeight = maxHeight;
				$('div[class="kbs_w"]:eq(0)').css({height: nowHeight + 'px'});
				/*导航栏对象, 导航栏高度*/
				var bar = $('#navigationBar'), 
					hgBar = $(bar).height();
				/*滚动条监听*/
				$('.panel-body').bind('scroll', function(){
					that.timeline.navBar(hgBar, bar);
				});
				/*初始化一次*/
				that.timeline.navBar(hgBar, bar);
			}
		},
		/*文章*/
		article: {
			/****版本*******/
			edition: function(){
				$('#edition').click(function(){
					alert('版本' + objId);
				});
			},
			/*文章编辑*/
			edit: function (objId){
				/****编辑*******/
				$('#edit').click(function(){
					window.open($.fn.getRootPath() 
						+ '/app/biztpl/article!index.htm?articleId=' + objId);
				});
			},
			/*文章关联*/
			relation:function (objId, objName,tplId){
					if(objId){
					/*数据*/
					$('#relationArticle').empty();
					PageUtils.ajax({
						url: '/app/biztpl/article!getObjectRelationOn.htm', 
						params: {objId: objId, 'pageNo':1,tplId:tplId},
						after:function(jsonResult){
							var htm = '';
							if(jsonResult != null){
								var data = jsonResult.data;
								if(jsonResult.success && data instanceof Array){
									$(data).each(function(i, o){
										if(o.name != objName) {/*不是当前实例*/
											htm += '<li><a onclick="ArticleSearch.fn.lookObject(\'' + o.object_id 
												+ '\', \'' + o.name+ '\');" title="'+o.name+'" ' 
												+ 'href="javascript:void(0);">' + o.name +'</a></li>'
										}
									});
								}
							}
							$('#relationArticle').html(htm);
						}
					});
				}
			}
		},
		/*实例*/
		obj:{
			/*参考文档（原文）*/
			refer: function(objId){
				$('#refer').click(function(){
					PageUtils.dialog.open('#dd_save', {
						params: {objId: objId}, 
						height:400, width:500, title: '参考文档',
				    	url: '/app/biztpl/article-search!attObj.htm'
					});
				});
			},
			/*收藏（实例收藏）*/
			favClip: function(objId, cateId){
				$('#favClip').click(function(){
					FavBallExt.f_openPanel(objId, null, cateId);
				});
			},
			/*对比*/
			cmpare: {
				addRow:function(o){
					if(typeof(o.compareObjectId) == "undefined"){
						$('#cmpareArticle').append('<li objId="' + o.object_id + '" saveInManager="' + 1 +'" >' 
							+ '<input type="checkbox"><a onclick="ArticleSearch.fn.lookObject(\'' + o.object_id  
							+ '\', \'' + o.name + '\');" title="' + o.name + '" ' 
							+ 'href="javascript:void(0);">' + o.name +'</a></li>');
					}else if(o.compareObjectId){
						$('#cmpareArticle').append('<li objId="' + o.compareObjectId + '" saveInManager="' + o.saveInManager +'" >' 
						+ '<input type="checkbox"><a onclick="ArticleSearch.fn.lookObject(\'' + o.compareObjectId 
						+ '\', \'' + o.compareObjectName + '\');" title="' + o.compareObjectName + '" ' 
						+ 'href="javascript:void(0);">' + o.compareObjectName +'</a></li>');
					}
				},
				rmRow:function(objId){
					var lis = $('#cmpareArticle').find('li[objId="' + objId + '"]');
					$(lis).each(function(){
						$(this).remove(); 
					});
				},
				init:function(objId, objName,tplId){
					$('#cmpareArticle').empty();
					/*文章对比数据*/
					PageUtils.ajax({
						url: '/app/biztpl/article!getObjectComparisOn.htm', 
						params: {objId: objId,tplId:tplId},
						after:function(jsonResult){
							if(jsonResult != null){
							var htm = '', data = jsonResult.data;
							if(jsonResult.success && data instanceof Array) {
								/*渲染列数据*/
								$(data).each(function(i, o){
									if(o.name != objName) {/*不是当前实例*/
										htm += that.obj.cmpare.addRow(o);
									}
								});
							}
							}
						}
					});
					/*选中对比*/
					$('#cmpObject').click(function(){
						var cks = $('#cmpareArticle').find('input[type="checkbox"]:checked');
						if(cks && cks.length > 0) {
							var versionIds = '';
							$(cks).each(function(){ 
								versionIds += ',' + $(this).parent().attr('objId');
							});
							var url = $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids=' + objId + versionIds;
							if(parent.TABOBJECT) {/*知识库中打开*/
								parent.TABOBJECT.open({
									id : 'sldb',
									name : '实例对比',
									hasClose : true,
									url : url,
									isRefresh : true
								}, this);
							} else {/*窗口打开*/
								var win = window.open(url);
								win.focus();
							}
						} else $.messager.alert('信息', '最少选择一篇文章!');
					});
					/*添加文章*/
					$('#addObject').click(function(){
						PageUtils.dialog.open('#dd_save', {
							params: {objId: objId}, width: 835, height: 420,
					    	title: '文章对比',
					    	url: '/app/biztpl/article-search!add.htm'
						});
					});
				}
			},
			
			//分类展示 add by eko.zhan at 2016-09-12 17:12
			view2d: function(objId, cateId){
				$('#btn2d').click(function(){
					location.href = $.fn.getRootPath() + '/app/biztpl/article-search.htm?displayMode=2d&articleId=' + objId;
				});
			}
		},
		/*标准问*/
		req: function(objId, cateId, cateBh){
			/*标准问-签读、评论、收藏*/
			$('div[class="c-meaubar"]').each(function(){
				var options = fn.parseJSON($(this).attr('options')), 
					faqId = options.faqId;
				
				/*签读*/
				$(this).find('li[class="c-icona"] a:eq(0)')
					.click(function(){
						PageUtils.ajax({
							url: '/app/search/search!markreadObject.htm',
							params: { 'valueId': faqId, 'objectId': objId, 
								'catePath': cateBh 
							},
							after:function(jsonResult){
								if(jsonResult == '1') 
									$.messager.alert('信息', '签读成功!');
								else $.messager.alert('信息', '已签读!');
							}
						});
				});
				
				/*评论*/
				$(this).find('li[class="c-iconb"] a:eq(0)')
					.click(function(){
						$.kbase.errcorrect({
							'objId': objId,
							'faqId': faqId,
							'cateId': cateId
						});
				});
				
				/*收藏*/
				$(this).find('li[class="c-iconc"] a:eq(0)')
					.click(function(){
						FavBallExt.f_openPanel(null, faqId, cateId);
				});
				
				/*推荐*/
				$(this).find('li[class="c-icond"] a:eq(0)')
					.click(function(){
					
						fn.layer({
							title: '分享', height: $(window).height()*0.90, width: '640', 
							url: '/app/recommend/recommend!open.htm',
							params: {
								cateid: cateId,
								valueid: faqId
							}
						});
				});
				
			});
		},
		/*附件*/
		attach: function (params){
			/**附件下载\预览*/
			$('.file-con').each(function(){
				var options = $(this).attr('options');
				options = fn.parseJSON(options);
				options.converterPath = params.converterPath;/*附件地址*/
				
				/*下载*/
				if(params.doDownload!="false"){
				var download = $(this).children('p:eq(0)');
				$(download).click(function(){
					var converterPath = options.converterPath,
						fileId = options.fileId, 
						fileName = options.fileName,
						type = fileName.substring(fileName.indexOf('.') + 1);
					var elemIF = document.createElement('iframe');
		            elemIF.src = $.fn.getRootPath() + '/app/wukong/search!download.htm?fileId=' + fileId + '&fileName=' + fileName;   
		            elemIF.style.display = 'none';
		            document.body.appendChild(elemIF);   
				});
				}
				
				/*预览*/
				var preview = $(this).children('div:eq(0)');
				$(preview).click(function(){
					var converterPath = options.converterPath,
						fileId = options.fileId, 
						fileName = options.fileName,
						type = fileName.substring(fileName.indexOf('.') + 1);
					var url = converterPath + '/attachment/preview.do?fileName=' + fileId + '.' + type;
					var scrHeight = screen.height - 85;
					var scrWidth = screen.width - 20;
					window.open(url , fileId, 'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,width=' 
						+ scrWidth + ',height='+scrHeight);
				});
			});
		},
		articleBhInit: function (robotPath,cateId,pageType){
			//文章路径及其点击事件
			var catParamData = {'format':'json', 'async':true, 'nodeid':cateId, 'nodetype':1};
			$.getJSON(robotPath+'p4pages/related-category.action?jsoncallback=?', catParamData, function(arr){
				if (arr!=null){
					$(arr).each(function(i, item){
						//name id type bh
						if($(arr).length > (i+1)){
							$('#categoryNv').append('<span categoryId="' + item.id + '" categoryName="' + item.name + '" style="cursor: pointer;">' + item.name + ' > </span>');
						}else{
							$('#categoryNv').append('<span categoryId="' + item.id + '" categoryName="' + item.name + '" style="cursor: pointer;" class="position_title"> ' + item.name + '</span>');
						}
					});
				}
			});
			if(pageType != "preview"){
				$("#categoryNv").on("click","span",function(){
					var categoryId = $(this).attr("categoryId");
					parent.TABOBJECT.open({
						id: categoryId,
						name : $(this).attr("categoryName"), 
						hasClose : true,
						url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId + '&isCate=true&t='+new Date().getTime(),
						isRefresh : true
					}, this);
				});
			}
		},
		setting: function (params){
			/*相关数据（实例id, 分类id, 分类路径）*/
			var objId = params.objId, objName = params.objName,/*实例id, 实例name*/
				cateId = params.cateId, cateName = params.cateName,/*分类id, 分类名称*/
				cateBh = params.cateBh, cateBhName = params.cateBhName,/*分类Bh, 分类BhName*/
				spotId = params.spotId, tplId = params.tplId,/*锚点, 模板id*/
				robotPath = params.robotPath,pageType = params.pageType;/*robot地址, 页面类型（是否是预览或者详情页）*/
			if(pageType != "preview"){//非预览页面(预览页面如果也需要此功能，可放开)
				
				/*********************文章***********************/
				/*文章版本*/
				that.article.edition(objId);
				/*文章编辑*/
				that.article.edit(objId);
				/*文章关联*/
				that.article.relation(objId, objName,tplId);
				
				/**********************实例**********************/			
				/*参考文档(原文)*/
				that.obj.refer(objId);
				/*收藏夹(实例收藏)*/
				that.obj.favClip(objId, cateId);
				/*文章对比(实例对比)*/
				that.obj.cmpare.init(objId, objName,tplId);
				/*文章分类展示*/
				that.obj.view2d(objId, cateId);
				
				/*********************标准问***********************/
				/*标准问-签读、评论、收藏*/
				that.req(objId, cateId, cateBh);
				
				/**********************附件**********************/
				/**附件下载\预览*/
				that.attach(params);
	
				/*最大化*/
				$('#open').click(function(){
					var url = $.fn.getRootPath() + '/app/biztpl/article-search.htm?&articleId=' + objId;
					var scrHeight = screen.height - 85;
					var scrWidth = screen.width - 20;
					var win = window.open(url , objId, 'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
						+ scrWidth + ',height=' + scrHeight);
					win.focus();
				});
			}
			/*文章路径初始化*/
			that.articleBhInit(robotPath,cateId,pageType);
			
			/**********************其他**********************/

			/**ie中复制或者手写网址到ck中会自动转换成a标签，但是没有target属性 **/
			$('a').click(function(){
				var div = $(this).parents('div.c-text-ans');
				if(div && div.length > 0){
					//修复ie低版本浏览器上A标签弹出空白窗口的问题 modify by eko.zhan at 2016-11-09 21:00
					if (navigator.isLowerBrowser()){
						$('a[onclick^="jump"]').attr('target', '_self');
						$('a[href^="http"]').attr('target', '_blank');
					}else{
						$(this).attr('target','_blank');
					}
				}
			});
			
			/* 计算目录区域高度 */
			var json = params.groupList;
			var length = json.length;
			length = Math.ceil(length/3);
			var singleHeight = $("[class='catalog-list'] li.level1").height();
			$("[class='main_ml']").height(length==0?50:(length * singleHeight + 20));
			$("[class='catalog-list']").height(length==0?50:(length * singleHeight + 20));
			
			/* 实例对比、关联tab样式微调 */
			$("[class='tabs']").css("padding","0px");
			
			/* 1024的屏幕，截取字符串 */
			if(screen.width == 1024){
				$("[class='catalog-list']").find("li a").each(function(){
					var text = $(this).attr("title");
					if(text.length > 6){
						text = text.substring(0,6) + '...';
						$(this).text(text);
					}
				});
			}

			/* 标题的简介折叠与展开 */
			$("body table[class='tab_cont']").on("click","[name='showOrHideAbs']",function(){
				var _this = $(this).parent().next();
				if($(_this).is(":hidden")){
					$(this).find("a").text("折叠↑");
					$(_this).show();
				}else{
					$(this).find("a").text("展开↓");
					$(_this).hide();
				}
			});

			/* 标签轮播 */
			$('#slider').jqueryScroll();
			var _this = $('.wait-pipei dd');
			_this.click(function(){
				_this.find('div').eq($('dd').index(this)).slideToggle('300');
			});
			$("input[name=all]").click(function(){
			    if(this.checked){
			        $("input[name=check_list]").prop("checked", true);
			    }else{
			        $("input[name=check_list]").prop("checked", false);
			    }
			});
			
			/*锚点定位*/
			if(spotId){
				/*定位有偏移*/
				// location.hash = '#spot' + spotId;
				try {
					$('#spot' + spotId).prepend('<input type="text" style="width:1px;" id="spotAAA">');
					$('#spotAAA').focus();
					$('#spotAAA').remove();
				} catch(e){
					console.log(e);
				}
			}
		},
		/*初始化*/
		init: function () {
			/*时光轴--初始化*/
			that.timeline.init();
		}
	});
	/*初始化*/
	that.init();
});

