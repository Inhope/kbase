function fmtDate(value){
	if(value != undefined){
		return $.fn.datebox.defaults.formatter(new Date(value.time));
	}else{
		return "";
	}
}
//操作按钮
function operateFn(value,row,index){
	var html = "<font class='moveStyle' onclick='moveUp(\""+row.id+"\");'>上移</font> | <font class='moveStyle' onclick='moveDown(\""+row.id+"\")';>下移</font>";
	return html;
}
//上移
function moveUp(id){
	$.ajax({
		type: "POST",
		url: $.fn.getRootPath() + "/app/biztpl/biz-tpl-property!moveUp.htm",
		data: "id="+id,
		dataType:"json",
		success: function(data) {
			if(data.status == 0){
				doSearch();
			}else{
				alert("操作失败");
			}
		}
	});
}
//下移
function moveDown(id){
	$.ajax({
		type: "POST",
		url: $.fn.getRootPath() + "/app/biztpl/biz-tpl-property!moveDown.htm",
		data: "id="+id,
		dataType:"json",
		success: function(data) {
			if(data.status == 0){
				doSearch();
			}else{
				alert("操作失败");
			}
		}
	});
}
function doSearch(){
	var name = $("#name").val();
	var status = $("#status").val();
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	var param = {
		name: name,
		status: status,
		startDate: startDate,
		endDate: endDate
	};
	$('#dataList').datagrid({
		url : $.fn.getRootPath() + "/app/biztpl/biz-tpl-property!dataList.htm",
		queryParams :param
	});
}
//关闭iframe并刷新页面
function closeIframe(){
	$("#openEditPropDiv").dialog("close");
	doSearch();
}

$(function(){
	//查询
	$("#btnSearch").on("click",function(){
		doSearch();
	});
	//重置
	$("#btnReset").on("click",function(){
		//$("#name").textbox("setValue","");
		$("#name").val("");
		$("#status").val("");
		$("#startDate").val("");
		$("#endDate").val("");
	});
	
	//新增、编辑
	$("#btnAdd,#btnEdit").on("click",function(){
		var _url = $.fn.getRootPath() + "/app/biztpl/biz-tpl-property!edit.htm";
		if($(this).attr("id") == "btnEdit"){
			var row = $("#dataList").datagrid("getSelections");
			if (row.length == 1){
				_url += "?id="+row[0].id;
			}else{
				alert("请选择一条数据");
				return;
			}
		}
		//打开编辑页面所在iframe
		$("#openEditPropIframe")[0].src = _url;
		$("#openEditPropDiv").dialog("open");
	});
	
	//删除
	$("#btnDel").on("click",function(){
		var _url = $.fn.getRootPath() + "/app/biztpl/biz-tpl-property!delete.htm";
		var rows = $("#dataList").datagrid("getSelections");
		if (rows.length > 0){
			var ids = [];
			for(var i=0;i<rows.length;i++){
				ids.push(rows[i].id);
			}
			if(confirm("确定要删除选中的数据吗？")){
				$.ajax({
					type: "POST",
					url: _url,
					data: "ids="+ids,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){
							alert("操作成功");
							doSearch();
						}else{
							alert("操作失败");
						}
					}
				});
			}
		}else{
			alert("请选择数据");
			return false;
		}
	});
});