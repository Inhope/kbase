var _load=function(){
	var mask = $('<div id="mask"></div>');
	var test = $('<div class="test"></div>');
	var window_w = $(window).width();
	var window_h = $(window).height();
	var _test = function(){
		return $('body').append(test);
	}
	var _mask = function(){
		return $('body').append(mask);
	}
	return {
		_test : _test,
		_mask : _mask,
		_w : window_w,
		_h : window_h,
		test : test,
		mask : mask
	}
}();
;(function ($) {
	$.fn.table=function (options) {
		var defaults={
			 close:'#cancel'
		}
		var options=$.extend(defaults,options);
		return this.each(function(){
			_load._mask();
			_load._test();
			_load.test.load( options.url, function( response, status, xhr ) { 
				if(status=='success'){
					var shl_w=$(options.w_h).width();
					var shl_h=$(options.w_h).height();
					var offset_w=(_load._w-shl_w)/2;
					var offset_h=(_load._h-shl_h)/2;
					var scroll_L=$(document).scrollLeft();
					var scroll_T=$(document).scrollTop();
					$('body').css('overflow','hidden');
					_load.mask.css({
						'width':(_load._w+scroll_L)+'px',
						'height':(_load._h+scroll_T)+'px',
						'display':'block'
					})
					_load.test.css({
						'position':'absolute',
						'top':(offset_h+scroll_T)+'px',
						'left':offset_w+'px'
					});
					$(options.close).click(function(){
						$(options.selector2).remove();
						$('body').css('overflow','auto');
						_load.mask.css('display','none');
						$('#mask').remove();
					})
				}
			});
		});
	};
})(jQuery);