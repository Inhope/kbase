$(function(){
	$('.wd_bj').click(function(){
		var wd_p_top = $(this).parent().offset().top;
		var wd_top = $(this).offset().top;
		// alert (wd_top);
		$(this).css('top','wd_top')
	})
	$(window).bind('load resize scroll',function(){
		$('.head_top').css('width',($(window).width()-310)+'px')
	});

	$(window).bind('load resize scroll',function(){
		$('.right_sidebar').css('width',($(window).width()-310)+'px')
	});

	$('body').on('click','.more',function(){
		$('.drop_down').eq($('.more').index(this)).slideDown('normal')
		$(this).parent('h2').css('display','none');
	});

	$('body').on('click','.up',function(){
		$('.drop_down').eq($('.up').index(this)).slideUp('normal')
	});


/*	$('.pipei dd').click(function(){
		$('.pipei dd').find('div').eq($('dd').index(this)).slideToggle('300')
	});*/

	$('.head_form button').click(function(){
		$('.head_form').find('.none').fadeToggle(400)
	});

	$("input[name=all]").click(function(){    
	    if(this.checked){    
	        $("input[name=check_list]").prop("checked", true);   
	    }else{    
	        $("input[name=check_list]").prop("checked", false); 
	    }    
	}); 

	$(".content-tab td  i").click(function(){ 
		$(this).eq($(this).index()).toggleClass('ico-off');
	});
	
	
	 $('body').on('mouseenter','.q_js,.q_as,.wz_more',function(){
	 	$(this).find('.ico-js,.q-ask-ico,.ico_wz_d').css('display','');
	 });
	 $('body').on('mouseleave','.q_js,.q_as,.wz_more',function(){
	 	$(this).find('.ico-js,.q-ask-ico,.ico_wz_d').fadeOut(3000);
	 });
	
	/*$( ".sortable" ).sortable({
     	cursor: "move",
     	items :"li>div",                   
      	opacity: 0.6,                       
     	revert: true,
     	cancel:'span',                 
     	});
		$('.sortable li>div').mousedown(function(){
			$(this).addClass('drag');
		}).mouseup(function(){
			$(this).removeClass('drag');
		}).mouseleave(function(){
			$(this).removeClass('drag');
	})*/
	//添加介绍
	var bian_ans=$('<div class="q_ask_wz2"><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div>');
	$('.q_ask_ans').click(function(){
		if(editCheck()){
			var _bian_ans = $('<div class="q_ask_wz2"><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div>');
			_bian_ans.insertBefore($(this)).slideDown('fast');
			
			var _newId = 'Draft' + new Date().getTime();
			var _textarea = document.createElement('textarea');
			_textarea.id = _newId;
			$(this).prev().prepend(_textarea);
			var _editor = CkEditImage.create.replace(_textarea,options('0'));
			map[_newId] = _editor;
			$(this).css('display','none');
		}
	})
	
	
	/**介绍  编辑器确定取消按钮事件**/
		$('body').on('click','div.q_ask_wz2 button.bj_confirm',function(){
			var textarea = $(this).parents('div.q_ask_wz2').children('textarea');
			var id = textarea.attr('id');
			var editor = map[id];
			var content = editor.getData();
			if(content && content.length > 0){
				var showDiv = $(this).parents('div.q_ask_wz2').siblings('div.q_js');
				if(!showDiv || showDiv.length == 0){
					var div = $('<div class="q_js"><a href="javascript:void(0)" style="display:none;"><i class="ico_bj"></i>编辑</a><i class="ico-pic"></i><div class="q_js_con"></div><i class="ico-js" style="display:none;"></i>');
					$(this).parents('div.q_ask_show2').append(div);
				}
				var js_div = $(this).parents('div.q_ask_wz2').siblings('div.q_js').children('div.q_js_con');
				js_div.html(content);
				$(this).parents('div.q_ask_wz2').siblings('div.q_js').css('display','');
				editor.destroy();
				textarea.remove();
				$(this).parents('div.q_ask_wz2').remove();
			} else{
				alert('内容不能为空');
			}
			
		});
		$('body').on('click','div.q_ask_wz2 button.bj_cancel',function(){
			$(this).parents('div.q_ask_wz2').css('display','none');
			var textarea = $(this).parents('div.q_ask_wz2').children('textarea');
			var id = textarea.attr('id');
			var editor = map[id];
			editor.destroy();
			textarea.remove();
			
			var showDiv = $(this).parents('div.q_ask_wz2').siblings('div.q_js');
			if(showDiv&&showDiv.length > 0){
				$(showDiv).css('display','');
			}else{
				$(this).parents('div.q_ask_show2').children('div.q_ask_ans').css('display','');
			}
			$(this).parents('div.q_ask_wz2').remove();
		});
		/**介绍  编辑机器确定取消按钮事件**/

	//修改编辑内容
	var bian_j=$('<div class="q_ask_wz">编辑器放这里---修改介绍内容<div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div>');
	$('body').on('dblclick','div.q_js div.q_js_con',function(){
		if(editCheck()){
			$(this).parent('div.q_js').children('a').click();
		}
	})
	$('body').on('click','div.q_js a',function(){
		var _bian_ans = $('<div class="q_ask_wz2"><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div>');
		_bian_ans.insertBefore($(this).parent('div.q_js')).slideDown('fast');
		
		var _newId = 'Draft' + new Date().getTime();
		var _textarea = document.createElement('textarea');
		_textarea.id = _newId;
		
		$(this).parent('div.q_js').siblings('div.q_ask_wz2').prepend(_textarea);
		var _editor = CkEditImage.create.replace(_textarea,options('0'));
		_editor.setData($(this).siblings('div.q_js_con').html());
		map[_newId] = _editor;
		$(this).parent('div.q_js').siblings('div.q_ask_wz2').css('display','');
		
		$(this).parent('div.q_js').css('display','none');
	});

	

	$('body').on('click','.ico-delete3',function(){
		$(this).parent().next().remove();
		$(this).parent().remove();
		
	}) 

	//介绍删除
	$('body').on('click','.ico-js',function(){
		if (window.confirm('确定删除吗')){
			$(this).parent('div.q_js').siblings('div.q_ask_ans').css('display','');
			$(this).prev('div.q_js_con').html('');
			$(this).parent('div.q_js').css('display','none');
		}
	})
	var add_ans=$('<div class="q_ask_top"><div class="q_ask_top_h"><em class="question-answer question"></em></div>	<div class="q_ask_wz"><div class="wd_bj"><div class="drop_down"><ul class="wz"><li id="dim"><div class="wz_l">维&nbsp;&nbsp;&nbsp;&nbsp;度:</div><div class="wz_r"><em></em></div></li><li id="cmds"><div class="wz_l">指&nbsp;&nbsp;&nbsp;&nbsp;令:</div><div class="wz_r"><em></em></div></li><li><div class="wz_l">有效期:</div><div class="wz_r"><input type="text" id="ans_startDate" _name="startDate" class="wz_date" value="" onclick="WdatePicker({maxDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"> ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_startDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li><div class="wz_l">追溯期:</div><div class="wz_r"><input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li id="attrachment"><div class="wz_l">附&nbsp;&nbsp;&nbsp;&nbsp;件:</div><div class="wz_r"></div></li><li><div class="wz_l"></div><div class="wz_r"><i class="wz_up">收起</i></div></li></ul></div>	<h2>维度:<em class="wd_t"></em><i class="more">更多</i></h2>								    </div><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div></div>');	
	var add_ask=$('<div class="q_ask_add"><em>问题:</em><input type="text" value="请填写问题" ><i class="ico-delete3"> </i> </div>');
	var info=$('<ul class="more2"><li id="more_01"><a href="javascript:void(0)">属性</a></li><li id="more_02"><a href="javascript:void(0)">扩展问</a></li><li id="more_03"><a href="javascript:void(0)">关键字</a></li><li id="more_04"><a href="javascript:void(0)">标签</a></li></ul>');
	
	var ico = $('<div class="num"><a href="javascript:void(0)" class="em_num">10</a></div>');
	/***显示更多答案***/
	$('body').on('click','.ico_wz_m',function(){
		var more  = $(this).parents('div.q_ans').children('div.wz_more[isKbase!="false"]:hidden');
		var img_url = '';
		var ico = $(this).children('div.num');
		if(more && more.length > 0){
			img_url = pageContext.contextPath + '/resource/biztpl/images/up_wz.gif';
			$(this).css('background','url("'+img_url+'") no-repeat left center');
			$(this).text('收起').append(ico);
			$(this).parents('div.q_ask_show').find('.wz_more[isKbase!="false"]').slideDown('normal');
		}else{
			img_url = pageContext.contextPath + '/resource/biztpl/images/more_wz.gif';
			$(this).css('background','url("'+img_url+'") no-repeat left center');
			$(this).text('更多').append(ico);
			$(this).parents('div.q_ask_show').find('.wz_more[isKbase!="false"]').not(':first').slideUp('normal');
		}
	/*$('.ico_wz_m').toggle(function(){
		var ico = $(this).children('div.num');
		//$(this).css('background','url("../images/up_wz.gif\') no-repeat left center');
		$(this).text('收起').append(ico)
		$('.q_ask_show').eq($('.ico_wz_m').index(this)).find('.wz_more').slideDown('normal')
	},function(){
		var ico = $(this).children('div.num');
		//$(this).css('background','url(\'../images/more_wz.gif\') no-repeat left center');
		$(this).text('更多').append(ico);
		$('.q_ask_show').eq($('.ico_wz_m').index(this)).find('.wz_more').not(':first').slideUp('normal')*/
	})
	/***显示更多答案***/
	
	/***答案更多属性**/
	$('body').on('click','.wz_zk',function(){
		$('.wz_down').eq($('.wz_zk').index(this)).slideDown('normal')
	});
	$('body').on('click','.wz_up',function(){
		//$('.wz_down').eq($('.wz_up').index(this)).slideUp('normal');
		$(this).parents('ul.wz').parent('div').slideUp('normal');
		$(this).parents('div.drop_down').next('h2').css('display','');
	});
	/**答案更多属性**/
	
	/**新增知识**/
	$('.add_zs').click(function(){
		if(editCheck()){
			selectDim($(this),null);
		}
	});
	
	/**添加答案或者新建标准问先选择维度**/
	function selectDim(obj1,obj2){
		test.load( "app/biztpl/article!pickAnswerAttr.htm?op=add&t="+new Date().getTime(), function( response, status, xhr ) {
				if(status=='success'){
					var shl_w=$('.wd_tab').width();
					var shl_h=$('.wd_tab').height();
					var offset_w=(window_w-shl_w)/2;
					var offset_h=(window_h-shl_h)/2;
					var scroll_L=$(document).scrollLeft();
					var scroll_T=$(document).scrollTop();
					$('body').css('overflow','hidden');
					mask.css({
						'width':(window_w+scroll_L)+'px',
						'height':$(document).height()+'px',
						'display':'block'
					})
					$('.wd_tab').css({
						'position':'absolute',
						'top':(offset_h+scroll_T)+'px',
						'left':offset_w+'px'
					}).hide().fadeIn(300);
					$('#cancel').click(function(){
						$('.wd_tab').hide();
						mask.css('display','none');
						$('body').css('overflow','auto');
					});
					
					$('div.wd_tab input[_type="all"]').click(function(){
						var code = $(this).attr('code');
						$('div.wd_tab input[code="'+code+'"]').attr("checked", $(this).is(':checked'));
					});
					$('div.button button#ok').click(function(){
							
							var dimName = '';
							var dimId = '';
							var _dimname = '';
							$('div.wd_tab input[type="checkbox"]:checked').not('input[_type="all"]').each(function(i,item){
								dimName += $(item).attr('_name') +  ','
								dimId += $(item).attr('name') +  ','
							});
							if(dimId && dimId.length > 0){
								dimId = dimId.substring(0,dimId.length-1);
								dimName = dimName.substring(0,dimName.length-1);
							}
							_dimname = dimName;
							if(dimId.length == 0){
								dimName = '所有维度'
							}else{
								dimName = dimName.length > 40 ? dimName.substring(0,40) + '...' : dimName;
							}
							if(obj1 && obj1.length > 0){//新增标准问
								//var q_div =  $(this).parents('li.q_ask').next('li.q_ask').children('div.q_ask_show');//标准问题所在div
								//var add_question=$('<div class="q_ask_show"><em>问题:</em><i class="ico-move"></i><span class="q-ask-span"><input type="text" value=""></span><span class="q-ask-ico"><i class="ico-add"></i><i class="ico-delete"></i></span></div>');
								var add_question = $('<div class="q_ask_show"><div class="q_as"><em class="question-answer question"></em><span class="q-ask-span" style="border: 1px solid red;"><input type="text" name="kbs-question" value="" id=""><i class="ico-more"></i><input type="hidden" _this_q_id="'+new Date().getTime()+'" name="tagIds" value=""></span><span class="q-ask-ico" style="display: none;"><i class="ico-delete"></i> <i class="ico-add"></i></span></div><div class="q_ans"></div></div>');
								var add_answer=$('<div class="q_ask_top"><div class="q_ask_top_h"><i></i></div>	<div class="q_ask_wz"><div class="wd_bj"><div class="drop_down">' +
										'<ul class="wz"><li id="dim"><div class="wz_l">维&nbsp;&nbsp;&nbsp;&nbsp;度:</div><div class="wz_r"><em></em></div></li><li id="cmds"><div class="wz_l">指&nbsp;&nbsp;&nbsp;&nbsp;令:</div><div class="wz_r"><em></em></div></li><li><div class="wz_l">有效期:</div><div class="wz_r"><input type="text" id="ans_startDate" _name="startDate" class="wz_date" value="" onclick="WdatePicker({maxDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"> ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_startDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li><div class="wz_l">追溯期:</div><div class="wz_r"><input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li id="attrachment"><div class="wz_l">附&nbsp;&nbsp;&nbsp;&nbsp;件:</div><div class="wz_r"></div></li><li><div class="wz_l"></div><div class="wz_r"><i class="wz_up">收起</i></div></li></ul>'+
										'</div>	<h2>维度:<em class="wd_t"></em><i class="more">更多</i></h2>								    </div><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div></div>');
								$(obj1).parents('li.q_ask').next("li.q_ask").prepend(add_question);
								
								
								add_question.append(add_answer);
								var _textarea = document.createElement('textarea');
								var _newId = new Date().getTime();
								_textarea.id = _newId;
								var _editor = CkEditImage.create.replace(_textarea,options('1'));
								map[_newId]  =  _editor;
								add_question.children('div.q_ask_top').children('div.q_ask_wz').prepend(_textarea);
								
								//维度显示赋值
								$(add_question).find('div.q_ask_top').find('div.wd_bj h2 em').text(dimName);
								$(add_question).find('div.q_ask_top').find('div.wd_bj h2 em').attr('title',_dimname);
								$(add_question).find('div.q_ask_top .drop_down').find('ul li[id="dim"] div.wz_r').find('em').text(dimName);
								$(add_question).find('div.q_ask_top .drop_down').find('ul li[id="dim"] div.wz_r').find('em').attr('title',_dimname);
								
								$(add_question).find('div.q_ask_top .drop_down').find('ul li[id="dim"] div.wz_r').attr('dimId',dimId);
								//时间继承
								$(add_answer).find('div.wd_bj').find('input[_name=startDate]').val($('input[name="startDate"]').val());
								$(add_answer).find('div.wd_bj').find('input[_name=endDate]').val($('input[name="endDate"]').val());
								$(add_answer).find('div.wd_bj').find('input[_name=traceDate]').val($('input[name="traceDate"]').val());
								
								
							}else if(obj2 && obj2.length > 0){//新增答案
								//维度校验
								var ans =	$(obj2).parents('div.q_as').siblings('div.q_ans').children('div.wz_more');
								var dimArray = new Array();//存储已经选择的维度组合
								ans.each(function(i, item){
									var dimIds = $(item).attr('dimid');
									dimArray.push(dimIds.split(',').sort().join(','));
								})
								var flg = checkDim(dimArray,dimId);
								if(!flg){
									return false;
								}
								var _index=$('div.q_ask_show span.q-ask-ico i.ico-add').index($(obj2));
							 	$(obj2).parents('.q_ask_show').append(add_ans);
								var _textarea = document.createElement('textarea');
								var _newId = new Date().getTime();
								_textarea.id = _newId;
								var _editor = CkEditImage.create.replace(_textarea,options('1'));
								$(obj2).parents('.q_ask_show').children('div.q_ask_top').children('div.q_ask_wz').prepend(_textarea);
								map[_newId]  =  _editor;
							 	
							 	$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.wd_bj h2 em').text(dimName);
							 	$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.wd_bj h2 em').attr('title',_dimname);
								
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').find('em').text(dimName);
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').find('em').attr('title',_dimname);
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').attr('dimId',dimId);
								
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('input[_name=startDate]').val($('input[name="startDate"]').val());
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('input[_name=endDate]').val($('input[name="endDate"]').val());
								$(obj2).parents('div.q_as').siblings('div.q_ask_top').find('div.drop_down').find('input[_name=traceDate]').val($('input[name="traceDate"]').val());
								
							}
							
							$('.wd_tab').remove();
							mask.css('display','none');
							$('body').css('overflow','auto');
					});
					
					$('table tr td').not('td:first-child').click(function(e){
						var _this=$(e.target.tagName);
						_this.eq(_this.index(this)).addClass('on');
					});
				}
			})
	}
	
	/**删除标准问**/
	$('body').on('click','.q_ask_show i.ico-delete',function(){
		if (window.confirm('确定删除吗')){
			var attrId = $(this).parent('span.q-ask-ico').siblings('span.q-ask-span').children('input').attr('attrid');
			if(attrId && attrId.length > 0){
				$('li[attrid="'+attrId+'"]').attr('delflag','1');
			}
			$(this).parents('div.q_ask_show').remove();
		}
	})
	 /***添加答案**/
	 $('body').on('click','.q_ask_show i.ico-add',function(){
		 if(editCheck()){
	 		selectDim(null,$(this));
		 }
	 })
	 $('body').on('mouseover','.q_ask_show i.ico-add',function(){
	 	$(this).attr('title','新增答案');
	 });
	 $('body').on('mouseover','.q_ask_show i.ico-delete',function(){
	 	$(this).attr('title','删除知识');
	 });
	 $('body').on('mouseover','.q_ask_show div.ico_wz_d',function(){
	 	$(this).attr('title','删除答案');
	 });
	 $('body').on('mouseover','.q_js i.ico-js',function(){
	 	$(this).attr('title','删除介绍');
	 });
	 
	  /***答案编辑器按钮事件**/
	 //确定按钮
	 $('body').on('click', 'div.q_ask_show button.bj_confirm', function(){
	 		var answer = $('<div style="display:none;" class="wz_more"><h2 class="wz_title gray">维度:<em class="wz_wd"></em><i class="wz_zk">展开</i></h2><div class="wz_more_c"></div><div class="wz_down">' +
	 				'<ul class="wz"><li><div class="wz_l"></div><div class="wz_r"><i class="wz_up">收起</i></li><li id="dim"><div class="wz_l">维&nbsp;&nbsp;&nbsp;&nbsp;度:</div><div class="wz_r"><em></em></div></li><li id="cmds"><div class="wz_l">指&nbsp;&nbsp;&nbsp;&nbsp;令:</div><div class="wz_r"><em></em></div></li><li><div class="wz_l">有效期:</div><div class="wz_r"><input type="text" id="ans_startDate" _name="startDate" class="wz_date" value="" onclick="WdatePicker({maxDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})" disabled="disabled"> ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_startDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})" disabled="disabled"></div></li><li><div class="wz_l">追溯期:</div><div class="wz_r"><input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})" disabled="disabled"></div></li><li id="attrachment"><div class="wz_l">附&nbsp;&nbsp;&nbsp;&nbsp;件:</div><div class="wz_r"></div></li></ul>'+
	 				'</div><div class="ico_wz_d" style="display: none;"></div></div>');
			//var q_ask_con = $('<div class="q_ask_con"></div>');
			var answer_div = $(this).parents('div.q_ask_top').siblings('div.q_ans');
			//删除编辑器及其相关元素
			var textarea = $(this).parent().parent().children('textarea');
			var id  = textarea.attr('id');
			var editor = map[id];
			var content = editor.getData();
			if(content && content.length > 0){
				var ans = $(this).parents('div.q_ask_show').children('div.q_ans').children('div.wz_more');
				if(!ans || ans.length == 0){
					$(this).parents('div.q_ask_show').children('div.q_ans').prepend($('<em class="question-answer answer"></em>'));
				}
				var q_ask_con =$(this).parents('div.q_ask_show').find('div.wz_more[editid="'+id+'"]');
				var answer_show = $(this).parents('div.q_ask_show').find('div.wz_more[isKbase!="false"]');
				var icon = $('<div class="ico_wz_m">更多<div class="num"><a class="em_num" href="javascript:void(0)"></a></div></div>');
				if(!q_ask_con || q_ask_con.length == 0){//新增答案
					q_ask_con = answer;
					answer_div.append(q_ask_con);
					
					if(answer_show.length == 0){
						$(q_ask_con).addClass('mt');
						$(q_ask_con).css('display','');
					}else{
						var icon = $('<div class="ico_wz_m">更多<div class="num"><a class="em_num" href="javascript:void(0)"></a></div></div>');
						$(icon).find('div.num a').text(answer_show.length + 1);
						$(this).parents('div.q_ask_top').siblings('div.q_ans').children('div.wz_more.mt').children('.ico_wz_m').remove();
						$(this).parents('div.q_ask_top').siblings('div.q_ans').children('div.wz_more.mt').children('.ico_wz_d').before(icon);
					}
				}
				
				$(q_ask_con).children('div.wz_more_c').html(content);
				editor.destroy();
				textarea.remove();
				
				//维度保存
				var dimName = $(this).parent('div.q_ask_bt').prev().find('h2 em').text();
				var dimId = $(this).parent('div.q_ask_bt').prev().find('ul li[id="dim"] .wz_r').attr('dimId');
				var _dimName = $(this).parent('div.q_ask_bt').prev().find('h2 em').attr('title');
				$(q_ask_con).attr('dimName',_dimName);
				$(q_ask_con).attr('dimId',dimId);
				
				var dim_tr = $(q_ask_con).children('div.wz_down').find('ul li[id="dim"] .wz_r');
				$(dim_tr).attr('dimId',dimId);
				$(dim_tr).children('em').text(dimName);
				$(dim_tr).children('em').attr('title',_dimName);
				$(q_ask_con).children('h2.wz_title.gray').find('em').text(dimName);
				$(q_ask_con).children('h2.wz_title.gray').find('em').attr('title',_dimName);
				
				//附件
				var p = $(this).parent('div.q_ask_bt').prev('div.wd_bj').children('div.drop_down').find('ul li[id="attrachment"] .wz_r').find('p');
				$(q_ask_con).children('div.wz_down').find('ul li[id="attrachment"] .wz_r').append(p);
				
				//时间
				var startDate = $(this).parent('div.q_ask_bt').prev('div.wd_bj').children('div.drop_down').find('input[_name="startDate"]').val();
				var endDate = $(this).parent('div.q_ask_bt').prev('div.wd_bj').children('div.drop_down').find('input[_name="endDate"]').val();
				var traceDate = $(this).parent('div.q_ask_bt').prev('div.wd_bj').children('div.drop_down').find('input[_name="traceDate"]').val();
				$(q_ask_con).find('input[_name="startDate"]').val(startDate);
				$(q_ask_con).find('input[_name="endDate"]').val(endDate);
				$(q_ask_con).find('input[_name="traceDate"]').val(traceDate);
				
				//指令
				var cmds = $(this).parent('div.q_ask_bt').prev().find('ul li[id="cmds"] .wz_r').find('em').text();
				$(q_ask_con).find('div.wz_down').find('ul li[id="cmds"]').find('em').text(cmds);
				$(this).parents('div.q_ask_top').remove();
			}else{
				alert('内容不能为空');
			}
		});
		
		//取消按钮
		 $('body').on('click', 'div.q_ask_show button.bj_cancel', function(){
			var textarea = $(this).parents('div.q_ask_wz').children('textarea');
			var id = textarea.attr('id');
			var editor = map[id];
			$(this).parents('div.q_ask_show').find('div.q_ask_con[editid="'+id+'"]').css('display','');
			editor.destroy();
			textarea.remove();
			$(this).parents('div.q_ask_top').remove();
		});
		//删除按钮
		$('body').on('click', 'div.q_ask_show button.bj_del', function(){
			var msg = '确定删除吗？';
			var length = $(this).parents('div.q_ask_show').children('div.q_ask_con').length;
			if(length < 2){
				msg = '该标准问只有一个答案,确定要删除吗？';
			}
			if (window.confirm(msg)){
				var editid = $(this).parents('div.q_ask_top').find('textarea').attr('id');
				$(this).parents('div.q_ask_show').find('div.q_ask_con[editid="'+editid+'"]').remove();
				var textarea = $(this).parents('div.q_ask_wz').children('textarea');
				var editor = map[editid];
				editor.destroy();
				textarea.remove();
				$(this).parents('div.q_ask_top').remove();
			}
		})
		/***答案编辑器按钮事件**/
		
		// 答案删除  当有多个答案，删除第一个答案，并将第二个答案变成第一个答案，修改答案个数
		$('body').on('click', 'div.q_ask_show div.ico_wz_d', function(){
			var msg = '确定删除吗？';
			var length = $(this).parents('div.q_ans').children('div.wz_more').length;
			if(length < 2){
				msg = '该标准问只有一个答案,确定要删除吗？';
			}
			if (window.confirm(msg)){
				var div = $(this).parents('div.q_ans');
				var icon = $(div).children('div.wz_more.mt').find('div.ico_wz_m');
				var ans = $(div).children('div.wz_more');
				var index = $(div).find('div.ico_wz_d').index(this);
				if(length < 2){
					$(ans).prev('em.question-answer.answer').remove();
				}
				$(this).parent('div.wz_more').remove();
				if(ans && ans.length > 1){
					if(index == 0){
						var wz_more  = $(div).children('div.wz_more:first');
						$(wz_more).addClass('mt');
						$(wz_more).css('display','');
					}
					var num = $(icon).children('div.num').find('a.em_num').text() - 1;
					$(div).children('div.wz_more.mt').find('div.ico_wz_m').remove();
					if(num > 1){
						$(icon).children('div.num').find('a.em_num').text(num);
						$(div).children('div.wz_more.mt').find('div.ico_wz_d').before(icon);
					}
				}
			}
		});
	  
		/**答案显示div双击事件*/
		$('body').on('dblclick', 'div.wz_more', function(){
					//editData($(this).next('span.q-ask-ico').children('i.ico-add'),$(this))
			if(editCheck()){
					var add_ans=$('<div class="q_ask_top"><div class="q_ask_top_h"><i>答案:</i></div>	<div class="q_ask_wz"><div class="wd_bj"><div class="drop_down">' +
							'<ul class="wz"><li><div class="wz_l"></div><div class="wz_r"><i class="wz_up">收起</i></div></li><li id="dim"><div class="wz_l">维&nbsp;&nbsp;&nbsp;&nbsp;度:</div><div class="wz_r"><em></em></div></li><li id="cmds"><div class="wz_l">指&nbsp;&nbsp;&nbsp;&nbsp;令:</div><div class="wz_r"><em></em></div></li><li><div class="wz_l">有效期:</div><div class="wz_r"><input type="text" id="ans_startDate" _name="startDate" class="wz_date" value="" onclick="WdatePicker({maxDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"> ― <input type="text" id="ans_endDate" _name="endDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_startDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li><div class="wz_l">追溯期:</div><div class="wz_r"><input type="text" id="ans_traceDate" _name="traceDate" class="wz_date" value="" onclick="WdatePicker({minDate:\'#F{$dp.$D(\\\'ans_endDate\\\')}\',dateFmt:\'yyyy-MM-dd\'})"></div></li><li id="attrachment"><div class="wz_l">附&nbsp;&nbsp;&nbsp;&nbsp;件:</div><div class="wz_r"></div></li></ul>'+
							'</div>	<h2>维度:<em class="wd_t"></em><i class="more">更多</i></h2>								    </div><div class="q_ask_bt"><button class="bj_confirm">确定</button><button class="bj_cancel">取消</button></div></div></div>');
				 		$(this).parents('div.q_ask_show').append(add_ans);
						var _textarea = document.createElement('textarea');
						if(!$(this).attr('editid')){
							$(this).attr('editid',new Date().getTime());
						}
						var _newId = $(this).attr('editid');
						var dimname = $(this).attr('dimname');
						if(!dimname || dimname.length == 0){
							dimname = '所有维度';
						}else{
							dimname = dimname.length > 40 ? dimname.substring(0,40) + '...' : dimname;
						}
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj h2 em').attr('title',$(this).attr('dimname'));
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj h2 em').text(dimname);
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('ul li[id="dim"] div.wz_r').find('em').attr('title',$(this).attr('dimname'));
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('ul li[id="dim"] div.wz_r').find('em').text(dimname);
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('ul li[id="dim"] div.wz_r').attr('dimId',$(this).attr('dimId'));
						//时间
						var  startDate = $(this).children('div.wz_down').find('input[_name="startDate"]').val();
						var  endDate = $(this).children('div.wz_down').find('input[_name="endDate"]').val();
						var  traceDate = $(this).children('div.wz_down').find('input[_name="traceDate"]').val();
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('input[_name="startDate"]').val(startDate);
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('input[_name="endDate"]').val(endDate);
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('div.wd_bj .drop_down').find('input[_name="traceDate"]').val(traceDate);
						
						//指令
						var cmds = $(this).find('ul li[id="cmds"]').find('em').text();
						$(this).parent('div.q_ans').siblings('div.q_ask_top').find('ul.wz li[id="cmds"]').find('em').text(cmds);
						
						_textarea.id = _newId;
						var _editor = CkEditImage.create.replace(_textarea,options('1'));
						_editor.setData($(this).children('div.wz_more_c').html());
						$(this).parents('div.q_ask_show').children('div.q_ask_top').children('div.q_ask_wz').prepend(_textarea);
						map[_newId]  =  _editor;
				 	//$(this).css('display','none');
			}
		});
		/**答案显示div双击事件*/
		
		/***答案附件预览**/
		$('div.q_ask_show').find('div.wz_down').find('ul li[id="attrachment"] .wz_r').find('a.wz_fj').click(function(){
		 	var name = $(this).text();
		 	var fileId = $(this).attr('fid');
		 	var url  = swf_address +  '/attachment/preview.do?fileName='+fileId + '.' + name.split('.')[1];
			window.open(url,"_blank") 
		 })
		 /***答案附件下载**/
		 $('div.q_ask_show').find('div.wz_down').find('ul li[id="attrachment"] .wz_r').find('a.wz_xz').click(function(){
		 	var fileId = $(this).siblings('a.wz_fj').attr('fid');
			var name = $(this).siblings('a.wz_fj').text();
			location.href = pageContext.contextPath + '/app/wukong/search!download.htm?fileId='+fileId+'&fileName='+name;
		 })
		 /***答案附件删除**/
		 $('body').on('click', 'i.wz_del', function(){
		 	if (window.confirm('确定删除吗')){
		 			var  a = $(this).siblings('a.wz_fj');
			 		var attId = $(a).attr('id');
					if($(a).attr('_type') == 'newUpload'){
							$(this).parent('p').remove();
					}else{
							var topNodeId = '0';
							var oid = $(this).parents('div.wz_more').attr('id');
							var msg = delAttament(topNodeId,oid,attId);
							if(msg){
								if(msg.result){
									var attachmentId = msg.attachmentId;
									$(a).parent('p').remove();
									alert('解除成功');
								}else{
									alert('解除失败');
								}
							}
				}
		 	}
		})
		

var mask=$('<div id="mask"></div>');
var test=$('<div class="test"></div>')
var window_w=$(window).width();
var window_h=$(window).height();
$('body').append(mask);
$('body').append(test);

/**
	 * 实例对比点击事件
	 
	$('input[name=slgl]').click(function(){
		$('input[name=slgl]').table({
			selector : 'input[name=slgl]',
			selector2 : '.shl',
			url : pageContext.contextPath + '/app/biztpl/article!slgl.htm' ,
			w_h : '.shl',
			close : '.shl_close'
		});
	});
	*/
	//url : pageContext.contextPath + '/app/biztpl/example!example.htm' ,
	//url : pageContext.contextPath + '/app/biztpl/article!slgl.htm' ,
	
/*$('.sidebar h3.ico-3').table({
	selector:'.sidebar h3.ico-3',
	selector2:'.module',
	url:'module_modify.html',
	w_h:'.module'
});*/
$('#upload').click(function(){
	var  url = encodeURI('app/biztpl/article!fuj.htm?type=0&categoryName='+$('[name="categoryName"]').val()+'&id='+articleId);
	$('#upload').table({
		selector:'#upload',
		selector2:'.fj',
		url:url,
		w_h:'.fj'
	});
});


$('input[name=version]').click(function(){
	test.load( "version.html", function( response, status, xhr ) {
		if(status=='success'){
			var shl_w=$('.version').width();
			var shl_h=$('.version').height();
			var offset_w=(window_w-shl_w)/2;
			var offset_h=(window_h-shl_h)/2;
			var scroll_L=$(document).scrollLeft();
			var scroll_T=$(document).scrollTop();
			$('body').css('overflow','hidden');
			mask.css({
				'width':(window_w+scroll_L)+'px',
				'height':(window_h+scroll_T)+'px',
				'display':'block'
			})
			$('.version').css({
				'position':'absolute',
				'top':(offset_h+scroll_T)+'px',
				'left':offset_w+'px'
			}).hide().fadeIn(300);
			$("#ver_all").click(function(){
				 if(this.checked){    
			        $("#ver_list :checkbox").prop("checked", true);   
			    }else{    
			        $("#ver_list :checkbox").prop("checked", false); 
			    } 
			})
			$("#ver_list i").click(function(){ 
				$(this).eq($(this).index()).toggleClass('ico-off');
			});
			$('#version_del').table({
				selector:'#version_del',
				selector2:'.info',
				url:'info.html',
				w_h:'.info',
				close:'.info_close'
			});
			$('.version_close').click(function(){
				$('.version').hide();
				mask.css('display','none');
				$('body').css('overflow','auto');
			})
		}
	});
});
function editCheck(){
	var editdiv = $('div.q_ask_top');
	var jseditdiv = $('div.q_ask_wz2');
	if((editdiv && editdiv.length > 0) || (jseditdiv && jseditdiv.length > 0)){
		alert('请先保存编辑器中的内容');
		return false;
	}
	return true;
}

$('input[name=property]').click(function(){
	test.load( "app/biztpl/article!property.htm?id="+articleId, function( response, status, xhr ) {
		if(status=='success'){
			var shl_w=$('.property').width();
			var shl_h=$('.property').height();
			var offset_w=(window_w-shl_w)/2;
			var offset_h=(window_h-shl_h)/2;
			var scroll_L=$(document).scrollLeft();
			var scroll_T=$(document).scrollTop();
			//$('div#mask').remove();
			mask.css({
				'width':window_w+'px',
				'height':$(document).height()+'px',
				'display':'block'
			})
			$('body').append(mask);
			//$('body').css('overflow','hidden');
			$('body').css('overflow','auto');
			$('.property').css({
				'position':'absolute',
				'top':(offset_h+scroll_T)+'px',
				'left':offset_w+'px',
				'width':'500px'
			}).hide().fadeIn(300);
			$("i.add").toggle( 
				function () {
					$(this).addClass('remove_click');
					$(this).parents('li').children('div').css('display','block');
					$('i.add').not(this).parents('li').children('div').css('display','none');
					$('i.add').not(this).removeClass('remove_click');
				}, 
				function () {
					$(this).removeClass('remove_click');
					$(this).parents('li').children('div').css('display','none');
				} 
			);
			$(".one_level_r").hover(function(){ 
				$(this).find("div.check_xl").css("display","block"); 
				},function(){ 
				$(this).find("div.check_xl").css("display","none"); 
	
			});
        	$('.check_xl').find('li').mouseover(function(){
        		$(this).attr('title',$(this).text()) 
        	})
			$(".select_checkBox").hover(function(){ 
						$(this).children('div.check_xl').css("display","block");
					},function(){
						$(".check_xl").css("display","none"); 
			}); 
			$(".property").bind("click",function(evt){
				if($(evt.target).parents(".prop_con > ul > li").length==0){
					$('.one_level').hide(); 
					if($('.one_level').css('display')=='none'){
						$('.prop_con i').removeClass('remove_click')
				  	}
				} 
			}); 
			$('.close,#cancel').click(function(){
				closeProperty();
			})
			
			/**数据赋值**/
			var val = $('textarea#textarea').text();
			if(val && val.length > 0){
				var data = eval(val);
				for(var i in data){  
					var tagId = data[i].tagId;
					var tagVal = data[i].tagVal;
					var obj = $('#'+tagId);
					if(tagVal &&  tagVal.length > 0){
						$(obj).find('input').val(tagVal);
					}else{
						var _type = $(obj).attr('_type');
						if(_type == 'select_choices'){
							$(obj).attr("selected",true);
						}else if(_type == 'multiple_choices'){
							$(obj).attr("checked",true);
						}
					}
				}
			}
			function closeProperty(){
				$('.property').hide();
				mask.css('display','none');
				$('div#mask').remove();
				$('body').css('overflow','auto');
			}
			//
			$('button[id=cancel]').click(function(){
				$('.property').hide();
				mask.css('display','none');
				$('div#mask').remove();
				$('body').css('overflow','auto');
			})
			$('button[id="ok"]').click(function(){
					var _property = [];
					var li = $('ul[id=allProperty]>li');
					li.each(function(i, item0){
						var type=$(item0).attr('type');
						
						if(type =='single_row_text'|| type=='date_text'){
							var property = {'id':'','tagId':'','tagVal':''};
							var val = $(item0).find('input');
							if(val.val()){
								property.id = $(item0).attr('_id');
								property.tagId = $(item0).attr('id');
								property.tagVal = val.val();
								_property.push(property);
							}
						}else{
								var p = $(item0).find('div[class=one_level_2]');
								p.each(function(i, item1){
									//	var tag = [];
									if(type=='multiple_choices'){
										var checkbox = 	$(item1).children('div').children('input[type=checkbox]');	//属性值列表
										var tagId;
										checkbox.each(function(i, cb){
										if($(cb).is(':checked')){//选中的属性
											tagId = $(cb).attr('id');//选中的属性值id
												if(tagId){
													var pro = {'id':'','tagId':'','tagVal':''};
													pro.tagId = tagId;
													pro.id = $(cb).attr('_id');
													//var select  =  $(cb).siblings('span.one_level_r').find('span.one_level_r select');//属性的关联数据
													//var box = $(cb).siblings('span.one_level_r').find('span.one_level_r span.select_checkBox');
													var select  =  $(cb).siblings('span.one_level_r').find('select');//属性的关联数据
													var box = $(cb).siblings('span.one_level_r').find('input[type=checkbox]');
													var cid;
													if($(select) && $(select).length > 0){
															cid = $(select).val(); //关联数据的id
															var c = {'id':'','tagId':'','tagVal':''};
															c.tagId = cid;
															c.id=$(select).find('option:selected').attr('_id');
															if(cid != '-1'){
																_property.push(c);
															}
													}else if($(box) && $(box).length > 0){
															box.each(function(i, b){
																var c = {'id':'','tagId':'','tagVal':''};
																if($(b).is(':checked')){
																	c.id=$(b).attr('_id');
																	c.tagId = $(b).attr('id');
																	_property.push(c);
																}
															})
													}
													
												}
												_property.push(pro);
										}
										})
									}else if(type=='select_choices'){
										var s = {'id':'','tagId':'','tagVal':''};
										var select  = $(item1).children('select');
										if(select && select.length > 0){
											s.tagId = $(select).val();
											s.id = $(select).find('option:selected').attr('_id');
											if($(select).val() != '-1'){
												_property.push(s);
											}
										}
									}
										
								})
						}
					})
					$('#textarea').text(JSON.stringify(_property));
					$("#textarea").text(JSON.stringify(_property));
					closeProperty();
			})
			//
		}
	});
});
	$('.head_top_tj').click(function(){
		//加载遮罩层
		ajaxLoading();
		setTimeout(function(){
			if(!valiAnswer()){
				ajaxEnding();
			  	return false;
			}
			if(!editCheck()){
				ajaxEnding();
				return false;
			}
			if(!_checkDataBatch()){
				ajaxEnding();
				return false;
			}
			var param = packArticleData(false);
			//$('.head_top_tj').attr('disabled',true);//启用该属性，禁用按钮
			$.post(pageContext.contextPath + '/app/biztpl/article!save.htm?op=save',param,function(data){
				var result = data.data;
				var code = result.code;
				if(code == '0'){
					alert('保存成功');
					//window.parent.ActionRegistry.execAction({name:'kb.reload-objcatetree',args:['4028cb81564565450156456794480003']});
					location.href = pageContext.contextPath + '/app/biztpl/article!index.htm?articleId=' + result.id;
				}else{
					//取消遮罩层
					ajaxEnding();
					alert(result.reason);
					//$('.head_top_tj').attr('disabled',false);//禁用该属性，启用按钮
				}
			}, 'json');
		},1)
	})
	//加载层
	function ajaxLoading(){
		var imageUrl = $.fn.getRootPath() + "/theme/red/resource/jquery/images/loading.gif";
		var maskLayer = $("<div id='maskLayer'><img src='"+imageUrl+"'><font>加载...</font></div>");
		maskLayer.css({
			"position" : "absolute",
			"zIndex" : "10000",
			"top" : $(document).scrollTop()+"px",
			"width" : $(window).width()+"px",
			"height" : $(window).height()+"px",
			"backgroundColor" : "lightgray",
			"opacity" : "0.6",
			"display" : "none",
			"padding-top" : $(window).height()/2+"px",
			"padding-left" : $(window).width()/2+"px",
			"filter" : "alpha(opacity=60)"
		});
		$("body").css("overflow","hidden");
		$("body").append(maskLayer);
		maskLayer.show();
	}
	//取消加载层
	function ajaxEnding(){
		$("body").css("overflow","");
		$("#maskLayer").remove();
	}
	
	function valiAnswer(){
		var flg = true;
		$('div[class="q_ask_show"]').find('[name="kbs-question"]').each(function(i,item){
			var ansDiv  = $(item).parents('div.q_ask_show').children('div.q_ans').find('div.wz_more');
			if(!ansDiv || ansDiv.length == 0){
				alert("[" + $(item).val() + "]的答案不存在");
				flg = false
				return false;
			}
		});
		return flg;
	}
		
	$('#fj_down').click(function(){
		var checkbox = $('table#list tr input[type="checkbox"]:checked').not('table#list tr input[name="all"]');
		if(checkbox.length == 1){
			var input  = checkbox[0];
			var fileId = $(input).parents('tr').attr('pid');
			var name = $(input).parent('div').text();
			location.href = pageContext.contextPath + '/app/wukong/search!download.htm?fileId='+fileId+'&fileName='+name;
		}else{
			alert('请勾选一个要操作的数据');
		}
	})
	$('#fj_look').click(function(){
		var checkbox = $('table#list tr input[type="checkbox"]:checked').not('table#list tr input[name="all"]');
		if(checkbox.length == 1){
			var input  = checkbox[0];
			var fileId = $(input).parents('tr').attr('pid');
			var name = $(input).parent('div').text();
			var url  = swf_address +  '/attachment/preview.do?fileName='+fileId + '.' + name.split('.')[1];
			window.open(url,"_blank");
		}else{
			alert('请勾选一个要操作的数据');
		}
	})
	
	
	$('#fj_del').click(function(){
		var checkbox = $('table#list tr input[type="checkbox"]:checked').not('table#list tr input[name="all"]');
			if(checkbox.length > 0){
				if (window.confirm('确定删除吗')){
					checkbox.each(function(i,item){
						var attId = $(item).attr('id');
						if($(item).parents('tr').attr('_name') == 'att'){
							$('table#list tr[id="'+attId+'"]').remove();
						}else{
							var topNodeId = '1';
							var oid = articleId;
							var msg = delAttament(topNodeId,oid,attId);
							if(msg){
								if(msg.result){
									var attachmentId = msg.attachmentId;
									$('table#list tr[id="'+attachmentId+'"]').remove();
									alert('解除成功');
								}else{
									alert('解除失败');
								}
							}
						}
					})
				}
			}else{
				alert('请选择一个附件进行删除！');
			}
	})
	var toolbar = 
					    [	  ['Maximize'],
						      ['Source','-','Save','NewPage','Preview','-','Templates','MyImage'],
						      ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker', 'Scayt'],
						      ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
						      ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select','Button', 'ImageButton', 'HiddenField'],
						       '/',
						      ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
						       ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
						       ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
						       ['Link','Unlink','Anchor'],
						      ['Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
						      '/',
						        ['Styles','Format','Font','FontSize','lineheight'],
						       ['TextColor','BGColor'],
						       ['Embed','Ansattr','zhiling']
						  ];
	function options(op){
		var options = {};
		var extraPlugins = 'MyImage,lineheight';
		if(op == '1'){
				extraPlugins = 'embed,ansattr,MyImage,lineheight,zhiling';
		}
		options.toolbar = toolbar;
		options.extraPlugins = extraPlugins;
		return options;
	}
	
	//左边树定位
	$("[class='sidebar-list']").on("click","span a",function(){
		var thisTop = $("ul [_id='"+$(this).attr("id")+"']").offset().top;
		var headHeight = $("[class='head_top']").outerHeight();
		$(window).scrollTop(0);
		$(window).scrollTop(thisTop-headHeight-10);
	});
	//点击更换模板
	$("#changeBiztpl").on("click",function(){
		if(confirm("请确保文章已保存，否则修改的数据将丢失")){
			var articleName = $("[name='articleName']").val();
			articleName = encodeURI(articleName);
			var _url = pageContext.contextPath + "/app/biztpl/article!changeBiztplPage.htm?articleName="+articleName;
			//打开编辑页面所在iframe
			$("#openChangeBiztplIframe")[0].src = _url;
			$("#openChangeBiztplDiv").dialog("open");
			_moveTop($("#openChangeBiztplDiv"),5);
		}
	});
	//修改模板
	$("#modifyBiztpl").on("click",function(){
		if(confirm("请确保文章已保存，否则修改的数据将丢失")){
			var articleId = $("[name='articleId']").val();
			var articleName = $("[name='articleName']").val();
			var tplId = $("[name='tplId']").val();
			var _url = pageContext.contextPath + "/app/biztpl/article!modifyBiztplPage.htm?articleId="+articleId+"&tplId="+tplId+"&articleName="+articleName;
			//打开编辑页面所在iframe
			$("#openModifyBiztplIframe")[0].src = _url;
			$("#openModifyBiztplDiv").dialog("open");
			_moveTop($("#openModifyBiztplDiv"),5);
		}
	});
	//点击标签按钮
	$("[name='label']").on("click",function(){
		var nodeIds = $(this).attr("nodeIds");
		var _url = pageContext.contextPath + "/app/biztpl/article!selectLabel.htm?nodeIds="+nodeIds;
		$("#openSelectLabelIframe")[0].src = _url;
		$("#openSelectLabelDiv").dialog("open");
		_moveTop($("#openSelectLabelDiv"),10);
	});
	
	//点击实例关联按钮
	$("[name='slgl']").on("click",function(){
		var _url = pageContext.contextPath + "/app/biztpl/example!example.htm?articleId="+articleId+"&tplId="+tplId;
		$("#openExampleIframe")[0].src = _url;
		$("#openExampleDiv").dialog("open");
		_moveTop($("#openSelectLabelDiv"),10);
	});
	//防止刷新页面之后，准确显示面板位置
	function _moveTop(_thisDiv,_height){
		$(_thisDiv).window("move",{top:$(window).scrollTop() + _height});
	}
	
	//点击标准问名称，显示答案
	$("[class='wait-pipei']").on("click","[name='qName']",function(){
		var unMatchAnswer = $(this).parent().find("div[name='unMatchAnswer']");
		if(unMatchAnswer.is(":hidden")){
			unMatchAnswer.show();
		}else{
			unMatchAnswer.hide();
		}
	});
	//全选
	$("[class='wait-pipei']").on("click","[name='checkAll']",function(){
		if($(this).is(":checked")){
			$("input[name='checkQues']").attr("checked",true);
		}else{
			$("input[name='checkQues']").attr("checked",false);
		}
	});
	//删除标准问
	$("[class='title-pipei']").on("click","[name='deleteQues']",function(){
		var checks = $("input[name='checkQues']:checked");
		if(checks.length > 0){
			if(confirm("确定删除吗?")){
				checks.each(function(i,item){
					$(this).parents("[name='unMatchQuestion']").remove();
				});
				alert("操作成功");
			}
		}else{
			alert("请选择数据");
		}
	});
	//打开移动到标题页面，记录标准问id
	$("[class='title-pipei']").on("click","[name='moveQues']",function(){
		var checks = $("input[name='checkQues']:checked");
		if(checks.length > 0){
			var qids = "";
			checks.each(function(i,item){
				if(qids == ""){
					qids = $(this).val();
				}else{
					qids += ","+$(this).val();
				}
				$("#moveQuestionId").val(qids);
			});
			$("#openMoveQuestionDiv").dialog("open");
			_moveTop($("#openMoveQuestionDiv"),100);
		}else{
			alert("请选择数据");
		}
	});
	//移动到标题页面，记录标题id
	$("[class='move']").on("click","[name='moveGroup']",function(){
		$("#moveGroupId").val($(this).attr("_id"));
		$("[name='moveGroup']").removeClass("title-checked");
		$(this).addClass("title-checked");
	});
	//确定将标准问移动到标题下
	$("#btnSure").on("click",function(){
		var groupId = $("#moveGroupId").val();
		if(groupId == ""){
			alert("请选择标题");
			return false;
		}
		if(confirm("确定移动到该标题下吗?")){
			var quesIds = $("#moveQuestionId").val();
			var quesIds = quesIds.split(",");
			for(var i=0;i<quesIds.length;i++){
				var questionId = quesIds[i];
				//新处理方式-保存工作留在保存文章时
				var div = $('div[name="qid_'+questionId+'"]').not('div[isKbase="false"]');
				if(div && div.length > 0){
					var _html = $('div[name="qid_'+questionId+'"]').not('div[isKbase="false"]').html();
					appendHtml(groupId,questionId,_html);
					$("[name='qid_"+questionId+"']").parent().remove();
				}
			}
			$("#openMoveQuestionDiv").dialog("close");
		}
	});
	
	//点击更多
	$(".content_ul").on("click",".ico-more",function(event){
		$("div.more2").hide();
		var more2 =  $(this).siblings('div.more2');
		if(!more2 || more2.length == 0){
			var info = '<div class="more2">	<div class="more_2"><div class="more_2_dsj"></div><ul class="more_2_ul">';
			if(showQa == "true"){
				info += '<li id="more_02"><a href="javascript:void(0)">类型</a></li>';
			}
			info += '<li id="more_04"><a href="javascript:void(0)">标签</a></li>	';
			info += '</ul></div></div>';
			$(this).parent(".q-ask-span").append($(info));
			var offset = $(event.target).offset();
			var _offset_top =  (-$('.more2').height()-12);
			var _offset_left = ($(event.target).offset().left-$(event.target).parent().offset().left-$('.more2').width()+35+$(event.target).width());
			$('.more2').hide().css({ top:_offset_top + "px", left: _offset_left +'px' });
			more2 = $(this).parent(".q-ask-span").find(".more2");
			$(more2).fadeIn(300);
		}else{
			$(more2).fadeIn(300);
		}
		event.stopPropagation();//防止事件冒泡
		$(".content_ul").click(function(e){
			$(".more2").hide();
		});
		$(".content_ul").on("click","#more_04",function(){
			$(".more2").hide();
			var _this = $(this).parents('span.q-ask-span').find("[name='tagIds']");
			var nodeIds = _this.val();
			var _this_q_id = _this.attr("_this_q_id");
			if(_this_q_id == ""){
				_this_q_id = new Date().getTime();
				_this.attr("_this_q_id",_this_q_id);
			}
			var _url = pageContext.contextPath + "/app/biztpl/article!selectLabel.htm?nodeIds="+nodeIds+"&_this_q_id="+_this_q_id;
			$("#openSelectLabelIframe")[0].src = _url;
			$("#openSelectLabelDiv").dialog("open");
			_moveTop($("#openSelectLabelDiv"),10);
		});
		$(".content_ul").on("click","#more_02",function(){
			$(".more2").hide();
			var _this = $(this).parents('span.q-ask-span').find("[name='tagIds']");
			var _this_q_id = _this.attr("_this_q_id");
			if(_this_q_id == ""){
				_this_q_id = new Date().getTime();
				_this.attr("_this_q_id",_this_q_id);
			}
			var articleName = $("[name='articleName']").val();
			var _url = pageContext.contextPath + "/app/biztpl/article!selectAttr.htm?_this_q_id="+_this_q_id+"&articleName="+articleName;
			$("#openSelectAttrIframe")[0].src = _url;
			$("#openSelectAttrDiv").dialog("open");
			_moveTop($("#openSelectAttrDiv"),10);
		});
	});
	//计算左边目录树高度并显示滚动条
	var sidebar = $('.sidebar-list');
	var offset_H = sidebar.height();
	var offset_T = 120;//sidebar.offset().top;
	var window_H = $(window).height();
	if ((offset_T+offset_H)>window_H){
		sidebar.css({'height':function(){
			return (window_H-offset_T)+'px';
		},'overflow-y':'scroll'});
	}
	
	var _keyWords = '${bizTplArticle.keyWords}';
	if(_keyWords == '' || _keyWords == null){
		$('input[name="keywords"]').css('color','gray');
	}

	//标准问拖拽 start add by wilson.li at 20161019
	//标题下标准问拖拽
	var $quesShowDiv;
	$(".q_as em.question").on("mousedown",function(event){
		$(this).css("cursor","move");
		$quesShowDiv = $(this).parent().parent();
		var offset_x = $quesShowDiv.offset().left;
		var offset_y = $quesShowDiv.offset().top;
		var mouse_x = event.pageX;
		var mouse_y = event.pageY;
		$(document).bind("mousemove", function(ev) {
			var _x = ev.pageX - mouse_x;
			var _y = ev.pageY - mouse_y;
			var now_x = (offset_x + _x) + "px";
			var now_y = (offset_y + _y) + "px";
			var thisWidth = $quesShowDiv.width();
			$quesShowDiv.css({
				top : now_y,
				left : now_x,
				'z-index' : "10",
				position : "absolute",
				width : thisWidth
			});
		});
	});
	$(".q_ask_show .q_as em.question").on("mouseup",function(event){
		var mouse_x = event.pageX;//鼠标x坐标
		var mouse_y = event.pageY;//鼠标y坐标
		var $quesAsk;//移动目标的标题区域
		
		$(".q_ask").each(function(i,item){
			var li_min_x = $(this).offset().left;
			var li_max_x = $(this).offset().left + $(this).outerWidth();
			var li_min_y = $(this).offset().top;
			var li_max_y = $(this).offset().top + $(this).outerHeight();
			if(mouse_x > (li_min_x-50) && mouse_x < li_max_x && mouse_y > li_min_y && mouse_y < li_max_y){
				$quesAsk = $(this);
			}
		});
		if($quesAsk){
			var $quesLevel = $quesAsk.prev();//目标标题
			var groupId = $quesLevel.attr("groupId");//标题id
			var questionId = $quesShowDiv.find('input[name="kbs-question"]').attr("id");//标准问id
			var attrId = $quesShowDiv.find("[name='kbs-question']").attr("attrid");//标准问所在属性id
			
			var moveToQues = false;
			var quesArr = [];//移动目标区域的所有标准问区域、过滤移动标准问
			$quesAsk.find(".q_ask_show").each(function(i,item){
				if($(this).css("position")=="absolute"){
				}else{
					quesArr.push(this);
				}
			});
			//移动到目标标准问的上或者下
			$(quesArr).each(function(){
				var li_min_x = $(this).offset().left;
				var li_max_x = $(this).offset().left + $(this).outerWidth();
				var li_min_y = $(this).offset().top;
				var li_max_y = $(this).offset().top + $(this).outerHeight();
				//li_min_x+50是考虑到“问”图标所在位置的误差
				if(mouse_x > (li_min_x-50) && mouse_x < li_max_x && mouse_y > li_min_y && mouse_y < li_max_y){
					//如果标准问带有属性，则更新属性的parentId
					if(attrId != undefined && attrId != ""){
						$(".content li[attrId='"+attrId+"']").attr("parentId",groupId);
					}
					//移动标准问
					if(questionId != $(this).find('input[name="kbs-question"]').attr("id")){
						if($(this).outerHeight()/2 > mouse_y-$(this).offset().top){
							$(this).before($quesShowDiv);
						}else{
							$(this).after($quesShowDiv);
						}
					}
					moveToQues = true;
					return false;
				}
			});
			if(!moveToQues){
				if(attrId != undefined && attrId != ""){
					$(".content li[attrId='"+attrId+"']").attr("parentId",groupId);
				}
				$quesAsk.find("div[class='q_ask_show2']").after($quesShowDiv);
			}
		}
		//还原定位设置
		$quesShowDiv.css({
			top : 0,
			left : 0,
			position : ""
		});
		$(this).css("cursor","default");
		$(document).unbind("mousemove");
	});
	//标题下标准问拖拽 end add by wilson.li at 20161019
});

//封装文章数据
function packArticleData(filter){
	var name = $('input[name="articleName"]').val();
	var semantic = $('input[name="semantic"]').val();
	var abstract = $('div.about').children('div.q_ask_show2').children('div.q_js').children('div.q_js_con').html();
	var keywords = $('input[name="keywords"]').val();
	if(keywords == '多个关键字请用逗号隔开'){
		keywords = "";
	}
	keywords = keywords.replace(/[,|，]/g,',');
	var article = {
					id: articleId,
					tplId: $('input[name="tplId"]').val(),
					name: name,
					label: $('input[name="label"]').attr('nodeIds'),
					traceDate: $('input[name="traceDate"]').val(),
					startDate: $('input[name="startDate"]').val(),
					endDate: $('input[name="endDate"]').val(),
					categoryName: $('input[name="category"]').val(),
					categoryId: $('input[name="categoryId"]').val(),
					keywords : keywords,
					semantic: semantic,
					abstract:abstract,
					attids:'',
					attNames:'',
					username:username
				}
		//标准问题
		var quesArr = [];
		var $quesArr = $('li.q_ask div.q_ask_show');
		$quesArr.each(function(i, item){
			var display = $(item).css('display');
			if(filter && display == 'none'){
				return true;
			}
			var q = $(item).children('div.q_as').find('input[type="text"]');
			var iskbase_q = $(q).attr('iskbase');
			var question = q.val();
			var answerArr = [];
			$(item).children('div.q_ans').find('div.wz_more').each(function(i, item1){
				var iskbase = $(item1).attr('iskbase');
				if(filter && iskbase == 'false'){
					return true;
				}
				var answer = $(item1).children('div.wz_more_c').html();
				answer = answer.replace(/<!--.*?-->/g,'');
				//附件
				var att_a = $(item1).children('div.wz_down').find('ul li[id="attrachment"] .wz_r').find('a.wz_fj[_type="newUpload"]');
				var attments = [];
				if(att_a && att_a.length > 0 ){
					att_a.each(function(i, a){
						attments.push({
							attId:$(a).attr('id'),
							attName:$(a).text(),
							attType:$(a).attr('attType') ? $(a).attr('attType') : '0',
							remark:$(a).attr('remark'),
							fileId:$(a).attr('fileId'),
							op:$(a).attr('op')
						});
					});
				}
				var startDate = $(item1).children('div.wz_down').find('input[_name="startDate"]').val();
				var endDate = $(item1).children('div.wz_down').find('input[_name="endDate"]').val();
				var traceDate = $(item1).children('div.wz_down').find('input[_name="traceDate"]').val();
				var dimtags = $(item1).attr('dimid');
				var dimsnames = $(item1).attr('dimname');
				if(dimtags.length == 0){
					dimsnames = '';
				}
				var cmds = $(item1).children('div.wz_down').find('li[id="cmds"]').find('div.wz_r').children('em').text();
				answerArr.push({
					id: $(item1).attr('id'),
					content: answer,
					attids: '',
					attNames:'',
					dimtags: dimtags,
					dimsnames: dimsnames,
					attments : attments,
					startDate:startDate,
					endDate:endDate,
					traceDate:traceDate,
					cmds:cmds
				})
			})
			quesArr.push({
				id: $(q).attr('id'),
				attrid: $(q).attr('attrid'),
				attrname: $(q).attr('attrname'),
				tagIds: $(q).parent().find('[name="tagIds"]').val(),
				groupid: $(q).parents('li.q_ask').prev('li.q_ask').attr('groupId'),
				name: question,
				answers: answerArr
				
			})
			
		});
	
	//待匹配标准问信息封装 add by wilson.li at 2016-07-25
	var questions = [];
	$("[name='unMatchQuestion']").each(function(i,item){
		var answers = [];
		$(item).find("[name='unMatchAnswer']").each(function(i,aItem){
			var aId = $(aItem).find("[name='unMatchAnswerId']").val();
			var aContent = $(aItem).find("[name='unMatchAnswerContent']").html();
			var aDimIds = $(aItem).find("[name='answerDimIds']").val();
			var aDimNames = $(aItem).find("[name='answerDimNames']").val();
			var aAttId = $(aItem).find("[name='answerAttIds']").val();
			var aAttNames = $(aItem).find("[name='answerAttNames']").val();
			var cmds = $(aItem).find("[name='answerCmds']").val();
			answers.push({
				id: aId,
				content: aContent,
				attids: aAttId,
				attNames:aAttNames,
				attments : [],
				dimtags: aDimIds,
				dimsnames: aDimNames,
				cmds:cmds
			});
		});
		var qId = $(item).find("[name='unMatchQuestionId']").val();
		var qName = $(item).find("[name='unMatchQuestionName']").val();
		var qAttrId = $(item).find("[name='unMatchQuestionAttrId']").val();
		var qAttrName = $(item).find("[name='unMatchQuestionAttrName']").val();
		var qGroupId = $(item).find("[name='unMatchQuestionGroupId']").val();
		quesArr.push({
			id: qId,
			attrid: qAttrId,
			attrname: qAttrName,
			tagIds:"",
			groupid: qGroupId,
			name: qName,
			answers: answers
		})
	});
	$.extend(article, {
		questions: quesArr,
		attments:att_article()
	})
	var group = $('li[parentId=""]');
	var groupList = getGroup(group);
	var property  = $('#textarea').text();
	var param = {
		data: JSON.stringify(article),
		groupData:JSON.stringify(groupList),
		property:property,
		relatedObjects:JSON.stringify(relatedObjects),
		objectId:articleId
	};
	return param;
}

//获取业务模版的结构
function getGroup(group){
	var  _json = [];
	group.each(function(i,item){
		var json = {'id':'','name':'','attrId':'','parentId':'','delflag':'','groupId':'','priority':'','tplId':'','level':'','abs':'','children':''};
		json.id = $(item).attr('id');
		json.groupId = $(item).attr('groupId');
		json.name = $(item).attr('name');
		json.attrId = $(item).attr('attrId');
		json.parentId = $(item).attr('parentId');
		json.priority = $(item).attr('priority');
		json.tplId = $('input[name="tplId"]').val();;
		json.level = $(item).attr('level');
		json.delflag = $(item).attr('delflag');
	    var	chil = []; 
		if(!$(item).attr('attrId')){//标题
			var js = $(item).next('li.q_ask').children('div.q_ask_show2').children('div.q_js').children('div.q_js_con');
			if(js && js.length > 0){
				var abs = $(item).next('li.q_ask').children('div.q_ask_show2').children('div.q_js').children('div.q_js_con').html();
				abs = abs.replace(/<!--.*?-->/g,'');
				json.abs = abs;
			}
			
			chil = getGroup($('li[parentId="'+$(item).attr('groupId')+'"]'));
		}
		json.children = chil;
		_json.push(json);
	});
	return _json;
}

//获取文章附件
function att_article(){
	var tr  = $('table#list tr[_type="new"]');
	var articleAtt = [];
	tr.each(function(i,item){
		var attId = $(item).children('td').find('input[type="checkbox"]').attr('id');
		var attName = $(item).children('td').children('div').text();
		
		var td = $(item).children('td').eq(5)
		var remark = td.text();
		var attType = $(item).attr('type');
		var fileId =  $(item).children('td').find('input[type="checkbox"]').attr('fileId');
		var op = $(item).children('td').find('input[type="checkbox"]').attr('op');
		articleAtt.push({
			attId:attId,
			attName:attName,
			attType:attType,
			remark:remark,
			fileId:fileId,
			op:op
		});
	})
	return articleAtt;
}
function changeDate(dq,obj){
	var ans  = $('div.q_ask_show').find('div.wz_more');
	if(ans && ans.length > 0){
		if (window.confirm('是否更新答案中的时间？')){
			var newDate = dq.cal.getNewDateStr();
			var valid  = $('div.wz_more div.wz_down').children('ul.wz').find('li#validDate');
			var trace = $('div.wz_more div.wz_down').children('ul.wz').find('li#traceDate');
			var name = $(obj).attr('name');
			 $('div.wz_more div.wz_down').children('ul.wz').children('li').find('input[_name="'+name+'"]').val(newDate);
		}
	}
}
function checkDim(dimArray,dim2){
	dim2 = dim2.split(',').sort().join(',');
	for(var i in dimArray) {
		if(dim2 == dimArray[i]){
			alert('维度组合重复');
			return false;
		}
	}
	return true;
}
function delAttament(topNodeId,oid,attId){
		var msg;
		$.ajax({
				type: "POST",
				url: pageContext.contextPath + '/app/biztpl/article!relieveRelation.htm',
				data: {attachmentId:attId,topNodeId:topNodeId,objectIds:oid},
				dataType:'json',
				async: false,
				success: function(result){
					msg =  result;
				},
				error :function(){
						 alert('解除失败');
				}
		});
		return msg;
}

//关闭更换模板页面
function closeIframe(){
	$("#openChangeBiztplDiv").dialog("close");
}
//关闭修改模板页面
function closeModifyIframe(){
	$("#openModifyBiztplDiv").dialog("close");
}
//显示滚动条
function showOverflow(){
	$("body").css("overflow","auto");
}
//隐藏滚动条
function hideOverflow(){
	$("body").css("overflow","hidden");
}
function closeProIframe(){
	$('.property').hide();
	$('#mask').css('display','none');
	$('body').css('overflow','auto');
}

//打开p4
function openP4(p4id,urlid){
	var _url = pageContext.contextPath + '/app/biztpl/article-instruction!p4editor.htm?p4id='+ p4id + '&urlid='+urlid;
	$("#openp4Iframe")[0].src = _url;
	$("#openp4Div").dialog("open");
	$("#openp4Div").parent().css('z-index',10000);
	$("#openp4Div").window("move",{top:$(window).scrollTop() + 10});
}
//设置p4id
function setP4Id(p4Id,urlid){
	document.getElementById('openZhiLingIframe').contentWindow.setP4Id(p4Id,urlid);
}
//设置指令
function setCmds(editid,cmds){
	$('textarea[id="'+editid+'"]').siblings('div.wd_bj').find('ul.wz').find('li#cmds').find('em').text(cmds);
}
//关闭指令
function closeZhiling(){
	$("#editzhiling").dialog("close");
}
//关闭p4
function closeP4(){
	$("#openp4Div").dialog("close");
}

//预览
function articlePreview(){
	$("[name='articleInfo']").val(JSON.stringify(packArticleData(true)));
	$("#iframeForm").submit();
	//下面是对应的打开dialog方式
	/* $("#articlePreviewDiv").dialog("open");
	$("#articlePreviewDiv").window("move",{top:$(window).scrollTop()}); */
}
//关闭选择本体页面
function closeSelectAttrIframe(){
	$("#openSelectAttrDiv").dialog("close");
}
//填充html内容
function appendHtml(groupId,questionId,_html){
	$("li[groupId='"+groupId+"']").next("li[class='q_ask']").find("div[class='q_ask_show2']").after(_html);
	//移动带有本体属性的待匹配问题
	var _input = $("[name='qid_"+questionId+"']").children('div.q_ask_show').children('div.q_as').find('input[name="kbs-question"]');
	var _attrid = $(_input).attr('attrid');
	var _attrname = $(_input).attr('attrname');
	if(_attrid && _attrname){
		var parentLi = $("li[groupId='"+groupId+"']");
		var li = $('<li id="" name="" attrId="" parentId="" groupId="" priority="" tplId=""	level="" delFlag=""	 style="display:none;"></li>');
		$(li).attr('id','');
		$(li).attr('attrId',_attrid);
		$(li).attr('name',_attrname);
		$(li).attr('groupId','');
		$(li).attr('parentId',groupId);
		$(li).attr('priority',Number($(parentLi).attr('priority'))+1);
		$(li).attr('tplId',$("[name='tplId']").val());
		$(li).attr('level','99');
		$(li).attr('delFlag','0');
		$('body').find('div.content').append(li);
	}
}
