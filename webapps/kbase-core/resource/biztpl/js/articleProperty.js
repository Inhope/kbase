$(function () {
	var PropertyType;
	if(typeof PropertyType == "undefined"){
　　　　var PropertyType = {
					/**单行文本**/
					SINGLE_ROW_TEXT:'single_row_text',
					/**日期**/
					DATE_TEXT:'date_text',
					/**复选列表**/
					MULTIPLE_CHOICES:'multiple_choices',
					/**下拉列表**/
					SELECT_CHOICES:'select_choices'
　　　　 }
	};
	$('i.add').click(function(){
					$('i.add').not(this).removeClass('remove_click');
					var index = $('i.add').index(this);
					$('.one_level').not($('.one_level').eq($('i.add').index(this))).css('display','none');
					$(this).toggleClass('remove_click');
					$('.one_level').eq($('i.add').index(this)).toggle();
				})
				$(".select_checkBox").hover(function(){ 
						$(this).children('div.check_xl').css("display","block");
					},function(){
						$(".check_xl").css("display","none"); 
		
	}); 
	$('button[id=cancel]').click(function(){
			closeProIframe();
	})
	$('button[id="ok"]').click(function(){
			var _property = [];
			var li = $('ul[id=allProperty]>li');
			li.each(function(i, item0){
				var type=$(item0).attr('type');
				
				if(type =='single_row_text'|| type=='date_text'){
					var property = {'id':'','tagId':'','tagVal':''};
					var val = $(item0).find('input');
					if(val.val()){
						property.id = $(item0).attr('_id');
						property.tagId = $(item0).attr('id');
						property.tagVal = val.val();
						_property.push(property);
					}
				}else{
						var p = $(item0).find('div[class=one_level]');
						p.each(function(i, item1){
							//	var tag = [];
							if(type=='multiple_choices'){
								var checkbox = 	$(item1).children('div').children('input[type=checkbox]');	//属性值列表
								var tagId;
								checkbox.each(function(i, cb){
								if($(cb).is(':checked')){//选中的属性
									tagId = $(cb).attr('id');//选中的属性值id
										if(tagId){
											var pro = {'id':'','tagId':'','tagVal':''};
											pro.tagId = tagId;
											pro.id = $(cb).attr('_id');
											//var select  =  $(cb).siblings('span.one_level_r').find('span.one_level_r select');//属性的关联数据
											//var box = $(cb).siblings('span.one_level_r').find('span.one_level_r span.select_checkBox');
											var select  =  $(cb).siblings('span.one_level_r').find('select');//属性的关联数据
											var box = $(cb).siblings('span.one_level_r').find('input[type=checkbox]');
											var cid;
											if($(select) && $(select).length > 0){
													cid = $(select).val(); //关联数据的id
													var c = {'id':'','tagId':'','tagVal':''};
													c.tagId = cid;
													c.id=$(select).find('option:selected').attr('_id');
													_property.push(c);
											}else if($(box) && $(box).length > 0){
													var c = {'id':'','tagId':'','tagVal':''};
													var bs = $(box).find('input[type=checkbox]');
													box.each(function(i, b){
														if($(b).is(':checked')){
															c.id=$(b).attr('_id');
															c.tagId = $(b).attr('id');
															_property.push(c);
														}
													})
											}
											
										}
										_property.push(pro);
								}
								})
							}else if(type=='select_choices'){
								var s = {'id':'','tagId':'','tagVal':''};
								var select  = $(item1).children('select');
								if(select && select.length > 0){
									s.tagId = $(select).val();
									s.id = $(select).find('option:selected').attr('_id');
									_property.push(s);
								}
							}
								
						})
				}
			})
			$('#textarea').text(JSON.stringify(_property));
			$("#textarea").text(JSON.stringify(_property));
			closeProIframe();
	})
	
	
	
})