$(function(){
	var reg = /(^\s*)|(\s*$)/g;//为空正则
	var _now_panel_id = "";
	
	if(defaultDataType != "" && defaultDataType != "single_row_text" && defaultDataType != "date_text"){
		$("#prop_tag").panel("open");
	}
	
	//属性类型下拉框
	$("#dataType").on("change",function(){
		var dataType = $(this).val();
		if(dataType == "single_row_text" || dataType == "date_text"){
			$("#prop_tag").panel("close");
		}else{
			$("#prop_tag").panel("open");
		}
	});
	
	//保存关联数据
	function saveRelateData(){
		var childNames = {};
		$("#valList_child [name='childName']").each(function(i,item){
			var _id = $(item).attr("id")==undefined?("tempId"+i):$(item).attr("id");
			childNames[_id] = $(item).val();
		});
		childNames = JSON.stringify(childNames);
		$("#"+_now_panel_id).find("[name='prop_tag_name']").attr("childNames",childNames);
		$("#"+_now_panel_id).find("[name='prop_tag_name']").attr("dataTypeChild",$("#data_type_child").val());
	}
	
	//新增关联数据
	function addRelateData(){
		var templet = $("#relateTemplet tbody");
		$("#valList_child").append($(templet).html());
	}
	
	//数据校验
	function checkData(){
		var checkResult = true;
		var name = $("#name").val();
		name = name.replace(reg,"");
		if(name == ""){
			alert("属性名称不能为空");
			checkResult = false;
			return;
		}
		if($("#id").val() == ""){
			//判断属性是否存在
			$.ajax({
				type: "POST",
				async: false,
				url: $.fn.getRootPath()+"/app/biztpl/biz-tpl-property!findByName.htm",
				data: "name="+name,
				dataType:"json",
				success: function(data) {
					if(data.status == 1){
						checkResult = false;
						alert("属性已存在，不能重名");
					}
				}
			});
		}
		var dataType = $("#dataType").val();//属性类型
		if(dataType == "single_row_text" || dataType == "date_text"){//单行文本和日期不考虑关联数据
			
		}else{
			$("#valList").find("[name='prop_tag_name']").each(function(i,item){
				var childNames = $(this).attr("childNames");
				var childVal = $(this).val();
				childVal = childVal.replace(reg,"");
				if(childVal == ""){
					alert("属性值不能为空");
					checkResult = false;
					return;
				}else{
					var json = JSON.parse(childNames);
					for(var key in json){
						var val = json[key];
						val = val.replace(reg,"");
						if(val == ""){
							alert("关联数据不能为空");
							checkResult = false;
							return;
						}
					}
				}
			});
		}
		return checkResult;
	}
	
	//打开关联数据面板
	$("#valList").on("click","[name='btnRelateData']",function(){
		//保存关联数据
		saveRelateData();
		//重置为新的关联数据模板
		if($(this).parents("tr").attr("id") != _now_panel_id){
			$("#valList_child tr").remove();
			//设置面板对应id
			_now_panel_id = $(this).parents("tr").attr("id");
			//填充模板
			var childNames = $("#"+_now_panel_id).find("[name='prop_tag_name']").attr("childNames");//关联数据
			var json = JSON.parse(childNames);
			for(var key in json){
				addRelateData();
				$("#valList_child").find("tr:last td:last input").val(json[key]);
				$("#valList_child").find("tr:last td:last input").attr("id",key);
			}
			$("#data_type_child").val($("#"+_now_panel_id).find("[name='prop_tag_name']").attr("dataTypeChild"));//下拉选项
			
		}
		$("#prop_tag_child").panel("open");
		//关联数据面板移动、样式变化
		var trTop = $("#prop_tag_child").parents("tr").offset().top;
		var divTop = $(this).offset().top;
		$("#prop_tag_child").parents("td").css("padding-top",divTop - trTop + "px");
		$("[name='btnRelateData']").removeClass("link-btn-select");
		$(this).addClass("link-btn-select");
	});
	
	//关联数据-点击保存
	$("#prop_tag_child").on("click","#btnSaveTag_child",function(){
		saveRelateData();
	});
	
	//新增属性值
	$("#prop_tag").on("click","#btnAddTag",function(){
		var templet = $("#propTagTemplet tbody");
		$(templet).find("tr").attr("id",new Date().getTime())
		$("#valList").append($(templet).html());
	});
	//删除属性值
	$("#prop_tag").on("click","[name='btnDelTag']",function(){
		var _this = this;
		var _id = $(_this).parents("tr").find("[name='prop_tag_name']").attr("id");
		_id = _id==undefined?"":_id;
		if(confirm("确定要删除选中的数据吗？")){
			$.ajax({
				type: "POST",
				url: $.fn.getRootPath()+"/app/biztpl/biz-tpl-property!delProperty.htm",
				data: "id="+_id,
				dataType:"json",
				success: function(data) {
					if(data.status == 0){
						$(_this).parents("tr[name='row_tr']").remove();
						if($(_this).parents("tr").attr("id") == _now_panel_id){
							$("#prop_tag_child").panel("close");
						}
					}else{
						alert("操作失败");
					}
				}
			});
		}
	});
	//关联数据-点击新增
	$("#prop_tag_child").on("click","#btnAddTag_child",function(){
		addRelateData();
	});
	//删除关联数据
	$("#prop_tag_child").on("click","#btnDelTag_child",function(){
		var ids = [];
		var objects = [];
		$("#valList_child").find("input:checkbox:checked").each(function(i,item){
			var _thisObj = $(this).parents("tr[name='row_relate']");
			var id = _thisObj.find("[name='childName']").attr("id");
			ids.push(id == undefined ? ("tempId"+i) : id);
			objects.push(_thisObj);
		});
		if(objects.length > 0){
			if(confirm("确定要删除选中的数据吗？")){
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/biztpl/biz-tpl-property!delPropertyTag.htm",
					data: "ids="+ids,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){
							$(objects).each(function(){
								$(this).remove();
							});
						}else{
							alert("操作失败");
						}
					}
				});
			}
		}else{
			alert("请选择数据");
		}
	});
	
	//保存
	$("#saveDo").on("click",function(){
		saveRelateData();
		//所有数据校验
		if(checkData()){
			var dataType = $("#dataType").val();//属性类型
			var childNames = {};
			if(dataType == "single_row_text" || dataType == "date_text"){//单行文本和日期不考虑关联数据
				
			}else{
				$("#valList").find("[name='prop_tag_name']").each(function(i,item){
					var child = {};
					child["id"] = $(this).attr("id") == undefined ? ("tempId"+i) : $(this).attr("id");
					child["name"] = $(this).val();
					child["dataTypeChild"] = $(this).attr("dataTypeChild");
					child["childNames"] = $(this).attr("childNames");
					childNames[i] = child;
				});
			}
			childNames = JSON.stringify(childNames);
			var name = $("#name").val();
			name = name.replace(reg,"");
			var param = {
				id: $("#id").val(),
				name: name,
				status: $("#status").val(),
				isSearch: $("#isSearch").val(),
				required: $("#required").val(),
				dataType: dataType,
				childNames: childNames
			};
			$.ajax({
				type: "POST",
				url: $.fn.getRootPath()+"/app/biztpl/biz-tpl-property!save.htm",
				data: param,
				dataType:"json",
				success: function(data) {
					if(data.status == 0){
						alert("操作成功");
						window.parent.closeIframe();
					}else{
						alert("操作失败");
					}
				}
			});
		}
	});
	
});