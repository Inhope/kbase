/**
 * 文章展示
 * @author Gassol.Bi
 * @date Jun 20, 2016 9:52:26 AM
 *
 */
 $(function(){
	var that = window.ArticleList = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*公共方法*/
	fn.extend({
		/*获取当前选中分类*/
		getCategory: function(){
			var node = $('#cate-article').tree('getSelected');
			if(node){
				var inBh = node.name, i = 10,
				parent = $('#cate-article').tree('getParent', node.target);
				while(parent && i > 0){
					inBh =  parent.name + '/' + inBh;
					parent = $('#cate-article').tree('getParent', parent.target);
					i--;
				}
				node.inBh = inBh;/*中文bh*/
			}
			return node;
		},
		/*获取当前选中文章*/
		getArticleId: function(){
			var rows = $('#now-tab').datagrid('getChecked');
			if(rows.length > 1) 
				$.messager.alert('警告','不允许同时操作多行数据!');
			else if (rows.length == 1)  
				return rows[0].id;
			else {
				$.messager.alert('警告','请选择需要操作的行!');
			}
			return false;
		},
		/*刷新列表*/
		reloadList: function(){
			var queryParams = $('#now-tab').datagrid('options').queryParams;
			queryParams.name = $('#name').textbox('getText');
			var node = fn.getCategory();/*当前选中的分类节点*/
			if(node) {
				queryParams.categoryId = node.id;
				queryParams.categoryBh = node.bh;
			} else {
				queryParams.categoryId = null;
				queryParams.categoryBh = null;
			}
	        $('#now-tab').datagrid('reload');
		},
		/*jquery-easyui tree 数据处理*/
		treeData: function(data){
			var idFiled = 'id', textFiled = 'text', parentField = 'pId';
			var i, l, treeData = [], tmpMap = [];
			for (i = 0, l = data.length; i < l; i++) {
				tmpMap[data[i][idFiled]] = data[i];
			}
			for (i = 0, l = data.length; i < l; i++) {
				if (tmpMap[data[i][parentField]] && data[i][idFiled] != data[i][parentField]) {
					if (!tmpMap[data[i][parentField]]['children'])
						tmpMap[data[i][parentField]]['children'] = [];
					data[i]['text'] = data[i][textFiled];
					tmpMap[data[i][parentField]]['children'].push(data[i]);
				} else {
					data[i]['text'] = data[i][textFiled];
					treeData.push(data[i]);
				}
			}
			return treeData;
		}
	});
	/*功能方法*/
	that.extend({
		init: function(){
			/*查询*/
			$('#now-search').linkbutton({
			    onClick: function(){
			    	fn.reloadList();
			    }
			});
			/*重置*/
			$('#now-clear').linkbutton({
			    onClick: function(){
					$('#name').textbox('setText', '');
					$('#cate-article').tree('unselect');
			    }
			});
			/*查看文章*/
			$('#btn-search').linkbutton({
			    onClick: function(){
			    	var articleId = fn.getArticleId();
			    	if(articleId){
			    		var url = '/app/biztpl/article-search.htm?';
							url += '&articleId=' + articleId;
						window.open($.fn.getRootPath() + url, articleId);
			    	}
			    }
			});
			/*新增文章*/
			$('#btn-add').linkbutton({
			    onClick: function(){
			    	var node = fn.getCategory();/*当前选中的分类节点*/
			    	var param = '';
			    	if(node == null){
			    		$.messager.alert('警告', '请选择文章分类');
			    		return false;
			    	}else{
			    		param = "categoryId="+node.id+"&categoryName="+node.inBh;
			    	}
			    	var content = '<iframe id="dd-article-add-iframe" name="dd-article-add-iframe" frameborder="0" ' 
			    		+ 'src="url" style="width:99.9%;height:98%;"></iframe>';
			    	content = content.replace(/url/g, $.fn.getRootPath() + '/app/biztpl/article!pick.htm?'+param);/*附件上传地址*/
			    	$('#dd_add-article').dialog({
					    title: '模板选择',
					    width: $(window).width()*0.5,
					    height: $(window).height()*0.9,
					    closed: false,    
					    cache: false,
					    content: content,
					    modal: true/*,
					    buttons:[{
							text:'确定',
							iconCls:'icon-ok',
							handler:function(){
								var btnOk = window.frames['dd-article-add-iframe']
									.document.getElementById('btnOk');
								$(btnOk).click();
							}
						},{
							text:'关闭',
							iconCls:'icon-no',
							handler:function(){
								$('#dd_add-article').dialog('close');
							}
						}]*/
					});    
					$('#dd_add-article').dialog('open');
			    }
			});
			/*编辑文章*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var articleId = fn.getArticleId();
			    	if(articleId){
			    		window.open($.fn.getRootPath() 
			    			+ '/app/biztpl/article!index.htm?articleId=' + articleId);
			    	}
			    }
			});
			/*删除文章*/
			$('#btn-remove').linkbutton({
			    onClick: function(){
			    	$.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
				            if (data) {
					                var articleId = fn.getArticleId();
							    	if(articleId){
							    		var rows = $('#now-tab').datagrid('getChecked'), 
							    			name = rows[0].name;
							    		if(!name) name = '未知';
							    		$.post($.fn.getRootPath() + '/app/biztpl/article!delete.htm', 
								    		{id: articleId, name:encodeURI(name)}, 
								    		function(data){
												var result = data.data;
												var code = result.rscode;
												if(code == '0'){
													$.messager.alert('警告', 
														'文章:<font color="red">' + decodeURI(result.name,"utf-8") + '</font>删除成功');
													fn.reloadList();
												} else {
													$.messager.alert('警告', 
														'文章:<font color="red">' + decodeURI(result.name,"utf-8") + '</font>删除失败');
												}
										}, 'json');
							    	}
				            }
			        });
			    }
			});
		}
	});
	/*初始化*/
	that.init();
});