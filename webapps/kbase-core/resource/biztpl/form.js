$(function() {
	
	$('td[name="articleName"]').click(function(){
		var tplId= $(this).parent('tr').attr('tplId');
		var name = $(this).text();
		var id = $(this).parent('tr').attr('id');
		var lastParams = '{"username":"'+$('input[name="username"]').val()+'","articleId":"'+id+'"}';
		var url = pageContext.contextPath + '/app/biztpl/article!index.htm?lastParams='+lastParams;
		if (tplId == null || tplId == undefined || tplId == '') {
			if (!window.confirm('本实例未加载业务模板，是否需要加载模板？')){
				return false;
			}
		}
		window.open(url);
	})
	
	$('body').on('click','button#reset',function(){
		$('input[name="name"]').val('');
		$('input[id="startDate"]').val('');
		$('input[id="endDate"]').val('');
		$('input[name="label"]').val('');
		$('input[name="traceDate"]').val('');
		$('input[name="traceDate"]').val('');
		$('input[name="semantic"]').val('');
		return false;
	})
	
	fromObject = {
		form : $('form'),
		submitBtn : $('button[type=submit]'),
		submitEvent : function() {
			var self = this;
			this.submitBtn.click(function(e, start, limit, filed, order,appendContent) {
					var action = self.form.attr('action');
					$('form input[name=start]').val(start);
					$('form input[name=limit]').val(limit);
					if(start && limit) {
						filed = filed ? filed : '';
						order = order ? order : '';
						appendContent = appendContent ? appendContent : '';
						self.form.attr('action', action);
					}
						
					window.setTimeout(function(){
						self.form.submit();
					}, 1);
			});
		},
		init : function() {
			this.submitEvent();
		}
	}
	
	fromObject.init();
	
	$('ul.wz_fy li a').click(function() {
		var start = $(this).attr('start');
		var limit = $(this).attr('limit');
		if(start && limit) {
			fromObject.submitBtn.trigger('click', [start, limit]);
		}
	});
	
	$('td input[type="checkbox"]').click(function(){
			$('td input[type=checkbox]').not(this).attr('checked',false);
	});
	$('button#relieve').click(function(){
		var input = $('td input[type=checkbox]:checked');
		if(input && input.length > 0){
			var tplId = $(input).parents('tr').attr('tplId');
			if (tplId == null || tplId == undefined || tplId == '') {
				alert('该实例没有套用文章不可解除');
			} else{
				if (window.confirm('确定解除文章吗？')){
					if (window.confirm(' 解除后数据将不可恢复，请确认')){
						var id = $(input).parents('tr').attr('id');
						var articleName =  $(input).siblings('td[name="articleName"]').text();
						$.post(pageContext.contextPath + '/app/biztpl/article!delete.htm', {id: id,name:encodeURI(articleName)}, function(data){
									var result = data.data;
									var code = result.rscode;
									if(code == '0'){
										alert('文章:' + decodeURI(result.name,"utf-8") + ' 解除除成功');
										$('form').submit();//刷新数据
									}else{
										alert('文章:' + decodeURI(result.name,"utf-8") + ' 解除失败');
									}
							}, 'json');
					}
				}
			}
		}else{
			alert('请选择一条数据操作');
		}
	})
	
	
	
	$.fn.tab=function (options) {
		var defaults={
			_selector : '.wz_title',
			_a : 'a',
			_table : 'table',
			_css : {
				'color':'#CC0000',
				'outline':'none'
			},
			_cssNone : {
				'color':'',
				'border':'none'
			}
		};
		var options=$.extend(defaults,options);
		$(defaults._selector).find(defaults._a).eq(searchType).css(options._css).siblings(options._a).css(options._cssNone);
		var _this = $(options._selector);
		return this.each(function(){
			_this.parent().find(options._table).hide().first(options._table).show();					
			_this.children(options._a).not(options._not).click(function(){
				var _index = $(this).index();
				$(this).css(options._css).siblings(options._a).css(options._cssNone);
				_this.parent().find(options._table).eq(_index).show().siblings(options._table).hide();
				$('form').find('input[name="searchType"]').val(_index);
				$('form').submit();
			})
		});
	};
});