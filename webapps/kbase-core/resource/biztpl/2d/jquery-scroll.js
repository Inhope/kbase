(function(a) {
	a.tiny = a.tiny || {};
	a.tiny.carousel = {
		options: {
			start: 1,
			display: 1,
			axis: "x",
			controls: true,
			pager: false,
			interval: false,
			intervaltime: 3000,
			rewind: false,
			animation: true,
			duration: 1000,
			callback: null
		}
	};

	function b(q, e) {
		var i = this,
			h = a(".viewport:first", q),
			g = a(".overview:first", q),
			k = g.children(),
			f = a(".next:first", q),
			d = a(".prev:first", q),
			l = a(".pager:first", q),
			w = 0,
			u = 0,
			p = 0,
			j = undefined,
			o = false,
			n = true,
			x = 0,
			result = 0,
			s = e.axis === "x";
		function m() {
			 if (e.controls) {
				 d.toggleClass("disable", p <= 0);
				if(x < k.length ){
					f.toggleClass("disable", !(p + 1 <= u))
				}else{
					f.toggleClass("disable", !(p + 1 < u))
				}
			 }
		}
		function t() {
			if (e.interval && !o) {
				clearTimeout(j);
				j = setTimeout(function() {
					p = p + 1 === u ? -1 : p;
					n = p + 1 === u ? false : p === 0 ? true : n;
					i.move(n ? 1 : -1);
				}, e.intervaltime)
			}
		}
		function r() {
			if (d.length > 0 && f.length > 0) {
				d.click(function() {
					i.move(-1);
					return false
				});
				f.click(function() {
					i.move(1);
					return false
				})
			}
		}
		this.move = function(y, z) {
			p = z ? y : p += y;
			if (p > -1 && p <= u) {
				var x = {};
				x[s ? "left" : "top"] = -(p * (result / k.length * e.display)); //w = 201
				g.animate(x, {
					queue: false,
					duration: e.animation ? e.duration : 0,
					complete: function() {
						if (typeof e.callback === "function") {
							e.callback.call(this, 1, p)
						}
					}
				});
				m();
				t()
			}
		};

		function c() {
			w = s ? a(k[0]).outerWidth(true) : a(k[0]).outerHeight(true);
			for( var j =0 ;j < k.length; j++){
				w = s ? a(k[j]).outerWidth(true) : a(k[j]).outerHeight(true);
				result += w;
			}
			x = Math.ceil(((s ? h.outerWidth() : h.outerHeight()) / (w * e.display)) - 1);
			u = Math.max(1, Math.ceil(k.length / e.display) - x);
			p = Math.min(u, Math.max(1, e.start)) - 2;
			//g.css(s ? "width" : "height", (w * k.length));
			g.css(s ? "width" : "height",3000);
			i.move(1);
			r();
			return i
		}
		return c()
	}
	a.fn.jqueryScroll = function(d) {
		var c = a.extend({}, a.tiny.carousel.options, d);
		this.each(function() {
			a(this).data("tcl", new b(a(this), c))
		});
		return this
	}
})(jQuery);