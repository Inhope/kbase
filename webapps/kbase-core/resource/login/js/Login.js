$(function(){
	//验证码图片
	$('#captcha').click(function(){
		var src = $(this).attr('src');
		src = src.substring('?');
		src += "?"+new Date().getTime();
		$(this).attr('src',src);
	});
	
	$(':input').keydown(function(e) {
		if(e.keyCode == 13) {
			$('#submit1').trigger('click');
		}
	});
	
	$('#submit1').click(function() {
		if($(this).attr('logined') == 'true')
			return false;
		loginLoading();
		var password = $('input[id=password]').val();	
		if(password && $.trim(password) != '') {
			$('input[name=password]').val($.md5(password));
		}
		
		window.setTimeout(function() {
			$("form:first").submit();
		}, 10);
	});
	
	loginLoading = function() {
		var i = 0;
		window.setInterval(function() {
			i ++;
			if(i > 6) {
				i = 1;
			}
			var v = '登陆中';
			for(var j = 0; j < i; j ++) {
				v += '.';
			}
			$('#submit1').attr({
				logined : 'true',
				value : v
			});
		}, 350);
	}
});