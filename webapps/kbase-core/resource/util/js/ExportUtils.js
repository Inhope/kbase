/**
 * 数据导出详情见	ExportUtils.java与ExtActionSupportExport.java
 	参考例子见：CategoryshowReportAction.java
 * @auther Gassol.Bi
 * @since 2016-07-12 17:28
 * @description
 * 		基于jquery easyui，需要引用的文件应包含：
 * 		css: easyui.css、jquery-ex.css
 * 		js: Jquery.js、Jquery-ex.js、jquery.easyui.min.js
 */
$(function(){
	var that = window.ExportUtils = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*公共方法*/
	fn.extend({
		/*无弹窗*/
		ajax: function(options){
			var url = options.url, params = options.params, 
				onBefore = options.onBefore, onSuccess = options.onSuccess;
			/*事件不存在，或者事件存在但是返回true*/
			if(!onBefore || (onBefore && onBefore.call(that))){
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + url, 
					data : params,
					async: false,
					dataType : 'json',
					success : function(jsonResult){
						if(onSuccess != null)
							onSuccess.call(that, jsonResult);
					},
					error : function(msg){
						$.messager.alert('信息', '网络错误，请稍后再试!');
					}
				});
			}
		},
		
		/*获取同步信息*/
		interval: null,
		getExportResult: function (exportResultURL, code, msgArea){
			var suffix = ''; /*进度条后缀*/
	  		/*定时器*/
	  		var interval = this.interval = setInterval(function(){
	  			try {
					fn.ajax({
						url: exportResultURL,
						params: {code: code},
						onSuccess: function(jsonResult){
							/*相关参数*/
							var failureTotal = jsonResult.failureTotal,/*成功总计*/ 
								successTotal = jsonResult.successTotal,/*失败总计*/ 
								rowTotal = jsonResult.rowTotal,/*数据总计*/ 
								rst = jsonResult.rst,/*处理结果*/ 
								msg = jsonResult.msg, /*处理消息*/ 
								filePath = jsonResult.filePath; /*文件路径*/
							var msgCont;
							
							/*数据准备阶段*/
							if(rst == 0) {
								if(suffix.length == 4) suffix = '';
								else suffix += '*';
								msgCont = '消息：查询数据， 进行中' + suffix;
							
							/*文件生成阶段*/
							} else if (rst == 1) {
								if(suffix.length == 4) suffix = '';
								else suffix += '*';
								msgCont = '消息： ' + msg + '， 进行中' + suffix + 
								 	'总计：<b style="color: red; size: 24px;">' + rowTotal + '</b>条，' + 
									'成功：<b style="color: red;size: 24px;">' + successTotal + '</b>条，' + 
									'失败：<b style="color: red;size: 24px;">' + failureTotal + '</b>条';
							
							/*文件生成完毕*/	
							} else if (rst == 9){
								if(interval != null) clearInterval(interval);
								msgCont = '消息： ' + msg +
									'<br />总计：<b style="color: red; size: 24px;">' + rowTotal + '</b>条，' + 
									'成功：<b style="color: red;size: 24px;">' + successTotal + '</b>条，' + 
									'失败：<b style="color: red;size: 24px;">' + failureTotal + '</b>条' + 
									'，<a href="' + $.fn.getRootPath() + filePath + '">点击下载</a>';
							
							/*错误异常*/	
							} else {
								if(interval != null) clearInterval(interval);
								msgCont = '消息：' + msg;
								
							}
							
							$(msgArea).html(msgCont);
						}
					});
				} catch(e) {
					$.messager.alert('信息', e);
					clearInterval(interval);
				}
	  		}, 500);
		}
	});
	
	/*公共方法*/
	that.extend({
		toExcel: function(exportDataURL, exportResultURL, params){
			fn.ajax({
				url: exportDataURL,
				params: params,
				onSuccess: function(jsonResult){
					var msgArea = this.msgArea; /*提示信息消息区*/
					msgArea.innerHTML = '消息：查询数据， 进行中';
					
					var code = jsonResult.code;
					if(code) {/*导出信息缓存key*/
						$('#win_ExportUtils').css('display', '');
						$('#win_ExportUtils').window({
							collapsible: false, closable: true,
							minimizable: false, maximizable: false,
							width:500,    
						  	height:150,
						  	title: '数据导出',
						  	modal:true,
						  	onOpen: function (){
						  		setTimeout(function (){
						  			/*导出信息缓存url, 导出信息缓存key, 提示信息标题区, 提示信息消息区*/
						  			fn.getExportResult(exportResultURL, code, msgArea);
						  		}, 1000);
						  	},
						  	onClose:function(){
						  		/*关闭定时器*/
						  		var interval = fn.interval;
						  		if(interval != null) clearInterval(interval);
						  	}
						});
					}
				}
			});
		},
		/*初始化*/
		tieArea: null, msgArea: null,
		init:function(){
			var div = document.createElement('DIV');
			div.id = 'win_ExportUtils';
			div.style.display = 'none';
			div.innerHTML = 
				'<div style="margin:10px 0 0 10px;">' + 
					'<b id="msg_ExportUtils">消息：查询数据中， 进行中<b><br/>' + 
				'</div>';
			document.body.appendChild(div);
			
			/*相关dom对象*/
	  		this.msgArea = document.getElementById('msg_ExportUtils'); /*消息区域*/
		}
	});
	
	/*初始化*/
	ExportUtils.init();
});