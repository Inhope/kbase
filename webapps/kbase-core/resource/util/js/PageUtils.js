/**
 * 积分管理——工具类
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 * @description
 * 		基于jquery easyui，需要引用的文件应包含：
 * 		css: icon.css、easyui.css、jquery-ex.css
 * 		js: Jquery.js、Jquery-ex.js、jquery.easyui.min.js、easyui-lang-zh_CN.js
 */
 ;(function($){
 	/*类声明*/
	var that = window.PageUtils = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
		arrayToString: function(array, separator){
			if(array) {
				var str = ''
				$(array).each(function(){
					str += this + separator;
				});
				/*去掉首尾separator*/
				var reg = new RegExp('^' + separator  + '|' 
					+ separator + '$', 'g');
				return str.replace(reg, '');
			}
			return '';
		},
		/*jquery-easyui tree 数据处理*/
		treeData: function(data){
			var idFiled = 'id', textFiled = 'text', parentField = 'pId';
			var i, l, treeData = [], tmpMap = [];
			for (i = 0, l = data.length; i < l; i++) {
				tmpMap[data[i][idFiled]] = data[i];
			}
			for (i = 0, l = data.length; i < l; i++) {
				if (tmpMap[data[i][parentField]] && data[i][idFiled] != data[i][parentField]) {
					if (!tmpMap[data[i][parentField]]['children']) {
						tmpMap[data[i][parentField]]['children'] = [];
						tmpMap[data[i][parentField]]['state'] = 'closed';
					}
					data[i]['text'] = data[i][textFiled];
					tmpMap[data[i][parentField]]['children'].push(data[i]);
				} else {
					data[i]['text'] = data[i][textFiled];
					treeData.push(data[i]);
				}
			}
			return treeData;
		},
		browser:{
			version: function (){/*浏览器验证*/
			    var userAgent = navigator.userAgent; /*取得浏览器的userAgent字符串*/
			    var isOpera = userAgent.indexOf("Opera") > -1;
			    if (isOpera) return 'Opera'; /*判断是否Opera浏览器*/
				else if (userAgent.indexOf('Firefox') > -1) return 'FF'; /*判断是否Firefox浏览器*/
				else if (userAgent.indexOf('Chrome') > -1) return 'Chrome';
			    else if (userAgent.indexOf('Safari') > -1) return 'Safari'; /*判断是否Safari浏览器*/
			    else if (userAgent.indexOf('compatible') > -1 && 
			    	userAgent.indexOf('MSIE') > -1 && !isOpera) {
			    	/*判断ie版本*/
			    	var IE5 = IE55 = IE6 = IE7 = IE8 = IE9 = false;
			        var reIE = new RegExp('MSIE (\\d+\\.\\d+);');
			        reIE.test(userAgent);
			        var fIEVersion = parseFloat(RegExp['$1']);
			        IE55 = fIEVersion == 5.5;
			        IE6 = fIEVersion == 6.0;
			        IE7 = fIEVersion == 7.0;
			        IE8 = fIEVersion == 8.0;
			        IE9 = fIEVersion == 9.0;
			        if (IE55) return 'IE55';
			        if (IE6) return 'IE6';
			        if (IE7) return 'IE7';
			        if (IE8) return 'IE8';
			        if (IE9) return 'IE9';
			    }
			    return '';
			},
			getWidth: function(){
				var winWidth = $(window).width();
				var myBrowser = fn.browser.version();/*浏览器*/
				if(myBrowser == 'IE9' || myBrowser == 'IE8' || myBrowser == 'IE7'){
					/*获取窗口宽度*/
					if (window.innerWidth)
					   winWidth = window.innerWidth;
					else if ((document.body) && (document.body.clientWidth))
					   winWidth = document.body.clientWidth;
					
					/*通过深入Document内部对body进行检测，获取窗口大小*/
					if (document.documentElement && document.documentElement.clientHeight
						&& document.documentElement.clientWidth) {
					   winWidth = document.documentElement.clientWidth;
					}
				}
				return (winWidth + '').replace('px', '');
			},
			getHeight: function(){
				var winHeight = $(window).height();
				var myBrowser = fn.browser.version();/*浏览器*/
				if(myBrowser == 'IE9' || myBrowser == 'IE8' || myBrowser == 'IE7'){
					/*获取窗口高度*/
					if (window.innerHeight)
					   winHeight = window.innerHeight;
					else if ((document.body) && (document.body.clientHeight))
					   winHeight = document.body.clientHeight;
					/*通过深入Document内部对body进行检测，获取窗口大小*/
					if (document.documentElement && document.documentElement.clientHeight
						&& document.documentElement.clientWidth) {
					   winHeight = document.documentElement.clientHeight;
					}
				}
				return (winHeight + '').replace('px', '');
			}
		}
	});
	
	that.extend({/*功能方法*/
		dialog: {
			open: function(dialog, options){/*有弹窗-easyui*/
				var before = options.before;
				/*事件不存在，或者事件存在但是返回true*/
				if(!before || before && before(options)){
					var h = options.height, w = options.width;
					if(!h) h = fn.browser.getHeight()*0.90;/*弹窗容器高度*/
					if(!w) w = fn.browser.getWidth()*0.95;/*弹窗容器宽度*/
					/*弹窗内容*/
					var content = options.content;
					if(typeof(content) == 'undefined'){
						var dialogId = $(dialog).attr('id');
						if(!dialogId) dialogId = dialog;/*弹窗容器id*/
						var url = options.url,/*请求地址*/
							params = options.params;/*请求参数*/
						/*弹窗id，用于页面回掉（譬如保存成功会进行弹窗关闭等）*/
						if(url.indexOf('?') > -1)
							url += '&dialogId=' + dialogId;
						else url += '?dialogId=' + dialogId;
						/*绑定参数*/
						if(params){
							for(var key in params){
								url += '&' + key + '=' + params[key];
							}
						}
						/*弹窗内容*/
						var dh = (h-40);
						if(options.buttons) dh = dh-38;
						var content = '<iframe id="' + dialogId + '-iframe" name="' 
							+ dialogId + '-iframe" frameborder="0" ' 
				    		+ 'src="url" style="width:99.9%;height:' + dh + 'px;"></iframe>';
				    	content = content.replace(/url/g, $.fn.getRootPath() + url);/*请求地址*/
					}
					/*打开弹窗*/
					$(dialog).dialog({
					    title: options.title,
					    height: h,
					    width: w,
					    cache: false,
					    content: content,
					    modal: true,
					    buttons: options.buttons
					});    
					$(dialog).dialog('open');
				}
			},
			close: function(dialogId){
				if(dialogId) $('#' + dialogId).dialog('close');
			}
		},
		window: {
			open: function(options){/*有弹窗-window*/
				var url = options.url, 
					winId = options.winId;
				var scrHeight = screen.height - 85, 
					scrWidth = screen.width - 20;
				var win = window.open($.fn.getRootPath() + url , winId, 
					'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
					+ scrWidth + ',height=' + scrHeight);
				win.focus();
			}
		},
		tab: {
			open: function(options){
				var id = options.id,
					name = options.name,
					url = options.url;
				if (parent.TABOBJECT) {
					parent.TABOBJECT.open({
						id : id,
						name : name,
						hasClose : true,
						url : $.fn.getRootPath() + url,
						isRefresh : true
					}, this);
				} else {
					window.open($.fn.getRootPath() + url).focus()
				}
			}
		},
		confirm: function(options){/*无弹窗*/
			var title = options.title;
			$.messager.confirm('确认','确定要' + 
				title + '选中的数据吗？', function(r){
			    if(r) that.ajax(options);   
			});
		},
		ajax: function(options){/*无弹窗*/
			var url = options.url, 
				params = options.params, 
				before = options.before, after = options.after;
			/*事件不存在，或者事件存在但是返回true*/
			if(!before || before && before(options)){
				$('body').ajaxLoading('提交中…');
		        $.ajax({
					type : 'post',
					url : $.fn.getRootPath() + url, 
					data : params,
					async: false,
					dataType : 'json',
					success : function(jsonResult){
						$('body').ajaxLoadEnd();
						after && after(jsonResult);
					},
					error : function(msg){
						$('body').ajaxLoadEnd();
						$.messager.alert('警告', msg);
					}
				});
			}
		},
		submit: function(form, options){
			var url = options.url, params = options.params, 
				before = options.before, after = options.after;
			$(form).form('submit', {
			    url: $.fn.getRootPath() + url, 
			    onSubmit: function(params){
			    	/*表单验证, 提交前事件严重*/
			    	return $(this).form('validate') 
			    		&& (!before || before && before(options, params));
			    },    
			    success:function(jsonResult){
			    	after && after(eval('(' + jsonResult + ')'));
			    }
			});
		},
		datagrid: {
			datagrid: null, getQueryParams: null,/*datagrid组件id, datagrid查询条件*/
			getChecked: function(){/*获取当前选中行(单选)*/
				var dg = this.datagrid;
				var rows = $(dg).datagrid('getChecked');
				if(rows.length > 1) 
					$.messager.alert('警告','不允许同时操作多行数据!');
				else if (rows.length == 1)  
					return rows[0].id;
				else {
					$.messager.alert('警告','请选择需要操作的行!');
				}
				return false;
			},
			getCheckeds: function(){/*获取当前选中行(多选)*/
				var dg = this.datagrid;
				var rows = $(dg).datagrid('getChecked');
				var ids = new Array();
				if(rows.length > 0) {
					$(rows).each(function(){
						ids.push(this.id);
					});
					return ids;
				} else {
					$.messager.alert('警告','请选择需要操作的行!');
				}
				return false;
			},
			reload: function(page){/*刷新列表*/
				var dg = this.datagrid;
				if(page) /*跳转到指定页，一般用于查询条件发生改变，跳转到首页*/
					$(dg).datagrid('options').pageNumber = page
				if(this.getQueryParams) {/*查询条件*/
					var queryParams = this.getQueryParams()
					if(queryParams) $(dg).datagrid('options')
						.queryParams = queryParams;
				}
		        $(dg).datagrid('reload');
			},
			formatStatus: function(value, row, url, before){/*状态开关样式*/
				var img = $.fn.getRootPath() + '/resource/biztpl/images/' 
					+ (value == 0 ? 'switch_on.png' : 'switch_off.png');
				return '<img name="changeStatus" status="' + value + '" rowid="' + row.id 
					+ '" style="cursor: pointer;" src="' + img + '" width="40px;" ' 
					+ 'onclick="PageUtils.datagrid.setStatus(this, \'' + url + '\', ' + before + ')">';
			},
			setStatus: function(obj, url, before){
				var id = $(obj).attr('rowid'), 
					st = parseInt($(obj).attr('status'))  == 0 ? 1 : 0 , 
					img = $.fn.getRootPath() + '/resource/biztpl/images/' 
						+ (st == 0 ? 'switch_on.png' : 'switch_off.png');
				if(url && id){
					if(before == null || (before != null && before.call(obj, id, st))){
						$.ajax({
							type: 'POST',
							url: $.fn.getRootPath() + url,
							data: { id: id, status: st},
							dataType: 'json',
							success: function(jsonResult) {
								if(jsonResult.rst) {
									$(obj).attr('status', st);
									$(obj).attr('src', img);
								} else 
									$.messager.alert('警告', jsonResult.msg);
								/*刷新列表，datagrid中的数据*/
								PageUtils.datagrid.reload();
							}
						});
					}
				} else $.messager.alert('警告','数据异常!');
			}
		},
		init: function(params){/*初始化*/
			that.datagrid.datagrid = params.datagrid;/*DataGrid控件*/
			that.datagrid.getQueryParams = params.getQueryParams; /*DataGrid查询条件*/
		}
	});
	
	/*扩大验证*/
	$.extend($.fn.validatebox.defaults.rules, {
		minLength: {
			validator: function(value, param){
	            return value.length >= param[0];
	        },    
	        message: 'Please enter at least {0} characters.'
	    },
	    digits:{
	    	validator: function(value){
	            return /^\d+$/.test(value);
	        },    
	        message: '只能输入正整数'
	    },
	    min:{
	    	validator: function(value, param){
	    		/*
	    		var val = param[0];
	    		if(typeof($(val)) == 'object') {
	    			val = $(val).val();
	    			if(!val) return true;
	    		}
	            return parseInt(value) >= val;
	    		*/
	    		return parseInt(value) >= param[0];
	        },    
	        message: '请输入一个最小为 {0} 的值'
	    },
	    max:{
	    	validator: function(value, param){
	            return parseInt(value) <= param[0];
	        },    
	        message: '请输入一个最大为 {0} 的值'
	    },
	   	mobile:{
	    	validator: function(value){
	            return /^1[3|4|5|7|8][0-9]\d{4,8}$/.test(value);
	        },    
	        message: '必须是合法的手机号码'
	    } 
	});
 })(jQuery);
 