	//删除
	function btnDel(){
		var _url = $.fn.getRootPath() + "/app/biztpl/article!deleteImage.htm";
		var rows = $("#dataList").datagrid("getSelections");
		if (rows.length > 0){
			var ids = [];
			for(var i=0;i<rows.length;i++){
				ids.push(rows[i].id);
			}
			$.messager.confirm('消息提醒', "图片可能关联标准问，确定删除么？", function (data) {
				if(data){
		            $.ajax({
					type: "POST",
					url: _url,
					data: "ids="+ids,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){
							$.messager.alert('消息提醒','删除成功','info');
							doSearch();
						}else{
							$.messager.alert('消息提醒','删除失败','error');
						}
					}
				  });
				}
		     });
		}else{
			$.messager.alert('消息提醒','请选择数据','info');
			return false;
		}
	}
	
	
	//重置
	function btnReset(){
		$("#now-toolbar input").each(function() {
		$(this).prop("value","");
	});
	}
	
	//初始化加载
	$(function(){
		$('#endDate').datebox('setValue', formatterDate(new Date()));
		doSearch();
		//双击事件
		$('#dataList').datagrid({  
		  onDblClickRow: function (rowIndex, rowData) {
		  	//window.open(rowData.filePath);
			var choose_image_toBig = $("#choose_image_toBig");
			var imageDom = $("#choose_image_toBig").find("img");
			var _height = $(window).height();
			var _width = $(window).width();
			choose_image_toBig.css({
				height:_height,
				width:_width
			});
			imageDom.css({
				'max-height':_height,
				'max-width':_width
			});
			imageDom.attr("src",rowData.filePath);
			choose_image_toBig.show();
		  }
		});  
		//选中事件
		$('#dataList').datagrid({  
		onCheck:function(rowIndex, rowData){
		        	if(rowData == null) /*jquery easyui 的bug 貌似有时候会丢掉参数rowIndex*/
		        		rowData = rowIndex;
		        	if(rowData == null) /*当无法获取的时候，采用getSelections方式*/
		        		rowData = $('#dataList').treegrid('getSelections')[0];
		        	if(rowData){
		        		$('#data').attr('isFile', 'true');
		        		$('#data').attr('filename', rowData.fileName);
		        		$('#data').attr('filepath', rowData.filePath);
		        	}
		        }
		});
	});
	
	//查询
	function doSearch(){
		var name = $.trim($("#imgName").val());
		var createUser =$.trim($("#createUser").val());
		var startDate = $.trim($('#startDate').datebox('getValue'));
		var endDate = $.trim($('#endDate').datebox('getValue'));
		var param = {
			name: name,
			createUser: createUser,
			startDate: startDate,
			endDate: endDate
		};
		$('#dataList').datagrid({
			url : $.fn.getRootPath() + "/app/biztpl/article!imageDataJson.htm",
			queryParams :param
		});
	}
	
	//返回一个图片
	function fmtImg(value, row, index){
		return "<img src='" + row.filePath + "' width='100px' height='100px' />"
	}
	
	//列表展示格式化时间
	function fmtDate(value){
		if(value != undefined){
			return $.fn.datebox.defaults.formatter(new Date(value.time));
		}else{
			return "";
		}
	}
	
	
	//格式化时间
	function formatterDate(date) {
	var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
	var month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0"
	+ (date.getMonth() + 1);
	return date.getFullYear() + '-' + month + '-' + day;
	};
