/**
 * 文章展示
 * @author Gassol.Bi
 * @date Jun 5, 2016 9:52:26 AM
 *
 */
$(function () {
	var that = window.CkEditImage = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*公共方法*/
	fn.extend({
		/*参数转换*/
		parseJSON: function(str){
			str = '{' + str + '}';
			return eval('(' + str + ')');
		},
		check: function(obj){
			var fileName = obj.value;
			var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
			if (suffix != ('.jpg') && suffix != ('.bmp') 
				&& suffix != ('.png') && suffix != ('.gif')) {
				$.messager.alert('信息', '请选择图片格式（.jpg,.bmp,.png,.gif）!');
				obj.parentElement.innerHTML += '';
			}
		}
	});
	/*功能方法*/
	that.extend({
		/*获取editor对象*/
		create: {
			editor: null,
			getEditor:function(){
				return this.editor;
			},
			replace: function(textarea, options){
				var height, toolbar, extraPlugins;
				if(options) {
					height = options.height; /*编辑器高度*/
					toolbar = options.toolbar;/*编辑器工具按钮*/
					extraPlugins = options.extraPlugins;/*拓展插件*/
				}
				/*编辑器高度*/
				if(height == null){
					height = 300;
				}
				/*编辑器工具按钮*/
				if(toolbar == null) {
					toolbar = 
						[
					      	['Cut','Copy','Paste','PasteText','PasteFromWord'],
					      	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
					      	['MyImage','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
					       	'/',
					      	['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
					       	['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
					       	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					       	['Link','Unlink','Anchor'],
					      	'/',
					        ['Styles','Format','Font','FontSize','lineheight'],
					       	['TextColor','BGColor'],
					       	['Embed','Ansattr'],
					       	['Maximize','Preview']
					  	];
					
				}
				/*拓展插件*/
				if(extraPlugins == null){
					extraPlugins = 'MyImage,lineheight';
				}
				//customConfig为指定config，未指定则用默认config
				var defaultConfig = $.fn.getRootPath()+'/library/ckeditor/config.js';
				if(typeof(customConfig) != "undefined"){
					defaultConfig = customConfig;
				}
				/*编辑器初始化*/
				return this.editor = CKEDITOR.replace(textarea, {
					uiColor: 'FAFAFA',
					height: height,
					toolbar: toolbar,
					removePlugins: 'elementspath',	/*去掉下方的 body div table 提示*/
					enterMode: CKEDITOR.ENTER_BR,
					shiftEnterMode: CKEDITOR.ENTER_P,
					allowedContent:  true,
					customConfig:defaultConfig,
					extraPlugins: extraPlugins/*, 
					filebrowserUploadUrl: pageContext.contextPath 附件上传地址
						+ '/app/biztpl/article!imageUpload.htm'*/
				});
			},
			/*editor初始化*/
			init: function(){
				/*Section 1 : 按下自定义按钮时执行的代码 */
				var cmdDef = {
			        exec: function(editor){
			        	$('#ckEdit-myImage-tabs').css({display: ''});
			        	$('#ckEdit-myImage').dialog({
			        		title: '图片插入',
			        		top: $(document).scrollTop() + ($(window).height()-200)/2,
			        		width: 400,    
    						height: 200,
			        		closed: true,
			        		modal: true
			        	}); 
			        	$('#ckEdit-myImage').dialog('open');
			        }
			    }, cmdName = 'MyImage'; 
			    /*Section 2 : 创建自定义按钮、绑定方法*/
				CKEDITOR.plugins.add(cmdName, {
					init: function(editor) {
						editor.addCommand(cmdName, cmdDef);
						editor.ui.addButton && editor.ui.addButton(cmdName, {
							label: '图片',
			                command: cmdName
						});
					}
				});
			}
		},
		/*上传*/
		upload: {
			init: function(){
				/*easyui拓展*/
				$.extend($.fn.validatebox.defaults.rules, {
					selectFileFormat: {
						validator: function(value, param){
							if(value) {
			        			var fileName = value;
								var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
								if (suffix == ('.jpg') || suffix == ('.bmp') 
									|| suffix == ('.png') || suffix == ('.gif')) {
									return true;
								} else {
									$(param[0]).textbox('setText', '');
								}
			        		}
							return false;
						}, 
						message: '请选择图片格式（.jpg,.bmp,.png,.gif）!' 
					} 
				});
				/*图片选择*/
				$('#imagechoose_link').linkbutton({
				    onClick: function(){
				    	var he = $(window).height()*0.9;
				    	var content = '<iframe id="dd-file-choose-iframe" name="dd-file-choose-iframe" frameborder="0" ' 
				    		+ 'src="url" style="width:100%;height:' + (he - 65 -20) + 'px;"></iframe>';
				    	content = content.replace(/url/g, $.fn.getRootPath() + '/app/util/choose!file.htm');/*附件上传地址*/
				    	$('#choose_file-myImage').dialog({
						    title: '图片选择',
						    width: $(window).width()*0.7,
						    height: he,
						    top: $(document).scrollTop() + ($(window).height()-he)/2,
						    closed: false,    
						    cache: false,
						    content: content,
						    modal: true,
							buttons:[{
								text:'保存',
								iconCls:'icon-ok',
								handler:function(){
									var data = window.frames['dd-file-choose-iframe']
										.document.getElementById('data');
									var isFile = $(data).attr('isFile'), 
										filename = $(data).attr('filename'), 
										filepath = $(data).attr('filepath');
									if(isFile){
										$('#image_link').textbox('setText', filename);
								    	$('#imagepath_link').val(filepath);
										$('#choose_file-myImage').dialog('close');
									} else {
										$.messager.alert('消息', '请选择有效文件!');
									}
								}
							}]
						});    
						$('#choose_file-myImage').dialog('open');
				    }   
				});  
			}
		},
		init: function () {
			/*editor初始化*/
			that.create.init();
			/*easyui拓展*/
			that.upload.init();
		}
	});
	/*初始化*/
	that.init();
});
