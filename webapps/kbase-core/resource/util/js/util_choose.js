var choose_layer = {
	station : function(sIds,sNames, chkStyle){
		if(typeof(sIds) == "undefined" || $("#"+sIds).length<1) return;
		if(typeof(sNames) == "undefined" || $("#"+sNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		var height = $(window).height()-10;
		var paramsName = "?height="+(height*0.90);
		paramsName += "&sIds="+sIds;
		paramsName += "&sNames="+sNames;
		paramsName += "&chkStyle="+chkStyle;
		this.s_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["岗位选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + "/app/util/choose!station.htm"+paramsName}, 
			area:["250px", height+"px"]
		});
	},
	role : function(rIds,rNames, chkStyle){
		if(typeof(rIds) == "undefined" || $("#"+rIds).length<1) return;
		if(typeof(rNames) == "undefined" || $("#"+rNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		var height = $(window).height()-10;
		var paramsName = "?height="+(height-105);
		paramsName += "&rIds="+rIds;
		paramsName += "&rNames="+rNames;
		paramsName += "&chkStyle="+chkStyle;
		this.r_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["角色选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + "/app/util/choose!role.htm"+paramsName}, 
			area:["250px", height+"px"]
		});
	},
	category : function(cIds,cNames,cFIds,cFNames,chkStyle){
		if(typeof(cIds) == "undefined" || $("#"+cIds).length<1) return;
		if(typeof(cNames) == "undefined" || $("#"+cNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		var height = $(window).height()-10;
		var paramsName = "?height="+(height-105);
		paramsName += "&cIds="+cIds;
		paramsName += "&cNames="+cNames;
		paramsName += "&chkStyle="+chkStyle;
		paramsName += "&cFIds="+cFIds;
		paramsName += "&cFNames="+cFNames;
		this.c_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["实例分类选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + "/app/util/choose!category.htm"+paramsName}, 
			area:["250px", height+"px"]
		});
	},
	workflow : function(wIds,wNames,moduleId,chkStyle){
		if(typeof(wIds) == "undefined" || $("#"+wIds).length<1) return;
		if(typeof(wNames) == "undefined" || $("#"+wNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		if(typeof(chkStyle) == "moduleId") return;
		var height = $(window).height()-10;
		var paramsName = "?height="+(height-105);
		paramsName += "&wIds="+wIds;
		paramsName += "&wNames="+wNames;
		paramsName += "&chkStyle="+chkStyle;
		paramsName += "&moduleId="+moduleId;
		this.w_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["流程选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + "/app/util/choose!workflow.htm"+paramsName}, 
			area:["250px", height+"px"]
		});
	},
	srtype : function(sIds,sNames,sFIds,sFNames,chkStyle){
		if(typeof(sIds) == "undefined" || $("#"+sIds).length<1) return;
		if(typeof(sNames) == "undefined" || $("#"+sNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		var width = document.body.clientWidth*0.9,
			height = document.body.clientHeight*0.85;
		var	paramsName = "?height=" + (height-105);
		paramsName += "&width=" + width;
		paramsName += "&sIds="+sIds;
		paramsName += "&sNames="+sNames;
		paramsName += "&chkStyle="+chkStyle;
		paramsName += "&sFIds="+sFIds;
		paramsName += "&sFNames="+sFNames;
		this.gs_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["服务类型选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + '/app/util/choose!srtype.htm' + paramsName}, 
			area:[width + 'px', height + 'px']
		});
	},
	user : function(uIds,uNames,chkStyle){
		if(typeof(uIds) == "undefined" || $("#"+uIds).length<1) return;
		if(typeof(uNames) == "undefined" || $("#"+uNames).length<1) return;
		if(typeof(chkStyle) == "undefined") return;
		var height = $(window).height()-10;
		var paramsName = "?height="+(height*0.90);
		paramsName += "&uIds="+uIds;
		paramsName += "&uNames="+uNames;
		paramsName += "&chkStyle="+chkStyle;
		this.gs_layer_index = $.layer({
			type:2, 
			border: [2, 0.3, '#000'], 
			title:["用户选择", 'font-size:12px;font-weight:bold;'], 
			closeBtn:[0, true],
			iframe:{src:$.fn.getRootPath() + "/app/util/choose!user.htm"+paramsName}, 
			area:["250px", height+"px"]
		});
	},
	close: function(layer_index){
		layer.close(layer_index);
	},
	s_layer_index : -1,
	r_layer_index : -1,
	c_layer_index : -1,
	w_layer_index : -1,
	u_layer_index : -1,
	gs_layer_index : -1
}

//岗位
function choose_stations(sIds,sNames){
	choose_layer.station(sIds,sNames,'');
}

function choose_station(sIds,sNames){
	choose_layer.station(sIds,sNames,'radio');
}

//角色
function choose_roles(rIds,rNames){
	choose_layer.role(rIds,rNames,'');
}

function choose_role(rIds,rNames){
	choose_layer.role(rIds,rNames,'radio');
}

//实例分类
function choose_categorys(cIds,cNames,cFIds,cFNames){
	if(typeof(cFIds) == "undefined" || $("#"+cFIds).length<1) cFIds = '';
	if(typeof(cFNames) == "undefined" || $("#"+cFNames).length<1) cFNames = '';
	choose_layer.category(cIds,cNames,cFIds,cFNames,'');
}

function choose_category(cIds,cNames,cFIds,cFNames){
	if(typeof(cFIds) == "undefined" || $("#"+cFIds).length<1) cFIds = '';
	if(typeof(cFNames) == "undefined" || $("#"+cFNames).length<1) cFNames = '';
	choose_layer.category(cIds,cNames,cFIds,cFNames,'radio');
}

//实例分类,moduleId:模板id
function choose_workflows(wIds,wNames,moduleId){
	choose_layer.workflow(wIds,wNames,moduleId,'');
}

function choose_workflow(wIds,wNames,moduleId){
	choose_layer.workflow(wIds,wNames,moduleId,'radio');
}

//服务类型
function choose_srtypes(sIds,sNames,sFIds,sFNames){
	if(typeof(sFIds) == "undefined" || $("#"+sFIds).length<1) sFIds = '';
	if(typeof(sFNames) == "undefined" || $("#"+sFNames).length<1) sFNames = '';
	choose_layer.srtype(sIds,sNames,sFIds,sFNames,'');
}

function choose_srtype(sIds,sNames,sFIds,sFNames){
	if(typeof(sFIds) == "undefined" || $("#"+sFIds).length<1) sFIds = '';
	if(typeof(sFNames) == "undefined" || $("#"+sFNames).length<1) sFNames = '';
	choose_layer.srtype(sIds,sNames,sFIds,sFNames,'radio');
}

//用户(未做完)
function choose_users(uIds, uNames){
	choose_layer.user(uIds,uNames,'');
}

function choose_user(uIds, uNames){
	choose_layer.user(uIds,uNames,'radio');
}

