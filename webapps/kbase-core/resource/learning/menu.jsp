<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/resource/learning/taglib.jsp" %>
<div style="float: left; width: 200px;height: 30px;padding-top:5px;padding-left: 10px;">
	<font style="font-size: 14px;">学习培训<b> > </b>
		${learnType == "ques"?"题库啊":"测试的"}
	</font>
</div>
<div style="float: right; width: 350px;height: 30px;" id="learning-tabs">
	<ul class="kbs-conn">
		<li>
			<a href="javascript:void(0);" _value="ques" style="${learnType=='ques'?'background: #edecec;':''}">题库</a>
		</li>
		<li>
			<a href="javascript:void(0);" _value="paper" style="${learnType=='paper'?'background: #edecec;':''}">试卷</a>
		</li>
		<li>
			<a href="javascript:void(0);" _value="exam" style="${learnType=='exam'?'background: #edecec;':''}">考试</a>
		</li>
		<li>
			<a href="javascript:void(0);" _value="course" style="${learnType=='course'?'background: #edecec;':''}">课件</a>
		</li>
		<li>
			<a href="javascript:void(0);" _value="train" style="${learnType=='train'?'background: #edecec;':''}">培训</a>
		</li>
	</ul>
</div>