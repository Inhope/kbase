/*试卷管理-随机题目*/
RandomQues = {
	/*方法*/
	fn: {
		quesTypes: {}, /*试卷管理-题目难易*/
		/*获取题目数据表*/
		getTable: function(flag){
			return  $('table[flag="' + flag + '"]:eq(0)');
		},
		getQuesTypes: function(){
			return this.quesTypes
		},
		setQuesTypes: function(quesTypes){
			var _that = this;
				_that.quesTypes = quesTypes;
			/*修改已有的数据类型*/
			var req = RandomQues.req, rule = RandomQues.rule, 
				tab_req = _that.getTable('req'), tab_rule = _that.getTable('rule');
			var sel_req = $(tab_req).find('select[name="key"]'); /*必选题*/
			var sel_rule = $(tab_rule).find('select[name="key"]');/*随机题规则*/
			/*分类选择*/
			var sel = '';
			for(var key in quesTypes){
				sel += '<option value="' + key + '">' 
					+ quesTypes[key] + '</option>';
			}
			/*移除关联行*/
			$(sel_req).each(function(){ 
				var key = $(this).val();/*当前类型*/
				$(this).empty();/*清空option*/
				$(this).append(sel);/*添加新的option*/
				$(this).val(key);
			});
			$(sel_rule).each(function(){ 
				var key = $(this).val();/*当前类型*/
				$(this).empty();/*清空option*/
				$(this).append(sel);/*添加新的option*/
				$(this).val(key);
			});
			
		},
		digits: function(value){
			return /^\d+$/.test(value);
		}
	},
	
	/*题目设置*/
	set: {
		/*获取表数据*/
		getData: function(noCheck){
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('set'),/*获取选中行*/
				data = new Array();
			/*获取数据*/
			$(tab).find('tr:gt(0)').each(function(i, row){
				var key = $(row).find('input[type="checkbox"][name="ck"]').val(),/*题目类型*/
					score = $(row).find('input[type="text"][name="key_score"]').val(),/*题目分数*/
					amount = $(row).find('input[type="text"][name="key_amount"]').val();/*题目数量*/
				data.push({'key': Number(key), 'score': Number(score), 'amount': Number(amount)});
			});
			/*数据验证*/
			var josnResult = {'rst': false}
			if(noCheck){/*不需要验证*/
				josnResult['rst'] = true;
				josnResult['data'] = data;
			} else {
				if(_that.checkData(data)){
					josnResult['rst'] = true;
					josnResult['data'] = data;
				}
			}
			return josnResult;
		},
		/*数据验证*/
		checkData: function(data){
			var bl = true;
			if(data){
				var score_ = 0;/*总分*/
				$(data).each(function(i, item){
					var score = item['score'],
						amount = item['amount'];
					/**
					if(score < 1) {
						layer.alert('"题目选择"-"每题分数"应该大于0!', -1);
						return bl = false;
					}
					if(amount < 1) {
						layer.alert('"题目选择"-"计划题数"应该大于0!', -1);
						return bl = false;
					}
					**/
					score_ += amount*score;
				});
				var score = _PaperScore;
				if(score_ != score){
					layer.alert('"题目选择"总分数(' + score_ + ')应与试卷总分(' + score + ')相等!', -1);
					return bl = false;
				}
			}
			return bl;
		},
		/*判断行是否在指定数组中*/
		inRowArray: function(row, rows){
			/*当前行的id*/
			var id = $(row).find('input[type="checkbox"]').val();
			/*选中的id*/
			var ids = new Array();
			$(rows).each(function(i, item){
				var id = $(item).find('input[type="checkbox"]').val();
				ids.push(id); 
			});
			ids = ids.join(',');
			return ids.indexOf(id);
		},
		
		/*点击*/
		click: function(key, tab){
			/*当前试题类型的name*/
			var _that = this, name;
			/*渲染选中行样式*/
			$('.left_title table tr').each(function(i, item){
				if($(item).attr('key') == key) {
					$(item).addClass('sel');
					name = $(item).children('td:eq(0)').text();
				}
				else $(item).removeClass('sel');
			});
			/*对应table添加行数据*/
			_that.addRow(key, name, tab);
		},
		/*新增*/
		addRow: function(key, name, tab){
			var _that = this, fn = RandomQues.fn;
			var key_ = $(tab).find('input[type="checkbox"][value="' + key + '"]').val();
			if(key != key_){
				$(tab).find('tr:last').after(
					'<tr>' + 
						'<td><input type="checkbox" name="ck" value="' + key + '" ></td>' + 
						'<td name="key_name">' + name + '</td>' + 
						'<td><input type="text" name="key_score" value="0" ></td>' + 
						'<td><input type="text" name="key_amount" value="0" ></td>' + 
					'</tr>');/*添加行*/
			}
		},
		/*上移*/
		moveUp: function(tab){
			var _that = this, 
				rows = $(tab).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows){
				$(rows).each(function(i, row){
					var index = $(row).index();/*当前行位置*/
					var row_ = $(row).prev();/*当前行的前一行*/
					if(index > 1 && /*标题行与第一行不上移, 当前行的前一行在被移动行中也不上移*/
						_that.inRowArray(row_, rows) == -1){
							$(row_).remove();
							$(row).after(row_);
						}
				});
			}
		},
		/*下移*/
		moveDown: function(tab){
			var _that = this, 
				maxLen = $(tab).find('tr').length;
				rows = $(tab).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows){
				$($(rows).toArray().reverse())./*倒序*/
					each(function(i, row){
						var index = $(row).index();/*当前行位置*/
						var row_ = $(row).next();/*当前行的后一行*/
						if(index < maxLen-1 && /*最后一行不下移, 当前行的后一行在被移动行中也不下移*/
							_that.inRowArray(row_, rows) == -1){
								$(row).remove();
								$(row_).after(row);
							}
				});
			}
		},
		/*删除*/
		delRow: function(tab){
			var _that = this, fn = RandomQues.fn,
				rows = $(tab).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows) {
				var msg_inx = layer.confirm('将移除已选类型的"必选题"、"随机题规则"!', function(){
					layer.close(msg_inx);
					$(rows).each(function(index, row){
						if($(row).index() != 0 ) {
							/*移除相关数据*/
							var key = $(row).find('input[type="checkbox"]').val(), 
								req = RandomQues.req, rule = RandomQues.rule, 
								tab_req = fn.getTable('req'), tab_rule = fn.getTable('rule');
							var row_req = $(tab_req).find('tr').has('select[name="key"] option[value="' + key + '"]:selected, input[name="key"][value="' + key + '"]'); /*必选题*/
							var row_rule = $(tab_rule).find('tr').has('select[name="key"] option[value="' + key + '"]:selected, input[name="key"][value="' + key + '"]');/*随机题规则*/
							/*移除关联行*/
							$(row_req).each(function(){ $(this).remove(); });
							$(row_rule).each(function(){ $(this).remove(); });
							/*移除当前行*/
							$(row).remove();
						}
					});
				});
			}
		},
		
		init: function(){
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('set');
			/*渲染标题表，行点击事件*/
			$('.left_title table tr').each(function(i, item){
				var key = $(item).attr('key');
				if(key){/*标题行无key值*/
					$(item).click(function(){
						_that.click(key, tab);
					});
				}
			});
			/*“题目类型设置”按钮事件*/
			$('td[class="set_title"]').children('input[type="button"]')
				.each(function(i, item){
					var name = $(item).attr('name');
					$(item).click(function(){
						if(name == 'upBtn'){
							_that.moveUp(tab);
						} else if (name == 'downBtn'){
							_that.moveDown(tab);
						} else if (name == 'delBtn'){
							_that.delRow(tab);
						}
					});
			});
			/*文本框设置数字验证监听*/
			$(tab).on('keyup', 'input[type="text"][name="key_score"], input[type="text"][name="key_amount"]', 
				function(){
					var v = $(this).val();
					if(!fn.digits(v) || Number(v)<1) 
//						$(this).val(1);/*判断是否是正整数*/
						$(this).val(0);
			});
			
			/*全选*/
			$(tab).find('tr:eq(0) input[type="checkbox"]').click(function(){
				var checked = $(this).attr('checked');
				if(checked == 'checked') 
					$(tab).find('tr input[type="checkbox"]').attr('checked', checked);
				else $(tab).find('tr input[type="checkbox"]').removeAttr('checked', checked);
			});
		}
	},
	
	/*概述表*/
	sum: {
		/*获取表数据*/
		getData: function(){
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('sum'),/*获取选中行*/
				data = new Array();
			/*刷新概述表*/
			_that.refresh();
			/*获取数据*/
			$(tab).find('tr:gt(0)').each(function(i, row){
				var score = $(row).find('td[name="score"]').text(),/*题目类型*/
					amount = $(row).find('td[name="amount"]').text(),/*题目数量*/
					amount_ = $(row).find('td[name="amount_"]').text();/*题目数量(真实)*/
				data.push({'score': Number(score), 'amount': Number(amount), 'amount_': Number(amount_)});
			});
			return _that.checkData(data);
		},
		/*数据验证*/
		checkData: function(data){
			var josnResult = {'rst': true},  bl = true;
			if(data){
				var score_ = 0;/*总分*/
				$(data).each(function(i, item){
					var score = item['score'],
						amount = item['amount'],
						amount_ = item['amount_'];
					/*渲染td样式*/
					if(amount_ > amount) {
						layer.alert('"题目选择"-"实际题数"不应大于"计划题数"为空!', -1);
						return bl = false;
					} else if(amount_ < amount){
						layer.alert('"题目选择"-"实际题数"不应小于"计划题数"为空!', -1);
						return bl = false;
					}
					/*总分*/
					score_ += amount_*score;
				});
				josnResult['rst'] = bl;
				josnResult['score'] = score_;
				josnResult['data'] = data;
			}
			return josnResult;
		},
		/*刷新概述表*/
		refresh: function(){
			var _that = this, fn = RandomQues.fn, 
				tab = fn.getTable('sum');
			var data = {};
			var reqResult = RandomQues.req.getData(true);
			$(reqResult.data).each(function(i, item){
				var key = item.key;
				if(data[key]) data[key] += 1;
				else data[key] = 1;
			});
			var ruleResult = RandomQues.rule.getData(true);
			$(ruleResult.data).each(function(i, item){
				var key = item.key,
					amount = item.amount;
				if(amount < 1) amount = 1;/*随机难度*/
				if(data[key]) data[key] += amount;
				else data[key] = amount;
			});
			$(tab).find('tr[key]').each(function(i, row){
				var key  = Number($(row).attr('key')),
					amount = Number($(row).find('td[name="amount"]').text()),
					score = Number($(row).find('td[name="score"]').text());
				if(!data[key]) data[key] = 0;
				var td_amount_ = $(row).find('td[name="amount_"]');/*实际题数td*/
				$(td_amount_).text(data[key]);
				$(row).find('td[name="score_"]').text(data[key]*score);
				/*渲染td样式*/
				if(data[key] != amount) 
					$(td_amount_).addClass('err');
				else $(td_amount_).removeClass('err');
			});
		},
		/*概述表初始化*/
		init: function(data){
			var _that = this, fn = RandomQues.fn, 
				tab = fn.getTable('sum');
			/*移除旧有数据行*/
			$(tab).find('tr:not(:first)').remove();
			/*移除新数据行*/
			for(var i = 0; i<data.length; i++){
				var key = data[i].key,
					name = _QuesTypes[key],
					score = data[i].score,
					amount = data[i].amount;
				$(tab).find('tr:last').after(
					'<tr key="' + key + '">' + 
						'<td>' + name + '</td>' + 
						'<td name="score">' + score + '</td>' + 
						'<td name="amount">' + amount + '</td>' + 
						'<td name="amount_">0</td>' + 
						'<td name="score_">0</td>' + 
					'</tr>');/*添加行*/
			}
			/*刷新概述表*/
			_that.refresh();
		}
	},
	
	/*必选题*/
	req: {
		/*获取表数据, noCheck(true不需要验证)*/
		getData: function(noCheck){
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('req'), /*获取选中行*/
				data = new Array();
			/*获取数据*/
			$(tab).find('tr:gt(0)').each(function(i, row){
				var key = $(row).find('select[name="key"], input[name="key"]').val(),/*题目类型*/
					quesId = $(row).find('input[name="quesId"]').val();/*题目*/
				data.push({'key': Number(key), 'quesId': quesId});
			});
			/*数据验证*/
			var josnResult = {'rst': false}
			if(noCheck){/*不需要验证*/
				josnResult['rst'] = true;
				josnResult['data'] = data;
			} else {
				if(_that.checkData(data)){
					josnResult['rst'] = true;
					josnResult['data'] = data;
				}
			}
			return josnResult;
		},
		/*数据验证*/
		checkData: function(data){
			var bl = true;
			if(data){
				$(data).each(function(i, item){
					var quesId = item['quesId'];
					if(!quesId) {
						layer.alert('"必选题"-"试题内容"不应为空!', -1);
						return bl = false;
					}
				});
			}
			return bl;
		},
		/*新增*/
		addRow: function(tab){
			var _that = this, fn = RandomQues.fn, 
				quesTypes = fn.getQuesTypes();/*题目类型*/
			/*分类选择*/
			var sel = '<select name="key">';
			for(var key in quesTypes){
				sel += '<option value="' + key + '">' 
					+ quesTypes[key] + '</option>';
			}
			sel += '</select>';
			
			/*追加行*/
			$(tab).find('tr:last').after(
				'<tr>' + 
					'<td>' + 
						'<input type="checkbox" name="ck" value="" >' + 
					'</td>' + 
					'<td>' + 
						sel + 
					'</td>' + 
					'<td>' + 
						'<input type="text" name="quesName" value="" readonly="readonly" style="text-align:left;">' + 
						'<input type="hidden" name="quesId" value="">' + 
					'</td>' + 
					'<td name="content"></td>' + 
				'</tr>');
			
			/*绑定题目选择事件*/
			$(tab).find('tr:last input[name="quesName"]').click(function(){
				_that.choose.execute(this, tab);
			});
			/*绑定类型改变事件*/
			sel = $(tab).find('tr:last select[name="key"]');
			$(sel).change(function(){
				_that.changeSel(this);
				/*刷新概述表*/
				RandomQues.sum.refresh();
			});
			/*刷新概述表*/
			RandomQues.sum.refresh();
		},
		/*删除*/
		delRow: function(tab){
			var _that = this, 
				rows = $(tab).find('tr').has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows) {
				$(rows).each(function(){
					if($(this).index() != 0 ) {
						$(this).remove();/*移除行*/
						RandomQues.sum.refresh();/*刷新概述表*/
					}
				});
			}
		},
		/*类型发生改变*/
		changeSel: function(obj){
			/*题目列， 内容列*/
			var ques = $(obj).parent().next(), 
				quesName = $(ques).children('input[name="quesName"]'),
				quesId = $(ques).children('input[name="quesId"]'),
				content = $(ques).next();
			$(quesName).val('');
			$(quesId).val('');
			$(content).html('');
		},
		/*题目选择*/
		choose:{
			/*事件(题目文本框， 表名)*/
			execute: function(obj, tab){
				/*移除行标识符*/
				$(tab).find('tr').each(function(){
					$(this).removeAttr('seleced');
				});
				/*添加选中行标识符*/
				var row  = $(obj).parent('td').parent('tr');
				$(row).attr('seleced', 'seleced');
				var key = $(row).find('select[name="key"]').val();
				if(key){
					var h = $("body").height(), w = $("body").width();
					$.kbase.picker.quesByType({
						key: key,/*题目分类类型*/
						initData: 'RandomQues.req.choose.initData',/*回显数据方法名*/
						callback: 'RandomQues.req.choose.callback',/*弹窗回掉方法名*/
						chkStyle: 'radio',/*单选*/
						diaWidth:w*0.9, diaHeight:h*0.9
					});
				}
			},
			/*数据回显*/
			initData: function(key){
				var _that = this, 
					fn = RandomQues.fn, 
					tab = fn.getTable('req'), /*获取选中行*/
					row = $(tab).find('tr[seleced="seleced"]');
				if(row){
					var id = $(row).find('input[name="quesId"]').val(), 
					name = $(row).find('input[name="quesName"]').val();
					if(id && name)
						return {'id': id, 'name': name};
				}
				return null;
			},
			/*弹窗保存回掉*/
			callback: function(key, data){
				var _that = this, 
					fn = RandomQues.fn, 
					req = RandomQues.req,
					tab = fn.getTable('req'), /*获取选中行*/
					row = $(tab).find('tr[seleced="seleced"]');
				data = data[0], id = data.id;
				$(row).find('input[name="quesId"]').val(id), 
				$(row).find('input[name="quesName"]').val(data.name);
				$(row).find('td[name="content"]').html('<textarea id="kd_' + id 
					+ '" style="display: none;">' + data.content + '</textarea>');
				req.editor.create('kd_' + id);
			}
		},
		/*富文本框*/
		editor: {
			/*生成*/
			create: function(id){
				KindEditor.create('textarea[id="' + id + '"]', 
						{width: '99%', readonlyMode : true,height: '60px', minHeight: '50px', items: []});
			},
			/*初始化*/
			init: function(){
				var _that = this;
				/*初始化相关富文本*/
				$('textarea[id^="kd_"]').each(function(ind, item){
					var id = $(item).attr('id');
					_that.create(id);
				});
			}
		},
		/*初始化*/
		init: function(){
			/*获取“必选”表*/
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('req');
			/*“必选”按钮事件*/
			$('td[class="req_title"]').children('input[type="button"]')
				.each(function(i, item){
					var name = $(item).attr('name');
					$(item).click(function(){
						if(name == 'addBtn'){
							_that.addRow(tab);
						} else if (name == 'delBtn'){
							_that.delRow(tab);
						}
					});
			});
			/*绑定题目选择事件(已有数据不进行编辑)
			$(tab).find('tr input[name="quesName"]').each(function(){
				$(this).click(function(){
					_that.choose.execute(this, tab);
				});
			});*/
			
			/*全选*/
			$(tab).find('tr:eq(0) input[type="checkbox"]').click(function(){
				var checked = $(this).attr('checked');
				if(checked == 'checked') 
					$(tab).find('tr input[type="checkbox"]').attr('checked', checked);
				else $(tab).find('tr input[type="checkbox"]').removeAttr('checked', checked);
			});
			
			/*富文本框渲染*/
			_that.editor.init();
		}
	},
	
	/*随机题规则*/
	rule: {
		/*获取表数据 noCheck(true不需要验证)*/
		getData: function(noCheck){
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('rule'), /*获取选中行*/
				data = new Array();
			/*获取数据*/
			$(tab).find('tr:gt(0)').each(function(i, row){
				var key = $(row).find('select[name="key"], input[name="key"]').val(),/*题目类型*/
					quesCateId = $(row).find('input[name="quesCateId"]').val(),/*分类id*/
					quesCateName = $(row).find('input[name="quesCateName"]').val(),/*分类name*/
					amount = $(row).find('input[name="amount"]').val();/*随机难度*/
				key = Number(key);
				var obj = {'key': key, 'desc': fn.getQuesTypes()[key],
					'quesCateId': quesCateId, 'quesCateName': quesCateName, 'amount': Number(amount)};
				/*每级难度数据*/
				for(var i=0; i<_Levels.length; i++){
					obj[_Levels[i].level] = 
						Number($(row).find('input[name="level'+ _Levels[i].level +'"]').val());
				}
				data.push(obj);
			});
			/*数据验证*/
			var josnResult = {'rst': false}
			if(noCheck){/*不需要验证*/
				josnResult['rst'] = true;
				josnResult['data'] = data;
			} else {
				if(_that.checkData(data)){
					josnResult['rst'] = true;
					josnResult['data'] = data;
				}
			}
			return josnResult;
		},
		/*数据验证*/
		checkData: function(data){
			var bl = true, flag = false, info;
			if(data){
				$(data).each(function(i, item){
					var desc = item['desc'],
						quesCateId = item['quesCateId'],
						quesCateName = item['quesCateName'],
						amount = item['amount'], amount_ = 0, 
						msg = '(题目类型(' + desc + ')-题目分类(' + quesCateName + '))';
					
					if(!quesCateId) {
						layer.alert('"随机题规则"-"题目分类"不应为空!', -1);
						return bl = false;
					}
					if(amount < 1) {
						layer.alert('"随机题规则' + msg + '"-"题目数量"应该大于0!', -1);
						return bl = false;
					}
					/*每级难度数据*/
					for(var i=0; i<_Levels.length; i++){
						amount_ += item[_Levels[i].level];
					}
					if(amount < amount_){
						layer.alert('"随机题规则' + msg + '"-"题目数量"不应小于其它难度之和!', -1);
						return bl = false;
					} else if (amount > amount_){
						flag = true;
						info = '存在随机题规则"题目数量"不等于其它难度之和，\n将从剩余符合条件的题目中随机抽取!'
					}
				});
			}
			if(flag){
				if(confirm(info)) {
					return bl;
				} else
					return false;
				/*
					layer.confirm(info, function(){
						return bl;
					}, function(){
						return false;
					});
				*/
			} else {
				return bl;
			}
		},
		/*新增*/
		addRow: function(tab, wid){
			var _that = this, fn = RandomQues.fn, 
				quesTypes = fn.getQuesTypes();/*题目类型*/
			/*分类选择*/
			var sel = '<select name="key">';
			for(var key in quesTypes){
				sel += '<option value="' + key + '">' 
					+ quesTypes[key] + '</option>';
			}
			sel += '</select>';
			/*行内容*/
			var row = '<tr>' + 
					'<td style="width: ' + wid + ';">' + 
						'<input type="checkbox" name="ck" value="" >' + 
					'</td>' + 
					'<td style="width: ' + wid + ';">' + 
						sel + 
					'</td>' + 
					'<td style="width: ' + wid + ';">' + 
						'<input type="text" name="quesCateName" readonly="readonly" value="">' + 
						'<input type="hidden" name="quesCateId" value="">' + 
					'</td>' + 
					'<td style="width: ' + wid + ';">' + 
						'<input type="text" name="amount" value="1">' + 
					'</td>';
			for(var i=0; i<_Levels.length; i++){
				var level = _Levels[i];
				row += '<td style="width: ' + wid + ';">' + 
							'<input type="text" name="level'+ level.level +'" value="0">' + 
						'</td>';
			}
			row += '</tr>';
			/*追加行*/
			$(tab).find('tr:last').after(row);
			/*绑定题目类型选择事件*/
			$(tab).find('tr:last input[name="quesCateName"]').click(function(){
				_that.choose.execute(this, tab);
			});
			/*绑定类型改变事件*/
			sel = $(tab).find('tr:last select[name="key"]');
			$(sel).change(function(){
				/*刷新概述表*/
				RandomQues.sum.refresh();
			});
			/*刷新概述表*/
			RandomQues.sum.refresh();
		},
		/*删除*/
		delRow: function(tab){
			var _that = this, 
				rows = $(tab).find('tr').has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows) {
				$(rows).each(function(){
					if($(this).index() != 0 ) {
						$(this).remove();/*移除行*/
						RandomQues.sum.refresh();/*刷新概述表*/
					}
				});
			}
		},
		choose: {
			/*事件(题目文本框， 表名)*/
			execute: function(obj, tab){
				/*移除行选择器标示*/
				$(tab).find('tr').each(function(){
					$(this).find('input[name="quesCateName"], input[name="quesCateId"]').removeAttr('id');
				});
				/*添加行选择器标示*/
				var row  = $(obj).parent('td').parent('tr');
				$(row).find('input[name="quesCateName"]').attr('id', 'quesCateName_select');
				$(row).find('input[name="quesCateId"]').attr('id', 'quesCateId_select');
				if(row){
					/*var h = $("body").height(), w = $("body").width();*/
					$.kbase.picker.singleQuesCate({
						returnField:"quesCateName_select|quesCateId_select",
						diaWidth:400, diaHeight:360
					});
				}
			}
		},
	
		init: function(){
			/*获取“随机题规则”表*/
			var _that = this, 
				fn = RandomQues.fn, 
				tab = fn.getTable('rule');
			/*表宽度设置*/
			var tds = $(tab).find('tr td'),
				wid = (99/tds.length).toFixed(1) + '%';
			$(tds).each(function(i, td){
				$(td).css({width: wid});
			});
			/*“随机题规则”按钮事件*/
			$('td[class="rule_title"]').children('input[type="button"]')
				.each(function(i, item){
					var name = $(item).attr('name');
					$(item).click(function(){
						if(name == 'addBtn'){
							_that.addRow(tab, wid);
						} else if (name == 'delBtn'){
							_that.delRow(tab);
						}
					});
			});
			/*文本框设置数字验证监听*/
			$(tab).on('keyup', 'input[type="text"][name^="level"], input[type="text"][name="amount"]', 
				function(){
					var v = $(this).val(),
						n = $(this).attr('name');
					if(!fn.digits(v) || Number(v)<1) {
						if(/^(level)\S+/.test(n))
							$(this).val(0);/*判断是否是正整数*/
						else $(this).val(1);/*判断是否是正整数*/
					}
						
					/*刷新概述表*/
					if(n=='amount')
						RandomQues.sum.refresh();
					/*随机难度与其他难度的验证*/
					var ind = $(this).parent().parent().index(),
						row = $(tab).find('tr:eq(' + ind + ')'),
						amount_input = $(row).find('input[type="text"][name="amount"]'),
						amount = Number(amount_input.val()),
						amount_ = 0;
						$(row).find('input[type="text"][name^="level"]').each(function(i, o){
							amount_ += Number($(o).val());
						});
						if(amount_ > amount) $(amount_input).addClass('err2');
						else $(amount_input).removeClass('err2');
			});
			/*绑定题目类型选择事件(已有数据不进行编辑)
			$(tab).find('tr input[name="quesCateName"]').each(function(){
				$(this).click(function(){
					_that.choose.execute(this, tab);
				});
			});*/
			
			/*全选*/
			$(tab).find('tr:eq(0) input[type="checkbox"]').click(function(){
				var checked = $(this).attr('checked');
				if(checked == 'checked') 
					$(tab).find('tr input[type="checkbox"]').attr('checked', checked);
				else $(tab).find('tr input[type="checkbox"]').removeAttr('checked', checked);
			});
		}
	},
	
	/*表单提交*/
	from: {
		score_: 0,
		data: null,/*表单数据*/
		/*表单数据验证*/
		check: function(){
			var _that = this, data = {};
			var setResult = RandomQues.set.getData(true); /*题型设置(不进行数据验证)*/
				data.set = setResult.data;/*题型设置数据*/
			var reqResult = RandomQues.req.getData(); /*必选题*/
			if(reqResult.rst){
				data.req = reqResult.data;/*必选题数据*/
				var ruleResult = RandomQues.rule.getData();/*随机题规则*/
				if(ruleResult.rst) {
					data.rule = ruleResult.data;/*随机题规则数据*/
					var sumResult = RandomQues.sum.getData();/*概述数据验证*/
					/*相关数据*/
					_that.score_ = sumResult.score;/*实际分数*/
					_that.data = data;/*实际实际数据*/
					return sumResult.rst;
				}
			}
			return false;
		},
		/*表单提交*/
		post: function(paperId, data){
			var load_index = layer.load('提交中…');
			$.post($.fn.getRootPath() + "/app/learning/paper!saveRandomQues.htm",
				{data: JSON.stringify(data), paperId: paperId},
				function(jsonResult){
					var msg_inx = layer.alert(jsonResult.msg, -1, function(){
						layer.close(msg_inx);
						layer.close(load_index);
						if(jsonResult.rst){/*操作成功*/
							parent.pageClick();//刷新当前页面
							parent.layer.closeAll();/*关闭弹窗*/
						}
					});
			}, 'json');
		},
		/*表单提交*/
		submit: function(){
			var _that = this, 
				validate = _that.check();/*表单数据验证*/
			/*表单数据验证成功*/
			if(validate){
				var paperId = _PaperId, /*试卷id*/
					score = _PaperScore,/*试卷总分*/
					score_ = _that.score_, /*实际分数*/
					data = _that.data;/*实际实际数据*/
				if(paperId && score){
					if(score == score_){/*总分与实际分数相等*/
						_that.post(paperId, data);
					} else {/*总分与实际分数不等*/
						var msg_inx = layer.alert('总分(' + score + ')与实际分数(' + score_ + ')不等，试卷将被置为“停用”状态', 
							-1, function(){
								layer.close(msg_inx);
								_that.post(paperId, data);
						});
					}
				} else {
					layer.alert('数据异常!', -1);
				}
			}
		},
		init: function(){
			var _that = this;
			/*保存*/
			$('#saveBtn').click(function(){
				_that.submit();
			});
		}
	},
	
	/*初始化*/
	init: function(){
		var _that = this;
		_that.set.init();/*题目设置*/
		_that.req.init();/*必选题*/
		_that.rule.init();/*随机题规则*/
		_that.from.init();/*表单提交*/
		
		/*下一步*/
		$('#nextBtn').click(function(){
			var setResult = _that.set.getData(), 
				data = setResult['data'];
			if(setResult.rst){
				/*试题类型*/
				var quesTypes = {};
				for(var i = 0; i < data.length; i++){
					var key = data[i].key;
					quesTypes[key] = _QuesTypes[key];
				}
				_that.fn.setQuesTypes(quesTypes);
				
				if(data && data.length > 0){
					$('tr[flag="next"], a[flag="next"]').css({display: ''});
					$('tr[flag="prev"], a[flag="prev"]').css({display: 'none'});
					_that.sum.init(data);
				} else {
					layer.alert('题目选择不应该为空!', -1);
					return false;
				}
			}
		});
		/*上一步*/
		$('#prevBtn').click(function(){
			$('tr[flag="next"], a[flag="next"]').css({display: 'none'});
			$('tr[flag="prev"], a[flag="prev"]').css({display: ''});
		});
	}
};

/*初始化*/
$(function(){
	/*html渲染*/
	$('tr[flag="next"], a[flag="next"]').css({display: 'none'});
	$('tr[flag="prev"], a[flag="prev"]').css({display: ''});
	/*js渲染*/
	RandomQues.init();
});