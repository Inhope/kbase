/**
 * 列表事件渲染
 * @author Gassol.Bi
 * @modified 2016-01-12 11:41:21
 */
PageUtils = {
	fn: {
		/*列表勾选标示*/
		ckAllId: '',
		ckName: '',
		/*分页跳转*/
		page: function(pageNo){
			alert('请复写function(pageNo){}方法!');
		},
		
		/*有弹窗*/
		shade: function(options){
			var _that = this, 
				before = options.before;
			/*事件不存在，或者事件存在但是返回true*/
			if(!before || before && before.call(_that, options)){
				var h = options.height, w = options.width;
				var	title = options.title, url = options.url, params = options.params;
				if(params){
					url += '?'; 
					for(var key in params){
						url += '&' + key + '=' + params[key];
					}
				}
				if(!h) h = '400px';
				if(!w) w = '700px';
				$.layer({
					type : 2,
					border : [ 2, 0.3, '#000' ],
					title : [ title, 'font-size:14px;font-weight:bold;' ],
					closeBtn : [ 0, true ],
					iframe : {
						src : $.fn.getRootPath() + url
					},
					area : [ w, h ]
				});
			}
			
		},
		/*无弹窗*/
		submit: function(options){
			var _that = this;
			var title = options.title, url = options.url, 
				params = options.params, before = options.before;
			/*事件不存在，或者事件存在但是返回true*/
			if(!before || before && before.call(_that)){
				layer.confirm("确定要" + title + "选中的数据吗？", function(){
					var loadindex = layer.load('提交中…');
					$.post($.fn.getRootPath() + url, params, 
						function(jsonResult) {
							layer.close(loadindex);
							loadindex = layer.alert(jsonResult.msg, -1, function(){
								layer.close(loadindex);
								if (jsonResult.rst) _that.page('1');
							});
					}, "json");
				});
			}
		},
		
		
		/*获取选中的行*/
		getChecked: function(){
			var ids = new Array();
			$('input:checkbox[name="' + this.ckName + '"]:checked').each(function(){
				ids.push($(this).val());
			});
			return ids;
		},
		/*获取单行选中的行*/
		getSingleChecked: function(){
			var _that = this, 
				ids = _that.getChecked();
			if(ids.length == 0) {
				layer.alert("请选择要操作的数据!", -1);
			} else if(ids.length > 1) {
				layer.alert("不能同时操作多条数据!", -1);
			} else {
				return ids;
			}
			return false;
		},
		/*获取多行行选中的行*/
		getMultiChecked: function(){
			var _that = this, 
				ids = _that.getChecked();
			if(ids == 0) {
				layer.alert("请选择要操作的数据!", -1);
			} else {
				return ids;
			}
			return false;
		},
		
		
		/*有弹窗，无选择*/
		fn0: function(option){
			this.shade(option);
		},
		/*有弹窗，单选择*/
		fn1: function(option){
			var _that = this, 
				ids = this.getSingleChecked();
			if(ids){
				if(option.params) option.params.ids = ids.join(',');
				else option.params = {'ids': ids.join(',')};
				_that.shade(option);
			}
		},
		/*无弹窗，多选择*/
		fn2: function(option){
			var _that = this, 
				ids = _that.getMultiChecked();
			if(ids){
				if(option.params) option.params.ids = ids.join(',');
				else option.params = {'ids': ids.join(',')};
				_that.submit(option);
			}
		},
		/*查询条件初始化*/
		reset: function(){
			$("input[id$='_select']").each(function() {
				$(this).attr('value', '');
			});
			$("select[id$='_select']").each(function() {
				$(this)[0].selectedIndex = '';
			});
		},
		/*列表初始化*/
		pageInit: function(){
			this.page('1');
		}
	},
	

	btns: {},/*功能按钮*/
	init: function(options){
		var _that = this;/*参数设置*/
		/*参数设置*/
		if(options) _that.setting(options);
		if(options.ckAllId) _that.fn.ckAllId = options.ckAllId;
		if(options.ckName) _that.fn.ckName = options.ckName;
		if(options.page) _that.fn.page = options.page;
		/*绑定全选事件*/
		var ckAllId = _that.fn.ckAllId, 
			ckName = _that.fn.ckName;
		$('#' + ckAllId).click(function(){
			var checked = $(this).attr('checked');
			if(checked == 'checked')
				$('input[type="checkbox"][name="' + ckName + '"]').attr('checked', checked);
			else $('input[type="checkbox"][name="' + ckName + '"]').removeAttr('checked', checked);
		});
	},
	setting: function(options){
		var _that = this;
		if(options.btns) _that.btns = options.btns;
		/*绑定按钮事件*/
		$.each(this.btns, function(i, item){/*按钮信息*/
			/*按钮id,名称,方法名称(或者方法)*/
			var id = item.id, name = item.name, fn = item.fn;
			if(id){/*按钮id*/
				var obj = $('#' + id);/*按钮*/
				if (typeof(fn) != 'function') {/*方法名称*/
					$(obj).click(function(){
						$(_that.fn).attr(fn).call(_that.fn, item);
					});
				} else {/*方法*/
					$(obj).click(function(){
						fn.call(_that, item, obj);
					});
				}
			} else if(name){/*按钮名称*/
				$('a[name="' + name + '"], input[name="' + name + '"]')
					.each(function(i, obj){/*按钮*/
						var item_ = $.extend(true, {}, item);/*复制(深度)按钮信息*/
						var params = $(obj).attr('params');/*其他参数*/
						if(params) item_.params = $.extend({}, 
							item_.params, eval('(' + params + ')'));/*合并其他条件*/
						if (typeof(fn) != 'function' ) {/*方法名称*/
							$(obj).click(function(){
								$(_that.fn).attr(fn).call(_that.fn, item_);
							});
						} else {/*方法*/
							$(obj).click(function(){
								fn.call(_that, item, obj);
							});
						}
				});
			}
		});
	}
};
 