/**
 *考卷类型
 */
$(function(){
	var paperCate = {
		element:{
			addBtn:$("#addBtn"),
			updateBtn:$("#updateBtn"),
			delBtn:$("#delBtn"),
			//查询输入框
			queryInput:$("#queryInput"),
			//新增、修改窗口
			addOrUpdateWin:$("#addOrUpdateWin"),
			//新增、修改窗口的保存按钮
			winSaveBtn:$("#winSaveBtn"),
			winId:$("#winId"),
			winName:$("#winName"),
			winRemark:$("#winRemark"),
		},
	//控件加载
		init:function(){
			$.fn.zTree.init($("#paperCateTree"), paperCate.config.ztreeSetting, paperCate.getTreeData());
			paperCate.bindEven();
		},
	//获取考卷类型数据
		getTreeData:function(){
			var zNodes = [];
			$.ajax({
				url : $.fn.getRootPath() + '/app/learning/paper-cate!list.htm',
				type : 'POST',
				dataType : 'json',
				async : false,
				success : function(result){
					if(result.success){
						zNodes = result.data;
					}
				}
			});
				return zNodes;
		},
		//添加(执行)
		doAddCate:function(){
			//将选中节点做为父节点，并获取父节点信息
			var parentId = null;
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var checkedNodes = zTree.getCheckedNodes();
			var checkedTreeNode = checkedNodes[0];
			if (checkedTreeNode) {
				parentId = checkedTreeNode.id;
			}
				
				
			var paperCateBean = {};
			paperCateBean.name = paperCate.element.winName.val();
			paperCateBean.remark = paperCate.element.winRemark.val();
			paperCateBean.parentId = parentId;
			
			//数据验证
			if(!paperCateBean.name || paperCateBean.name === ""){
				paperCate.element.winName.focus();
				$("#nameValid").show();
				return;
			}else{
				$("#nameValid").hide();
			}
			
			$.ajax({
				url : $.fn.getRootPath() + '/app/learning/paper-cate!addOrEditDo.htm',
				type : 'POST',
				dataType : 'json',
				data : paperCateBean,
				success : function(result){
					if(result.success){
						paperCate.closeWin();
						//修改节点name
						var treeNode = result.data;
						paperCate.addTreeNode(treeNode);
					}
				}
			});
		},
		//添加（进入添加页面）
		inAddCate:function(){
			paperCate.element.addOrUpdateWin.show();
			//设定操作窗口状态 
			paperCate.config.winOperate = 0;
		},
		//修改(执行)
		doUpdateCate:function(){
			//获取选中节点和父节点id
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var checkedNodes = zTree.getCheckedNodes();
			var checkedTreeNode = checkedNodes[0];
			
			var paperCateBean = {};
			paperCateBean.id = paperCate.element.winId.val();
			paperCateBean.name = paperCate.element.winName.val();
			paperCateBean.remark = paperCate.element.winRemark.val();
			
			//数据验证
			if(!paperCateBean.name || paperCateBean.name === ""){
				paperCate.element.winName.focus();
				$("#nameValid").show();
				return;
			}else{
				$("#nameValid").hide();
			}
			
			$.ajax({
				url : $.fn.getRootPath() + '/app/learning/paper-cate!addOrEditDo.htm',
				type : 'POST',
				dataType : 'json',
				data : paperCateBean,
				success : function(result){
					if(result.success){
						paperCate.closeWin();
						//修改选择节点信息
						checkedTreeNode.id = result.data.id;
						checkedTreeNode.name = result.data.name;
						checkedTreeNode.remark = result.data.remark;
						zTree.updateNode(checkedTreeNode,false);
					}
				}
			});
		},
		//修改（进入修改页面）
		inUpdateCate:function(){
			//设定操作窗口状态 
			paperCate.config.winOperate = 1;
			//获得选中节点id
			var id = null;
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var checkedNodes = zTree.getCheckedNodes();
			var checkedTreeNode = checkedNodes[0];
			if (checkedNodes.length == 0) {
				parent.layer.alert('请先选择一个节点');
				return;
			}else{
				id = checkedTreeNode.id;
			}
				
			$.ajax({
				url : $.fn.getRootPath() + '/app/learning/paper-cate!getPaperCate.htm',
				type : 'POST',
				dataType : 'json',
				data : {id : id},
				success : function(result){
					if(result.success){
						paperCate.element.winId.val(result.data.id);
						paperCate.element.winName.val(result.data.name);
						paperCate.element.winRemark.val(result.data.remark);
						paperCate.element.addOrUpdateWin.show();
					}
				}
			});
		},
		//删除(执行)，将选中的节点进行删除,并联动删除子节点
		doDelCate:function(){
			//获得id
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var nodes = zTree.getCheckedNodes();
			var treeNode = nodes[0];
			if (nodes.length == 0) {
				parent.layer.alert('请先选择一个节点');
				return;
			}
				
			//确认是否执行删除操作
			parent.layer.confirm('确定删除?', function(index){
				//获取选中的节点
				$.ajax({
					url : $.fn.getRootPath() + '/app/learning/paper-cate!delete.htm',
					type : 'POST',
					dataType : 'json',
					data:{id:treeNode.id},
					success : function(result){
						parent.layer.close(index);
						if(result.success){
							zTree.removeNode(treeNode);
						}
					},
					error:function(){
						parent.layer.close(index);
					}
				});
			});
		},
		closeWin:function(){
			paperCate.element.winId.val("");
			paperCate.element.winName.val("");
			paperCate.element.winRemark.val("");
			paperCate.element.addOrUpdateWin.hide();
		},
		addTreeNode:function(newNode){
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var treeNode = zTree.getCheckedNodes()[0];
			zTree.addNodes(treeNode ? treeNode : null, newNode);
		},
		delTreeNode:function(){
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			var nodes = zTree.getCheckedNodes();
			var treeNode = nodes[0];
			if (nodes.length == 0) {
				parent.layer.alert('请先选择一个节点');
				return;
			}
			zTree.removeNode(treeNode, false);
		},
		expandParentNode:function(treeNode){
			var parentNode = treeNode.getParentNode();
			if(parentNode){
				//递归回掉
				paperCate.expandParentNode(parentNode);
			}
			//展开此treeNode节点
			if(!treeNode.open){
				$.fn.zTree.getZTreeObj("paperCateTree").expandNode(treeNode,true,false,false);
			}
		},
		//查询，将匹配的结果选中,高亮
		searchCate:function(e){
			var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
			//将原有查询的nodes去掉高亮和选中状态
			var checkNodes = zTree.getCheckedNodes();
			for( var i=0, l=checkNodes.length; i<l; i++) {
				checkNodes[i].highlight = false;
				checkNodes[i].checked = false;
				zTree.updateNode(checkNodes[i]);
			}
			
			//获取查询值
			var searchValue = $.trim(paperCate.element.queryInput.val());
			if(searchValue && searchValue != ""){
				//匹配node
				var nodeList = zTree.getNodesByParamFuzzy("name", searchValue);
				//将匹配node选中,高亮
				for( var i=0, l=nodeList.length; i<l; i++) {
					//在高亮之前需要递归展开父节点
					paperCate.expandParentNode(nodeList[i]);
					//选中,高亮
					nodeList[i].highlight = true;
					nodeList[i].checked = true;
					zTree.updateNode(nodeList[i]);
				}
			}
			
		},
		//事件绑定
		bindEven:function(){
			paperCate.element.queryInput.bind("propertychange", paperCate.searchCate).bind("input", paperCate.searchCate);
			paperCate.element.addBtn.bind("click",paperCate.inAddCate);
			paperCate.element.updateBtn.bind("click",paperCate.inUpdateCate);
			paperCate.element.delBtn.bind("click",paperCate.doDelCate);
			paperCate.element.winSaveBtn.bind("click",function(){
				//根据窗口状态，选择操作
				if(paperCate.config.winOperate === 0){
					paperCate.doAddCate();
				}else if(paperCate.config.winOperate === 1){
					paperCate.doUpdateCate();
				}
			});
		},
		config:{
			//窗口确定操作状态：保存 0/编辑 1
			winOperate:0,
			//控件配置
			ztreeSetting : {
				edit: {
					drag:{
						isCopy:false,
						prev:false,
						next:false
					},
					enable : true,
					showRemoveBtn: false,
					showRenameBtn: false
				},
				data: {
					key: {
						title: "name"
					},
					simpleData: {
						enable: true,
						pIdKey:	"parentId",
					}				
				},
				view: {
					fontCss:function(treeId, treeNode){
						return (!!treeNode.highlight) ? {color:"#A60000", "font-weight":"bold"} : {color:"#333", "font-weight":"normal"};
					},
					showIcon:false
				},
				check:{
					enable: true,
					chkboxType: { "Y": "s", "N": "ps" },
					autoCheckTrigger:true
				},
				callback:{
					onClick:function (e, treeId, treeNode){
							alert("DD");
							$("#btnReset").click();
							$("#btnSubm").click();
						},
					onCheck:function(event, treeId, treeNode) {
						var zTree = $.fn.zTree.getZTreeObj("paperCateTree");
						//选中则高亮，反之不高亮
						if(treeNode.checked){
							treeNode.highlight = true;
							zTree.updateNode(treeNode,false);
						}else{
							treeNode.highlight = false;
							zTree.updateNode(treeNode,false);
						}
					},
					onDrop:function(event, treeId, treeNodes, targetNode, moveType){
						//如果拖拽改变了父子级关系，则需要修改数据库数据
						if(moveType === "inner"){
							var id = treeNodes[0].id;
							var parentId = "";
							if(targetNode){
								parentId = targetNode.id;
							}
							
							$.ajax({
								url : $.fn.getRootPath() + '/app/learning/paper-cate!updateRelation.htm',
								type : 'POST',
								dataType : 'json',
								data:{id:id,parentId:parentId},
								success : function(result){
								}
							});
						}
					}
				}
			}
		}
	};
	paperCate.init();
});

		
	
