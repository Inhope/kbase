var TreeUtils = {
	options: {},/*设置*/
	setting: function(options){
		this.options = options;
	}
};
$(function(){
	var initTree = {
		/*有弹窗*/
		openShade: function(options){
			var h = options.height, w = options.width,
				title = options.title, url = options.url + '?1=1';
			var params = options.params;
			if(params){
				for(var key in params){
					url += '&' + key + '=' + params[key];
				}
			}
			
			if(!h) h = '400px';
			if(!w) w = '700px';
			$.layer({
				type : 2,
				border : [ 2, 0.3, '#000' ],
				title : [ title, 'font-size:14px;font-weight:bold;' ],
				closeBtn : [ 0, true ],
				iframe : {
					src : $.fn.getRootPath() + url
				},
				area : [ w, h ]
			});
		},
		/*无弹窗*/
		noShade: function(options){
			var title = options.title, url = options.url, 
				params = options.params;
			
			layer.confirm("确定要" + title + "选中的数据吗？", function(){
				var loadindex = layer.load('提交中…');
				$.post($.fn.getRootPath() + url, params, 
					function(jsonResult) {
						layer.alert(jsonResult.msg, -1, function(){
							layer.close(loadindex);
							if (jsonResult.rst) pageClick('1');
						});
				}, "json");
			});
		},
		
		
		
		/*获取选中的行*/
		_getSelected: function(){
			var ids = new Array();
			$('input:checkbox[name="' + this._listflag + '"]:checked').each(function(){
				ids.push($(this).val());
			});
			return ids;
		},
		/*获取单行选中的行*/
		getSingleSelected: function(){
			var ids = this._getSelected();
			if(ids.length == 0) {
				layer.alert("请选择要操作的数据!", -1);
			} else if(ids.length > 1) {
				layer.alert("不能同时操作多条数据!", -1);
			} else {
				return ids;
			}
			return false;
		},
		/*获取多行行选中的行*/
		getMultiSelected: function(){
			var ids = this._getSelected();
			if(ids == 0) {
				layer.alert("请选择要操作的数据!", -1);
			} else {
				return ids;
			}
			return false;
		},
		
		/*有弹窗，无选择*/
		fn0: function(option){
			this.openShade(option);
		},
		/*有弹窗，单选择*/
		fn1: function(option){
			var ids = this.getSingleChecked();
			if(ids){
				if(option.params) option.params.ids = ids.join(',');
				else option.params = {'ids': ids.join(',')};
				this.openShade(option);
			}
		},
		/*无弹窗，多选择*/
		fn2: function(option){
			var ids = this.getMultiChecked();
			if(ids){
				if(option.params) option.params.ids = ids.join(',');
				else option.params = {'ids': ids.join(',')};
				this.noShade(option);
			}
		},
		
		/*功能*/
		_btns:{},
		/*获取选中的行*/
		_getChecked: function(){
			var ids = new Array();
			$('input:checkbox[name="' + this._listflag + '"]:checked').each(function(){
				ids.push($(this).val());
			});
			return ids;
		},
		
		
		treeId: '',
		getZTreeObj: function(){
			return $.fn.zTree.getZTreeObj(this.treeId);
		},
		/*控件加载*/
		init:function(){
			$.fn.zTree.init($("#" + this.treeId), this.config.ztreeSetting);
		},
		
		/*初始化*/
		init: function(){
			this._listflag = ListTreeUtils.options.listflag;
			
			var _that = this;
			$.each(ListTreeUtils.options.btns, function(i, item){
				var id = item.id, fn = item.fn;
				if (typeof(fn) != "function" ) {
					$('#' + id).click(function(){
						$(_that).attr(fn).call(_that, item);
					});
				} else {
					$('#' + id).click(fn);
				}
			});
		}
	}
	/*初始化*/
	initTree.init();
});