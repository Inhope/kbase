$(function(){
	//出题参考
	$("#quesRefer").on("click",function(){
		parent.TABOBJECT.open({
			id : 'addQuesRefer',
			name : '出题参考',
			hasClose : true,
			url : $.fn.getRootPath()+"/app/learning/ques!referUI.htm",
			isRefresh : true
		}, this);
	});
	
	//设置
	$("#quesSetting").on("click",function(){
		var url = "/app/learning/ques!settingUI.htm";
		var title = "设置";
		parent.__kbs_layer_index_setting = parent.$.layer({
			type: 2,
			border: [2, 0.3, '#000'],
			title: [title, 'font-size:14px;font-weight:bold;'],
			closeBtn: [0, true],
			iframe: {src : $.fn.getRootPath()+url},
			area: ['500px', '300px']
		});
	});
	
	//新增
	$("#quesAdd").on("click",function(){
		var url = "/app/learning/ques!addUI.htm";
		var title = "新增题目";
		__kbs_layer_index_one = $.layer({
			type: 2,
			border: [2, 0.3, '#000'],
			title: [title, 'font-size:14px;font-weight:bold;'],
			closeBtn: [0, true],
			iframe: {src : $.fn.getRootPath()+url},
			area: ['650px', '100%'],
			end:function(){
				loadForm();
			}
		});
	});
	
	//编辑
	$("#quesEdit").on("click",function(){
		var _quesIds = $("[name='quesId']:checked");
		if(_quesIds.length != 1){
			layer.alert("请选择一个题目", -1);
			return false;
		}
		var url = "/app/learning/ques!updateUI.htm?id="+$(_quesIds).val();
		var title = "编辑题目";
		__kbs_layer_index_update = $.layer({
			type: 2,
			border: [2, 0.3, '#000'],
			title: [title, 'font-size:14px;font-weight:bold;'],
			closeBtn: [0, true],
			iframe: {src : $.fn.getRootPath()+url},
			area: ['650px', '100%'],
			end:function(){
				loadForm();
			}
		});
	});
	
	//删除
	$("#quesDel").on("click",function(){
		var _quesIds = [];
		if($("[name='quesId']:checked").length < 1){
			layer.alert("请选择题目", -1);
			return false;
		}
		$("[name='quesId']:checked").each(function(){
			_quesIds.push($(this).val());
		});
		var url = $.fn.getRootPath()+"/app/learning/ques!delete.htm";
		var params = "id="+_quesIds;
		layer.confirm("确定要删除选中的数据吗？",function(){
			//判断是否被试卷引用过
			$.ajax({
				type: "POST",
				url: $.fn.getRootPath()+"/app/learning/ques!checkQuesUsed.htm",
				data: "ids="+_quesIds,
				dataType:"json",
				success: function(data) {
					if(data.status == 0){//无试卷引用该题目
						doDelete(url,params);
					}else if(data.status == 1){//试卷引用该题目
						layer.confirm(data.message+"，此操作有可能对在线考试造成影响，确定删除吗？",function(){
							doDelete(url,params);
						});
					}else if(data.status == 2){//系统出错
						layer.alert(data.message,-1);
					}
				}
			});
		});
	});
	
	//启用-停用
	$("#quesStart,#quesStop").on("click",function(){
		var _quesIds = [];
		if($("[name='quesId']:checked").length < 1){
			layer.alert("请选择题目", -1);
			return false;
		}
		$("[name='quesId']:checked").each(function(){
			_quesIds.push($(this).val());
		});
		var _status = "0";
		var _title = "";
		if($(this).attr("id") == 'quesStart'){
			_status = "0";
			_title = "启用";
		}else{
			_status = "1";
			_title = "停用";
		}
		var url = $.fn.getRootPath()+"/app/learning/ques!updateStatus.htm";
		var params = "id="+_quesIds+"&status="+_status;
		layer.confirm("确定要"+_title+"选中的数据吗？",function(){
			if($(this).attr("id") == "quesStop"){
				//判断是否被试卷引用过
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/ques!checkQuesUsed.htm",
					data: "ids="+_quesIds,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){//无试卷引用该题目
							doUpdateStatus(url,params);
						}else if(data.status == 1){//试卷引用该题目
							layer.confirm(data.message+"，此操作有可能对在线考试造成影响，确定停用吗？",function(){
								doUpdateStatus(url,params);
							});
						}else if(data.status == 2){//系统出错
							layer.alert(data.message,-1);
						}
					}
				});
			}else{
				doUpdateStatus(url,params);
			}
		});
	});
	
	//屏蔽过期数据
	$("#isShowOff").on("click",function(){
		loadForm();
	});
	
	//预览
	$("[class='gonggao_con_nr']").on("click","[name='quesInfo']",function(){
		var _id = $(this).attr("_id");
		parent.__kbs_layer_index_info = parent.$.layer({
			type: 2,
			border: [2, 0.3, '#000'],
			title: false,
			//title: ["预览", 'font-size:14px;font-weight:bold;'],
			//closeBtn: [0, true],
			iframe: {src : $.fn.getRootPath()+"/app/learning/ques!info.htm?id="+_id},
			area: ['700px', '350px']
		});
	});
	
	//题目移动到分类
	$("#moveToCate").on("click",function(){
		var ids = [];
		$("[name='quesId']:checked").each(function(index,item){
			ids.push($(item).val());
		});
		if(ids.length == 0){
			layer.alert("请选择题目",-1);
			return false;
		}
		var _cateId = $("#quesCateId_select").val();
		if(_cateId == ""){
			layer.alert("请选择分类",-1);
			return false;
		}
		var _url = $.fn.getRootPath()+"/app/learning/ques!moveToCate.htm";
		$.ajax({
			type: "POST",
			url: _url,
			data: "cateId="+_cateId+"&ids="+ids,
			success: function(data) {
				data = jQuery.parseJSON(data);
				if(data.success){
					layer.alert(data.message,-1);
					loadForm();
				}
			},
			error: function(msg){
				alert(msg);
			}
		});
	});
	
	//重置查询条件
	$("#selectReset").on("click",function(){
		$("#createUser").val("");
		$("#startDate").val("");
		$("#endDate").val("");
		$("#quesContent").val("");
		$("#kbValue").val("");
		$("#type option:first").attr("selected","selected");
		$("#level option:first").attr("selected","selected");
		$("#status option:first").attr("selected","selected");
		$("#isShowOff").removeAttr("checked");
	});
	
	//加载数据表单函数
	loadForm = function(){
		$('body').ajaxLoading('正在查询数据...');
		var isShowOff;
		if($("#isShowOff").is(":checked")){
			isShowOff = 1;
		}else{
			isShowOff = 0;
		}
		var param = {
			pageNo:$("#pageNo").val(),
			content:$("#quesContent").val(),
			kbValue:$("#kbValue").val(),
			startDate:$("#startDate").val(),
			endDate:$("#endDate").val(),
			quesTypeId:$("#type").val(),
			quesCateId:$("#quesCateId").val(),
			level:$("#level").val(),
			status:$("#status").val(),
			isShowOff:isShowOff
		};
		$.ajax({
			type: "POST",
			url: $.fn.getRootPath()+"/app/learning/ques!getQuesListJson.htm",
			data: param,
			success: function(htmlResult) {
				var tab = $('#quesList');
	        	$(tab).find("tr:not(:first)").remove();
	        	$(tab).find("tr:first").after($(htmlResult).find('tr'));
	        	$('body').ajaxLoadEnd();
			}
		});
	}
	
	//点击查询按钮
	$("#submitForm").on("click",function(){
		$("#pageNo").val(1);//重置页码
		loadForm();
	});
	
	//默认加载数据表单
	$("#submitForm").click();
	
	//导入题目
	$("#quesImport").on("click",function(){
		$.layer({
			type: 1,
		    title: false,//["测试", 'font-size:14px;font-weight:bold;'],
		    area: ['400', '300'],
		    border: [5, 0.3, '#000'], //去掉默认边框
		    shade: [0], //去掉遮罩
		    //closeBtn: [0, false], //去掉默认关闭按钮
		    shift: 'left', //从左动画弹出
		    page: {
		        dom: '#ImportShade'
		    }
		});
	});
	//导出题目
	$("#quesExport").on("click",function(){
		$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/ques!exportQues.htm");
		$("#form0").submit();
	});
	
	//全选框操作
	$("#checkAll").on("click",function(){
		if($(this).is(":checked")){
			$("#quesList [type='checkbox']").each(function(){
				$(this).attr("checked", true)
			});
		}else{
			$("#quesList [type='checkbox']").each(function(){
				$(this).attr("checked", false)
			});
		}
	});
	
});
//执行删除
function doDelete(url,data){
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		success: function(data) {
			data = jQuery.parseJSON(data);
			if(data.status == 1){
				layer.alert(data.message,-1);
				loadForm();
			}
		},
		error: function(msg){
			alert(msg);
		}
	});
}
//执行更新题目状态 
function doUpdateStatus(url,data){
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		success: function(data) {
			data = jQuery.parseJSON(data);
			if(data.status == 1){
				layer.alert(data.message,-1);
				loadForm();
			}
		},
		error: function(msg){
			layer.alert(msg,-1);
		}
	});
}
//分页查询
function pageClick(pageNo){
	$("#pageNo").val(pageNo);
	loadForm();
}