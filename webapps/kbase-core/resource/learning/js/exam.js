//预览试卷
function showPaperInfo(paper_id,paper_type){
	if(paper_type ==0){
		var title = "试卷信息";
		var url = $.fn.getRootPath()+'/app/learning/exam!previewPaper.htm?paperId='+paper_id;
		parent.__kbs_layer_index = parent.$.layer({
			type: 2,
			border: [2, 0.3, '#000'],
			title: [title, 'font-size:14px;font-weight:bold;'],
			closeBtn: [0, true],
			iframe: {src :url},
			area: ['80%', '90%']
		});
	}else{
		parent.layer.alert('不可预览随机试卷!', -1);
	}
}

//全选反选
function checkAll(){
	$("input:checkbox[name='exam_id']").each(function(){
		$(this).attr("checked",$(this).attr("checked")=='checked'?false:true);
	});
}

function selectReset(){
	$('#start_time').val('');
	$('#end_time').val('');
	$("#exam_name").val('');
	$("#paper_name").val('');
	$("#status option:first").attr("selected","selected");
}

//查询
function pageClick(pageNo){
	$(document).ajaxLoading('处理数据中...');
	//$("#form0").attr("action", $.fn.getRootPath()+"/app/learning/exam.htm");
	$("#pageNo").val(pageNo);
	//$("#form0").submit();
	var params = $("#form0").serialize();
	$.ajax({
			url : $.fn.getRootPath() + '/app/learning/exam!examlist.htm?'+params,
			type : 'POST',
			sync : false,
			dataType : 'html',
			success : function(html) {
				$(document).ajaxLoadEnd();
				$('#content').html(""); 
				$('#content').html(html);
			}
		});
}

//预览
function topreview(id){
	var title = "预览";
	var url = $.fn.getRootPath()+'/app/learning/exam!preview.htm?id='+id;
	opshade(url,title);
};

$(function(){
  //培训分类div中的tree
  var cateTreeObject = {
		cateEl : $('#examCateNames_select'),
		ktreeEl : $('div.cateContent'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/learning/exam-cate!examcatetree.htm",
				autoParam : ["id"]
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					cateTreeObject.cateEl.val(treeNode.name);
					$('#examCateIds_select').val(treeNode.id);
				}
			}
		},
		render : function() {
			var self = this;
			self.cateEl.val('');
			self.cateEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.cateEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			self.cateEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$('#examCateIds_select').val('');
				} else if(keyArg.keyCode == 13) {
					self.cateEl.is(':hidden') || self.cateEl.hide();
				}
			});
			$.fn.zTree.init($('ul#examcatetree'), this.treeAttr );
		}
	}
    cateTreeObject.render();
	$(document).on('click','#toadd',function(){
		var title = "新增考试";
		var url = $.fn.getRootPath()+'/app/learning/exam!toaddorudate.htm';
		opshade(url,title);
	});
	
	$(document).on('click','#export',function(){
		var params = $("#form0").serialize();
		$(document).ajaxLoading('处理数据中...');
		$.ajax({
			url : $.fn.getRootPath() + '/app/learning/exam!processData.htm?'+params,
			type : 'POST',
			sync : false,
			dataType : 'json',
			success : function(data) {
				$(document).ajaxLoadEnd(); 
				if(data.success) {
					var iframe = document.createElement("iframe");
		            iframe.src = $.fn.getRootPath()+"/app/learning/exam!downLoad.htm?fileName=" + data.message;
		            iframe.style.display = "none";
		            document.body.appendChild(iframe);
				} else {
					parent.layer.alert(data.message, -1);
				}
			},
			error : function(data, textStatus, errorThrown) {
				$(document).ajaxLoadEnd();
				parent.layer.alert('导出失败!', -1);
			}
		});
	});
	
	$(document).on('click','#todelete',function(){
		var checkids = [];
		var status = [];
		$("input:checkbox[name='exam_id']:checked").each(function(i, item){
			checkids.push($(this).val());
			status.push($(this).attr('status'));
		});
		if(checkids.length<=0 || checkids.length>1){
			layer.alert("请选择一项进行删除!", -1);
			return false;
		}if(status!='0'){
			layer.alert("只能删除考试未进行的!", -1);
			return false;
		}
		parent.layer.confirm("确定要删除吗？", function(index){
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/learning/exam!delexam.htm',
				data :{"id":checkids.join(',')},
				async: true,
				dataType : 'json',
				success : function(data){
					if(data.result == true){
						layer.alert("删除成功!", -1, function(){
							window.location.reload();
						});
					}else{
						layer.alert("删除失败!", -1);
					}
				},
				error : function(msg){
					layer.alert("网络错误，请稍后再试!", -1);
				}
			});
			parent.layer.close(index);
		});
	});
	
	$(document).on('click','#toedit',function(){
		var checkids = [];
		var status = [];
		$("input:checkbox[name='exam_id']:checked").each(function(i, item){
			checkids.push($(this).val());
			status.push($(this).attr('status'));
		});
		if(checkids.length<=0 || checkids.length>1){
			layer.alert("请选择一项进行编辑!", -1);
			return false;
		}
		if(status!='0'){
			layer.alert("只能编辑考试未进行的!", -1);
			return false;
		}
		var title = "编辑考试";
		var url = $.fn.getRootPath()+'/app/learning/exam!toaddorudate.htm?id='+checkids;
		opshade(url,title);
	});
	
	$(document).on('click','#tomove',function(){
		var checkids = [];
		$("input:checkbox[name='exam_id']:checked").each(function(i, item){
			checkids.push($(this).val());
		});
		if(checkids.length<=0){
			layer.alert("请选择一项进行移动!", -1);
			return false;
		}if($('#examCateIds_select').val()==''){
			layer.alert("请选择移动的分类!", -1);
			return false;
		}
		
		layer.confirm("确定要选中的数据移动至该分类吗？", function(){
			var loadindex = layer.load('提交中…');
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/learning/exam!moveexamtocate.htm', 
				data :{"ids":checkids.join(','),"cate_id":$('#examCateIds_select').val()},
				async: true,
				dataType : 'json',
				success : function(data){
					layer.close(loadindex);
					if(data.result){
						layer.alert("操作成功!", -1);
						window.location.reload();
					}else{
						layer.alert("操作失败!", -1);
					}
				},
				error : function(msg){
					layer.alert("网络错误，请稍后再试!", -1);
				}
			});
		});
	});
});

function opshade(url,title){
	parent.__kbs_layer_index = parent.$.layer({
		type: 2,
		border: [2, 0.3, '#000'],
		title: [title, 'font-size:14px;font-weight:bold;'],
		closeBtn: [0, true],
		iframe: {src :url},
		area: ['60%', '600px'],
		end:function(){
			var pageNo = $("#pageNo").val();
			if(title.indexOf("新增") > -1){
				pageClick(1);
			}else{
				pageClick(pageNo);
			}
		}
	});
}
function showdetailinfo(id){
	var title = "成绩管理";
	var url = $.fn.getRootPath()+'/app/learning/examinee!getExaminees.htm?examineeinfo.exam_id='+id;
	parent.__kbs_layer_index = parent.$.layer({
		type: 2,
		border: [2, 0.3, '#000'],
		title: [title, 'font-size:14px;font-weight:bold;'],
		closeBtn: [0, true],
		iframe: {src :url},
		area: ['90%', '90%']
	});
}
/*下拉形式显示div中的tree*/
function showCate(cateContent, inputName) {
	var cityObj = $("#"+inputName);
	var cityOffset = $(cityObj).offset();
	$("#"+cateContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

	$("body").bind("mousedown", function(event){
		onBodyDown(event, cateContent)
	});
}

function onBodyDown(event, cateContent) {
	if (!(event.target.id == cateContent || $(event.target).parents("#"+cateContent).length>0)) {
		hideCate(cateContent);
	}
}

function hideCate(cateContent) {
	$("#"+cateContent).fadeOut("fast");
	$("body").unbind("mousedown",function(event){
		onBodyDown(event, cateContent)
	});
}
/*下拉形式显示div中的tree*/