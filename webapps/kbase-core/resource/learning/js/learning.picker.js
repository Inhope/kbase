/**
 *
 *  使用方法：
 *		参数说明：
 *			传入一个对象
 *		属性说明：
 *			returnField	返回的字段，desc|id，两个字段采用竖线分割，第一个字段接收描述，第二个字段接收id
 *			diaWidth 对话框宽度
 *			diaHeight 对话框高度
 *			title 对话框标题，默认为false
 *		建议只传入returnField值，不传入title界面更好看
 *		$.kbase.picker.deptUser({returnField:"deptuserdesc|deptuserid"});
 */
;(function($){
	$.kbase = $.kbase || {};
	$.kbase.picker = $.kbase.picker || {};
	
	$.extend($.kbase.picker, {
		
		/**
		 * 试卷选择分类树(单选)
		 */
		singlePaperSelect: function(opts){
			opts.url = "/app/learning/paper!getPaperTree.htm?";
			_open(opts);
		},
		
		/**
		 * 考试分类选择(单选)
		 */
		singleExamCate: function(opts){
			opts.url = "/app/learning/exam-cate!examcateselect.htm?";
			_open(opts);
		},
		
		/**
		 * 课件分类选择(单选)
		 */
		singleCourseCate: function(opts){
			opts.url = "/app/learning/course-cate!singleCourseCate.htm?";
			_open(opts);
		},
		
		/**
		 * 课件知识来源(多选)
		 */
		multiRelatedQues: function(opts){
			opts.url = "/app/learning/course!multiRelatedQues.htm?";
			_open(opts);
		},
		
		/**
		 * 添加试题(多选)
		 */
		multiCourseQues: function(opts){
			opts.url = "/app/learning/course!multiCourseQues.htm?";
			_open(opts);
		},
		
		/**
		 * 培训绑定课件(多选)
		 */
		multiCourse: function(opts){
			opts.url = "/app/learning/train!multiCourse.htm?";
			_open(opts);
		},
		
		/**
		 * 培训绑定课件(单选)
		 */
		singleCourse: function(opts){
			opts.url = "/app/learning/train!singleCourse.htm?";
			_open(opts);
		},
		
		/**
		 * 培训分类选择(单选)
		 */
		singleTrainCate: function(opts){
			opts.url = "/app/learning/train-cate!singleTrainCate.htm?";
			_open(opts);
		},
		
		/**
		 * 试题分类选择(单选)
		 */
		singleQuesCate: function(opts){
			opts.url = "/app/learning/ques!singleQuesCate.htm?";
			_open(opts);
		},
		
		/**
		 * '试卷分类'选择
		 * 	使用：试卷选分类
		 * @author Gassol.Bi
		 * @modified 2016-1-8 11:41:21
		 */
		paperCate: function(opts){
			opts.url = '/app/learning/paper-cate!choose.htm';
			_open2(opts);
		},
		
		/**
		 * 按类型选择题目
		 * 	使用：试卷关联题目
		 * 	基本的参数:
		 * 		{key:题目类型， 
		 * 		initData：数据初始化方法名（eg:RandomQues.req.choose.initData）回掉参数key， 
		 * 		callback：回掉方法名     （eg:RandomQues.req.choose.callback）回掉参数参数key, data}
		 * 	其他参数： {chkStyle：树单选radio、多选checkbox
		 * 			diaWidth：弹窗宽度, diaHeight:弹窗高度 }
		 * @author Gassol.Bi
		 * @modified 2016-1-8 11:41:21
		 */
		quesByType: function(opts){
			opts.url = '/app/learning/paper!chooseQues.htm';
			_open2(opts);
		},
		
		/**
		 * 按分类选择试卷
		 * 	使用：考卷关联试卷
		 * 	基本的参数:
		 * 		{initData：数据初始化方法名（eg:CoilAddOrEdit.choose.initData）回掉参数参数无， 
		 * 		callback：回掉方法名（eg:CoilAddOrEdit.choose.callback）回掉参数参数data}
		 * 	其他参数： {diaWidth：弹窗宽度, diaHeight:弹窗高度 }
		 * @author Gassol.Bi
		 * @modified 2016-01-18 11:41:21
		 */
		paperByCate: function(opts){
			opts.url = '/app/learning/paper!chooseByCate.htm';
			_open2(opts);
		},
		
		
		//部门用户带搜索（多选）
		multiDeptusersearch: function(opts){
			opts.url = "/app/learning/train!deptusersearch.htm?";
			_open(opts);
		}
		
	});
	
	/**
	 * private function
	 */
	function _open(opts){
		var returnField = opts.returnField;
		var diaWidth = opts.diaWidth;
		var diaHeight = opts.diaHeight;
		var title = opts.title;
		var url = opts.url;
		returnField = returnField==undefined?"":returnField;
		diaWidth = diaWidth==undefined?540:diaWidth;
		diaHeight = diaHeight==undefined?340:diaHeight;
		title = title==undefined?false:title;
		if (url.length>0){
			window.__kbs_picker_index = $.layer({
				type:2, 
				border: [5, 0.3, '#000'], 
				title: title,
				closeBtn:[0, true],
				iframe:{src: $.fn.getRootPath() + url +"&returnField="+returnField}, 
				area:[diaWidth+"px", diaHeight+"px"]
			});
		}else{
			alert('[kbase] location error');
		}
		
	}
	
	/**
	 * 附加更多的参数
	 * @author Gassol.Bi
	  * @modified 2016-1-8 11:41:21
	 */
	function _open2(opts){
		var diaWidth = opts.diaWidth;
		var diaHeight = opts.diaHeight;
		var title = opts.title;
		var url = opts.url;
		diaWidth = diaWidth==undefined?540:diaWidth;
		diaHeight = diaHeight==undefined?340:diaHeight;
		title = title==undefined?false:title;
		if (url.length>0){
			/*绑定其他参数*/
			url += '?'
			for(var key in opts){ 
				if(key != 'diaWidth' && key != 'diaHeight' 
					&& key != 'title' && key != 'url') {
						url += '&' + key + '=' + opts[key];
					}
			}
			/*打开弹窗*/
			window.__kbs_picker_index = $.layer({
				type:2, 
				border: [5, 0.3, '#000'], 
				title: title,
				closeBtn:[0, true],
				iframe:{src: $.fn.getRootPath() + url}, 
				area:[diaWidth+"px", diaHeight+"px"]
			});
		}else{
			alert('[kbase] location error');
		}
	}
	
})(jQuery);