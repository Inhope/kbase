$(function(){
	var zTree;
	_setting = {
		edit: {
			enable: true,
			showRemoveBtn: false,
			showRenameBtn: false
		},
		view: {
			dblClickExpand: false
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			//右键点击事件
			onRightClick: function(e, treeId, treeNode){},
			//双击事件
			onDblClick: function(e, treeId, treeNode){},
			//点击事件
			onClick: function(e, treeId, treeNode){
				//初始查询参数
				$("#selectReset").click();
				$("#quesCateId").val(treeNode.id);
				$("#pageNo").val(1);
				loadForm();
			},
			//拖动事件
			onDrop:function(event, treeId, treeNodes, targetNode, moveType){
				//如果拖拽改变了父子级关系，则需要修改数据库数据
				if(moveType === "inner"){
					var id = zTree.getSelectedNodes()[0].id;
					var pId = "";
					if(targetNode){
						pId = targetNode.id;
					}
					$.ajax({
						url : $.fn.getRootPath() + '/app/learning/ques!updateRelation.htm',
						type : 'POST',
						dataType : 'json',
						data:{id:id,pId:pId},
						success : function(result){
						}
					});
				}else{
					layer.alert("不能移动节点跟顶级节点同级!", -1);
					return false;
				}
			}
		}
	};
	_loadTree = function(){
		$.ajax({
			type: "POST",
			dataType : 'json',
			url: $.fn.getRootPath()+ '/app/learning/ques!cateJson.htm',
			async: false,
			success: function(data){
				zTree=$.fn.zTree.init($("#treeDemo"), _setting, data);
			}
		});
	}
	//加载树
	_loadTree();
	//获取弹窗数据
	_getNode = function(){
		var id = $("#id").val();
		var name = $("#name").val();
		var pId = $("#pId").val();
		var pName = $("#pName").text();
		var remark = $("#remark").val();
		//if(pId=='root'){pId=''};
		return { 'id':id,'name':name,'pId':pId,'pName':pName,'remark':remark };
	}
	//执行新增、修改
	_saveTreeNode = function(newNode,node,flag){
		if(newNode.name == ''){
			layer.alert("节点名称不为空!", -1);
			return false;
		}
		
		//判断同级分类是否有重复的分类
		var node_p = zTree.getNodeByParam("id", newNode.pId, null);
		var node_ = zTree.getNodesByFilter(function(node){
			if(node.pId ==  newNode.pId && node.name == newNode.name){
				return true;
			}else{
				return false;
			}
		}, true, node_p);
		if(node_ != null && node_.id != newNode.id){
			layer.alert("同级分类下不应存在相同分类!", -1);
			return false;
		}
		
		$.ajax({
			type : 'post',
			url : $.fn.getRootPath() + '/app/learning/ques!saveCate.htm', 
			data : newNode,
			async: true,
			dataType : 'json',
			success : function(data){
				newNode = data.data;
				if(flag == "add"){
					zTree.addNodes(node, newNode);
				}else{
					_loadTree();
				} 
				layer.close(window.__kbs_layer_index);
				layer.alert("操作成功!", -1);
			},
			error : function(msg){
				newNode = null;
				layer.alert("网络错误，请稍后再试!", -1);
			}
		});
	}
	//点击新增
	$("#btnNewCate").click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			$('#pId').val(node.id);
			$('#pName').text(node.name);
			$('#id').val('');
			$('#name').val('');
			$('#remark').val('');
			//绑定新增事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				_saveTreeNode(_getNode(),node,"add");
			});
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: "新增",
			    area: ['auto', 'auto'],
				offset: ['100px','400px'],
			    border: [3, 0.3, '#000'], //默认边框
			    shade: [0.3 , '#000' , true], //遮罩
			    closeBtn: [0, true], //关闭按钮
			    //shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	
	//点击编辑
	$('#btnEditCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			if (node.id=='root'){
				layer.alert('无法编辑根节点', -1);
				return false;
			}
			$('#pId').val(node.pId);
			$('#pName').text(node.pName);
			$('#id').val(node.id);
			$('#name').val(node.name);
			$('#remark').val(node.remark);
			//绑定编辑事件
			$("#addOrEdt").unbind("click");
			$("#addOrEdt").bind("click",function(){
				_saveTreeNode(_getNode(),node,"edit");
			});
			window.__kbs_layer_index = $.layer({
				type: 1,
			    title: "修改",
			    area: ['auto', 'auto'],
				offset: ['100px','400px'],
			    border: [3, 0.3, '#000'], //默认边框
			    shade: [0.3 , '#000' , true], //遮罩
			    closeBtn: [0, true], //关闭按钮
			    //shift: 'left', //从左动画弹出
			    page: {
			        dom: '#rContent'
			    }
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
	//点击删除
	$('#btnDelCate').click(function(){
		var nodes = zTree.getSelectedNodes();
		if (nodes.length>0){
			var node = nodes[0];
			if (node.id=='root'){
				layer.alert('无法删除根节点', -1);
				return false;
			}
			var msg = "确定要删除选中的节点吗？";
			if (node.children && node.children.length > 0) {
				msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
			}
			layer.confirm(msg, function(index){
				$.ajax({
					type: "POST",
					url: $.fn.getRootPath()+"/app/learning/ques!cateHaveQues.htm",
					data: "id="+node.id,
					dataType:"json",
					success: function(data) {
						if(data.status == 0){//分类下无题目
							$.ajax({
								type : 'post',
								url : $.fn.getRootPath() + '/app/learning/ques!delCate.htm', 
								data : {'id': node.id},
								async: true,
								dataType : 'json',
								success : function(data){
									if(data.status == 1){
										zTree.removeNode(node);
									}
									layer.alert(data.message, -1);
									layer.close(index);
								},
								error : function(msg){
									newNode = null;
									layer.alert("网络错误，请稍后再试!", -1);
								}
							});
						}else if(data.status == 1){//分类下有题目
							layer.alert(data.message+"，不能删除!", -1);
						}
					}
				});
			});
		}else{
			layer.alert('请选择节点', -1);
		}
	});
});