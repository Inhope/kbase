/**
 * 考卷类型
 * @author Gassol.Bi
 * @modified 2016-01-12 11:41:21
 */
var TreeUtils = {
	options: {},/*设置*/
	setting: function(options){
		this.options = options;
	},
	/*获取树对象*/
	getZTreeObj: function(){
		return $.fn.zTree.getZTreeObj(this.options.treeId);
	}
};

$(function(){
	var TreeInit = {
		/*控件配置*/
		treeId: null,/*树id*/
		rootNodeId: null,/*树根节点id*/
		treeObj: null,/*树对象*/
		dragURL: null,/*树节点拖动请求地址*/
		treeSetting : {
			view: {
				selectedMulti: true
			},
			async: {
				enable: true,
				url: '',
				autoParam: ["id"]
			},
			data: {
				simpleData: {
					enable: true
				}				
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			callback:{
				onClick:function (e, treeId, treeNode){
					$('#btnReset').click();
					$('#paperCate_bh').val(treeNode.bh);
					$('#btnSubm').click();
				},
				beforeDrag: function (treeId, treeNodes){
					for (var i=0, l=treeNodes.length; i<l; i++) {
						if (treeNodes[i].id == TreeInit.rootNodeId) 
							return false;/*根节点不允许拖动*/
					}
					return true;
				},
				beforeDrop: function (treeId, treeNodes, targetNode){
					var node, treeObj = TreeInit.treeObj, 
						nodes = treeObj.getSelectedNodes();
					if (nodes.length == 1) {
						node = nodes[0];/*只允许拖动一个节点*/
						
						/*判断节点名称是否重复*/
						var node1 = treeObj.getNodesByFilter(function(nd){
							if(nd.pId == targetNode.id && nd.name == node.name) return true;
							return false;
						}, true, targetNode);
						/*同名岗位与当前需要编辑的岗位不是同一个*/
						if(node1 && node1.id != node.id){
							layer.alert('同级分类下不应存在相同名称的分类!', -1);
							return false;
						}
						/*发送请求，修改数据*/
						var loadindex = layer.load('提交中…');
						$.ajax({
							type : 'post',
							url : TreeInit.dragURL, 
							data : {'id': node.id, 'pId': targetNode.id},
							async : false,
							dataType : 'json',
							success : function(jsonResult){
								layer.close(loadindex);
								layer.alert(jsonResult.msg, -1, function(){
									if(jsonResult.rst) /*操作成功, 移动节点*/
									treeObj.moveNode(targetNode, node, "inner");
									layer.closeAll();
								});
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					} else {
						layer.alert("请勿同时拖动多个节点!", -1);
					}
					return false;
				}
			}
		},
		
		
		
		/*有弹窗*/
		shadeId: null,/*弹窗id*/
		beforeOpenShade: function(flag, node, treeObj){return false},
		openShade: function(options){
			var _that = this;
			var h = options.height, w = options.width, 
				title = options.title;
			if(!h) h = 'auto';
			if(!w) w = 'auto';
			$.layer({
				type : 1,
				border : [ 2, 0.3, '#000' ],
				offset: ['100px','400px'],
				title : [ title, 'font-size:14px;font-weight:bold;' ],
				closeBtn : [ 0, true ],
				page: {dom : $('#' + _that.shadeId)},
				area : [ w, h ]
			});
		},
		
		
		
		/*获取树对象*/
		getZTreeObj: function(){
			return $.fn.zTree.getZTreeObj(this.treeId);
		},
		/*获取选中的节点*/
		getSelected: function(){
			return this.getZTreeObj().getSelectedNodes();
		},
		/*获取单节点选中的节点*/
		getSingleSelected: function(){
			var nodes = this.getSelected();
			if(nodes.length == 0) {
				layer.alert("请选择要操作的节点!", -1);
			} else if(nodes.length > 1) {
				layer.alert("不能同时操作多个节点!", -1);
			} else {
				return nodes[0];
			}
			return false;
		},
		/*获取多节点节点选中的节点*/
		getMultiSelected: function(){
			var nodes = this.getSelected();
			if(nodes == 0) {
				layer.alert("请选择要操作的节点!", -1);
			} else {
				return nodes;
			}
			return false;
		},


		
		add: function(option){
			var nodes = this.getSingleSelected();
			if(nodes){
				if(option.params) option.params.nodes = nodes;
				else option.params = {'nodes': nodes};
				/*打开弹窗前操作*/
				if(this.beforeOpenShade('add', nodes, this.getZTreeObj())){
					this.openShade(option);
				} else {
					layer.alert("实现的beforeOpenShade方法需返回true!", -1);
				}
			}
		},
		edit: function(option){
			var nodes = this.getSingleSelected();
			if(nodes){
				if(option.params) option.params.nodes = nodes;
				else option.params = {'nodes': nodes};
				if(this.beforeOpenShade('edit', nodes, this.getZTreeObj())){
					this.openShade(option);
				} else {
					layer.alert("实现的beforeOpenShade方法需返回true!", -1);
				}
			}
		},
		drop: function(option){
			var _that = this, treeObj = _that.treeObj;
			/*需要删除的节点*/
			var nodes = this.getMultiSelected();
			if(nodes){
				var ids = new Array();
				$(nodes).each(function(i, obj){ids.push(obj.id);});
				if(option.params) option.params.ids = ids.join(',');
				else option.params = {'ids': ids.join(',')};
				ids = option.params.ids;/*删除节点的id*/
				
				/*删除操作*/
				var title = option.title, url = option.url, 
					params = option.params;
				layer.confirm("确定要" + title + "选中的节点吗？", function(){
					var loadindex = layer.load('提交中…');
					$.post($.fn.getRootPath() + url, params, 
						function(jsonResult) {
							layer.close(loadindex);
							layer.alert(jsonResult.msg, -1, function(){
								if(jsonResult.rst){
									/*获取被删除的节点*/
									var nodes = treeObj.getNodesByFilter(function(node){
										return (ids.indexOf(node.id)>-1);
									});
									/*删除节点*/
									$(nodes).each(function(index, obj){
										treeObj.removeNode(obj);
									});
								}
								layer.closeAll();
							});
					}, "json");
				});
			}
		},
		search: function(option){
			var keywords = $('#' + option.keywords).val(), i = 0;//搜索结果
			if(keywords){
				var ztreeObj = this.getZTreeObj();
				ztreeObj.getNodesByFilter(function(node){
					var pattern = new RegExp('.*' + keywords + '.*', 'g');
					if(pattern.test(node.name)){
						ztreeObj.selectNode(node, true);
						i++;
					} else {
						ztreeObj.cancelSelectedNode(node);
					}
					return null;
				});
			}
			$('#' + option.keywords)[0].focus();
			if(i == 0) layer.alert('未查询到相关分类!', -1);
		},
		
		
		
		/*控件加载*/
		init:function(){
			var _that = this;
			/*渲染树弹窗事件*/
			this.shadeId = TreeUtils.options.shadeId;
			this.beforeOpenShade = TreeUtils.options.beforeOpenShade;
			/*渲染树操作事件*/
			$.each(TreeUtils.options.btns, function(i, item){
				var id = item.id, fn = item.fn;
				if (typeof(fn) != "function" ) {
					$('#' + id).click(function(){
						$(_that).attr(fn).call(_that, item);
					});
				} else {
					$('#' + id).click(fn);
				}
			});
			/*渲染树*/
			this.treeId = TreeUtils.options.treeId;/*树id*/
			this.rootNodeId = TreeUtils.options.rootNodeId;/*树根节点id*/
			this.treeSetting.async.url = TreeUtils.options.treeURL;/*树数据初始化请求地址*/
			this.dragURL = TreeUtils.options.dragURL;/*树节点拖动请求地址*/
			if(!this.dragURL) _that.treeSetting.edit.enable = false;
			this.treeObj = $.fn.zTree.init($("#" + this.treeId), this.treeSetting);
			
		}
	};
	TreeInit.init();
});

		
	
