$(function(){


    //全选操作
	$("#checkAll").on('click', function(){
		if($(this).is(":checked")){
			$("input[type='checkbox']").each(function(){
				$(this).attr("checked", true)
			});
		}else{
			$("input[type='checkbox']").each(function(){
				$(this).attr("checked", false)
			});
		}
	});
    
	
});


Array.prototype.indexOf = function(item){
	for(var i=0,j; j=this[i]; i++){
		if(j == item){
			return i;
		}
	}
	return -1;
}


Array.prototype.contains = function (element) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == element) {
			return true;
		}
	}
	return false;
}




/*下拉形式显示div中的tree*/
function showCate(cateContent, inputName) {
	var cityObj = $("#"+inputName);
	var cityOffset = $(cityObj).offset();
	$("#"+cateContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

	$("body").bind("mousedown", function(event){
		onBodyDown(event, cateContent)
	});
}

function onBodyDown(event, cateContent) {
	if (!(event.target.id == cateContent || $(event.target).parents("#"+cateContent).length>0)) {
		hideCate(cateContent);
	}
}

function hideCate(cateContent) {
	$("#"+cateContent).fadeOut("fast");
	$("body").unbind("mousedown",function(event){
		onBodyDown(event, cateContent)
	});
}
/*下拉形式显示div中的tree*/
	