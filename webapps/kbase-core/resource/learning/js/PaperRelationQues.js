/*试卷管理-手工题目*/
RelationQues = {
	/*方法*/
	fn: {
		/*获取题目数据表*/
		getTable: function(key){
			if(key) return $('table[key="' + key + '"]');
			return  $('table[key]');
		},
		/*获取所有题目数据*/
		getQuesData: function(key){
			var _that = this, quesData = {}, 
				table = _that.getTable(key);
			$(table).each(function(i, item){
				var key = $(item).attr('key'), data = new Array();
				$(item).find('tr').each(function(i, item){
					if(i > 0) {
						var id = $(item).find('input[name="' + key + '_quesId"]').val();
						var name = $(item).find('td:eq(1)').text();
						var score = $(item).find('input[name="' + key + '_score"]').val();
						data.push({'id':id, 'score': score, 'name': name, 'key': key});
					}
				});
				quesData[key] = data;
			});
			if(key) return quesData[key];
			return quesData;
		},
		/*判断行是否在指定数组中*/
		inRowArray: function(row, rows){
			var ids = new Array();
			$(rows).each(function(i, item){
				var id = $(item).find('input[name*="_quesId"]').val();
				ids.push(id); 
			});
			ids = ids.join(',');
			/*当前行的id*/
			var id = $(row).find('input[name*="_quesId"]').val();
			return ids.indexOf(id);
		},
		/*刷新标题表*/
		refresh: function(){
			var _that = this, quesData = {}, 
				quesData = _that.getQuesData()
				amount_ = 0, score_ = 0;
			for(var key in quesData){
				var data = quesData[key],
					amount = 0, score = 0;
				$(data).each(function(i, item){
					amount += 1;
					score += Number(item.score);
				});
				amount_ += amount;
				score_ += score;
				$('#' + key + '_amount').text(amount);
				$('#' + key + '_score').text(score);
			}
			$('#t_amount').text(amount_);
			$('#t_score').text(score_);
		},
		
		/*点击*/
		click: function(key){
			/*所有的题目表*/
			var _that = this, 
				table = _that.getTable();
			/*渲染选中行样式*/
			$('.left_title table tr').each(function(i, item){
				if($(item).attr('key') == key)
					$(item).addClass('sel');
				else $(item).removeClass('sel');
			});
			/*显示对应内容table*/
			$(table).each( function(i, item){
				if($(item).attr('key') == key) 
					$(item).parent().css({display: ''});
				else $(item).parent().css({display: 'none'});
			});
		},
		/*上移*/
		moveUp: function(key){
			var _that = this, 
				table = _that.getTable(key),/*当前表*/
				rows = $(table).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows){
				$(rows).each(function(i, row){
					var index = $(row).index();/*当前行位置*/
					var row_ = $(row).prev();/*当前行的前一行*/
					if(index > 1 && /*标题行与第一行不上移, 当前行的前一行在被移动行中也不上移*/
						_that.inRowArray(row_, rows) == -1){
							$(row_).remove();
							$(row).after(row_);
						}
				});
			}
		},
		/*下移*/
		moveDown: function(key){
			var _that = this, 
				table = _that.getTable(key),/*当前表*/
				maxLen = $(table).find('tr').length;
				rows = $(table).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows){
				$($(rows).toArray().reverse())./*倒序*/
					each(function(i, row){
						var index = $(row).index();/*当前行位置*/
						var row_ = $(row).next();/*当前行的后一行*/
						if(index < maxLen-1 && /*最后一行不下移, 当前行的后一行在被移动行中也不下移*/
							_that.inRowArray(row_, rows) == -1){
								$(row).remove();
								$(row_).after(row);
							}
				});
			}
		},
		/*删除*/
		delRow: function(key){
			var _that = this, 
				table = _that.getTable(key),/*当前表*/
				rows = $(table).find('tr').not(":first").has('input[type="checkbox"]:checked');/*当选中行*/
			if(rows) {
				if(rows.length<=0){
					parent.layer.alert('请选择要删除的列',-1);
					return false;
				}
				$(rows).each(function(){
					/*移除行*/
					$(this).remove();
					/*刷新概述表*/
					_that.refresh();
				});
			}
		},
		/*新增*/
		addRow: function(key, row){
			var _that = this, 
				table = _that.getTable(key);/*当前表*/
			var score = $(table).find('input[type="text"][name="defScore"]').val();/*默认分数*/
			$(table).find('tr:last').after(/*添加行*/
				'<tr>' +
					'<td style="text-align: center;">' + 
						'<input type="checkbox" name="' + key + '_quesId" value="' + row.id + '" />' + 
					'</td>' + 
					'<td>' + row.name + '</td>' + 
					'<td><span>分值<input type="text" name="' + key + '_score" value="' + score + '"></span></td>' + 
				'</tr>');
			$(table).find("input").focus();
			/*刷新概述表*/
			_that.refresh();
		},
		/*新增回掉*/
		addRowCallback: function(key, data){
			var _that = this, /*回显修改数据*/
				data_ = _that.getQuesData(key);
			if(key && data){
				var ids = new Array();
				$(data_).each(function(i, item){ ids.push(item.id); });
				ids = ids.join(',');
				$(data).each(function(i, item){
					if(ids.indexOf(item.id) == -1){
						_that.addRow(key, item);
					}
				});
			}
		}
	},
	
	/*表单提交*/
	from: {
		quesData: null,/*表单数据*/
		scoreReal: 0,/*总分*/
		/*表单数据验证*/
		check: function(){
			var _that = this, fn = RelationQues.fn, 
				quesData = fn.getQuesData();
			if(quesData){
				/*表单数据验证, 总分数*/
				var validate = true, scoreReal = 0;
				for(key in quesData){
					var data = quesData[key];
					if(data && data.length > 0){
						$(data).each(function(i, ques){
							if(ques.score == '0') {
								var keyName = $('.left_title table tr[key="' + key + '"] td:eq(0)').text();
								layer.alert(keyName + '中不应该存在0分的题目!', -1);
								return validate = false;
							} else {
								scoreReal += Number(ques.score);
							}
						});
					}
					if(!validate) break;
				}
				_that.quesData = quesData;
				_that.scoreReal = scoreReal;
				return validate;
			}
			return false;
		},
		/*表单提交*/
		post: function(paperId, quesData){
			var load_index = layer.load('提交中…');
			$.post($.fn.getRootPath() + "/app/learning/paper!saveRelatedQues.htm?paperId=" + paperId + "&isRandom=" + $("[name='isRandom']").val(), 
				{data: JSON.stringify(quesData)},
				function(jsonResult){
					var msg_inx = layer.alert(jsonResult.msg, -1, function(){
						layer.close(msg_inx);
						layer.close(load_index);
						if(jsonResult.rst){/*操作成功*/
							parent.pageClick();//刷新当前页面
							parent.layer.closeAll();/*关闭弹窗*/
						}
					});
			}, 'json');
		},
		/*表单提交*/
		submit: function(){
			var _that = this, 
				validate = _that.check(),/*表单数据验证*/
				quesData = _that.quesData,/*表单数据*/
				scoreReal = _that.scoreReal;/*总分*/
			/*表单数据验证成功*/
			if(validate){
				var paperId = _paperId, /*试卷id*/
					score = _Score;/*试卷总分*/
				if(paperId && scoreReal){
					if(score == scoreReal){/*总分与实际分数相等*/
						_that.post(paperId, quesData);
					} else {/*总分与实际分数不等*/
						var msg_inx = layer.alert('总分(' + score + ')与实际分数(' + scoreReal + ')不等，试卷将被置为“停用”状态', 
							-1, function(){
								layer.close(msg_inx);
								_that.post(paperId, quesData);
						});
					}
				} else {
					layer.alert('数据异常!', -1);
				}
			}
		}
	},
	
	/*初始化*/
	init: function(){
		var _that = this;
		/*默认选择第一行*/
		_that.fn.click(0);
		/*渲染标题表，行点击事件*/
		$('.left_title table tr').each(function(i, item){
			var key = $(item).attr('key');
			if(key){/*标题行无key值*/
				$(item).click(function(){
					_that.fn.click(key); 
				});
			}
		});
		/*渲染题目表样式*/
		var h = $("body").height(), w = $("body").width(),
			tabs = _that.fn.getTable();
		$(tabs).each(function(i, item){
			/*渲染高度*/
			$(item).parent().css({height: h-100});
			/*渲染按钮事件*/
			var key = $(item).attr('key');
			$(item).find('.btns').children('input[type="button"]')
				.each(function(i, item){
					var name = $(item).attr('name');
					$(item).click(function(){
						if(name == 'upBtn'){
							_that.fn.moveUp(key);
						} else if (name == 'downBtn'){
							_that.fn.moveDown(key);
						} else if (name == 'delBtn'){
							_that.fn.delRow(key);
						} else if (name == 'addBtn'){
							$.kbase.picker.quesByType({
								key: key,/*题目分类类型*/
								initData: 'RelationQues.fn.getQuesData',/*回显数据方法名*/
								callback: 'RelationQues.fn.addRowCallback',/*弹窗回掉方法名*/
								diaWidth:w*0.9, diaHeight:h*0.9
							});
						}
					});
			});
		});
		
		/*表单保存*/
		$('#saveRelationQues').click(function(){
			_that.from.submit();
		});
		
		/*文本框设置数字验证监听*/
		$(tabs).on('keyup', 'input[type="text"][name$="_score"], input[type="text"][name="defScore"]', 
			function(){
				var v = $(this).val(),
					n = $(this).attr('name');
				if(!/^\d+$/.test(v) || Number(v)<1) 
					$(this).val(0);/*判断是否是正整数*/
				if(name != 'defScore'){
					/*刷新概述表*/
					_that.fn.refresh();
				}
		});
	}
};

/*初始化*/
$(function(){
	RelationQues.init();
});

//手工题目是否随机排序
function changeval(_this){
	var ischecked = false;
	if($(_this).is(':checked')){
		ischecked = true;
	}
	$("[name='isRandom']").each(function(){
		if(ischecked){
			$(this).val(1);
			$(this).attr("checked","checked");
		}else{
			$(this).val(0);
			$(this).removeAttr("checked");
		}
	});
}