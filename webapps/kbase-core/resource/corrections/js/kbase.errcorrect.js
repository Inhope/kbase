/**
 * fire an error correction
 * @authro eko.zhan 
 * @since 2015-08-18 17:10 
 */
;(function($){
	$.kbase = $.kbase || {};
	$.extend($.kbase, {
		errcorrect: function(opts){
			//必需的参数
			var _default = {
				'objId': '',
				'faqId': '',
				'cateId': ''/*,
				'question': '',
				'answer': ''*/
			}
			_default = $.extend(_default, opts);
			var _params = $.param(_default);
			var _url = $.fn.getRootPath() + "/app/corrections/corrections!send.htm?" + _params;
			//IE浏览器使用window.showModalDialog的方式显示评论界面
   			if (!!window.ActiveXObject || "ActiveXObject" in window){
				//_showModalDialog({url: _url, width: 835, height: 400});
   				_open(_url, "知识纠错", 835, 400);
   			}else{
				parent.__kbs_layer_index = parent.$.layer({
					type: 2,
					border: [10, 0.3, '#000'],
					title: false,
					closeBtn : [0, true],
					move: '.xubox_title',
					iframe: {src : _url, scrolling: 'no'},
					area: ['832px', '400px']
				});     			
   			}		
		}	
	});
	
	function _open(url, title, width, height){
		//获得窗口的垂直位置 
		var top = (screen.height - 30 - height) / 2; 
		//获得窗口的水平位置 
		var left = (screen.width - 10 - width) / 2; 
		if (navigator.isLowerBrowser()){
			title = title.replace(/-/g, '');
		}
		var win = window.open(url, title, 'height=' + height + ',width=' + width + ',top=' + top + ',left=' + left + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=no,titlebar=no');
		win.focus();
	}
})(jQuery);
/**
 * @author eko.zhan
 * url, width, height, args
 */
function _showModalDialog(opts){
	opts = $.extend(true, {
		width: 500,
		height: 400,
		args: window
	}, opts);
	var _diaWidth = opts.width;
	var _diaHeight = opts.height;
	var _scrWidth = screen.width;
	var _scrHegiht = screen.height;
	var _diaLeft = (_scrWidth-_diaWidth)/2;
	var _diaTop = (_scrHegiht-_diaHeight)/2;
	var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
	window.showModalDialog(opts.url, opts.args, params);
}