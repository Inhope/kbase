$(function() {

    $('#unSolvedPanel input.unsolve_xinxi_an1').parent().click(function(){
		 var remarks=$('#rmmarks_val').val();
		 if ($.trim(remarks)==""){
		    parent.layer.alert("请填写回复", -1);
		 }
		 else
		 {
		 $.ajax({
				url : $.fn.getRootPath() + '/app/corrections/corrections!updatecorrections.htm',
				type : 'POST',
				data:{
				  "id":$('#id_val').val(),
				  "remarks":$('#rmmarks_val').val()
				},
				timeout : 15000,
				sync : false,
				dataType : 'json',
				success : function(data) {
				   if(data.success=="success")
				   {
				     $('body').hideShade();
				     $('#unSolvedPanel').hide();
				     parent.parent.$(document).hint("处理成功");
				     tableObject.render();
				   }
				   
				   
				}
			});
		}		
    });
    
	var tableObject = {
		tableEl : $('table'),
		forms : {
			startTime : $('#startTime'),
			endTime : $('#endTime'),
			type :$('#type'),
			status :$('#status'),
			type :$('#type'),
			startTimeUi : null,
			endTimeUi : null,
			categoryName : $('#categoryName'),
			objectContent : $('#objectContent'),
			question : $('#question'),
			dept : $('#dept'),
			person : $('#person'),
			searchBtn : $('#searchBtn'),
			exportBtn : $('#exportBtn'),
			dateSetting : {
				required : false,
				closeText : '关闭',
				currentText : '今天',
				okText : '确定',
				editable : false,
				formatter :function(datetime){
					var year = datetime.getFullYear();
					var month = datetime.getMonth();
					month = month + 1;
					month = month < 10 ? '0' + month : month;
					var date = datetime.getDate();
					date = date < 10 ? '0' + date : date;
					var hour = datetime.getHours();
					hour = hour < 10 ? '0' + hour : hour;
					var minute = datetime.getMinutes();
					minute = minute < 10 ? '0' + minute : minute;
					return year + '-' + month + '-' + date + ' ' + hour + ':' + minute + ':00';
				}
			},
			forIe6 : function() {
				this.startTime.next().find('input').css({
					'border' : '0',
					'width' : '128px'
				}).parent().parent().css({
					'position' : 'relative'
				});
				
				this.startTime.next().find('input').next().css({
					'position' : 'absolute',
					'top' : '7px'
				});
	
				this.endTime.next().find('input').css({
					'border' : '0',
					'width' : '128px'
				}).parent().parent().css({
					'position' : 'relative'
				});
				
				this.endTime.next().find('input').next().css({
					'position' : 'absolute',
					'top' : '7px'
				});
			},
			validation : function() {
				var startTime = this.startTimeUi.val();
				var endTime = this.endTimeUi.val();
				if(startTime && endTime) {
					var s = new Date(startTime.replace(/-/gi, '/')).getTime();
					var e = new Date(endTime.replace(/-/gi, '/')).getTime();
					if(s > e) {
						parent.layer.alert('开始时间不能晚于结束时间', -1);
						return false;
					}
				}
				return true;
			},
			getValues : function() {
				return {
					startTime : this.startTimeUi.val(),
					endTime : this.endTimeUi.val(),
					objectName : this.objectContent.val(),
					question : this.question.val(),
					type : this.type.val(),
					status : this.status.val(),
					deptBh : this.dept.attr('bh'),
					personName : this.person.val()
				}
			}
		},
		pager : null,
		tableAttributes : {
			title : '纠错统计查询',
			width : $(window).width()-16,
			height : '375',
			nowrap : true,
			autoRowHeight : false,
			striped : true,
			collapsible : false,
			url: $.fn.getRootPath() + '/app/statement/corrections-statement!list.htm',
			method : 'POST',
			sortOrder : 'desc',
			remoteSort : false,
			loadMsg:'加载中...',
			pagination:true,
			rownumbers:true,
			singleSelect: true,
			toolbar : '#searchCondition',
			fitColumns : true,
			
			onDblClickRow :function(rowIndex,rowData){
			   $.ajax({
						url : $.fn.getRootPath() + '/app/corrections/corrections!findcorrections.htm?id='+rowData['id'],
						type : 'POST',
						timeout : 15000,
						sync : false,
						dataType : 'json',
						success : function(data) {
							$(document).ajaxLoadEnd(); 
							var w = ($('body').width() - $('#unSolvedPanel').width())/2;
							$('#unSolvedPanel').css('left',w+'px');
							$('body').showShade();
							$('#unSolvedPanel').show();  
							$('#rmmarks_val').val(data.corrections.remarks);
							$('#id_val').val(data.corrections.id);
							$('#type_val').val(data.corrections.typelabel);
							$('#level_val').val(data.corrections.levellabel);
							$('#status_val').val(data.corrections.statuslabel);
							$('#content_val').val(data.corrections.content);
							$('#question_val').val(data.corrections.question);
							if(data.corrections.status=='1')
							{   
							  $('#issubmit').html("");
							}
							if(data.corrections.status!='1')
							{   
							  $('#issubmit').html('<input type="button" class="unsolve_xinxi_an1">');
							}
							
							$('#unSolvedPanel div.unsolve_xinxi_dan_title a').click(function(){
								$('body').hideShade();
								$('#unSolvedPanel').hide();
							});
						},
						error : function(data, textStatus, errorThrown) {
							$(document).ajaxLoadEnd();
							parent.layer.alert('请求失败!', -1);
						}
					});
			 },
			
			columns : [
				[   
				    {field:'id',hidden:true,width:0},
					{field:'question',title:'标准问',width:200},
					{field:'content',title:'纠错原因',width:200},
					{field:'statuslabel',title:'状态',width:200,styler:function(value,row,index){
                       if (value == '未处理'){
					      return 'color:red;';
					     }
					    }
					   },
					{field:'levellabel',title:'紧急程度',width:100},
					{field:'typelabel',title:'类型',width:140},
					{field:'userInfo',title:'操作人',width:120,formatter: function (value) { return value.userChineseName }},
					{field:'create_time',title:'创建时间',width:120}
				]
			]
		},
		pagerAttributes : {
			pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10, 20, 50],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        onBeforeRefresh:function(){
	        	
	        }
		},
		loadData : function() {
			!this.forms.validation() || this.tableEl.datagrid('load', this.forms.getValues());
		},
		/*******渲染表格*******/
		render : function() {
			var self = this;
			//初始化表格
			this.tableEl.datagrid(this.tableAttributes);
			this.pager = this.tableEl.datagrid('getPager');
			$(this.pager).pagination(this.pagerAttributes);
			
			//初始化日期时间控件
			this.forms.startTime.datetimebox(this.forms.dateSetting);
			this.forms.endTime.datetimebox(this.forms.dateSetting);
			this.forms.forIe6();
			this.forms.startTimeUi = this.forms.startTime.next().find('input:eq(0)');
			this.forms.endTimeUi = this.forms.endTime.next().find('input:eq(0)');
			
			this.forms.searchBtn.click(function() {
				self.loadData();
			});
		}
	}
	tableObject.render();
 });
 
 
/**	
	var categoryTreeObject = {
		knowledgeTypeEl : $('#categoryName'),
		ktreeEl : $('div.t-p-d'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountCategoryTree.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					categoryTreeObject.knowledgeTypeEl.val(treeNode.name);
					categoryTreeObject.knowledgeTypeEl.attr('bh', treeNode.bh);
				}
			}
		},
		render : function() {
			var self = this;
			self.knowledgeTypeEl.val('');
			self.knowledgeTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 5) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.knowledgeTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.knowledgeTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_title>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#categoryTree'), this.treeAttr );
		}
	}
	
	var deptTreeObject = {
		deptTypeEl : $('#dept'),
		ktreeEl : $('div.t-p-d1'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					deptTreeObject.deptTypeEl.val(treeNode.name);
					deptTreeObject.deptTypeEl.attr('bh', treeNode.bh);
				}
			}
		},
		render : function() {
			var self = this;
			self.deptTypeEl.val('');
			self.deptTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 5) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.deptTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#deptTree'), this.treeAttr );
		}
	}
	var exportExcel = {
		exceportbtn : $('#exportBtn'),
		render : function(){
			this.exceportbtn.click(function(){
				/*
				var startTime = $('#startTime').val();
				var endTime = $('#endTime').val();
				if(startTime && endTime) {
					var s = new Date(startTime).getTime();
					var e = new Date(endTime).getTime();
					if(s > e) {
						alert('开始时间不能晚于结束时间');
						return;
					}
				}*/
				
	/**			var _total = tableObject.tableEl.datagrid('getPager').data("pagination").options.total;
				
				if (tableObject.forms.validation() && _total<5000) {
					$(document).ajaxLoading('处理数据中...');
					//
					$.ajax({
						url : $.fn.getRootPath() + '/app/statement/click-amount!processData.htm',
						type : 'POST',
						timeout : 15000,
						sync : false,
						dataType : 'json',
						/*
						data : {
						    startTime : startTime,
							endTime : endTime,
							bh : $('#categoryName').attr('bh'),
							objectName : $('#objectContent').val(),
							question : $('#question').val(),
							dept : $('#dept').val(),
							personName : $('#person').val()
						},*/
			/**			data: tableObject.forms.getValues(),
						success : function(data, textStatus, jqXHR) {
							$(document).ajaxLoadEnd(); 
							if(data.success) {
								var iframe = document.createElement("iframe");
					            iframe.src = $.fn.getRootPath()+"/app/statement/click-amount!downLoad.htm?fileName=" + data.message;
					            iframe.style.display = "none";
					            document.body.appendChild(iframe);
							} else {
								alert(data.message);
							}
						},
						error : function(data, textStatus, errorThrown) {
							$(document).ajaxLoadEnd();
							alert('导出失败!');
						}
					});
				} //end validation
			});
		}
	}
	exportExcel.render();
	tableObject.render();
	categoryTreeObject.render();
	deptTreeObject.render();
});**/