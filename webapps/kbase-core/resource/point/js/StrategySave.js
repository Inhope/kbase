/**
 * 积分策略-新增、保存
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.StrategySave = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
		dialog: {/*当前窗口*/
			id: null,
			setId: function(dialogId){ this.id = dialogId; },
			getId: function(){ return this.id;}
		}
	});
	
	that.extend({/*基本方法*/
		init: function(){/*初始化*/
			/*表单保存*/
			$('#btn-save').click(function(){
				PageUtils.submit('#form1', {
					url: '/app/points/p-strategy!doSave.htm',
		    		after: function(jsonResult){
		    			parent.$.messager.alert('信息', jsonResult.msg, 'info', function(){
			   				if (jsonResult.rst){
			   					/*关闭弹窗*/
			    				parent.PageUtils.dialog.close(fn.dialog.getId());
			    				/*操作成功-刷新列表*/
			    				parent.PageUtils.datagrid.reload();
			   				}
			   			});
		    		}
				});
			});
		},
		set: function(params){
			/*当前窗口id*/
			fn.dialog.setId(params.dialogId);
			/*周期设置,用于对‘执行次数’、‘有效期’进行控制*/
			actCyc = params.actCyc;
			if(!actCyc) actCyc = '2';/*为空是默认选择每天*/
			$('#actCyc').combobox('select', '2');/*选1的时候有bug，因此先选一个其它选项*/
			$('#actCyc').combobox('select', actCyc);
		}
	});
	/*初始化*/
	that.init();
});