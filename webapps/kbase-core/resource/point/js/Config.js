/**
 * 积分配置项
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.Config = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
	});
	
	that.extend({/*基本方法*/
		init: function(){/*初始化*/
			/*编辑*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var id  = PageUtils.datagrid.getChecked(); /*当前选中行*/
			    	if(id){
			    		PageUtils.dialog.open('#dd_save', {
				    		params: { configid: id },
				    		title: '配置项编辑', width: '600',
				    		url: '/app/points/p-config!toSave.htm',
				    	});
			    	}
			    }
			});
		}
	});
	/*初始化*/
	that.init();
});