/**
 * 用户积分设置
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.UserSet = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
	});
	
	that.extend({/*基本方法*/
		userSet: function(id){/*用户积分设置*/
			if(id){
	    		PageUtils.dialog.open('#dd_save', {
		    		params: { userid: id },
		    		title: '自定义策略', width: '600', height: '400',
		    		url: '/app/points/p-user!toUpdate.htm',
		    	});
	    	}
		},
		userDetail:function(id){/*用户积分详情*/
			if(id){
	    		PageUtils.dialog.open('#dd_save', {
		    		params: { userid: id },
		    		title: '自定义策略', width: '650', height: '400',
		    		url: '/app/points/p-user!toDetail.htm',
		    	});
	    	}
		},
		init: function(){/*初始化*/
			/*查询*/
			$('#now-search').linkbutton({
			    onClick: function(){
			    	/*查询跳转到第一页*/
			    	PageUtils.datagrid.reload(1);
			    }
			});
			
			/*重置*/
			$('#now-clear').linkbutton({
			    onClick: function(){
			    	$('#jobNumber').textbox('clear');
			    	$('#userChineseName').textbox('clear');
			    	$('#depIds').combotree('clear');
			    	$('#roleId').combotree('clear');
			    }
			});
			
			/*导出选择*/
			$('#btn-expSel').linkbutton({
			    onClick: function(){
					ExportUtils.toExcel('/app/points/p-set!exportData.htm',
						'/app/points/p-set!exportResult.htm', PageUtils.datagrid.getQueryParams());
			    }
			});
			
			/*导出全部*/
			$('#btn-expAll').linkbutton({
			    onClick: function(){
					ExportUtils.toExcel('/app/points/p-set!exportData.htm',
						'/app/points/p-set!exportResult.htm');
			    }
			});
		}
	});
	/*初始化*/
	that.init();
});