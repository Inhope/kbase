/**
 * 积分策略
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.Strategy = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
		datas:{/*相关数据*/
			datas: {},
			set:function(key, data){ this.datas[key] = data; },
			get:function(key){ return this.datas[key]; }
		}
	});
	
	that.extend({/*基本方法*/
		strategyExt: function(id){/*自定义策略*/
			if(id){
	    		PageUtils.dialog.open('#dd_save_ext', {
		    		params: { strategyid: id },
		    		title: '自定义策略', width: $(window).width()*0.95, height: $(window).height()*0.9,
		    		url: '/app/points/p-strategy-ext.htm',
		    	});
	    	}
		},
		init: function(){/*初始化*/
			/*新增*/
			$('#btn-add').linkbutton({
			    onClick: function(){
			    	PageUtils.dialog.open('#dd_save', {
			    		title: '策略新增', width: '600',
			    		url: '/app/points/p-strategy!toSave.htm',
			    	});
			    }
			});
			
			/*编辑*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var id  = PageUtils.datagrid.getChecked(); /*当前选中行*/
			    	if(id){
			    		PageUtils.dialog.open('#dd_save', {
				    		params: { strategyid: id },
				    		title: '策略编辑', width: '600',
				    		url: '/app/points/p-strategy!toSave.htm',
				    	});
			    	}
			    }
			});
			
			/*删除*/
			$('#btn-remove').linkbutton({
			    onClick: function(){
			    	var id  = PageUtils.datagrid.getChecked(); /*当前选中行*/
			    	if(id){
			    		PageUtils.confirm({
				    		title: '删除', params: { strategyid: id },
				    		url: '/app/points/p-strategy!delete.htm',
				    		after: function(jsonResult){
				    			$.messager.alert('信息', jsonResult.msg, 'info', function(){
					   				if (jsonResult.rst){
					    				/*操作成功-刷新列表*/
					    				PageUtils.datagrid.reload(1);
					   				}
					   			});
				    		}
				    	});
			    	}
			    }
			});
		},
		set: function(params){
			fn.datas.set('cycleTypes', params.cycleTypes);/*策略周期类型*/
		}
	});
	/*初始化*/
	that.init();
});