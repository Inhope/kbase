/**
 * 积分管理
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
$(function () {
	var that = window.Manager = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*公共方法*/
	fn.extend({
		updateTab: function(tab, url){
			url = $.fn.getRootPath() + url;
			$('#tt').tabs('update', {
				tab: tab,
				options: {
					border:false,
					content: '<iframe frameborder="0"  src="' + url + '" '
						+ 'style="width:100%;height:99%;"></iframe>'
				}
			});
		}
	});
	
	/*基本方法*/
	that.extend({
		/*初始化*/
		init: function(){
			/*面板初始化*/
			$('#tt').tabs({
				fit:true,border:'false',
				onSelect:function(title, index){
					var tab = $(this).tabs('getTab', index),
						id = $(tab).panel('options').id,
						body = $(tab).panel('body');
					if (id == 'summary') {/*积分概要*/
						fn.updateTab(tab, '/app/points/p-manager!summary.htm');
					} else {
						if($(body).html() == ''){
							if (id == 'configs') {/*积分配置*/
								fn.updateTab(tab, '/app/points/p-config.htm');
							} else if (id == 'strategy'){/*积分策略*/
								fn.updateTab(tab, '/app/points/p-strategy.htm');
							} else if (id == 'setting'){/*积分设置*/
								fn.updateTab(tab, '/app/points/p-set.htm');
							}
						}
					}
				}
			});
			$('#tt').tabs('select', 1);
			$('#tt').tabs('select', 2);
			//$('#tt').tabs('select', 3);
			$('#tt').tabs('select', 0);
		}
	});
	
	/*初始化*/
	that.init();
});
