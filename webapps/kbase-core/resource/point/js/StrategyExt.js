/**
 * 积分自定义策略
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.StrategyExt = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
		datas:{/*相关数据*/
			datas: {},
			set:function(key, data){ this.datas[key] = data; },
			get:function(key){ return this.datas[key]; }
		}
	});
	
	that.extend({/*基本方法*/
		init: function(){/*初始化*/
			/*新增*/
			$('#btn-add').linkbutton({
			    onClick: function(){
			    	var strategyId = $('#now-grid').datagrid('options')
			    		.queryParams.strategyId;
			    	PageUtils.dialog.open('#dd_save', {
			    		params: {strategyId: strategyId},
			    		title: '自定义策略新增', width: '500',
			    		url: '/app/points/p-strategy-ext!toSave.htm'
			    	});
			    }
			});
			
			/*编辑*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var id  = PageUtils.datagrid.getChecked(); /*当前选中行*/
			    	if(id){
			    		/*rangBhs为所选目录CATALOG、部门DEPT所有涉及的父子部门id*/
			    		var rangBhs = fn.datas.get('sgExtDatas')[id]['rangBhs'],
			    		 	cateRangBhs = rangBhs['CATALOG'], 
			    		 	deptRangBhs = rangBhs['DEPT'];
			    		/*deptRangBhs去重处理——原因：数据太长*/
			    		var deptIds = new Array();
			    		$(deptRangBhs).each(function(i, item1){
			    			$(item1.split('.')).each(function(m, item2){
			    				if(deptIds.indexOf(item2) == -1)
			    					deptIds.push(item2);
			    			});
			    		});
			    		PageUtils.dialog.open('#dd_save', {
				    		params: { strategyExtId: id, cateRangBhs: cateRangBhs, 
				    			deptRangBhs: PageUtils.fn.arrayToString(deptIds, ',')},
				    		title: '自定义策略编辑', width: '500',
				    		url: '/app/points/p-strategy-ext!toSave.htm'
				    	});
			    	}
			    }
			});
			
			/*删除*/
			$('#btn-remove').linkbutton({
			    onClick: function(){
			    	var id  = PageUtils.datagrid.getChecked(); /*当前选中行*/
			    	if(id){
			    		PageUtils.confirm({
				    		title: '删除', params: { strategyExtId: id },
				    		url: '/app/points/p-strategy-ext!delete.htm',
				    		after: function(jsonResult){
				    			$.messager.alert('信息', jsonResult.msg);
				    			if(jsonResult.rst) /*操作成功-刷新列表*/
				    				PageUtils.datagrid.reload();
				    		}
				    	});
			    	}
			    }
			});
		}
	});
	/*初始化*/
	that.init();
});