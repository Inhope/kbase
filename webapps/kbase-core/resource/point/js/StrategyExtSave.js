/**
 * 积分自定义策略-新增、编辑
 * @auther Gassol.Bi
 * @date 2016-8-5 17:00:35
 */
 $(function () {
	var that = window.StrategyExtSave = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({/*公共方法*/
		dialog: {/*当前窗口*/
			id: null,
			setId: function(dialogId){ this.id = dialogId; },
			getId: function(){ return this.id;}
		},
		datas:{/*相关数据*/
			datas: {},
			set:function(key, data){ this.datas[key] = data; },
			get:function(key){ return this.datas[key]; }
		},
		getRealcidBycbh: function(cbhs){
			var cids = new Array();
			$(ocbhs).each(function(){
				var cbh = this, 
					post = cbh.lastIndexOf('.');
				if(post == -1)
					cids.push(cbh);
				else cids.push(cbh.spilt(post));
			});
			return cids;
		},
		/*判断*/
		judge: function(cbhs, dbhs, datas){
			/*处理结果*/
			var result = {rst:true, msg:'没有冲突!'};
			/*其他规则数据*/
			var rangBhs = datas['rangBhs'], 
				name = datas['name'];/*规则名称*/
			/*规则分类信息*/
			var ocbhs = rangBhs['CATALOG'];
			/*因为分类的id与bh不一致，所以应该都以bh做比较*/
			if(cbhs && !fn.conflict(cbhs, ocbhs)) {
				result.rst = false;
				result.msg = '分类与自定义规则（' + name + '）中的分类存在冲突!';
			}
			/*规则部门信息*/
			var odbhs = rangBhs['DEPT'];
			if(dbhs && !fn.conflict(dbhs, odbhs)) {
				result.rst = false;
				result.msg = '部门与自定义规则（' + name + '）中的部门存在冲突!';
			}
			/*返回结果*/
			return result;
		},
		/*冲突*/
		conflict: function(bhs, obhs){
			var rst = true;
			if(bhs && bhs.length > 0 && 
				obhs && obhs.length > 0) {
				/*所选节点bh出现在其他策略规则节点的bh上，认为该节点是父节点*/
				/*其他策略规则节点的bh出现在所选节点bh上，认为该节点是子节点*/
				for(var i = 0; i < bhs.length; i++) {
					var bh = bhs[i];
					for(var j = 0; j < obhs.length; j++){
						var obh = obhs[j];/*防止出现 1000.1005L 与1000.1000.1005L这样的数据*/
						if((',' + obh + '.').indexOf(',' + bh + '.') > -1 
							|| (',' + bh + '.').indexOf(',' + obh + '.') > -1) {
							rst = false;
							break;
						}
					}
					if(!rst) break;
				}
			}
			return rst;
		}
	});
	
	that.extend({/*基本方法*/
		init: function(){/*初始化*/
			/*表单保存*/
			$('#btn-save').click(function(){
				PageUtils.submit('#form1', {
					url: '/app/points/p-strategy-ext!doSave.htm',
					before: function(){
						/*验证结果*/
						var rst = true, msg = '';
						
						/*积分项判断*/
						var configPoints = 0;
						$('input[name="configPoints"]').each(function(){
							var point = $(this).val();
							if(point) configPoints += Number(point);
						});
						if(configPoints == 0) {
							rst = false;
							msg = '无意义的自定义策略，不允许积分配置项同时为0!';
						} else {
							/*规则条件判断*/
							var cbhs = $('#cateBhs').val();/*所勾选的分类ids,bhs*/
							var dbhs = $('#deptBhs').val();/*所勾选的部门ids,bhs*/
							if(cbhs) cbhs = cbhs.split(',');
							if(dbhs) dbhs = dbhs.split(',');
							/*与其他规则做冲突判断*/
							if(cbhs.length > 0 || dbhs.length > 0 ) {
								/*全部自定义策略数据，当前自定义策略id(详见StrategyExtSave.set方法)*/
								var sgExtDatas = fn.datas.get('sgExtDatas'),
									sgExtId = fn.datas.get('sgExtId');
								if(sgExtDatas) {
									for(var key in sgExtDatas) {/*循环遍历冲突判断*/
										if(key != sgExtId) {/*不和自己做冲突严重*/
											var result = fn.judge(cbhs, dbhs, sgExtDatas[key])
											rst = result.rst;
											if(!rst){
												msg = result.msg
												break;
											}
										}
									}
								}
							} else {
								rst = false;
								msg = '无意义的自定义策略，不允许限定条件“分类”与“部门”同时为空!';
							}
						}
						
						/*显示结果*/
						if(!rst) $.messager.alert('信息', msg);
						return rst;
					},
		    		after: function(jsonResult){
		    			if(jsonResult.rst) {
		    				parent.$.messager.alert('信息', jsonResult.msg);
		    				/*关闭弹窗*/
		    				parent.PageUtils.dialog.close(fn.dialog.getId());
		    				/*操作成功-刷新列表*/
		    				parent.PageUtils.datagrid.reload();
		    			} else {
		    				$.messager.alert('信息', jsonResult.msg);
		    			}
		    		}
				});
			});
		},
		set: function(params){
			/*当前窗口id*/
			fn.dialog.setId(params.dialogId);
			
			/*已有自定策略部门、分类勾选数据（用于当前自定义策略的冲突判断）*/
			var sgExtDatas = parent.StrategyExt.fn.datas.get('sgExtDatas');
			fn.datas.set('sgExtDatas', sgExtDatas);/*用于当前自定义策略存前冲突判断，详见save方法：*/
			
			/*当前自定义策略-所选分类及部门回显*/
			var sgExtId = params.strategyExtId;
			fn.datas.set('sgExtId', sgExtId);/*当前自定义策略id，详见save方法：*/
			if(sgExtId){/*只有编辑时才需要回显*/
				/*第一步，目的在于事先将这些节点数据直接加载出来，在“分类回显”、“部门回显”时将id转换成文字,
				 当前自定义策略-所选分类及部门的所有父子节点数据（用于数据回显-参见StrategyExtSave.jsp）*/
				var mapCates = {}, rangCateNodes = PageUtils.fn.treeData(params.rangCates);
				$(rangCateNodes).each(function(){ mapCates[this.id] = this.children; });
				fn.datas.set('rangCateNodes', mapCates);/*分类*/
				var mapDepts = {}, rangDeptNodes = PageUtils.fn.treeData(params.rangDepts);
				$(rangDeptNodes).each(function(){ mapDepts[this.id] = this.children; });
				fn.datas.set('rangDeptNodes', mapDepts);/*部门*/
				
				/*第二步，设置回显值*/
				var datas = sgExtDatas[sgExtId];/*当前自定义策略,所选分类及部门的所有父子节点数据*/
				var cateIds = datas['rangIds']['CATALOG'];/*分类*/
				if(cateIds) {
					$('#cc').combotree('setValues', cateIds);
					$('#cateIds').val(PageUtils.fn.arrayToString(cateIds, ','));
				}
				var deptIds = datas['rangIds']['DEPT'];/*部门*/
				if(deptIds) {
					$('#cd').combotree('setValues', deptIds);
					$('#deptIds').val(PageUtils.fn.arrayToString(deptIds, ','));
				}
				
				/*第三步，设置影藏bh值*/
				var rangBhs = datas['rangBhs'],
					rangCateBhs = rangBhs['CATALOG'],
					rangDeptBhs = rangBhs['DEPT'];
				$('#cateBhs').val(PageUtils.fn.arrayToString(rangCateBhs, ','));
				$('#deptBhs').val(PageUtils.fn.arrayToString(rangDeptBhs, ','));
			}
		}
	});
	/*初始化*/
	that.init();
});