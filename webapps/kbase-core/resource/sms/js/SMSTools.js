/**
 * 广州移动-短信发送工具类 拷贝自\resource\custom\gzyd\js\GzydMappings.js
 * copy by heart.cao 2016-05-22
 */
$(function(){
	var that = window.SMSTools = {}, 
	fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({	
		/*短信*/
		sms: {
			/*弹窗打开*/
			open: function(categoryId, questionId){
				/*是否右键打开*/
				var qas = this.getQas(categoryId, questionId);
				if(!qas) return false;
				var ngccObj = SMSUtils.getData();
				if(!ngccObj) return false;
				/*打开短信弹窗*/
				return fn.shade.open({
					width: 900,
					height: 600,
					url: $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!send.htm',
					args: {'qas': qas, 'ngccObj': ngccObj, 'categoryId': categoryId}
				});
			},
			/*获取短信标题及短信内容*/
			getQas: function(categoryId, questionId){
				var qa = '';
				if(typeof(questionId)!="undefined" && $.trim(questionId)!=''){
					//来自模糊、精准搜索结果列表点击发短信图标
					var q = this.tagHandle($("#"+questionId+"_question").text());
					var a = this.tagHandle($("#"+questionId+"_answer").text());
					var isableSMS = $("#"+questionId+"_question").attr("isableSMS");
					if(isableSMS == 'true'){
						qa += '{qid:"'+questionId+'",q:"'+q+'",a:"'+a+'"},';
					}else{
						alert("知识点:'"+q+"',<br/>无权限发送短信!");
						return false;
					}
				}else if($("#gzyd_sms_question").length!=0 && $("#gzyd_sms_answer").length!=0){
					//来自模糊搜索结果命中详情展示
					var q = this.tagHandle($("#gzyd_sms_question").text());
					var a = this.tagHandle($("#gzyd_sms_answer").text());
					var qId = $("#gzyd_sms_question").attr("gzyd_sms_qId");
					var cId = $("#gzyd_sms_question").attr("gzyd_sms_cId");
					if(typeof(categoryId)=="undefined"
					     || $.trim(categoryId)=='') categoryId = cId;
					var isableSMS = $("#gzyd_sms_question").attr("isableSMS");
					if(isableSMS == 'true'){
						qa += '{qid:"'+qId+'",q:"'+q+'",a:"'+a+'"},';
					}else{
						alert("知识点:'"+q+"',<br/>无权限发送短信!");
						return false;
					}
				}
				qa = qa.replace(/^,|,$/,'');
				return eval('['+qa+']');
			},
			/*
			 * 标准问、标准回复内容处理
			 */
			tagHandle: function(con){
				con = con.replace(/<[^>]+>/g, "");//html标签处理
				con = con.replace(/(^\s*)|(\s*$)/g, "");//去除首尾空白
				con = con.replace(/\"/g, "\\\"");//转义双引号
				return con;
			}
		},	
		/*弹窗打开*/
		shade: {
			open: function(opts){
				var _diaWidth = opts.width;
				var _diaHeight = opts.height;
				var _scrWidth = screen.width;
				var _scrHegiht = screen.height;
				var _diaLeft = (_scrWidth-_diaWidth)/2;
				var _diaTop = (_scrHegiht-_diaHeight)/2;
				var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'
					+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
				return window.showModalDialog(opts.url, opts.args, params);
			}
		}
	});
	
	/*基础方法*/
	that.extend({
		/*短信相关*/
		sms:{
			/*发短信*/
			send: function(categoryId, questionId){
				return fn.sms.open(categoryId, questionId);
			}
		}
	});
});