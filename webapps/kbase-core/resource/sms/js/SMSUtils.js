/*获取“C:/CSP_DATE/yyyyMMdd.txt”中相关参数*/
var SMSUtils = {
	_basePath: 'C:/CSP_DATE/',
	_LOCATIONS:{
	    '01':{'tag':'guangzhou','name':'广州'},
	    '02':{'tag':'shenzhen','name':'深圳'},
	    '03':{'tag':'dongguan','name':'东莞'},
	    '04':{'tag':'foshan','name':'佛山'},
	    '05':{'tag':'shantou','name':'汕头'},
	    '06':{'tag':'zhuhai','name':'珠海'},
	    '07':{'tag':'huizhou','name':'惠州'},
	    '08':{'tag':'zhongshan','name':'中山'},
	    '09':{'tag':'jiangmen','name':'江门'},
	    '11':{'tag':'shaoguan','name':'韶关'},
	    '12':{'tag':'heyuan','name':'河源'},
	    '13':{'tag':'meizhou','name':'梅州'},
	    '14':{'tag':'shanwei','name':'汕尾'},
	    '15':{'tag':'yangjiang','name':'阳江'},
	    '16':{'tag':'zhanjiang','name':'湛江'},
	    '17':{'tag':'maoming','name':'茂名'},
	    '18':{'tag':'zhaoqing','name':'肇庆'},
	    '19':{'tag':'qingyuan','name':'清远'},
	    '20':{'tag':'chaozhou','name':'潮州'},
	    '21':{'tag':'jieyang','name':'揭阳'},
	    '22':{'tag':'yunfu','name':'云浮'}
	},
	_BRANDS:{
	    '9':{'tag':'qqt','name':'全球通'},
	    '11':{'tag':'dgdd','name':'动感地带'},
	    '17':{'tag':'szx','name':'神州行'}
	},
	_obtainToday: function(){
		var date = new Date();
		date.setDate(date.getDate());
		var y = date.getFullYear() + '';
		var m = (date.getMonth() + 1) + '';
		var d = date.getDate() + '';
		if(m < 10) m = '0' + m;
		if(d < 10) d = '0' + d;
		return (y + m + d);
	},
	_obtainData: function(src){
		var ts;
		try {
			var fso = new ActiveXObject("Scripting.FileSystemObject");
			if(!src) src = this._basePath + this._obtainToday() + '.txt';
			ts = fso.OpenTextFile(src, 1);/*1表示只读*/
			var arr = ts.ReadAll();
			if(arr.indexOf('\r\n') > -1){
				arr = arr.split('\r\n');
				return arr[arr.length-2];/*获取最后一条数据*/
			} else {
				return arr;
			}
		} catch (err){
			alert('错误：'+err + '' 
				+ '\n原因：1.请确认今日已做过相关业务操作，以确保记录文件的生成' 
				+ '\n      2.当前功能仅支持IE浏览器');
		} finally {
			if(ts) ts.Close();
		}
		return null;
	},
	getData: function (){
		var data  = this._obtainData();//获取处理数据
		if(data && data.indexOf('|') > -1){
			data = data.split('|');
			var ngccObj = {};//返回结果
			ngccObj.SERIALNO = data[0];
			ngccObj.CONTACTID = data[1];
			ngccObj.SUBSNUMBER = data[2];
			//归属地
			ngccObj.SUBSCITYID = data[3];
			var locationObj =  this._LOCATIONS[data[3]];
			if(typeof(locationObj)!='undefined'){
			   ngccObj.LOCATION_TAG = locationObj.tag;
			   ngccObj.LOCATION_NAME = locationObj.name;
			}
			//品牌
			ngccObj.SUBSBRANDID = data[4];
			var brandObj =  this._BRANDS[data[4]];
			if(typeof(brandObj)!='undefined'){
			   ngccObj.BRAND_TAG = brandObj.tag;
			   ngccObj.BRAND_NAME = brandObj.name;
			}
			ngccObj.DATATIME = data[5];
			return ngccObj;
		}
		return false;
	}
}
