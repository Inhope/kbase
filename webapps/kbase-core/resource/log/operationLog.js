var olog = {
	constants:{
		search:"查询",
		browse:"浏览"
	},
	defaults:{
		url: $.fn.getRootPath() + '/app/log/operation-log!log.htm'
	},
	/**
	 * 知识分类点击日志
	 * categoryId
	 */
	category:function(opts){
		$.extend(opts || {}, {
			touchEvent:"3"
		});
		$.post(this.defaults.url,opts);
	},
	/**
	 * 知识分类点击日志
	 * objectId
	 */
	obj:function(opts){
		$.extend(opts || {}, {
			touchEvent:"4"
		});
		$.post(this.defaults.url,opts);
//		this.doPost(opts);
	},
	/**
	 * 标准问点击日志
	 * qaId 标准问id
	 * name 标准问name
	 * numOfPage 标准问所在的页数
	 * sequence 数据所在当前页的序号
	 * sizeOfPage 每页显示的数据量
	 * @author baidengke
	 * @date 2016年3月28日
	 */
	qaTitle:function(opts){
		$.extend(opts || {}, {
			touchEvent:"5"
		});
		$.post(this.defaults.url,opts);
	},
	qaDetail:function(opts){
		$.extend(opts || {}, {
			touchEvent:"6"
		});
		$.post(this.defaults.url,opts);
	},
	qaPreview:function(opts){
		$.extend(opts || {}, {
			touchEvent:"7"
		});
		$.post(this.defaults.url,opts);
	},
	page:function(opts){
		$.extend(opts || {}, {
			touchEvent:"8"
		});
		this.doPost(opts);
//		$.post(this.defaults.url,opts);
	},
	sign:function(opts){
		$.extend(opts || {}, {
			touchEvent:"1"
		});
		$.post(this.defaults.url,opts);
	},
	doPost:function(opts){
		$.ajax({ 
			url: $.fn.getRootPath() + '/app/log/operation-log!log.htm', 
			type:"POST",
			data:opts,
			async:false,
			success: function(){
//				console.log("log success");
			},
			error:function(XMLHttpRequest, textStatus, errorThrown){
//				console.log("log error");
			}
		});
	}
};




