$(document).ready(function() {
	//显示共有和所有属性
	var showAttr = {
		showSame : $('li>input[type=radio]:eq(1)'),
		showAll : $('li>input[type=radio]:eq(0)'),
		privateAttr : $('tr[private=true]'),
		init : function() {
			this.showAll.attr('checked', true);
			var self = this;
			self.showAll.click(function() {
				self.privateAttr.show();
			});
			
			self.showSame.click(function() {
				self.privateAttr.hide();
			});
		}
	}
	showAttr.init();
	
	//删除和至于首列
	var option = {
		deleteCol : $('a[id="deleteCol"]'),
		setIndex : $('a[id=setIndex]'),
		init : function() {
			var self = this;
			//删除当前比对实例
			self.deleteCol.click(function() {
				var tr = $(this).parent().parent().parent();
				var td = $('td:not(:eq(0))', tr);
				if(td.size() == 2) {
					parent.layer.alert('至少要保留两个实例进行比对', -1);
				} else {
					var deleteId = $(this).parent().parent().attr('id');
					parent.layer.confirm("确定删除吗?", function(index){
						var idArr = new Array();
						td.each(function(index, element) {
							if($(this).attr('id') != deleteId) {
								idArr.push($(this).attr('id'));
							}
						});
						var ids = idArr.join(',');
						parent.layer.close(index);
						window.location.href = $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids=' + ids;
					});
				}
			});
			
			//置顶当前实例
			self.setIndex.click(function() {
				var tr = $(this).parent().parent().parent();
				var td = $('td:not(:eq(0))', tr);
				var setIndexId = $(this).parent().parent().attr('id');
				var idArr = new Array();
				td.each(function(index, element) {
					if($(this).attr('id') != setIndexId) {
						idArr.push($(this).attr('id'));
					}
				});
				var ids = idArr.join(',');
				window.location.href = $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids=' + setIndexId + ',' + ids;
			});
		}
	}
	option.init();
});