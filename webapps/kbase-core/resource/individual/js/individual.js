
function openShade(shadeId){
			var w = ($('body').width() - $('#'+shadeId).width())/3;
			$('#'+shadeId).css('left',w+'px');
			$('body').showShade();
			$('#'+shadeId).show();
}
      	
function selectTag(id,selfObj){
	// 操作标签
	var tag = document.getElementById("tags").getElementsByTagName("li");
	var taglength = tag.length;
	for(i=0; i<taglength; i++){
		tag[i].className = "";
	}
	selfObj.parentNode.className = "selectTag";
	var url=$.fn.getRootPath()+"/app/individual/individual!getindividual.htm?moduleId="+id;
	var _index = parent.layer.load("请稍候...");
	$.ajax({
			url : url,
			type : 'POST',
			//timeout : 15000,
			sync : false,
			dataType : 'html',
			success : function(data) {
				parent.layer.close(_index);
			   $("#tagContent").remove();
			   $(".individual_right").append(data);
			}
		});	
}

function　menuselect(self,img_url,flag,src){
    if(flag=='' || flag=='correction'){
       $(".individual_right ul").css("display","none");
       $("#tagContent0").css("display","none");
       $("#tagContent1").css("display","none");
       $("#menuframe").css("display","block");
       $("#menuframe").attr("src",$.fn.getRootPath() + src);
       if(flag=='correction'){
          $(".tags2").css("display","block");
          $(".individual_right ul li").removeClass("selectTag"); 
          $(".tags2 li").eq(0).addClass("selectTag");
       }
    }
    
    else{
       window.location.href=$.fn.getRootPath() +src;
    }
    
    
    
    $(".individual_left ul li a").css({"background":"","color":""});
    $(".individual_ico1").css({"background":"url("+$.fn.getRootPath()+"/resource/individual/images/individual/individual_ico1.png) no-repeat top center","color":""});
    $(".individual_ico2").css({"background":"url("+$.fn.getRootPath()+"/resource/individual/images/individual/individual_ico2.png) no-repeat top center","color":""});
    $(".individual_ico5").css({"background":"url("+$.fn.getRootPath()+"/resource/individual/images/individual/individual_ico5.png) no-repeat top center","color":""});
    $(".individual_ico3").css({"background":"url("+$.fn.getRootPath()+"/resource/individual/images/individual/individual_ico3.png) no-repeat top center","color":""});
    $(".individual_ico4").css({"background":"url("+$.fn.getRootPath()+"/resource/individual/images/individual/individual_ico4.png) no-repeat top center","color":""});
    $(".individual_circle").css({"background":"#666","color":"#FFF"});
    $(".individual_circle1").css({"background":"#666","color":"#FFF"});
    $(self).find(".individual_circle").css({"background":"#c22a2c","color":"#fff"});
    $(self).find(".individual_circle1").css({"background":"#c22a2c","color":"#fff"});
    $(self).css({"background":"url("+img_url+") no-repeat top center","color":"#c22a2c"});
    
    
}

function　more_info(type,method,src,tag){
    var module_id=$("#module_id").val();
    var _index = parent.layer.load("请稍候...");
    $.ajax({
			url : $.fn.getRootPath() + '/app/individual/individual!workflowcount.htm?moduleId='+module_id,
			type : 'POST',
			//timeout : 15000,
			sync : false,
			dataType : 'json',
			success : function(data) {
				parent.layer.close(_index);
				if (data==undefined || data==null) return false;
			   if(data.workflow1!=0){
			     $(".tags3 li a span").eq(0).html("("+data.workflow1+")");
			   }
			   if(data.workflow2!=0){
			     $(".tags3 li a span").eq(1).html("("+data.workflow2+")");
			   }
			   if(data.workflow3!=0){
			     $(".tags3 li a span").eq(2).html("("+data.workflow3+")");
			   }
			   if(data.workflow4!=0){
			     $(".tags3 li a span").eq(3).html("("+data.workflow4+")");
			   }
			   if(data.workflow5!=0){
			     $(".tags3 li a span").eq(4).html("("+data.workflow5+")");
			   }
			},
			error : function() {
				parent.layer.close(_index);
				alert('请求失败!');
			}
		});
    
    
    var url="";
    if(type!=''){
      url=$.fn.getRootPath()+src+"?type="+type+"&method="+method+"&moduleId="+module_id; 
    }
    else{
       url=$.fn.getRootPath()+src
    }
    var name=$(".tags1 .selectTag a span").html();
    $(".individual_right ul li").removeClass("selectTag"); 
    $("#"+method+"").addClass("selectTag");
    
    $(".individual_right ul").css("display","none");
    $(".rationalize_option ul").css("display","block");    
    $("."+tag+"").css("display","block");
    for(i=0; j=document.getElementById("tagContent"+i); i++){
		j.style.display = "none";
	}
	$(".rationalize_select_btn").html(name);	
    $("#menuframe").css("display","block");
	$("#menuframe").attr("src",url);
}

function selectchange(tag,url,id,name){
   $("#module_id").val(id);
   $(".tags3 li a span").html("");
   var _index = parent.layer.load("请稍候...");
   $.ajax({
			url : $.fn.getRootPath() + '/app/individual/individual!workflowcount.htm?moduleId='+$("#module_id").val(),
			type : 'POST',
			//timeout : 15000,
			sync : false,
			dataType : 'json',
			success : function(data) {
				parent.layer.close(_index);
				if (data==undefined || data==null) return false;
			   if(data.workflow1!=0){
			     $(".tags3 li a span").eq(0).html("("+data.workflow1+")");
			   }
			   if(data.workflow2!=0){
			     $(".tags3 li a span").eq(1).html("("+data.workflow2+")");
			   }
			   if(data.workflow3!=0){
			     $(".tags3 li a span").eq(2).html("("+data.workflow3+")");
			   }
			   if(data.workflow4!=0){
			     $(".tags3 li a span").eq(3).html("("+data.workflow4+")");
			   }
			   if(data.workflow5!=0){
			     $(".tags3 li a span").eq(4).html("("+data.workflow5+")");
			   }
			},
			error : function() {
				parent.layer.close(_index);
				alert('请求失败!');
			}
		});
   $(".rationalize_select_btn").html(name);
   $("."+tag+" li").removeClass();
   $("."+tag+" li").eq(0).addClass("selectTag"); 
   $(".individual_right ul").css("display","none");
   $(".rationalize_option").css("display","none"); 
   $(".rationalize_option ul").css("display","block");  
   $("."+tag+"").css("display","block");
   for(i=0; j=document.getElementById("tagContent"+i); i++){
		j.style.display = "none";
   }
   $("#menuframe").css("display","block");
   $("#menuframe").attr("src",$.fn.getRootPath()+url+"&moduleId="+id);
}
 
 
