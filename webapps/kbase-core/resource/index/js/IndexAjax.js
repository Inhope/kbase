$(function() {
	for (var i = 1; i <= 10; i++) {
		indexData(i);
	}
	
	//add by eko.zhan at 2016-06-20 13:30 面板内容为空宽度为84px，动态调整宽度
	//只会影响到面板内容为空出现错误样式的情况，正常情况下不会执行该代码
	if ($('.content_news1').width()<100){
		var _width = screen.width;
		$('.content_news1').width(_width/3);
	}
	
	/**************************************************************************************************/
	/*相关方法-利于拓展*/
	var that = window.IndexAjax = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	fn.extend({
		/*面板内容长度截取*/ 
		substring: function(str, isRight, len){
			if(!len) {/*未指定长度*/
				len = 20;
				if(isRight) len = 12;
			}/*如果已经指定长度，将使用指定长度不判断作用*/
			if(str.length < len) return str;
			else return str.substring(0, len) + '...';
		},
		myBrowser: function (){
		    var userAgent = navigator.userAgent; /*取得浏览器的userAgent字符串*/
		    var isOpera = userAgent.indexOf("Opera") > -1;
		    if (isOpera) return 'Opera'; /*判断是否Opera浏览器*/
			else if (userAgent.indexOf('Firefox') > -1) return 'FF'; /*判断是否Firefox浏览器*/
			else if (userAgent.indexOf('Chrome') > -1) return 'Chrome';
		    else if (userAgent.indexOf('Safari') > -1) return 'Safari'; /*判断是否Safari浏览器*/
		    else if (userAgent.indexOf('compatible') > -1 && 
		    	userAgent.indexOf('MSIE') > -1 && !isOpera) return 'IE'; /*判断是否IE浏览器*/
		    return '';
		}
	});
	that.extend({
		/*右侧点击标题收缩面板*/ 
		rightPanelObject: {
			rightLiPanel : $('div.content_you ul>li'),
			showLiClass : 'content_dq',
			renderEvent : function() {
				var self = this;
				$.each(self.rightLiPanel, function(i) {
					$(this).children(':eq(0)').children(':eq(0)').click(function() {
						var content = $(this).next();
						if(content.is(':hidden')) {
							content.slideDown(250, function() {
								$(this).next().show();
							});
							$(this).parent().parent().addClass(self.showLiClass);
						} else {
							content.slideUp(250, function() {
								$(this).parent().parent().removeClass(self.showLiClass);
							}).next().slideUp(250);
						}
					});
				});
				
			}
		},
		init: function(){
			/*右侧点击标题收缩面板*/ 
			that.rightPanelObject.renderEvent();
		}
	});
	that.init();
	/**************************************************************************************************/
});

function indexData(i) {
	var dataUrl = $.fn.getRootPath() + '/app/index/index!indexData.htm';
	$.ajax({
		url : dataUrl,
		type : 'POST',
		timeout : 5000,
		dataType : 'json',
		data : { dataType : i },
		success : function(result) {
			/*面板没有数据也显示更多——“快速链接”必须有更多才能操作*/
			if (result != null && result.length > -1){
				loadData(result, i);
			} 
		},
		error : function() {
			
		}
	});
}

var isGzyd = false;/*判断是否为广州移动项目*/
function loadData(data, i) {
	var leftMaxLen = 6;/*左侧能显示的最大条目数量*/
	if(data.length < leftMaxLen) leftMaxLen = data.length;
	/*面板显示是否显示在右侧*/
	var isRight = $('#dataType_' + i).attr('isRight') == 'isRight';
	
	if (i == 1) {//最新知识
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a  _id="' + data[k].valueId + '" title_="' + data[k].objectName + '"  title="' + data[k].question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].question, true) + '</a>';
			}
			$('#dataType_1 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a  _id="' + data[k].valueId + '" title_="' + data[k].objectName + '"  title="' + data[k].question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].question, false) + '</a>';
				html += '<span>' + data[k].createtime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_1 ul').html(html);
		}
//		
		$('#dataType_1 div[class$="_con"] a').click(function() {//点击事件
			parent.TABOBJECT.open({
				id : $(this).attr('title_'),
				name : $(this).attr('title_'),
				title : $(this).attr('title_'),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=3&askContent=' + encodeURIComponent($(this).attr('title')),
				isRefresh : true
			}, this);
		});
		$('#dataType_1 a.more').unbind('click');//更多点击事件
		$('#dataType_1 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'newKnowledges',
				name : '最新知识',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/index/knowledges!newKnowledges.htm',
				isRefresh : true
			}, this);
		});
	} else if (i == 2) {//最热知识
		//最热知识
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].name + '" id="' + data[k].objectId + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].name, true) + '</a>';
			}
			$('#dataType_2 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].name + '" id="' + data[k].objectId + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].name, false) + '</a>';
				html += '<span></span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_2 ul').html(html);
		}
//		
		/*div[class$="_con"] 限定点击事件的范围，原因会干扰面板标题事件 （$('#dataType_2 a') 会包含标题中的a标签）*/
		$('#dataType_2 div[class$="_con"] a').click(function() {//点击事件
			parent.TABOBJECT.open({
				id : $(this).attr("title"),
				name : $(this).attr("title"),
				title : $(this).attr("title"),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=3&objId=' + $(this).attr('id'),
				isRefresh : true
			}, this);
		});
		$('#dataType_2 a.more').unbind('click');//更多点击事件
		$('#dataType_2 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'hotKnowledges',
				name : '最热知识',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/index/knowledges!hotKnowledges.htm',
				isRefresh : true
			}, this);
		});
	
	} else if (i == 3) {//更新知识
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a  _id="' + data[k].valueId + '" title_="' + data[k].objectName + '"  title="' + data[k].question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].question, true) + '</a>';
			}
			$('#dataType_3 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a  _id="' + data[k].valueId + '" title_="' + data[k].objectName + '"  title="' + data[k].question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].question, false) + '</a>';
				html += '<span>' + data[k].edittime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_3 ul').html(html);
		}
//		
		$('#dataType_3 div[class$="_con"] a').click(function() {//点击事件
			parent.TABOBJECT.open({
				id : $(this).attr('title_'),
				name : $(this).attr('title_'),
				title : $(this).attr('title_'),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=3&askContent=' + encodeURIComponent($(this).attr('title')),
				isRefresh : true
			}, this);
		});
		$('#dataType_3 a.more').unbind('click');//更多点击事件
		$('#dataType_3 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'newKnowledges',
				name : '更新知识',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/index/knowledges!updateKnowledges.htm',
				isRefresh : true
			}, this);
		});
	} else if (i == 4) {//用户收藏夹
		var html = '';
		var frameId = new Date().getTime() + 'n';
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].favClipName + '" href="javascript:void(0);" id="' + data[k].id + '" frameId="' + frameId + '">';
				html += IndexAjax.fn.substring(data[k].favClipName, true) + '</a>';
			}
			$('#dataType_4 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].favClipName + '" href="javascript:void(0);" id="' + data[k].id + '" frameId="' + frameId + '">';
				html += IndexAjax.fn.substring(data[k].favClipName, false) + '</a>';
				html += '<span>' + data[k].createTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_4 ul').html(html);
		}
		
		$('#dataType_4 div[class$="_con"] a').click(function() {//点击事件
			//add by eko.zhan at 2016-07-29 10:30 固体配置的知识搜藏转向
			var title = '知识收藏';
			if ($(this).attr('id').length<32){
				title = $(this).text();
			}
			parent.TABOBJECT.open({
				id : frameId,
				name : title,
				hasClose : true,
				url : $.fn.getRootPath() + '/app/fav/fav-clip-object.htm?fid=' + $(this).attr('id'),
				isRefresh : true
			}, this);
		});
		$('#dataType_4 a.more').unbind('click');//更多点击事件
		$('#dataType_4 a.more').click(function() {
			parent.TABOBJECT.open({
				id : frameId,
				name : '知识收藏',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/fav/fav-clip.htm',
				isRefresh : true
			}, this);
		});
	}else if (i == 5) {//快速链接
		var html = '';
		var frameId = new Date().getTime() + 'n';
		if (isRight) {
			if(isGzyd){
				if(IndexAjax.fn.myBrowser() == 'IE')
					html += '<a href="###" ';
				else html += '<a href="javascript:void(0)" ';
				html += 'title="原子化知识库" onclick="javascript:qkLinklog(this);">';
				html += '原子化知识库</a>';
			}
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].name + '" href="' + data[k].address + '" target="_blank" >';
				html += IndexAjax.fn.substring(data[k].name, true) + '</a>';
			}
			$('#dataType_5 div.dq_con').html(html);
		} else{//左侧
			if(isGzyd){
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				if(IndexAjax.fn.myBrowser() == 'IE')
					html += '<a href="###" ';
				else html += '<a href="javascript:void(0)" ';
				html += 'title="原子化知识库" onclick="javascript:qkLinklog(this);">';
				html += '原子化知识库</a>';
				html += '<span></span>';
				html += '</li>';
			}
			for (var k = 0; k < leftMaxLen-1; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].name + '" href="' + data[k].address + '" target="_blank" >';
				html += IndexAjax.fn.substring(data[k].name, false) + '</a>';
				html += '<span>' + data[k].createTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_5 ul').html(html);
		}
		
		$('#dataType_5 a.more').unbind('click');//更多点击事件
		$('#dataType_5 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'quilLink',
				name : '快速链接',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/link/quik-link!.htm',
				isRefresh : true
			}, this);
		});
	}else if (i == 6) {//岗位知识
		var html = '', obj = $('#dataType_6');
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].name + '" id="' + data[k].objectId + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].name, true) + '</a>';
			}
			$(obj).find('div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].name + '" id="' + data[k].objectId + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].name, false) + '</a>';
				html += '<span>' + data[k].startTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$(obj).find('ul').html(html);
		}
		
		// 点击列
		$(obj).find('div[class$="_con"] a').click(function() {
			parent.TABOBJECT.open({
				id : $(this).attr("title"),
				name : $(this).attr("title"),
				title : $(this).attr("title"),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=4&objId=' + $(this).attr('id'),
				isRefresh : true
			}, this);
		});
		// 点击“更多”
		$(obj).find('a.more').unbind('click');
		$(obj).find('a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'roleKnowledge',
				name : '岗位知识',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/index/knowledges!roleKnowledges.htm',
				isRefresh : true
			}, this);
		});
	}else if (i == 7) {//公布栏
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].title + '" onclick="javascript:parent.getNoticeContent(\'' + data[k].id + '\')" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].title, true) + '</a>';
			}
			$('#dataType_7 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].title + '" onclick="javascript:parent.getNoticeContent(\'' + data[k].id + '\')" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].title, false) + '</a>';
				html += '<span>' + data[k].createTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_7 ul').html(html);
		}
//		
		$('#dataType_7 a.more').unbind('click');//更多点击事件
		$('#dataType_7 a.more').click(function() {
			if (isGzyd || window.__kbs_isBailian) {
				if ($(parent.document).find('#navBulletin').length>0){
					$(parent.document).find('#navBulletin').get(0).click();
				}else{
					var _diaWidth = screen.width-50;
					var _diaHeight = screen.height-100;
					var _scrWidth = screen.width;
					var _scrHegiht = screen.height;
					var _diaLeft = (_scrWidth-_diaWidth)/2;
					var _diaTop = (_scrHegiht-_diaHeight)/2;
					var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0, resizable=1, status=0, fullscreen=0';
					window.open($.fn.getRootPath() + '/app/notice/bulletin.htm', '_blank', params);
				}
			} else {
				parent.TABOBJECT.open({
					id : 'notice',
					name : '公告管理',
					hasClose : true,
					url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote=0',
					isRefresh : true
				}, this);
			}
		});
	
	}else if (i == 8) {//推荐
		//木有数据暂不处理
		//add by eko.zhan at 2016-06-05 14:00
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].kbVal.question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].kbVal.question, true) + '</a>';
			}
			$('#dataType_8 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].kbVal.question + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].kbVal.question, false) + '</a>';
				html += '<span>' + data[k].createTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_8 ul').html(html);
		}
		
		$('#dataType_8 div[class$="_con"]').on('click', 'a[class!="more"]', function(){
			var question = $(this).text();
			parent.TABOBJECT.open({
				id : question,
				name : question,
				title : question,
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=5&askContent=' + encodeURIComponent(question),
				isRefresh : true
			}, this);
		});
//		
		$('#dataType_8 a.more').unbind('click');//更多点击事件
		$('#dataType_8 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'recomKnow_more',
				name : '推荐知识',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/index/knowledges!recomKnowledges.htm',
				isRefresh : true
			}, this);
		});
	}else if (i == 9) {//培训
		//右侧
		if (isRight) {
			var html = '';
			for (var k = 0; k < data.length; k++) {
				html += '<a  _id="' + data[k].id + '" _type="' + data[k].type + '"  title="' + data[k].name + '" _courseId="' + data[k].courseId + '" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].name, true) + '</a>';
			}
			$('#dataType_9 div.dq_con').html(html);
			$('#dataType_9 div.dq_more').remove();//因为是培训和考试混合的数据，更多不知道指向何处
			
			$('#dataType_9 div.dq_con a').click(function() {//点击事件
				clickData(this);
			});
		}else{
			var btnHtml = "";
			btnHtml += '<span id="trainOrExam">';
			btnHtml += '<a href="javascript:void(0);">培训</a>|<a href="javascript:void(0);">考试</a>';
			btnHtml += '</span>';
			$('#dataType_9 .content_news1_title').append(btnHtml);
			appendLearnHtml(data,'');
			//我的课堂-培训、考试点击事件
			$(".content_index_news").on("click","#dataType_9 #trainOrExam a",function(){
				if(!$(this).hasClass("dangqian")) {
					$(this).parent().find("a").removeClass("dangqian");
					$(this).addClass("dangqian");
					var type = $(this).text()=='考试'?'exam':'train';
					var url = "";
					if(type == 'exam'){
						url = $.fn.getRootPath() + '/app/index/index!getUserExamList.htm';
					}else if(type == 'train'){
						url = $.fn.getRootPath() + '/app/index/index!getUserTrainList.htm'
					}
					$.ajax({
						url : url,
						type : 'POST',
						timeout : 5000,
						dataType : 'json',
						success : function(data) {
							if(data.success) {
								data = data.data;
								appendLearnHtml(data,type);
							}
						}
					});
				}
			});
		}
	}else if (i == 10) {//便签
		var html = '';
		//右侧
		if (isRight) {
			for (var k = 0; k < data.length; k++) {
				html += '<a title="' + data[k].title + '" onclick="javascript:parent.getNoteContent(\'' + data[k].id + '\')" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].title, true) + '</a>';
			}
			$('#dataType_10 div.dq_con').html(html);
		} else{//左侧
			for (var k = 0; k < leftMaxLen; k++) {
				html += '<li>';
				html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
				html += '<a title="' + data[k].title + '" onclick="javascript:parent.getNoteContent(\'' + data[k].id + '\')" href="javascript:void(0);">';
				html += IndexAjax.fn.substring(data[k].title, false) + '</a>';
				html += '<span>' + data[k].startTime + '</span>';
				html += '</li>';
			}
			html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
			$('#dataType_10 ul').html(html);
		}
//		
		$('#dataType_10 a.more').unbind('click');//更多点击事件
		$('#dataType_10 a.more').click(function() {
			parent.TABOBJECT.open({
				id : 'note',
				name : '便签管理',
				hasClose : true,
				url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote=1',
				isRefresh : true
			}, this);
		});
	}
	$('div.content_news1').css('width','98%');//解决chrome显示bug
}



function qkLinklog(obj){
	$.post($.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!qkLinkLog.htm',
		{title: $(obj).attr('title')}, function(jsonResult){ }, 'json');
}

//我的课堂-数据点击事件
function clickData(_this){
	var url = "";
	var id = "learnCenter";
	var name = "个人中心";
	var title = "个人中心";
	if($(_this).attr("_type") == 'train'){
		url = $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+$(_this).attr("_courseId");
		id = "courseInfo";
		name = "课程学习";
		title = "课程学习";
	}else if($(_this).attr("_type") == 'exam'){
		url = $.fn.getRootPath() + '/app/learning/examinee.htm';
	}
	parent.TABOBJECT.open({
		id : id,
		name : name,
		title : title,
		hasClose : true,
		url : url,
		isRefresh : true
	}, this);
}
//我的课堂-刷新数据列表
function appendLearnHtml(data,type){
	$('#dataType_9 ul').children().remove();
	var html = "";
	for (var k = 0; k < data.length; k++) {
		html += '<li>';
		html += '<img height="3" wclassth="3" src="/kbase-core/theme/blue/resource/index/images/index_dian.jpg">';
		html += '<a  _id="' + data[k].id + '" _title="' + data[k].name + '" _type="' + data[k].type + '" _courseId="' + data[k].courseId + '" href="javascript:void(0);">';
		html += data[k].type=='exam'?'<font color="red">【考试】</font>':'<font color="green">【培训】</font>';
		if(data[k].name.length > 20){
			html += data[k].name.substring(0,20) + '...' + '</a>';
		}else{
			html += data[k].name + '</a>';
		}
		html += '<span>' + data[k].time + '</span>';
		html += '</li>';
	}
	if (data.length > 0){
		html += '<li class="content_news1_more"><a href="javascript:void(0);" class="more">更多</a></li>';
	}
	$('#dataType_9 ul').html(html);
	//更多点击事件
	$('#dataType_9 a.more').unbind('click');
	$('#dataType_9 a.more').click(function() {
		parent.TABOBJECT.open({
			id : "learnCenter",
			name : "个人中心",
			title : "个人中心",
			hasClose : true,
			url : $.fn.getRootPath() + '/app/learning/learn.htm',
			isRefresh : true
		}, this);
	});
	//数据列表点击事件
	$('#dataType_9 ul a:not(:last)').click(function() {
		clickData(this);
	});
}
