$(function(){
	/***************加载分页********************/
	fenye();
	/******************p4**********************/
//	$('iframe').load(function(){
//		var height = $(this).contents().find("body").height();
//		console.log(height);
//		$(this).height(height);
//	});
	/****************显示答案*******************/
	showAns();
	/***************预览*******************/
	yulan();
	/****************详情****************/
	detail();
	/**************排序******************/
	order();
		
});


function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
	var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
	var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
	var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
		
	});
	function fenyeSubmit(start){
		$('#fenyeForm input[name="start"]').val(start);
		$('#fenyeForm').submit();
	}
}

function showAns(){
	$('h1 a').click(function(){
		var ans = $(this).parent().parent().next();
		if(ans.is(":hidden")){
			ans.show();
		}else{
			ans.hide();
		}
	});
}

function yulan(){
	$('div.yiban_title_right input').removeAttr('checked');
	
	$('div.yiban_title_right a').click(function(){
		if($(this).next().attr('checked') != 'checked'){
			$(this).next().attr('checked','checked');
			$('h6 a').parent().parent().next().show();
		}else{
			$(this).next().removeAttr('checked');
			$('h6 a').parent().parent().next().hide();
		}
	});
	$('div.yiban_title_right input').click(function(){
		if($(this).attr('checked') == 'checked'){
			$('h6 a').parent().parent().next().show();
		}else{
			$('h6 a').parent().parent().next().hide();
		}
	});
}

//function order(){
//	var asc = $('#fenyeForm input[name="asc"]').val();
//	var order = $('#fenyeForm input[name="order"]').val();
//	if(asc=='true'){
//		if(order == 'create_time'){
//			$('#createClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico3.jpg) no-repeat right center');
//		}else if(order == 'edit_time'){
//			$('#editClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico3.jpg) no-repeat right center');
//		}
//	}else if(asc=='false'){
//		if(order == 'create_time'){
//			$('#createClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico1.jpg) no-repeat right center');
//		}else if(order == 'edit_time'){
//			$('#editClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico1.jpg) no-repeat right center');
//		}
//	}
//	
//	$('#createClick').click(function(){
//		$('#fenyeForm  input[name="order"]').val('create_time');
//		$('#fenyeForm').submit();
//	});
//	$('#editClick').click(function(){
//		$('#fenyeForm  input[name="order"]').val('edit_time');
//		$('#fenyeForm').submit();
//	});
//	
//}


function detail(){
	$('a.detail').click(function(){
		var question = $(this).attr('href');
		parent.TABOBJECT.open({
			id : 'values',
			name : '知识展示',
			hasClose : true,
			url : $.fn.getRootPath()+"/app/search/search.htm?searchMode="+searchMode+"&askContent="+encodeURIComponent(question),
			isRefresh : true
		}, this);
		return false;
	});
	
}
function order(){
	$('div.yiban_con_title h5 a').click(function(){
		var desc = 'asc';
		var orderFiled = 'hot';
		if($(this).attr('desc')=='asc'){
			desc = 'desc';
		}
		$('#fenyeForm input[name="desc"]').val(desc);
		$('#fenyeForm input[name="orderFiled"]').val(orderFiled);
		$('#fenyeForm').submit();
	});
}