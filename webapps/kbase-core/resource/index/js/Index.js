$(function() {
	//add by eko.zhan at 2016-06-20 13:30 面板内容为空宽度为84px，动态调整宽度
	//只会影响到面板内容为空出现错误样式的情况，正常情况下不会执行该代码
	if ($('.content_news1').width()<100){
		var _width = screen.width;
		$('.content_news1').width(_width/3);
	}

	var rightPanelObject = {
		rightLiPanel : $('div.content_you ul>li'),
		showLiClass : 'content_dq',
		hideOtherMenuBox : function(obj) {
			var o = this.rightLiPanel.not(obj);
			o.children('div.dq_con').slideUp(450, function() {
				o.removeClass(self.showLiClass);
			}).next().slideUp(450);
		},
		renderEvent : function() {
			var self = this;
			
			$.each(self.rightLiPanel, function(i) {
				$(this).children(':eq(0)').click(function() {
					var content = $(this).next();
				
					if(content.is(':hidden')) {
						content.slideDown(250, function() {
							$(this).next().show();
						});
						$(this).parent().addClass(self.showLiClass);
//						self.hideOtherMenuBox($(this).parent());
					} else {
						content.slideUp(250, function() {
							$(this).parent().removeClass(self.showLiClass);
						}).next().slideUp(250);
					}
				});
			});
			
		}
	}
	
	rightPanelObject.renderEvent();
	
	var favPanelObject = {
		frameId : new Date().getTime() + 'n',
		favObjectEl : $('div#favClipDiv ul li a'),
		favObjectLeftEl : $('div#favClipDiv1>a'),
		favObjectMoreEl : $('a[id=favClipMore]'),
		onlyObjectClick : function() {
			this.favObjectEl.attr('frameId', this.frameId).click(function() {
				doclick(this);
			});
			this.favObjectLeftEl.attr('frameId', this.frameId).click(function() {
				doclick(this);
			});
			
			function doclick(object) {
				var fid = $(object).attr('id');
				window.parent.TABOBJECT.open({
					id : $(object).attr('frameId'),
					name : '知识收藏',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/fav/fav-clip-object.htm?fid=' + fid,
					isRefresh : true
				}, object);
			}
		},
		moreObjectClick : function() {
			this.favObjectMoreEl.attr('frameId', this.frameId).click(function() {
				var frameId = $(this).attr('frameId');
				window.parent.TABOBJECT.open({
					id : frameId,
					name : '知识收藏',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/fav/fav-clip.htm',
					isRefresh : true
				}, this);
			});
		},
		renderEvent : function() {
			this.onlyObjectClick();
			this.moreObjectClick();
		}
	}
	
	favPanelObject.renderEvent();
	
	var noticePanelObject = {
		noticeMoreBtnEl : $('a[id=noticeMoreBtn]'),
		moreClick : function() {
			this.noticeMoreBtnEl.click(function() {
				//modify by eko.zhan at 2016-02-22 广东移动更多打开新公告管理
				if (parent.categoryTag=='true'){
					$(parent.document).find('#navBulletin').get(0).click();
				}else{
					parent.TABOBJECT.open({
						id : 'notice',
						name : '公告管理',
						hasClose : true,
						url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote=0',
						isRefresh : true
					}, this);
				}
			});
		},
		renderEvent : function() {
			this.moreClick();
		}
	}
	noticePanelObject.renderEvent();
	
	var notePanelObject = {
		noteMoreBtnEl : $('a[id=noteMoreBtn]'),
		moreClick : function() {
			this.noteMoreBtnEl.click(function() {
				parent.TABOBJECT.open({
					id : 'note',
					name : '便签管理',
					hasClose : true,
					url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote=1',
					isRefresh : true
				}, this);
			});
		},
		renderEvent : function() {
			this.moreClick();
		}
	}
	notePanelObject.renderEvent();

	//最热知识的年月日的事件处理
	var periodObject = {
		period : $('span[id=period] a'),
		activityClass : 'dangqian',
		dataGrid : $('div#hotDiv>ul'),
		periodClick : function() {
			var self = this;
			self.period.click(function() {
				if(!$(this).hasClass(self.activityClass)) {
					self.period.addClass(self.activityClass).not($(this)).removeClass(self.activityClass);
					self.loadData($(this));
				}
			});
		},
		loadData : function(p) {
			var self = this;
			$.ajax({
				url : $.fn.getRootPath() + '/app/index/index!getHotKnowlegesWithPeriod.htm',
				type : 'POST',
				timeout : 5000,
				dataType : 'json',
				data : {
				    period : p.text()
				},
				success : function(data, textStatus, jqXHR) {
					if(data.success) {
						self.dataGrid.children(':not(:last)').remove();
						for(var i = 0; i < data.data.length; i ++) {
							var li = $('<li></li>');
							li.html('<img height="3" wclassth="3" src="' + $.fn.getRootPath() + '/theme/' + parent.USER_THEME + '/resource/index/images/index_dian.jpg"><a id=' + data.data[i].objectId + ' title=' + data.data[i].name + ' href="javascript:void(0);">' + data.data[i].name + '</a>');
							self.dataGrid.children(':last').before(li);
						}
						$('div#hotDiv a:not(:last)').click(function() {
							parent.TABOBJECT.open({
								id : $(this).attr("title"),
								name : $(this).attr("title"),
								title : $(this).attr("title"),
								hasClose : true,
								url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=3&objId=' + $(this).attr('id'),
								isRefresh : true
							}, this);
						});
					}
				},
				error : function(data, textStatus, errorThrown) {
					
				}
			});
		},
		renderEvent : function() {
			this.periodClick();
		}
	}
	
	periodObject.renderEvent();
	
	//最新知识跳转----左侧 title_ 实例名称
	$('div#newPanel ul li a:not(:last)').click(function() {
		_openLatest($(this).attr('_id'), $(this).attr('title'), $(this).attr('title_'));
	});
	
	//最新知识跳转----左侧（更多）
	$('div#newPanel ul li a:last').click(function() {
		parent.TABOBJECT.open({
			id : 'newKnowledges',
			name : '最新知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!newKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//最新知识跳转----右侧 title_ 实例名称
	$('div#newPanel1 a').click(function() {
		_openLatest($(this).attr('_id'), $(this).attr('title'), $(this).attr('title'));
	});
	
	//打开最新知识-内部方法
	function _openLatest(id, question, objname){
		var _url = '';
		if (pageContext.isWukong==1){
			_url = pageContext.contextPath + '/app/wukong/search!detail.htm?id=' + id;
		}else{
			_url = $.fn.getRootPath() + '/app/search/search.htm?searchMode=3&askContent=' + encodeURIComponent(question)
		}
		
		/*当前点击dom， 是否开启业务模板， 业务模板-文章id*/
	    var currentDom = $('a[_id=' + id + ']:eq(0)'),
	    	bizTplEnable = $(currentDom).attr('bizTplEnable'), bizTplArticleId = $(currentDom).attr('bizTplArticleId');
	    if (bizTplEnable == 'true' && bizTplArticleId) {
	    	/*如果开启业务模板，来自业务模板的实例调整新的页面*/
	    	_url =  $.fn.getRootPath() + '/app/biztpl/article-search.htm?&articleId=' + bizTplArticleId;
	    	_url += '&spotId=' + id;
	    }
	    	
		/*打开弹窗*/
		parent.TABOBJECT.open({
			id : objname,
			name : objname,
			title : objname,
			hasClose : true,
			url : _url,
			isRefresh : true
		}, this);
	}
	
	//最新知识更多---右侧
	$('div#newPanel1').next().click(function() {
		parent.TABOBJECT.open({
			id : 'newKnowledges',
			name : '最新知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!newKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//最热知识跳转---左侧
	$('div#hotDiv a:not(:last)').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=3&objId=' + encodeURIComponent($(this).attr('id')),
			isRefresh : true
		}, this);
	});
	
	//最热知识更多跳转---左侧
	$('div#hotDiv a:last').click(function() {
		parent.TABOBJECT.open({
			id : 'hotKnowledges',
			name : '最热知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!hotKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//最热知识跳转---右侧
	$('div#hotDiv1 a').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=3&objId=' + encodeURIComponent($(this).attr('id')),
			isRefresh : true
		}, this);
	});
	
	//最热知识更多跳转---右侧
	$('div#hotDiv1').next().click(function() {
		parent.TABOBJECT.open({
			id : 'hotKnowledges',
			name : '最热知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!hotKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//岗位知识跳转---左侧
	$('div#roleKnowledgePanel a:not(:last)').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=4&objId=' + $(this).attr('id'),
			isRefresh : true
		}, this);
	});
	
	//更多岗位知识跳转---左侧
	$('div#roleKnowledgePanel a:last').click(function() {
		parent.TABOBJECT.open({
			id : 'roleKnowledge',
			name : '岗位知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!roleKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//岗位知识跳转---右侧
	$('div#roleKnowledgePanel1 a').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=4&objId=' + $(this).attr('id'),
			isRefresh : true
		}, this);
	});
	
	//更多岗位知识--右侧
	$('div#roleKnowledgePanel1').next().click(function() {
		parent.TABOBJECT.open({
			id : 'roleKnowledge',
			name : '岗位知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!roleKnowledges.htm',
			isRefresh : true
		}, this);
	});
	//快速链接--更多-----------左侧
	$('#more_quick_link').click(function(){
		parent.TABOBJECT.open({
		id : 'quilLink',
		name : '快速链接',
		hasClose : true,
		url : $.fn.getRootPath() + '/app/link/quik-link!.htm',
		isRefresh : true
		}, this);
	});
	//快速链接--更多-----------右侧
	$('#quick_link_more a').click(function(){
		parent.TABOBJECT.open({
		id : 'quilLink',
		name : '快速链接',
		hasClose : true,
		url : $.fn.getRootPath() + '/app/link/quik-link!.htm',
		isRefresh : true
		}, this);
	});
	//推荐知识---左侧 title_ 实例名称
	$('div#recom_object_left ul li a:not(:last)').click(function(){
		parent.TABOBJECT.open({
			id : $(this).attr("title_"),
			name : $(this).attr("title_"),
			title : $(this).attr("title_"),
			hasClose : true,
			//url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=5&objId=' + $(this).attr('id'),
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=5&askContent=' + encodeURIComponent($(this).attr("title")),
			isRefresh : true
		}, this);
	});
	//推荐知识---右侧 title_ 实例名称
	$('div#recom_object_right a').click(function(){
		parent.TABOBJECT.open({
			id : $(this).attr("title_"),
			name : $(this).attr("title_"),
			title : $(this).attr("title_"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=5&askContent=' + encodeURIComponent($(this).attr("title")),
			isRefresh : true
		}, this);
	});
	//推荐知识---更多----左侧
	$('div#recom_object_left ul li a:last').click(function() {
		parent.TABOBJECT.open({
			id : 'recomKnow_more',
			name : '推荐知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!recomKnowledges.htm',
			isRefresh : true
		}, this);
	});
	//推荐知识---更多----右侧
	$('div#recom_object_more_right a').click(function() {
		parent.TABOBJECT.open({
			id : 'recomKnow_more',
			name : '推荐知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!recomKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	
	//最新知识跳转----左侧 title_ 实例名称
	$('div#updatePanel ul li a:not(:last)').click(function() {
		_openUpdatest($(this).attr('_id'), $(this).attr('title'), $(this).attr('title_'));
		/*parent.TABOBJECT.open({
			id : $(this).attr("title_"),
			name : $(this).attr("title_"),
			title : $(this).attr("title_"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=11&askContent=' + encodeURIComponent($(this).attr("title")),
			isRefresh : true
		}, this);*/
	});
	
	//更新知识跳转----左侧（跟多）
	$('div#updatePanel ul li a:last').click(function() {
		parent.TABOBJECT.open({
			id : 'updateKnowledges',
			name : '更新知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!updateKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//更新知识跳转----右侧 title_ 实例名称
	$('div#updatePanel1 a').click(function() {
		_openUpdatest($(this).attr('_id'), $(this).attr('title'), $(this).attr('title_'));
		/*
		parent.TABOBJECT.open({
			id : $(this).attr("title_"),
			name : $(this).attr("title_"),
			title : $(this).attr("title_"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=11&askContent=' + encodeURIComponent($(this).attr("title")),
			isRefresh : true
		}, this);*/
	});
	
	//打开更新知识
	function _openUpdatest(id, question, objname){
		_openLatest(id, question, objname);
	}
	
	//更新知识更多---右侧
	$('div#updatePanel1').next().click(function() {
		parent.TABOBJECT.open({
			id : 'updateKnowledges',
			name : '更新知识',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/index/knowledges!updateKnowledges.htm',
			isRefresh : true
		}, this);
	});
	
	//我的课堂
	var trainOrExamObject = {
		trainOrExam : $('span[id="trainOrExam"] a'),
		activityClass : 'dangqian',
		dataGrid : $('div#trainOrExamDiv>ul'),
		trainOrExamClick : function() {
			var self = this;
			self.trainOrExam.click(function() {
				if(!$(this).hasClass(self.activityClass)) {
					self.trainOrExam.addClass(self.activityClass).not($(this)).removeClass(self.activityClass);
					self.loadData($(this));
				}
			});
		},
		loadData : function(p) {
			var url = "";
			if(p.text() == '考试'){
				url = $.fn.getRootPath() + '/app/index/index!getUserExamList.htm';
			}else if(p.text() == '培训'){
				url = $.fn.getRootPath() + '/app/index/index!getUserTrainList.htm'
			}
			var self = this;
			$.ajax({
				url : url,
				type : 'POST',
				timeout : 5000,
				dataType : 'json',
				//data : {},
				success : function(data) {
					if(data.success) {
						self.dataGrid.children(':not(:last)').remove();
						for(var i = 0; i < data.data.length; i ++) {
							var li = $('<li></li>');
							var html = '<img height="3" wclassth="3" src="' + $.fn.getRootPath() + '/theme/' + parent.USER_THEME + '/resource/index/images/index_dian.jpg">';
							html += '<a _courseId='+data.data[i].courseId+' _type=' + data.data[i].type + ' _id=' + data.data[i].id + ' _title=' + data.data[i].name + ' href="javascript:void(0);">';
							if(p.text() == '考试'){
								html += '<font color="red">【考试】</font>';
							}else if(p.text() == '培训'){
								html += '<font color="green">【培训】</font>';
							}
							if(data.data[i].name.length > 20){
								html += data.data[i].name.substring(0,20) + '...';
							}else{
								html += data.data[i].name;
							}
							html += '</a><span>'+data.data[i].time+'</span>';
							li.html(html);
							self.dataGrid.children(':last').before(li);
						}
					}
				}
			});
		},
		renderEvent : function() {
			this.trainOrExamClick();
		}
	}
	trainOrExamObject.renderEvent();
	//我的课堂-数据列表-点击事件
	$('div#trainOrExamDiv').on("click","a:not(:last)",function() {
		var url = "";
		var id = "learnCenter";
		var name = "个人中心";
		var title = "个人中心";
		if($(this).attr("_type") == 'train'){
			url = $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+$(this).attr("_courseId");
			id = "courseInfo";
			name = "课程学习";
			title = "课程学习";
		}else if($(this).attr("_type") == 'exam'){
			url = $.fn.getRootPath() + '/app/learning/examinee.htm';
		}
		parent.TABOBJECT.open({
			id : id,
			name : name,
			title : title,
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	});
	//我的课堂-更多-点击事件
	$('div#trainOrExamDiv').on("click","#ceMore a",function() {
		var url = $.fn.getRootPath() + '/app/learning/learn.htm';
		console.info(url);
		parent.TABOBJECT.open({
			id : "learnCenter",
			name : "个人中心",
			title : "个人中心",
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	});
	//我的课堂-显示更多
	$("#trainOrExam").on("click","a",function(){
		$("#ceMore").show();
	});
	//我的课堂---右侧点击
	$('div#rightTrainOrExam').on("click","a",function() {
		var url = "";
		var id = "learnCenter";
		var name = "个人中心";
		var title = "个人中心";
		if($(this).attr("_type") == 'train'){
			url = $.fn.getRootPath() + '/app/learning/learn!courseInfo.htm?courseId='+$(this).attr("_courseId");
			id = "courseInfo";
			name = "课程学习";
			title = "课程学习";
		}else if($(this).attr("_type") == 'exam'){
			url = $.fn.getRootPath() + '/app/learning/examinee.htm';
		}
		parent.TABOBJECT.open({
			id : id,
			name : name,
			title : title,
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	});
});