$(function(){
	var recommendObject = {
		//弹出框体
		remPanel : $('div.rem-ball'),
		//弹出框体，保存按钮
		btnSaveRemmend : $('#recommendCommit'),
		
		//全选与取消全选
		btnRemmend : $('#recommendbtn'),
		selAll : $(':checkbox.sel_all'),
		selOne : $(':checkbox.sel_one'),
		editBtn : $('#edit'),
		delBtn : $('#del'),
		
		//部门树
		zTree_D : null,
		
		//显示推荐面板
		showRemPanel : function(recomid,objId){
			var _this = this;
			$(document).showShade();
			this.remPanel.css({
				'top' : '50px',
				'left' : ($(window).width() - this.remPanel.width()) / 2 + 'px',
				'position' : 'absolute',
				'zIndex' : '10009'
			}).show();
			if(recomid == null || recomid == ''){
				//标准问详情页面进入，faqId全局变量
				objId = faqId;
				this.btnSaveRemmend.val('推荐');
			}else{
				//列表页面进入,将数据添加在弹出窗体保存按钮上
				this.btnSaveRemmend.val('修改');
			}
			this.btnSaveRemmend.attr('faqId',objId);//标准问id
			
			
			//数据回显 -------------------------------------------------------- 开始
			$.post($.fn.getRootPath()+"/app/recommend/recommend!getRecommendObject.htm",
				{'objId':objId},
				function(data){
					$('input[name="rem_time"]').each(
						function(){
							if($(this).val() == data.rem_time){
								$(this).attr("checked","checked");
							}
						}
					);
								
					$('#depIds_select').val(data.deptIds);
					$('#depNames_select').val(data.deptNames);
					$('#depPIds_select').val(data.PIds);
					$('#status_select').val(0);
					
					//部门岗位树初始化(参见：Recommend.jsp 中部门岗位树)
					var zTreeObj = $.fn.zTree.getZTreeObj("treeDemo");
					if(zTreeObj != null) zTreeObj.destroy();
					$.fn.zTree.init($("#treeDemo"), setting);
			});
			//数据回显 -------------------------------------------------------- 结束
		},
		//关闭推荐面板
		closeRemPanel : function(){
			$(document).hideShade();
			this.remPanel.hide();
		},
		init : function(){
			var self = this;
			//推荐按钮
			this.btnRemmend.click(function(){
				//显示推荐弹出窗体
				self.showRemPanel();
			});
			//编辑——列表
			this.editBtn.click(function(){
				var acount = 0;
				for(var i=0; i<self.selOne.length; i++){
					if($(self.selOne[i]).attr('checked') == 'checked'){
						acount ++;
					}
				}
				if(acount == 1){
					//self.showRemPanel($('input[type="checkbox"]:checked').attr('id'),$('input[type="checkbox"]:checked').attr('objId'));
					parent.__kbs_layer_index = parent.$.layer({
						type : 2,
						border : [10, 0.3, '#000'],
						title : false,
						closeBtn : [0, true],
						iframe : {
							src : $.fn.getRootPath() + '/app/recommend/recommend!open.htm?id=' + $('input[type="checkbox"]:checked').attr('id')
						},
						area : ['540', '480']
					});
				}else if(acount > 1){
					parent.$(document).hint('只能选择一项进行修改');
				}else{
					parent.$(document).hint('请选择一项进行修改');
				}
			});
			//删除——列表
			this.delBtn.click(function(){
				var acount = 0;
				for(var i=0; i<self.selOne.length; i++){
					if($(self.selOne[i]).attr('checked') == 'checked'){
						acount ++;
					}
				}
				if(acount==0){
					parent.$(document).hint('请选择待删除的数据');
					return false;
				}
				parent.layer.confirm('确定删除吗？', function(){
					$('body').ajaxLoading('正在删除数据...');
					
			    	var recomids = '';
					for(var i=0; i<self.selOne.length; i++){
						if($(self.selOne[i]).attr('checked') == 'checked'){
							if(recomids == ''){
								recomids += $(self.selOne[i]).attr('id');
							}else{
								recomids += ','+$(self.selOne[i]).attr('id');
							}
						}
					}
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath()+"/app/recommend/recommend!deleteRecom.htm",
						data : {
							recomids : recomids
						},
						dataType : 'json',
						success : function(data){
							$(parent.document).hint(data.message);
							if(data.success){
								 for(var i=0; i<self.selOne.length; i++){
									if($(self.selOne[i]).attr('checked') == 'checked'){
										$(self.selOne[i]).parent().parent().hide();
										$(self.selOne[i]).removeAttr('checked');
									}
								}
							}
							$('body').ajaxLoadEnd();
						},
						error : function(){
							//parent.$(document).hint('出错了');
							$(parent.document).hint('出错了');
						}
					});
				});
			});
			this.selAll.click(function(){
				if($(self.selAll).attr('checked') == 'checked'){
					for(var i=0; i<self.selOne.length; i++){
						$(self.selOne[i]).attr('checked','checked');
					}
				}else{
					for(var i=0; i<self.selOne.length; i++){
						$(self.selOne[i]).removeAttr('checked');
					}
				}
			});
			this.selOne.click(function(){
				var acount = 0;
				for(var i=0; i<self.selOne.length; i++){
					if($(self.selOne[i]).attr('checked') == 'checked'){
						acount ++;
					}
				}
				if(acount != self.selOne.length){
					$(self.selAll).removeAttr('checked');
				}
			});
		}
	}
	recommendObject.init();
});