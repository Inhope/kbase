$(function() {
	$("#month").bind('click',function(){
		$("#startTime").val("");
		$("#endTime").val("");
		WdatePicker({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false })
	})
	$("#startTime").bind('focus',function(){
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		var mindate=$("#month").val()+"-01";
		WdatePicker({minDate:mindate,maxDate:maxdate});
	})
	$("#endTime").bind('focus',function(){
		var mindate=$("#month").val()+"-01";
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		if($("#startTime").val()!=''){
			mindate=$("#startTime").val();
		}
		WdatePicker({minDate:mindate,maxDate:maxdate});
	})
	var tableObject = {
		tableEl : $('table'),
		forms : {
			startTime : $('#startTime'),
			endTime : $('#endTime'),
			month : $('#month'),
			startTimeUi : null,
			endTimeUi : null,
			categoryName : $('#categoryName'),
			objectContent : $('#objectContent'),
			question : $('#question'),
			dept : $('#dept'),
			person : $('#person'),
			searchBtn : $('#searchBtn'),
			exportBtn : $('#exportBtn'),
			/**
			dateSetting : {
				required : false,
				closeText : '关闭',
				currentText : '今天',
				okText : '确定',
				editable : false,
				formatter :function(datetime){
					var year = datetime.getFullYear();
					var month = datetime.getMonth();
					month = month + 1;
					month = month < 10 ? '0' + month : month;
					var date = datetime.getDate();
					date = date < 10 ? '0' + date : date;
					var hour = datetime.getHours();
					hour = hour < 10 ? '0' + hour : hour;
					var minute = datetime.getMinutes();
					minute = minute < 10 ? '0' + minute : minute;
					return year + '-' + month + '-' + date + ' ' + hour + ':' + minute + ':00';
				}
			},
			forIe6 : function() {
				this.month.next().find('input').css({
					'border' : '0',
					'width' : '128px'
				}).parent().parent().css({
					'position' : 'relative'
				});
				
				this.startTime.next().find('input').css({
					'border' : '0',
					'width' : '128px'
				}).parent().parent().css({
					'position' : 'relative'
				});
				
				this.startTime.next().find('input').next().css({
					'position' : 'absolute',
					'top' : '7px'
				});
	
				this.endTime.next().find('input').css({
					'border' : '0',
					'width' : '128px'
				}).parent().parent().css({
					'position' : 'relative'
				});
				
				this.endTime.next().find('input').next().css({
					'position' : 'absolute',
					'top' : '7px'
				});
			},
			**/
			validation : function() {
				var startTime = this.startTime.val();
				var endTime = this.endTime.val();
				if(startTime && endTime) {
					var s = new Date(startTime.replace(/-/gi, '/')).getTime();
					var e = new Date(endTime.replace(/-/gi, '/')).getTime();
					if(s > e) {
						parent.layer.alert('开始时间不能晚于结束时间', -1);
						return false;
					}
				}
				return true;
			},
			
			getValues : function() {
				return {
					month : this.month.val(),
					startTime : this.startTime.val(),
					endTime : this.endTime.val(),
					categoryBh : this.categoryName.attr('bh'),
					objectName : this.objectContent.val(),
					question : this.question.val(),
					deptBh : this.dept.attr('bh'),
					personName : this.person.val()
				}
			}
		},
		pager : null,
		tableAttributes : {
			title : '知识点选量统计查询',
			width : $(window).width() - 16,
			height : '445',
			nowrap : true,
			autoRowHeight : false,
			striped : true,
			collapsible : false,
//			url: $.fn.getRootPath() + '/app/statement/click-amount!list.htm',
			method : 'POST',
			sortOrder : 'desc',
			remoteSort : false,
			loadMsg:'加载中...',
			pagination:true,
			rownumbers:true,
			singleSelect: true,
			toolbar : '#searchCondition',
			fitColumns : true,
			fit: true,
			columns : [
				[
					{field:'question',title:'标准问',width:200},
					{field:'objectName',title:'文章名',width:200},
					{field:'categoryName',title:'所属分类',width:200},
					{field:'personName',title:'操作人员',width:100},
					{field:'deptName',title:'所属部门',width:180},
					{field:'createTime',title:'访问时间',width:120}
				]
			]
		},
		pagerAttributes : {
			pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10, 20, 50],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        onBeforeRefresh:function(){
	        	
	        }
		},
		loadData : function(url) {
			if(url){
				!this.forms.validation() || this.tableEl.datagrid({
					url : url,    
					queryParams : this.forms.getValues()
				});
			}else{
				this.tableEl.datagrid('load', this.forms.getValues());
			}
		},
		/*******渲染表格*******/
		
		render : function() {
		
			var self = this;
			//初始化表格
			this.tableEl.datagrid(this.tableAttributes);
			this.pager = this.tableEl.datagrid('getPager');
			$(this.pager).pagination(this.pagerAttributes);
			/**
			//初始化日期时间控件
			this.forms.month.datebox({
				formatter: function(date){
					var y = date.getFullYear();
					var m = date.getMonth()+1;
					return y+'-'+(m<10?('0'+m):m);
				},
				parser: function(s){
					if (!s) return new Date();
					var ss = s.split('-');
					var y = parseInt(ss[0],10);
					var m = parseInt(ss[1],10);
					var d = 1;
					if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
						return new Date(y,m-1,d);
					} else {
						return new Date();
					}
				}
			});
			this.forms.startTime.datetimebox(this.forms.dateSetting);
			this.forms.startTime.datebox().datebox('calendar').calendar({
				validator: function(date){
					var now = new Date();
					return date<=now;
				}
			});
			this.forms.endTime.datetimebox(this.forms.dateSetting);
			this.forms.endTime.datebox().datebox('calendar').calendar({
				validator: function(date){
					var now = new Date();
					return date<=now;
				}
			});
			this.forms.forIe6();
			this.forms.startTimeUi = this.forms.startTime.next().find('input:eq(0)');
			this.forms.endTimeUi = this.forms.endTime.next().find('input:eq(0)');
				**/
			this.forms.searchBtn.click(function() {
				var monthValue = $("#month").val();
				if (monthValue=='') {
					parent.layer.alert('请选择月份', -1);
					return false;
				}
//				self.tableEl.datagrid({
//					url: $.fn.getRootPath() + '/app/statement/click-amount!list.htm'
//				});
				var url = $.fn.getRootPath() + '/app/statement/click-amount!list.htm';
				self.loadData(url);
			});
		
		}
		
	}
	
	var categoryTreeObject = {
		knowledgeTypeEl : $('#categoryName'),
		ktreeEl : $('div.t-p-d'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountCategoryTree.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					categoryTreeObject.knowledgeTypeEl.val(treeNode.name);
					categoryTreeObject.knowledgeTypeEl.attr('bh', treeNode.bh);
				}
			}
		},
		render : function() {
			var self = this;
			self.knowledgeTypeEl.val('');
			self.knowledgeTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 2) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.knowledgeTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.knowledgeTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_title>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#categoryTree'), this.treeAttr );
		}
	}
	
	var deptTreeObject = {
		deptTypeEl : $('#dept'),
		ktreeEl : $('div.t-p-d1'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					deptTreeObject.deptTypeEl.val(treeNode.name);
					deptTreeObject.deptTypeEl.attr('bh', treeNode.bh);
				}
			}
		},
		render : function() {
			var self = this;
			self.deptTypeEl.val('');
			self.deptTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.deptTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#deptTree'), this.treeAttr );
		}
	}
	var exportExcel = {
		exceportbtn : $('#exportBtn'),
		render : function(){
			this.exceportbtn.click(function(){
				/*新的导出方式（解决数据量过大的问题） Gassol.Bi 2016-07-12 17:28*/
				ExportUtils.toExcel('/app/statement/click-amount!exportData.htm',
					'/app/statement/click-amount!exportResult.htm', tableObject.forms.getValues());
				/*
				var _total = tableObject.tableEl.datagrid('getPager').data("pagination").options.total;
				
				if (tableObject.forms.validation() && _total<5000) {
					$(document).ajaxLoading('处理数据中...');
					//
					$.ajax({
						url : $.fn.getRootPath() + '/app/statement/click-amount!processData.htm',
						type : 'POST',
						timeout : 15000,
						sync : false,
						dataType : 'json',
						data: tableObject.forms.getValues(),
						success : function(data, textStatus, jqXHR) {
							$(document).ajaxLoadEnd(); 
							if(data.success) {
								var iframe = document.createElement("iframe");
					            iframe.src = $.fn.getRootPath()+"/app/statement/click-amount!downLoad.htm?fileName=" + data.message;
					            iframe.style.display = "none";
					            document.body.appendChild(iframe);
							} else {
								parent.layer.alert(data.message, -1);
							}
						},
						error : function(data, textStatus, errorThrown) {
							$(document).ajaxLoadEnd();
							parent.layer.alert('导出失败!', -1);
						}
					});
				} //end validation
				*/
			});
		}
	}
	exportExcel.render();
	tableObject.render();
	categoryTreeObject.render();
	deptTreeObject.render();
});