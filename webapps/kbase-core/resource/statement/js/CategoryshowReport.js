$(function() {
	$("#month").bind('click',function(){
		$("#startTime").val("");
		$("#endTime").val("");
		WdatePicker({ dateFmt: 'yyyy-MM', isShowToday: false, isShowClear: false, onpicked:function(){
			/*设置默认初始日期，防止因为日期过早使开始日期的‘时分秒’为‘23:59:59’，
				@author Gassol.Bi @date 2016-7-4 14:37:26*/
			$('#startTime').val($('#month').val() + '-01 00:00:00');
		}});
	})
	$("#startTime").bind('focus',function(){
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		var mindate=$("#month").val()+"-01";
		WdatePicker({isShowToday: false, minDate:mindate + ' 00:00:00', 
			maxDate:maxdate + ' 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'});
	})
	$("#endTime").bind('focus',function(){
		var mindate=$("#month").val()+"-01";
		if($("#month").val()==''){
			parent.layer.alert('月份不能为空!', -1);
			return false;
		}
		var maxdate=$("#month").val()+"-%ld";
		if($("#startTime").val()!=''){
			mindate=$("#startTime").val();
		}
		WdatePicker({isShowToday: false, minDate:mindate + ' 00:00:00', 
			maxDate:maxdate + ' 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'});
	})
	var tableObject = {
		tableEl : $('table'),
		forms : {
			startTime : $('#startTime'),
			endTime : $('#endTime'),
			month : $('#month'),
			startTimeUi : null,
			endTimeUi : null,
			dept : $('#dept'),
			categoryName : $('#categoryName'),
			dept : $('#dept'),
			person : $('#person'),
			visittype:$('#visittype'),
			searchBtn : $('#searchBtn'),
			validation : function() {
				var startTime = this.startTime.val();
				var endTime = this.endTime.val();
				if(startTime && endTime) {
					var s = new Date(startTime.replace(/-/gi, '/')).getTime();
					var e = new Date(endTime.replace(/-/gi, '/')).getTime();
					if(s > e) {
						parent.layer.alert('开始时间不能晚于结束时间', -1);
						return false;
					}
				}
				return true;
			},
			
			getValues : function() {
				var categorybh = this.categoryName.attr('bh');
				if(categorybh) {
					var reg = new RegExp('^(专业知识点\\\\)', 'g');
					if(!reg.test(categorybh)) 
						categorybh = '专业知识点\\' + categorybh;
				}
				return {
					"report.month" : this.month.val(),
					"report.visittype" : this.visittype.val(),
					"report.starttime" : this.startTime.val(),
					"report.endtime" : this.endTime.val(),
					"report.categorybh" : categorybh,
					"report.deptbh" : this.dept.attr('bh'),
					"report.username" : this.person.val()
				}
			}
		},
		pager : null,
		tableAttributes : {
			title : '知识分类展示查询',
			nowrap : true,
			autoRowHeight : false,
			striped : true,
			collapsible : false,
//			url: $.fn.getRootPath() + '/app/statement/click-amount!list.htm',
			method : 'POST',
			sortOrder : 'desc',
			remoteSort : false,
			loadMsg:'加载中...',
			pagination:true,
			rownumbers:true,
			singleSelect: true,
			toolbar : '#searchCondition',
			fitColumns : true,
			fit: true,
			columns : [
				[
					{field:'categoryname',title:'业务名称',width:320,
						formatter: function(value,row,index){
							 return "<span title="+value+">"+value+"</span>"
						}
					},
					{field:'visittype',title:'访问方式',width:70,
					formatter: function(value,row,index){
						if(value=='1'){
							return "知识目录访问";
						}if(value=='2'){
							return "标准问中访问";
						}if(value=='3'){
							return "指定业务访问";
						}if(value=='4'){
							return "一般搜索列表";
						}
					}},
					{field:'username',title:'操作人员',width:50},
					{field:'deptname',title:'所属部门',width:260,
						formatter: function(value,row,index){
							 return "<span title="+value+">"+value+"</span>"
						}
					},
					{field:'visittime',title:'访问时间',width:100}
				]
			]
		},
		pagerAttributes : {
			pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10, 20, 50],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        onBeforeRefresh:function(){
	        	
	        }
		},
		loadData : function(url, page) {
			if(url){
				var params = {
					url : url,    
					queryParams : this.forms.getValues()
				};
				if(page) params.pageNumber = 1;
				!this.forms.validation() || this.tableEl.datagrid(params);
			}else{
				this.tableEl.datagrid('load', this.forms.getValues());
			}
		},
		/*******渲染表格*******/
		
		render : function() {
		
			var self = this;
			//初始化表格
			this.tableEl.datagrid(this.tableAttributes);
			this.pager = this.tableEl.datagrid('getPager');
			$(this.pager).pagination(this.pagerAttributes);
			this.forms.searchBtn.click(function() {
				var monthValue = $("#month").val();
				if (monthValue=='') {
					parent.layer.alert('请选择月份', -1);
					return false;
				}
//				self.tableEl.datagrid({
//					url: $.fn.getRootPath() + '/app/statement/click-amount!list.htm'
//				});
				var url = $.fn.getRootPath() + '/app/statement/categoryshow-report!findCatecoryshow.htm';
				self.loadData(url, 1);
			});
		
		}
		
	}
	
	var categoryTreeObject = {
		knowledgeTypeEl : $('#categoryName'),
		ktreeEl : $('div.t-p-d'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/main/ontology-category!list.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					categoryTreeObject.knowledgeTypeEl.val(treeNode.name);
					var s=treeNode.name;
		            while(treeNode=treeNode.getParentNode())s=treeNode.name+'\\'+s;
					categoryTreeObject.knowledgeTypeEl.attr('bh', s);
				}
			}
		},
		render : function() {
			var self = this;
			self.knowledgeTypeEl.val('');
			self.knowledgeTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 2) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.knowledgeTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.knowledgeTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_title>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#categoryTree'), this.treeAttr );
		}
	}
	
	var deptTreeObject = {
		deptTypeEl : $('#dept'),
		ktreeEl : $('div.t-p-d1'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/statement/click-amount!clickAmountDeptTree.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					deptTreeObject.deptTypeEl.val(treeNode.name);
					var s=treeNode.name;
		            while(treeNode=treeNode.getParentNode())s=treeNode.name+'\\'+s;
					deptTreeObject.deptTypeEl.attr('bh', s);
				}
			}
		},
		render : function() {
			var self = this;
			self.deptTypeEl.val('');
			self.deptTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.deptTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#deptTree'), this.treeAttr );
		}
	}
	var exportExcel = {
		exceportbtn : $('#exportBtn'),
		render : function(){
			this.exceportbtn.click(function(){
				/*新的导出方式（解决数据量过大的问题） Gassol.Bi 2016-07-12 17:28*/
				ExportUtils.toExcel('/app/statement/categoryshow-report!exportData.htm',
					'/app/statement/categoryshow-report!exportResult.htm', tableObject.forms.getValues());
				/*
				var _total = tableObject.tableEl.datagrid('getPager').data("pagination").options.total;
				
				if (tableObject.forms.validation() && _total<20000) {
					$(document).ajaxLoading('处理数据中...');
					//
					$.ajax({
						url : $.fn.getRootPath() + '/app/statement/categoryshow-report!processData.htm',
						type : 'POST',
						sync : false,
						dataType : 'json',
						data: tableObject.forms.getValues(),
						success : function(data, textStatus, jqXHR) {
							$(document).ajaxLoadEnd(); 
							if(data.success) {
								var iframe = document.createElement("iframe");
					            iframe.src = $.fn.getRootPath()+"/app/statement/categoryshow-report!downLoad.htm?fileName=" + data.message;
					            iframe.style.display = "none";
					            document.body.appendChild(iframe);
							} else {
								parent.layer.alert(data.message, -1);
							}
						},
						error : function(data, textStatus, errorThrown) {
							$(document).ajaxLoadEnd();
							parent.layer.alert('导出失败!', -1);
						}
					});
				} //end validation
				*/
			});
		}
	}
	exportExcel.render();	
	tableObject.render();
	categoryTreeObject.render();
	deptTreeObject.render();
});