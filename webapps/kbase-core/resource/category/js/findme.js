$(function(){
	/*快速搜索查找区域的id(全局)*/
	window.__kbs_global_finder_id = '11';
	$($('div[class="gz-cn2"]')).each(function(){
		if($(this).css('display') != 'none') {
			window.__kbs_global_finder_id = $(this).attr('id');
			return false;
		}
	});
	var that = window.CategoryFinder = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*快速搜索查找区域的id(局部)、检索内容、光标位置、检索数量、TextRange对象*/	
	var finderId, finderKeyword, 
		finderInd = 0, finderInds, rng;
	fn.extend ({
		/*初始化*/
		findInit: function (){
			/*检索内容*/
			var str = $('.kbs-global-finder-keyword').val();
			/*检索初始化*/
			if(finderId != window.__kbs_global_finder_id 
				|| finderKeyword != str) {
				finderId = window.__kbs_global_finder_id;
				finderKeyword = $('.kbs-global-finder-keyword').val();
				
				var obj = document.getElementById(finderId);
				rng = document.body.createTextRange();
				rng.moveToElementText(obj);
				rng.moveStart('character', 0);
				
				/*获取可见范围内检索内容出现的数量*/
				var xContent = obj.innerHTML; 
				var _text = fn.replaceTags(xContent);
				/*
				var p = document.createElement("p");
				p.innerHTML = _text;
				document.body.appendChild(p);
				*/
				finderInds = _text.split(finderKeyword).length-1;
				/*
				$('#btnGoup').after('<textarea rows="20" cols="20">' + finderKeyword + '</textarea>' 
					+ '<textarea rows="20" cols="20">' + _text + '</textarea>');
				*/
				finderInd = 0;
				return true;
			}
			return false;
		},
		replaceTags: function (xStr){ 
			var regExp = /<\/?[^>]+>/gi;
			xStr = xStr.replace(regExp, ''); 
			return xStr; 
		}
	});
	
	/*方向*/
	var toDirect = null;
	that.extend({
		/*上一个*/
		doPrev: function(){
			fn.findInit();
			try{
				/*方向发生改变*/
				if(toDirect !=null && toDirect != 1) {
					rng.move('character', -(finderKeyword.length));
				}
				if(toDirect == null) finderInd = finderInds;
				else finderInd--;
				toDirect = 1;
				
				/*查询*/
				if(finderInd < 1) {
					finderInd = finderInds;
					rng.moveToElementText(document.getElementById(finderId));
					rng.moveStart('character', -1);
				}
				if(rng.findText(finderKeyword, -1)) {
					rng.select();
					rng.collapse(true);
					/*渲染*/
					$('.kbs-global-finder-total').text(finderInd + '/' + finderInds);
				} else {
					rng.moveToElementText(document.getElementById(finderId));
					rng.moveStart('character', -1);
					that.doPrev();
				}
			} catch (e) {
				rng.moveToElementText(document.getElementById(finderId));
				rng.moveStart('character', -1);
				that.doPrev();
			}
		},
		/*下一个*/
		doNext: function(){
			fn.findInit();
			try{
				/*方向发生改变*/
				if(toDirect != null && toDirect != 0)
					rng.move('character', finderKeyword.length);
				toDirect = 0;
				
				/*查询*/
				finderInd++;
				if(finderInd > finderInds) {
					finderInd = 1;
					rng.moveToElementText(document.getElementById(finderId));
					rng.moveStart('character', 0);
				}
				if(rng.findText(finderKeyword)) {
					rng.select();
					rng.collapse(false);
					/*渲染*/
					$('.kbs-global-finder-total').text(finderInd + '/' + finderInds);
				}
			} catch (e){
				rng.moveToElementText(document.getElementById(finderId));
				rng.moveStart('character', 0);
				that.doNext();
			}
		},
		init: function(){
			$('.kbs-global-finder-btn:first').click(function(){
				that.doPrev();
			});
			$('.kbs-global-finder-btn:last').click(function(){
				that.doNext();
			});
		}
	});
	that.init();
});
