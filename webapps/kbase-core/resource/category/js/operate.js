/**
 * @author heart.cao
 * @since 2015-09-23 
 */
$(function(){
	//动态获取高度	
	var _initTop = 0;
	
	try{
		_initTop = $(parent).height() - (170 + 80);
		//modify by eko.zhan at 2015-10-09 Banner栏隐藏时，也需要动态的调整高度
		if ($(parent.window.document).find('.header:hidden').length>0){
			_initTop = _initTop + 73;
		}
		if (!window.__kbs_has_parent){
			_initTop = _initTop + 120;
		}
	}catch(e){
		_initTop = 400;
	}
	
	if ($('#kbs-global-gztable-css').length==0){
	
       //操作横排 长度自动计算
	   $(['<style type="text/css" id="kbs-global-gztable-css">.kbs-global-gztable-css{position:fixed;height:33px;right:10px;top:'+_initTop+'px;width:'
	   +($(".gzcontent").width())+'px;border:1px double #C1CDCD;text-align:center;cursor:pointer;z-index:3333;}</style>'].join('')).appendTo('head');
	   
	   //操作横排 长度固定
	   //$(['<style type="text/css" id="kbs-global-gztable-css">.kbs-global-gztable-css{position:fixed;height:33px;right:10px;top:'+_initTop+'px;width:300px;border:1px double #C1CDCD;text-align:center;cursor:pointer;z-index:3333;}</style>'].join('')).appendTo('head');
	}
	
	//计算操作行 高度
	$(parent).unbind("scroll");
	if (window.__kbs_has_parent){
		$(parent).bind("scroll",function () {
	         var _scrollTop = $(parent).scrollTop();
			_top = _scrollTop + _initTop;
			$('.kbs-global-gztable-css').css('top', _top);	
			
			if($('.kbs-global-finder-logo').length>0){
				$('.kbs-global-finder-logo').css('top', _scrollTop+10);
				$('.kbs-global-finder-input').css('top', _scrollTop+10);			
			}
	    });
	}
	
});