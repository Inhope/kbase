$(function () {
	var that = window.CategoryAttachPreview = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*公共方法*/
	fn.extend({
		
	});
	
	/*功能方法*/
	that.extend({
		set:function(params){
			/*附件id、附件名称、附件类型、分类id*/
			var fileId = params.fileId,
				fileName = params.fileName,
				fileType = params.fileType,
				categoryId = params.categoryId;
			
			/*下载*/
			$('#btn-download').click(function(){
				var elemIF = document.createElement('iframe');
	            elemIF.src = $.fn.getRootPath() + '/app/wukong/search!download.htm?fileId=' 
	            	+ fileId + '&fileName=' + fileName + '.' + fileType;
	            elemIF.style.display = 'none';
	            document.body.appendChild(elemIF);
			});
			
			/*版本*/
			$('#btn-version').click(function(){
				PageUtils.dialog.open('#dd_easyui', {
					params: params, 
					height:400, width:500, title: '文档版本',
			    	url: '/app/category/category-attach!version.htm'
				});
			});
		},
		init: function(){
			
		}
	});
	/*初始化*/
	that.init();
});