$(function(){
	/*列表初始化-公共*/
	PageUtils.init({
		datagrid: $('#now-grid') /*datagrid对象*/
	});
	
	var that = window.CategoryAttach = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*公共方法*/
	fn.extend({
		tree: {
			refresh:function(fileNames){
				/*重新加载树数据*/
				$('#file-tree').tree({
					url: $.fn.getRootPath() + '/app/biztpl/article!attamentLoad.htm?objectId=' + _CateId + '&node=0',
					onBeforeExpand: function(node) {// 获取该节点下其他数据
	                     $('#file-tree').tree('options').url = $.fn.getRootPath()
	                     	+ '/app/biztpl/article!attamentLoad.htm?objectId=' + _CateId + '&node=' + node.id;
	                },
	                onContextMenu: function(e, node){
						e.preventDefault();
						if(!node.leaf){
							// 查找节点
							$(this).tree('select', node.target);
							// 显示快捷菜单
							$('#mm').menu('show', {
								left: e.pageX,
								top: e.pageY
							});
						}
					},
					onLoadSuccess: function(){
						if(fileNames) {
							if(!(fileNames instanceof Array)) {
								fileNames = [ fileNames ];
							}
							$(fileNames).each(function(){
								fn.tree.expand(this);
							});
						}
					}
				});
			},
			/*通过名字获取节点id*/
			getIdByName: function(fileName){
				if(fileName){
					var node, roots = $('#file-tree').tree('getRoots');
					if(roots) $(roots).each(function(){
						if(this.text == fileName){
							node = this;
							return false;
						}
					});
					if(node) return node.id;
				}
				return '';
			},
			/*获取选中节点id*/
			getSelected: function(){
				var node = $('#file-tree').tree('getSelected');
				if(node) return node.id;
				return '';
			},
			/*展开节点*/
			expand: function(fileName){
				var node, roots = $('#file-tree').tree('getRoots');
				if(roots) $(roots).each(function(){
					if(this.text == fileName){
						node = this;
						return false;
					}
				});
				if(node) $('#file-tree').tree('expand', node.target);
			}
		}
	});
	/*功能方法*/
	that.extend({
		/*引用文档*/
		relate:function(fileId, fileName){
			var verId = fn.tree.getIdByName(fileName);
			if(verId) {
				$.messager.alert('确认', '"' + fileName + '"已被引用!');
				fn.tree.expand(fileName);
			} else {
				$.messager.confirm('确认','您确认引用当前文件吗？', function(r){   
				    if (r){    
				       	PageUtils.ajax({
				    		params: {
				    			'fileId': fileId,
				    			'fileName': fileName,
				    			'cateId': _CateId
				    		},
				    		url: '/app/category/category-attach!relate.htm',
							after: function(jsonResult) {
								$.messager.alert('确认', jsonResult.msg, 'info',
									function(){
										if(jsonResult.rst) {
											fn.tree.refresh(fileName);
										}
									});
							}
				    	});
				    }
				});
			}
		},
		/*解除文档*/
		remove: function(){
			$.messager.confirm('消息', '确认需要解除分类与当前文档的关系吗？', function(r){
				if (r){
					var fileId = fn.tree.getSelected();
					if(fileId){
						PageUtils.ajax({
				    		params: {
				    			'attachmentId': fileId,
				    			'topNodeId': _AttType,
				    			'objectIds': _CateId
				    		},
				    		url: '/app/biztpl/article!relieveRelation.htm',
							after: function(jsonResult) {
								var msg = '操作失败';
								if(jsonResult.result) msg = '操作成功';
								$.messager.alert('确认', msg, 'info',
									function(){
										if(jsonResult.result) {
											fn.tree.refresh();
										}
									});
							}
				    	});
					}
				}
			});
		},
		/*手动通知robot-search索引*/
		cateAttSync: function(){
			$.messager.confirm('确认','确认手动通知同步索引?', function(r){   
			    if (r){    
			       	PageUtils.ajax({
			    		params: {
			    			'cateId': _CateId
			    		},
			    		url: '/app/category/category-attach!cateAttSync.htm',
						after: function(jsonResult) {
							$.messager.alert('确认', jsonResult.msg, 'info',
								function(){
								});
						}
			    	});
			    }
			});
		},
		/*初始化*/
		init: function(){
			/*已关联附件*/
			fn.tree.refresh();
			
			/*查询*/
			$('#now-search').linkbutton({
			    onClick: function(){
			    	/*查询跳转到第一页*/
				    PageUtils.datagrid.reload(1);
			    }
			});
			
		}
	});
	that.init();
});