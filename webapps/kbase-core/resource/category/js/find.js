/**
 * @author eko.zhan
 * @since 2015-08-05 13:40 
 */
$(function(){
	//modify by eko.zhan at 2015-08-12 $.broswer.msie 在IE11上无法识别
	
	if (!!window.ActiveXObject || "ActiveXObject" in window){
		$.kbase.find('all');
		/**
		 * 分类展示页面调用快捷搜索插件，只查询div中的页
		 */
		$('.kbs-global-finder-logo').hide();
		if ($('ul.gzul li').length>1){
			$('ul.gzul li').click(function(){
				$('.kbs-global-finder-input').hide();
				$('.kbs-global-finder-logo').hide();
			});
			$('ul.gzul li:last').click(function(){
				$('.kbs-global-finder-logo').show();
				//广东移动提出 上一条下一条默认显示
				$('.kbs-global-finder-input').show();
			});
			
			//增加滚动条滚动后查询框依然显示屏幕中央的效果 add by eko.zhan at 2015-08-14
			/*
			window.setInterval(function(){
				var _scrollTop = $(parent).scrollTop();
				
				var _initTop = 10;
				
				//var _top = $('.kbs-global-finder-input').css('top');
				//if (_top.lastIndexOf('px')!=-1){
				//	_top = _top.substring(0, _top.length-2);
				//}
				
				
				_top = _scrollTop + _initTop;
				
				$('.kbs-global-finder-logo').css('top', _top);
				$('.kbs-global-finder-input').css('top', _top);
				
			}, 500);*/
		}
	}
});