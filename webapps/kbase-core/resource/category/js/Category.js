$(function(){
	/********************页面初始化*******************************/
	init();
	/********************点击事件*******************************/
	tagClick();
	/********************附件预览*******************************/
	openAttachment();
	openP4();
	/********************其他平台*******************************/
	otherPlatform();
	/*********************短信**********************************/
	message();
	/*********************右键**********************************/
	rightClick();
	//jeasyMenu();	//用jeasyui的菜单替换之前的右键菜单
	/*********************答案反馈*********************************/
	//unsolved();
	/***********************指定业务*******************************/
	zdyw();
	/****************************全屏显示*********************************/
	doFullScreen();
	showAllP4();
	qaposition();
	
});
var tag1=0;

/***
 * 外部点击q进入分类展示页面，定位到该q在哪一个tab也中
 * */
function qaposition(){
	if(questionId){
		var question = $('span#'+questionId);//根据qid获取其所在的位置
		if(question.length > 0){
			/*跳转到分类展示页面标准问定位 Gassol.Bi 2016-9-30 11:25:33*/
			try {
				var tab1 = question.parents('div.gz-cn2');
				var tab1_id = tab1.attr('id');//获取tab1的id
				var tag1_index = tab1_id.substring(0,1);//获取该q对应的tab1所在ul.gzul的位置
				var tag2_index = tab1_id.substring(1,2);//获取该q对应的tab2所在ul.gzul02的位置
				
				/*控制显示区域显示*/
				tab1.siblings('ul.gzul').children('li').eq(Number(tag1_index)-1).click();
				question.parents('div.gzform01').prev('ul.gzul02').children('li').eq(Number(tag2_index)-1).click();
				
				/*滚动条移动*/
				$(question).before('<input type="text" style="width:1px;" id="spotAAA">');
				var spot = $('#spotAAA');
				$(spot).focus();
				/*定位修正*/
				var t = $(tab1).scrollTop();
				var j = $(spot).offset().top - $(tab1).offset().top;
				$(tab1).animate({'scrollTop': t + j}, 100);
				/*移除定位标签*/
				$(spot).remove();
				/*文本选中*/
				var obj = question[0];
				if(document.selection){/*IE*/
					var rng = document.body.createTextRange();
					rng.moveToElementText(obj);
					rng.moveStart('character', 0);
					rng.select();
				} else {/*非IE*/
					//var rng = document.createRange();
			        //rng.selectNodeContents(obj);
				}
			} catch(e){
				alert(e);
			}
		}
	}
}

function init(){
	//$('ul.gzul li:eq(0)').attr('class','active');	hidden by eko.zhan at 2016-04-22 13:36 模块隐藏在页面渲染
	/*$('div.gz-cn2').each(function(index){
		if(index!=0){
			$(this).css('display','none');
		}
	});
	*/
	//modify by eko.zhan at 2015-12-30 11:35 隐藏除了第一个元素分类展示面板以外的所有面板，作用与上面注释掉的代码一样
	//hidden by eko.zhan at 2016-04-22 13:36 模块隐藏在页面渲染
	//$('div.gz-cn2').not($('div.gz-cn2:eq(0)')).hide();
	
	
	//modify by eko.zhan at 2015-12-30 11:40 
	//修改分类展示面板高度，适应屏幕，在IE7上获取parent耗时导致页面加载卡顿，这是一个很蛋疼的问题，所以获取screen高度
	//modify by eko.zhan at 2016-06-20 15:40 优化窗口高度，适应不同屏幕
	var height = screen.height * 0.53;
	if (!window.__kbs_has_parent){
		//全屏显示
		height = screen.height * 0.70;
	}
	$('.gzcontent .gz-cn2').height(height);
	
	/*
	//Gassol.Bi 2016-9-30 10:37 造成跳转定位问题，详情见：方法 qaposition() 
	//add by eko.zhan at 2016-07-01 13:43 分类展示页面样式有误
	window.setTimeout(function(){
		$('ul.gzul02 li:first').click();
		
		$('div.gz-cn2:first').scrollTop(0);
	}, 500);
	*/
}

function tagClick(){
	$('ul.gzul li').each(function(index){
		$(this).click(function(){
			$('ul.gzul li').removeAttr('class','active');//去除所有tag1高亮
			$(this).attr('class','active');//点击的tag1高亮
			$('div.gz-cn2').css('display','none');//隐藏所有主div
			if(index!=$('ul.gzul li').length-1 && index!=$('ul.gzul li').length-2){
				$('div.gz-cn2').each(function(i){
					if($(this).attr('tag1') == index && "0" == $(this).attr('tag2')){//点击对应tag1的全文显示
						tag1 = $(this).attr('tag1');
						
						//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
						window.__kbs_global_finder_id = $(this).attr('id');
						
						$(this).css('display','block');
					}
				});
				$('ul.gzul02 li').removeAttr('class','active');//去除所有tag2高亮
				$('ul.gzul02 li').each(function(index){//点击的tag2高亮
					if($(this).attr('tag2') == "0"){
						$(this).attr('class','active');
					}
				});
			}else if(index == $('ul.gzul li').length-2){
				$('#sms').show();
				window.__kbs_global_finder_id = $('#sms').attr('id');
			}else{
				$('div.gz-cn2').last().show();
				
				//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
				window.__kbs_global_finder_id = $('div.gz-cn2').last().attr('id');
			}
			
			_flushBodyHeight();
		});
	});
	
	
	$('ul.gzul02 li').each(function(index){
		$(this).click(function(){
			var tag2 = $(this).attr('tag2');
			$('ul.gzul02 li').removeAttr('class','active');//去除所有tag2高亮
			$('ul.gzul02 li').each(function(index){//点击的tag2高亮
				if($(this).attr('tag2') == tag2){
					$(this).attr('class','active');
				}
			});
			$('div.gz-cn2').css('display','none');//隐藏所有主div
			$('div.gz-cn2').each(function(i){
				if(tag1 == $(this).attr('tag1') && tag2 == $(this).attr('tag2')){
					
					//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
					window.__kbs_global_finder_id = $(this).attr('id');

					$(this).css('display','block');
				}
			});
			
			_flushBodyHeight();
		});
	});
	
}

/**
 * @author eko.zhan at 2015-10-12 14:30
 * 重新计算页面高度，修正滚动条高度
 */
function _flushBodyHeight(){
	//modify by eko.zhan at 2015-10-28 15:15 高度由内部页签决定不刷新父层的滚动条高度
	return false;
	//add by eko.zhan at 2015-10-09 20:05 
	$(parent.document).find('.content_content').find('iframe:visible').height($('body').get(0).scrollHeight);
	
	//
	//没有滚动条的页签切换时导致工具条不显示 modify by eko.zhan at 2015-10-10 13:27
	if (document.body.scrollHeight<$(parent).height()){
		if (window.__kbs_toolbar_top_height==undefined){
			window.__kbs_toolbar_top_height = $('.kbs-global-gztable-css').css('top');
		}
		$('.kbs-global-gztable-css').css('top', document.body.scrollHeight-50);
	}else if (window.__kbs_toolbar_top_height!=undefined){
		$('.kbs-global-gztable-css').css('top', window.__kbs_toolbar_top_height);
	}
}

/************请求action获取category路径***************/
function navigate_init(robot_context_path,categoryId){
/*
	var data = {
		'format':'json','async':true,'jsoncallback':'navigateCb','nodeid':categoryId,'nodetype':3
	};
	var url = robot_context_path+'p4pages/related-category.action'+'?format=json&async=true&nodeid='+categoryId+'&ts='
			+ new Date().getTime()+'&jsoncallback=navigateCb&nodetype=1';
	var script = document.createElement('script');  
	script.setAttribute('src', url);  
	document.getElementsByTagName('head')[0].appendChild(script);*/
	//modify by eko.zhan at 2015-09-22 16:15
	var data = {'format':'json', 'async':true, 'nodeid':categoryId, 'nodetype':1};
	$.getJSON(robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
		if (arr!=null){
			$(arr).each(function(i, item){
				//name id type bh
				$('#categoryNv').append('<a href="javascript:void(0)" categoryId="' + item.id + '"> > ' + item.name + '</a>');
			});
		}
	});
}
window.navigateCb = function(data) {
	if(data){
		var navigateJObj = $('#categoryNv');
		for(var i=0;i<data.length;i++){
			var aJObj;
			aJObj = $('<a/>').attr('href','javascript:void(0);').attr('categoryId',data[i]['id']).html(">"+data[i]['name']);
			navigateJObj.append(aJObj);
		}
//		navigateJObj.children('a').click(function(){
//			openOv('',$(this).attr('categoryId'));
//		});
	}
}

function openAttachment(){
	$('a.openAttachment').each(function(){
		$(this).click(function(){
			var _this = this;
			//var id = $(this).attr('filename');
			var _filename = $(_this).attr('_name');
           	var _fileid = $(_this).attr('filename');
           	var _filetype = '';
           	if (_filename.lastIndexOf('.')!=-1){
           		_filetype = _filename.substring(_filename.lastIndexOf('.'));
           	}
           	var fileName = _fileid + _filetype;
           	/*
			$.getJSON(swfAddress+'?format=json&async=true&fileName='+fileName+'&callbackparam=?', function(data){
				if (data==null || data=="null") return false;
				var s = data;
				s = s.substring(s.indexOf('/')+1,s.length);
				s = swfAddress.substring(0,swfAddress.indexOf('attachment')) + s;
				window.parent.open(s);
			});*/
			window.open(swfAddress + '?fileName='+fileName);
			
		});
		
	});
	$('a.showAttachment').each(function(){
		var self = $(this);
		$(this).click(function(){
			var _this = this;
			var _filename = $(_this).attr('_name');
           	var _fileid = $(_this).attr('filename');
           	var _filetype = '';
           	if (_filename.lastIndexOf('.')!=-1){
           		_filetype = _filename.substring(_filename.lastIndexOf('.'));
           	}
           	var fileName = _fileid + _filetype;
			if(self.attr('open') != 'open'){
				self.parent().find('a').removeAttr('open');
				self.parent().prev().find('a').removeAttr('open');
				self.attr('open','open');
				var id = self.attr('filename');
				self.parent().next().html('<iframe src="'+swfAddress   + '?fileName='+fileName+'"></iframe>');
			}else{
				self.removeAttr('open');
				self.parent().next().html('');
			}
		});
	});
}

/**@lvan.li 20160509 针对广州移动分类展示p4一键展开**/
function showAllP4(){
	$('#showAllP4').click(function(){
		// var category_a = $('div.gz-cn2:visible').find('a#categoryP4');
		// $(category_a).each(function(){
		// 	$(this).click();
		// });

		// modified by barry.li at 2016-11-1  一键展开 P4， 在全屏显示的情况下，同时展开 P4 和 附件
		var category_a = $('div.gz-cn2:visible').find('a#categoryP4');
		if($('div[name="p4div"]').length == 0) {
			$(category_a).each(function () {
				$(this).after('<div name="p4div"><iframe src="' + $(this).attr('url') + '?t=' + Math.random() + '"></iframe></div>')
			});
		}else{
			$('div[name="p4div"]').remove();
		}
		if(!window.__kbs_has_parent){
			$('a.showAttachment').each(function(){
				$(this).click();
			});
		}

		if($('div[name="p4div"]').length == 0 && $('a.showAttachment').attr('open')!='open'){
			$(this).text('一键展开');
		}else if($('div[name="p4div"]').length > 0 && $('a.showAttachment').attr('open')=='open'){
			$(this).text('全部关闭');
		}
	});
}
function openP4(){
	$('a.showP4').each(function(){
		var self = $(this);
		self.unbind("click");//@lvan.li 20160509 否则会有多个click事件
		$(this).click(function(){
			var _this = this;
			// if ($(_this).attr('open')!='open'){
			if ($('div[name="p4div"]').length == 0){
				$(_this).attr('open', 'open');
				//moidfy by eko.zhan at 2016-09-22 16:30  增加随机数，强制刷新缓存
				$(_this).after('<div name="p4div"><iframe src="' + $(_this).attr('url') + '?t=' + Math.random() + '"></iframe></div>')
			}else{
				$(_this).removeAttr('open');
				// $(_this).next('div[name="p4div"]').remove();
				$('div[name="p4div"]').remove();
			}
		});
		
	});
}

function otherPlatform(){
	$('div.increase_dan_title').click(function(){
		$('div.increase_d').hide();
	});
	$('a.other_platform').click(function(){
		/*
		$('div.increase_d').hide();
		var valueId = $(this).attr('valueId');
		$.ajax({
	   		type: "POST",
	   		url: $.fn.getRootPath()+"/app/category/all-ans.htm",
	   		data: {valueId:valueId},
	   		dataType:'json',
	   		async: true,
	   		success: function(msg){
	   			if(msg && msg.length>0){
	   				var s = '';
	   				for(var i=0;i<msg.length;i++){
	   					s += '<tr>';
	   					s += '<td width="22%" height="0" align="left" valign="middle">';
	   					s += msg[i].platform;
	   					s += '</td>';
	   					s += '<td width="78%" height="0" align="left" valign="middle">';
	   					s += msg[i].ans;
	   					s +='</td></tr>';
	   				}
	   				$('div.increase_d tbody').html(s);
	   				var scrollTop = parseInt(window.parent.document.documentElement.scrollTop);
		   			$('div.increase_d').css('top',50+scrollTop+'px');
		   			$('div.increase_d').show();
	   			}else{
	   				parent.$(document).hint("没有其他平台答案");
	   			}
	   		},
	   		error :function(){
	   			parent.layer.alert('获取失败');
	   		}
		});
		*/
		var valueid = $(this).attr('valueId');
		_showModalDialog({url: $.fn.getRootPath() + '/app/category/all-ans!view.htm?valueId=' + valueid});
					
	});
}


function message(){
	$('input.check').click(function(){
		var valueId = $(this).attr('valueId');
		if($(this).attr('checked')){
			$('input.check').each(function(){
				if($(this).attr('valueId')==valueId){
					$(this).attr('checked',true);
				}
			});
		}else{
			$('input.check').each(function(){
				if($(this).attr('valueId')==valueId){
					$(this).removeAttr('checked');
				}
			});
		}
	});
	
}

/**
 * 右键菜单
 * @auther eko.zhan at 2016-07-29 21:00
 */
function jeasyMenu(){
	
	$(document).on('contextmenu', function(e){
		e.preventDefault();
	});
	
	var _objid = '';			//实例id
	var _valueid = '';			//标准问id
	var _ques = '';				//标准问
	var _answer = '';			//标准答案
	var _answerHtml = '';
	var _enableSms = false;		//是否可发送短信
	
	$('div.gzform01 table tr td[class="answer"]').on('mouseup', function(e){
		if (e.button==2){
			$('#mm').menu('show', {
			    left: e.pageX,
			    top: e.pageY
			});
			
			var item = this;
			_objid = $(item).attr('_oid');
			_ques = $.trim($(item).prev().find('span.kbs-question').text());
			_answer = $.trim($(item).find('div:first').text());
			_answerHtml = $.trim($(item).find('div:first').html());
			_valueid = $(item).prev().prev().text()	//标准问id
			
			//设置答案反馈的值
			$('#corrform input[name="question"]').val($(this).prev().html());
			$('#corrform input[name="value_id"]').val($(this).prev().prev().html());
			askContent=$(this).prev().html();
			faqId=$(this).prev().prev().html();
			
			
			//判定能否发送短信和收藏短信
			gzyd_sms_checkbox_obj = $(this).parent().children('td:last').find('input[type="checkbox"][name^="gzyd_sms_checkbox"]');
			if (gzyd_sms_checkbox_obj){
				$('#menu ul').find("li[id='gzyd_sms_checkbox_now']").remove();
				var _enableSms = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isableSMS");
				if(!_enableSms){
					/*$('#menu ul').append('<li id="gzyd_sms_checkbox_now" onclick="javascript:GzydMappings.sms.send(true);">'
						+'<a href="###">发送短信</a></li>');*/
					$('#mm').menu('disableItem', $('#jNavFavSms').get(0));
					$('#jNavFavSms').attr('_state', '0');
					$('#mm').menu('disableItem', $('#jNavSendSms').get(0));
				}else{
					$('#jNavFavSms').attr('_state', '1');
					$('#mm').menu('enableItem', $('#jNavFavSms').get(0));
					$('#mm').menu('enableItem', $('#jNavSendSms').get(0));
				}
			}
			
			//针对IE浏览器增加 复制 功能
			if (!!window.ActiveXObject || "ActiveXObject" in window){
				//IE浏览器允许复制
				$('#mm').menu('enableItem', $('#jNavCopy').get(0));
			}else{
				$('#mm').menu('disableItem', $('#jNavCopy').get(0));
			}
			
		}
	});
	
	//纠错/答案反馈
	$('#jNavErrCorrect').click(function(){
		$.kbase.errcorrect({
			'objId': _objid,
			'faqId': _valueid,
			'cateId': categoryId
		});
	});
	//收藏
	$('#jNavFavSms').click(function(){
		if ($(this).attr('_state')==null || $(this).attr('_state')==0) return false;
		var param = [];
		window.__kbs_fav_params = param;
		param.push({
			id: _valueid,
			cateid: categoryId,
			question: _ques,
			answer: _answer
		});
		
		window.__kbs_fav_params = param;
	
		var url = $.fn.getRootPath() + '/app/fav/fav-detail.htm?key=SMS';
		window._open(url, '请选择收藏夹', 800, 400);
		return false;
	});
	$('#jNavFavCatetag').click(function(){
		alert('该功能尚在开发中，敬请期待');
		return false;
	});
	$('#jNavFavKbval').click(function(){
		alert('该功能尚在开发中，敬请期待');
		return false;
	});
	//收藏至文件夹，兼容老版本收藏
	$('#jNavFavDefault').click(function(){
		FavBallExt.f_openPanel(null, _valueid, categoryId);
	});
	//推荐
	$('#jNavRecommend').click(function(){
		parent.__kbs_layer_index = parent.$.layer({
			type : 2,
			border : [10, 0.3, '#000'],
			title : false,
			closeBtn : [0, true],
			iframe : {
				src : $.fn.getRootPath() + '/app/recommend/recommend!open.htm?cateid=' + categoryId + '&valueid=' + _valueid
			},
			area : ['540', '480']
		});
	});
	//复制
	$('#jNavCopy').click(function(){
		window.clipboardData.setData("Text", _ques + '\r\n' + _answer);
		//window.clipboardData.setData("Text", _ques + '\r\n' + _answerHtml);
		alert('复制成功');
	});
	//发送短信
	$('#jNavSendSms').click(function(){
		GzydMappings.sms.send(true);
	});
	//上一版本
	$('#jNavVersion').click(function(){
		_showModalDialog({url: $.fn.getRootPath() + '/app/category/category!versionAns.htm?valueId=' + _valueid+'&objId='+_objid});
	});
}

/**
 * 广东移动要求收藏菜单分级，之前的菜单已经无法满足，故用jeasyui的菜单，function jeasyMenu
 * @deprecated by eko.zhan at 2016-07-29 21:00
 */
function rightClick(){
	var _objid = '';			//实例id
	var _valueid = '';			//标准问id
	var _ques = '';				//标准问
	var _answer = '';			//标准答案
	var _answerHtml = '';
	
	var x;var y;
	/*********************右键菜单事件**********************************/
	menuClick();
	//阻止页面默认右键事件
	document.oncontextmenu=function(e){
		if(e && e.preventDefault){
			//阻止默认浏览器动作(W3C) 
			e.preventDefault(); 
		}else{
			//IE中阻止函数器默认动作的方式 
			window.event.returnValue = false; 
		}
	};
	
	//表格第二列（答案列）绑定右键事件
	$('div.gzform01 table tr td[class="answer"]').each(function(i, item){
		//alert($(item).text());
		
		$(this).mousedown(function(e){
			if(e.button == 2){	//右键
				_objid = $(item).attr('_oid');
				_ques = $.trim($(item).prev().find('span.kbs-question').text());
				_answer = $.trim($(item).find('div:first').text());
				_answerHtml = $.trim($(item).find('div:first').html());
				_valueid = $(item).prev().prev().text()	//标准问id
				
				showMenu(e);
				//设置答案反馈的值
				$('#corrform input[name="question"]').val($(this).prev().html());
				$('#corrform input[name="value_id"]').val($(this).prev().prev().html());
				askContent=$(this).prev().html();
				faqId=$(this).prev().prev().html();
				
				//针对IE浏览器增加 复制 功能
				if (!!window.ActiveXObject || "ActiveXObject" in window){
					if ($('#navCopy').length==0){
						$('#menu ul').append('<li><a href="javascript:void(0);" id="navCopy">复制</a></li>');
					}
				}
				
				//点击右键时当前标准问对象(右键发送短信用),Category.jsp右键事件赋值
				gzyd_sms_checkbox_obj = $(this).parent().children('td:last')
					.find('input[type="checkbox"][name^="gzyd_sms_checkbox"]');
				if(gzyd_sms_checkbox_obj){
					$('#menu ul').find("li[id='gzyd_sms_checkbox_now']").remove();
					var isableSMS = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isableSMS");
					if(isableSMS){
						$('#menu ul').append('<li id="gzyd_sms_checkbox_now" onclick="javascript:GzydMappings.sms.send(true);">'
							+'<a href="###">发送短信</a></li>');
						$('#navFavSms').attr('_state', 1);
					}else{
						$('#navFavSms').attr('_state', 0);
					}
				}
			}
		});
	});
	
	/* hidden by eko.zhan at 2015-08-17 绑定事件查找td元素循环次数太多影响js性能
	$('tr').each(function(){
		$(this).find('td').each(function(index){
			if(index==2){
				$(this).mousedown(function(e){
					if(e.button == 2){
						showMenu(e);
						//设置答案反馈的值
						$('#corrform input[name="question"]').val($(this).prev().html());
						$('#corrform input[name="value_id"]').val($(this).prev().prev().html());
						askContent=$(this).prev().html();
						faqId=$(this).prev().prev().html();
						
						//点击右键时当前标准问对象(右键发送短信用),Category.jsp右键事件赋值
						gzyd_sms_checkbox_obj = $(this).next().next().children("input[type='checkbox']");
						if(gzyd_sms_checkbox_obj){
							$('#menu ul').find("li[id='gzyd_sms_checkbox_now']").remove();
							var isableSMS = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isableSMS");
							if(isableSMS){
								$('#menu ul').append('<li id="gzyd_sms_checkbox_now" onclick="javascript:GzydMappings.rightClick_duanxin();"><a href="###">发送短信</a></li>');
							}
						}
					}
				});
			}
		});
	});
		*/
	function showMenu(e){
		
		if(e.pageX || e.pageY){ 
			x=e.pageX;
			y=e.pageY;
		} else{
			x=e.clientX + document.body.scrollLeft - document.body.clientLeft;
			y=e.clientY + document.body.scrollTop - document.body.clientTop;
		}
		$('#menu').css({"top":y+"px", "left":x+"px", "visibility":"visible"});
		e.stopPropagation();
//		$("body").mousedown(onBodyMouseDown);
		$("body").bind("mousedown", onBodyMouseDown);
	}
	function hideMenu(){
		$('#menu').css({"visibility":"hidden"});
		$("body").unbind("mousedown", onBodyMouseDown);
	}
	/**************点击其他位置隐藏右键菜单************************/
	function onBodyMouseDown(event){
		if (!(event.target.id == "menu" || $(event.target).parents("#menu").length>0)) {
			$("#menu").css({"visibility" : "hidden"});
		}
	}
	
	function menuClick(){
		//评论反馈
		$('#menu li:eq(0)').click(function(){
			hideMenu();
			/*
			var w = ($('body').width() - $('#satisfaction').width())/2;
			$('#satisfaction').css({"top":y+"px", "left":w+"px"});
			$('body').showShade();
			$("#typelist").hide();
			$("#form1")[0].reset();
			$("#form2")[0].reset();
			$("#score").html($("input[name='satisfaction']:checked").val());
			$.ajax({
			   		type: "POST",
			   		url:  $.fn.getRootPath() + '/app/corrections/corrections!getsatisfaction.htm',
			   		data:{'value_id':faqId},
			   		async: true,
			   		dataType : 'html',
			   		success: function(data){
			   		    $("#fenye").remove();
			   		    $("#satisfact_content").remove();
			   			$("#satisfaction").append(data);
			   			$('#commcount').html($('#satisfactioncount').val());
			   		},
			   		error :function(){
			   			parent.layer.alert('获取失败');
			   		}
			});
			$('#satisfaction').show();
			*/
			/*
			var _data = {
				'objId': _objid,
				'faqId': _valueid,
				'cateId': categoryId,
				'question': _ques,
				'answer': _answer
			}
			var _params = $.param(_data);
			var _url = $.fn.getRootPath() + "/app/corrections/corrections!send.htm?" + _params;
			parent.__kbs_layer_index = parent.$.layer({
				type: 2,
				border: [10, 0.3, '#000'],
				title: false,
				closeBtn : [0, true],
				iframe: {src : _url, scrolling: 'no'},
				area: ['832px', '600px']
			});*/
			
			$.kbase.errcorrect({
				'objId': _objid,
				'faqId': _valueid,
				'cateId': categoryId/*,
				'question': _ques,
				'answer': _answer*/
			});
		});
		
		//收藏
		$('#navFav').click(function(e){
			//收藏标准问
			var faqId = _valueid;
			FavBallExt.f_openPanel(null, faqId, categoryId);
			/*
			parent.__kbs_layer_index = parent.$.layer({
				type : 2,
				border : [10, 0.3, '#000'],
				title : false,
				closeBtn : [0, true],
				iframe : {
					src : $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?objid=' + _objid + '&cataid=' + categoryId
				},
				area : ['500', '300']
			});
			*/
			
			hideMenu();
		});
		
		//收藏
		$('#navFavSms').click(function(){
			if ($(this).attr('_state')==null || $(this).attr('_state')==0) return false;
			var param = [];
			window.__kbs_fav_params = param;
			//这里传入很多为空的参数是防止在IE7下解析数组出错，共用一个方法
			param.push({
				id: _valueid,
				cateid: categoryId,
				question: _ques,
				answer: _answer,
				title: '',
				name: '',
				content: '',
				locations: ''
			});
			var url = $.fn.getRootPath() + '/app/fav/fav-detail.htm?key=SMS';
			window._open(url, '请选择收藏夹', 800, 400);
			return false;
		});
		
		//业务收藏
		$('#navFavCatetag').click(function(){
		    var name = "";
			$('#categoryNv a').each(function(){
				name += $(this).text();
			})
			var title = $('#categoryNv [categoryid = '+categoryId+']').text();
			var param = [];
			window.__kbs_fav_params = param;
			//这里传入很多为空的参数是防止在IE7下解析数组出错，共用一个方法
			param.push({
				id: _valueid,
				cateid: categoryId,
				question: _ques,
				answer: _answer,
				name: name,
				title: title,
				content: '',
				locations: ''
			});
			var url = $.fn.getRootPath() + '/app/fav/fav-detail.htm?key=CATETAG';
			window._open(url, '请选择收藏夹', 800, 400);
			return false;
		});
		
		//知识点收藏
		$('#navFavKbval').click(function(){
			var content = "";
			$('#categoryNv a').each(function(){
				content += $(this).text();
			})
			var title = $('#categoryNv [categoryid = '+categoryId+']').text();
			var obj = $(this);
			var param = [];
			window.__kbs_fav_params = param;
			//这里传入很多为空的参数是防止在IE7下解析数组出错，共用一个方法
			param.push({
				id: _valueid,
				cateid: categoryId,
				question: _ques,
				answer: _answer,
				content: content,
				title: title,
				name: '',
				locations: ''
			});
			var url = $.fn.getRootPath() + '/app/fav/fav-detail.htm?key=KBVALUE';
			window._open(url, '请选择收藏夹', 800, 400);
			return false;
		});
		
		//复制
		$('#navCopy').live('click', function(){
			window.clipboardData.setData("Text", _ques + '\r\n' + _answer);
			//window.clipboardData.setData("Text", _ques + '\r\n' + _answerHtml);
			$(document).hint('复制成功');
			hideMenu();
		});
		
		//推荐
		$('#navRecommend').live('click', function(){
			parent.__kbs_layer_index = parent.$.layer({
				type : 2,
				border : [10, 0.3, '#000'],
				title : false,
				closeBtn : [0, true],
				iframe : {
					src : $.fn.getRootPath() + '/app/recommend/recommend!open.htm?cateid=' + categoryId + '&valueid=' + _valueid
				},
				area : ['540', '480']
			});
			hideMenu();
		});
		//上一版本
		$('#version').live('click',function(){
			_showModalDialog({url: $.fn.getRootPath() + '/app/category/category!versionAns.htm?valueId=' + _valueid+'&objId='+_objid});
			hideMenu();
		});
		
		//生成微信码 该功能只是调试作用暂不开放 add by eko.zhan at 2016-02-19
		$('#navWeixinCode').click(function(){
			var param = {
				valueid: _valueid,
				content: _answer
			}
			$.post($.fn.getRootPath() + '/app/category/weixin!random.htm', param, function(data){
				alert('请使用微信向公众号发送微信码 ' + data.message);
			}, 'json')
			
			hideMenu();
		});
		
		/**
		 * 实例版本对比 @author Gassol.Bi
		 */
		$('#objVersion').click(function(){
			window._open($.fn.getRootPath() + '/app/search/object-search!toVersionObject.htm?objId=' + _objid, 
				'实例对比', 470, 300);
		});
	}
} //end function rightClick


function zdyw(){
	$('ul.zdyw li a').each(function(index){
		var question = $.trim($(this).parent().attr('q'));
		var self = $(this);
		$(this).click(function(){
			if(self.next().next().attr('index')){
				self.css('color','#666666');
				if(self.next().next().attr('index')==index){
					self.next().next().remove();
					return false;
				}else{
					self.next().next().remove();
				}
			}
			$('body').showShade();
			//@ lvan.li 20150308 指定业务的相关答案不能准确命中修复
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/category/related-ans!zdyw.htm",
		   		data: {question:question,type:index%2+1},
		   		dataType:'json',
		   		async: true,
		   		success: function(msg){
		   			if(msg == null){
		   				alert('当前地市未关联相应的知识');
		   			}else if(index%2 == 0){
		   				self.css('color','#0000FF');
		   				self.parent().append('<div index="'+index+'" style="border:1px solid;margin:1px;">'+msg+'</div>');
		   				openP4();
		   				
		   				_flushBodyHeight();
		   			}else if(index%2 == 1){
		   				var url = $.fn.getRootPath()+"/app/category/category.htm?type=3&categoryId="+msg.id+"&categoryName="+msg.name;
		   				parent.TABOBJECT.open({
		   					id : msg.id,
		   					name : msg.name,
		   					hasClose : true,
		   					url : url,
		   					isRefresh : true
		   				}, this);
		   			}
		   			$('body').hideShade();
		   		},
		   		error :function(){
		   			alert('当前地市未关联相应的知识');
		   			$('body').hideShade();
		   		}
			});
		});
	});
}

/**
 * @author eko.zhan 
 * @since 2015-10-10 12:05
 * @author eko.zhan at 2015-10-27 11:10 增加回到顶部功能
 * 增加全屏显示功能
 */
function doFullScreen(){
	$('#btnFullScreen').click(function(){
		//判断当前页面是否全屏，已全屏就不再全屏显示
		if (window.__kbs_has_parent){
			var _url = location.href;
			//var _params = 'fullscreen=yes, toolbar=yes, scrollbars=yes, location=no, status=no, menubar=no, resizable=no';
			var _params = 'height=' + screen.availHeight + ', width=' + screen.availWidth + ', top=0, left=0, toolbar=no, scrollbars=yes, location=no, status=no, menubar=no, resizable=yes';
			window.open(_url, '_blank', _params);
		}
	});
	
	//置顶
	$('#btnGoup').click(function(){
		//var tabBody = $('.gzcontent .gz-cn2:visible:last');
		var tabBody = $('#' + window.__kbs_global_finder_id);
		tabBody.scrollTop(0);
	});
}

/**
 * @author eko.zhan
 * url, width, height, args
 */
function _showModalDialog(opts){
	opts = $.extend(true, {
		width: 500,
		height: 400,
		args: window
	}, opts);
	var _diaWidth = opts.width;
	var _diaHeight = opts.height;
	var _scrWidth = screen.width;
	var _scrHegiht = screen.height;
	var _diaLeft = (_scrWidth-_diaWidth)/2;
	var _diaTop = (_scrHegiht-_diaHeight)/2;
	var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
	window.showModalDialog(opts.url, opts.args, params);
}
