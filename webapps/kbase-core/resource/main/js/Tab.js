TAB = function() {
	TABOBJECT = {
		'tabTitle' : $('div.titile'),
		'tabContent' : $('div.content_content'),
		'titleHideClass' : 'xinjian_wc',
		'titleActivityClass' : 'xinjian_wz',
		'contentHideClass' : 'content_content_hide',
		'existTabs' : new Array(),
		'default' : { //所有参数
			'id' : new Date().getTime(), //tab id
			'name' : '', //tab 名称
			'url' : '', //tab主体引入的页面的地址
			'html' : '', //tab主体的元素字符串
			'index' : -1, //tab的索引值，以0开始
			'isNewTab' : true, //是否重新打开一个新的tab
			'hasClose' : true, //tab右上角的关闭图标
			'isRefresh' : false //是否刷新页面
		},
		'exists' : function(tabId) {
			return $.inArray(tabId, this.existTabs);
		},
		'close' : function(obj) {
			if(!(obj.index > -1)) {
				obj.index = this.exists(obj.id);
			}
			if(obj.index > -1) {
				this.tabTitle.children(':eq(' + obj.index + ')').remove();
				this.tabContent.children(':eq(' + obj.index + ')').remove();
				this.activity({
					index : obj.index - 1				
				});
				this.existTabs.splice(obj.index, 1);
			}
		},
		'hide' : function(obj) {
			if(!(obj.index > -1)) {
				obj.index = this.exists(obj.id);
			}
			
			if(obj.index > -1) {
				this.tabTitle.children(':eq(' + obj.index + ')').children('span').removeClass(this.titleActivityClass).addClass(this.titleHideClass).css('cursor', 'pointer');
				this.tabTitle.children(':eq(' + obj.index + ')').children('span').next().children().attr('src', $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/xin_xx1.jpg');
				this.tabContent.children(':eq(' + obj.index + ')').addClass(this.contentHideClass);
			}
		},
		'activity' : function(obj) {
			if(!(obj.index > -1)) {
				obj.index = this.exists(obj.id);
			}
			
			if(obj.index > -1) {
				for(var i = 0; i < this.existTabs.length; i ++) {
					if(i == obj.index) {
						this.tabTitle.children(':eq(' + i + ')').children('span').removeClass(this.titleHideClass).addClass(this.titleActivityClass).css('cursor', 'auto');
						this.tabTitle.children(':eq(' + obj.index + ')').children('span').next().children().attr('src', $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/xin_xx.jpg');
						this.tabContent.children(':eq(' + i + ')').removeClass(this.contentCloseClass);
					} else {
						var hideConfig = {
							'index' : i
						}
						this.hide(hideConfig);
					}
				}
			}
		},
		'open' : function(obj, from) {
			var self = this;
			//modify by eko.zhan at 2014-10-23 ceair提出需求：知识展示在新的tab中打开，如需修改代码如下
			//obj.id = Math.random();
			var tabIndex = this.exists(obj.id);
			if(from) {
				$(from).attr('frameId', obj.id);
			}
			obj.isNewTab = obj.isNewTab && obj.isNewTab == true ? obj.isNewTab : false;
			if(!obj.isNewTab && tabIndex > -1) {
				this.activity(obj);
				if(obj.isRefresh) {
					//标题
					this.tabTitle.find("span[class='xinjian_wz']").eq(0).attr('title',obj.title);
					//内容
					var iframe = this.tabContent.children(':eq(' + tabIndex + ')');
					iframe.attr('src', obj.url);
					if (!isLowerBrowser()){	//add by eko.zhan at 2016-04-25 11:55 低版本浏览器不显示遮罩层，因为太特么卡了
						var m = this.tabContent.iframeLoading('加载中……');
					}
					var handler = window.setInterval(function() {
						if(document.readyState == 'complete') {
							this.clearInterval(handler);
							if (!isLowerBrowser()){	//add by eko.zhan at 2016-04-25 11:55 低版本浏览器不显示遮罩层，因为太特么卡了
								m.iframeLoadEnd();
							}
						}
					}, 200);
					
					setCurrentIframeHeight(iframe);
				}
			} else {
				for(var i = 0; i < this.existTabs.length; i ++) {
					var hideConfig = {
						'index' : i,
						'id' : this.existTabs[i]
					}
					this.hide(hideConfig);
				}

				var tabNode = $('<p></p>');
				if(obj.title!=null&&obj.title!=''){
					tabNode.append($('<span class="xinjian_wz" title = "'+obj.title+'">' + TABOBJECT_name(obj.name) + '</span>'));
				}else{
					//modify by eko.zhan at 2015-11-16 16:00 广东移动提出需要显示title
					tabNode.append($('<span class="xinjian_wz" title="' + obj.name + '">' + TABOBJECT_name(obj.name) + '</span>'));
				}
				if(obj.hasClose) {
					tabNode.append($('<a href="javascript:void(0)" class="xinjian_XX"><img src="' + $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/xin_xx.jpg" width="8" height="8"/></a>').click(function() {
						self.close({
							id : obj.id
						});
					}));
				}
				this.tabTitle.append($(tabNode).click(function(){
					self.activity({
						'id' : obj.id
					});
				}));
				if(obj.html) {
					this.tabContent.append($('<div>' + obj.html + '</div>'));
				} else {
					var iframe = $('<iframe src="' + obj.url + '" width="100%" height="454" frameborder="0" scrolling="no"></iframe>');
					this.tabContent.append(iframe);
					if (!isLowerBrowser()){	//add by eko.zhan at 2016-04-25 11:55 低版本浏览器不显示遮罩层，因为太特么卡了
						var m = iframe.iframeLoading('加载中……');
					}
					var handler = window.setInterval(function() {
						if(document.readyState == 'complete') {
							this.clearInterval(handler);
							if (!isLowerBrowser()){	//add by eko.zhan at 2016-04-25 11:55 低版本浏览器不显示遮罩层，因为太特么卡了
								m.iframeLoadEnd();
							}
						}
					}, 200);
					
					setCurrentIframeHeight(iframe);
				}
				this.existTabs.push(obj.id);
				//右键
				tabNode.bind('contextmenu',function(e){
					if(e && e.preventDefault){
						//阻止默认浏览器动作(W3C) 
						e.preventDefault(); 
					}else{
						//IE中阻止函数器默认动作的方式 
						window.event.returnValue = false; 
					}
				});
				var x;var y;
				tabNode.mousedown(function(e){
					if(e.button == 2){
						$('#menu_close').attr('objId',obj.id);
						if(e.pageX || e.pageY){ 
							x=e.pageX;
							y=e.pageY;
						} else{
							x=e.clientX + document.body.scrollLeft - document.body.clientLeft;
							y=e.clientY + document.body.scrollTop - document.body.clientTop;
						}
						$('#menu_close').css({"top":y+"px", "left":x+"px", "visibility":"visible"});
						e.stopPropagation();
						$("body").bind("mousedown",function(event){
							if (!(event.target.id == "menu_close" || $(event.target).parents("#menu_close").length>0)) {
								$("#menu_close").css({"visibility" : "hidden"});
								$("body").unbind("mousedown");
							}
						});
					}
				});
			}
		},
		'getActivityPanel' : function() {
			return this.tabContent.children().not(':hidden');
		},
		'initClose' : function(){
			$('#menu_close').bind('contextmenu',function(e){
				if(e && e.preventDefault){
					//阻止默认浏览器动作(W3C) 
					e.preventDefault(); 
				}else{
					//IE中阻止函数器默认动作的方式 
					window.event.returnValue = false; 
				}
			});
			var self = this;
			$('#menu_close li:eq(0)').click(function(){
				var id = $('#menu_close').attr('objId');
				var index = self.exists(id)<0 ? 0:self.exists(id);
				var del = new Array();
				if(index>-1){
					for(var i=1; i<self.existTabs.length; i++){
						if(i != index){
							del.push(i);
						}
					}
					self.activity({
						index : index				
					});
					for(var j=del.length-1; j>=0; j--){
						self.tabTitle.children(':eq(' + del[j] + ')').remove();
						self.tabContent.children(':eq(' + del[j] + ')').remove();
						self.existTabs.splice(del[j], 1);
					}
					del.splice(0,del.length);
				}
				$('#menu_close').css('visibility','hidden');
				$("body").unbind("mousedown");
			});
		}
	}
}

/*
* TABOBJECT标题长度修改
* 参数：
	TABOBJECT_name 标题
	len 长度 默认值4
*/
function TABOBJECT_name(TABOBJECT_name,len){
	if(isNaN(len))len = 4;
	//占位符'...' 默认长度为1汉字长度
	if(TABOBJECT_name!=null && TABOBJECT_name.length>len+1){
		TABOBJECT_name = TABOBJECT_name.substr(0,len)+'...';
	}
	return TABOBJECT_name;
}

/*
 * 设定iframe高度自适应 
 */
function reinitIframe(){
	var iframes = document.getElementsByTagName('iframe');
	var bodyHeight = $(window).height();
	var mainHeight = bodyHeight - 140;
	try{
		for(var i = 0; i < iframes.length; i ++) {
			var iframe = iframes[i];
/*			var bHeight = iframe.contentWindow.document.body.offsetHeight;
			var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
			var dHeight = iframe.contentWindow.document.body.clientHeight;
			var height = Math.max(bHeight, dHeight);
			if(dHeight < mainHeight)
				dHeight = mainHeight;
			iframe.height =  dHeight;*/
			parseInt()
			var realHeight = $(iframe).contents().height();
			iframe.height = realHeight < mainHeight ? mainHeight : realHeight;
		}
	} catch (ex){}
	
}

/**
 * @author eko.zhan at 2015-10-08 14:49
 * @author eko.zhan at 2015-10-30 11:10
 * @iframe jQuery的iframe对象
 * 在 reinitIframe 方法中，定时刷新iframe高度，可能会影响到性能
 * 设置iframe高度
 */
function setCurrentIframeHeight(iframe){
	//TODO chrome 下iframe尚未加载 readyState 却是 complete
	//alert(iframe.get(0).contentWindow.document.readyState);
	if ($.browser.msie &&  $.browser.version<8){
		var t = window.setInterval(function(){
			if ($(iframe).contents()[0].readyState=='complete'){
				var bodyHeight = $(window).height();
				var mainHeight = bodyHeight-140;
				var realHeight = $(iframe).contents().height();
				//alert(mainHeight + ' | ' + realHeight);
				$(iframe).height(realHeight < mainHeight ? mainHeight : realHeight);
				window.clearInterval(t);
			}
		}, 500);
	}
}

//modify by eko.zhan at 2015-11-10 18:19 IE8 以下高度调整不采用 定时刷新的方式
if ($.browser.msie &&  $.browser.version<8){
	
}else{
	window.setInterval("reinitIframe()", 500);
}

/**
 * 判断是否是低版本浏览器 ie6 7 8
 */
function isLowerBrowser(){
	var agent = navigator.userAgent.toLowerCase();
	if (agent.indexOf('msie 6')!=-1 || agent.indexOf('msie 7')!=-1 || agent.indexOf('msie 8')!=-1){
		return true;
	}
	return false;
}