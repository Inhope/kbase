$(function() {
	OBJECT.init();
	gettaskhint();
	TABOBJECT.initClose();
	
	//头部各功能按钮
	HeaderObject = {
//		logoutBtn : $('div.header_right_bottom li>a:eq(0)'),
//		pwdEditBtn : $('div.header_right_bottom li>a:eq(1)'),
//		settingBtn : $('div.header_right_bottom li>a:eq(2)'),
//		userInfoBtn : $('div.header_right_bottom li>a:eq(3)'),
//		createFaqBtn : $('div.header_right_bottom li>a:eq(4)'),
//		noticeManageBtn : $('div.header_right_bottom li>a:eq(5)'),
//		mindManagerBtn : $('div.header_right_bottom li>a:eq(6)'),
        logoutBtn : $('#navLogout'),
		pwdEditBtn : $('#navPwdEdit'),
		settingBtn : $('#navSetting'),
		userInfoBtn : $('#navUserInfo'),
		createFaqBtn : $('#navCreateFaq'),
		noticeManageBtn : $('#navNoticeManage'),
		mindManagerBtn : $('#navMindManager'),
		centerManagerBtn : $('#navIndividual')
	}
	
	//加载 发起流程 入口 add by eko.zhan at 2015-04-29 11:45
	if ($.workflow.isUsable()){
		$('#navLogout').parent('li').parent('ul').append('<li><a id="navFireWorkflow" href="javascript:void(0);">发起流程</a></li>');
		$('#navFireWorkflow').click(function(){
			$.workflow.fire();
		});
	}
	
	if(typeof($('#navIndividual').html())!='undefined'){
	    //个人中心
		HeaderObject.centerManagerBtn.click(function(){
			TABOBJECT.open({
				id : 'center',
				name : '个人中心',
				hasClose : true,
				url: $.fn.getRootPath()+'/app/personal/personal!center.htm',
				//老的个人中心入口地址，hidden by eko.zhan at 2015-07-08
				//url: $.fn.getRootPath()+'/app/individual/individual.htm',
				isRefresh : true
			}, this);
		});
		
		//获取个人中心数量（10分钟更新一次数据）是否开启流程在getTodoCount4Personal方法中判断
		$.workflow.getTodoCount4Personal();
		window.setInterval(function(){
			$.workflow.getTodoCount4Personal();
		}, 10*60*1000);
		/*
		var kbase_workflow_show = $("#kbase_workflow_show").val();
		if(kbase_workflow_show=='true'){
			var individual = null;
			try{
				//个人中心数量
				getindividualSum(HeaderObject.centerManagerBtn);
				var individual = setInterval(function(){getindividualSum(HeaderObject.centerManagerBtn);},3*60*1000);
			}catch(e){
				if(individual != null)clearInterval(individual);
			}
		}*/
	}else{
	   //公告管理
		HeaderObject.noticeManageBtn.click(function(){
			TABOBJECT.open({
				id : 'notice',
				name : $(this).html(),
				title : $(this).html(),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/notice/notice!list.htm',
				isRefresh : true
			}, this);
		});
		//我的信息
		HeaderObject.userInfoBtn.click(function(){
			TABOBJECT.open({
				id : 'myInfo',
				name : '我的信息',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/auther/work-flow.htm',
				isRefresh : true
			}, this);
		});
		
		var kbase_workflow_show = $("#kbase_workflow_show").val();
		if(kbase_workflow_show=='true'){
			var interval = null;
			try{
				//查询待办数量
				getWaitAuditingSum(HeaderObject.userInfoBtn);
				var interval = setInterval(function(){getWaitAuditingSum(HeaderObject.userInfoBtn);},60000);
			}catch(e){
				if(interval != null)clearInterval(interval);
			}
		}
			    
		//修改密码
		HeaderObject.pwdEditBtn.click(function(){
			TABOBJECT.open({
				id : 'pwdEdit',
				name : $(this).html(),
				title : $(this).html(),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/auther/user!pwdEditTo.htm',
				isRefresh : true
			}, this);
		});
	}
	
	//公告管理
	$('#navBulletin').on('click', function(){
		var _diaWidth = screen.width-50;
		var _diaHeight = screen.height-100;
		var _scrWidth = screen.width;
		var _scrHegiht = screen.height;
		var _diaLeft = (_scrWidth-_diaWidth)/2;
		var _diaTop = (_scrHegiht-_diaHeight)/2;
		var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0, resizable=1, status=0, fullscreen=0';
		window.open($.fn.getRootPath() + '/app/notice/bulletin.htm', '_blank', params);
	});
	
	//页面默认加载第一个选项卡(首页)
	TABOBJECT.open({
		id : new Date().getTime(),
		name : '首页',
		hasClose : false,
		url : $.fn.getRootPath() + '/app/index/index.htm'
	}, this);
	
	
	
	//我的脑图
	HeaderObject.mindManagerBtn.click(function(){
		TABOBJECT.open({
			id : 'mindmanager',
			name : $(this).html(),
			title : $(this).html(),
			hasClose : true,
			//url : $.fn.getRootPath() + '/app/mindmap/mindmap!getmindmap.htm',
			url: $.fn.getRootPath()+'/app/mindmap/mindmap!show.htm',
			isRefresh : true
		}, this);
	});
	
	//合理化建议
	HeaderObject.createFaqBtn.click(function(){
		//window.__KBASE_WORKFLOW_MODE 流程引擎启用版本(1新版本,默认就版本)
		if(window.__KBASE_WORKFLOW_MODE=="1"){
			$.layer({
			    type: 2,
			    border: [10, 0.3, '#000'],
			    title: "合理化建议",
			    closeBtn: [0, true],
			    iframe: {src : $.fn.getRootPath() + '/app/auther/work-flow!knowledgeCreate.htm'},
			    area: ['500px', '350px']
			});
		}else{
			TABOBJECT.open({
				id : 'createFaq',
				name : $(this).html(),
				title : $(this).html(),
				hasClose : true,
				url : $.fn.getRootPath() + '/app/auther/work-flow!knowledgeCreate.htm',
				isRefresh : true
			}, this);
		}
	});
    //首页设置
	HeaderObject.settingBtn.click(function() {
		TABOBJECT.open({
			id : 'indexSetting',
			name : $(this).html(),
			title : $(this).html(),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/auther/index-setting.htm',
			isRefresh : true
		}, this);
	});
	//热词
	//一般搜索
	$('div.content_gjz a').click(function(){
		TABOBJECT.open({
			//id : 'values',
			//name : '知识展示',
			id : 'ordinary_s',
			name : '搜索',
			title : $(this).html(),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=9&askContent='+encodeURIComponent($(this).html()),
			isRefresh : true
		}, this);
	});
	//注销
	HeaderObject.logoutBtn.click(function() {
		layer.confirm('确定退出系统?', function(){
			$.workflow.logout();
			window.setTimeout(function(){
				window.location.href = $.fn.getRootPath() + '/app/login/logout.htm';
			}, 500);
		});
	});
	
	//add by eko.zhan at 2015-08-17 go top required jquery.goup.min.js
	//由于 wanpeng.liu 已经完成了[回到顶部]的功能，所以这里屏蔽掉，如有需要，以后再放开该功能，请勿删除
	//$.goup({trigger: 100, bottomOffset: 20, locationOffset: 20});
	
}); //end document.ready function

OBJECT = {
	init : function() {
		/***********渲染头部事件***********/
		!window.HEADER || window.HEADER();
		/*********渲染左侧树事件*********/	
		!window.LEFT || window.LEFT();
		/*********渲染选项卡事件*********/	
		!window.TAB || window.TAB();
		
	}
}

function getindividualSum(obj){
	$.ajax({
		type: "POST",
		dataType : 'text',
		timeout : 5000,
		url: $.fn.getRootPath()+'/app/individual/individual!getindividualcount.htm',
		async: false,
		success: function(msg){
			if(msg=='0'||isNaN(msg)){
				$(obj).html("个人中心");
			}else {
				$(obj).html("个人中心(<b style=\"font-size: 18px;color: yellow;\" >"+msg+"</b>)");
			}
		}
	});
}

function gettaskhint(){
   if(isOpentask){
     $.ajax({
        url: $.fn.getRootPath()+'/app/task/pe-task!gettaskcount.htm',
		type: "POST",
		timeout : 5000,
		async: false,
		success: function(msg){
			if(msg>0){
			  layer.alert("您有"+msg+"个任务需要完成",-1);
			}
		}
	});
   }

}


function getWaitAuditingSum(obj){
	$.ajax({
		type: "POST",
		dataType : 'text',
		timeout : 5000,
		url: $.fn.getRootPath()+'/app/auther/work-flow!getWaitAuditingSum.htm',
		async: false,
		success: function(msg){
			if(msg=='0'||isNaN(msg)){
				$(obj).html("我的信息");
			}else {
				$(obj).html("我的信息(<b style=\"font-size: 18px;color: yellow;\" >"+msg+"</b>)");
			}
		}
	});
}

//add by eko.zhan at 2016-01-20 用于在弹出窗中打开公告管理（兼容IE7）
//调用方法详见 WEB-INF/app/bulletin/index.jsp
var KbasePlugin = {
	guideNotice: function(){
		TABOBJECT.open({
			id : 'notice',
			name : '公告管理',
			hasClose : true,
			url : $.fn.getRootPath()+'/app/notice/notice!list.htm?isVirgin=1&isNote=0',
			isRefresh : true
		});
	},
	openNotice: function(_url){
		TABOBJECT.open({
			id : 'notice',
			name : '公告管理',
			hasClose : true,
			url : _url,
			isRefresh : true
		});	
	}
}
