LEFT = function() {
	NavigationPortaitLeft = {
		parentMenu : $('div.content_left>ul.juli>li'),
		nodes : $('ul.jiedian>li'),
		showStyle : 'left_1',
		hideStyle : 'left_2',
		openOrCloseLong : 250,
		init : function() {
			var self = this;
			!this.parentMenu || this.parentMenu.each(function(index){
				$('>div:eq(0)', $(this)).click(function() {
					if($(this).hasClass(self.showStyle)) {
						$(this).removeClass(self.showStyle).addClass(self.hideStyle);
						$(this).next().slideUp(self.openOrCloseLong);
					} else {
						$(this).removeClass(self.hideStyle).addClass(self.showStyle);
						$(this).next().slideDown(self.openOrCloseLong)
						var notThisObject = self.parentMenu.not($(this).parent());
						$('>div:eq(0)', notThisObject).removeClass(self.showStyle).addClass(self.hideStyle);
						$('>div:eq(0)', notThisObject).next().slideUp(self.openOrCloseLong);
					}
				});
			});
			
			//所有结点的点击事件
			this.nodes.click(function(){
				if(!$(this).attr('frameId')) {
					$(this).attr('frameId', new Date().getTime());
				}
				
				var text = $.trim($('a', this).text());
				
				//if(text.length > 5) {
					//text = text.substring(0, 5);
				//}
				TABOBJECT.open({
					id : $(this).attr('frameId'),
					name : text,
					title : text,
					hasClose : true,
					url : $('a', this).attr('url'),
					isRefresh : true
				}, this);
				return;
			});
		}
	}
	NavigationPortaitLeft.init();
	
	NavigationPortaitLeftDrag = {
		dragDiv : $('div.content_line'), //被拉伸的div
		miniBtn : $('div.content_line>a'), //拉伸div上的三角按钮
		leftPanel : $('div.content_left_bg'), //左侧面板
		rightPanel : $('div.content_right'), //右侧面板
		categoryTree : $('ul#categoryTree'),
		smsCategoryTree : $('ul#smsCategoryTree'),
		isLeft : 'left',
		isRight : 'right',
		leftImg : $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/left_san2.jpg',
		rightImg : $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/left_san1.jpg',
		leftPanelDisplayWidth : 0,
		leftPanelMinWidth : 170,
		leftPanelMaxWidth : 350,
		dragOption : {
			bDraging : false,// 是否在拖动	
			initDiffX : 0, //鼠标初始点击的横坐标
			initDragDivX : 0 //鼠标初始点击拖拽树时点击点距离树的最左侧值
		},
		drag : function() {
			var self = this;
			var categoryTreeWidth = 0;
			var smsCategoryTreeWidth = 0;
			this.dragDiv.mousedown(function(e1) {
				self.dragOption.bDraging = true;
				if(this.setCapture) { 
					this.setCapture();  
			    } else if(window.captureEvents){
			    	window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP);
			    } 
			    categoryTreeWidth = self.categoryTree.width();
			    smsCategoryTreeWidth = self.smsCategoryTree.width();
				self.dragOption.initDiffX = e1.clientX + document.body.scrollLeft - document.body.clientLeft;
				self.dragOption.initDragDivX = self.dragOption.initDiffX - self.dragDiv.offset().left;
				//绑定mousemove事件
				$(document).on('mousemove','*', function(e) {
					if(self.dragOption.bDraging) {
						var moveX = e.clientX + document.body.scrollLeft - document.body.clientLeft;
						var changeX = moveX - self.dragOption.initDiffX; 
						//update by wilson.li at 2016-05-10
						if(changeX > 5){
							moveX = self.leftPanelMaxWidth;
							self.dragOption.bDraging = false;
							//解绑mousemove事件
							$(document).unbind("mousemove","");
							if(this.releaseCapture) { 
								this.releaseCapture();  
						    }
						}else if(changeX < -5){
							moveX = 210;
							self.dragOption.bDraging = false;
							//解绑mousemove事件
							$(document).unbind("mousemove","");
							if(this.releaseCapture) { 
								this.releaseCapture();  
						    }
						}
						changeX = moveX - self.dragOption.initDiffX; 
						var lw = self.leftPanel.width();
						if(self.leftPanelMinWidth <= moveX) {
							self.leftPanel.width(moveX - self.dragOption.initDragDivX);
							//设置竖线的left值
							self.dragDiv.css('left', moveX - self.dragOption.initDragDivX);
							//设置右侧面板的宽度
							self.rightPanel.css('marginLeft', moveX - self.dragOption.initDragDivX + 6);
							//设置书的宽度
							self.categoryTree.width(categoryTreeWidth + changeX);
							//设置短信树宽度
							self.smsCategoryTree.width(smsCategoryTreeWidth + changeX);
							//记录面板宽度的值
							self.leftPanelDisplayWidth = self.leftPanel.width();
						}
					}
				});
			});

			$(document).find('*').mouseup(function() {
				self.dragOption.bDraging = false;
				//解绑mousemove事件
				$(document).unbind("mousemove","");
				
				if(this.releaseCapture) { 
					this.releaseCapture();  
			    }
			});
		},
		flexible : function() {
			this.leftPanelDisplayWidth = this.leftPanel.width();
			var self = this;
			this.miniBtn.click(function() {
				//modify by eko.zhan at 2015-10-10 10:28 调整固定左侧导航栏后，内嵌至NGCC在IE7下会出现分隔栏位置出错的情况
				//向左收
				//if($(this).attr('flag') == self.isLeft) {
				if (self.leftPanel.get(0).style.display!='none'){
					$(this).attr('flag', self.isRight);
					$('img', this).attr('src', self.rightImg);
					var leftPanelWidth = self.leftPanel.width();
					self.leftPanel.animate({width : 0}, 250, function() {
						$(this).hide();
					});
					//self.dragDiv.css('cursor', 'pointer').animate({left: (self.dragDiv.offset().left - self.leftPanelDisplayWidth)}, 250);
					self.dragDiv.animate({left: 0}, 250);
					var rightPanelMarginLeft = parseInt(self.rightPanel.css('marginLeft').replace('px', ''));
					self.rightPanel.animate({marginLeft: (rightPanelMarginLeft - self.leftPanelDisplayWidth) + 'px'}, 250);
					
				}
				//向右伸
				else {
					$(this).attr('flag', self.isLeft);
					$('img', this).attr('src', self.leftImg);
					
					self.leftPanel.show().animate({width : self.leftPanelDisplayWidth}, 250);
					//self.dragDiv.css('cursor', 'col-resize').animate({left: (self.dragDiv.offset().left + self.leftPanelDisplayWidth)}, 250);
					self.dragDiv.animate({left: self.leftPanelDisplayWidth}, 250);
					var rightPanelMarginLeft = parseInt(self.rightPanel.css('marginLeft').replace('px', ''));
					self.rightPanel.animate({marginLeft: (rightPanelMarginLeft + self.leftPanelDisplayWidth) + 'px'}, 250);
					
				}
				
				//如果启用 easyui tab 切换分隔栏要调整tab宽度
				if ($('#tabs').length==1){
					window.setTimeout(function(){
						var _tabWidth = $(window).width()-10-$('.content_left_bg').width();
						pageContext.resizeTab(_tabWidth);
					}, 300);
				}
			});
			
			//当左侧收起之后给之加上click事件
			self.dragDiv.bind('click', function() {
				if(self.leftPanel.is(':hidden'))
					self.miniBtn.trigger('click');
			});
			
			self.dragDiv.dblclick(function() {
				self.miniBtn.trigger('click');
			});
		},
		render : function() {
			this.flexible();
			this.drag();
		}
	};
	NavigationPortaitLeftDrag.render();
	
	//add by eko.zhan at 2016-07-14 16:18 拖拽分隔线动态调整主显示区域宽高
	var isDrag = false;
	$('.content_line').mousedown(function(){
		isDrag = true;
		//console.log($('.content_left_bg').width());
	});
	
	$('body').mouseup(function(){
		if (isDrag){
			isDrag = false;
			//console.log($('.content_left_bg').width());
			//如果启用 easyui tab 切换分隔栏要调整tab宽度
			//add by eko.zhan at 2016-07-14 15:00 这里有点问题，这段代码会执行4次，click body的时候会执行五次
			if ($('#tabs').length==1){
				var _tabWidth = $(window).width()-10-$('.content_left_bg').width();
				pageContext.resizeTab(_tabWidth);
			}
		}
	});
	
	
	/*******************加载树事件***********************/
	//$.fn.zTree.init($("#categoryTree"), setting);
	//treeSearch();
	//@author eko.zhan 
	var menuH = ($('ul[class="juli"]').children('li').length)*32;/*一级菜单的数量*/
	var seachH = 65;/*知识分类搜索区高度*/
	var headH = 75*Math.abs(SystemKeys.userData.bannerCollapse-1);/*head的高度*/
	$("#categoryTree").kbsTree({
		menuId: 'treeMenuJ',
		keywordsId: 'treeInput',
		searchBtnId: 'treeButton',
		style:{
			height: $(window).height()-menuH-seachH-headH
		}
	});
	/*******************加载树事件结束***********************/
	/*******************加载短信树事件***********************/
	//首次点击 短信发送 按钮时，加载短信树 add by heart.cao 2016-05-09
	var _hasLoadSMSTree = 0;
	$('a#smsCategoryMenu').click(function(){
	    if(_hasLoadSMSTree==0){
			$('#smsCategoryTree').smsTree({
			    style:{
					height: $(window).height()-menuH-headH
				}
			});
			_hasLoadSMSTree = 1;	    
	    }
	    if($(this).parent().attr("class")=='left_2'){
            if(TABOBJECT.exists('smsBox')==-1){
				TABOBJECT.open({
					id : 'smsBox',
					name : '短信发送',
					title : '短信发送',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/sms/sms-ontology!smsBox.htm',
					isRefresh : true
				}, this);		            
            }else{ //再次点击 “短信发送” 直接跳转到已打开 “短信发送” tab    modified by barry.li at 20161028
				// $('span:contains("短信发送")').each(function () {
				// 	$(this).click();
				// });

				//modified by Barry.li at 20161116
				$('#tabs').tabs('select', '短信发送');
			}
	    }
	});
	/*******************加载短信树事件结束***********************/
}