HEADER = function() {
	backTop();
    /*****************更改用户密码********************/
    window.setTimeout(function(){
        modifyUserPwd();
    },300);
	/*****************选择归属地********************/
	locationChange();
	/*****************选择品牌********************/
	brandChange();
	/*****************维度刷新********************/
	refresh();
	/*****************跑马灯*******************/
	//$('#noticeMarquee').html('');
	//getPaoMaDeng();
	/** 每隔20分钟定时检查一次公告跑马灯的更新内容 modify by eko.zhan at 2015-02-13 ***/
	marqueeLoop();
	/* hidden by eko.zhan at 2015-06-25 定时器导致东航CLOSE_WAIT问题暂时未解决*/
	window.setInterval(function(){
		marqueeLoop();
	}, 30*60*1000);
	/*******************右下角弹窗*********************/
	
	//getTanChuang();
	/*
	setInterval(function(){
		if($('div.ggzs_dan').is(":hidden")){
			getTanChuang();
		}
	},60000*5);*/
	//弹窗初始化
	//modify by eko.zhan at 2016-08-24 09:45 
	$('#win').window({
		title: '公告弹窗列表',
	    width: 470,
	    height: 230,
	    top:($(window).height() - 230),
	    left:($(window).width() - 520),
	    modal: false,
	    closed: true,
	    draggable: false,
	    collapsible: true,
	    minimizable: false,
	    resizable: false,
	    maximizable: false,
	    onCollapse: function(){
	    	$(this).panel('move', {top: $(window).height()-50})
	    },
	    onBeforeExpand: function(){
	    	$(this).panel('move', {top: $(window).height()-230})
	    }
	});
	/** 每隔5分钟检查一次公告弹窗 modify by eko.zhan at 2015-02-13 **/
	//首次公告弹窗时间推迟3分钟
	window.setTimeout(function(){
	   noticeLoop();
	}, 3*60*1000);
	/* hidden by eko.zhan at 2015-06-25 定时器导致东航 CLOSE_WAIT 问题暂时未解决*/
	window.setInterval(function(){
		if($('div.ggzs_dan').is(":hidden")){
			noticeLoop();
		}
	}, 10*60*1000); //公告弹窗间隔时间调整为10分钟
	/********************一般搜索****************************/
	search();
	
	/********************高级搜索****************************/
	var seniorSearch = {
		searchDiv : $('#seniorSearchDiv'),
		seniorSearchBtnEl : $('#seniorSearchBtn'),
		closeBtnEl : $('div.index_dan_title a'),
		form : {
			contentEl : $('#s_content'),
			searchType : $('input[name=s_searchType]:checked'),
			agingType : $('#s_agingType option:selected'),
			createDateEl : $('#s_createDate'),
			endDateEl : $('#s_endDate'),
			catgoryDirEl : $('#s_catgory_dir option:selected'),
			location : $('#s_location option:selected'),
			brand : $('#s_brand option:selected')
		},
		dateSetting : {
			required : false,
			closeText : '关闭',
			currentText : '今天',
			editable : false,
			formatter :function(e){
				var year = e.getFullYear();
				var month = e.getMonth()+1;
				if(month <= 9){
					month = '0' + month;
				}
				var date = e.getDate();
				if(date <= 9){
					date = '0' + date;
				}
				return year+'-'+month+'-'+date;
			}
		},
		disabled : function() {
			var self = this;
			$('input[name=s_searchType]').click(function() {
				if($(this).val() == '3') {
					$('#s_agingType').attr('disabled', 'disabled');
					$('#s_catgory_dir').attr('disabled', 'disabled');
					// modify by baidengke 2016年5月19日 附件加上维度搜索条件
					$('#s_location').attr('disabled', false);
					$('#s_brand').attr('disabled', false);
				} else {
					$('#s_agingType').removeAttr('disabled');
					$('#s_catgory_dir').removeAttr('disabled');
					$('#s_location').removeAttr('disabled');
					$('#s_brand').removeAttr('disabled');
				}
			});
		},
		renderEvent : function() {
			var self = this;
			this.seniorSearchBtnEl.click(function() {
				if(self.searchDiv.is(':hidden')) {
					/*modify by eko.zhan at 2016-10-13 14:22 部分客户提出高级搜索面板弹出时搜索关键词默认显示首页上的关键词*/
					var keywords = $('.kuang').val();
					if (keywords.indexOf('请输入您要搜索的内容')==-1){
						self.form.contentEl.val(keywords);
					}else{
						self.form.contentEl.val('');
					}
					$(document).showShade();
					self.searchDiv.css({
						left : ($(window).width() - self.searchDiv.width()) / 2,
						top : ($(window).height() - self.searchDiv.height()) / 2,
						zIndex : '10471'
					}).show();
				} else {
					self.searchDiv.hide();
				}
			});
			
			self.closeBtnEl.click(function() {
				$(document).hideShade();
				self.searchDiv.hide();
			});
			
			var startDate = self.form.createDateEl.datebox(self.dateSetting).next().css('width', '128px').find('input').css('width', '107px');
			
			startDate.parent().parent().css({
				'position' : 'relative'
			});
			
			startDate.next().children('span').css({
				'position' : 'absolute',
				'right' : '3',
				'top' : '2px'
			});
			
			var endDate = self.form.endDateEl.datebox(self.dateSetting).next().css('width', '128px').find('input').css('width', '107px');
		
			//执行高级搜索
			$('#s_commit').attr('frameId', 'snior_s').click(function() {
				if(!self.form.contentEl.val()) {
					parent.layer.alert('请填写搜索内容', -1);
					return false;
				}
				if(startDate.val() && endDate.val()) {
					var s = new Date(startDate.val().replace(/-/gi, '/')).getTime();
					var e = new Date(endDate.val().replace(/-/gi, '/')).getTime();
					if(e <= s) {
						parent.layer.alert('起始日期必须小于结束日期', -1)
						return false;
					}
				}
				TABOBJECT.open({
					id : $(this).attr('frameId'),
					name : '高级搜索',
					title : self.form.contentEl.val(),
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/senior-search.htm?' + getValues(),
					isRefresh : true
				}, this);
				self.closeBtnEl.trigger('click');
				return false;
				
			});
			
			//返回url参数 key=value
			var getValues = function() {
				var content = self.form.contentEl.val();
				var searchType = $('input[name=s_searchType]:checked').val();
				var agingType = $('#s_agingType option:selected').val();
				var startDateText = startDate.val();
				var endDateText = endDate.val();
				var catgoryDirId = $('#s_catgory_dir option:selected').val();
				catgoryDirId = catgoryDirId ? catgoryDirId : '';
				var location = $('#s_location option:selected').val();
				var brand = $('#s_brand option:selected').val();
				location = location ? location : '';
				brand = brand ? brand : '';
				return 'content=' + encodeURIComponent(content) + '&searchType=' + searchType + '&agingType=' + 
						agingType + '&startDate=' + startDateText + '&endDate=' + endDateText +
						'&catgoryDirId=' + catgoryDirId + '&location=' + location + '&brand=' + brand;
			}
			
			this.disabled();
			
			$('#s_cancel').click(function() {
				$('#s_agingType').removeAttr('disabled');
				$('#s_catgory_dir').removeAttr('disabled');
				$('#s_location').removeAttr('disabled');
				$('#s_brand').removeAttr('disabled');
			});
		}
	};
	seniorSearch.renderEvent();
	
	/*****目录搜索****/
	
	var dirSearch = {
		searchDiv : $('#dirSearch'),
		seniorSearchBtnEl : $('#dirSearchBtn'),
		closeBtnEl : $('div.index_dan_title a'),
		form : {
			contentEl : $('#dir_content')
		},
		disabled : function() {
			var self = this;
		},
		renderEvent : function() {
			var self = this;
			this.seniorSearchBtnEl.click(function() {
				if(self.searchDiv.is(':hidden')) {
					$(document).showShade();
					self.searchDiv.css({
						left : ($(window).width() - self.searchDiv.width()) / 2,
						top : ($(window).height() - self.searchDiv.height()) / 2,
						zIndex : '10471'
					}).show();
				} else {
					self.searchDiv.hide();
				}
			});
			
			self.closeBtnEl.click(function() {
				$(document).hideShade();
				self.searchDiv.hide();
			});
			
			var flg = false;
			
			//搜索
			$('#dir_commit').attr('frameId', 'snior_d').click(function() {
				var param = getValues();
					  if(!self.form.contentEl.val() && !flg) {
							parent.layer.alert('请选择搜索条件', -1);
							return false;
						}
						TABOBJECT.open({
							id : $(this).attr('frameId'),
							name : '目录搜索',
							title : self.form.contentEl.val(),
							hasClose : true,
							url : $.fn.getRootPath() + '/app/search/diretory-search.htm?' +param ,
							isRefresh : true
						}, this);
						flg = false;
						self.closeBtnEl.trigger('click');
						return false;
				
			});
			
			//返回url参数 key=value
			var getValues = function() {
				var content = self.form.contentEl.val();
				var searArray = $('#dir_form select')
				var params = '';
				searArray.each(function(){
					var id = this.id;
					var tag = this.id.split('_')[1];
					var value = $(this).val();
					if(value != '' && value !=undefined && value!=null){
						flg = true;
						params = params + tag + '=' + encodeURIComponent(value) + '&';
					}
				});
				params = params +  'dirName=' + encodeURIComponent(content)
				return params;
			}
			
			this.disabled();
			
		}
	};
	dirSearch.renderEvent();
	
	/***********全文按钮事件***********/
	$('a[id=quanwen]').attr('frameId', 'snior_s').click(function() {
		var text = $('div.content_ss>input.kuang').val();
		if(text) {
			if($.trim(text) == '请输入您要搜索的内容...') {
				$.fn.hint('请输入您要搜索的内容...');
			} else {
				var url = '/app/search/senior-search.htm?searchType=1&content=' + encodeURIComponent(text) +'&location='+$('#location').val()+'&brand='+$('#brand').val();
				if(isHuawei() == 'true'){
					var node = $("#categoryTree").tree('getSelected');
					var catgoryDirId = (node!=null&&node.id!=undefined)?node.id:'';
					url += "&catgoryDirId="+catgoryDirId;
				}
				TABOBJECT.open({
					id : $(this).attr('frameId'),
					name : '模糊搜索',
					title : text,
					hasClose : true,
					url : $.fn.getRootPath() + url,
					isRefresh : true
				}, this);
			}
		} else{
			$.fn.hint('请输入您要搜索的内容...');
		}
		return false;
	});
	
	//lvan.li 20160112
	/***********精准搜索按钮事件***********/
	$('a[id=exact]').attr('frameId', 'exact_s').click(function() {
		var text = $('div.content_ss>input.kuang').val();
		if(text) {
			if($.trim(text) == '请输入您要搜索的内容...') {
				$.fn.hint('请输入您要搜索的内容...');
			} else {
				var url = '/app/search/exact-search.htm?searchType=4&content=' + encodeURIComponent(text);
				if(isHuawei() == 'true'){//是否华为
					var node = $("#categoryTree").tree('getSelected');
					var id = (node!=null&&node.id!=undefined)?node.id:'';
					url += "&id="+id;
				}
				TABOBJECT.open({
					id : $(this).attr('frameId'),
					name : '精准搜索',
					title : text,
					hasClose : true,
					url : $.fn.getRootPath() + url,
					isRefresh : true
				}, this);
			}
		} else{
			$.fn.hint('请输入您要搜索的内容...');
		}
		return false;
	});
	
	/***********附件按钮事件***********/
	$('a[id=fujian]').attr('frameId', 'snior_s').click(function() {
		var text = $('div.content_ss>input.kuang').val();
		if(text) {
			if($.trim(text) == '请输入您要搜索的内容...') {
				$.fn.hint('请输入您要搜索的内容...');
			} else {
				TABOBJECT.open({
					id : $(this).attr('frameId'),
					name : '附件搜索',
					title : text,
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/senior-search.htm?searchType=3&content=' + encodeURIComponent(text),
					isRefresh : true
				}, this);
			}
		} else{
			$.fn.hint('请输入您要搜索的内容...');
		}
		return false;
	});
	
	/*** add by eko.zhan at 2015-09-23 09:40 ***/
	if (SystemKeys.userData.bannerCollapse==1){
		doBannerCollapse();
	}
} //end function Header

/**
 * 公告跑马灯效果
 * @author eko.zhan
 * @version 2015-02-13
 */
function marqueeLoop(){
//	$.post($.fn.getRootPath()+"/app/notice/notice!getPaoMaDeng.htm", {userid: SystemKeys.userId}, function(msg){
//		var marqueeText = "";
//		if (msg!=null && msg.length>0){
//			marqueeText = "<ul>";
//			$(msg).each(function(i, item){
//				marqueeText += "<li id=\"" + item.id + "\">" + item.title + "</li>";
//			});
//			marqueeText += "</ul>";
//		}
//		$("marquee").html(marqueeText);
//		
//		//公告单击事件绑定
//		$("marquee ul li").unbind("click");
//		$("marquee ul li").css("cursor", "pointer").bind("click", function(){
//			getNoticeContent($(this).attr("id"));
//		});
//	}, 'json');
	var _url = $.fn.getRootPath()+"/app/notice/notice!getPaoMaDeng.htm";
	//modify by eko.zhan at 2016-01-27 广东移动公告采用新版本
	//modify by eko.zhan at 2016-09-02 13:00 百联集团也启用新版本公告
	if (window.__kbs_enable_bulletin || window.__kbs_isBailian=='true'){
		_url = $.fn.getRootPath()+"/app/notice/bulletin!marquee.htm";
	}
	
	$.ajax({
   		type: "POST",
   		url: _url,
   		data: {userid: SystemKeys.userId},
   		dataType:'json',
   		async: true,
   		/* hidden by eko.zhan at 2015-12-24 11:45
   		beforeSend: function(request) {
   			request.setRequestHeader("Connection", "close");
   		},*/
   		success: function(msg){
   			var marqueeText = "";
   			if (msg!=null && msg.length>0){
   				marqueeText = "<ul>";
   				$(msg).each(function(i, item){
   					marqueeText += "<li id=\"" + item.id + "\">" + item.title + "</li>";
   				});
   				marqueeText += "</ul>";
   			}
   			$("marquee").html(marqueeText);
   			
   			//公告单击事件绑定
   			$("marquee ul li").unbind("click");
   			$("marquee ul li").css("cursor", "pointer").bind("click", function(){
   				getNoticeContent($(this).attr("id"));
   			});
   		},
   		error: function(){
   		}
	});
} //end function marqueeLoop
/**
 * 公告右下角弹窗
 * @author eko.zhan  
 * @version 2015-02-13 
 */
function noticeLoop(){
	var _url = $.fn.getRootPath()+"/app/notice/notice!getTanChuang.htm";
	//modify by heart.cao at 2016-02-01 广东移动公告采用新版本
	//modify by eko.zhan at 2016-09-02 13:00 百联集团也启用新版本公告
	if (window.__kbs_enable_bulletin || window.__kbs_isBailian=='true'){
		//_url = $.fn.getRootPath()+"/app/notice/bulletin!hasPopup.htm?" + new Date();
		_url = $.fn.getRootPath()+"/app/notice/bulletin!popup.htm?" + new Date();
	}
	$.ajax({ 
   		type: "POST",
   		url: _url,
   		data: {userid: SystemKeys.userId},
   		dataType:'json',
   		async: true,
   		success: function(msg){
   			if (msg!=null){  
   			    //modify by heart.cao at 2016-04-27 广东移动公告采用新版本(新版本弹窗采用列表形式)
				//modify by eko.zhan at 2016-09-02 13:00 百联集团也启用新版本公告
   			    if (window.categoryTag=='true' || window.__kbs_isBailian=='true'){
   			    	/*
   			        if(msg.result == '1'){
						// 用easyui_window替代IE弹窗
		        		$('#win').find('iframe').attr('src', $.fn.getRootPath() + '/popupbox.jsp');
						$('#win').window('open');
   			        }*/
   			    	//modify by eko.zhan at 2016-10-09 15:25
   			    	$('#popupPanel').find('tr').not(':first').remove();
   			    	if (msg.length>0){
   			    		$(msg).each(function(i, item){
   			    			$('#popupPanel').append('<tr _id="' + item.id + '" class="kbs-row"><td><a href="javascript:void(0)" title="'+item.name+'" onclick="doReadBulletin(\'' + item.id + '\')">' + item.title + '</a></td><td>' + item.edittime + '</td><td>' + item.createuser + '</td></tr>');
   			    		});
   			    		$('#popupPanel').append('<tr ><td colspan="3" align="right"><a href="javascript:void(0)" onclick="openInboxView();">更多&gt;&gt;</a></td></tr>');
   			    		$('#win').window('open');
   			    	}else{
   			    		//公告窗口一直打开的情况下，如果没有数据则关闭窗口
   			    		$('#win').window('close');
   			    	}
   			    }else{   
	   			    //修改窗口标题//根据类型 分为 知识库便签-1  知识库公告-0
	   				$('div.ggzs_dan_title b').html((($.trim(msg.isnote)=='1')?'知识库便签':'知识库公告'));
	   				//修改标题
	   				$('div.ggzs_dan_con b').html(msg.title);
	   				//修改内容
	   				$('div.ggzs_dan_con a').html((msg.htmlContent==undefined||msg.htmlContent=='')?msg.content:msg.htmlContent);
	   				//内容绑定事件
	   				$('div.ggzs_dan_con a').click(function(){
	   				    if($.trim(msg.isnote)=='1'){
	   				       getNoteContent(msg.id);
	   				    }else{
	   				       getNoticeContent(msg.id); 
	   				    }
	   					$('div.ggzs_dan').hide();
	   				});
	   				var _ind = $.layer({
	   					type: 1,
	   					title: false,
	   					area: ['auto', 'auto'],
	   					border: [0], //去掉默认边框
	   					shade: [0], //去掉遮罩
	   					closeBtn: [0, false], //去掉默认关闭按钮
	   					page: {dom : 'div.ggzs_dan'}
	   				});
	   				// 关闭
	   				$('div.ggzs_dan_title a').unbind("click");
	   				$('div.ggzs_dan_title a').bind("click", function(){
	   					$.post($.fn.getRootPath()+"/app/notice/notice!close.htm", {id:msg.id}, function(data){
	   						layer.close(_ind);
	   					});
	   				});			
   			    }   			
   			}
   		},
   		error: function(){
   		}
	});
} //end function noticeLoop

/*
* 打开收件箱
* add by heart.cao 2016-10-31
*/
function openInboxView(){
	var _diaWidth = screen.width-50;
	var _diaHeight = screen.height-100;
	var _scrWidth = screen.width;
	var _scrHegiht = screen.height;
	var _diaLeft = (_scrWidth-_diaWidth)/2;
	var _diaTop = (_scrHegiht-_diaHeight)/2;
	var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0, resizable=1, status=0, fullscreen=0';
	window.open($.fn.getRootPath() + '/app/notice/bulletin.htm?seltype=1', '_blank', params);
}

/* 打开公告 add by eko.zhan at 2016-10-09 17:00 */
function doReadBulletin(id){
	var _url = $.fn.getRootPath() + '/app/notice/bulletin!read.htm?id=' + id;
	$('#popupPanel').find('tr[_id="' + id + '"]').removeClass('kbs-row');
	window.open(_url,'_blank');
	//所有弹出公告已读后，弹出框自动关闭  add by heart.cao 2016-10-31
	if($('#popupPanel').find('.kbs-row').length==0){
	   $('#win').window('close');
	}
}

function getNoticeContent(id){
	//modify by eko.zhan at 2016-01-27 广东移动公告采用新版本
	if (window.categoryTag=='true' || window.__kbs_isBailian=='true'){
		TABOBJECT.open({
			id : 'notice',
			name : '公告管理',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/notice/bulletin!read.htm?id='+id,
			isRefresh : true
		}, this);
	}else{
		TABOBJECT.open({
			id : 'notice',
			name : '公告管理',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/notice/notice!notice.htm?id='+id+"&isNote=0",
			isRefresh : true
		}, this);
	}
}
function getNoteContent(id){
	TABOBJECT.open({
		id : 'note',
		name : '便签管理',
		hasClose : true,
		url : $.fn.getRootPath() + '/app/notice/notice!notice.htm?id='+id+"&isNote=1",
		isRefresh : true
	}, this);
}

function search(){
	Suggest.init($('input.kuang')[0]);
	
	/*
	//wukong搜索初始化 add by eko.zhan at 2016-04-23 12:57
	if ($('#btnWukongSearch').length==1){
    	$('input.kuang').width(519);
    	$('#btnWukongSearch').css({'left': 523});
    	
    	if (SystemKeys.userData.enableLocation=='false'){
    		$('.content_right_top').css({'float': 'left', 'margin-left': -30});
    	}else{
    		$('.content_right_top').css({'float': 'left', 'margin-left': 80});
    	}
    }else{
    	if (SystemKeys.userData.enableLocation=='false'){
    		$('.content_right_top').css({'float': 'left', 'margin-left': -30});
    	}else{
    		$('.content_right_top').css({'margin-left': 150});
    		$('.content_ss').width('500px');
    	}
    }
	*/
	
    //
    $('#btnWukongSearch').on('click', function(){
    	var keyword = $('input.kuang').val();
    	var url = $.fn.getRootPath() + '/app/wukong/search.htm?keyword=' + encodeURIComponent(keyword);
    	if (keyword=='请输入您要搜索的内容...'){
    		return false;
    	}
    	
    	if(isHuawei() == 'true'){
			var node = $("#categoryTree").tree('getSelected');
			var catgoryDirId = (node!=null&&node.id!=undefined)?node.id:'';
			url += "&categoryDirId="+catgoryDirId;
		}
    	TABOBJECT.open({
			id : 'wukong-search',
			name : '搜索详情',
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
    	
    	//modify by eko.zhan at 2016-9-07 18:59 没有 return false 会无限循环？？？原因待查
    	return false;
    });
    
//	$('input.kuang').val('请输入搜索内容...').css('color','#cccccc');
//	$('input.kuang').focus(function(){
//		if($(this).val()=='请输入搜索内容...'){
//			$(this).val('').css('color','');
//		}
//	});
//	$('input.kuang').blur(function(){
//		if($.trim($(this).val())==''){
//			$(this).val('请输入搜索内容...').css('color','#cccccc');
//		}
//	});
	$('input.kuang').next().click(function(){
		var askContent = $('input.kuang').val();
		normSearch(askContent);
	});
//	$("input.kuang").keypress(function (e) {
//		var askContent = $('input.kuang').val();
//		if(e.keyCode==13){
//			normSearch(askContent);
//		}
//	});
}

//一般搜索
function normSearch(askContent){
	//如果开启wukong搜索，则走新的搜索引擎 add by eko.zhan at 2016-04-23 12:55
	if ($('#btnWukongSearch').length==1){
		$('#btnWukongSearch').click();
		return false;
	}
	if($.trim(askContent)=='请输入您要搜索的内容...' || $.trim(askContent)==''){
		$(document).hint("请输入您要搜索的内容...");
		return false;
	}
	window.question = askContent;
	
	/***
	 * @authorlvan.li 20160509 搜索框和放大镜以及模糊搜事件索合并，当模糊按钮单独出现时回车键按照模糊搜索走，否则按照以前的逻辑走
	 * ***/
	if (($('#quanwen:visible').length == 1) && ($('.anniu').length == 0)){
		$('#quanwen').click();
	} else {
		//modify by eko.zhan at 2016-04-23 21:05 
		//categoryTag是字符串'true' 将搜索修改为模糊搜索 
		if(categoryTag=='true' || categoryTag==true){
			TABOBJECT.open({
				id : $(this).attr('frameId'),
				name : '模糊搜索',
				title : askContent,
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/senior-search.htm?searchType=1&content=' + encodeURIComponent(askContent) +'&location='+$('#location').val()+'&brand='+$('#brand').val(),
				isRefresh : true,
				forceOpen: true
			}, this);
		}else{
			//modify by eko.zhan at 2016-10-13 13:38 
			//搜索关键词每次打开的Tab都是独立的页面，部分客户提出质疑，希望每次搜索结果集是同一个页面，通过forceOpen控制
			var url = '/app/search/search.htm?isSearch=y&searchMode=1&askContent='+encodeURIComponent(askContent);
			if(isHuawei() == 'true'){
				var node = $("#categoryTree").tree('getSelected');
				var id = (node!=null&&node.id!=undefined)?node.id:'';
				url += "&id="+id;
			}
			TABOBJECT.open({
				id : 'ordinary_s',
				name : '搜索',
				title : askContent,
				hasClose : true,
				url : $.fn.getRootPath() + url,
				isRefresh : true
				//forceOpen: true
			}, this);
		}
	}
	return false;
}

function locationChange(){
	$('#location').change(function(){
		var tag = $(this).find("option:selected").val();
		var name = $(this).find("option:selected").text();
		$.ajax({
	   		type: "POST",
	   		url: $.fn.getRootPath()+"/main!location.htm",
	   		data: {tag:tag,name:name},
	   		dataType:'text',
	   		async: true,
	   		success: function(msg){
	   			//modify by eko.zhan at 2015-10-30
	   			treeRefresh();
	   			try{
	   				layer.msg(msg, 2, -1);
	   				//window.location.reload();
	   			}catch(e){
	   				alert(msg);
	   			}
	   		}
		});
		$('#s_location').val(tag);
		$('#dim_location').val(tag);
	});
}

function brandChange(){
	$('#brand').change(function(){
		var tag = $(this).find("option:selected").val();
		var name = $(this).find("option:selected").text();
		$.ajax({
	   		type: "POST",
	   		url: $.fn.getRootPath()+"/main!brand.htm",
	   		data: {tag:tag,name:name},
	   		dataType:'text',
	   		async: true,
	   		success: function(msg){
	   			//modify by eko.zhan at 2015-10-30
	   			treeRefresh();
	   			try{
	   				layer.msg(msg, 2, -1);
	   				//window.location.reload();
	   			}catch(e){
	   				alert(msg);
	   			}
	   		}
		});
		$('#s_brand').val(tag);
		$('#dim_brand').val(tag);
	});
}
/**
* 刷新维度信息（归属地、品牌）
* @author heart.cao
* @date 2016-06-17
**/
function refresh(){
	$('#refreshBtn').click(function(){
		var ngccObj = SMSUtils.getData();
		if(ngccObj){
		    //设置更新前台归属地信息
		    var _locationTag = (typeof(ngccObj.LOCATION_TAG)!='undefined')?ngccObj.LOCATION_TAG:'';
		    if($.trim(_locationTag)!=''){
				$('#s_location').val(_locationTag);
				$('#dim_location').val(_locationTag);
				$('#location').val(_locationTag);		    
		    }
		    //设置更新前台品牌信息
			var _brandTag = (typeof(ngccObj.BRAND_TAG)!='undefined')?ngccObj.BRAND_TAG:'';
			if($.trim(_brandTag)!=''){
				$('#s_brand').val(_brandTag);
				$('#dim_brand').val(_brandTag);
				$('#brand').val(_brandTag);
			}
			//设置更新SESSION中的归属地、品牌信息
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/main!refresh.htm?"+new Date(),
		   		data: {
		   		   locationtag: _locationTag,
		   		   locationname: ngccObj.LOCATION_NAME,
		   		   brandtag: _brandTag,
		   		   brandname: ngccObj.BRAND_NAME		   		   
		   		},
		   		dataType:'text',
		   		async: true,
		   		success: function(msg){
		   			treeRefresh();
		   			alert(msg.replace(/^\"|\"$/g,'')); 
		   			/*
		   			try{
		   				layer.msg(msg, 2, -1);
		   			}catch(e){
		   				alert(msg);
		   			}*/
		   		}
			});			
		}
	});
}

/**lvan  目录树刷新*/
function treeRefresh(){
	$("#categoryTree").tree("reload");
	try{
	   $("#smsCategoryTree").tree('options').url = $.fn.getRootPath() + '/app/sms/sms-ontology!list.htm';  
       $("#smsCategoryTree").tree("reload");
    }catch(e){}
	try{
	   //console.log("====" + SMSTreePlugin);
	   if(SMSTreePlugin)SMSTreePlugin.clearSMSBox();
    }catch(e){}    
}

/**
* 判断是否需要修改用户密码，如果需要，弹出密码修改对话框
* add by heart.cao 2015-08-06
*/
function modifyUserPwd(){
    if(SystemKeys.force_change=="1" 
         && "202cb962ac59075b964b07152d234b70"==$.trim(SystemKeys.userPwd)){
         jQuery.layer({
			type: 2,
			border: [10, 0.3, '#000'],
			title: false,
			closeBtn: false,
			iframe: {src : $.fn.getRootPath() + '/app/auther/user!pwdEditTo.htm?mtype=1'},
			area: ['550px', '350px']
		});
    }
}

function backTop(){
	/************滚动条监听*******************/
	$(window).bind("scroll", function(){
		var height=document.documentElement.scrollHeight - document.documentElement.clientHeight;
		if($(window).scrollTop()>100 && height>170){
//			$("#header").addClass("search-fixed");
			
			$("#backTop").click(function(){
			  $(window).scrollTop("");
			});
			$("#backTop").show();
			
		}else if($(window).scrollTop()<100){
//			$("#header").removeClass("search-fixed");
			$("#backTop").hide();
		}
	}); 
}

/**
 * @author eko.zhan
 * @since 2015-09-23 09:40
 * Banner 栏隐藏功能
 * @modify by eko.zhan at 2015-10-09 13:45 固定banner栏和左侧导航栏，只滚动主面板
 */
function doBannerCollapse(){
	$('body').dblclick(function(){
		$('.header').hide();
		$('.content_left').css({'margin-top': '8px'});
		$('.content_right_top').css({'margin-top': '10px'});
		$('#kbs-bookmark').removeClass('kbs-bookmark-collapse').addClass('kbs-bookmark');
	});
	
	$('body').append('<div id="kbs-bookmark"></div>');
	$('.content_left').css({'padding-top': '0px'});
	
	$('#kbs-bookmark').click(function(){
		var l = $('ul[class="juli"]').children('li').length-1;
		if ($('.header:hidden').length==1){
			//展开banner
			$('.content_left').css({'margin-top': '73px'});
			$('.content_right_top').css({'margin-top': '80px'});
			$('.header').show();
			$(this).removeClass('kbs-bookmark').addClass('kbs-bookmark-collapse');
			
			//知识分类（高度调整）
			$('#categoryTree').css({height: $(window).height()-105-l*30-65});
			
			//如果启用 easyui tab 切换分隔栏要调整tab高度
			if ($('#tabs').length==1){
				var _tabHeight = $('#tabs').height() - 73;
				parent.pageContext.resizeTab(null, _tabHeight);
			}
		}else{
			//折叠banner
			$('body').dblclick();
			
			//知识分类（高度调整）
			$('#categoryTree').css({height: $(window).height()-105-l*30});
			
			//如果启用 easyui tab 切换分隔栏要调整tab高度
			if ($('#tabs').length==1){
				var _tabHeight = $('#tabs').height() + 73;
				parent.pageContext.resizeTab(null, _tabHeight);
			}
		}
	});
	
	//使书签按钮一直靠近页面左边对齐 update by wilson.li at 2015-11-25 下午一点
	$(window).scroll(function () {
        var _left = $(window).scrollLeft();
        $('#kbs-bookmark').animate({right:-_left},0);
    });
	
	$('body').dblclick();
}