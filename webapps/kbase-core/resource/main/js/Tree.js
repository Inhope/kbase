//add by eko.zhan at 2015-08-21 10:35
//在低版本浏览器中（IE6/7/8）树结构展开动画效果影响卡顿，在这里修改之
var ua = navigator.userAgent.toLowerCase();
var zTreeExpandSpeed = 'fast';
if (/msie/.test(ua)){
	zTreeExpandSpeed = '';
}

var setting = {
	view: {
			//expandSpeed: "fast"//空不显示动画
			expandSpeed: zTreeExpandSpeed //空不显示动画
	},
	async : {
		enable : true,
		url : $.fn.getRootPath()+"/app/main/ontology-category!list.htm",
		autoParam : ["id", "name=n", "level=lv","bh"],
		otherParam : {},
		dataFilter: ajaxDataFilter
	},
	callback : {
		onClick : onClick,
		onRightClick: onRightClick
	}
	
};

function ajaxDataFilter(treeId, parentNode, responseData) {
    if (responseData) {
      for(var i =0; i < responseData.length; i++) {
      	if(responseData[i].isParent=="false")
        	responseData[i].icon = $.fn.getRootPath() + '/theme/' + window.USER_THEME + '/resource/main/images/left_ico5.jpg';
      }
    }
    return responseData;
};
function onClick(event, treeId, treeNode, clickFlag) {
	
	if(!treeNode.isParent && categoryTag=='true'){
		var url = $.fn.getRootPath() + '/app/category/category.htm?type=1&categoryId='+treeNode.id+"&categoryName="+treeNode.name;
		TABOBJECT.open({
			id : treeNode.id,
			name : treeNode.name,
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	}else{
		var url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id='+treeNode.id;
		TABOBJECT.open({
			id : treeId,
			name : treeNode.name,
			//name : '分类展示',
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	}
}

/**********************按id展开节点********************************/
function expandNodeById(id){
	//hidden by eko.zhan at 2015-09-24 12:52
	return false;
	var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
	var	cNode = treeObj.getNodeByParam("id", id);
	treeObj.expandNode(cNode, true);
	
}
/** ***************按pid,id选中某个节点*********************** */
function selectNodeById(pid,id) {
	//hidden by eko.zhan at 2015-09-24 12:52
	return false;
	expandNodeById(pid);
	
	
	var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
	var cNode = treeObj.getNodeByParam("id", id);
	var t = window.setInterval(function(){
		if(cNode){
			treeObj.selectNode(cNode);
			window.clearInterval(t);
		}else{
			cNode = treeObj.getNodeByParam("id", id);
		}
	},300);
	
}
/******************按bh选中节点**************************/
function selectNodeByBh(bh) {
	var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
	var	cNode = treeObj.getNodeByParam("bh", bh);
	treeObj.selectNode(cNode);
	$("#treeInput").focus();
}
/**********************按路径展开并选中最后一个节点***************************************/
var searchIndex=0;
var searchBh="start";
function expandPath(nodes){
	var	cNode;
	setTimeout(function(){
		if(searchBh.length < nodes.length-6){
			var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
			//如果树没有加载完成 等0.3秒后再重新执行expandPath
			if(treeObj.getNodes().length<1){
				expandPath(nodes);
			}else{
				searchBh=nodes.substring(0,9+searchIndex*5);
				cNode = treeObj.getNodeByParam("bh", searchBh);
				treeObj.expandNode(cNode, true);
				timeout();
			}
		}else{
			selectNodeByBh(nodes);
		}
	},300);
	
	function timeout(){
		setTimeout(function(){
			if(!cNode.open){
				timeout();
			}else{
				searchIndex++;
				expandPath(nodes);
			}
		},10);
	}
}


/************右击事件******************/
function onRightClick(event, treeId, treeNode) {
	var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
	if (treeNode && !treeNode.noR) {
		treeObj.selectNode(treeNode);
		showRMenu("node", event.clientX, event.clientY);
	}
}
/*****************显示右键菜单************************/
function showRMenu(type, x, y) {
	rMenu = $("#treeMenu");
	rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
	$("#treeMenu ul").show();
	$("body").bind("mousedown", onBodyMouseDown);

}
/*****************隐藏右键菜单************************/
function hideRMenu() {
	rMenu = $("#treeMenu");
	if (rMenu) rMenu.css({"visibility": "hidden"});
	$("body").unbind("mousedown", onBodyMouseDown);
}
/**************点击其他位置隐藏右键菜单************************/
function onBodyMouseDown(event){
	rMenu = $("#treeMenu");
	if (!(event.target.id == "rMenu" || $(event.target).parents("#treeMenu").length>0)) {
		rMenu.css({"visibility" : "hidden"});
	}
}
/**************新开一个窗口******************/
function newPanel(){
	hideRMenu();
	var treeObj = $.fn.zTree.getZTreeObj("categoryTree");
	var cnode = treeObj.getSelectedNodes();
	alert(cnode[0].text);
	TABOBJECT.open({
			//id : new Date().getTime(),
			//name : '实例列表',
			//name : '分类展示',
			id : cnode[0].id,
			name : cnode[0].text,
			hasClose : true,
			url : $.fn.getRootPath() + '/app/object/ontology-object.htm?rightClick=true&id='+cnode[0].id,
			isRefresh : true
		}, this);
}



	/************************点击搜索******************************/
function treeSearch(){
	
	function search() {
		var text=$.trim($("#treeInput").val());
		if(text=="请输入搜索内容..." || text==""){
			$(document).hint("请输入搜索内容...");
			return;
		}
		
		/****************访问后台搜索***************************/
		searchIndex=0;
		searchBh="start";
		$.ajax({
	   		type: "POST",
	   		url: $.fn.getRootPath()+"/app/main/ontology-category!search.htm",
	   		data: {name:text},
	   		async: true,
	   		success: function(msg){
	   			if(msg=="null"){
	   				$(document).hint("没有查找到\""+text+"\"相关分类");
	   				return;
	   			}
	   			msg=msg.replace("\"","");
	   			msg=msg.replace("\"","");
	   			expandPath(msg);
	   		}
		});
	}
	
	//$("#treeButton").click(function (){search();return false;});
	$("#treeInput").focus(function (e) {
		if($.trim($(this).val())=="请输入搜索内容..."){
			$(this).val("");
		}
	});
	$("#treeInput").blur(function (e) {
		if($.trim($(this).val())==""){
			$(this).val("请输入搜索内容...");
		}
	});
	
	$("#treeInput").keypress(function (e) {
		if(e.keyCode==13){
			search();
		}
	});
}
