/**
 * @modify by eko.zhan at 2015-01-31
 * 修改链接信息新增和编辑界面，修改该视图界面样式
 */
$(function(){
	var option = {
		addBtn : $('#btnAdd'),
		editBtn : $('#btnEdit'),
		removeBtn : $('#btnDel'),
		checkBox : {
			checkAllBox : $('#selAll'),
			childCheckBox : $("input[xname='childCheckBox']")
		},
		//添加和修改面板
		linkPanel : {
			panel : $('div.f-div'),
			closeBtn : $('div.f-div a'),
			commitBtn : $('div.f-div button'),
			linkName : $('div.link_name input'),
			linkUrl : $('div.link_url input'),
			panelTitle : $('div.f-div-t b')
		},
		showPanel : function(){
			//剧中显示
			this.linkPanel.panel.css({
				left : ($(document).width() - this.linkPanel.panel.width()) / 2,
				top : 100
			}).show();
			//连接名获得焦点
			this.linkPanel.linkName.focus();
		},
		closePanel : function(){
			$('body').hideShade();
			this.linkPanel.panel.hide();
			//清空
			this.linkPanel.linkName.val('');
			this.linkPanel.linkUrl.val('');
			this.linkPanel.commitBtn.attr('id', '');
		},
		init : function(){
			var self = this;
			self.checkBox.childCheckBox.attr('checked', false);
			self.checkBox.checkAllBox.attr('checked', false).click(function(){
				var isCheck = $(this).attr('checked');
				if(isCheck) {
					self.checkBox.childCheckBox.attr('checked', true);
				} else {
					self.checkBox.childCheckBox.attr('checked', false);
				}
			});
			//打开添加面板
			/*
			this.addBtn.click(function(){
				//显示蒙版
				$('body').showShade();
				self.linkPanel.panelTitle.text('添加链接');
				self.showPanel();
			});*/
			//打开修改面板/
			/*
			this.editBtn.click(function(){
				var checked = $("input[xname='childCheckBox']:checked");
				if(checked.size() == 0) {
					parent.layer.alert('请选择一行进行修改', -1);
				} else if(checked.size() > 1) {
					parent.layer.alert('只能选择一行进行修改', -1);
				} else {
					//显示蒙版
					$('body').showShade();
					self.linkPanel.panelTitle.text('修改链接');
					self.linkPanel.linkName.val($.trim(checked.parent().next().find('a').text()));
					self.linkPanel.linkUrl.val($.trim(checked.parent().next().next().find('a').text()));
					self.linkPanel.commitBtn.attr('id', checked.attr('id'));
					self.showPanel();
				}
			});*/
			//关闭面板
			this.linkPanel.closeBtn.click(function() {self.closePanel();});
			//添加和保存按钮
			this.linkPanel.commitBtn.click(function(){
				var linkName = self.linkPanel.linkName.val();
				var linkUrl = self.linkPanel.linkUrl.val();
				if(!linkName){
					parent.layer.alert('请填写链接名!', -1);
					self.linkPanel.linkName.focus();
					return;
				}else if(!linkUrl){
					parent.layer.alert('请填写Url!', -1);
					self.linkPanel.linkUrl.focus();
					return;
				}else if (linkUrl.indexOf('http')==-1){
					parent.layer.alert('Url必须以http开头', -1);
					self.linkPanel.linkUrl.select();
					return false;
				}
				$('body').ajaxLoading('正在保存数据...');
				$.ajax({
					type : 'POST',
					url : $.fn.getRootPath() + '/app/link/quik-link!addOrEditLink.htm',
					timeout : 5000,
					dataType : 'json',
					data : {
						linkId : self.linkPanel.commitBtn.attr('id'),
					    linkName : $.trim(linkName),
					    linkUrl : $.trim(linkUrl)
					},
					success : function(data) {
						if(data.success) {
							window.location.reload();
						} else {
							parent.layer.alert(data.message, -1)
						}
						self.closePanel();
						$('body').ajaxLoadEnd();
					},
					error : function(data) {
						$('body').ajaxLoadEnd();
						parent.layer.alert('请求异常，请检查网络!', -1);
						self.closePanel();
					}
				});
			});
			this.removeBtn.click(function(){
				var checked = $("input[xname='childCheckBox']:checked");
				if(checked.size() > 0) {
					parent.layer.confirm('确定要删除该链接吗？', function(index){
						parent.layer.close(index);
						var ids = '';
						for(var i = 0; i < checked.size(); i ++) {
							if(i == (checked.size() - 1)) 
								ids += $(checked[i]).attr('id');
							else 
								ids += $(checked[i]).attr('id') + ',';
						}
						$('body').ajaxLoading('正在删除数据...');
						$.ajax({
							type : 'POST',
							url : $.fn.getRootPath() + '/app/link/quik-link!removeLink.htm',
							timeout : 5000,
							dataType : 'json',
							data : {
								ids : ids
							},
							success : function(data) {
								$('body').ajaxLoadEnd();
								if(data.success) {
									window.location.reload();
								} else {
									parent.layer.alert(data.message, -1)
								}
							},
							error : function(data) {
								parent.layer.alert('请求异常，请检查网络!', -1);
								$('body').ajaxLoadEnd();
							}
						});
					});
				}else{
					parent.layer.alert('请选择要删除的链接', -1);
				}
			});
		}
	}
	option.init();
	
	//新建链接， 编辑链接
	$("#btnAdd,#btnEdit").click(function(){
		var _elemId = $(this).attr("id");
		var _checkId = "";
		var _editLinkName = "";
		var _editLinkUrl = "http://";
		if (_elemId=="btnEdit"){
			var checked = $("input[xname='childCheckBox']:checked");
			if(checked.size()==0) {
				layer.alert('请选择一行进行修改', -1);
				return false;
			} else if(checked.size()>1) {
				layer.alert('只能选择一行进行修改', -1);
				return false;
			}
			_checkId = checked.attr("id");
			_editLinkName = checked.attr("xlinkname");
			_editLinkUrl = checked.attr("xlinkurl");
		}
		
		var _tableStyle = "font-size:12px;table-layout:fixed;margin:10px;border:1px solid #eee;";
		var _tdStyle = "border:1px dotted #eee;";
		var _inputStyle = "border:1px solid #eee;height:25px;line-height:25px;width:280px;";
		var _btnStyle = "padding-left:5px;padding-right:5px;padding-top:3px;padding-bottom:3px;border:1px solid #7E7E7E;background-color:#eee;cursor:pointer;";
		var _diaHtml = [
							"<input type=\"hidden\" id=\"linkId\" value=\""+_checkId+"\">",
							"<table width=\"420\" height=\"110\" cellspacing=\"0\" cellpadding=\"2\" style=\""+_tableStyle+"\">",
							"<tr>",
							"<td width=\"25%\" style=\""+_tdStyle+"\"><b>链接名称</b></td>",
							"<td width=\"75%\" style=\""+_tdStyle+"\"><input type=\"text\" id=\"linkName\" style=\""+_inputStyle+"\" value=\""+_editLinkName+"\"/></td>",
							"</tr>",
							"<tr>",
							"<td style=\""+_tdStyle+"\"><b>链接地址</b></td>",
							"<td style=\""+_tdStyle+"\"><input type=\"text\" id=\"linkUrl\" style=\""+_inputStyle+"\" value=\""+_editLinkUrl+"\"/></td>",
							"</tr>",
							"<tr>",
							"<td colspan=\"2\" style=\""+_tdStyle+"\" align=\"center\"><input type=\"button\" id=\"btnSave\" value=\"提交\" style=\""+_btnStyle+"\"/></td>",
							"</tr>",
							"</table>"
						].join(" ");
		var pageii = $.layer({
		    type: 1,
		    title: false,
		    area: ['auto', 'auto'],
		    border: [10, 0.3, '#000'],
		    closeBtn: [0, true],
		    shift: 'left', //从左动画弹出
		    page: {
		        html: _diaHtml
		    }
		});
		
		$("#btnSave").unbind("click");
		$("#btnSave").bind("click", function(){
			var linkId = $("#linkId").val();
			var linkName = $("#linkName").val();
			var linkUrl = $("#linkUrl").val();
			if (linkName==""){
				layer.alert('请填写链接名!', -1);
				return false;
			}else if(linkUrl==""){
				layer.alert('请填写链接地址!', -1);
				return false;
			}else if (linkUrl.indexOf('http')==-1){
				layer.alert('链接地址必须以http开头', -1);
				return false;
			}
			$('body').ajaxLoading('正在保存数据...');
			$.ajax({
				type : 'POST',
				url : $.fn.getRootPath() + '/app/link/quik-link!addOrEditLink.htm',
				dataType : 'json',
				data : {
					linkId : $.trim(linkId),
				    linkName : $.trim(linkName),
				    linkUrl : $.trim(linkUrl)
				},
				success : function(data) {
					if(data.success) {
						window.location.reload();
					} else {
						layer.alert(data.message, -1)
					}
					$('body').ajaxLoadEnd();
					layer.close(pageii);
				},
				error : function(data) {
					$('body').ajaxLoadEnd();
					parent.layer.alert('请求异常，请检查网络!', -1);
					self.closePanel();
				}
			});
		}); //end btnSave
	}); //end btnAdd/btnEdit
});