$(function() {
	
	$('div.shezhi_con_nr input[type=checkbox]').click(function() {
		var checked = $('div.shezhi_con_nr input[type=checkbox]:checked');
		if(checked.size() > 4) {
			$(this).attr('checked', false);
			alert('最多只能选择4个面板在首页显示')
		}
	});
	
	//保存
	$('input[value=保存]').click(function() {
		var checked = $('div.shezhi_con_nr input[type=checkbox]:checked');
		var nochecked = $('div.shezhi_con_nr input[type=checkbox]').not(':checked');
		var theme = $('div.shezhi_con_nr input[type=radio]:checked');
//		if(checked.size() == 0) {
//			alert('至少选择一个面板');
//			return;
//		}
		
		if($('#sortable1 li').length == 0) {
			parent.parent.$(document).hint('左边最少显示一个面板');
			return;
		}
//		parent.$(document).ajaxLoading('数据保存中...');
		var userIndexPanelIds = '';
		var orders = '';
		var userRightPanleIds = '';
		var rightOrders = '';
		
//		$.each(checked, function(i) {
//			userIndexPanelIds += $(checked[i]).attr('id') + ',';
//			orders += $('option:selected', $(checked[i]).next().next()).text() + ',';
//		});
//		var userRightPanleIds = '';
//		var rightOrders = '';
//		$.each(nochecked,function(i){
//			userRightPanleIds += $(nochecked[i]).attr('id') + ',';
//			rightOrders += $('option:selected', $(nochecked[i]).next().next()).text() + ',';
//		});
//		
//		userIndexPanelIds = userIndexPanelIds.substring(0, userIndexPanelIds.length - 1);
//		orders = orders.substring(0, orders.length - 1);
//		
//		userRightPanleIds = userRightPanleIds.substring(0, userRightPanleIds.length - 1);
//		rightOrders = rightOrders.substring(0, rightOrders.length - 1);
		
		$('#sortable1 li').each(function(index){
			userIndexPanelIds += $(this).attr('panelId') + ',';
			orders += index +',';
		});
		$('#sortable2 li').each(function(index){
			userRightPanleIds += $(this).attr('panelId') + ',';
			rightOrders += index +',';
		});
		
		userRightPanleIds = userRightPanleIds.substring(0, userRightPanleIds.length - 1);
		rightOrders = rightOrders.substring(0, rightOrders.length - 1);
		
		$.ajax({
			url : $.fn.getRootPath() + '/app/auther/index-setting!saveOrEdit.htm',
			type : 'POST',
			timeout : 5000,
			dataType : 'json',
			//url的参数
			data : {
				userIndexPanelIds : userIndexPanelIds,
				orders : orders,
				
				userRightPanleIds : userRightPanleIds,
				rightOrders : rightOrders,
				
				theme : theme.attr('theme_value')
			},
			success : function(data, textStatus, jqXHR) {
				parent.parent.$(document).ajaxLoadEnd();
//				alert(data.message);
//				if(confirm('是否需要重新登陆进行界面渲染?')) {
//					parent.location.href = $.fn.getRootPath() + '/app/login/logout.htm';
//				}
				parent.parent.$(document).hint(data.message+'<br/>下次登陆生效');
			},
			error : function() {
//				alert('请求超时');
				parent.parent.$(document).hint('请求超时');
				parent.$(document).ajaxLoadEnd();
			}
		});
	});
	
	
	///拖动排序
	$( "#sortable1 li").each(function(index){
		if(index==0){
			$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/1.jpg")');
		}else if(index==1){
			$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/2.jpg")');
		}else if(index==2){
			$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/3.jpg")');
		}else if(index==3){
			$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/4.jpg")');
		}
	});
	$( "#sortable1, #sortable2" ).sortable({
		connectWith : ".connectedSortable",
		update : function(){
			$( "#sortable1 li").each(function(index){
				if(index==0){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/1.jpg")');
				}else if(index==1){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/2.jpg")');
				}else if(index==2){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/3.jpg")');
				}else if(index==3){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/4.jpg")');
				}
			});
			$( "#sortable2 li").each(function(index){
					$(this).css('background-image','');
			});
		}
	}).disableSelection();
	$( "#sortable2" ).sortable({
		stop : function(){
			if($('#sortable1 li').length > 4){
				$(this).sortable('cancel');
				parent.parent.$(document).hint('左边最多显示4个面板');
			}
			
			$( "#sortable1 li").each(function(index){
				if(index==0){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/1.jpg")');
				}else if(index==1){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/2.jpg")');
				}else if(index==2){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/3.jpg")');
				}else if(index==3){
					$(this).css('background-image','url("'+$.fn.getRootPath()+'/theme/red/resource/auther/images/shezhi/4.jpg")');
				}
			});
			$( "#sortable2 li").each(function(index){
					$(this).css('background-image','');
			});			
		}
	});
	
});