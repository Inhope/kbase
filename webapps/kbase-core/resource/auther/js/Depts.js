/**
 * 部门管理
 * @author Gassol.Bi
 * @modified 2016-1-8 11:41:21
 */
 $(function(){
 	/*去掉前后空格*/
 	String.prototype.trim = function() {return this.replace(/(^\s*)|(\s*$)/g,'');}
 	
	/*关闭编辑区*/
	function closePanel (){
		$('.xinxi_con_nr_right').css("display", "none");/*影藏所有编辑区*/
	}
	/*显示编辑区*/
	function openPanel (i){
		$('.xinxi_con_nr_right').css("display", "none");/*影藏所有编辑区*/
		$("#content" + i).css("display", "");/*显示指定编辑区*/
	}
 	/*左侧部门树*/
 	var Tree0 = {
 		treeId: 'treeDemo',
 		setting: {
 			view: {
				dblClickExpand: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async: {
				enable: true,
				url: $.fn.getRootPath() + '/app/auther/dept!asyncData.htm',
				autoParam:['id', 'type']
			},
			callback: {
				/*左击事件*/
				onClick: function(event, treeId, treeNode){
					if(treeNode.type=="station"){
						StationUtils.addOrEdit('edit');
					}else{
						DeptUtils.addOrEdit('edit');
					}
				},
				/*左击事件(前)*/
				beforeClick: function (treeId, treeNode, clickFlag){
					/*顶级部门不存在，不能编辑*/
					return (treeNode.id != '0');
				},
				/*右击事件*/
				onRightClick: function(event, treeId, treeNode){
					if(treeNode.id == '0'){/*顶级部门不存在，不能创建岗位	*/							
						$("#addStation").css("display","none");
						$("#deptImport").css("display","block");
					}else{
						$("#deptImport").css("display","none");
						$("#addStation").css("display","block");											
					}
					/*影藏主区*/
					$('.xinxi_con_nr_right').css("display","none");
								
					var treeObj = $.fn.zTree.getZTreeObj(treeId);
					if (treeNode && !treeNode.noR) {
						treeObj.selectNode(treeNode);
						var cityObj = $("#"+treeNode.tId);		
						var cityOffset = $(cityObj).offset();
						Tree0.showRMenu(cityOffset.left+$("#"+treeNode.tId+"_span").outerWidth()*2-2, 
							cityOffset.top + $("#"+treeNode.tId+"_span").outerHeight());
					}
				},
				/*右击事件(前)*/
				beforeRightClick: function (treeId, treeNode){
					/*岗位不能创建部门，岗位不存在上下级关系*/
					return treeNode != null && (treeNode.type!="station");
				}
			}
 		},
 		/*显示右键菜单*/
 		showRMenu: function(x, y) {
			var rMenu = $("#treeMenu");	
			rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
			$("#treeMenu ul").show();
			$("body").bind("mousedown", this.onBodyMouseDown);
		},
		/*隐藏右键菜单*/
		hideRMenu: function() {
			var rMenu = $("#treeMenu");
			if (rMenu) rMenu.css({"visibility": "hidden"});
			$("body").unbind("mousedown", this.onBodyMouseDown);
		},
		/*点击其他位置隐藏右键菜单*/
		onBodyMouseDown: function(event){
			var rMenu = $("#treeMenu");
			if (!(event.target.id == "rMenu" || 
				$(event.target).parents("#treeMenu").length>0)) {
					rMenu.css({"visibility" : "hidden"});
			}
		},
		init: function(){
			return $.fn.zTree.init($('#' + this.treeId), this.setting);
		},
		getZTreeObj: function(){
			return $.fn.zTree.getZTreeObj(this.treeId);
		},
		refresh: function(nodeId){
			var treeObj = this.getZTreeObj(),
				node = treeObj.getNodeByParam("id", nodeId, null);
			treeObj.reAsyncChildNodes(node, "refresh");
		}
 	}
 	/*右侧菜单树(编辑、新增)*/
 	var Tree1 = {
 		treeId: 'treeDemo1',
 		setting: {
			check: {
				enable: true,
				chkStyle: "radio",
				radioType: "all"
			},
			view: {
				dblClickExpand: false
			},
			async: {
				enable: true,
				url: $.fn.getRootPath() + "/app/auther/dept!asyncData.htm",
				autoParam:["id"],
				dataFilter: function(treeId, parentNode, childNodes) {
					/*节点数据过滤(岗位不能作为父节点(Tree1应去掉岗位节点))*/
					if(childNodes) Tree1.dataFilter(childNodes);
				    return childNodes;
				}
			},
			callback: {
				/*所属部门选择*/
				onCheck: function(e, treeId, treeNode) {
					$("#name_pdept").attr("value", treeNode.name);
					$("#id_pdept").attr("value",  treeNode.id);
				},
				/*所属部门选择（前）*/
				beforeCheck: function (treeId, treeNode){
					var node = Tree0.getZTreeObj().getSelectedNodes()[0];
					/*岗位不能作为父节点(Tree1应去掉岗位节点)*/
					if(treeNode.type == 'station'){
						parent.layer.alert("岗位不能作为父节点!", -1);
						return false;
					}
					/*验证选择的节点是不是当前选中节点本身*/
					if(node.id == treeNode.id){
						parent.layer.alert("父部门不能为当前部门本身!", -1);
						return false;
					}
					while(treeNode.getParentNode() != null){
						/*验证选择的节点是不是当前选中节点下级*/
						treeNode = treeNode.getParentNode();
						if(node.id==treeNode.id){
							parent.layer.alert("父部门不能为当前部门的子部门!", -1);
							return false;
						}
					}
					return true;
				}			
			}
		},
		/*所属部门下拉菜单*/
		showMenu: function () {
			var cityObj = $("#name_pdept");
			var cityOffset = $("#name_pdept").offset();
			$("#menuContent1").css({left:cityOffset.left + "px", 
				top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
			
			$("body").bind("mousedown", this.onBodyDown);
		},
		/*所属部门下拉影藏*/
		hideMenu: function () {
			$("#menuContent1").fadeOut("fast");
			$("body").unbind("mousedown", this.onBodyDown);
		},
		/*触发所属部门下拉影藏*/
		onBodyDown: function (event) {
			if (!(event.target.id == "menuContent1" || 
				$(event.target).parents("#menuContent1").length>0)) {
					Tree1.hideMenu();
			}
		},
		/*节点数据过滤(岗位不能作为父节点(Tree1应去掉岗位节点))*/
		dataFilter: function(nodes){
			/*岗位不能作为父节点(Tree1应去掉岗位节点)*/
			for(var i=0; i<nodes.length; i++){
				if(nodes[i].type == 'station') {
					nodes.splice(i, 1); i--;
				}
			}
			return nodes;
		},
		/*获取Tree1*/
		getZTreeObj: function(nodes){
			return $.fn.zTree.init($('#' + this.treeId), this.setting, this.dataFilter(nodes));
		}
 	}
 	/*部门方法*/
 	var DeptUtils = {
 		/*部门编辑、新建*/
		addOrEdit: function (status){
			var _that = this;
			
			//显示编辑区
			openPanel(0);
			
			var treeObj = Tree0.getZTreeObj();
			var node = treeObj.getSelectedNodes()[0];//当前选中节点				
			if(node==null||node==''){					
				parent.layer.alert("请先选择节点！", -1);
				return;
			}
			Tree0.refresh(node.id);//刷新树(防止子节点重名)
			
			//信息初始化
			_that.reset(status, node, treeObj);
			
			//绑定信息重置
			$("#resetDept").unbind("click");
			$("#resetDept").bind("click",function (){
				_that.reset(status, node, treeObj);
			});
			//绑定保存
			$("#saveDept").unbind("click");
			$("#saveDept").bind("click",function (){
				_that.save(status, treeObj);
			});
			//绑定删除
			$("#delDept").unbind("click");
			$("#delDept").bind("click",function (){
				_that.del(node, treeObj);
			});
		},
 		/*部门重置*/
 		reset: function (status, node, treeObj){
			$("input[id$='_dept']").each(function (){
				$(this).attr("value","");
			});				
			$("textarea[id$='_dept']").each(function (){
				$(this).attr("value","");
			});			
			$("#name_pdept").unbind("click");
			
			if(status=="edit"){
				//编辑赋值
				if(node.pId!=null){
					//部门名称被修改后，子部门的父部门显示的还是以前的名称
					var pNode = treeObj.getNodeByParam("id",node.pId,null);
					$("#id_pdept").attr("value",pNode.id);	
					$("#name_pdept").attr("value",pNode.name);
					//编辑页面部门岗位树初始化
					treeObj = Tree1.getZTreeObj(treeObj.getNodes());
					//获取岗位节点
					var nodes = treeObj.getNodesByFilter(function filter(treeNode){
						if(treeNode.type=="station") return true;
					});
					//删掉岗位节点
					for(var i=0;i<nodes.length;i++){
						treeObj.removeNode(nodes[i]);
					}
					//回显所属部门
					treeObj.checkNode(treeObj.getNodeByParam("id",node.pId, null),true,false);
				}else{
					$("#id_pdept").attr("value","");
					$("#name_pdept").attr("value","");
				}
								
				//绑定点击事件
				$("#name_pdept").click(function (){ Tree1.showMenu();});	
				$("#id_dept").attr("value",node.id!=null?node.id:'');
				$("#name_dept").attr("value",node.name!=null?node.name:'');
				$("#descContent_dept").attr("value",node.descContent!=null?node.descContent:'');						
				
				//标题显示
				$("#title0").html("<b>部门编辑</b>");
				//显示删除按钮
				$("delDept").css("display","block");
			} else {
				//新建赋值
				$("#id_pdept").attr("value",node.id!=null?node.id:'');
				$("#name_pdept").attr("value",node.name!=null?node.name:'');
				
				//标题显示
				$("#title0").html("<b>部门新增</b>");	
				//隐藏删除按钮
				$("delDept").css("display","none");
			}
		},
		/*部门保存*/
		save: function (status, treeObj){
			var id_dept = $("#id_dept").val();
			var name_dept = $("#name_dept").val().trim();
			var id_pdept = $("#id_pdept").val(), /*新的父部门*/
				id_pdept_ ;/*旧的父部门*/
			if(id_dept) id_pdept_ = 
				treeObj.getNodeByParam('id', id_dept, null).pId;
			
			//判断同级部门是否有重复的部门
			if(name_dept != ''){
				var node_p = treeObj.getNodeByParam("id", id_pdept, null);
				var node_ = treeObj.getNodesByFilter(function(node){
					if(node.pId == id_pdept && node.name == name_dept 
						&& node.type == 'dept') return true;
					return false;
				}, true, node_p);
				//同名部门与当前需要编辑的部门不是同一个
				if(node_ != null && node_.id != id_dept){
					parent.layer.alert("同级部门下不应存在相同部门!", -1);
					return false;
				}
			}else{
				parent.layer.alert("部门名称不应为空!", -1);
				return false;
			}
			//保存按钮不可用
			document.getElementById("saveDept").disabled = true;
			$.post($.fn.getRootPath()+"/app/auther/dept!addOrEditDept.htm",
				{
					'id_dept':id_dept,
					'name_dept':name_dept,
					'id_pdept': id_pdept != '0' ? id_pdept : '',
					'name_pdept':$("#name_pdept").val(),
					'descContent_dept':$("#descContent_dept").val()
				},
				function(jsonResult){
					parent.layer.alert(jsonResult.msg, -1);
					if(jsonResult.rst){
						if(id_pdept_ && id_pdept_ != id_pdept) {/*编辑-父节点发生改变*/
							/*移除原始节点*/
							treeObj.removeNode(treeObj.getNodeByParam('id', id_dept, null));
						}
						Tree0.refresh(id_pdept);/*刷新目标父节点*/
						closePanel();/*关闭显示区*/
					}
					//保存按钮可用
					document.getElementById("saveDept").disabled = false;	
				},
			"json");
		},
		/*部门删除*/
		del: function(node, treeObj){		
			if(node==null){
				parent.layer.alert("请选择部门!", -1);
				return;
			}else{
				parent.layer.confirm("确定删除"+node.name+"吗？", function(index){
					$.post($.fn.getRootPath()+"/app/auther/dept!delDept.htm",
					{'id_dept':node.id},
					function(jsonResult){
						parent.layer.alert(jsonResult.msg, -1);			
						if(jsonResult.rst){
							Tree0.refresh(node.pId);//刷新树
							closePanel();//关闭显示区
						}				
					},"json");
				});
			}
		}
 	}
 	/*岗位方法*/
 	var StationUtils = {
 		/*岗位新建、编辑*/
		addOrEdit: function(status){
			var _that = this;
			
			//显示编辑区
			openPanel(1);
			
			var treeObj = Tree0.getZTreeObj();
			var node = treeObj.getSelectedNodes()[0];//当前选中节点				
			if(node==null||node==''){					
				parent.layer.alert("请先选择节点！", -1);
				return;
			}
			Tree0.refresh(node.id);//刷新树(防止子节点重名)
			
			//信息初始化
			_that.reset(status, node, treeObj);
			
			//绑定信息重置
			$("#resetStation").unbind("click");
			$("#resetStation").bind("click",function (){
				_that.reset(status, node, treeObj);
			});
			//绑定保存
			$("#saveStation").unbind("click");
			$("#saveStation").bind("click",function (){
				_that.save(status, treeObj);
			});
			//绑定删除
			$("#delStation").unbind("click");
			$("#delStation").bind("click",function (){
				_that.del(node, treeObj);
			});
		},
 		/*岗位重置*/
		reset: function (status, node, treeObj){
			$("input[id$='_station']").each(function (){
				$(this).attr("value","");
			});				
			$("textarea[id$='_station']").each(function (){
				$(this).attr("value","");
			});
			
			if(status=="edit"){
				//编辑赋值
				if(node.pId!=null){
					//部门名称被修改后，子部门的父部门显示的还是以前的名称
					var pNode = treeObj.getNodeByParam("id",node.pId,null);
					$("#id_pstation").attr("value",pNode.id);	
					$("#name_pstation").attr("value",pNode.name);
				}else{
					$("#id_pstation").attr("value","");
					$("#name_pstation").attr("value","");
				}
				
				$("#id_station").attr("value",node.id!=null?node.id:'');
				$("#name_station").attr("value",node.name!=null?node.name:'');
				$("#descContent_station").attr("value",node.descContent!=null?node.descContent:'');						
				
				//标题显示
				$("#title1").html("<b>岗位编辑</b>");
				//显示删除按钮
				$("delStation").css("display","block");
			}else{
				//新建赋值
				$("#id_pstation").attr("value",node.id!=null?node.id:'');
				$("#name_pstation").attr("value",node.name!=null?node.name:'');
				
				//标题显示
				$("#title1").html("<b>岗位新增</b>");	
				//隐藏删除按钮
				$("delStation").css("display","none");
			}		
			
		},
		/*岗位保存*/
		save: function (status, treeObj){
			var id_station = $("#id_station").val();
			var name_station = $("#name_station").val().trim();
			var id_pstation = $("#id_pstation").val();
			
			//判断同级部门是否有重复的部门
			if(name_station!=''){
				var node_p = treeObj.getNodeByParam("id", id_pstation, null);
				var node_ = treeObj.getNodesByFilter(function(node){
					if(node.pId == id_pstation && node.name == name_station && 
						node.type == 'station') return true;
					return false;
				}, true, node_p);
				//同名岗位与当前需要编辑的岗位不是同一个
				if(node_ != null && node_.id != id_station){
					parent.layer.alert("同级部门下不应存在相同岗位!", -1);
					return false;
				}
			}else{
				parent.layer.alert("岗位名称不应为空!", -1);
				return false;
			}
			//判断是否是顶级部门
			id_pstation = id_pstation!='0'?id_pstation:'';
		
			//保存按钮不可用
			document.getElementById("saveStation").disabled = true;			
			$.post($.fn.getRootPath()+"/app/auther/dept!addOrEditStation.htm",
				{
					'id_station':id_station,
					'name_station':name_station,
					'id_pstation':id_pstation,
					'name_pstation':$("#name_pstation").val(),
					'descContent_station':$("#descContent_station").val()
				},
				function(jsonResult){
					parent.layer.alert(jsonResult.msg, -1);
					if(jsonResult.rst){
						Tree0.refresh(id_pstation);//刷新树
						closePanel();//关闭显示区		
					}
					//保存按钮可用
					document.getElementById("saveStation").disabled = false;	
				}, 
			"json");
		},
		/*岗位删除*/
		del: function (node, treeObj){
			if(node==null){
				parent.layer.alert("请选择岗位!");
				return;
			}else{
				parent.layer.confirm("确定删除"+node.name+"吗？", function(){
					$.post($.fn.getRootPath()+"/app/auther/dept!delStation.htm",
					{'id_station':node.id},
					function(jsonResult){
						parent.layer.alert(jsonResult.msg, -1);		
						if(jsonResult.rst){
							Tree0.refresh(node.pId);//刷新树
							closePanel();//关闭显示区
						}			
					},"json");
				});
			}
		}	
 	}
 	/*部门管理*/
 	var initPage = {
		/*初始化*/
		init: function(){
			/*左侧部门树初始化*/
			Tree0.init();
			/*右键菜单事件绑定*/
			$('#addDept').click(function(){ DeptUtils.addOrEdit('add');});
			$('#addStation').click(function(){ StationUtils.addOrEdit('add');});
		}
 	}
 	initPage.init();
 });
 
 
 
 