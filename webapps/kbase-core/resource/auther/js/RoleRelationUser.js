/**
 * 角色关联用户
 * @author Gassol.Bi
 * @modified 2016-3-7 11:41:21
 */
$(function(){
	/*属性拓展*/
	var that = window.RelationUser = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*公共方法*/
	fn.extend({
		/*获取角色id*/
		getRoleId: function(){
			var trSel = $('div[class="juese_left_con"]').find('tr[class*="visited"]')[0];
			return $(trSel).find('input[id="role_id"]').val();
		},
		/*选择器*/
		choose: {
			multiUserByDept:{
				/*事件(题目文本框， 表名)*/
				execute: function(){
					var _that = this, 
						roleId = fn.getRoleId();
					if(roleId){
						$('body').ajaxLoading('正在加载数据...');
						$.ajax({
							type : 'post',
							url : $.fn.getRootPath() + '/app/util/picker!roleuserJson.htm', 
							data : {'id': roleId},
							async: false,
							dataType : 'json',
							success : function(data){
								$('body').ajaxLoadEnd();
								_that._initData = data;
								$.kbase.picker.multiUserByDept2({
									deptNocheck: false,/*是否允许部门进行勾选*/
									diaHeight:$(window).height()*0.9,
									initData: 'RelationUser.fn.choose.multiUserByDept.initData',/*回显数据方法名*/
									callback: 'RelationUser.fn.choose.multiUserByDept.callback'/*弹窗回掉方法名*/
								});
							},
							error : function(msg){
								layer.alert("网络错误，请稍后再试!", -1);
							}
						});
					} else {
						layer.alert("请选择角色!", -1);
					}
				},
				/*数据回显*/
				_initData: null,
				initData: function(){
					return this._initData;
				},
				/*弹窗保存回掉*/
				callback: function(data, backSucc){
					var _that = this, userIds = '',
						roleId = fn.getRoleId();
					$(data).each(function(){ userIds += this.id + ','});
					if(roleId){
						var loadindex = layer.confirm('确定关联当前数据？', function(){
							layer.close(loadindex);
							loadindex = layer.load('提交中…');
							$.ajax({
								type : 'post',
								url : $.fn.getRootPath() + '/app/auther/role!relationUser.htm', 
								data : {'roleId': roleId, 'userIds': userIds},
								async: false,
								dataType : 'json',
								success : function(jsonResult){
									layer.alert(jsonResult.msg, -1, function(){
										if (jsonResult.rst) {
											backSucc();
										}
										layer.closeAll();
									});
								},
								error : function(msg){
									layer.alert("网络错误，请稍后再试!", -1);
								}
							});
						});
					} else {
						layer.alert("数据异常!", -1);
					}
				}
			}
		}
		
	});
	/*主方法*/
	that.extend({
		/*关联用户*/
		relationUser: function(){
			fn.choose.multiUserByDept.execute();
		},
		init: function(){
			/*关联用户*/
			$('#relationUser').click(function(){
				that.relationUser();
			});
		}
	});
	/*初始化*/
	that.init();
});
 