$(function(){
	//知识树
	var ztree = {
		ajaxDataFilter : function(treeId, parentNode, responseData){
			for(var i =0; i < responseData.length; i++) {
				if(responseData[i].bh.indexOf('L.')<0){
		    		responseData[i].nocheck = true;
				}
		    }
		    return responseData;
		},
		setting : {
			view: {
				expandSpeed: "fast"//空不显示动画
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/auther/role-object!list.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				otherParam : {},
				dataFilter: this.ajaxDataFilter
			},
			check: {
				enable: true,
				chkboxType : { "Y" : "s", "N" : "s" }
			},
			callback : {
		//		onCheck : onCheck
			}
		},
		init : function(){
			$.fn.zTree.init($("#roleObjectTree"), this.setting);
		}
	}
	ztree.init();
});


