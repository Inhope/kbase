$(function() { 
	$(document).ajaxLoading('数据处理中...');
	$('div.content_right_bottom').height($(window).height());
	//页面右侧菜单伸缩展开
	var switchRightMenu = {
		menu : $('div.juese_right_bg>div.juese_right1'),
		init : function(){
			var self = this;
			!this.menu || this.menu.each(function(index){
				var s = this;
				$(s).children('div.juese_content').prev().click(function(){
					var chld = $(this).next();
					var other = $('div.juese_content').not(chld);
					if(chld.is(':hidden')){
						$(chld).slideDown(250);
						$(other).slideUp(250);
						$(this).removeClass('juese_right3_title').addClass('juese_right1_title');
						$(other).prev().removeClass('juese_right1_title').addClass('juese_right3_title');
					}else{
						$(chld).slideUp(250);
						$(this).removeClass('juese_right1_title').addClass('juese_right3_title');
					}
				});
			});
			$(this.menu.children('div.juese_content')[0]).show();
		}
	}
	switchRightMenu.init();
	
    //右侧角色信息
	var rolePermObject = {
		rolenameEl : $('#rolename'),
		descroleEl : $('#roledesc'),
		roleId : null,
		setValues : function(rn, dr, rId) {
			this.rolenameEl.val(rn);
			this.descroleEl.val(dr);
			this.roleId = rId;
		},
		getValues: function() {
			return {
				roleId : this.roleId,
	   			name : $.trim(this.rolenameEl.val()),
	   			desc : $.trim(this.descroleEl.val())
			}
		},
		validation : function() {
			if(!(this.rolenameEl.val() && $.trim(this.rolenameEl.val()) != '')) {
				parent.layer.alert('请填写角色名', -1);
				this.rolenameEl.focus();
				return false;
			} else{
				return true;
			}
		}
	}
	var roleObject = {
	    //角色数组
		rolesEl : $('div.juese_left_con tbody tr'),
		renderEvent : function() {
			var self = this;
			//鼠标悬浮变色	
			self.rolesEl.hover(
				function(){
					$(this).addClass("hover").css('cursor', 'default');
				},
				function(){
                   $(this).removeClass("hover");//鼠标离开移除hover样式
         		}
			);
			//默认选中第一行的样式
			$(self.rolesEl[0]).addClass("visited").css('cursor', 'default');
			//单击某角色
			self.rolesEl.click(function() {
				$(document).ajaxLoading('数据加载中...');
				$(this).addClass('visited').siblings().removeClass('visited');
			    //为右侧角色信息赋值
				rolePermObject.setValues($.trim($(this).children(':eq(0)').text()), $.trim($(this).children(':eq(1)').text()), $(this).attr('id'));
				//点击角色，权限管理初始化数据
				treeObject.clickRole($(this).attr('id'));
				//点击某一角色，改变选中角色岗位知识树、目录范围树。
				knowledgeztree.refreshTree({roleId : $(this).attr('id')});
				catalogTree.refreshTree({roleId : $(this).attr('id')});
			});
		},
		//默认显示的角色描述
		renderDefault : function() {
			if(this.rolesEl.size() > 0) {
				var defaultRole = $(this.rolesEl[0]);
				rolePermObject.setValues($.trim(defaultRole.children(':eq(0)').text()), $.trim(defaultRole.children(':eq(1)').text()), defaultRole.attr('id'));
			}
		},
		clearBackColor : function() {
			$('div.juese_left_con tbody tr').removeClass('visited');
		},
		addRow : function(roleId, roleName, roleDesc) {
			var tr = $('<tr id=' + roleId + ' class="visited"><td align="center" width="40%">' + roleName + '<input id="role_id" type="hidden" value="'+roleId+'" /></td><td width="60%">' + roleDesc + '</td></tr>');
			this.rolesEl.parent().append(tr);
			tr.hover(
				function(){
					$(this).addClass("hover").css('cursor', 'default');
				},
				function(){
                   $(this).removeClass("hover");//鼠标离开移除hover样式
         		}
			);
			var self = this;
			tr.click(function() {
				self.clearBackColor();
				$(document).ajaxLoading('数据加载中...');
				$(this).addClass('visited');
			    //为右侧角色信息赋值
				rolePermObject.setValues($.trim($(this).children(':eq(0)').text()), $.trim($(this).children(':eq(1)').text()), $(this).attr('id'));
				//点击角色，权限管理初始化数据
				treeObject.clickRole($(this).attr('id'));
				//点击某一角色，改变选中角色岗位知识树、目录范围树。
				knowledgeztree.refreshTree({roleId : $(this).attr('id')});
				catalogTree.refreshTree({roleId : $(this).attr('id')});
			});
		},
		init : function() {
			this.renderEvent();
			this.renderDefault();
		}
	}
	roleObject.init();
	
	
	
	//****************************************************************************************************************************
	//权限管理
	treeObject = {
		imgNullpermtree : $('img.nullpermtree'),
		fontNullpermtree : $('a.nullpermtree'),
		imgonepermtree : $('img.onepermtree'),
		fontonepermtree : $('a.onepermtree'),
		btnsave_perms_btn : $('#save_perms_btn'),
		treePanel : $('ul#systemTree'),
		zTree : null,
		setting : {
				async: {
					enable: true,
					url: $.fn.getRootPath()+"/app/auther/power!getTreeMenu.htm",
					otherParam: {}
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				view: {
					dblClickExpand: false
				},
				check: {
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "ps", "N": "ps" }
				},
				callback: {
					onClick: function(event, treeId, treeNode, clickFlag) {
						var treeObj = $.fn.zTree.getZTreeObj(treeId);
						treeObj.checkNode(treeNode,treeNode.checked?false:true , true);
					}
				}
								
		},
		savePower : function(roleId) {
			if(roleId==null||roleId=="")return;
			$(document).ajaxLoading('数据处理中...');
			nodes = this.zTree.getCheckedNodes(true);		
			var menuIds = "";//初始化
			for (var i=0;i<nodes.length;i++){
				menuIds +=  nodes[i].id + ",";
			}
			menuIds = menuIds.substring(0, menuIds.length-1);
							
			$.post($.fn.getRootPath()+"/app/auther/power!savePower.htm",
				{"menuIds":menuIds,"roleId":roleId},function (data){
					if(data=="1"){
						parent.layer.alert("保存成功!", -1);
					}else{
						parent.layer.alert("保存失败!", -1);
					}
				$('document').ajaxLoadEnd();				
			},"html");
		},
		clickRole : function (roleId){
			this.setting.async.otherParam = {'roleId':roleId};
			//菜单树初始化
			$.fn.zTree.init(this.treePanel, this.setting);	
			this.zTree = $.fn.zTree.getZTreeObj("systemTree");		
			//绑定保存事件
			$(this.btnsave_perms_btn).unbind("click");
			$(this.btnsave_perms_btn).click(function (){
				treeObject.savePower(roleId);		
			});
			
			//初始化折叠按钮
			$(this.imgonepermtree).attr("src",$.fn.getRootPath()+"/theme/" + parent.USER_THEME + "/resource/auther/images/juese/juese_ico4.jpg");
			$(this.fontonepermtree).html("折叠所有节点");
		},
		//展开全部
		expandNodeAll : function(flag){
			this.zTree.expandAll(flag);
			if(flag){
				$(this.imgonepermtree).attr("src",$.fn.getRootPath()+"/theme/" + parent.USER_THEME + "/resource/auther/images/juese/juese_ico4.jpg");
				$(this.fontonepermtree).html("折叠所有节点");
			}else{
				$(this.imgonepermtree).attr("src",$.fn.getRootPath()+"/theme/" + parent.USER_THEME + "/resource/auther/images/juese/juese_ico3.jpg");
				$(this.fontonepermtree).html("展开所有节点");
			}				
		},
		//清空
		checkNodeAll :function(){
			this.zTree.checkAllNodes(false);
		},
		init : function() {
			//保存
			this.clickRole(rolePermObject.roleId);
			
			//折叠
			var self = this;
			this.fontonepermtree.click(function (){
				var ss = $(self.fontonepermtree).html();
				if(ss.indexOf("展开")>-1){					
					self.expandNodeAll(true);
				}else{
					self.expandNodeAll(false);
				}				
			});
			
			this.imgonepermtree.click(function (){
				var ss = $(self.fontonepermtree).html();
				if(ss.indexOf("展开")>-1){					
					self.expandNodeAll(true);
				}else{
					self.expandNodeAll(false);
				}				
			});
			
			//清空
			//this.fontNullpermtree.click(function(){
				//self.checkNodeAll();
			//});
			//this.imgNullpermtree.click(function(){
				//self.checkNodeAll();
			//});
		}
	}
	treeObject.init();
	
	//****************************************************************************************************************************	
	var refreshTrees = {
		refresh : function(){
			rolePermObject.setValues('','', '');
	    	treeObject.clickRole('');
	    	knowledgeztree.refreshTree({'roleId':''});
	    	catalogTree.refreshTree({'roleId':''});
		}
	}
	//角色描述
	var saveRole = {
	    buttonSave : $('#saveRole'),
	    btnsaveRole : $('#save_roleinfo'),
	    save : function(){
			var roleName = rolePermObject.rolenameEl.val();
			var roleDesc = rolePermObject.descroleEl.val();
			var roleId = rolePermObject.roleId;
			$(document).ajaxLoading('数据处理中...');
			$.ajax({
		   		type : "POST",
		   		url : $.fn.getRootPath() + "/app/auther/role!saveRole.htm",
		   		data : rolePermObject.getValues(),
		   		async : true,
		   		dataType : 'json',
		   		success : function(data){
		   			$(document).ajaxLoadEnd();
		   			if(data.success) {
			   			if(data.message == '修改成功'){
			   				//在页面修改
		   					var tr = $('tr.visited');
		   					tr.children(':eq(0)').text(roleName);
		   					tr.children(':eq(1)').text(roleDesc);
			   			}else{
			   				//在表格中添加一行
			   				roleObject.addRow(data.message, roleName, roleDesc);
			   				rolePermObject.roleId = data.message;
			   				//显示菜单树
			   				treeObject.clickRole(rolePermObject.roleId);
			   			}
			   			parent.layer.alert('保存成功', -1);
		   			} else {
		   				parent.layer.alert(data.message, -1);
		   			}
		   		},
		   		error : function(){
	   				alert("请求失败!");
	   				$(document).ajaxLoadEnd();
		   		}
			 });
	    },
	    init : function(){
	        var self = this;
	        //新增
	    	this.buttonSave.click(function(){
	    	    //清空右侧角色信息
		    	rolePermObject.setValues('','', '');
		    	//初始化功能权限、岗位知识、目录范围的zTree
		    	treeObject.clickRole('');
		    	knowledgeztree.refreshTree({'roleId':''});
		    	catalogTree.refreshTree({'roleId':''});
		    	//点击添加按钮时清除角色列表的背景色
		    	roleObject.clearBackColor();
		    	$(switchRightMenu.menu[0]).children('div.juese_content').prev().removeClass('juese_right3_title').addClass('juese_right1_title');
		    	$(switchRightMenu.menu[0]).children('div.juese_content').show();
		    	$(switchRightMenu.menu).not(':eq(0)').children('div.juese_content').prev().removeClass('juese_right1_title').addClass('juese_right3_title');
		    	$(switchRightMenu.menu).not(':eq(0)').children('div.juese_content').hide();
		    	rolePermObject.rolenameEl.focus();
    		});
    		this.btnsaveRole.click(function() { 
    			if(rolePermObject.validation()){
    				self.save();
    			}
    		});
	    }
	}
	saveRole.init();
	
	//目录
	var catalogTree = {
		butImgnull : $('img.qikongcatalog'),
		butFontnull : $('a.qikongcatalog'),
		butImgReflsh : $('img.refleshcatalog'),
		butFontReflsh : $('a.refleshcatalog'),
		btnsave_catalog_btn : $('#save_catalog_btn'),
		zTree : null,
		catalogTreeEl : $('#catalogTree'),
		setting : {
			view: {
				expandSpeed: "fast"//空不显示动画
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/auther/role!categoryList.htm",
				autoParam : ['id', 'bh'],
				otherParam : {}
			},
			data: {
				simpleData: {
					enable: true,
					pIdKey : 'parentId'
				}
			},
			check: {
				enable: true,
				chkboxType : { "Y" : "s", "N" : "s" }
			},
			callback: {
				beforeAsync : function(treeId, treeNode) {
					if(treeNode) {
						return !(treeNode.bh.indexOf('L') > -1);
					}
				},
				onAsyncSuccess : function(event, treeId, treeNode, msg) {
					var zyzsdId = $("#zyzsdId").val();
					var treeObj = $.fn.zTree.getZTreeObj(treeId);
					var zyzsdNode = treeObj.getNodeByParam("id", zyzsdId, null);
					//是否显示所有目录（zyzsdNode为空显示所有部门，反之不显示所有目录）
					if(zyzsdNode != null){
						//获取专业知识点下的节点
						var nodes = treeObj.getNodesByParam("parentId", zyzsdNode.id, zyzsdNode);
						//获取所有一级节点
						var nodes_ = treeObj.getNodesByParam("parentId", null);
						for(var i=0;i<nodes_.length;i++){
							//移除所有节点
							treeObj.removeNode(nodes_[i]);
						}
						//添加专业知识点下的节点
						treeObj.addNodes(null, nodes);
					}
					$(document).ajaxLoadEnd();
				}
			}
		},
		setZtreeParam : function(params) {
			this.setting.async.otherParam = params;
		},
		refreshTree : function(params) {
			this.setZtreeParam(params);
			$.fn.zTree.init(this.catalogTreeEl, this.setting);
		},
		save : function(){
			var self = this;
			var roleCatalog = this.zTree.getCheckedNodes(true);
			$(document).ajaxLoading('数据处理中...');
			var catalogId = '';
			var catalogBh = '';
			for(var i = 0; i < roleCatalog.length; i++){
				if(catalogId == ''){
					catalogId += roleCatalog[i].id;
					catalogBh += roleCatalog[i].bh;
				} else {
					catalogId += ','+roleCatalog[i].id;
					catalogBh += ','+roleCatalog[i].bh;
				}
			}
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath() + "/app/auther/role!saveCatalog.htm",
		   		data: {
		   			roleId : rolePermObject.roleId,
		   			catalogs : catalogId,
		   			catalogBh :catalogBh
		   		},
		   		async: true,
		   		dataType : 'json',
		   		success : function(data){
		   			if(data.success) {
			   			knowledgeztree.refreshTree({
			   				'roleId' : rolePermObject.roleId
			   			});
		   			}
		   			$(document).ajaxLoadEnd();
					parent.layer.alert(data.message, -1);
				},
				error : function(){
					parent.layer.alert('请求超时!', -1);
					$(document).ajaxLoadEnd();
				}
	   		});
		},
		init : function(){
			var self = this;
			this.setZtreeParam({roleId : rolePermObject.roleId});
			$.fn.zTree.init(this.catalogTreeEl, this.setting);
			this.zTree = $.fn.zTree.getZTreeObj('catalogTree');
    		this.btnsave_catalog_btn.click(function(){
	    		if(!rolePermObject.roleId){
	    			parent.layer.alert('请先选择一个角色再操作!', -1);
	    			return false;
	    		}
	   			self.save();
    		});
			//反选
			this.butImgnull.click(function(){
			    self.butFontnull.trigger('click');
		    });
			this.butFontnull.click(function(){
				var nodes = self.zTree.getCheckedNodes(true);
				for(var i = 0; i < nodes.length; i ++) {
					nodes[i].checked = false;
					self.zTree.updateNode(nodes[i]);
				}
			});
			//刷新
			this.butImgReflsh.click(function(){
				$(document).ajaxLoading('数据加载中...');
			    self.refreshTree({
			    	'roleId' : rolePermObject.roleId
			    });
		    });
			this.butFontReflsh.click(function(){
				self.butImgReflsh.trigger('click');
			});
		}
	}
	catalogTree.init();
	//实例
	knowledgeztree = {
		butImgnull : $('img.qikongobject'),
		butFontnull : $('a.qikongobject'),
		butImgReflsh : $('img.refleshobject'),
		butFontReflsh : $('a.refleshobject'),
		btnsave_objects_btn : $('#save_objects_btn'),
		roleObjectTreeEl : $('#roleObjectTree'),
		zTree : null,
		setting : {
			view: {expandSpeed: "fast"},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/auther/role!objectList.htm",
				autoParam : ['id', 'bh'],
				dataFilter : function(treeId, parentNode, responseData){
					for(var i =0; i < responseData.length; i++) {
						if(responseData[i].isParent == "true"){
				    		responseData[i].nocheck = true;
						}
				    }
				    return responseData;
			   }
			},
			data: {
				simpleData: {
					enable: true,
					pIdKey : 'parentId'
				}
			},
			check: {
				enable: true,
				chkboxType : { "Y" : "s", "N" : "s" }
			},
			callback : {
				beforeAsync : function(treeId, treeNode) {
					if(treeNode) {
						return !(treeNode.bh.indexOf('L') > -1);
					}
				},
				onAsyncSuccess : function(event, treeId, treeNode, msg) {
					var zyzsdId = $("#zyzsdId").val();
					var treeObj = $.fn.zTree.getZTreeObj(treeId);
					var zyzsdNode = treeObj.getNodeByParam("id", zyzsdId, null);
					//是否显示所有目录（zyzsdNode为空显示所有部门，反之不显示所有目录）
					if(zyzsdNode != null){
						//获取专业知识点下的节点
						var nodes = treeObj.getNodesByParam("parentId", zyzsdNode.id, zyzsdNode);
						//获取所有一级节点
						var nodes_ = treeObj.getNodesByParam("parentId", null);
						for(var i=0;i<nodes_.length;i++){
							//移除所有节点
							treeObj.removeNode(nodes_[i]);
						}
						//添加专业知识点下的节点
						treeObj.addNodes(null, nodes);
					}
					$(document).ajaxLoadEnd();
				}
			}
		},
		setZtreeParam : function(params) {
			this.setting.async.otherParam = params;
		},
		refreshTree : function(params) {
			this.setZtreeParam(params);
			this.zTree = $.fn.zTree.init(this.roleObjectTreeEl, this.setting);
		},
		save : function(){
			var self = this;
			$(document).ajaxLoading('数据处理中...');
			var objectId = '';
			var objectBh = '';
			var roleObjects = this.zTree.getCheckedNodes(true);
			for(var i = 0; i < roleObjects.length; i++){
				if(!objectId){
					objectId += roleObjects[i].id;
					objectBh += roleObjects[i].getParentNode().bh + '.' + roleObjects[i].id;
				} else {
					objectId += ',' + roleObjects[i].id;
					objectBh += ',' + roleObjects[i].getParentNode().bh + '.' + roleObjects[i].id;
				}
			}
			
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + "/app/auther/role!saveObject.htm",
				data : {
					roleId : rolePermObject.roleId,
					object : objectId,
					objectBh : objectBh										   																			   					   			
				},
				async: true,
		   		dataType : 'json',
		   		success : function(data){
		   			$(document).ajaxLoadEnd();
					parent.layer.alert(data.message, -1);
				},
				error : function(){
					$(document).hint('请求失败');
					$(document).ajaxLoadEnd();
				}
			});
		},
		init : function(){
			var self = this;
			this.setZtreeParam({
				roleId : rolePermObject.roleId
			});
			$.fn.zTree.init(this.roleObjectTreeEl, this.setting);
			this.zTree = $.fn.zTree.getZTreeObj('roleObjectTree');
			this.btnsave_objects_btn.click(function(){
				if(!rolePermObject.roleId){
	    			parent.layer.alert('请先选择一个角色再操作!', -1);
	    			return false;
	    		}
				self.save();
			});
			//清空
			this.butImgnull.click(function(){
				var nodes = self.zTree.getCheckedNodes(true);
				for(var i = 0; i < nodes.length; i ++) {
					nodes[i].checked = false;
					self.zTree.updateNode(nodes[i]);
				}
			});
			this.butFontnull.click(function(){
				self.butImgnull.trigger('click');
			});
			//刷新
			this.butImgReflsh.click(function(){
				$(document).ajaxLoading('数据加载中...');
			    self.refreshTree({
			    	'roleId' : rolePermObject.roleId
			    });
			});
			this.butFontReflsh.click(function(){
				self.butImgReflsh.trigger('click');
			});
		}
	}
	knowledgeztree.init();
	//查询知识
	var searchIndex=0;
	var searchBh="start";
	var searchObject = {
		//查询实例
		text : $("input#treeInput"),
		input : $("#treeInput"),
		btnSearch : $("#treeButton"),
		//查询目录
		catalogText : $('input#catagoryInput'),
		btncatalogInput : $("#catagoryInput"),
		btncatagorySearch : $('#catagoryButton'),
		defaultText: '请输入要搜索的内容...',
		selectNodeByBh : function(bh, type){//参数type用于判断在哪里查询（目录范围或者角色岗位知识）
			var treeObj = '';
			if(type == 'object'){
				treeObj = $.fn.zTree.getZTreeObj("roleObjectTree");
			}else{
				treeObj = $.fn.zTree.getZTreeObj("catalogTree");
			}
			var	cNode = treeObj.getNodeByParam("bh", bh);
			treeObj.selectNode(cNode);
			this.input.focus();
		},
		expandPath : function(bh, type){
			this.selectNodeByBh(bh, type);
		},
		search : function(type){
			var self = this;
			var name = '';
			if(type == 'catalog'){
				name = this.catalogText.val();
			}else{
				name = this.text.val();
			}
			$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + "/app/auther/role!searchObject.htm",
				data : {
					name : $.trim(name),
					type : type
				},
				async: false,
				dataType : 'json',
				success : function(msg){
					self.expandPath(msg, type);
				},
				error : function(msg){parent.$(document).hint("请求异常");}
			});
		},
		init : function(){
			var self = this;
			//搜索实例
			this.btnSearch.click(function() {
				if(!self.text.val() || $.trim(self.text.val()) == self.defaultText) {
					parent.layer.alert(self.defaultText, -1);
					return;
				}
				self.search('object');
			});
			//搜索目录
			this.btncatagorySearch.click(function() {
				if(!self.catalogText.val() || $.trim(self.catalogText.val()) == self.defaultText) {
					parent.layer.alert(self.defaultText, -1);
					return;
				}
				self.search('catalog');
			});
			this.input.blur(function (e) {
				if(!$(this).val() || $.trim($(this).val()) == '') {
					$(this).val(self.defaultText);
				}
			});
			this.btncatalogInput.blur(function (e) {
				if(!$(this).val() || $.trim($(this).val()) == '') { 
					$(this).val(self.defaultText);
				}
			});
			this.input.focus(function (e) {
				if($.trim($(this).val()) == self.defaultText) {
					$(this).val("");
				}
			});
			this.btncatalogInput.focus(function (e) {
				if($.trim($(this).val()) == self.defaultText){
					$(this).val("");
				}
			});
			this.input.keypress(function (e) {
				if(e.keyCode == 13) {
					self.search('object');
				}
			});
			this.btncatalogInput.keypress(function (e) {
				if(e.keyCode == 13) {
					self.search('catalog');
				}
			});
		}
	}
	searchObject.init();
	//删除角色
	var deleteRole = {
		buttonDelete : $('#deleteRole'),
		deleteRole : function(){
			//角色id
			var roleId = rolePermObject.roleId;
			$.post($.fn.getRootPath() + "/app/auther/role!ajaxTotalUser.htm",
				{"roleId":roleId},
				function(data){
					var bl = true,confirmMsg = "确定要删除当前角色吗？";
					if(data.total>0)confirmMsg = confirmMsg + "\n当前角色已被"+data.total+"个用户引用！";
					parent.layer.confirm(confirmMsg, function(index){
						$(document).ajaxLoading('正在删除...');
						$.ajax({
							type: "POST",
						   	url: $.fn.getRootPath() + "/app/auther/role!deleteRole.htm",
						   	data: {
						   		roleId : roleId
						   	},
						   	async: true,
						   	dataType : 'json',
						   	success: function(data){
						   		$(document).ajaxLoadEnd();
						   		if(data.success){
						   			parent.layer.alert(data.message, -1);
						   			//在页面删除一行 
							   		$('tr.visited').remove();
							   		refreshTrees.refresh();
						   		}else{
						   			parent.layer.alert(data.message, -1)
						   		}
						   	}
						});
						parent.layer.close(index);
					});
				},"json");
			
		},
		init : function(){
			var self = this;
			self.buttonDelete.click(function() {
				self.deleteRole();
			});
		}
	}
	deleteRole.init();
});
