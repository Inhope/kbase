/**
 * modify by eko.zhan at 2015-01-27
 * 奇怪，当前文件在全文中并没有被引用，不知是否我查找遗漏的缘故，如果你发现这个文件有被引用，请在此标记一下，thx
 */
$(function() {
	//为浮层里的内容赋值
	var userObject = {
		userId : $('#roleId'),
		userName :　$('#name'),
		password : $('#password'),
		roles : $('input:checkbox.role'),
		setValues : function(un,pd,id){
			this.userName.val(un);
			this.password.val(pd);
			this.userId.val(id);
		},
		//修改用户时改变选中用户的角色勾选状态
		setRolesCheckStatus : function(rss){
			if(rss == ''){
				for(var i = 0; i < this.roles.length; i++){
					this.roles[i].checked = false;
				}
			}else{
				for(var i = 0; i < this.roles.length; i++){
					for(var j = 0;j < rss.length; j++){
						if($.trim(this.roles[i].id) == $.trim(rss[j].id)){
							this.roles[i].checked = true;
						}
					}
				}
			}
		}
	}
	//用户列表
	var users = {
		userList : $('div#contents tbody tr:not(:eq(0))'),
		userObject : userObject,
		mouseThings : function(){
			var self = this;
			//鼠标悬浮改变行颜色
			this.userList.hover(
				function(){
					$(this).addClass("hover");
				},
				function(){
                   $(this).removeClass("hover");
         		}
			);
			this.userList.click(function(){
				$(this).addClass('visited').siblings().removeClass('visited');
				self.userObject.setValues($.trim($(this).children(':eq(0)').text()),$.trim($(this).children(':eq(1)').text()),$(this).attr('id'));
				var roles = $(this).find('input:hidden.roleId');
				self.userObject.setRolesCheckStatus(roles);
			});
		},
		init : function(){
			this.mouseThings();
		}
	}
	users.init();
	//弹出或关闭浮层(添加用户和修改用户时)
	var divForSaveOrUpdate = {
		userObject : userObject,
		saveDiv : $('div.save_user_div'),
		saveDivC : $('div.save_user_div_c'),
		showDiv : function(){
			var w = ($('body').width() - $('div.perm_d').width())/2;
			this.saveDiv.css('left',w+'px');
			this.saveDiv.css('top',50+'px');
			$('body').showShade();
			this.saveDiv.show();
			this.saveDivC.show();	
		},
		closeDiv : function(){
			$('body').hideShade();
			this.saveDiv.hide();
			this.userObject.setValues('','','');
			this.userObject.setRolesCheckStatus('');
		},
		init : function(){
			this.showDiv();
			this.closeDiv();		
		}
	}
	divForSaveOrUpdate.init();
	
	//添加用户或修改用户
	var saveOrUpdate = {
		//打开或关闭浮层
		divForSaveOrUpdate : divForSaveOrUpdate,
		//打卡添加浮窗
		butOpenSaveUserDiv : $('#but_save_user'),
		//打卡修改浮层
		butOpenUpdateUserDiv : $('#updateUser'),
		//关闭浮层
		butCloseDiv : $('#close_user'),
		//添加或修改按钮
		butSaveOrUpdateUser : $('#save_user'),
		//用户详细信息
		userObject : userObject,
		//用户列表
		users :　users,
		//添加或修改事件
		saveOrUpdate : function(){
			var self = this;
			this.butSaveOrUpdateUser.click(function(){
				var userId = self.userObject.userId.val();
				var username = self.userObject.userName.val();
				var password = self.userObject.password.val();
				var roles = self.userObject.roles;
				var rolesId = '';
				var showRoles = ''; 
				for(var i=0; i<roles.length; i++){
					if(roles[i].checked == true){
						showRoles += '<input type="hidden" class="roleId" id="'+roles[i].id+'" value="'+roles[i].id+'" />';
						if(rolesId == ''){
							rolesId += roles[i].id;
						}else{
							rolesId += ','+roles[i].id;
						}
					}
				}
				$.ajax({
					type : 'POST',
					url : $.fn.getRootPath() + "/app/auther/user!saveUser.htm",
					data : {
						userId : userId,
						name : username,
			   			password : password,
			   			roles : rolesId
					},
					async : true,
					dataType : 'json',
					success : function(data){
						if(data.success){
							//修改用户角色时，需把角色id加进去
							data.message += showRoles;
							if(userId == ''){
								parent.layer.alert('添加成功', -1);
								//在页面添加一行
								$('div#contents tbody tr:last-child').after('<tr id='+data.data.id+'><td>'+data.data.username+'</td>'+
								'<td>'+data.data.password+'</td><td>'+data.message+'</td></tr>');
								self.users.userList = $('div#contents tbody tr:not(:eq(0))');
								self.users.init();
								self.divForSaveOrUpdate.closeDiv();
							}else{
								parent.layer.alert('修改成功', -1);
								//在页面修改选中行
								for(var i = 0; i < self.users.userList.length; i++){
					   				if($(self.users.userList[i]).attr('id') == data.data.id){
					   					$(self.users.userList[i]).children(':eq(0)').text(data.data.username);
					   					$(self.users.userList[i]).children(':eq(1)').text(data.data.password);
					   					$(self.users.userList[i]).children(':eq(2)').html(data.message);
					   				}
					   			}
					   			self.divForSaveOrUpdate.closeDiv();
							}
						}
					},
					error : function(msg){
						parent.layer.alert('添加失败', -1);
					}
				});
			});
		},
		init : function(){
			var self = this;
			this.butOpenSaveUserDiv.click(function(){
				self.userObject.setValues('','','');
				self.userObject.setRolesCheckStatus('');
				self.divForSaveOrUpdate.showDiv();
			});
			this.butOpenUpdateUserDiv.click(function(){
				self.divForSaveOrUpdate.showDiv();
			});
			this.butCloseDiv.click(function(){
				self.divForSaveOrUpdate.closeDiv();
			});
			this.saveOrUpdate();
		}
	}
	saveOrUpdate.init();
	//删除用户
	var deleteUser = {
		butDeleteUser : $('#daleteUser'),
		users : users,
		init : function(){
			var self = this;
			this.butDeleteUser.click(function(){
				if(confirm('确定要删除吗?')){
					var id = $('#roleId').val();
					$.ajax({
						type : 'POST',
						url : $.fn.getRootPath() + "/app/auther/user!delUser.htm",
						data : {
							userId : id
						},
						async : true,
						dataType : 'json',
						success : function(msg){
							if(msg == 'ok'){
								parent.layer.alert('删除成功', -1);
								//在页面删除一行 
				   				for(var i = 0; i<self.users.userList.length; i++){
					   				if($(self.users.userList[i]).attr('id') == id){
					   					$(self.users.userList[i]).remove();
					   				}
					   			}
							}else{
								parent.layer.alert('删除失败', -1);
							}
						},
						error : function(msg){
							parent.layer.alert('删除失败', -1);
						}
					});
				}
			});
		}
	}
	deleteUser.init();
});