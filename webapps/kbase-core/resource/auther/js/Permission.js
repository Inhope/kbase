/********权限树***************/
var rMenu;
var zTree;
var setting = {
	async: {
		enable: true, //开启异步加载方式
		url: $.fn.getRootPath() + "/app/auther/perm!showPerm.htm",  //异步加载时请求路径
		autoParam: ["id", "name", "purl", "isParent", "checked", "open"], //异步加载时加载哪些属性
		dataType : "json" //数据格式
	},	 
	
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false
	},
	check: {
	     enable: true
    },
	data: {
		simpleData: {
			enable : true
		},
		keep : {
			parent : true
		}
	},
	callback: {
	   onRightClick: OnRightClick
	}
};

/**********实现右键菜单**************/
    function OnRightClick(event, treeId, treeNode) {
			if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
				zTree.cancelSelectedNode();
			} else if (treeNode && treeNode.isParent) {
				zTree.selectNode(treeNode);
				showRMenu("root", event.clientX, event.clientY);
			}else if(treeNode && !treeNode.isParent){
			    showRMenu("node", event.clientX, event.clientY);
			}
		} 
	//显示右键菜单
	function showRMenu(type, x, y) {
			$("#rMenu ul").show();
			if (type == "root") {
				$("#p_add_div").show();
				$("#p_add").show();
				$("#p_update_div").show();
				$("#p_update").show();
				$("#p_del").show();
			} else {
			    $("#p_del").show();
				$("#p_update").show();
				$("#p_add").hide();
				$("#p_add_div").hide();
				$("#p_update_div").hide();
			}
			rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
		}
		
		//隐藏右键菜单
		function hideRMenu() {
			if (rMenu) rMenu.css({"visibility": "hidden"});
			$("body").unbind("mousedown", onBodyMouseDown);
		}
		
		//点击页面隐藏右键菜单
		function onBodyMouseDown(event){
			if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
				rMenu.css({"visibility" : "hidden"});
			}
		}
		
    /*****end**********/
	$(document).ready(function(){
	    rMenu = $("#rMenu");
		var t = $("#tree");
		$.fn.zTree.init(t, setting);
		zTree = $.fn.zTree.getZTreeObj("tree");
		//浮层操作
		var option = {
			newPerm_div : $('#p_add'),//添加权限浮窗
			savePerm : $('#save_P'),//添加权限
			save_div : $('#p_add_div'),//添加目录浮窗
			saveReset : $('#reset_p'),//重置添加
			
			updatePerm_div : $('#p_update'),//修改权限浮窗
			updatePerm : $('#perm_update'),//修改权限
			updateReset : $('#perm_reset'),//重置修改
			
			deletePerm : $('#p_del'),//删除
			closePerm : $('#closePerm'),//关闭浮窗
			body : $('body'),
			permhover : $('div#rMenu ul').children('li'),
			init : function() {
				this.body.click(function(){
					hideRMenu();
				});
				this.permhover.hover(
					function(){
						$(this).addClass("hover").css('cursor', 'default');
					},
					function(){
	                   $(this).removeClass("hover");//鼠标离开移除hover样式
	         		}
				);
				//弹出添加浮窗
			    this.newPerm_div.click(function(){
					var w = ($('body').width() - $('div.perm_d').width())/2;
					$('div.perm_d').css('left',w+'px');
					$('div.perm_d').css('top',50+'px');
					$('body').showShade();
					$('div.perm_d').show();
					$('div.perm_d_c').show();
					$('.url').show();
					$('#url_div').show();
					$('div.perm_d_u').hide();
					hideRMenu();
				});
				
				//填出添加目录浮窗
                this.save_div.click(function(){
                    var w = ($('body').width() - $('div.perm_d').width())/2;
					$('div.perm_d').css('left',w+'px');
					$('div.perm_d').css('top',50+'px');
					$('body').showShade();
					$('div.perm_d').show();
					$('div.perm_d_c').show();
					$('div.perm_d_u').hide();
					$('.url').hide();
					$('#url_div').hide();
					hideRMenu();
                });

				//弹出修改权限浮窗
				this.updatePerm_div.click(function(){
				    var w = ($('body').width() - $('div.perm_d').width())/2;
					$('div.perm_d').css('left',w+'px');
					$('div.perm_d').css('top',50+'px');
					$('body').showShade();
					$('div.perm_d').show();
					$('div.perm_d_u').show();
					$('div.perm_d_c').hide();
					var nodes = $.fn.zTree.getZTreeObj("tree").getSelectedNodes();
					for(var i = 0; i<nodes.length; i++){
					$('.update_name').val(nodes[0].name);
				       if(nodes[i].isParent){
							$('.update_url').val('此处不可以添加url');
						    $('.update_url').attr('disabled',true);
						}else{
						    $('.update_url').attr('disabled',false);
							$('.update_url').val(nodes[0].purl);
						}
					}
					hideRMenu();
				});
				
			    //关闭浮窗
				this.closePerm.click(function(){
					$('div.perm_d').hide();
					$('body').hideShade();
				});
				
				//清空修改
				this.updateReset.click(function(){
				    var name = $('.name').val('');
			        var url = $('.url').val('');
				});
				
				//清空添加
				this.saveReset.click(function(){
				    var name = $('.name').val('');
			        var url = $('.url').val('');
				});
				
				//添加权限
				this.savePerm.click(function(){
				    
				    var name = $('.name').val();
			        var url = $('.url').val();
			        if(name == null || $.trim(name) == ''){
			            parent.$(document).hint("名字不可以为空");
			        }else{
				        var parentId = ''
				        var nodes = $.fn.zTree.getZTreeObj("tree").getSelectedNodes();
				        if(nodes[0].isParent){
				            parentId = nodes[0].id;
				        }
					    $('div.perm_d').hide();
						$('body').hideShade();
						$('body').ajaxLoading('正在提交数据...');
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath() + "/app/auther/perm!savePerm.htm",
					   		data: {
					   			name : name,
					   			url : url,
					   			parentId : parentId
					   		},
					   		async: true,
					   		dataType : 'json',
					   		success: function(data){
					   		
					   			if(data.success) {
						   			
						   			var newNode = '';
						   			var pid = data.data.id;
						   			var purl = data.data.url;
						   			var path = data.data.path;
						   			var parentId = data.data.parentId;
						   			//判断是否是目录
									if(purl == null || $.trim(purl) == ''){
									   newNode = {id : pid, parentId : parentId, name : name, purl : purl, path : path, isParent : true};
									}else{
									   newNode = {id : pid, parentId : parentId, name : name, purl : purl, path : path, isParent : false};
									}
									if (nodes[0]) {
										nodes[0].zAsync = 'false';//父节点展开时不自动异步加载
										zTree.addNodes(nodes[0], newNode);
									} 
									hideRMenu();
						   			$('.name').val('')
						   			$('.url').val('')
					   			} else {
					   				parent.layer.alert(data.message, -1);
					   			}
					   			$('body').ajaxLoadEnd();
					   		},
					   		error : function(){
					   			parent.$(document).hint("添加失败");
					   			$('body').ajaxLoadEnd();
					   		}
						 });
					} 
				});
				
				//修改权限
				this.updatePerm.click(function(){
				    var name = $('.update_name').val();
			        var url = $('.update_url').val();
			        
			        if(name == null || $.trim(name) == ''){
			            parent.$(document).hint("名字不可以为空");
			        }else{
			            var id = '';
			            var parentId = '';
			            var path = '';
			            var purl = '';
			            var nodes = $.fn.zTree.getZTreeObj("tree").getSelectedNodes();
				        id = nodes[0].id;
				        path = nodes[0].path;
				        if(nodes[0].isParent){
				           purl == null;
				           parentId == null;
				        }else{
			        	   purl = nodes[0].purl;
					       parentId = nodes[0].parentId;
				        }
						$('div.perm_d').hide();
						$('body').hideShade();
						$('body').ajaxLoading('正在提交数据...');
						$.ajax({
					   		type: "POST",
					   		url: $.fn.getRootPath() + "/app/auther/perm!updatePerm.htm",
					   		data: {
					   			name : name,
					   			url : purl,
					   			id : id,
					   			parentId : parentId,
					   			path : path
					   		},
					   		async: true,
					   		success: function(msg){
						   		if(msg == '"success"'){
						   			parent.$(document).hint("修改成功");
						   			hideRMenu();
									nodes[0].name = name;
									nodes[0].purl = url;
									nodes[0].id = id;
									nodes[0].path = path;
									nodes[0].parentId = parentId;
									zTree.updateNode(nodes[0]);
						   			$('body').ajaxLoadEnd();
						   		}else{
						   		    parent.$(document).hint("修改失败");
						   		}
					   		},
					   		error: function(){
					   			parent.$(document).hint("修改失败");
					   			$('body').ajaxLoadEnd();
					   		}
						 });
			        }
				});
				
				//删除权限
			   this.deletePerm.click(function(){
		  		    hideRMenu();
			        var msg = '';
			        var nodes = zTree.getSelectedNodes();
			        if (nodes[0].children && nodes[0].children.length > 0) {
			           msg = "子权限会随此删除！\n\n请确认！";
			        }else{
			           msg = "确定要删除吗?\n\n请确认！";
			        }
			        parent.layer.confirm(msg, function(index){
			        	var id = nodes[0].id;
					    $.ajax({
					        type : "post",
							url : $.fn.getRootPath() + "/app/auther/perm!deletePerm.htm",
							data: {
					   			id : id
					   		},
					   		async: true,
							success : function(msg) {
								if(msg == '"success"'){
									hideRMenu();
									
								    if(nodes[0].children && nodes[0].children.length > 0){
								   		zTree.removeNode(nodes[0]);
									    parent.$(document).hint("删除成功");
								    }else {
								        zTree.removeNode(nodes[0]);
										parent.$(document).hint("删除成功");
								    }
							  
								}else{
								     parent.$(document).hint("删除失败");
								} 
							}
					    });
					    parent.layer.close(index);
			        });
			   });
			}
		}
		option.init();
	});	