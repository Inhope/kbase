function submitForm() {
/*
	var _nullArr = ["system_title", "system_login_title"]; 
	var _param = [];
	
	var flag = true;
	$("input[zokee='x-man']").each(function(i, item){
		var label = $.trim($(item).attr("xtext"));
		var name = $.trim($(item).attr("xname"));
		var value = $.trim($(item).val());
		var index = $(item).attr("i");
		
		if ($.inArray(name, _nullArr)!=-1 && value==""){
			alert(label + "不能为空");
			flag = false;
			return false;
		}
		
		if (name=="system_banner_title" && value==""){
			value = $("input[xname='system_login_title'][zokee='x-man']").val();
		}
		if (name=="system_banner_logo" && value==""){
			value = $("input[xname='system_login_logo'][zokee='x-man']").val();
		}
		
		_param.push({
			"label" : label,
			"name" : name,
			"value" : value,
			"i" : index
		});
	});
	
	if (!flag) return false;
	*/
	//系统标题不能为空
	if ($("input[name='system_title']").val()==""){
		parent.layer.alert($("input[xname='system_title']").attr("xtext") + "不能为空", -1);
		return false;
	}
	
	//如果登录标题和登录logo同时为空，则默认为系统标题
	//if ($("input[name='system_login_title']").val()=="" && $("input[name='imgName_system_login_logo']").val()==""){
	//	$("input[xname='system_login_title']").textbox('setValue', $("input[xname='system_title']").val());
	//}
	//如果banner标题和bannerlogo同时为空，则默认为banner标题
	//if ($("input[name='system_banner_title']").val()=="" && $("input[name='imgName_system_banner_logo']").val()==""){
	//	$("input[xname='system_banner_title']").textbox('setValue', $("input[xname='system_login_title']").val());
	//}
	
	var _param = [];
	$("input[zokee='x-man']").each(function(i, item){
		var label = $.trim($(item).attr("xtext"));
		var name = $.trim($(item).attr("xname"));
		var value = $.trim($(item).val());
		var index = $(item).attr("i");
		
		_param.push({
			"label" : label,
			"name" : name,
			"value" : value,
			"i" : index
		});
	});
	
	var _url = $.fn.getRootPath() + "/app/auther/system!addSystemParam.htm";
	$.post(_url, {"json" : JSON.stringify(_param)}, function(data){
		parent.layer.alert(data.message, -1);
	}, "json");
}

var uploadImage = function(){
	var imgFieldName = arguments[0];
	var imgName =  $("#"+imgFieldName).val();
	if (imgName==""){
		$(parent.document).hint("请选择文件");
		return false;
	}else{
		if(imgName.toLocaleLowerCase().match("^.+\\.(jpg|png|bmp|ico|jpeg|gif)$")==null) {
			parent.layer.alert("目前只支持jpg|png|bmp|ico|jpeg|gif等格式的图片", -1);
			return false;
		}
	}
	
	//var imgPath = $("input[name='"+imgFieldName+"']").filebox('getValue');
	//alert(imgPath);
	//$("input[name='system_shortcut_icon']")
	//$.blockUI({message: "请稍候"});
	var index = parent.layer.load("请稍候...", 0);
	var _url = $.fn.getRootPath() + "/app/auther/system!uploadImg.htm";
	$.ajaxFileUpload({
		url : _url,
		secureuri : false,
		fileElementId : imgFieldName,
		data : {"imgFieldName": imgFieldName, "imgName": imgName},
		dataType : 'json',
		timeout : 5000,
		success : function(data, status) {
			if (data.success) {
				$(parent.document).hint("上传成功");
			} else {
				$(parent.document).hint(data.message);
			}
			parent.layer.close(index);
		},
		error : function(data, status, e){// 服务器响应失败处理函数
			$(parent.document).hint("处理超时,请重新上传!");
			parent.layer.close(index);
		}
	});
};

var deleteImage = function(){
	var imgFieldName = arguments[0];
	parent.layer.confirm("确定删除吗", function(){
		$.post($.fn.getRootPath() + "/app/auther/system!deleteImg.htm", {"imgFieldName": imgFieldName}, function(data){
			$(parent.document).hint(data.message);
			$("#"+imgFieldName).nextAll("a").last().hide();
		}, "json");
	});
}