/**
 * 用户管理
 * 
 * @author Gassol.Bi
 * @date Jul 15, 2016 10:18:07 AM
 */
$(function(){
	var that = window.UserInitPage = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	/*导入、导出——提示标题, 处理结果, 提示标题前缀, 提示标题后缀*/
	var intervalTitle = null, interval = null,
		prefix = '', suffix = '';
	/*工具方法*/
	fn.extend({
		/*相关数据*/
		datas:{
			datas: {},
			set:function(key, data){ this.datas[key] = data; },
			get:function(key){ return this.datas[key]; }
		},
		/*无弹窗*/
		ajax: function(options){
			var url = options.url, params = options.params, 
				onBefore = options.onBefore, onSuccess = options.onSuccess;
			/*事件不存在，或者事件存在但是返回true*/
			if(!onBefore || (onBefore && onBefore.call(that))){
				var loadindex = layer.load('提交中…');
				$.ajax({
					type : 'post',
					url : $.fn.getRootPath() + url, 
					data : params,
					async: false,
					dataType : 'json',
					success : function(jsonResult){
						layer.close(loadindex);
						if(onSuccess != null)
							onSuccess.call(that, jsonResult);
					},
					error : function(msg){
						layer.close(loadindex);
						layer.alert('网络错误，请稍后再试!', -1);
					}
				});
			}
		},
		/*弹出框打开*/
		openShade: function (shadeId){
			var w = ($('body').width() - $('#'+shadeId).width())/2;
			$('#'+shadeId).css('left',w+'px');
			$('#'+shadeId).css('top','0px');
			$('body').showShade();
			$('#'+shadeId).show();					
		},
		/*弹出框关闭*/
		closeShade: function (shadeId){
			$('#'+shadeId).css("display","none");
			$('body').hideShade();			
		},
		/*导入/导出*/
		importOrExport: {
			title: function(){//提示标题
				if(suffix.length == 4) suffix = '';
				$('#importWarning_title').html('<b>' + prefix + suffix + '<b/>');
				suffix += '*';
			},
			/*导入导出结束初始化*/
			init: function (){
				if(interval != null) clearInterval(interval);
				if(intervalTitle != null) clearInterval(intervalTitle);
				$('#userImportShadeClose').bind('click',function(){
					fn.closeShade('userImportShade');
				});
			},
			/*导入、导出消息提示*/
			execute: function (type){
				if(type == 'import'){
					prefix = '导入中'
				}else if(type == 'export'){
					prefix = '导出中'
				}else{
					return false;
				}
				/*文件手动导出按钮*/
				var fileDownBtn = $('#fileDownBtn');
				if(fileDownBtn) $(fileDownBtn).remove();
				//显示消息提示区
				$('#scBefore').css('display', 'none');
				$('#importWarning').css('display', 'block');
				$('#scAfter').css('display', 'none');
				$('#importHandle').css('display', 'none');
				$('#rowTotal').html(0);
				$('#fuccessTotal').html(0);
				$('#failureTotal').html(0);
				$('#userImportShadeClose').unbind('click');//去掉关闭事件
				//提示标题
				intervalTitle = setInterval('UserInitPage.fn.importOrExport.title()', 500);
				//提示结果
			   	interval = setInterval(function(){
			   		$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/auther/user!importUserResult.htm', 
						data : {'type':type},
						async: false,
						dataType : 'json',
						success : function(data){
							$('#rowTotal').html(data.rowTotal);
							$('#fuccessTotal').html(data.fuccessTotal);
							$('#failureTotal').html(data.failureTotal);
							if(data.result==true){/*导出、入结束*/ 
								fn.importOrExport.init();
								/*渲染文件手动导出、入按钮*/
								var filePath = data.result_FilePath;
								if(filePath) {
									$('#failureTotal').parent('p').
										after('<a id="fileDownBtn" style="color: blue" href="' 
											+ filePath + '">点击下载</a>');
								}
							}
						},
						error : function(msg){
							fn.importOrExport.init();
							parent.layer.alert('网络错误，请稍后再试!', -1);
						}
					});
			   	},500);
			}
		}
	});
	
	/*功能方法*/
	that.extend({
		/*数据导出*/
		exportUser: function (status){
			//打开提示框
			$("#title_importOrExport").html("批量导出");
			fn.openShade("userImportShade");
			
			//导出消息提示
			fn.importOrExport.execute('export');
			// 把status参数改层optype,避免和查询条件中的status冲突。modify by alan.zhang at 2016-04-06
			//$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!exportUser.htm?status="+status);
			$("#form0").attr("action", $.fn.getRootPath() + "/app/auther/user!exportUser.htm?optype="+status);
			$("#form0").submit();
		},
		/*数据导入*/
		importUser: function (){
			if($("#userFile").val()==null||$("#userFile").val()==''){
				parent.layer.alert("请选择需要上传的文件!", -1);
				return false;
			}
			
			//导入消息提示
			$("#title_importOrExport").html("批量导入");
			fn.importOrExport.execute('import');
			
			//数据导入
			$.ajaxFileUpload({
		        url: $.fn.getRootPath() + '/app/auther/user!importUser.htm',
		        secureuri: false,
		        fileElement: $('#userFile'),
		        uploadFileParamName : 'userFile',
		        success: function(data, status) {
		        	$('#scBefore').css('display','none');
					$('#importWarning').css('display','none');
					$('#scAfter').css('display','block');
					$('#importHandle').css('display','block');
					
					data = eval('(' + $(data).text() + ')');
				   	$('#scAfter p').html(data.returnResult);
		        	fn.importOrExport.init();
		        },
		        error: function(obj, msg, e) {
		        	fn.importOrExport.init();
		        	parent.layer.alert('网络错误，请稍后再试!', -1);
		        }
		    });
		},
		/*分页跳转*/
		pageClick: function (pageNo){
			$('body').ajaxLoading('正在查询数据...');
			$("#form0").attr("action", $.fn.getRootPath() + "/app/auther/user!initPage.htm");
			$("#pageNo").val(pageNo);
			$("#form0").submit();
		},
		/*帐号启用/停用*/
		accountNumber: function(status){
			var msg = "停用"
			if(status=="0"){
				//0表示启用，1表示停用
				msg = "启用"
			}
						
			var userId_list = $("input:checkbox[name='userId_list']:checked");
			if(userId_list.length<1){
				parent.layer.alert("请选择要"+msg+"的账号!", -1);
				return;
			}
			
			var bl = true;
			var userIds = "";
			$(userId_list).each(function(){
				var a =  $(this).next().val();
				if(a==status){
					bl = false;
				}else{
					userIds += $(this).val()+",";
				}					
				
			});

			if (bl && userId_list!=""){
				parent.layer.confirm("确定要"+msg+"选中的账号吗？", function(){
					$('body').ajaxLoading('正在保存数据...');
					$.post($.fn.getRootPath()+"/app/auther/user!accountNumber.htm",
						{'userIds':userIds,'status':status},
						function(data){
							if(data=="1"){
								parent.layer.alert("操作成功!", -1);					
								$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!initPage.htm");
								$("#form0").submit();
							}else{
								parent.layer.alert("操作失败!", -1);
								$('body').ajaxLoadEnd();
							}
							
					},"html");
				});
			}else{
				parent.layer.alert("已有账号被"+msg+"!", -1);
			}
			
		},
		/*密码重置*/
		passwordReset: function (){	
			var bl = false;
			var userIds = "";
			$("input:checkbox[name='userId_list']:checked").each(function(){
				bl = true;
				userIds += $(this).val()+",";
			});
			if(!bl){
				parent.layer.alert("请选择要重置密码的账户!", -1);
				return;
			}
			if (bl){
				parent.layer.confirm("确定重置选中账户的密码吗？", function(){
					$('body').ajaxLoading('正在保存数据...');
					$.post($.fn.getRootPath()+"/app/auther/user!passwordReset.htm",
						{'userIds':userIds},
						function(data){
							if(data=="1"){
								parent.layer.alert("操作成功!", -1);					
								$("#form0").attr("action", $.fn.getRootPath()+"/app/auther/user!initPage.htm");
								$("#form0").submit();
							}else{
								parent.layer.alert("操作失败!", -1);
								$('body').ajaxLoadEnd();
							}
					},"html");
				});
			}
		},	
		/*查询条件初始化*/
		userSelectReset: function (){
			$("input[id$='_select']").each(function(){
				$(this).attr("value","");
			});
			$("select[id$='_select']").each(function(){
				$(this)[0].selectedIndex = '';
			});
			$('#depIds_select').combotree('setValue', '');
		},
		/*全选*/
		checkAll: function (){
			var checked = $('input[type="checkbox"][name="ck_all"]').attr('checked');
			if(checked == 'checked')
				$('input[type="checkbox"][name="userId_list"]').attr('checked', checked);
			else $('input[type="checkbox"][name="userId_list"]').removeAttr('checked', checked);
		},
		/*修复部门的bh*/
		repairBH: function(){
			PageUtils.ajax({
				url: '/app/auther/dept!repairBH.htm',
				after: function(jsonResult) {
					$.messager.alert('信息', jsonResult.msg);
				}
			});
		},
		/*初始化*/
		init: function(){
			//新增用户
			$("#userAdd").click(function(){
				PageUtils.dialog.open('#dd_save', {
		    		title: '新增', width: '600', height: $(window).height()*0.98,
		    		url: '/app/auther/user!addOrEditUserTo.htm',
		    	});
			});
			
			//编辑用户
			$("#userEdit").click(function(){
				var userId_list = $("input:checkbox[name='userId_list']:checked")
				if(userId_list.length==0){
					parent.layer.alert("请选择要编辑的数据!", -1);
					return;
				}else if(userId_list.length>1){
					parent.layer.alert('不能同时编辑多条数据!', -1);
					return;
				}else{
					var userId = $(userId_list[0]).val();
					PageUtils.dialog.open('#dd_save', {
			    		params: {'userId': userId},
			    		title: '编辑', width: '600', height: $(window).height()*0.98,
			    		url: '/app/auther/user!addOrEditUserTo.htm'
			    	});
				}									
			});			
			
			//批量导入
			$("#userImport").click(function(){
			 	$('#scBefore').css("display","block");
				$('#scAfter').css("display","none");
				$('#importWarning').css("display","none");
				$('#importHandle').css("display","block");
				fn.openShade("userImportShade");
			});
			
			//批量导入弹出窗关闭
			$('#userImportShadeClose').click(
				function(){fn.closeShade("userImportShade");
			});
			
			//删除用户
			$("#userDel").click(function(){
				var bl = false;
				var userIds = "";
				$("input:checkbox[name='userId_list']:checked").each(function(){
					bl = true;
					userIds += $(this).val()+",";
				});
				if(!bl){
					parent.layer.alert("请选择要删除的数据!", -1);
					return;
				}
				if (bl){
					parent.layer.confirm("确定要删除选中的数据吗？", function(){
						$('body').ajaxLoading('正在保存数据...');
						$.post($.fn.getRootPath()+"/app/auther/user!delUser.htm",
							{'userIds':userIds},
							function(data){
								if(data=="1"){
									parent.layer.alert("操作成功!", -1);					
									that.pageClick('1');
								}else{
									parent.layer.alert("操作失败!", -1);
									$('body').ajaxLoadEnd();
								}
						},"html");
					});
				}
			});
		},
		set: function(params){
			/*部门查询数据回显*/
			var mapDepts = {}, rangDeptNodes = PageUtils.fn.treeData(params.rangDepts);
			$(rangDeptNodes).each(function(){ mapDepts[this.id] = this.children; });
			fn.datas.set('rangDeptNodes', mapDepts);
		}
	});
	/*初始化*/
	that.init();
});

