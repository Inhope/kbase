//知识反馈
function workFlowKnowledgeFeedback(objId){
	var navigateJObj = $('#categoryNv').children("a").eq(-1);
	if(navigateJObj != null){
		var objName = $(navigateJObj).text();
		/*
			parent.TABOBJECT.open({
					id : 'knowledgeFeedback',
					name : '知识反馈',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/auther/work-flow!knowledgeFeedback.htm?objId='+objId+"&objName="+objName,
					isRefresh : true
			}, this);			
		*/
		
		var workFlowShadeCotent = $('body').children('div[id="workFlowShadeCotent"]');
		if($(workFlowShadeCotent).length<1){
			$('body').append("<div id=\"workFlowShadeCotent\"></div>");
		}
		
		$.post(
			$.fn.getRootPath() + '/app/auther/work-flow!knowledgeFeedback.htm',
			{'objId':objId,'objName':objName},
			function (data){
				$("#workFlowShadeCotent").html(data);
				openShade("workFlowShade");			
			},
			"html"
		);		
	}	
}

//弹出框打开
function openShade(shadeId){
	var w = ($('body').width() - $('#'+shadeId).width())/2;
	$('#'+shadeId).css('left',w+'px');
	$('body').showShade();
	$('#'+shadeId).show();
}
			
//弹出框关闭
function closeShade(shadeId){
	$('#'+shadeId).hide();
	$('body').hideShade();
}
