$(function() {
	
	!window.SeniorSearchForFull || window.SeniorSearchForFull();
	!window.SeniorSearchForObject || window.SeniorSearchForObject();
	!window.SeniorSearchForValue || window.SeniorSearchForValue();
	
	//渲染表单事件
	var timeObject = {
		startDate : $('#startDate'),
		endDate : $('#endDate'),
		startDateUi : null,
		endDateUi : null,
		dateSetting : {
			required : false,
			closeText : '关闭',
			currentText : '今天',
			editable : false,
			formatter :function(e){
				var year = e.getFullYear();
				var month = e.getMonth()+1;
				if(month <= 9){
					month = '0' + month;
				}
				var date = e.getDate();
				if(date <= 9){
					date = '0' + date;
				}
				return year+'-'+month+'-'+date;
			}
		},
		forIe6 : function() {
			this.startDate.next().find('input').parent().parent().css({
				'position' : 'relative'
			});
			
			this.startDate.next().find('input').next().css({
				'position' : 'absolute',
				'top' : '2px'
			});

			this.endDate.next().find('input').css({
				'border' : '0',
				'width' : '103px'
			}).parent().parent().css({
				'position' : 'relative'
			});
			
			this.endDate.next().find('input').next().css({
				'position' : 'absolute',
				'top' : '2px'
			});
		},
		render : function() {
			this.startDate.click(function(){
				WdatePicker({readOnly:true});
			});
			this.endDate.click(function(){
				WdatePicker({readOnly:true});
			});
//			this.startDate.datebox(this.dateSetting);
//			this.startDateUi = this.startDate.next().find('input:eq(0)');
//			this.endDate.datebox(this.dateSetting);
//			this.endDateUi = this.endDate.next().find('input:eq(0)');
//			this.forIe6();
		}
	}
	
	timeObject.render();
	
	fromObject = {
		form : $('form'),
		contentEl : $('#content'),
		submitBtn : $('div.content_right_bottom input[type=button].an1'),
		resetBtn : $('div.content_right_bottom input[type=reset]'),
		disabled : function() {
			var self = this;
			$('input[name=searchType]').click(function() {
				if($(this).val() == '3') {
					$('#agingType').attr('disabled', 'disabled');
					$('#catgoryDirId').attr('disabled', 'disabled');
					// modify by baidengke 2016年5月19日 附件加上维度搜索条件
					$('#location').attr('disabled', false);
					$('#brand').attr('disabled', false);
				} else {
					$('#agingType').removeAttr('disabled');
					$('#catgoryDirId').removeAttr('disabled');
					$('#location').removeAttr('disabled');
					$('#brand').removeAttr('disabled');
				}
			});
		},
		validation : function () {
			var startDateValue = timeObject.startDate.val();
			var endDateValue = timeObject.endDate.val();
			if(startDateValue && endDateValue) {
				var s = new Date(startDateValue.replace(/-/gi, '/')).getTime();
				var e = new Date(endDateValue.replace(/-/gi, '/')).getTime();
				if(e <= s) {
					parent.$.fn.hint('起始日期必须小于结束日期')
					return false;
				}
			}
			if(!this.contentEl.val()) {
				parent.$.fn.hint('请填写搜索内容');
				return false;
			}
			return true;
		},
		setFromValue : function() {
			this.contentEl.val(this.contentEl.prev().val());
		},
		submitEvent : function() {
			var self = this;
			this.submitBtn.click(function(e, start, limit, filed, order,appendContent) {
				if(self.validation()) {
					var action = self.form.attr('action');
					if(start && limit) {
						filed = filed ? filed : '';
						order = order ? order : '';
						appendContent = appendContent ? appendContent : '';
						self.form.attr('action', action + '?start=' + start + '&limit=' + limit + '&orderFiled=' + filed + '&desc=' + order);
					}
						
					window.setTimeout(function(){
						self.form.submit();
					}, 1);
					$(document).ajaxLoading('正在加载数据...');
				}
				return false;
			});
		},
		init : function() {
			this.setFromValue();
			this.submitEvent();
			this.disabled();
			this.resetBtn.click(function(){
				timeObject.startDate.val('');
				timeObject.startDate.attr('value','');
				timeObject.endDate.val('');
				timeObject.endDate.attr('value','');
//				timeObject.startDate.datebox('setValue','');
//				timeObject.endDate.datebox('setValue','');
				$('#agingType').val('');
				$('#catgoryDirId').val('');
				$('#location').val('');
				$('#brand').val('');
			});
		}
	}
	fromObject.init();
	
	var fenye = $('div.gonggao_con_nr_fenye a').click(function() {
		
		
		var start = $(this).attr('start');
		var limit = $(this).attr('limit');
		
		if(start && limit) {
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $(this).parent().attr('logInfo');
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.page({
					pageNo:(start/limit) + 1,
					pageTotalNo:logInfo[1],
					touchContext:olog.constants.search
				});
			}
			
			fromObject.submitBtn.trigger('click', [start, limit]);
		}
	});
	setCurrentIframeHeight();
	
});

var setCurrentIframeHeight = function(){
	var _iframe = $(parent.document).find('div.content_content iframe:visible');
	_iframe.height($('body').height());
}