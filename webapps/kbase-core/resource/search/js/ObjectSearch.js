$(function(){
	/*************绑定页面事件*************/
	bindEven();
	/****************显示答案*******************/
	showAns();
	/***************预览*******************/
	yulan();
	/****************详情****************/
	detail();	
	
	/****************对比实例相关实例*******************/
	onto();
	
	/***********实例历史版本*************/
	window.ObjectVersion();
	/****************原文*******************/
	yuanwen();
	
	/************搜索对比实例*************/
	window.ObjectSearchForComparison();
	/****************收藏知识*************/
	window.FavBall();
	/************@lvan.li 20160308 业务模版按钮事件**/
	yewumoban();
});

/**
 * 绑定页面元素事件
 * 
 * @author baidengke
 * @date 2016年3月29日
 */
function bindEven(){
	//签读按钮
	$("a[name='signl']").click(function(){
		var curO = this;
		layer.confirm("确定解读",function(index){
			var logInfo = $(curO).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.sign({
					contentId:logInfo[0],
					content:$("<div></div>").append(logInfo[1]).text(),
					categoryId:logInfo[2],
					sequence:logInfo[3]
				});
			}
			layer.close(index);
		});
	});
}

function showAns(){
	$('h1 a').click(function(){
		var ans = $(this).parent().parent().next();
		if(ans.is(":hidden")){
			ans.show();
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $(this).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.qaTitle({
					qaId:logInfo[1],
					qaName:$("<div></div>").append(logInfo[2]).text(),
					sequence:logInfo[0],
					categoryId:logInfo[3],
					objectId:logInfo[4],
					touchContext:olog.constants.browse
				});
			}
		}else{
			ans.hide();
		}
	});
}

function yulan(){
	$('div.slzs_title_right input').attr('checked',false);
	
	$('div.slzs_title_right a').click(function(){
		if(!$(this).next().attr('checked')){
			$(this).next().attr('checked',true);
			$('h6 a').parent().parent().next().show();
		}else{
			$(this).next().attr('checked',false);
			$('h6 a').parent().parent().next().hide();
		}
	});
	$('div.slzs_title_right input').click(function(){
		if($(this).attr('checked')){
			$('h6 a').parent().parent().next().show();
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $(this).attr("logInfo");
			if (logInfo) {
				logInfo = logInfo.split("|#|");
				olog.qaPreview({
					categoryId:logInfo[0],
					objectId:logInfo[1],
					touchContext:olog.constants.browse
				});
			}
		}else{
			$('h6 a').parent().parent().next().hide();
		}
	});
}


function detail(){
	$('a.detail').click(function(){
		var question = $(this).attr('href');
		var questionId = $(this).attr('qid');
		var _url = '';
		if (parent.SystemKeys.isWukong == 1){
			_url = $.fn.getRootPath() + '/app/wukong/search!detail.htm?id=' + questionId;
		}else{
			_url = $.fn.getRootPath()+"/app/search/search.htm?searchMode=4&askContent="+encodeURIComponent(question);
		}
		window.location.href = _url;
		
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $(this).attr("logInfo");
		if (logInfo) {
			logInfo = logInfo.split("|#|");
			olog.qaDetail({
				qaId : logInfo[1],
				qaName : logInfo[2],
				sequence : logInfo[0],
				categoryId:logInfo[3],
				objectId: $("<div></div>").append(logInfo[4]).text(),
				touchContext:olog.constants.browse
			});
		}
		return false;
	});
}


/************请求action获取category路径***************/
function navigate_init(robot_context_path,objId){
	var data = {
		'format':'json','async':true,'jsoncallback':'navigateCb','nodeid':objId,'nodetype':3
	};
	var url = robot_context_path+'p4pages/related-category.action'+'?format=json&async=true&nodeid='+objId+'&ts='
			+ new Date().getTime()+'&jsoncallback=navigateCb&nodetype=2';
	var script = document.createElement('script');  
	script.setAttribute('src', url);  
	document.getElementsByTagName('head')[0].appendChild(script);
}
window.navigateCb = function(data) {
var tempPathforinstance='';
	if(data){
		var navigateJObj = $('#categoryNv');
		for(var i=0;i<data.length;i++){
			var aJObj;
			var dataName = data[i]['name'];
			//长度超过一定长度截取
			if(dataName.length > 10){
				dataName = dataName.substring(0,10)+ '...';
			}
			if(i == data.length-1){
				aJObj = $('<a/>').attr({'href':'javascript:void(0);','title':data[i]['name']}).html(dataName).css('cursor','default');
				navigateJObj.append(aJObj);
			}else{
				aJObj = $('<a/>').attr({'href':'javascript:void(0);','title':data[i]['name']}).attr('objId',data[i]['id']).html(dataName);
				navigateJObj.append(aJObj);
			}
		}
		navigateJObj.children('a').click(function(){
			openOv($(this).attr('objId'), $(this).attr('title'));
		});
	}
}

function openOv(categoryId, categoryName){
	if(!categoryName) categoryName = '分类展示';
	if(categoryId){
		window.parent.selectNodeById('',categoryId);
		parent.TABOBJECT.open({
			id : 'categoryTree',
			//name : '实例列表',
			name : categoryName,
			hasClose : true,
			url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId,
			isRefresh : true
		}, this);
	}
}

function onto(){
	$('div.slzs_right_title ul li').mouseover(function(){
		if(! $(this).hasClass('dq')){
			$('div.slzs_right_title ul li').not($(this)).removeClass('dq');
			$(this).addClass('dq');
			$('div.slzs_right_con ul').each(function(){
				if($(this).is(':hidden')){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		}
	});
	
	$('div.slzs_right_con>ul:eq(0)>div:eq(0)>li>a').click(function(){
		var objId = $(this).attr('objId');
		parent.TABOBJECT.open({
			id : 'sldb',
			name : '实例对比',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=8&objId='+objId,
			isRefresh : true
		}, this);
	});
	
	//add by eko.zhan at 2016-0-20 11:45 
	//傻逼页面，我很生气，麻痹一个onclick事件写了这个多个js，jsp里include的页面居然也包含了大量的js，还include个毛线，找个代码费时费力
	$('div.slzs_right_con a[objid][objid!=""]').click(function(){
		var objId = $(this).attr('objId');
		parent.TABOBJECT.open({
			id : 'sldb',
			name : $(this).attr('title'),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=8&objId='+objId,
			isRefresh : true
		}, this);
	});
	
	$('li.slzs_anniu a').each(function(index){
		if(index==0){
			$(this).click(function(){
				var oids = new Array();
				oids.push(objId);
				
				$('div.slzs_right_con>ul:eq(0)>div:eq(0) li>input').each(function(i){
					if($(this).attr('checked')=='checked'){
						oids.push($(this).next().attr('objId'));
					}
				});
				if(oids.length < 2){
					parent.$(document).hint("至少要选择一个实例");
					return ;
				}else if(oids.length > 5){
					parent.$(document).hint("最多只能选择四个实例");
					return ;
				}else{
					parent.TABOBJECT.open({
						id : 'sldb',
						name : '实例对比',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids='+oids,
						isRefresh : true
					}, this);
				}
			});
		}
	});
	
	
}


//原文
function yuanwen(){
	var flag = true;
	$.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
	   		async: false,
	   		dataType : 'json',
	   		success: function(msg){
	   			if(msg.length==0){
//	   				$(document).hint('没有原文');
	   				$('div.gonggao_titile_right a:eq(0)').html('没有原文').css({
//   					'backgroundColor' : '#cccccc',
   					'backgroundImage' : 'url(' + $.fn.getRootPath() + '/resource/search/images/gonggao_bg1.jpg)',
//   					'color' : '#cccccc',
   					'cursor' : 'default'
   				});
	   			}else{
	   				flag=false;
	   			}
	   		}
		});
		if(!flag){
			$('div.gonggao_titile_right a:eq(0)').click(function(){
				getFileTree(objId);
			});
			$('#closeYuanwen').click(function(){
				closeFileTreeDiv();
			});
		}
}
function getFileTree(objId){
	var flag = true;
	$.ajax({
   		type: "POST",
   		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
   		async: false,
   		dataType : 'json',
   		success: function(msg){
   			if(msg.length==0){
   				$(document).hint('没有原文');
   			}else{
   				flag=false;
   			}
   		}
	});
	if(flag){
		return;
	}
	
	var fileTreeSetting = {
	view: {
			expandSpeed: ""//空不显示动画
	},
	async : {
		enable : true,
		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
		autoParam : ["id", "name=n", "level=lv","bh"],
		otherParam : {}
	},
	callback : {
		onClick : fileClick
	}
};
	
	
	$.fn.zTree.init($("#fileTree"), fileTreeSetting);
	$('#fileTree').height('165px');
	$('#fileTree').parent().height('190px');
	$('#fileTree').parent().css('overflow','auto');
	var w = ($('body').width() - $('div.gonggao_d').width())/2;
	$('div.gonggao_d').css('left',w+'px');
	$('body').showShade();
	$('div.gonggao_d').show();
	
}

function fileClick(event, treeId, treeNode, clickFlag){
	if(treeNode.level==1){
		window.open(searchCtxPath+"attachmentDown?attachmentId="+treeNode.id);
	}
}

function yewumoban(){
	$('#yewumoban').click(function(){
					objName = (objName==undefined || objName=='')?'分类展示':objName;
					url = $.fn.getRootPath() + '/app/template/template.htm?objId='+objId;
					parent.TABOBJECT.open({
						id : objId+'_template',
						name : objName,
						hasClose : true,
						url : url,
						isRefresh : true
					}, this);
	});
}

function closeFileTreeDiv(){
	$('body').hideShade();
	$('div.gonggao_d').hide();
}

function openP4(url){
	window.open(url+'?'+new Date().getTime());
	return false;
}
