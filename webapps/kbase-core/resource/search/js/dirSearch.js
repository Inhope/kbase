$(function() {
	fenye();
	$('div.dangqian_1 a').click(function() {
		var categoryId = $(this).attr('categoryId');
		var cname = $(this).text();
		 cname = cname.replace(/<\/?[^>]*>/gim,"");//去掉所有的html标记
         cname = cname.replace(/(^\s+)|(\s+$)/g,"");//去掉前后空格
		openOv(categoryId,cname);
		//add by baidengke 2016年3月27日
		olog.category({
			categoryId:categoryId
		});
	});
});


function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
	var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
	var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
	var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
		
	});
	function fenyeSubmit(start){
		$('#fenyeForm input[name="isSearch"]').val('n');
		$('#fenyeForm input[name="start"]').val(start);
		$('#fenyeForm').submit();
	}
}


function openOv(categoryId,cname) {
	if (categoryId) {
		try {
			var node = window.parent.$('#categoryTree')
					.tree('find', categoryId);
			window.parent.$('#categoryTree').tree('expand', node.target);
		} catch (e) {
			window.parent.selectNodeById(categoryId, categoryId);
		}

		cname = (cname == undefined || cname == '')	? '页面展示': cname;
		window.parent.TABOBJECT.open({
					id : categoryId,
					name : cname,
					hasClose : true,
					url : $.fn.getRootPath()
							+ '/app/object/ontology-object.htm?id='
							+ categoryId,
					isRefresh : true
				}, this);
	}
}