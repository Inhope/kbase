/**
 * 故障树（图）-拓展
 * @auther Gassol.Bi
 * @since 2016-03-16 17:28
 * @description
 */
$(function(){
	var i = 0; var flag;/*编辑模式or查看模式*/
	var that = window.FaultGraphEx = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	if (!mxClient.isBrowserSupported()) {
		mxUtils.error('Browser is not supported!', 200, false);
	} else {
		/*容器*/
		var container = document.createElement('div');/*图形背景设置*/
		container.style.position = 'absolute';
		container.style.left = '0px';
		container.style.top = '0px';
		container.style.right = '0px';
		container.style.bottom = '0px';
		container.style.overflow = 'auto';
		container.style.width = '100%';
		container.style.height = '100%';
		container.style.background = 'rgba(0, 0, 0, 0) url("' 
			+ mxClient.imageBasePath + '/grid.gif") repeat scroll 0 0 ';
		mxEvent.disableContextMenu(container);
		document.body.appendChild(container);/*渲染*/
				
		/*图形*/
		var graph = new mxGraph(container);/*在容器中创建图形*/
		graph.graphHandler.scaleGrid = true;
		graph.setTooltips(!mxClient.IS_TOUCH);
		graph.panningHandler.ignoreCell = true;
		graph.container.style.cursor = 'move';
		
		/*布局*/
		var layout = new mxCompactTreeLayout(graph, false);
		layout.useBoundingBox = false;
		layout.edgeRouting = false;
		layout.levelDistance = 50;
		layout.nodeDistance = 10;
		layout.isVertexMovable = function(cell) { 
			return true; 
		};
		var layoutMgr = new mxLayoutManager(graph);
		layoutMgr.getLayout = function(cell) {
			if (cell.getChildCount() > 0) {
				return layout;
			}
		};
		
		/*节点*/
	    var style = graph.getStylesheet().getDefaultVertexStyle();
		style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE;
		style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_MIDDLE;/*内容垂直居中*/
		style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT;/*内容左对齐*/
		style[mxConstants.STYLE_FONTCOLOR] = '#1d258f';
		style[mxConstants.STYLE_ROUNDED] = '1';/*边框边角弧度*/
		style[mxConstants.STYLE_FONTSIZE] = 10;
		style[mxConstants.STYLE_WHITE_SPACE] = 'wrap';/*内容自动换行*/
		graph.getStylesheet().putCellStyle('process', style);
		
		style = mxUtils.clone(style);
		style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RHOMBUS;
		style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RhombusPerimeter;
		style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;/*内容居中*/
		graph.getStylesheet().putCellStyle('condition', style);
		
		style = mxUtils.clone(style);
		style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_ELLIPSE;
		style[mxConstants.STYLE_PERIMETER] = mxPerimeter.EllipsePerimeter;
		graph.getStylesheet().putCellStyle('end', style);
						
	    /*线*/
	    style = graph.getStylesheet().getDefaultEdgeStyle();
		style[mxConstants.STYLE_ROUNDED] = true;
		style[mxConstants.STYLE_ENTRY_X] = 0.5; // center
		style[mxConstants.STYLE_ENTRY_Y] = 0; // top
		style[mxConstants.STYLE_ENTRY_PERIMETER] = 0; // disabled
		style[mxConstants.STYLE_EXIT_X] = 0.5; // center
		style[mxConstants.STYLE_EXIT_Y] = 1; // bottom
		style[mxConstants.STYLE_EXIT_PERIMETER] = 0; // disabled
		style[mxConstants.STYLE_EDGE] = mxEdgeStyle.TopToBottom;	
		
		/*折叠功能设置*/
		graph.foldingEnabled = true;/*是否允许折叠*/
		graph.collapsedImage = new mxImage(mxClient.imageBasePath + '/collapsed.gif', 9, 9);
		graph.expandedImage = new mxImage(mxClient.imageBasePath + '/expanded.gif', 9, 9);
		graph.isCellFoldable = function(cell){/*定义那些按钮可以折叠*/
			return this.model.getOutgoingEdges(cell).length > 0;
		};
		graph.cellRenderer.getControlBounds = function(state){/*折叠节点渲染*/
			if (state.control != null) {
				var oldScale = state.control.scale;
				var w = state.control.bounds.width / oldScale;
				var h = state.control.bounds.height / oldScale;
				var s = state.view.scale;
				/* state为节点（(state.x, state.y)左上角坐标， 
				 *	(state.width, state.height)节点宽高）
				 
				 return new mxRectangle(state.x + state.width + w / 2 * s + TreeNodeShape.prototype.segment*s,
					state.y + state.height / 2 - h / 2 * s,
					w * s, h * s);
				*/
				return new mxRectangle(state.x + state.width / 2 - w / 2 * s,
										state.y + state.height + 10,
										w * s, h * s);
			}
			return null;
		};
		function toggleSubtree (graph, cell, show){/*折叠节点影藏*/
			show = (show != null) ? show : true;
			var cells = [];
			graph.traverse(cell, true, function(vertex) {
				if (vertex != cell) {
					cells.push(vertex);
				}
				return vertex == cell || !graph.isCellCollapsed(vertex);
			});
			graph.toggleCells(show, cells, true);
		}
		graph.foldCells = function(collapse, recurse, cells) {/*重写折叠方法*/
			this.model.beginUpdate();
			try {
				for(var i in cells){
					var cell = cells[i];
					toggleSubtree(this, cell, !collapse);
					this.model.setCollapsed(cell, collapse);
					layout.execute(this.getDefaultParent());
					
					recurse && this.foldCells(collapse, recurse, this.getChildVertices(cell));
				}
			} finally {
				this.model.endUpdate();
			}
		};
		
		/*其他设置*/
		graph.htmlLabels = true;/*Enables HTML labels as wrapping is only available for those*/
		graph.setCellsDisconnectable(false);/*是否允许节点断开链接*/
		graph.setCellsSelectable(true);/*是否允许节点选中*/
		graph.setAutoSizeCells(true);/*编辑后节点自动伸缩*/
		graph.setPanning(true);/*是否开启面板功能*/
		graph.panningHandler.useLeftButtonForPanning = true;/*允许左键拖曳面板（需开启setPanning(true)）*/
		var oldGetPreferredSizeForCell = graph.getPreferredSizeForCell;
		graph.getPreferredSizeForCell = function(cell) {
			/*单元格宽度调整*/ 
			/*
			var result = oldGetPreferredSizeForCell.apply(this, arguments);
			if (result != null) {
				//result.width = Math.max(180, result.width);
				result.width = 180;
			}
			return result;
			*/
		};
		/*图形选择*/
		graph.getSelectionModel().addListener(mxEvent.CHANGE, 
			function(sender, evt){
				var div = '#graphCellEditDIV';
				if(flag == 'show') div = '#graphCellShowDIV';
				/*选中的节点*/
				var cell = graph.getSelectionCell();
				if(cell != null && cell.isVertex()){
					/*相关参数*/
					var cellId = cell.getId(), 
						cellName = cell.getValue();/*节点id*/
					var treeId = _GRAPH_ID;/*树id*/
					var parent = fn.handle.getParent(cell),
						cellPId = parent != null ? parent.getId() : '';/*父节点id*/
					var eventContent, req, reqId, linkId, linkTreeId, linkdesc,
						attachment, attachmentName;
					
					/*顶点不允许查看
					if(cellId　== _GRAPH_ROOT_ID) { 
						$(div).hide();
						return false;
					} */
					
					/*获取数据*/
					$('body').ajaxLoading('正在查询数据...');
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath() + '/app/search/fault-graph!graphCellEdit.htm', 
						data : {'vertexId': cellId, 'treeId': treeId, 'vertexPId': cellPId},
						async: false,
						dataType : 'json',
						success : function(data){
							if(data.rst){
								eventContent = data.eventContent;
								req = data.req;
								reqId = data.reqId;
								attachment = data.attachment;
								attachmentName = data.attachmentName;
								linkdesc = data.linkdesc;
								var link = data.link;
								if (link) {
									link = link.split('\|');
									linkTreeId = link[0];
									linkId = link[1];
								}
								/*显示编辑区*/
								$(div).window('open');
							} else {
								mxUtils.alert(data.msg);
							}
							$('body').ajaxLoadEnd();
						},
						error : function(msg){
							mxUtils.alert('网络错误，请稍后再试!');
							$('body').ajaxLoadEnd();
						}
					});
					/*初始化编辑区*/
					$('#cellId').val(cellId);
					$('#cellName').textbox('setText', cellName);
					$('#treeId').val(treeId);
					$('#eventContent').textbox('setText', eventContent);
					$('#attachment').textbox('setText', attachmentName);
					$('#cc').textbox('setText', req);
					$('#ct').textbox('disableValidation');/*禁用验证*/
					$('#ct').textbox('setText', linkdesc);
					$('#ct').textbox('enableValidation');/*启用验证*/
					if(flag == 'show'){
						$('#cellName').val(cellName);
						/*附件*/
						$('#download').remove();
						if(attachmentName) {
							$('<a id="download" target="_blank" href="' +  $('#swf_address').val()
									+ 'filename=' + attachment + '&dir=faultTree&realname=' + attachmentName + '">'
									+ '下载</a>').appendTo('#attachmentTd');
						}
						$('#link').remove();
						if(linkdesc){
							$('<a id="link" href="javascript:void(0);" onclick="">'
								+ '查看</a>').appendTo('#ctTd');
							$('#link').click(function(){
								FaultGraphEx.fn.handle.linkShow(graph, linkId, linkTreeId, linkdesc);
							});
						}
					} else {
						$('#cellPId').val(cellPId);/*当前节点父节点id*/
						$('#req').val(req);
						$('#reqId').val(reqId);
						/*节点关联*/
						$('#linkdesc').val(linkdesc);
						$('#linkId').val(linkId);
						$('#linkTreeId').val(linkTreeId);
					}
				} else {
					/*$(div).hide();*/
				}
		});
		/*右键菜单*/
		graph.panningHandler.factoryMethod = function createPopupMenu(menu, cell, evt){
			fn.setting.menu(flag, menu, cell, evt);
		};
		
		/*主方法*/
		that.extend({
			/*详情*/
			show: function(options){
				/*查看模式*/
				flag = 'show';
				
				/*图形设置*/
				graph.setCellsResizable(false);/*是否允许节点调整大小*/
				graph.setCellsEditable(false);/*是否允许节点编辑*/
				
				/*双击事件*/
				graph.dblClick = function(evt, cell){
					if (cell != null) {
						if(cell.isVertex())
							fn.handle.show(graph, cell);
						else 
							graph.startEditingAtCell(cell);
					}
				};
				
				/*重写单元格标签内容发生改变（详情页面不允许修改）*/
				graph.cellLabelChanged = function(cell, newValue, autoSize){};
				
				/*初始化图形数据*/
				var rst = false;/*错误返回xml*/
				graph.getModel().beginUpdate();
				try {
					var params = '?graphId=' + _GRAPH_ID 
						+ '&reqId=' + _PAGE_REQID;
					var xmlDoc = mxUtils.load($.fn.getRootPath() 
						+ '/app/search/fault-graph!getGraphXml.htm' + params).getXml();
					if(xmlDoc != null && xmlDoc.documentElement != null
						&& xmlDoc.documentElement.nodeName != 'parsererror'
						&& xmlDoc.documentElement.nodeName != 'html'){
						var node = xmlDoc.documentElement;
						var dec = new mxCodec(node.ownerDocument);
						dec.decode(node, graph.getModel());
						rst = true;/*有正确返回xml*/
					}
				} finally {
					graph.getModel().endUpdate();
					
					/*有正确返回xml*/
					if(rst){
						/*折叠所有节点
						graph.foldCells(true, true, fn.handle.getChilds(graph, 
						graph.getModel().getCell(_GRAPH_ROOT_ID)));*/
						
						/*高亮‘关联当前标准问’的节点*/
						if(_PAGE_REQID) {
							$.ajax({
								type : 'post',
								url : $.fn.getRootPath() + '/app/search/fault-graph!getFaultLink.htm', 
								data : {'graphId': _GRAPH_ID, 'reqId': _PAGE_REQID},
								async: false,
								dataType : 'json',
								success : function(jsonResult){
									if(jsonResult.rst){
										var data = jsonResult.data;
										if(data) {
											if(!(data instanceof Array))
												data = data.split(',');
											var nodes = new Array();
											$(data).each(function(){
												nodes.push(graph.getModel().getCell(this));
											});
											/*高亮节点*/
											fn.tracker.execute(nodes);
										}
									} else {
										mxUtils.alert(jsonResult.msg);
									}
								},
								error : function(msg){
									mxUtils.alert('网络错误，请稍后再试!');
								}
							});
						} else {
							/*节点检索*/
							fn.search.execute( _PAGE_SEARCH);
						}
					}
					/*修改图形高度
					$('body').find("svg:eq(0)").css("height", '99%;');*/
				}
			},
			/*编辑*/
			edit: function(options){
				/*编辑模式*/
				flag = 'edit';
				
				/*绑定单元格保存事件*/
				$('#graphCellSave').click(function(){
					/*提交表单*/
					$('#graphCellEdit').form('submit', {
				        url: $.fn.getRootPath() + '/app/search/fault-graph!graphCellSave.htm', 
				        onSubmit: function(param){
					    	return $(this).form('validate');
					    },
				        success:function(jsonResult){
				        	jsonResult = eval('(' + jsonResult + ')');
				        	$.messager.alert('信息', jsonResult.msg);
				        	if(jsonResult.rst) {
								/*修改节点*/
								graph.getModel().beginUpdate();
								try {
									/*当前节点*/
									var cell = graph.getModel().getCell($('#cellId').val());
									if(cellName != cell.getValue()) { /*改变节点内容*/
										var cellName = $('#cellName').textbox('getText');
										graph.cellLabelChanged(cell, cellName, true);
										graph.updateCellSize(cell);/*内容发生改变节点宽度调整*/
										/*清除历史高亮节点*/
										fn.tracker.clear();
										/*保存图形数据*/
										var enc = new mxCodec(mxUtils.createXmlDocument());
		  								var node = enc.encode(graph.getModel());
		  								var xml = encodeURIComponent(mxUtils.getXml(node));
		  								var params = '?graphId=' + _GRAPH_ID;
			  							$('body').ajaxLoading('正在保存数据...');
									    mxUtils.post($.fn.getRootPath() + '/app/search/fault-graph!saveGraphXml.htm' + params, 
									    	'xml=' + xml, function(req){
									    		$('body').ajaxLoadEnd();
									    });
									}
								} finally {
									graph.getModel().endUpdate();
								}
				        	}
				        }
				    });
				});
				
				/*图形设置*/
				graph.setCellsResizable(true);/*是否允许节点调整大小*/
				graph.setCellsEditable(true);/*是否允许节点编辑*/
				
				/*双击事件*/
				graph.dblClick = function(evt, cell){
					if (cell != null) {
						if(cell.isVertex())
							fn.handle.edit(graph, cell);
						else 
							graph.startEditingAtCell(cell);
					}
				};
				
				/*初始化图形数据*/
				graph.getModel().beginUpdate();
				try {
					var xmlDoc = mxUtils.load($.fn.getRootPath() 
						+ '/app/search/fault-graph!getGraphXml.htm?graphId=' + _GRAPH_ID).getXml();
					if(xmlDoc != null && xmlDoc.documentElement != null
						&& xmlDoc.documentElement.nodeName != 'parsererror'
						&& xmlDoc.documentElement.nodeName != 'html'){
						var node = xmlDoc.documentElement;
						var dec = new mxCodec(node.ownerDocument);
						dec.decode(node, graph.getModel());
					} else {
						var parent = graph.getDefaultParent();
						/*初始化根节点*/
						/*function insertVertex(parent, id, value, x, y, width, height, style, relative)*/
						var h = graph.container.offsetHeight;
						var w = graph.container.offsetWidth;
						var v = graph.insertVertex(parent, _GRAPH_ROOT_ID,
							_GRAPH_NAME, w/2 - 45, 20, 180, 40, 'process');
					}
				} finally {
					graph.getModel().endUpdate();
					/*修改图形高度*/
					$(container).find("svg:eq(0)").css("height", '99.4%;');
				}
			}
		});
		
		/*公共方法*/
		fn.extend({
			search: {/*检索*/
				cells: null,
				execute: function(searchCon){
					var root = graph.getModel().getCell(_GRAPH_ROOT_ID);/*根节点*/
					searchCon = searchCon ? searchCon.
						replace(/(^\s*)|(\s*$)/g, ''): searchCon;
					if(searchCon){
						var regExp = new RegExp('.*' + searchCon + '.*'),
							vertices = fn.handle.getChilds(root, 
								function(vertex){/*获取符合检索条件的节点*/
									return regExp.test(vertex.value.replace(/\s/g, ''));
								});
						/*高亮节点*/
						if(regExp.test(root.value.replace(/\s/g, '')))
							vertices.push(root);/*检索范围应当包含根节点*/
						fn.tracker.execute(vertices);
						
						/*展开已经折叠的节点*/
						$(vertices).each(function(){
							var cells = new Array(root), 
								cell = fn.handle.getParent(this);
							while(cell != null && cell.id != _GRAPH_ROOT_ID){
								cells.push(cell);
								cell = fn.handle.getParent(cell);
							}
							graph.foldCells(false, true, cells);
						});
					} else {
						graph.scrollCellToVisible(root, true);
					}
				}
			},
			tracker: {/*节点渲染*/
				cells: null,
				execute: function(cells_){
					var cells = this.cells;
					if(cells == null){
						/*初始化高亮节点集合*/
						cells = new Array();
					}
					/*节点移动*/
					graph.scrollCellToVisible(cells_[0], true);
					graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, '', cells);
					graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, '#00FF00', cells_);
					/*高亮的对象*/
					this.cells = cells_;
				},
				/*清除高亮节点背景色*/
				clear: function(){
					var cells = this.cells;
					if(cells != null){
						graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, '', cells);
						this.cells = cells;
					}
				}
			},
			/*相关设置*/
		    setting:{
			    /*右键菜单*/
			    menu: function(flag, menu, cell, evt){
			    	var model = graph.getModel();
					if (cell != null){
						if(flag == 'edit'){/*编辑模式*/
							if (model.isVertex(cell)){
								var style = cell.getStyle();
								style = mxUtils.getStylename(style);/*获取样式名称*/
								if(style != 'end'){
									var submenu1 = menu.addItem('添加', mxClient.imageBasePath + '/check.png');
									
									menu.addItem('事件', mxClient.imageBasePath + '/check.png', 
										function(){
											fn.handle.addChild(graph, cell, 'process');
									}, submenu1);
									
									menu.addItem('判断', mxClient.imageBasePath + '/check.png', 
										function(){
											fn.handle.addChild(graph, cell, 'condition');
									}, submenu1);
									
									menu.addItem('结束', mxClient.imageBasePath + '/check.png', 
										function(){
											fn.handle.addChild(graph, cell, 'end');
									}, submenu1);
								}
								/*
								menu.addItem('编辑', mxClient.imageBasePath + '/text.gif', 
									function(){
										//graph.startEditingAtCell(cell);
										fn.handle.edit(graph, cell);
								});
								*/
							}
							if (cell.id != _GRAPH_ROOT_ID && model.isVertex(cell)){
								menu.addItem('删除', mxClient.imageBasePath + '/delete.gif', 
									function(){
										fn.handle.deleteSubtree(graph, cell);
								});
							}
							menu.addSeparator();
						} 
						/*
						else {
							if(cell.getId()　!= _GRAPH_ROOT_ID) {
								menu.addItem('查看', mxClient.imageBasePath + '/text.gif', 
									function(){
										fn.handle.show(graph, cell);
								});
								menu.addSeparator();
							}
						}
						*/
						if(cell.getId()　!= _GRAPH_ROOT_ID) {/*父节点*/
							menu.addItem('父节点', mxClient.imageBasePath + '/zoom.gif', 
								function(){
									var cell = graph.getSelectionCell(),
										parent = fn.handle.getParent(cell);
								   	fn.tracker.execute([parent]);
							});
							if(flag == 'show'){
								menu.addItem('详情', mxClient.imageBasePath + '/zoom.gif', 
									function(){
										/*打开标准问详情*/
										var cell = graph.getSelectionCell(),
											title = cell.value,
											askContent = $('#cc').textbox('getText');
										$.ajax({
											type : 'post',
											url : $.fn.getRootPath() + '/app/search/fault-graph!getObject.htm', 
											data : {'ques': askContent},
											async: false,
											dataType : 'json',
											success : function(data){
												if(data.rst){
													var title = data.objname;
													parent.TABOBJECT.open({
														id : title,
														name : title,
														title : title,
														hasClose : true,
														url : $.fn.getRootPath() 
															+ '/app/search/search.htm?searchMode=3&askContent=' 
															+ encodeURI(askContent),
														isRefresh : true
													}, this);
												} else {
													mxUtils.alert(data.msg);
												}
											},
											error : function(msg){
												mxUtils.alert('网络错误，请稍后再试!');
											}
										});	
								});
							}
							menu.addSeparator();
						}
					}
					menu.addItem('全局', mxClient.imageBasePath + '/zoom.gif', 
						function(){
							graph.fit();
					});
					menu.addItem('局部', mxClient.imageBasePath + '/zoomactual.gif', 
						function(){
							graph.zoomActual();
							/*将选中的节点移动到中心*/
							var cell = graph.getSelectionCell();
							if(cell != null && cell.isVisible()) {
								/*新节点样式调整，及位置移动*/
								fn.tracker.execute([cell]);
							}
					});
					if (cell == null){
						if(flag == 'edit'){
							menu.addSeparator();
							menu.addItem('保存', mxClient.imageBasePath + '/save.gif', 
								function(){/*导出*/
									fn.tracker.clear();/*清除历史高亮节点*/
								    var enc = new mxCodec(mxUtils.createXmlDocument());
		  								var node = enc.encode(graph.getModel());
		  								var xml = encodeURIComponent(mxUtils.getXml(node));
		  								var params = '?graphId=' + _GRAPH_ID;
		  							$('body').ajaxLoading('正在保存数据...');
								    mxUtils.post($.fn.getRootPath() + '/app/search/fault-graph!saveGraphXml.htm' + params, 
								    	'xml=' + xml, function(req){
								    		$('body').ajaxLoadEnd();
								    });
							});
						}
					}
			    },
			    
			    /*工具栏*/
			    mxToolbar: function(content){
					var tb = new mxToolbar(content);
					tb.addItem('Zoom In', mxClient.imageBasePath + '/zoom_in32.png',function(evt) {
						graph.zoomIn();
					});
		
					tb.addItem('Zoom Out', mxClient.imageBasePath + '/zoom_out32.png',function(evt){
						graph.zoomOut();
					});
					
					tb.addItem('Actual Size', mxClient.imageBasePath + '/view_1_132.png',function(evt){
						graph.zoomActual();
					});
		
					tb.addItem('Print', mxClient.imageBasePath + '/print32.png',function(evt){
						var preview = new mxPrintPreview(graph, 1);
						preview.open();
					});
		
					tb.addItem('Poster Print', mxClient.imageBasePath + '/press32.png',function(evt){
						var pageCount = mxUtils.prompt('Enter maximum page count', '1');
						if (pageCount != null){
							var scale = mxUtils.getScaleForPageCount(pageCount, graph);
							var preview = new mxPrintPreview(graph, scale);
							preview.open();
						}
					});
				}
		    }
		});
	}
	
	/*公共方法*/
	fn.extend({
		/*节点操作*/
		handle: {
			/*添加节点上*/
			addChild: function (graph, cell, style) {
				var model = graph.getModel();
				var parent = graph.getDefaultParent();
				var vertex;
				
				model.beginUpdate();
				try {
					vertex = graph.insertVertex(parent, Math.uuid().replace(/-/g, '').toLowerCase(), 
						'child' + ++i, 0, 0, 180, 40, style);
					graph.insertEdge(parent, null, '', cell, vertex);
				} finally {
					model.endUpdate();
				}
				/*新节点样式调整，及位置移动*/
				fn.tracker.execute([vertex]);
			},
			/*删除节点*/
			deleteSubtree: function (graph, cell) {
				/*Gets the subtree from cell downwards*/
				var cells = [];
				graph.traverse(cell, true, function(vertex){
					cells.push(vertex);
					return true;
				});
				graph.removeCells(cells);
			},
			
			/*父节点*/
			getParent: function(cell){
				var parent;
				for (var i = 0; i < cell.getEdgeCount(); i++) {
					var edge = cell.getEdgeAt(i);
					if (edge.source != cell) {
						parent = edge.source;
						break;
					}
				}
				return parent;
			},
			/*所有上级节点*/
			getParents: function(cell){
				// 所有上级节点
				var parents = new Array();
				// 获取上级节点
				var parent = this.getParentCell(cell);
				while(parent != null){
					parents.push(parent);
					// 获取上级节点
					parent = this.getParentCell(parent);
				}
				return parents;
			},
			
			/*子节点*/
			getChild: function(parent, filter){
				// 下级节点
				var childs = new Array();
				// 获取下级节点
				for (var i = 0; i < parent.getEdgeCount(); i++) {
					var edge = parent.getEdgeAt(i), 
						vertex = edge.target;
					if (vertex != parent) 
					if (filter == null || (filter != null
						 && filter(vertex))) 
						childs.push(vertex);
				}
				return childs;
			},
			/*所有下级节点*/
			getChilds: function(parent, filter){
				// 下级节点
				var childs = new Array();
				var recurse = {
					execute: function (parent, filter) {
						/*当前子节点*/ 
						var _that = this,
						child = fn.handle.getChild(parent);/*所有子节点*/
						if (filter != null) {/*子节点过滤*/
							$(child).each(function(i, vertex){
								if(filter(vertex)) {
									childs.push(vertex);
								}
							});
						} else {/*子节点不过滤*/
							childs = childs.concat(child);/*当前子节点*/ 
						}
						$(child).each(function(){
							_that.execute(this, filter);
						});
					}
				};
				/*执行递归*/
				recurse.execute(parent, filter);
				return childs;
			},
			
			/*节点查看*/
			linkShow: function(graph, linkId, linkTreeId, linkdesc){
				/*打开关联节点*/
				var iframe_content = '<iframe name="dd-tree-choose-iframe" frameborder="0" src="url" style="width:100%;height:99%;"></iframe>';
				var url = $.fn.getRootPath() + '/app/search/fault-graph!graphShow.htm';
	        		url += '?searchContent=' + encodeURI(linkdesc.split('->')[1]) 
						+ '&graphId=' + linkTreeId;
				var content = iframe_content.replace(/url/g, url);
				$('#dd-tree-choose').dialog({
				    title: '节点关联',
				    width: document.body.clientWidth*0.95,
				    height: document.body.clientHeight*0.85,
				    closed: false,
				    cache: false,
				    content: content,
				    modal: true
				});    
				$('#dd-tree-choose').dialog('open');
				
				/*
				var xmlDoc = mxUtils.load($.fn.getRootPath() 
					+ '/app/search/fault-graph!getGraphXml.htm?graphId=' + linkTreeId).getXml();
				if(xmlDoc != null && xmlDoc.documentElement != null
					&& xmlDoc.documentElement.nodeName != 'parsererror'
					&& xmlDoc.documentElement.nodeName != 'html'){
					
					var sgraph = new mxGraph();
					var node = xmlDoc.documentElement;
					var dec = new mxCodec(node.ownerDocument);
					dec.decode(node, sgraph.getModel());
					
					var source = sgraph.getModel().getCell(linkId);
					var cells = this.getChilds(sgraph, source);
					
					var parent = graph.getDefaultParent();
					var target = graph.getModel().getCell($('#id').val());
					graph.getModel().beginUpdate();
					try {
						var index = graph.model.getChildCount(target);
						graph.cellsAdded(cells, parent, index, source, target);
					} finally {
						graph.getModel().endUpdate();
					}
				}
				*/
			}
		}
	});
});