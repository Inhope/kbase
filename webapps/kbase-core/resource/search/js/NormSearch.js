$(function(){
	
	/***********************绑定页面事件*************************/
	bindEvent();
	/***********************分类展示*************************/
	categoryTag();
	/***********************历史版本*************************/
	getVersion();
	/***********************推荐答案*************************/
	suggest();
	/********************加深搜索内容*************************/
	changeColor();
	/***************加载分页********************/
	fenye();
	/******************p4**********************/
//	$('iframe').load(function(){
//		var height = $(this).contents().find("body").height();
//		console.log(height);
//		$(this).height(height);
//	});
	/****************显示答案*******************/
	showAns();
	/***************预览*******************/
	yulan();
	/****************排序*******************/
	order();
	/****************详情****************/
	detail();
	/******************二次搜索*********************/
		$('input.yiban_ss_anniu').parent().click(function(){
			var askMore = $(this).prev().val();
			$('#fenyeForm input[name="askMore"]').val(askMore);
			$('#fenyeForm input[name="searchMode"]').val('1');
			$('#fenyeForm').submit();
		});
		$("input.yiban_ss_anniu").parent().prev().keypress(function (e) {
			var askMore = $(this).val();
			if(e.keyCode==13){
				$('#fenyeForm input[name="askMore"]').val(askMore);
				$('#fenyeForm input[name="searchMode"]').val('1');
				$('#fenyeForm').submit();
			}
		});
		
});

/**
 * 绑定页面事件
 * @author baidengke
 * @date 2016年4月8日
 */
function bindEvent(){
	//签读按钮
	$("a[name='signl']").click(function(){
		var curO = this;
		parent.layer.confirm("确定解决",function(index){
			var logInfo = $(curO).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.sign({
					contentId:logInfo[0],
					content:$("<div></div>").append(logInfo[1]).text(),
					categoryId:logInfo[2],
					sequence:logInfo[3],
					pageNo:logInfo[4],
					pageSize:logInfo[5],
					objectId:logInfo[6]
				});
			}
			parent.layer.close(index);
		});
	});
}


function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
	var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
	var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
	var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
		
	});
	function fenyeSubmit(start){
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $("#pageTool").attr('logInfo');
		if(logInfo){
			logInfo = logInfo.split("|#|");
			olog.page({
				pageNo:(start/logInfo[0]) + 1,
				pageTotalNo:logInfo[1],
				touchContext:olog.constants.search
			});
		}
		
		$('#fenyeForm input[name="isSearch"]').val('n');
		$('#fenyeForm input[name="start"]').val(start);
		$('#fenyeForm').submit();
	}
}

function showAns(){
	$('h1 a').click(function(){
		var ans = $(this).parent().parent().next();
		if(ans.is(":hidden")){
			ans.show();
			setCurrentIframeHeight();
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $(this).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.qaTitle({
					sequence:logInfo[0],
					pageNo:logInfo[1],
					pageSize:logInfo[2],
					qaId:logInfo[3],
					qaName:$("<div></div>").append(logInfo[4]).text(),
					categoryId:logInfo[5],
					objectName:logInfo[6],
					objectId:logInfo[7],
					touchContext:olog.constants.search
				});
			}
		}else{
			ans.hide();
		}
	});
}

function yulan(){
	$('div.yiban_title_right input').attr('checked',false);
	
	$('div.yiban_title_right a').click(function(){
		if(!$(this).next().attr('checked')){
			$(this).next().attr('checked',true);
			$('div.dangqian_1 h6').parent().next().show();
			setCurrentIframeHeight();
			// add by baidengke 2016年3月28日 操作日志记录
			olog.qaPreview({
				touchContext:olog.constants.search
			});
		}else{
			$(this).next().attr('checked',false);
			$('div.dangqian_1 h6').parent().next().hide();
		}
	});
	$('div.yiban_title_right input').click(function(){
		if($(this).attr('checked')){
			$('div.dangqian_1 h6').parent().next().show();
			setCurrentIframeHeight();
			// add by baidengke 2016年3月28日 操作日志记录
			olog.qaPreview({
				touchContext:olog.constants.search
			});
		}else{
			$('div.dangqian_1 h6').parent().next().hide();
		}
	});
}

function order(){
	var asc = $('#fenyeForm input[name="asc"]').val();
	var order = $('#fenyeForm input[name="order"]').val();
	if(asc=='true'){
		if(order == 'createtime'){
			$('#createClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico3.jpg) no-repeat right center');
		}else if(order == 'edittime'){
			$('#editClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico3.jpg) no-repeat right center');
		}
	}else if(asc=='false'){
		if(order == 'createtime'){
			$('#createClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico1.jpg) no-repeat right center');
		}else if(order == 'edittime'){
			$('#editClick').css('background','url( '+$.fn.getRootPath()+'/resource/search/images/yiban_ico1.jpg) no-repeat right center');
		}
	}
	
	$('#createClick').click(function(){
		$('#fenyeForm  input[name="order"]').val('createtime');
		$('#fenyeForm').submit();
	});
	$('#editClick').click(function(){
		$('#fenyeForm  input[name="order"]').val('edittime');
		$('#fenyeForm').submit();
	});
	$('#dscount').click(function(){
		$('#fenyeForm  input[name="order"]').val('dscount');
		$('#fenyeForm').submit();
	});
	
}


function detail(){
	$('a.detail').click(function(){
		var question = $(this).attr('question');
		var questionId = $(this).attr('qid');
		var _url = '';
		if (parent.SystemKeys.isWukong == 1){
			_url = $.fn.getRootPath() + '/app/wukong/search!detail.htm?id=' + questionId;
		}else{
			_url = $.fn.getRootPath()+"/app/search/search.htm?searchMode=2&askContent="+encodeURIComponent(question);
		}
		
		parent.TABOBJECT.open({	 
			id : $(this).attr("title_"),
			name : $(this).attr("title_"),
			title : $(this).attr("title_"),
			hasClose : true,
			url : _url,
			isRefresh : true
		}, this);
		
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $(this).attr("logInfo");
		if (logInfo) {
			logInfo = logInfo.split("|#|");
			olog.qaDetail({
				sequence : logInfo[0],
				pageNo : logInfo[1],
				pageSize : logInfo[2],
				qaId : logInfo[3],
				qaName : $("<div></div>").append(logInfo[4]).text(),
				categoryId:logInfo[5],
				objectName:logInfo[6],
				objectId:logInfo[7],
				touchContext:olog.constants.search
			});
		}
		
		return false;
	});
}

function changeColor(){
	var ac = askContent.split("");
	var ac2 = $('input[name="askMore"]').val().split("");
	
	//Array.prototype.push.apply(ac,ac2);
	ac = ac.concat(ac2);
	ac = ac.toString();
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]")
    var rs = "";
	for (var i = 0; i < ac.length; i++) {
	    rs = rs + ac.substr(i, 1).replace(pattern, '');
	}
	ac = rs;
	
	var regString = '';
	regString = ac.split('').join('|');
	/*
	for(var i=0;i<ac.length;i++){
		if(i != 0 ){
			regString += '|';
		}
		regString += ac[i];
	}*/
	var listTextEl = $('h1 a');
	listTextEl.each(function(index) {
//		$(this).html($(this).html().replace(new RegExp('('+regString+')', 'ig'), '<font color="red">$1</font>'));
	});
	
	$('h1 a').each(function(){
		if($(this).attr('title'))
			$(this).attr('title',$(this).attr('title').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
	});
	$('a.detail').each(function(){
		if($(this).attr('question'))
			$(this).attr('question',$(this).attr('question').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
	});
	$('highlight').attr('color','red');
	$('highlight').each(function(){
		if($(this).parent().html())
			$(this).parent().html($(this).parent().html().replace(new RegExp('highlight', 'ig'), 'font'));
	});
}


function suggest(){
	$('div.yiban_right a').click(function(){
		var question = $(this).attr('title');
		$.post($.fn.getRootPath()+"/app/search/senior-search!ask.htm",
			{'content':question},function(result){
				result = eval('(' + result + ')');
				if(result.result){
					parent.TABOBJECT.open({
						id : result.objectName,
						name : result.objectName,
						title : result.objectName,
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=6&askContent="+encodeURIComponent(question),
						isRefresh : true
					}, this);
				}else{
					//一般搜索
					parent.TABOBJECT.open({
						id : 'ordinary_s',
						name : '搜索',
						title : question,
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=6&askContent="+encodeURIComponent(question),
						isRefresh : true
					}, this);
				}
		});
		//window.location.href = $.fn.getRootPath()+"/app/search/search.htm?searchMode=6&askContent="+question;
		return false;
	});
}

var setCurrentIframeHeight = function(){
	
	var _iframe = $(parent.document).find('div.content_content iframe:visible');
	_iframe.height($('body').height());
}