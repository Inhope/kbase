ObjectVersion = function() {
	var versionListObject = {
		listPanel : $('div.index_chu'),
		dataTable : $('div.index_dan_con ul li.biaoge>table'),
		isShow : function() {
			return !this.listPanel.is(':hidden');
		},
		versionBtn : $('div.gonggao_titile_right a:eq(1)'),
		closeBtn : $('div.index_d div.index_dan_title a'),
		compBtn : $('div.index_d div.xgai a.tijiao input[type=button]'),
		closePanel : function() {
			var self = this;
			this.closeBtn.click(function() {
				self.listPanel.hide();
				$(document).hideShade();
			});
		},
		renderComp : function(versionIds) {
			var self = this;
			//objId : '02cd8473bc894a0390e54c33f5225cd3',//window.objId,
			//versionIds : versionIds
//			var goUrl = $.fn.getRootPath() + '/app/search/object-search!comparisonObject.htm?objId=02cd8473bc894a0390e54c33f5225cd3&versionIds=' + versionIds;
//			window.showModalDialog(goUrl, null, "dialogHeight: 768px; dialogWidth: 1024px; status:no; scroll:yes;resizable:yes;");
				
			parent.TABOBJECT.open({
				id : 'comparison_object',
				name : '实例对比',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/object-search!comparisonObject.htm?objId=' + window.objId + '&versionIds=' + versionIds,
				isRefresh : true
			}, this);
		},
		render : function() {
			var self = this;
			this.versionBtn.click(function() {
				if(self.dataTable.find('tr').size() == 1) {
//					parent.layer.alert('当前实例还没有产生历史版本!', -1)
					return false;
				}
				if(!self.isShow()) {
					$(document).showShade();
					self.listPanel.css({
						'top' : '50px',
						'left' : ($(window).width() - self.listPanel.width()) / 2 + 'px',
						'zIndex' : '100010'
					}).show();
				}
			});
			
			this.closePanel();
			
			this.compBtn.click(function() {
				var checkVserion = $('div.index_d input[type=checkbox]:checked');
				if(checkVserion.size() == 0) {
					parent.layer.alert('请选择比对项', -1)
				} else if(checkVserion.size() == 2) {
					var versionIds = new Array(),bl = true;
					$.each(checkVserion, function(i) {
						if($("#"+$(this).attr('id')+"_1").length>0){
							parent.layer.alert('不应选择已经屏蔽的数据!', -1);
							return bl = false;
						}
						versionIds.push($(this).attr('id') + ';' + $(this).attr('index'));
					});
					if(bl)self.renderComp(versionIds.join(','));
				} else {
					parent.layer.alert('只能请选择两项进行对比', -1)
				}
			});
		}
	};
	
	versionListObject.render();

};