var AttachmentINDEX;

var success_jsonpCallback = function(s,id){
	//modify by eko.zhan at 2015-01-06
	//s = s.substring(s.indexOf('/')+1,s.length);
	//s = swfAddress.substring(0,swfAddress.indexOf('attachment')) + s;
//	console.log(s);
	var fp = new FlexPaperViewerEmbedding( 'swf'+id, {
		config : {
			SwfFile : escape(s),
			Scale : 1,
			ZoomTransition : 'easeOut',
			ZoomTime : 0.5,
			ZoomInterval : 0.2,
			FitPageOnLoad : false,
			FitWidthOnLoad : true,
			FullScreenAsMaxWindow : false,
			ProgressiveLoading : true,
			MinZoomSize : 0.2,
			MaxZoomSize : 5,
			SearchMatchAll : false,
			InitViewMode : 'Portrait',
			PrintPaperAsBitmap : false,
			ViewModeToolsVisible : true,
			ZoomToolsVisible : true,
			NavToolsVisible : true,
			CursorToolsVisible : true,
			SearchToolsVisible : true,

			localeChain : 'zh_CN'
		}
	});
	$('#openAttachment'+id).show();	
}

/*
 * @modify by eko.zhan at 2014-09-09
 * 增加参数attName， 传入文件原始名称，根据后缀名解析是否为swf文件，如果是，采用其他方法预览
 * @modify by eko.zhan at 2015-01-06
 * 附件预览多文件支持
 */
var openAttachment = function(id,index,fileName, attName) {
	if(!$('#openAttachment'+id).is(":hidden") && AttachmentINDEX==index){
		closeAttachment(id);
		return;
	}
	 AttachmentINDEX=index;
	
	 $(".attachmentIndex").html('');
	 $(".attachmentIndex").hide();
	 
	 
	if (typeof(attName)!=undefined && attName!=null){
		var _srcSuffix = attName.substring(attName.lastIndexOf("."));
		if (attName!=undefined && attName!=null && fileName.substring(fileName.lastIndexOf("."))!=_srcSuffix){
			fileName += _srcSuffix;
		}
		$.getJSON(swfAddress+'?format=json&async=true&fileName='+fileName+'&id='+id+'&callbackparam=?', function(data){
			//返回 ?('cmb-swf/DATA/swf/1386059508843/20120406134111530.swf','e2cc06a7b9de42f19c4369d5fc0ea0d3')
			//@fixme data返回的值是第一个参数，如何拿到第二个参数，当然，在这里用不到第二个参数
			//第一个参数而swf文件地址，直接采用jquery.flash.js插件播放
			if (data==null || data=="null") return false;	//  cmb-swf/DATAS/swf/1402024414531/20150104162245840.swf
			
			$('#openAttachment'+id).html('<a id="swf'+id+'" style="width:750px;height:480px;display:block"></a>');
			
			var _url = data;	//cmb-swf/DATA/swf/1402024414531/201501071554346240.jpg
			_url = _url.substring(_url.indexOf('/')+1, _url.length);
			_url = swfAddress.substring(0, swfAddress.lastIndexOf('attachment')) + _url;
			
			var _destSuffix = data.substring(data.lastIndexOf(".")+1);
			if (_srcSuffix==".swf"){
				$('#swf'+id).flash({ 
					src : _url,
					height: 480,
					width: 750,
					version: 8 
				});
			}else if (_destSuffix=="swf"){
				success_jsonpCallback(_url, id);
			}else if (/(png|jpg|gif|ico)$/.test(_destSuffix)){
				$('#openAttachment'+id).html('<iframe style="height:480px;width:750px;" src="' + _url + '"></iframe>');
				/*$('#openAttachment'+id).html("<a href=\"" + _url + "\" title=\"Flashlight\" class=\"lightsGal\"><img src=\"" + _url + "\" alt=\"Flashlight\" style=\"max-width:750px;max-height:480px;\"/></a>");
				try{
					$('a.lightsGal').zoomimage();
				}catch(e){
					$('#openAttachment'+id).html('<iframe style="height:480px;width:750px;" src="' + _url + '"></iframe>');
				}*/
			}else if (/(html)$/.test(_destSuffix)){
				$('#openAttachment'+id).html('<iframe style="height:480px;width:750px;" src="' + _url + '"></iframe>');
			}else if (_destSuffix=="flv"){
				return false;
			}
			
			$('#openAttachment'+id).show();
		});
		return;
	}
}


var closeAttachment = function(id){
	$('#openAttachment'+id).html('');
	$('#openAttachment'+id).hide();
}



/*
//html显示
var openAttachment = function(id,index,fileName, attName) {
	if(!$('#openAttachment'+id).is(":hidden") && AttachmentINDEX==index){
		closeAttachment(id);
		return;
	}
	 AttachmentINDEX=index;
	
	 $(".attachmentIndex").html('');
	 $(".attachmentIndex").hide();
	 
	 
//	$('#openAttachment'+id)
//			.html('<a id="swf'+id+'" style="width:680px;height:480px;display:block"></a>');
	
	if (typeof(attName)!=undefined && attName!=null){
		$.getJSON(swfAddress+'?format=json&async=true&fileName='+fileName+'.doc&id='+fileName+'&callbackparam=?', function(data){
			//返回 ?('cmb-swf/DATA/swf/1386059508843/20120406134111530.swf','e2cc06a7b9de42f19c4369d5fc0ea0d3')
			//@fixme data返回的值是第一个参数，如何拿到第二个参数，当然，在这里用不到第二个参数
			//第一个参数而swf文件地址，直接采用jquery.flash.js插件播放
			if (data==null || data=="null") return false;
			var s = data;
			s = s.substring(s.indexOf('/')+1,s.length);
			s = swfAddress.substring(0,swfAddress.indexOf('attachment')) + s;
			$('#openAttachment'+id).html('<iframe style="height:480px;width:780px;" src="'+s+'"></iframe>');
			$('#openAttachment'+id).show();
		});
		return;
	}
	
//	var url = swfAddress+'?format=json&async=true&fileName='+fileName+'&id='+id+'&callbackparam=success_jsonpCallback';
//	var script = document.createElement('script');  
//	script.setAttribute('src', url);  
//	document.getElementsByTagName('head')[0].appendChild(script);

	
}*/