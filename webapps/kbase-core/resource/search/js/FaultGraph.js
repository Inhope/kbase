/**
 * 故障树（图）
 * @auther Gassol.Bi
 * @since 2016-03-16 17:28
 * @required FaultGraph-ex.js
 * @description
 */
 
 $(function(){
	var that = window.FaultGraph = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*公共方法*/
	fn.extend({
		/*页签*/
		tab: {
			open: function(options){
				parent.TABOBJECT.open({
					id : '故障树-' + options.title,
					name : '故障树-' + options.title,
					title : options.title,
					hasClose : true,
					url : options.url,
					isRefresh : true
				}, this);
			}
		},
		/*弹窗*/
		shade: {
			open: function(options){
				var before = options.before;
				/*事件不存在，或者事件存在但是返回true*/
				if(!before || before && before.call(_that, options)){
					var shadeId = options.shadeId;
					var w = ($('body').width() - $('#' + shadeId).width())/2;
					$('#' + shadeId).css('left', w + 'px');
					$('#' + shadeId).css('top', '0px');
					$('body').showShade();
					$('#' + shadeId).css('display', '');
				}
			},
			close: function(shadeId){
				$('#'+shadeId).css('display', 'none');
				$('body').hideShade();
			}
		},
		/*联想控件初始化*/
		suggest: function(options){
			var dom = options.dom,  obj = $(dom), 
				url = $.fn.getRootPath()  + '/app/search/suggested.htm';
			$(obj).combobox({
			    url: url,
				valueField: 'id',
				textField: 'question',
				onBeforeLoad: function(param){/*刷新前，绑定查询条件*/
				},
				onShowPanel: function(){/*下拉时触发*/
					$(obj).combobox('reload', url);
				},
				onLoadSuccess: function(){/*刷新后，数据回显查询内容*/
				},
				keyHandler: {
					query: function(q){/*内容发生改变时刷新控件结果*/
						$(this).combobox('reload', url);
					}
				},
				onSelect: function (record){
					
				}
			});
		},
		/*节点检索*/
		search: {
			/*初始化*/
			init: function(){
				$('#ss').searchbox({
					width: '275px',
					searcher: function(searchCon){
						FaultGraphEx.fn.search.
							execute(searchCon);
					}, 
					prompt:'请输入检索内容...' 
				});
			}
		}
	});
	
	/*主方法*/
	that.extend({
		/*编辑页面*/
		edit: {
			init: function(){
				/*初始化详情mxgraph图*/
				FaultGraphEx.edit();
				/*节点检索*/
				fn.search.init();
			}
		},
		/*详情页面*/
		show: {
			init: function(){
				/*初始化详情mxgraph图*/
				FaultGraphEx.show();
				/*节点检索*/
				fn.search.init();
			}
		},
		/*初始化*/
		init: function(){
			/*按钮（打开故障树页面）-判断-是否应该显示*/
			var btns = $('a[name="faultTree"], a[id="faultTree"], input[name="faultTree"]');
			if(btns && btns.length > 0){
				$.ajax({/*标准问-判断-关联故障树并且返回相关数据*/
					type : 'post',
					url : $.fn.getRootPath() + '/app/search/fault-graph!getGraphSum.htm', 
					data : {'reqId': faqId},
					async: false,
					dataType : 'json',
					success : function(josnResult){
						if(josnResult.rst) {/*标准问-判断-成功*/
							var sum = josnResult.sum;
							if(sum > 0) {/*存在关联的标准问-显示影藏按钮、绑定相关事件*/
								$(btns).each(function(){
									$(this).css({display: ''}); /*显示已经影藏的按钮*/
									$(this).click(function(){/*绑定按钮点击事件*/
										/*相关数据*/
										var options = $(this).attr('options');
										options = eval('(' + options + ')');
										var reqId = options.params.reqId;/*标准问id*/
										
										if(sum == 1) {
											var treeId = josnResult.treeId, treeName = josnResult.treeName
											var url = $.fn.getRootPath() + '/app/search/fault-graph!graphShow.htm';
											url += '?reqId=' + reqId
												+ '&graphId=' + treeId;
											fn.tab.open({/*打开详情页面*/
												title: treeName,
												url: url
											});
										} else {
											/*data 数据加载（该属性自1.3.2版开始可用,但是当前页面使用是1.3.1版） 
											var rows = josnResult.data;*/
											$('#da-faultTree').datagrid({
											   	url: $.fn.getRootPath() + '/app/search/fault-graph!getGraphData.htm',
											   	queryParams: {reqId:reqId},
											   	rownumbers: true, fit: true,
											    columns:[[
											        {field:'name',title:'名称',width:200},
											        {field:'typeName',title:'类型',width:120}, 
											        {field:'userName',title:'创建人',width:120}, 
											        {field:'id', title:'操作', width:100, formatter: 
											        	function(value, row, index){
											        		var url = $.fn.getRootPath() + '/app/search/fault-graph!graphShow.htm';
											        		url += '?reqId=' + reqId
																+ '&graphId=' + row.id;
															return '<a href="javascript:void(0);"' 
															+ 'onclick="javascript:FaultGraph.fn.tab.open({title: \'' + row.name 
																+ '\', url: \''+ url + '\'});">打开</a>';
														}
											        }
											    ]]
											});
											/*打开弹窗*/
											$('#dg-faultTree').dialog({
												top: 20,
												iconCls:'icon-save', resizable:true, 
												closed: true, modal:true,
												width:600, height:300, 
												title: '故障树选择'
											});
											$('#dg-faultTree').dialog('open');
										}
									});
								});
							}
						} else {
							alert('操作异常!');
						}
					},
					error : function(msg){
						alert('网络错误，请稍后再试!');
					}
				});
			}
		}
	});
	/*初始化*/
	that.init();
});