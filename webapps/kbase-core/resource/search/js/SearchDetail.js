$(function(){
	/*************绑定页面事件*************/
	bindEven();
	/***************弹出p4**********************/
//	openP4(firstP4);
	/****************显示答案*******************/
	showAns();
	/***************预览*******************/
	yulan();
	/****************详情****************/
	detail();
	/****************知识关联对比实例相关实例*******************/
	onto();
	/******************解决未解决***********************/
	solved();
	unsolved();
	/***********实例历史版本*************/
	window.ObjectVersion();
	/************搜索对比实例*************/
	window.ObjectSearchForComparison();
	/****************收藏知识*************/
	window.FavBall();
	/****************原文*******************/
	yuanwen();
	/*************当前页面增加需要处理的一些脚本 add by eko.zhan at 2015-08-20 15:37 ******************/
	nomonhan ();
	/*************推荐功能 add by eko.zhan at 2015-10-10 17:51 ******************/
	recommend();
});

/**
 * 绑定页面元素事件
 * 
 * @author baidengke
 * @date 2016年3月29日
 */
function bindEven(){
	//签读按钮
	$("a[name='signl']").click(function(){
		var curO = this;
		parent.layer.confirm("确定解决",function(index){
			var logInfo = $(curO).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.sign({
					contentId:logInfo[0],
					content:$("<div></div>").append(logInfo[1]).text(),
					categoryId:logInfo[2]
				});
			}
			parent.layer.close(index);
		});
	});
	
	$("#logSign").click(function(){
		var curO = this;
		parent.layer.confirm("确定解决",function(index){
			var logInfo = $(curO).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				olog.sign({
					contentId:logInfo[0],
					content:$("<div></div>").append(logInfo[1]).text(),
					categoryId:logInfo[2],
					sequence:logInfo[3]
				});
			}
			parent.layer.close(index);
		});
	});
	
	
}

//评分分页
function pageClick(pageNo){
	var check=$("input[name='filter']:checked").val();
	$.ajax({
		url:$.fn.getRootPath()+"/app/corrections/corrections!getsatisfaction.htm",
		type:"post",
		data:{"pageNo":pageNo,'value_id':faqId,'satisfaction':check},
		timeout:5000,
		async:false,
		dataType:"html",
		success:function(data){
		    $("#fenye").remove();
			$("#satisfact_content").remove();
   			$("#satisfaction").append(data);
		}
	})		
}



function closesatisfaction(){
   $("#satisfact_content").remove();
   $('body').hideShade();
   $('#satisfaction').hide();
}

//评分选择操作
function changeinfo(type,name){
   $("#score").html(type);
   if(name!='满意'){
     $("#typelist").show();
   }else{
      $("input[name='type']").attr("checked",false);
      $("#typelist").hide();
   }
}

//过滤信息
function filterinfo(type){
   $.ajax({
	   		type: "POST",
	   		url:  $.fn.getRootPath() + '/app/corrections/corrections!getsatisfaction.htm',
	   		data:{'value_id':faqId,'satisfaction':type},
	   		async: true,
	   		dataType : 'html',
	   		success: function(data){
	   		    $("#fenye").remove();
	   		    $("#satisfact_content").remove();
	   			$("#satisfaction").append(data);
	   		},
	   		error :function(){
	   			parent.layer.alert('获取失败');
	   		}
	});
}

//评分提交
function satisfactionsubmit(){
   var params = $("#form1").serialize();
   if($("#RadioGroup1_0").val()!='满意' && $.trim($("#sat_content").val())==''){
      parent.layer.alert('请填写评价内容',-1);
      return;
   }else{
	   $.ajax({
	   		url:  $.fn.getRootPath() + '/app/corrections/corrections!save.htm?'+params,
	   		type: "POST",
	   		data:{'value_id':faqId, 
	   		      'askcontent':askContent, //标准问
	   		      'answer':askAnswer, //标准答
	   		      'platform':inner,//平台
	   		      'brand':brand,//品牌
	   		      'brandDesc':brandDesc,//品牌
	   		      'location':askLocation,//归属地
	   		      'locationDesc':askLocationDesc,//归属地
	   		      'categoryId':categoryId},
	   		async: true,
	   		dataType : 'json',
	   		success: function(data){
	   		　　parent.$(document).hint('评分成功',-1);
		  	   $("#average").html(data);
		  	   $('body').hideShade();
			   $('#satisfaction').hide();
	   		},
	   		error :function(){
	   			parent.layer.alert('提交失败');
	   		}
	   });
   }	   
}


function showAns(){
	$('div.dkfi_news p a').unbind('click');
//	$('div.dangqian_3 p a').unbind('click');// modify by baidengke 2016年3月29日 修改bug，点击事件被绑定多次
	$('div.dkfi_news p a').click(function(){
		$(this).parent().parent().removeClass('dkfi_news');
		$(this).parent().parent().addClass('dangqian_3');
		$(this).parent().parent().parent().addClass('dangqian');
		$('div.dangqian_3').next().show();
		hideAns();
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $(this).attr("logInfo");
		if(logInfo){
			logInfo = logInfo.split("|#|");
			olog.qaTitle({
				sequence:logInfo[0],
				qaId:logInfo[1],
				qaName: $("<div></div>").append(logInfo[2]).text(),
				categoryId:logInfo[3],
				objectId:logInfo[4],
				touchContext:olog.constants.browse
			});
		}
	});
//	hideAns();  // modify by baidengke 2016年3月29日 修改bug，点击事件被绑定多次
	function hideAns(){
		$('div.dangqian_3 p a').unbind('click');// modify by baidengke 2016年3月29日 修改bug，点击事件被绑定多次
		$('div.dangqian_3 p a').click(function(){
			$(this).parent().parent().addClass('dkfi_news');
			$(this).parent().parent().removeClass('dangqian_3');
			$(this).parent().parent().parent().removeClass('dangqian');
			$('div.dkfi_news').next().hide();
			showAns();
		});
	}
}


function yulan(){
	$('div.xiamian input').attr('checked',false);
	
	$('div.xiamian a').click(function(){
		if(!$(this).prev().attr('checked')){
			$(this).prev().attr('checked',true);
			
			$('div.dkfi_news').each(function(){
				$(this).addClass('dangqian_3');
				$(this).removeClass('dkfi_news');
				$(this).parent().addClass('dangqian');
				$(this).next().show();
			});
			
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $("#preview").attr("logInfo");
			if (logInfo) {
				logInfo = logInfo.split("|#|");
				olog.qaPreview({
					categoryId:logInfo[0],
					objectId:logInfo[1],
					touchContext:olog.constants.browse
				});
			}
		}else{
			$(this).prev().attr('checked',false);
			
			$('div.dangqian_3').each(function(){
				$(this).addClass('dkfi_news');
				$(this).removeClass('dangqian_3');
				$(this).parent().removeClass('dangqian');
				$(this).next().hide();
			});
		}
		showAns();
	});
	$('div.xiamian input').click(function(){
		if($(this).attr('checked')){
			//单击预览
			
			$('div.dkfi_news').each(function(){
				
				$(this).addClass('dangqian_3');
				$(this).removeClass('dkfi_news');
				$(this).parent().addClass('dangqian');
				$(this).next().show();
			});
			
			//modify by eko.zhan at 2016-01-13 00:07 预览增加高度只针对IE7
			var _iframe = $(parent.document).find('div.content_content iframe:visible');
			//_iframe.height(_iframe.contents().height());
			parent.setCurrentIframeHeight(_iframe);
			
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $("#preview").attr("logInfo");
			if (logInfo) {
				logInfo = logInfo.split("|#|");
				olog.qaPreview({
					categoryId:logInfo[0],
					objectId:logInfo[1],
					touchContext:olog.constants.browse
				});
			}
		}else{
			$('div.dangqian_3').each(function(){
				$(this).addClass('dkfi_news');
				$(this).removeClass('dangqian_3');
				$(this).parent().removeClass('dangqian');
				$(this).next().hide();
			});
		}
	});
}

function detail(){
	$('a.detail').click(function(){
		var question = $(this).attr('question');
		//modify by eko.zhan at 2014-10-23 ceair提出需求：知识展示点击明细在新的tab中打开，如需修改代码如下
		/*var _url = $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+question;
		parent.TABOBJECT.open({	 
			id : 'values',
			name : '知识展示',
			hasClose : true,
			url : _url,
			isRefresh : true
		}, this);*/
		window.location.href = $.fn.getRootPath()+"/app/search/search.htm?searchMode=8&askContent="+encodeURIComponent(question);
		// modify by baidengke 2016年3月29日 操作日志记录
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $(this).attr("logInfo");
		if (logInfo) {
			logInfo = logInfo.split("|#|");
			olog.qaDetail({
				sequence : logInfo[0],
				qaId : logInfo[1],
				qaName :  $("<div></div>").append(logInfo[2]).text(),
				categoryId:logInfo[3],
				objectId:logInfo[4],
				touchContext:olog.constants.browse
			});
		}
		return false;
	});
}


/************请求action获取category路径***************/
function navigate_init(robot_context_path,objId){
/*
	var data = {
		'format':'json','async':true,'jsoncallback':'navigateCb','nodeid':objId,'nodetype':3
	};
	var url = robot_context_path+'p4pages/related-category.action'+'?format=json&async=true&nodeid='+objId+'&ts='
			+ new Date().getTime()+'&jsoncallback=navigateCb&nodetype=2';
	var script = document.createElement('script');  
	script.setAttribute('src', url);  
	document.getElementsByTagName('head')[0].appendChild(script);
	*/
	//modify by eko.zhan at 2015-09-22 16:15
	var data = {'format':'json', 'async':true, 'nodeid':objId, 'nodetype':2};
	$.getJSON(robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
		if (arr!=null){
			$(arr).each(function(i, item){
				//name id type bh
				var name = item.name;
				if (name.length>10){
					name = name.substring(0, 10) + '...';
				}
				$('#categoryNv').append('<a href="javascript:void(0)" _id="' + item.id + '" title="' + item.name + '">' + name + '</a>');
			});
		}
	});
}
window.navigateCb = function(data) {
var tempPathforinstance='';
	if(data){
		var navigateJObj = $('#categoryNv');
		for(var i=0;i<data.length;i++){
			var aJObj;
			var nameJObj = data[i]['name'];
			//限制字数
			if(nameJObj.length >= 10){
				nameJObj = nameJObj.substring(0,10) + '...';
			}
			if(i == data.length-1){
				aJObj = $('<a/>').attr('href','javascript:void(0);').attr('objId',data[i]['id']).html(nameJObj);
				navigateJObj.append(aJObj);
			}else{
				aJObj = $('<a/>').attr('href','javascript:void(0);').attr('categoryId',data[i]['id']).html(nameJObj);
				navigateJObj.append(aJObj);
			}
		}
		navigateJObj.children('a').click(function(){
			if($(this).attr('objId')){
				openObj($(this).attr('objId'));
			}else if($(this).attr('categoryId')){
				openCategory($(this).attr('categoryId'));
			}
		});
	}
}

function openCategory(categoryId, cateName){
	cateName = cateName==undefined?'分类展示':cateName;
	window.parent.selectNodeById('',categoryId);
	parent.TABOBJECT.open({
		id : 'categoryTree',
		//name : '实例列表',
		name : cateName,
		hasClose : true,
		url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId,
		isRefresh : true
	}, this);
}

function openObj(objId){
	window.location.href = $.fn.getRootPath()+"/app/search/object-search.htm?searchMode=10&objId="+objId;
}


function onto(){
	$('div.slzs_right_title ul li').mouseover(function(){
		if(! $(this).hasClass('dq')){
			$('div.slzs_right_title ul li').not($(this)).removeClass('dq');
			$(this).addClass('dq');
			var index = $('div.slzs_right_title ul li').index($(this));
			$('div.slzs_right_con ul').each(function(i){
				if(i==index){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		}
	});
	
	$($('div.slzs_right_con ul')[0]).children('li').children('a').click(function(){
		var content = $(this).html();
//		$('input.kuang',window.parent.document).val(content);
//		window.location.href =  $.fn.getRootPath()+'/app/search/search.htm?askContent='+content;
//		parent.TABOBJECT.open({
//			id : 'zsgl',
//			name : '知识关联',
//			hasClose : true,
//			url : $.fn.getRootPath()+'/app/search/search.htm?searchMode=5&askContent='+content,
//			isRefresh : true
//		}, this);
		
		$.post($.fn.getRootPath()+"/app/search/senior-search!ask.htm",
			{'content':content},function(result){
				result = eval('(' + result + ')');
				if(result.result){
					parent.TABOBJECT.open({
						id : result.objectName,
						name : result.objectName,
						title : result.objectName,
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=5&askContent="+encodeURIComponent(content),
						isRefresh : true
					}, this);
				}else{
					//一般搜索
					parent.TABOBJECT.open({
						id : 'ordinary_s',
						name : '搜索',
						//之前这里是question，关联知识无法命中标准问走在这里会报错 question is undefined modify by eko.zhan at 2016-01-13 12:26
						title : content,
						hasClose : true,
						url : $.fn.getRootPath()+"/app/search/search.htm?searchMode=5&askContent="+encodeURIComponent(content),
						isRefresh : true
					}, this);
				}
		});
	});
	
	$('div.slzs_right_con>ul:eq(1)>div:eq(0)>li>a').click(function(){
		var objId = $(this).attr('objId');
		parent.TABOBJECT.open({
			id : 'sldb',
			name : '实例对比',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=8&objId='+objId,
			isRefresh : true
		}, this);
	});
	
	
	$($('div.slzs_right_con ul')[2]).children('li').children('a').click(function(){
		var objId = $(this).attr('objId');
		parent.TABOBJECT.open({
			id : 'slgl',
			name : '实例关联',
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=9&objId='+objId,
			isRefresh : true
		}, this);
	});
	
	$('li.slzs_anniu a').each(function(index){
		if(index==0){
			$(this).click(function(){
				var oids = new Array();
				oids.push(objId);
				$('div.slzs_right_con>ul:eq(1)>div:eq(0) li>input').each(function(i){
					if($(this).attr('checked')=='checked'){
						oids.push($(this).next().attr('objId'));
					}
				});
				if(oids.length < 2){
					parent.$(document).hint("至少要选择一个实例");
					return ;
				}else if(oids.length > 5){
					parent.$(document).hint("最多只能选择四个实例");
					return ;
				}else{
					parent.TABOBJECT.open({
						id : 'sldb',
						name : '实例对比',
						hasClose : true,
						url : $.fn.getRootPath() + '/app/comparison/business-contrast.htm?oids='+oids,
						isRefresh : true
					}, this);
				}
			});
		}
	});
	
}

function unsolved(){
	$('#unSolvedPanel div.unsolve_xinxi_dan_title a').click(function(){
		$('body').hideShade();
		$('#unSolvedPanel').hide();
	});
	$('#unSolvedPanel input.unsolve_xinxi_an2').parent().click(function(){
		$('#unSolvedPanel textarea').val('');
	});
	$('#unSolvedPanel input.unsolve_xinxi_an1').parent().click(function(){
		var reason = $('#content').val();
		var params=$("#corrform").serialize();
		//modify by eko.zhan at 2014-10-23 纠错原因不能为空
		if ($.trim(reason)==""){
			parent.layer.alert("请填写纠错原因", -1);
			return false;
		}
		
		else
		$.ajax({
	   		type: "POST",
	   		url : $.fn.getRootPath() + "/app/corrections/corrections!save.htm?"+params,
	   		async: false,
	   		dataType : 'json',
	   		success: function(msg){
	   			parent.parent.$(document).hint(msg);
	   		}
	   　});
	
		var url = robot_context_path+'faq-vote.action'+'?reason='+reason+'&userId='+human_id+"&option=2&async=true&faqId="+faqId+"&ts="
			+ new Date().getTime()+'&jsoncallback=cbSolved'+'&sessionId='+sessionUId+'&brand='+brand+'&platform='+inner+'&question='+askContent;
		var script = document.createElement('script');
		script.setAttribute('src', url);  
		document.getElementsByTagName('head')[0].appendChild(script);
		$('#unSolvedPanel textarea').val('');
		$('body').hideShade();
		$('#unSolvedPanel').hide();
		return false;
	});
}
function solved(){
	$('h5 a').each(function(index){
			index = index - 1;// modify by baidengke 2016年4月12日 由于前面加了一个解决按钮，所以下标-1
			var id = $(this).attr("id");
			if(index == 0){//签读
				if($(this).is('[id^="0"]')){
					//签读按钮
					var obj = $('a[id^="0"]:eq(0)');
					if(stime > 0){//签读时间（放置在SearchDetail.jsp上）
						//按钮初始化
						$(obj).css("cursor","default");//去掉a标签的光标变化
						$(obj).css("background-color","#00b0f0");//修改样式颜色
						$(obj).html("签读20");
						//定时器
						var interval = setInterval(function(){
							if(stime>0){
								$(obj).html("签读"+stime);
								stime--;
							}else{
								clearInterval(interval);//取消倒计时
								$(obj).css("cursor","pointer");//去掉a标签的光标变化
								$(obj).css("background-color","#67a909");//修改样式颜色
								$(obj).html("签读");
								$(obj).bind("click",function(){
									markread(obj);
								});
							}
						},1000);
					} else {//不做倒计时
						$(obj).bind("click",function(){
							markread(obj);
						});
					}
				}
			}else if(index == 1){//点赞
				$(this).click(function(){
					var url = robot_context_path+'faq-vote.action'+'?userId='+human_id+"&option=1&async=true&faqId="+faqId+"&ts="
						+ new Date().getTime()+'&jsoncallback=cbSolved'+'&sessionId='+sessionUId+'&brand='+brand+'&platform='+inner+'&question='+askContent;
					var script = document.createElement('script');
					script.setAttribute('src', url);  
					//load javascript  
					document.getElementsByTagName('head')[0].appendChild(script);
					return false;
				});
			}else if (index==2){//纠错
				$(this).click(function(){
					var _ques = $('#gzyd_sms_question').text().trim();
					var _answer = $('#gzyd_sms_answer').text().trim();
					/*
					var _data = {
						'objId': objId,
						'faqId': faqId,
						'cateId': categoryId,
						'question': _ques,
						'answer': _answer
					}*/
					$.kbase.errcorrect({
						'objId': objId,
						'faqId': faqId,
						'cateId': categoryId/*,
						'question': _ques,
						'answer': _answer*/
					});
					//var _params = "objId="+objId+"&faqId="+faqId+"&cateId="+categoryId+"&question="+askContent+"&answer="+askAnswer;
					/*
					var _params = $.param(_data);
	                var _url = $.fn.getRootPath() + "/app/corrections/corrections!send.htm?" + _params;
					parent.__kbs_layer_index = parent.$.layer({
						type: 2,
						border: [10, 0.3, '#000'],
						title: false,
						closeBtn : [0, true],
						iframe: {src : _url, scrolling: 'no'},
						area: ['832px', '600px']
					});
					*/
				/*	hidden by eko.zhan at 2015-08-18 11:55 纠错在多处调用，避免代码重复，弹出一个iframe层
					var w = ($('body').width() - $('#satisfaction').width())/2;
					$('#satisfaction').css('left',w+'px');
					$('body').showShade();
					$("#typelist").hide();
					$("#form1")[0].reset();
					$("#form2")[0].reset();
					$("#score").html($("input[name='satisfaction']:checked").val());
					$.ajax({
					   		type: "POST",
					   		url:  $.fn.getRootPath() + '/app/corrections/corrections!getsatisfaction.htm',
					   		data:{'value_id':faqId},
					   		async: true,
					   		dataType : 'html',
					   		success: function(data){
					   		    $("#fenye").remove();
					   		    $("#satisfact_content").remove();
					   			$("#satisfaction").append(data);
					   			$('#commcount').html($('#satisfactioncount').val());
					   		},
					   		error :function(){
					   			parent.layer.alert('获取失败');
					   		}
					});
					$('#satisfaction').show();
					*/
					//直接对接产品的未解决接口
	//				var url = robot_context_path+'faq-vote.action'+'?reason='+reason+'&userId='+human_id+"&option=2&async=true&faqId="+faqId+"&ts="
	//					+ new Date().getTime()+'&jsoncallback=cbSolved'+'&sessionId='+sessionUId+'&brand='+brand+'&platform='+inner+'&question='+askContent;
	//				var script = document.createElement('script');
	//				script.setAttribute('src', url);  
	//				//load javascript  
	//				document.getElementsByTagName('head')[0].appendChild(script);
	//				return false;	
				});
			}else if(id=="gzyd_flzs"){//分类展示
				$(this).click(function(){
					//modify by eko.zhan at 2015-10-09 知识详情页面单击分类展示按钮进入分类展示页面，Tab页签标题显示分类名称
					var tabName = $('#categoryNv a:last').attr('title');
					var tabId = $('#categoryNv a:last').attr('_id');
					tabName = (tabName==undefined || tabName=='')?'分类展示':tabName;
					url = $.fn.getRootPath() + '/app/category/category.htm?type=2&categoryId='+categoryId+'&categoryName='+tabName;
					parent.TABOBJECT.open({
						id : tabId,
						name : tabName,
						hasClose : true,
						url : url,
						isRefresh : true,
						forceOpen: true	//add by eko.zhan at 2016-06-17 17:57 从知识详情进入分类展示强制open一个Tab页
					}, this);
				});
			}else if(id=="favClipQuestion"){//标准问收藏
				$(this).click(function(){
					FavBallExt.f_openPanel(null, faqId, categoryId);
				});
			}else if(id=="yewumoban"){
				//@lvan.li 20160308 业务模板按钮事件
				$(this).click(function(){
					var tabName = $('#categoryNv a:last').attr('title');
					tabName = (tabName==undefined || tabName=='')?'分类展示':tabName;
					url = $.fn.getRootPath() + '/app/template/template.htm?objId='+objId;
					parent.TABOBJECT.open({
						id : objId+'_template',
						name : tabName,
						hasClose : true,
						url : url,
						isRefresh : true
					}, this);
				});
			}

	});
	
}

function markread(obj){
	var objectMarkread = $(obj).attr("id");
	objectMarkread = objectMarkread.split("_");
	if(objectMarkread[0]=='0'){//当前用户未将该标准问标记为已读
		//已阅
		//var _data = {"userId":human_id,"faqId":faqId,"objId":objId};
		var _data = {"valueId":faqId,"objectId":objId,"categoryId":categoryId,"catePath":objectMarkread[1]};
		$.ajax({
			url :  $.fn.getRootPath() + '/app/search/search!markreadObject.htm',
			type : 'POST',
			timeout : 5000,
			async: true,
			dataType : 'json',
			data : _data,
			success : function(data, textStatus, jqXHR) {
				if(data=="1") {
					parent.layer.alert("操作成功!", -1);
					$(obj).attr("id","1_"+objectMarkread[1]);
					$(obj).css("cursor","default");//去掉a标签的光标变化
					$(obj).css("background-color","gray");//修改样式颜色
					$(obj).html("已阅");
					$(obj).unbind("click");
				}else{
					parent.layer.alert("操作失败!", -1);
				}
			},
			error : function(data, textStatus, errorThrown) {parent.layer.alert("操作异常!", -1);}
		});
	}
}

function cbSolved(){
	//parent.$(document).hint("已提交");
	$('h5 a').each(function(index){
		if(index > 0){// modify by baidengke 2016年4月12日 由于前面加了一个解决按钮，为了不影响解决按钮的功能，故加了一个判断
			$(this).unbind('click');
		}
	});
}

//原文
function yuanwen(){
	var flag = true;
	$.ajax({
   		type: "POST",
   		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
   		async: false,
   		dataType : 'json',
   		success: function(msg){
   			if(msg.length==0){
//	   				$(document).hint('没有原文');
   				$('div.gonggao_titile_right a:eq(0)').html('没有原文').css({
//   					'backgroundColor' : '#cccccc',
   					'backgroundImage' : 'url(' + $.fn.getRootPath() + '/resource/search/images/gonggao_bg1.jpg)',
//   					'color' : '#cccccc',
   					'cursor' : 'default'
   				});
   			}else{
   				flag=false;
   			}
   		}
	});
	if(!flag){
		$('div.gonggao_titile_right a:eq(0)').click(function(){
			getFileTree(objId);
		});
		$('#closeYuanwen').click(function(){
			closeFileTreeDiv();
		});
	}
}
function getFileTree(objId){
	var flag = true;
	$.ajax({
   		type: "POST",
   		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
   		async: false,
   		dataType : 'json',
   		success: function(msg){
   			if(msg.length==0){
   				$(document).hint('没有原文');
   			}else{
   				flag=false;
   			}
   		}
	});
	if(flag){
		return;
	}
	
	var fileTreeSetting = {
	view: {
			expandSpeed: ""//空不显示动画
	},
	async : {
		enable : true,
		url : $.fn.getRootPath() + "/app/search/attachment!getNode.htm?objId="+objId,
		autoParam : ["id", "name=n", "level=lv","bh"],
		otherParam : {}
	},
	callback : {
		onClick : fileClick
	}
};
	
	
	$.fn.zTree.init($("#fileTree"), fileTreeSetting);
	var w = ($('body').width() - $('div.gonggao_d').width())/2;
	$('div.gonggao_d').css('left',w+'px');
	$('body').showShade();
	$('div.gonggao_d').show();
	
}

function fileClick(event, treeId, treeNode, clickFlag){
	if(treeNode.level==1){
		window.open(searchCtxPath+"attachmentDown?attachmentId="+treeNode.id);
	}
}



function closeFileTreeDiv(){
	$('body').hideShade();
	$('div.gonggao_d').hide();
}

function openP4(url){
	if(url!=null && url!=''){
		//window.open(url+'?'+new Date().getTime());
		if (window.navigator.userAgent.indexOf("Chrome") > -1 ) {
			window.open(url+'?'+new Date().getTime());
		}else{
			/*
			var scrWidth = screen.width;
			var scrHeight = screen.height; 
			window.showModalDialog(url+'?'+new Date().getTime() ,null ,'dialogLeft:0px;dialogTop:0px;center:yes;resizable:yes;minimize:yes;maximize:yes;dialogHeight:'+scrHeight+'px;dialogWidth:'+scrWidth+'px;')
			*/
			var scrWidth = screen.width - 20;
			var scrHeight = screen.height - 85;
			var win = window.open(url+'?'+new Date().getTime() ,null ,'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
				+ scrWidth + ',height=' + scrHeight);
			win.focus();
		}
	}
	
	return false;
}

/**
 * @author eko.zhan
 * @since 2015-10-10 17:52
 * 推荐功能，以前的推荐在 recommend/js/recommend.js 弃用之
 */
function recommend(){
	$('#recommendbtn').click(function(){
		parent.__kbs_layer_index = parent.$.layer({
			type : 2,
			border : [10, 0.3, '#000'],
			title : false,
			closeBtn : [0, true],
			iframe : {
				src : $.fn.getRootPath() + '/app/recommend/recommend!open.htm?cateid=' + categoryId + '&valueid=' + faqId
			},
			area : ['540', '480']
		});
	});
}

/**
 * @author eko.zhan 
 * @since 2015-08-20 16:30
 * 当前页面增加需要处理的一些脚本 add by eko.zhan at 2015-08-20 15:37
 * 之所以要新增一个方法，是为了遵从之前的js设计模式，至于之前的设计模式为何如此，I also do not know
 * 方法名 Nomonhan 来自历史上的今天：1939年8月20日苏军在朱可夫的指挥下在诺门罕前线发起反攻
 */
function nomonhan (){
	//add by eko.zhan at 2015-08-20 15:30  答案里采用超链接会在本页面转向新的地址
	//modify by eko.zhan at 2016-10-18 17:20 IE上单击会弹出一个空白窗口
	$('#gzyd_sms_answer').find('a[href!="javascript:void(0);"]').attr('target', '_blank');
	//add by eko.zhan at 2015-08-27 09:30 页面刷新滚动到顶部
	$(parent).scrollTop(0);
	
	$('#categoryNv').on('click', 'a', function(item){
		var _this = this;
		var _id = $(_this).attr('_id');
		var _text = $(_this).attr('title');
		if ($(_this).index()==$('#categoryNv a').length-1){
			//最后一个导航==实例
			openObj(_id);
		}else{
			//分类
			openCategory(_id, _text);
		}
	});
	
	//add by eko.zhan at 2015-09-07 17:28 
	//针对部分客户要求manager配置附件名称能直接打开附件的需求做出修改
	//manager 知识配置如下： [link url="{kbase}"]故乡的脚印.xlsx[/link]维度只能是知识库
	//modify by eko.zhan at 2015-09-08 16:40 
	//客户要求也要显示预览功能
	/* 该功能暂不对外开放
	$('a[href="{kbase}"]:visible').after('<a style="color:#8080C0;margin-left:5px;" href="javascript:void(0)">[预览]</a>');
	$('a[href="{kbase}"]:visible').next().click(function(){
		var _this = $(this).prev();
		var _att = $(_this).text();
		_att = _att.substring(1, _att.length-1);
		
		$('a[kbsname="'+_att+'"]').click();
	});
	$('a[href="{kbase}"]:visible').attr('href', 'javascript:void(0)').click(function(){
		var _this = this;
		var _att = $(_this).text();
		_att = _att.substring(1, _att.length-1);
		
		var $target = $('a[kbsname="'+_att+'"]');
		if ($target.length>0){
			$target = $target.parentsUntil('div.dq_wz').parent().find('span a');
			window.open($target.attr('href'));
		}
		return false;
	});
	*/
	
}