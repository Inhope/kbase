/**
 * 故障树	
 * @auther Gassol.Bi
 * @since 2016-03-16 17:28
 * @description
 */
$(function(){
	var that = window.FaultTree = {}, fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	fn.extend({
		getyyyyMMdd: function(){
			var date = new Date();
			date.setDate(date.getDate());
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			if(m < 10) m = '0' + m;
			if(d < 10) d = '0' + d;
			return y + '-' + m + '-' + d;
		},
		/*有弹窗*/
		shade: function(options){
			var before = options.before;
			/*事件不存在，或者事件存在但是返回true*/
			if(!before || (before && before.call(that, options))){
					var h = options.height, w = options.width;
					if(!w) w = document.body.clientWidth*0.98;
					if(!h) h = document.body.clientHeight*0.9;
					/*打开弹窗*/
					$('#dg-dialog').dialog({
						iconCls:'icon-save', resizable:true, 
						closed: true, modal:true,
						width:w, height:h, 
						title: options.title,
						queryParams: options.params,
						href: options.url,
						buttons: options.buttons
					});
					$('#dg-dialog').dialog('open');
			}
		},
		/*无弹窗*/
		submit: function(options){
			var title = options.title, url = options.url, 
				params = options.params, before = options.before;
			/*事件不存在，或者事件存在但是返回true*/
			if(!before || (before && before.call(that))){
				$.messager.confirm('确认', '确定要' + title + '选中的数据吗？', 
				function(r){
					if(r){
						$.post(url, params, 
							function(jsonResult) {
								$.messager.alert('信息', jsonResult.msg);
								if(jsonResult.rst) 
									$('#now-search').click();
									$('#now-tree').tree({
								        url:$.fn.getRootPath() +'/app/search/fault-type!syncData.htm'
								    });
						}, 'json');
					}
				});
			}
		},
		
		
		/*获取选中的行*/
		getChecked: function(){
			var ids = new Array();
			$($('#now-tab').datagrid('getChecked')).each(function(){
				ids.push(this.id);
			});
			return ids;
		},
		/*获取单行选中的行*/
		getSingleChecked: function(){
			var ids = fn.getChecked();
			if(ids.length == 0) {
				$.messager.alert('警告', '请选择要操作的数据!');
			} else if(ids.length > 1) {
				$.messager.alert('警告', '不能同时操作多条数据!');
			} else {
				return ids.join(',');
			}
			return false;
		},
		/*获取多行行选中的行*/
		getMultiChecked: function(){
			var ids = fn.getChecked();
			if(ids == 0) {
				$.messager.alert('警告', '请选择要操作的数据!');
			} else {
				return ids.join(',');
			}
			return false;
		}
	});
	
	that.extend({
		init: function(){
			/*查询条件初始化
			$('#createDate').datebox('setValue', fn.getyyyyMMdd());*/
			
			/*刷新列表*/
			var init = setTimeout(function (){
				clearTimeout(init);/*取消定时器*/
				$('#now-tab').datagrid({
					url: $.fn.getRootPath() + '/app/search/fault-tree!getFaultData.htm',
					queryParams: {}
	            });
			}, 500);
			
			/*查询按钮*/
			$('#now-search').click(function(){
				var queryParams = $("#now-tab").datagrid('options').queryParams;
				queryParams.name = $('#name').textbox('getText');
				queryParams.userName = $('#userName').textbox('getText');
		        queryParams.createDate = $('#createDate').datebox('getValue');
		        /*分类选中的节点*/
		        var node = $('#now-tree').tree('getSelected');
		        if(node) queryParams.typeId = node.id;
		        else queryParams.typeId = null;
		        $("#now-tab").datagrid('reload');
			});
			
			/*重置按钮*/
			$('#now-clear').click(function(){
				$('#name').textbox('setText', '');
				$('#userName').textbox('setText', '');
				$('#createDate').datebox('setValue', '');
				$('#now-tree').tree('unselect');
			});
			
			
			/*编辑、新增弹窗-相关按钮*/
			var buttons = 
				[{
	                text: '保存',
	                iconCls: 'icon-save',
	                handler: function () {
	                	$('#faultTreeEdit').form('submit', {
					        url: $.fn.getRootPath() + '/app/search/fault-tree!faultSave.htm', 
					        onSubmit:function(params){
								return $(this).form('validate');
					        },
					        success:function(jsonResult){
					        	jsonResult = eval('(' + jsonResult + ')');
					        	$.messager.alert('信息', jsonResult.msg);
					        	if(jsonResult.rst) {
					        		$('#now-search').click();
									$('#dg-dialog').dialog('close');
					        	}
					        }
					    });
	                }
	            }, {
	                text: '取消',
	                iconCls: 'icon-cancel',
	                handler: function () {
	                	$('#dg-dialog').dialog('close');
	                }
	            }];
			
			/*新增按钮*/
			$('#add').click(function(){
				fn.shade({
					width: 300, 
				    height: 150, 
				    title: '新增',
				    params: {}, 
				    url: $.fn.getRootPath() 
				    	+ '/app/search/fault-tree!faultEdit.htm',
				    buttons: buttons
				});
			});
			
			/*编辑按钮*/
			$('#edit').click(function(){
				var ids = fn.getSingleChecked()
				if(ids) fn.shade({
					width: 300, 
				    height: 150,  
				    title: '编辑',  
				   	params: {'ids': ids}, 
				    url: $.fn.getRootPath() 
				    	+ '/app/search/fault-tree!faultEdit.htm',
				   	buttons: buttons
				});
			});
			
			/*删除按钮*/
			$('#del').click(function(){
				var ids = fn.getMultiChecked()
				if(ids) fn.submit({
				    title: '删除',
				    params: {'ids': ids}, 
				    url: $.fn.getRootPath() 
				    	+ '/app/search/fault-tree!faultDel.htm'
				});
			});
			
			/*故障树-编辑*/
			$('#tree-edit').click(function(){
				var ids = fn.getSingleChecked()
				if(ids) {
					/*
					fn.shade({
					    title: '编辑',  
					   	params: {'ids': ids}, 
					    url: $.fn.getRootPath() 
					    	+ '/app/search/fault-graph!graphEdit.htm'
					});
					*/
					window.open($.fn.getRootPath() 
					    	+ '/app/search/fault-graph!graphEdit.htm?ids=' + ids, ids);
				}
			});
			
			
			/*故障树-查看*/
			$('#tree-show').click(function(){
				var ids = fn.getSingleChecked()
				if(ids) {
					/*
					fn.shade({
					    title: '编辑',  
					   	params: {'ids': ids}, 
					    url: $.fn.getRootPath() 
					    	+ '/app/search/fault-graph!graphEdit.htm'
					});
					*/
					window.open($.fn.getRootPath() 
					    	+ '/app/search/fault-graph!graphShow.htm?graphId=' + ids, ids);
				}
			});
		}
	});
	/*初始化*/
	that.init();
	
	
	
	/***************************************************故障树分类操作******************************************************************************/
	var PAGE = PAGE || {};

	PAGE.FUNC = {
			
			init: function(){

				/*编辑、新增弹窗-相关按钮*/
				var buttons = 
					[{
		                text: '保存',
		                iconCls: 'icon-save',
		                handler: function () {

						var name = $("#name_faultType").val();
						var name2 = $("#name2_faultType").val();

						if(name == ''){
							 $.messager.alert('信息', "故障树分类名称不能为空!");
							 return;
						}
		                
						$.post($.fn.getRootPath() + '/app/search/fault-type!checkData.htm',
								 {"name":name,"name2":name2},
								 function(data){
									 var d = $.parseJSON(data);
									 if(d.rst){
										 $('#faultTypeAddOrEdit').form('submit', {
										        url: $.fn.getRootPath() + '/app/search/fault-type!addOrEditFaultTypeDo.htm', 
										        onSubmit:function(params){
													return $(this).form('validate');
										        },
										        success:function(jsonResult){
										        	jsonResult = eval('(' + jsonResult + ')');
										        	$.messager.alert('信息', jsonResult.msg);
										        	if(jsonResult.rst) {
										        		$('#now-search').click();
														$('#dg-dialog').dialog('close');
													    $('#now-tree').tree({
													        url:$.fn.getRootPath() +'/app/search/fault-type!syncData.htm'
													    });
										        	}
										        }
										    });
									  }else{
										  $.messager.alert('信息', d.msg);
									  }
									
								 });
		                }
		            }, {
		                text: '取消',
		                iconCls: 'icon-cancel',
		                handler: function () {
		                	$('#dg-dialog').dialog('close');
		                }
		            }];

				/*新增按钮*/
				$('#faultType_add').click(function(){
					fn.shade({
						width: 400, 
					    height: 250, 
					    title: '新增',
					    params: {}, 
					    url: $.fn.getRootPath() 
					    	+ '/app/search/fault-type!addOrEditFaultTypeTo.htm',
					    buttons: buttons
					});
				});
				
				/*编辑按钮*/
				$('#faultType_edit').click(function(){
					 var node = $('#now-tree').tree('getSelected');/*分类选中的节点*/
					 if(node == null || typeof(node) == "undefined"){
						 $.messager.alert('信息', "请选择故障树分类!");
						 return;
					 }

					 var id_faultType = node.id;
					if(id_faultType) fn.shade({
						width: 400, 
					    height: 250,  
					    title: '编辑',  
					   	params: {'id_faultType': id_faultType}, 
					    url: $.fn.getRootPath() 
					    	+ '/app/search/fault-type!addOrEditFaultTypeTo.htm',
					   	buttons: buttons
					});
				});
				
				/*删除按钮*/
				$('#faultType_del').click(function(){
					 var node = $('#now-tree').tree('getSelected');/*分类选中的节点*/
					 if(node == null || typeof(node) == "undefined"){
						 $.messager.alert('信息', "请选择故障树分类!");
						 return;
					 }
					var ids = node.id;/*分类选中的节点*/
					if(ids) fn.submit({
					    title: '删除',
					    params: {'ids': ids}, 
					    url: $.fn.getRootPath() 
					    	+ '/app/search/fault-type!delFaultType.htm'
					});
				});
			}
		};
	
	/*初始化*/
	PAGE.FUNC.init();
});