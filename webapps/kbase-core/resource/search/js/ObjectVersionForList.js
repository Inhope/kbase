var objectId;
function categoryTag(){
	$('a.categoryTag').click(function(){
		var url = $.fn.getRootPath()+"/app/category/category.htm?type=4&categoryId="+$(this).attr('categoryId')+"&categoryName="+$(this).attr('categoryName');
		parent.TABOBJECT.open({
			id : $(this).attr('categoryId'),
			name : $(this).attr('categoryName'),
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	});
}
function getVersion(){
	//版本对比按钮
	$('a.objectVersion').click(function(){
		objectId = $(this).attr('objectId');
		$(document).showShade();
		$.ajax({
			url:$.fn.getRootPath()+'/app/search/search!objectVersion.htm',
			type:'post',
			data:{objectId:objectId},
			timeout:5000,
			dataType:'json',
			success:function(data){
				showVersion(data);
				$(document).hideShade();
			},
			error:function(msg){
				parent.parent.$(document).hint('加载失败');
				$(document).hideShade();
			}
		});
	});
	//关闭按钮
	$('div.index_dan_title a').click(function(){
		$('div.index_chu').hide();
	});
	//对比
	var btn = $('div.index_d div.xgai a.tijiao:eq(0)');
	btn.click(function() {
		var checkVserion = $('div.index_d input[type=checkbox]:checked');
		if(checkVserion.size() == 0) {
			parent.layer.alert('请选择比对项', -1)
		} else if(checkVserion.size() == 2) {
			var versionIds = new Array(),bl = true;
			$.each(checkVserion, function(i) {
				if($("#"+$(this).attr('id')+"_1").length>0){
					parent.layer.alert('不应选择已经屏蔽的数据!', -1);
					return bl = false;
				}
				versionIds.push($(this).attr('id') + ';' + $(this).attr('index'));
			});
//			if(bl)self.renderComp(versionIds.join(','));
			if(bl){
				parent.TABOBJECT.open({
					id : 'comparison_object',
					name : '实例对比',
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/object-search!comparisonObject.htm?objId=' + objectId + '&versionIds=' + versionIds.join(','),
					isRefresh : true
				}, this);
			}
		} else {
			parent.layer.alert('只能请选择两项进行对比', -1)
		}
	});
	init();
	//公开、屏蔽
	$('#gongkai').click(function(){
		 maskObject('0');
	});
	$('#pingbi').click(function(){
		 maskObject('1');
	});
}

function showVersion(data){
	if(data.list.length < 1){
		parent.layer.alert('当前实例还没有产生历史版本!', -1)
		return false;
	}
	//显示版本内容
	var text = '<tr>'
		+'<td class="biaoti" align="center">'
		+'版本号'
		+'</td>'
		+'<td class="biaoti" align="center">'
		+'版本变更时间'
		+'</td>'
		+'<td class="biaoti" align="center">'
		+'版本变更原因'
		+'</td>'
		+'</tr>';
	for(var i=0; i<data.list.length; i++){
		if(data.permission !=1 && data.list[i].status == '1'){
			continue;
		}
		text += '<tr>';
		text += '<td align="center">';
		text += '<input type="checkbox" id="'+data.list[i].revisionId+'" index="'+(data.list.length-i)+'"/>';
		text += data.list.length-i;
		if(i==0)
			text += '(当前)';
		text += '<span id="'+data.list[i].revisionId+'_'+data.list[i].status+'">';
		if(data.list[i].status == '1')
			text += '(屏蔽)';
		text += '</span>';
		text += '</td>';
		text += '<td align="center">'+data.list[i].commitTime+'</td>';
		text += '<td align="center">'+data.list[i].message+'</td>';
		text += '</tr>';
	}
	$('li.biaoge table').html('');
	$('li.biaoge table').append(text);
	
	//公开屏蔽按钮权限判断
	if(data.permission ==1){
		$('#pingbi').show();
		$('#gongkai').show();
	}else{
		$('#pingbi').hide();
		$('#gongkai').hide();
	}
	
	$('div.index_chu').css({
		'top' : '50px',
		'left' : ($(window).width() - $('div.index_chu').width()) / 2 + 'px',
		'zIndex' : '100010'
	}).show();
}


function init(){
	//去掉勾选
	$("input[type=checkbox]:checked").each(function(){
		$(this).attr('checked',false);
	});
}

//屏蔽或者公开的方法(flag=='0'公开,flag=='1'屏蔽)
function maskObject(flag){
	var bl = true, parameters = {},title = "屏蔽", versionIds = new Array();
	$("input[type=checkbox]:checked").each(function(){
		//如果是公开，数据中不应有已公开的数据
		if(flag=='0'&&$("#"+$(this).attr('id')+"_0").length>0){
			parent.layer.alert("不应包含已公开的数据!", -1);
			return bl = false;
		}
		//如果是屏蔽，数据中不应有已屏蔽的数据
		if(flag=='1'&&$("#"+$(this).attr('id')+"_1").length>0){
			parent.layer.alert("不应包含已公开的屏蔽!", -1);
			return bl = false;
		}
		versionIds.push("'"+$(this).attr('id')+"'");
	});
	parameters.flag = flag;
	if(flag=='0')title = "公开";//标题
	//获取的数据不正确,跳出方法
	if(!bl)return bl;
	if(versionIds.length>0){
		parent.layer.confirm("你确定要"+title+"这些版本吗？", function(){
			versionIds = versionIds.join(',');
			parameters.objectId = objectId;
			parameters.versionIds = versionIds;
			$.ajax({
		   		type: "POST",
		   		url : $.fn.getRootPath()+'/app/search/search!maskObject.htm',
		   		data : parameters,
		   		async: true,
		   		dataType : 'json',
		   		success: function(result){
		   			if(result=="1"){
						parent.layer.alert("操作成功!", -1);
						$("input[type=checkbox]:checked").each(function(){
							//公开
							if(flag=='0'){
								$("#"+$(this).attr('id')+"_1").text("");
								$("#"+$(this).attr('id')+"_1").attr("id",$(this).attr('id')+"_0");
							}
							//屏蔽
							if(flag=='1'){
								$("#"+$(this).attr('id')+"_0").text("("+title+")");
								$("#"+$(this).attr('id')+"_0").attr("id",$(this).attr('id')+"_1");
							}
						});
						//修改父页面"上个版本"的版本号
						bl = true;
						$("input[type=checkbox]").each(function(index,element){
							//公开
							if(index!=0&&$("#"+$(this).attr('id')+"_0").length>0){
								$("#prevVersionCompBtn").attr("pv",$(this).attr('id'));
								return bl = false;
							}
						});
						if(bl)$("#prevVersionCompBtn").attr("pv",'');
						init();//初始化
					}else{
						parent.layer.alert("操作失败!", -1);
					}
		   		},
		   		error: function(){}
			});
		});
	}else{
		parent.layer.alert("请选择要"+title+"的版本!", -1);
	}
}
