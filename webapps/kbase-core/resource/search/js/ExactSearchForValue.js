$(function() {
	!window.SeniorSearchForValue || window.SeniorSearchForValue();
	setCurrentIframeHeight();
	/**@author lvan.li 20160516 每个业务下qa只显示3个，多余的隐藏*/
	$('.moreQa').each(function(){
		$(this).click(function(){
			var more = $(this).parent().parent().parent().children('ul.moreul');
			if(more.is(':hidden')){
				more.show();
				$(this).html('↑隐藏更多');
			}else{
				more.hide();
				$(this).html('↓显示更多');
			}
			setCurrentIframeHeight();
		});
	});
});
SeniorSearchForValue = function(object) {
	//点击单个知识名字展开收起答案
	$('div.dangqian_1 h1 a').not('._ymzs').click(function() {
		if($(this).parent().parent().next().is(':hidden')) {
			$(this).parent().parent().next().show();
			setCurrentIframeHeight();
			// add by baidengke 2016年3月28日 操作日志记录
			var logInfo = $(this).attr("logInfo");
			if(logInfo){
				logInfo = logInfo.split("|#|");
				//modify by eko.zhan at 2016-07-01
				//应该是缺 olog 有报错
				try{
					olog.qaTitle({
						sequence:logInfo[0],
						pageNo:logInfo[1],
						pageSize:logInfo[2],
						qaId:logInfo[3],
						qaName: $("<div></div>").append(logInfo[4]).text(),
						touchContext:olog.constants.search
					});
				}catch(e){}
			}
		} else {
			$(this).parent().parent().next().hide();
		}
		
		
	});
	
	//单个知识点进入页面展示页面
	$('div.dangqian_1 h1 a._ymzs').click(function() {
		var self = $(this);
		var ftp = self.attr('ftp');//操作类型 1-短信发送  0-页面展示  modify by heart.cao 2016-06-07
		var qid = self.attr('id');
		var bhdiv = self.closest('ul').siblings('div.bh');
		var cId = bhdiv.attr('categoryid');
		if(ftp=='1'){
		    SMSTools.sms.send(cId, qid);
		}else{
			var objname = bhdiv.children('a').text();
			objname = (objname == undefined || objname == '')	? '页面展示': objname;
			url = $.fn.getRootPath() + '/app/category/category.htm?type=2&categoryId='+ cId + '&categoryName=' + objname+'&questionId=' + qid;
			parent.TABOBJECT.open({
							id : cId,
							name : objname,
							hasClose : true,
							url : url,
							isRefresh : true
			}, this);		
		}
	});
	
	//点击预览展开所有知识的答案
	$('div.gaoji_title_right a').click(function() {
		var ans = $('div.gaoji_con_nr div.dangqian_2');
		if(!$(this).next().attr('checked')) {
			$(this).next().attr('checked', true)
			ans.show();
			try{
				// add by baidengke 2016年3月28日 操作日志记录
				olog.qaPreview({
					touchContext:olog.constants.search
				});
			}catch(e){}
		} else {
			ans.hide();
			$(this).next().attr('checked', false)
		}
		setCurrentIframeHeight();
	});
	
	//modify by eko.zhan at 2016-07-04 10:00 
	$('div.gaoji_title_right input[type=checkbox]').click(function(){
		var checked = $(this).attr('checked');
		if (checked==undefined){
			//不勾选预览
			$('div.gaoji_con_nr div.dangqian_2').hide();
		}else{
			//勾选预览
			$('div.gaoji_con_nr div.dangqian_2').show();
		}
		
	});
	
	//点击详情进入知识展示页面
	$('div.dangqian_1 h6 a').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr('title_'),
			name : $(this).attr('title_'),
			title : $(this).attr('title_'),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=7&askContent=' + encodeURIComponent($(this).attr('value')),
			isRefresh : true
		}, this);
		
		// add by baidengke 2016年3月28日 操作日志记录
		var logInfo = $(this).attr("logInfo");
		if (logInfo) {
			logInfo = logInfo.split("|#|");
			try{
				olog.qaDetail({
					sequence : logInfo[0],
					pageNo : logInfo[1],
					pageSize : logInfo[2],
					qaId : logInfo[3],
					qaName : $("<div></div>").append(logInfo[4]).text(),
					touchContext:olog.constants.search
				});
			}catch(e){}
		}
	});
	
	/*
	 * 高级搜索结果列表中添加分类展示按钮
	 * update by alan.zhang at 2015-12-11 16:11
	 * modify by eko.zhan at 2016-07-01 17:22
	 * 只有启用分类展示才打开分类展示页面
	 */
	$('div.dangqian_1 h4 a').click(function(){
		if (enableCateTag=='true'){
			var url = $.fn.getRootPath()+"/app/category/category.htm?type=1&categoryId="+$(this).attr('categoryId')+"&categoryName="+$(this).attr('categoryName');
			parent.TABOBJECT.open({
				id : $(this).attr('categoryId'),
				name : $(this).attr('categoryName'),
				hasClose : true,
				url : url,
				isRefresh : true
			}, this);
		}
	});
	
	//排序，按热度、按修改时间、按创建时间
//	var order = {
//		createTimeEl : $('a[filed=create_time]'),
//		editTimeEl : $('a[filed=edit_time]'),
//		hotEl : $('a[filed=hot]'),
//		start : $('div#currentPage #start').val(),
//		limit :  $('div#currentPage #pageSize').val(),
//		contentSearch : $('div.gaoji_ss input:text'),
//		orderClick : function() {
//			var self = this;
//			self.createTimeEl.click(function() {
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'starttime', self.createTimeEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//			
//			self.editTimeEl.click(function() {
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'edittime', self.editTimeEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//			
//			self.hotEl.click(function(){
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'hot', self.hotEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//		}
//	};
//	order.orderClick();
	changeColor();
	//在结果中搜索
	var searchByResult = {
		btnSearch : $('div.gaoji_ss input:button'),
		contentSearch : $('div.gaoji_ss input:text'),
		form : $('form'),
		init : function(){
			var self = this;
			this.btnSearch.click(function(){
				fromObject.submitBtn.trigger('click');
			});
			this.contentSearch.keydown(function(event) {
	          if (event.keyCode == "13") {//keyCode=13是回车键
	              searchByResult.btnSearch.click();
	          }
	      });
		}
	}
	searchByResult.init();
	
	fenye();
};

function changeColor(){
	
	/*var text = '1122<a href="http://gd.10086.cn/commodity/options/detail/HK_DAY_MEAL.shtml">香港日HK套餐</a>办理（网站只支持全球通客户开通）HKxxxx999<a href="http://gd.10086.cn/commodity/options/detail/HK_DAY_MEAL.shtml">香港日HK套餐99</a>办理（网站只支持全球通客户开通99）HKxxxx';
    var keyword = "HK";
  	var rx = /(.*?)(<a[^>]+>)([^>]+)(<\/a>(.*))/ig;
    text = text.replace(rx, function ($0, $1, $2, $3,$4) {
    	if(!$1){
    		$1 = '';
    	}
		return $1.replace(keyword, '<font color="red">' + keyword + '</font>') + $2 + $3.replace(keyword, '<font color="red">' + keyword + '</font>') + $4.replace(keyword, '<font color="red">' + keyword + '</font>'); 
	});
    console.log(text)*/
	
//	var ac = askContent.split("");   
//	ac = ac.toString();
//	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]")
//    var rs = "";
//	for (var i = 0; i < ac.length; i++) {
//	    rs = rs + ac.substr(i, 1).replace(pattern, '');
//	}
//	ac = rs;
//	var regString = '';
//	regString = ac.split('').join('|');
//	/*
//	for(var i=0;i<ac.length;i++){
//		if(i != 0 ){
//			regString += '|';
//		}
//		regString += ac[i];
//	}
//	*/
//	//alert(regString);
//	var listTextEl = $('h1 a');
//	listTextEl.each(function(index) {
////		$(this).html($(this).html().replace(new RegExp('('+regString+')', 'ig'), '<font color="red">$1</font>'));
//	});
//	
//	var listTextEl2 = $('div.dangqian_2 div');
//	listTextEl2.each(function(index) {
////		var matchStr = $(this).html().match(new RegExp('(<[^>]+>)','ig'));
////		//console.log(matchStr);
////		$(this).html($(this).html().replace(new RegExp('(<[^>]+>)','ig'), '[%]'));
////		$(this).html($(this).html().replace(new RegExp('('+regString+')', 'ig'), '<font color="red">$1</font>'));
////		if(matchStr!=null && matchStr.length>0){
////			for(var i=0; i<matchStr.length ; i++){
////				var tIndex = $(this).html().indexOf('[%]');
////				var txt = $(this).html();
////				$(this).html(txt.substring(0,tIndex)+matchStr[i]+txt.substring(tIndex+3),txt.length);
////			}
////		}
//		
//	});
	$('h1 a').each(function(){
		if($(this).attr('title'))
			$(this).attr('title',$(this).attr('title').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
	});
	$('h6 a').each(function(){
		if($(this).attr('value'))
			$(this).attr('value',$(this).attr('value').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
	});
//	$('a.detail').each(function(){
//		if($(this).attr('question'))
//			$(this).attr('question',$(this).attr('question').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
//	});
	/*$('highlight').attr('color','red');
	$('highlight').each(function(){
		if($(this).parent().html())
			$(this).parent().html($(this).parent().html().replace(new RegExp('highlight', 'ig'), 'font'));
	});*/
	$('h1 a').each(function(){
		var text =  $(this).html();
		if(text.indexOf('</a>') > 0){
		    var keyword = askContent;
		    var rx = /(.*?)(<a[^>]+>)([^>]+)(<\/a>(.*))/ig;
		    text = text.replace(rx, function ($0, $1, $2, $3,$4) {
		    	if(!$1){
		    		$1 = '';
		    	}
		    	
				return $1.replace(keyword, '<font color="red">' + keyword + '</font>') + $2 + $3.replace(keyword, '<font color="red">' + keyword + '</font>') + $4.replace(keyword, '<font color="red">' + keyword + '</font>'); 
			});
		} else {
			 text = text.replace(askContent, '<font color="red">'+ askContent +'</font>');
		}
		 $(this).html(text);
	});
	$('div.dangqian_2 div').each(function(){
		var text =  $(this).html();
		if(text.indexOf('</a>') > 0){
		    var keyword = askContent;
		    var rx = /(.*?)(<a[^>]+>)([^>]+)(<\/a>(.*))/ig;
		    text = text.replace(rx, function ($0, $1, $2, $3,$4) {
		    	if(!$1){
		    		$1 = '';
		    	}
				return $1.replace(keyword, '<font color="red">' + keyword + '</font>') + $2 + $3.replace(keyword, '<font color="red">' + keyword + '</font>') + $4.replace(keyword, '<font color="red">' + keyword + '</font>'); 
			});
		} else {
			 text = text.replace(askContent, '<font color="red">'+ askContent +'</font>');
		}
		 $(this).html(text);
	});
}

function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('#start').val());
	var limit = parseInt($('#pageSize').val());
	var currentPage = parseInt($('a.dang').text());
	var totalPage = $('#totalPage').val();//parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
	});
	function fenyeSubmit(start){
		$('#fenyeForm input[name="start"]').val(start);
		$('#fenyeForm').submit();
		setCurrentIframeHeight();
	}
}
var setCurrentIframeHeight = function(){
	var _iframe = $(parent.document).find('div.content_content iframe:visible');
	_iframe.height($('body').height());
}