var FavBall = function() {
	var favBallObject = {
		favBtn : $('div.gonggao_titile_right a:eq(3)'),
		closeBtn : $('div.fav-ball div.main div.title a'),
		panel : $('div.fav-ball'),
		saveBtn : $('div.fcontent div.bottom a.tijiao input[type=button]'),
		render : function() {
			var self = this;
			
			self.saveBtn.click(function() {
				var favId = $('select[id=favOption] option:selected').val();
				var objId = window.objId;
				$('body').ajaxLoading('数据处理中...');
				$.ajax({
					url : $.fn.getRootPath() + '/app/fav/fav-clip-object!save.htm',
					type : 'POST',
					timeout : 5000,
					dataType : 'json',
					async : false,
					data : {
						oid : objId,
						fid : favId,
						categoryId : categoryId
					},
					success : function(data, textStatus, jqXHR) {
						$('body').ajaxLoadEnd();
						parent.layer.alert(data.message, -1);
						self.closeBtn.trigger('click');
					},
					error : function() {
						parent.layer.alert('请求超时', -1)
						$('body').ajaxLoadEnd();
					}
				});
			});
			
			//选择收藏夹
			self.favBtn.click(function() {
				//在实例列表页面和问答明细页面都有  objId 和 categoryId 两个js对象
				parent.__kbs_layer_index = parent.$.layer({
					type : 2,
					border : [10, 0.3, '#000'],
					title : false,
					closeBtn : [0, true],
					iframe : {
						src : $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?objid=' + objId + '&cataid=' + categoryId
					},
					area : ['500', '300']
				});
				return false;
				//下面是老的收藏夹代码 hidden by eko.zhan at 2015-08-13 10:50
				
				$(document).ajaxLoading('数据请求中...');
				$.ajax({
					url : $.fn.getRootPath() + '/app/fav/fav-clip!loadData.htm',
					type : 'POST',
					timeout : 5000,
					dataType : 'json',
					async : false,
					success : function(data, textStatus, jqXHR) {
						$(document).ajaxLoadEnd();
						if(data.success) {
							if(data.data.length == 0) {
								parent.layer.alert('请先创建收藏夹!', -1);								
							} else {
								var favOption = $('#favOption').html('');
								for(var i = 0; i < data.data.length; i ++) {
									var option = $('<option value="' + data.data[i].id +'">' + data.data[i].favClipName +'</option>');
									favOption.append(option);
								}
								$(document).showShade();
								self.panel.css({
									'top' : '50px',
									'left' : ($(window).width() - self.panel.width()) / 2 + 'px',
									'position' : 'absolute',
									'zIndex' : '100010'
								}).show();
							}
							
						} else {
							parent.layer.alert(data.message, -1);
							$(document).ajaxLoadEnd();
						}
					},
					error : function() {
						parent.layer.alert('请求超时', -1);
						$(document).ajaxLoadEnd();
					}
				});
			});		
			
			//隐藏收藏夹弹层
			self.closeBtn.click(function() {
				$(document).hideShade();
				self.panel.hide();
			});
		}
	}
	favBallObject.render();
}

var FavBallExt = {
	//打开收藏夹
	f_openPanel : function (objId, faqId, categoryId){
		var params = '?1=1';
		if(objId != null && objId != '' && objId != undefined) params += '&objid=' + objId;
		if(faqId != null && faqId != '' && faqId != undefined) params += '&faqid=' + faqId;
		if(categoryId != null && categoryId != '' && categoryId != undefined) params += '&cataid=' + categoryId;
		parent.__kbs_layer_index = parent.$.layer({
			type : 2,
			border : [10, 0.3, '#000'],
			title : false,
			closeBtn : [0, true],
			iframe : {
				src : $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm' + params
			},
			area : ['500', '300']
		});
	}
	
}