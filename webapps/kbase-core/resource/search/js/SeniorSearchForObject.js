SeniorSearchForObject = function() {
	//点击单个实例名字进入知识列表
	$('div.dangqian_1 h1 a').click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr('title'),
			name : $(this).attr('title'),
			title : $(this).attr('title'),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=7&objId=' + $(this).attr('value')+'&categoryId='+$(this).attr('categoryId'),
			isRefresh : true
		}, this);
		
		// add by baidengke 2016年3月27日 操作日志记录
		olog.obj({
			categoryId:$(this).attr('category'),
			objectName:$(this).attr('title'),
			objectId:$(this).attr('value'),
			touchContext:olog.constants.search
		});
	});
//	var order = {
//		startTimeEl : $('a[filed=create_time]'),
//		editTimeEl : $('a[filed=edit_time]'),
//		hotEl : $('a[filed=hot]'),
//		start : $('div#currentPage #start').val(),
//		limit :  $('div#currentPage #pageSize').val(),
//		contentSearch : $('div.gaoji_ss input:text'),
//		init : function() {
//			var self = this;
//			this.startTimeEl.click(function() {
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'starttime', self.startTimeEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//			
//			this.editTimeEl.click(function() {
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'edittime', self.editTimeEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//			self.hotEl.click(function(){
//				if($(this).attr('desc') == 'asc') {
//					$(this).attr('desc', 'desc');
//				} else {
//					$(this).attr('desc', 'asc');
//				}
//				fromObject.submitBtn.trigger('click', [self.start, self.limit, 'hot', self.hotEl.attr('desc'),self.contentSearch.val()]);
//				return false;
//			});
//		}
//	};
//	order.init();
	//在结果中搜索
	var searchForResult = {
		btnSearch : $('div.gaoji_ss input:button'),
		contentSearch : $('div.gaoji_ss input:text'),
		form : $('form'),
		init : function(){
			var self = this;
			this.btnSearch.click(function(){
				fromObject.submitBtn.trigger('click', [order.start, order.limit, 'edittime', order.editTimeEl.attr('desc'),self.contentSearch.val()]);
			});
			this.contentSearch.keydown(function(event) {
	          if (event.keyCode == '13') {//keyCode=13是回车键
	              searchForResult.btnSearch.click();
	          }
      });
		}
	}
	searchForResult.init();
	changeColor();
};

function changeColor(){
//	var ac = askContent.split("");   
//	var regString = '';
//	for(var i=0;i<ac.length;i++){
//		if(i != 0 ){
//			regString += '|';
//		}
//		regString += ac[i];
//	}
	
//	var listTextEl = $('h1 a');
//	listTextEl.each(function(index) {
//		$(this).html($(this).html().replace(new RegExp('('+regString+')', 'ig'), '<font color="red">$1</font>'));
//	});
	$('h1 a').each(function(){
		if($(this).attr('title'))
			$(this).attr('title',$(this).attr('title').replace(new RegExp('(<[^>].*?>)', 'ig'), ''));
	});
	$('highlight').attr('color','red');
	$('highlight').each(function(){
		if($(this).parent().html())
			$(this).parent().html($(this).parent().html().replace(new RegExp('highlight', 'ig'), 'font'));
	});
}