//**************************************公共方法（开始）*************************************
//分页跳转
function pageClick(pageNo) {
	$('body').ajaxLoading('正在查询数据...');
	$("#form0").attr("action", SystemKeys.userData.viewUrl);
	$("#pageNo").val(pageNo);
	$("#form0").submit();

}

// 查询条件初始化
function selectReset() {
	$("input[id$='_select']").each(function() {
		$(this).attr("value", "");
	});
	$("select[id$='_select']").each(function() {
		$(this)[0].selectedIndex = '';
	});
}

// 弹出框打开
function openShade(id) {
	var url, bl = false, title, parameters = {};
	if (id == "ppPaperAdd") {
		url = "/app/task/pp-paper!addOrEditTo.htm";
		title = "新增试卷";
	} else if (id == "ppPaperEdit") {
		url = "/app/task/pp-paper!addOrEditTo.htm";
		title = "编辑试卷";
		bl = true;
		var paperId =  $("input:checkbox[name='ppPaperId_list']:checked").val();
		if (paperId != null && paperId != '') {
			url += '?id=' + paperId;
		}
	} else {
		return;
	}

	if (bl) {
		var ppPaperId_list = $("input:checkbox[name='ppPaperId_list']:checked")
		if (ppPaperId_list.length == 0) {
			parent.layer.alert("请选择要操作的数据!", -1);
			return;
		} else if (ppPaperId_list.length > 1) {
			parent.layer.alert("不能同时操作多条数据!", -1);
			return;
		} else {
			var ppPaperId = $(ppPaperId_list[0]).val();
			parameters.ppPaperId = ppPaperId;
		}
	}
	var index = parent.$.layer({
		type : 2,
		border : [ 2, 0.3, '#000' ],
		title : [ title, 'font-size:14px;font-weight:bold;' ],
		closeBtn : [ 0, true ],
		iframe : {
			src : $.fn.getRootPath() + url
		},
		area : [ '700px', '400px' ]
	});
	
	$(window.parent.$("#addPpp")).remove();
	$(window.parent.$("body")).append('<input id="addPpp" type="hidden" value="'+index+'" />');
}

function noShade(id) {
	var url, bl = false, title, parameters = {};
	if (id == "ppPaperDel") {// 删除试卷
		url = "/app/back/pp-paper!delUser.htm";
		title = "删除";
	} else {
		return;
	}

	var ppPaperIds = "";
	$("input:checkbox[name='ppPaperId_list']:checked").each(function() {
		bl = true;
		ppPaperIds += $(this).val() + ",";
	});
	parameters.ppPaperIds = ppPaperIds;

	if (bl) {
		if (confirm("确定要" + title + "选中的数据吗？")) {
			$('body').ajaxLoading('正在保存数据...');
			$.post($.fn.getRootPath() + url, parameters, function(data) {
				if (data == "1") {
					parent.layer.alert("操作成功!", -1);
					pageClick('1');
				} else {
					parent.layer.alert("操作失败!", -1);
					$('body').ajaxLoadEnd();
				}
			}, "html");
		}
	} else {
		parent.layer.alert("请选择要" + title + "的数据!", -1);
	}
}

//删除试卷事件  add by wilson.li at 2016-06-06
$(function(){
	$("#ppPaperDel").on("click",function(){
		var checkDatas = $("input:checkbox[name='ppPaperId_list']:checked");
		if(checkDatas.length <= 0){
			parent.layer.alert("请选择要操作的数据!", -1);
			return false;
		}
		var paperIds = [];
		checkDatas.each(function(i,item){
			paperIds.push($(item).val());
		});
		$.ajax({
			type: "POST",
			url: $.fn.getRootPath()+"/app/task/pp-paper!checkPaperUsed.htm",
			data: "paperIds="+paperIds,
			dataType:"json",
			success: function(data) {
				if(data.status){
					var taskNames = "";
					data = data.list;
					for(var i=0;i<data.length;i++){
						taskNames += (i==0?"":",")+data[i].name
					}
					layer.alert("试卷被【"+taskNames+"】引用，无法删除",-1);
					return false;
				}else{
					layer.confirm("确定要删除选中的数据吗？",function(){
						$.ajax({
							type: "POST",
							url: $.fn.getRootPath()+"/app/task/pp-paper!paperDelDo.htm",
							data: "paperIds="+paperIds,
							dataType:"json",
							success: function(data) {
								if(data.status){
									layer.alert("操作成功!",-1);
									pageClick('1');
								}else{
									layer.alert("操作失败!",-1);
								}
							}
						});
					});
				}
			}
		});
	});
});

