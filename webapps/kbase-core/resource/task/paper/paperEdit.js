////账号新增、编辑		
//			function addOrEditPpPaper(){
//				$(form1).validate({
//					rules : {
//						"ppPaper.ppPaperName" : {
//							required : [ "事件名称" ]
//						},
//						"ppPaper.rewardResult" : {
//							required : [ "奖励结果" ],
//							number : [ "奖励结果" ]
//						}
//					},
//					// 验证通过时的处理
//					success : function() {
//				
//					},
//					errorPlacement : function(error, element) {
//						error.appendTo(element.parent());
//					},
//					focusInvalid : false,
//					onkeyup : false
//				});
//				
//				var flag = $(form1).validate().form();
//				if(flag){
//					$('#form1').ajaxLoading('正在保存数据...');						
//					$("#form1").attr("action", $.fn.getRootPath()+"/app/fore/ppPaper!addOrEditDo.htm");
//					$("#form1").submit();
//				}
//				
//			}


$(function() {
	treeInit();
	formInit();
});
var ids = new Array();
function formInit(){
	// 选目录
	$('#ppTypeCheck').click(function() {
		/*
		var bodyWidth = $('body').width();
		var tableWidth = $('table').width();
		var width = (bodyWidth - tableWidth) / 2 + tableWidth - 400;
		$(this).next().css('left', width + 'px');
		$(this).next().show();
		*/
		var _offset = $(this).offset();
		$(this).next().css({left:_offset.left + "px", top:_offset.top + $(this).outerHeight() + "px"}).slideDown("fast");
	});
	$('#treeChecked').focus(
			function() {
				ids = new Array();
				var nodes = $.fn.zTree.getZTreeObj("ppTypeCheckTree").getCheckedNodes(true);
				var leafNodes = new Array();
				var s = '';
				for(var i=0; i<nodes.length; i++){
					if(nodes[i].isLeaf){
						leafNodes.push(nodes[i]);
//						s += nodes[i].name+'<br/>';
						ids.push(nodes[i].id);
					}
				}
				for(var i=0; i<leafNodes.length; i++){
					if(i<9){
						s += "&nbsp;&nbsp;";
					}
					s += (i+1) +'.';
					if(leafNodes[i].questionType==0){
						s+= '判断';
					}else if(leafNodes[i].questionType==1){
						s+= '单选';
					}else if(leafNodes[i].questionType==2){
						s+= '多选';
					}
					s +=' '+ leafNodes[i].name+'<br/>';
				}
				$('#checkedQuestions').html(s);
				if(ids.length>0){
					$('#ppTypeCheck').val('已选择');
				}else{
					$('#ppTypeCheck').val('未选择');
				}
			});
	if ($('#ppTypeCheckTree').length > 0) {
		$('*').bind(
				'click',
				function(e) {
					var self = $('#ppTypeCheckTree').parent();
					if ((self.offset().left <= e.pageX && e.pageX <= (self
							.offset().left + self.width()))) {

					} else {
						if (!self.is(':hidden'))
							self.hide();
					}
				});
		$('*').bind(
				'focus',
				function(e) {
					var self = $('#ppTypeCheckTree').parent();
					if ((self.offset().left <= e.pageX && e.pageX <= (self
							.offset().left + self.width()))) {

					} else {
						if (!self.is(':hidden'))
							self.hide();
					}
				});
	}
	
	//预览
	$('#preViewBtn').click(function(){
		if(ids.length<1){
			parent.layer.alert('请选择试题',-1);
			return false;
		}
		
		$.ajax({
			url : $.fn.getRootPath()+ "/app/task/pp-question!preView.htm",
			type : "post",
			data : {'ids':ids.join(',')},
			timeout : 5000,
			dataType : "json",
			success : function(data) {
//				console.log(data);
				var p1 = '<span style="font-weight:bold;">判断</span><br/>';
				var p2 = '<span style="font-weight:bold;">单选</span><br/>';
				var p3 = '<span style="font-weight:bold;">多选</span><br/>';
				var len = p1.length;
				for(var i=0;i<data.length;i++){
					var index = 97;
					if(data[i].questionType==0){
						p1 += (i+1);
						if(i<9){
							p1 += "&nbsp;&nbsp;";
						}
						p1 += '. '+data[i].questionName+'<br/>';
						if(data[i].isRight==1){
							p1 += '<span style="display:inline-block;width:25px;color:red;">√</span>'+String.fromCharCode(index)+'. 正确<br/>';
							p1 += '<span style="display:inline-block;width:25px;color:red;"> </span>'+String.fromCharCode(index+1)+'. 错误<br/>';
						}else{
							p1 += '<span style="display:inline-block;width:25px;color:red;"> </span>'+String.fromCharCode(index)+'. 正确<br/>';
							p1 += '<span style="display:inline-block;width:25px;color:red;">√</span>'+String.fromCharCode(index+1)+'. 错误<br/>';
						}
						p1 +='<br/>';
					}else if(data[i].questionType==1 ){
						p2 += (i+1)+'. '+data[i].questionName+'<br/>';
						var ans = data[i].ppAnswer;
						for(var j=0; j<ans.length;j++){
							if(ans[j].isRight==1){
								p2 += '<span style="display:inline-block;width:25px;color:red;">√</span>';
							}else{
								p2 += '<span style="display:inline-block;width:25px;color:red;"> </span>';
							}
							p2 += String.fromCharCode(index+j) + '. ' + ans[j].content+'<br/>';
						}
						p2 +='<br/>';
					}else if(data[i].questionType==2 ){
						p3 += (i+1)+'. '+data[i].questionName+'<br/>';
						var ans = data[i].ppAnswer;
						for(var j=0; j<ans.length;j++){
							if(ans[j].isRight==1){
								p3 += '<span style="display:inline-block;width:25px;color:red;">√</span>';
							}else{
								p3 += '<span style="display:inline-block;width:25px;color:red;"> </span>';
							}
							p3 += String.fromCharCode(index+j) + '. ' + ans[j].content+'<br/>';
						}
						p3 +='<br/>';
					}
				}
				var p ='<div style="overflow:auto;width:700px;height:364px;font-size:14px;"><div style="margin:20px 20px 20px 20px;">';
				if(p1.length>len)
					p += p1;
				if(p2.length>len)
					p += p2;
				if(p3.length>len)
					p += p3;
				p += '</div></div>';
				parent.$.layer({
				    type: 1,
				    shade: [0],
				    area:['700px', '400px'],
				    title: ['试卷预览'],
				    border: [10, 0.3, '#000'],
				    page: {html : p},
				    closeBtn: [0, true]
				});
			}
		});
	});
	//提交
	$(form1).validate({
		rules : {
			"ppPaper.paperName" : {
				required : [ "试卷名称" ]
			},
			"ppPaper.isEnable" : {
				required : [ "试卷状态" ]
			}
		},
		// 验证通过时的处理
		success : function() {

		},
		errorPlacement : function(error, element) {
			error.appendTo(element.parent());
		},
		focusInvalid : false,
		onkeyup : false
	});
	
	$('#submit').click(function(){
		var flag1 = $(form1).validate().form();
		if(ids.length<1){
			var p = $('#ppTypeCheck').parent().parent();
			p.find('lable').remove();
			var s = '<lable><font color="red">请选择试题</font></lable>';
			p.append(s);
		}
		if(!flag1 || ids.length<1){
			return false;
		}
		var paperName = $('input[name="ppPaper.paperName"]').val();
		var id = $('input[name="ppPaper.id"]').val();
		var isEnable = $('#isEnable_select').val();
		$.ajax({
			url : $.fn.getRootPath()+ "/app/task/pp-paper!addOrEditDo.htm",
			type : "post",
			data : {'ids':ids.join(','),'ppPaper.paperName':paperName,'ppPaper.id':id,'ppPaper.isEnable':isEnable},
			timeout : 5000,
			dataType : "json",
			success : function(data) {
				parent.layer.alert(data,-1);
				if (data == '操作成功!') {
					var index = $(window.parent.$("#addPpp")).val();
//					parent.location.reload();
/*
					$(window.parent.document).find('iframe').each(function(){
						if(!$(this).is(":hidden")){
							if($(this).attr('src').contains('/kbase-core/app/task/pp-paper.htm'));
							 $(this).attr("src",$(this).attr('src')); 
						}
					});
*/
					$(parent.document).find('div.content_content').find('iframe').each(function(){
						if (!$(this).is(":hidden")){
							$(this).attr("src", $.fn.getRootPath()+'/app/task/pp-paper.htm'); 
							return false;
						}
					});					
					if (index)
						parent.layer.close(index);
				}
			}
		});
	});
	
}

//初始化选择的题目
function initTreeCheck(){
	var tree = $.fn.zTree.getZTreeObj("ppTypeCheckTree");
	var ppid = questions.split(',');
	ids = new Array();
	if(!ppid || ppid=='')
		return false;
	for(var i=0; i<ppid.length; i++){
		ids.push(ppid[i]);
		var node = tree.getNodeByParam("id", ppid[i]);
		if(node!=null){
			tree.checkNode(node);
		}
	}
	if(ppid.length>0){
	    /**
		var nodes = tree.getCheckedNodes(true);
		var leafNodes = new Array();
		var s = '';
		for(var i=0; i<nodes.length; i++){
			if(nodes[i].isLeaf){
				ids.push(nodes[i].id);
				leafNodes.push(nodes[i]);
			}
		}
		for(var i=0; i<leafNodes.length; i++){
			if(i<9){
				s += "&nbsp;&nbsp;";
			}
			s += (i+1) +'.';
			if(leafNodes[i].questionType==0){
				s+= '判断';
			}else if(leafNodes[i].questionType==1){
				s+= '单选';
			}else if(leafNodes[i].questionType==2){
				s+= '多选';
			}
			s +=' '+ nodes[i].name+'<br/>';
		}
		$('#checkedQuestions').html(s);
		**/
		$('#ppTypeCheck').val('已选择');
	}
}

/** ***********目录选题************* */
function treeInit() {
	var setting = {
		edit : {
			enable : true,
			showRemoveBtn : false,
			showRenameBtn : false
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		async : {
			enable : true,
			url : $.fn.getRootPath() + '/app/task/pp-paper!questionsTree.htm'
		},
		check: {
			enable: true,
			chkboxType : { "Y" : "s", "N" : "s" }
		},
		callback: {
			onAsyncSuccess: initTreeCheck
		}
	};

	$.fn.zTree.init($("#ppTypeCheckTree"), setting);

}