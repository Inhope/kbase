<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/library/jquery/js/jquery-easyui/themes/metro/easyui.css"/>
<style type="text/css">
	.icon-task {
		background: url("${pageContext.request.contextPath}/resource/task/imgs/task.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	.icon-exam {
		background: url("${pageContext.request.contextPath}/resource/task/imgs/exam.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	.icon-ques {
		background: url("${pageContext.request.contextPath}/resource/task/imgs/ques.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js"></script>

<!-- layer -->
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/library/layer/extend/layer.ext.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/library/jquery/js/jquery-easyui/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath }/resource/task/task-menu.js"></script>