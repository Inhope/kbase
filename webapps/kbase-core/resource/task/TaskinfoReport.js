
//勾选
function zTreeOnCheck(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
	nodes = zTree.getCheckedNodes(true);
	v = "";
	depIds = "";
	//nodes.sort(function compare(a,b){return a.id-b.id;});
	for (var i=0, l=nodes.length; i<l; i++) {
		v += nodes[i].name + ",";
		depIds +=  nodes[i].bh + ",";
	}
	
	if (v.length > 0 ) v = v.substring(0, v.length-1);
	if (depIds.length > 0 ) depIds = depIds.substring(0, depIds.length-1);
	$("#dept").attr("value", v);
	$("#deptid").attr("value", depIds);
}		


function virtualOnCheck(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("virtualdepttree"),
	nodes = zTree.getCheckedNodes(true);
	v = "";
	depIds = "";
	for (var i=0, l=nodes.length; i<l; i++) {
		v += nodes[i].name + ",";
		if(typeof(nodes[i].deptset) == "undefined"){
			depIds +=  nodes[i].id + ",";
		}else{
			depIds +=  nodes[i].deptset + ",";
		}
		
	}
	
	if (v.length > 0 ) v = v.substring(0, v.length-1);
	if (depIds.length > 0 ) depIds = depIds.substring(0, depIds.length-1);
	$("#virtualname").attr("value", v);
	$("#virtualdeptid").attr("value", depIds);
}
			
function showMenu(menuContent,inputName) {
	var cityObj = $("#"+inputName);
	var cityOffset = $(cityObj).offset();
	$("#"+menuContent).css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

	$("body").bind("mousedown", function(event){
		onBodyDown(event,menuContent)
	});
}

function hideMenu(menuContent) {
	$("#"+menuContent).fadeOut("fast");
	$("body").unbind("mousedown",function(event){
		onBodyDown(event,menuContent)
	});
}

function onBodyDown(event,menuContent) {
	if (!(event.target.id == menuContent || $(event.target).parents("#"+menuContent).length>0)) {
		hideMenu(menuContent);
	}
}


$(function() {
	
	var setting = {
		async: {
			enable: true,
			url : $.fn.getRootPath() + "/app/util/picker!deptbydhJson.htm",
			autoParam:["id", "name","level"]
		},
		check: {
			enable: true,
			chkStyle: "checkbox",
			chkboxType: { "Y": "", "N": "" }
		},
		callback: {
			onCheck: zTreeOnCheck									
		}
	};
	
	var virtualsetting = {
		async: {
			enable: true,
			url : $.fn.getRootPath() + "/app/custom/dh/dhpicker!getvirtualtree.htm",
			autoParam:["id", "name","level"]
		},
		check: {
			enable: true,
			chkStyle: "checkbox",
			chkboxType: { "Y": "", "N": "" }
		},
		callback: {
			onCheck: virtualOnCheck									
		}
	};
	$.fn.zTree.init($('#virtualdepttree'), virtualsetting);	
	$.fn.zTree.init($('#treeDemo'), setting);		
	
	var tableObject = {
		tableEl : $('table'),
		forms : {
			start_time:$('#start_time'),
			end_time:$('#end_time'),
			taskname:$('#taskname'),
			virtualdept_id:$('#virtualdept_id'),
			searchBtn : $('#searchBtn'),
			getValues : function() {
				return {
					"start_time":this.start_time.val(),
					"end_time":this.end_time.val(),
					"taskname":this.taskname.val(),
					"virtualdept_id":this.virtualdept_id.val(),
				}
			}
		},
		pager : null,
		tableAttributes : {
			title : '任务考试报表',
			width : $(window).width() - 16,
			height : '445',
			nowrap : true,
			autoRowHeight : false,
			striped : true,
			collapsible : false,
//			url: $.fn.getRootPath() + '/app/statement/click-amount!list.htm',
			method : 'POST',
			sortOrder : 'desc',
			remoteSort : false,
			loadMsg:'加载中...',
			pagination:true,
			rownumbers:true,
			singleSelect: true,
			toolbar : '#searchCondition',
			fitColumns : true,
			columns : [
				[
					{field:'virtualdeptname',title:'虚拟部门',width:100,
						formatter: function(value,row,index){
							return "<span title='"+row.taskInfo.virtualdeptname+"'>"+row.taskInfo.virtualdeptname+"</span>" 
						}
					},
					{field:'taskname',title:'任务名称',width:120},
					{field:'fromdate',title:'开始时间',width:120},
					{field:'todate',title:'禁用时间',width:100},
					{field:'notcomplete',title:'未完成人数',width:50,
						formatter: function(value,row,index){
							return row.taskInfo.notcomplete
						}
					},
					{field:'complete',title:'完成人数',width:50,
						formatter: function(value,row,index){
							return row.taskInfo.complete
						}
					},
					{field:'beforecomplete',title:'事前完成',width:50,
						formatter: function(value,row,index){
							return row.taskInfo.beforecomplete
						}
					},
					{field:'aftercomplete',title:'事后完成',width:50,
						formatter: function(value,row,index){
							return row.taskInfo.aftercomplete
						}
					},
					{field:'completepecent',title:'完成百分比',width:50,
						formatter: function(value,row,index){
							return row.taskInfo.completepecent
						}
					}
				]
			]
		},
		pagerAttributes : {
			pagination : false,
			pageNumber : 1,
			pageSize : 10,
			initData : false
		},
		/**
		pagerAttributes : {
			pageNumber : 1,
			pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10, 20, 50],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        onBeforeRefresh:function(){
	        	
	        }
		},
		**/
		loadData : function(url) {
			this.tableEl.datagrid({
					url : url,    
					queryParams :this.forms.getValues()
			})
		},
		/*******渲染表格*******/
		
		render : function() {
		
			var self = this;
			//初始化表格
			this.tableEl.datagrid(this.tableAttributes);
			this.pager = this.tableEl.datagrid('getPager');
			$(this.pager).pagination(this.pagerAttributes);
			this.forms.searchBtn.click(function() {
				var url = $.fn.getRootPath() + '/app/task/tasktest-report!findtaskinfos.htm';
				self.loadData(url);
			});
		
		}
		
	}
	
	
	var deptTreeObject = {
		deptTypeEl : $('#dept'),
		ktreeEl : $('div.t-p-d1'),
		treeAttr : {
			view : {
				expandSpeed: ''
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/util/picker!deptbydhJson.htm",
				autoParam : ["id", "name=n", "level=lv", "bh"],
				otherParam : {}
			},
			callback : {
				onClick : function(event, treeId, treeNode) {
					deptTreeObject.deptTypeEl.val(treeNode.name);
					deptTreeObject.deptTypeEl.attr('bh', treeNode.bh);
				}
			}
		},
		render : function() {
			var self = this;
			self.deptTypeEl.val('');
			self.deptTypeEl.focus(function(e){
				self.ktreeEl.css({
					'top' : ($(this).height() + $(this).offset().top + 1) + 'px',
					'left' : $(this).offset().left + 'px'
				});
				if(self.ktreeEl.is(':hidden'))
					self.ktreeEl.show();
			});
			
			$('*').bind('click', function(e){
				if((self.ktreeEl.offset().left <= e.pageX && e.pageX <= (self.ktreeEl.offset().left + self.ktreeEl.width())) && ((self.ktreeEl.offset().top - self.deptTypeEl.height()) <= e.pageY && e.pageY <= (self.ktreeEl.offset().top + self.ktreeEl.height()))) {
					
				} else {
					if(!self.ktreeEl.is(':hidden'))
						self.ktreeEl.hide();
				}
			});
			
			self.deptTypeEl.bind('keydown', function(keyArg) {
				if(keyArg.keyCode == 8) {
					$(this).val('');
					$(this).attr('bh', '');
				} else if(keyArg.keyCode == 13) {
					self.ktreeEl.is(':hidden') || self.ktreeEl.hide();
//					tableObject.loadData();
				}
			});
			
			self.ktreeEl.find('div#catetory_dept>div>a').click(function(){
				$(this).parent().parent().parent().hide();
			});
			
			$.fn.zTree.init($('ul#deptTree'), this.treeAttr );
		}
	}
	var exportExcel = {
		exceportbtn : $('#exportBtn'),
		render : function(){
			this.exceportbtn.click(function(){
				var _total = tableObject.tableEl.datagrid('getPager').data("pagination").options.total;
				
				if (_total<5000) {
					$(document).ajaxLoading('处理数据中...');
					//
					$.ajax({
						url : $.fn.getRootPath() + '/app/task/tasktest-report!processtaskData.htm',
						type : 'POST',
						sync : false,
						dataType : 'json',
						data: tableObject.forms.getValues(),
						success : function(data, textStatus, jqXHR) {
							$(document).ajaxLoadEnd(); 
							if(data.success) {
								var iframe = document.createElement("iframe");
					            iframe.src = $.fn.getRootPath()+"/app/task/tasktest-report!downLoad.htm?fileName=" + data.message;
					            iframe.style.display = "none";
					            document.body.appendChild(iframe);
							} else {
								parent.layer.alert(data.message, -1);
							}
						},
						error : function(data, textStatus, errorThrown) {
							$(document).ajaxLoadEnd();
							parent.layer.alert('导出失败!', -1);
						}
					});
				} //end validation
			});
		}
	}
	exportExcel.render();
	tableObject.render();
	//deptTreeObject.render();
});