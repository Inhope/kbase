/**
 * @author eko.zhan at 2015-06-18
 */
$(function(){
	$('.tree-title').css({'font-size': '14px', 'font-weight': 'bolder'});
	$('.tree-node').css({'padding-top': '5px', 'padding-bottom': '5px'});
	
	$('ul.easyui-tree').tree({
		onClick: function(node){
			var id = node.id;
			if (id=='doTaskMgr'){
				location.href = $.fn.getRootPath() + "/app/task/pb-task.htm";
			}else if (id=='doExamMgr'){
				location.href = $.fn.getRootPath() + "/app/task/pp-paper.htm";
			}else if (id=='doQuesMgr'){
				location.href = $.fn.getRootPath() + "/app/task/pp-question.htm";
			}
		}
	});
});