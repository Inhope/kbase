<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="easyui-layout" data-options="fit:false" id="cc" style="height:600px;">
			<div data-options="region:'west',split:true" title="任务考试管理" style="width:150px;">
				<ul class="easyui-tree">
					<li data-options="id:'doTaskMgr', iconCls: 'icon-task'">
		                <span>任务管理</span>
		            </li>
		            <li data-options="id:'doExamMgr', iconCls: 'icon-exam'">
		                <span>试卷管理</span>
		            </li>
		            <li data-options="id:'doQuesMgr', iconCls: 'icon-ques'">
		                <span>题库管理</span>
		            </li>
		        </ul>
			</div>
			<div data-options="region:'center',title:''">
				<div id="p" class="easyui-panel" data-options="fit:true" style="border:0px;">