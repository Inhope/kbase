$(function() {
	/** ****************表单事件***************** */
	formInit();
	treeInit();
	edit();
});

/** ****************表单事件***************** */



var type;
function formInit() {
	//移动试题
    $('#tomove').click(function(){
		var ids = new Array();
		var typeid = $('input[name="ppQuestion.ppType.id"]').val();
		$('input[name="question_id"]').each(function(){
			ids.push($(this).val());
		})
		if(typeid ==''){
			parent.layer.alert("请选择移至的目录",-1);
			return false;
		}
		$.ajax({
				url : $.fn.getRootPath()+ "/app/task/pp-question!movequestion.htm",
				type : "post",
				data : {'ids':ids.join(','),'typeid':typeid},
				timeout : 5000,
				dataType : "json",
				success : function(data) {
					if(data.success='success'){
						parent.layer.alert('移动成功',-1);
					}else{
						parent.layer.alert('移动失败',-1);
					}
					$(parent.document).find('div.content_content').find('iframe').each(function(){
						if (!$(this).is(":hidden")){
							$(this).attr("src", $.fn.getRootPath()+'/app/task/pp-paper.htm'); 
							parent.layer.close(parent.index);
							return false;
						}
					});
				}
			});
	})
	// 类型选择
	$('input[name="ppQuestion.questionType"]').click(function() {
		$('div[ansType="0"]').parent().parent().show();
		$('div[ansType="0"]').parent().prev().show();
		ansInit($(this).val());
	});

	// 增加一条
	$('input.ansAdd')
			.click(
					function() {
						var ansSize = $('div[ansType="' + type
								+ '"] div.all_ans div').length;
						var ans;
						if (type == 1) {
							ans = '<div>'
									+ '	<input type="radio" name="ppAnswers.isRight" value="'
									+ ansSize
									+ '" />'
									+ ' <input type="hidden" name="ppAnswers.id" />'
									+ '	<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>'
									+ '	<input type="button" class="ansDel" value="删除" />'
									+ '</div>';
						} else if (type == 2) {
							ans = '<div>'
									+ '	<input type="checkbox" name="ppAnswers.isRight" value="'
									+ ansSize
									+ '" />'
									+ ' <input type="hidden" name="ppAnswers.id">'
									+ '	<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500"/>'
									+ '	<input type="button" class="ansDel" value="删除" />'
									+ '</div>';
						}
						$('div[ansType="' + type + '"] div.all_ans')
								.append(ans);
						delBind();
					});
	
	delBind();
	// 选目录
	$('#ppTypeCheck').click(function() {
		/*
		var bodyWidth = $('body').width();
		var tableWidth = $('table').width();
		var width = (bodyWidth - tableWidth) / 2 + tableWidth - 400;
		$(this).next().css('left', width + 'px');
		$(this).next().show();
		*/
		var _offset = $(this).offset();
		$(this).next().css({left:_offset.left + "px", top:_offset.top + $(this).outerHeight() + "px"}).slideDown("fast");
	});
	$('#treeChecked').focus(
			function() {
				var nodes = $.fn.zTree.getZTreeObj("ppTypeCheckTree")
						.getSelectedNodes();
				$('#ppTypeCheck').val(
						'/题目分类' + (nodes[0].path ? nodes[0].path : ''));
				$('#ppTypeCheck').prev().val(nodes[0].id);
			});
	if ($('#ppTypeCheckTree').length > 0) {
		$('*').bind(
				'click',
				function(e) {
					var self = $('#ppTypeCheckTree').parent();
					if ((self.offset().left <= e.pageX && e.pageX <= (self
							.offset().left + self.width()))) {

					} else {
						if (!self.is(':hidden'))
							self.hide();
					}
				});
		$('*').bind(
				'focus',
				function(e) {
					var self = $('#ppTypeCheckTree').parent();
					if ((self.offset().left <= e.pageX && e.pageX <= (self
							.offset().left + self.width()))) {

					} else {
						if (!self.is(':hidden'))
							self.hide();
					}
				});
	}
	// 提交
	$(form1).validate({
		rules : {
			"ppQuestion.questionName" : {
				required : [ "试题题目" ]
			},
			"ppQuestion.ppType.path" : {
				required : [ "关联目录" ]
			},
			"ppQuestion.questionType" : {
				required : [ "答案类型" ]
			// number : [ "奖励结果" ]
			},
			"ppQuestion.isRight" : {
				required : [ "正确答案" ]
			}
		// "ppAnswers.isRight":{
		// required : [ "正确答案" ]
		// },
		// "ppAnswers.content":{
		// required : [ "答案内容" ]
		// }
		},
		// 验证通过时的处理
		success : function() {

		},
		errorPlacement : function(error, element) {
			error.appendTo(element.parent());
		},
		focusInvalid : false,
		onkeyup : false
	});
	function validateAns() {
		function va() {
			var parent;
			var flag = false;
			if(type == 0){
				return true;
			}else if (type == 1 || type == 2) {

				$('input[name="ppAnswers.isRight"]:checked').each(function() {
					if (!$(this).attr('disabled')) {
						flag = true;
					}
				});
				$('input[name="ppAnswers.content"]').each(function() {
					if (!$(this).attr('disabled')) {
						parent = $(this).parent().parent();
						if (!$(this).val() || $(this).val() == '') {
							flag = false;
						}
					}
				});
				parent.find('lable').remove();
				if (!flag) {
					var s = '<lable><font color="red">至少选择一个正确答案，答案必须输入</font></lable>';
					parent.append(s);
				}
			}
			return flag;
			
		}
		var vaAns = va();
		$('input[name="ppAnswers.isRight"]').click(function() {
			va()
		});
		$('input[name="ppAnswers.content"]').blur(function() {
			va()
		});
		return vaAns;
	}
	$('#submit').click(function() {
		var flag1 = $(form1).validate().form();
		var flag2 = validateAns();
		if (flag1 && flag2) {
			var data = $("#form1").serialize();
			$.ajax({
				url : $.fn.getRootPath()
						+ "/app/task/pp-question!addOrEditDo.htm",
				type : "post",
				data : data,
				timeout : 5000,
				dataType : "html",
				success : function(data) {
					parent.layer.alert(data,-1);
					if (data == '操作成功!') {
						var index = $(window.parent.$("#addPpq")).val();
//						parent.location.reload();
/*
						$(window.parent.document).find('iframe').each(function(){
							if(!$(this).is(":hidden")){
								if($(this).attr('src').contains('/kbase-core/app/task/pp-paper.htm'));
								 $(this).attr("src",$(this).attr('src')); 
							}
						});
*/
						$(parent.document).find('div.content_content').find('iframe').each(function(){
							if (!$(this).is(":hidden")){
								$(this).attr("src", $.fn.getRootPath()+'/app/task/pp-question.htm'); 
								return false;
							}
						});						
						if (index)
							parent.layer.close(index);
					}
				}
			});
		}
	});
}

//删除一条
function delBind() {
	$('input.ansDel').click(
			function() {
				if(type==1){
					if($('div[ansType="' + type + '"] input[type="radio"]').length<=2){
						parent.layer.alert('至少保留两个答案');
						return false;
					}
				}else if(type==2){
					if($('div[ansType="' + type + '"] input[type="checkbox"]').length<=2){
						parent.layer.alert('至少保留两个答案');
						return false;
					}
				}
				$(this).parent().remove();
				$('div[ansType="' + type + '"] input[type="radio"]').each(
						function(index) {
							$(this).val(index);
						});
				$('div[ansType="' + type + '"] input[type="checkbox"]')
						.each(function(index) {
							$(this).val(index);
						});
			});
}
//初始化答案
function ansInit(val){
	if (val == 0) {
		type = 0;
		$('div[ansType="1"]').hide()
		$('div[ansType="1"] input').attr('disabled', 'disabled');
		$('div[ansType="2"]').hide();
		$('div[ansType="2"] input').attr('disabled', 'disabled');
		$('div[ansType="0"]').show();
		$('div[ansType="0"] input').removeAttr('disabled');
	} else if (val == 1) {
		type = 1;
		$('div[ansType="0"]').hide().attr('disabled', 'disabled');
		$('div[ansType="0"] input').attr('disabled', 'disabled');
		$('div[ansType="2"]').hide().attr('disabled', 'disabled');
		$('div[ansType="2"] input').attr('disabled', 'disabled');
		$('div[ansType="1"]').show().removeAttr('disabled');
		$('div[ansType="1"] input').removeAttr('disabled');
	} else if (val == 2) {
		type = 2;
		$('div[ansType="0"]').hide().attr('disabled', 'disabled');
		$('div[ansType="0"] input').attr('disabled', 'disabled');
		$('div[ansType="1"]').hide().attr('disabled', 'disabled');
		$('div[ansType="1"] input').attr('disabled', 'disabled');
		$('div[ansType="2"]').show().removeAttr('disabled');
		$('div[ansType="2"] input').removeAttr('disabled');
	}
}

/** ***********目录************* */
function treeInit() {
	var setting = {
		edit : {
			enable : true,
			showRemoveBtn : false,
			showRenameBtn : false
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		async : {
			enable : true,
			url : $.fn.getRootPath() + '/app/util/choose!ppTypeData.htm'
		}
	};

	$.fn.zTree.init($("#ppTypeCheckTree"), setting);

}
/***********编辑题目*************/
function edit(){
//	alert(questionType);
	if(!questionType)
		return false;
	$('div[ansType="0"]').parent().prev().show();
	$('div[ansType="0"]').parent().parent().show();
	$('div[ansType="'+questionType+'"]').show();
	$('div[ansType="'+questionType+'"] input').removeAttr('disabled');
	type=questionType;
	ansInit(questionType);
	if(questionType=='1'){
		var datas=eval('('+ppAnswer+')');
		$('div[ansType="' + questionType + '"] div.all_ans').html('');
		for(var i=0;i<datas.length;i++){
			ans = '<div>'
				+ '	<input type="radio" name="ppAnswers.isRight" value="'
				+ i + '"';
			ans += (datas[i].isRight==1 ? ' checked="checked" ':'');
			ans += ' />'
				+ '	<input type="hidden" name="ppAnswers.id" '
				+ 'value="'+datas[i].id+'" '
				+ '/>'
				+ '	<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500" '
				+ 'value="'+datas[i].content+'" '
				+ '/>'
				+ '	<input type="button" class="ansDel" value="删除" />'
				+ '</div>';
			$('div[ansType="' + questionType + '"] div.all_ans').append(ans);
		}
		delBind();
	}else if(questionType=='2'){
		var datas=eval('('+ppAnswer+')');
		$('div[ansType="' + questionType + '"] div.all_ans').html('');
		for(var i=0;i<datas.length;i++){
			ans = '<div>'
				+ '	<input type="checkbox" name="ppAnswers.isRight" value="'
				+ i + '"';
			ans += (datas[i].isRight==1 ? ' checked="checked" ':'');
			ans += ' />'
				+ '	<input type="hidden" name="ppAnswers.id" '
				+ 'value="'+datas[i].id+'" '
				+ '/>'
				+ '	<input type="text" name="ppAnswers.content" style="width:330px;" maxlength="500" '
				+ 'value="'+datas[i].content+'" '
				+ '/>'
				+ '	<input type="button" class="ansDel" value="删除" />'
				+ '</div>';
			$('div[ansType="' + questionType + '"] div.all_ans').append(ans);
		}
		delBind();
	}
	
}
