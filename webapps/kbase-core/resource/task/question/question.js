//**************************************公共方法（开始）*************************************
//分页跳转
function pageClick(pageNo) {
	$('body').ajaxLoading('正在查询数据...');
	$("#form0").attr("action", $.fn.getRootPath() + "/app/task/pp-question.htm");
	$("#pageNo").val(pageNo);
	$("#form0").submit();

}

function checkAll(){
	$("input:checkbox[name='ppQuestionId_list']").each(function(){
		$(this).attr("checked",$(this).attr("checked")=='checked'?false:true);
	});
}

// 查询条件初始化
function selectReset() {
	$("input[id$='_select']").each(function() {
		$(this).attr("value", "");
	});
	$("select[id$='_select']").each(function() {
		$(this)[0].selectedIndex = '';
	});
	$('#ppTypeNames').html('');
	$('#ppTypeNamesInput').val('');
	$('#ppTypeId').val('');
}

// 弹出框打开
function openShade(id) {
	var url, bl = false, title, parameters = {};
	if (id == "ppQuestionAdd") {
		url = "/app/task/pp-question!addOrEditTo.htm";
		title = "新增题库";
	} else if (id == "ppQuestionEdit") {
		url = "/app/task/pp-question!addOrEditTo.htm";
		title = "编辑题库";
		bl = true;
	}else if (id == "ppQuestionMove") {
		var c = $('input:checkbox[name="ppQuestionId_list"]:checked');
		if(c.length <= 0){
			parent.layer.alert('请选择要移动的试题',-1);
			return false;
		}
		var ids = new Array();
		c.each(function(){
				ids.push($(this).val());
		});
		url = "/app/task/pp-question!tomovequestion.htm?ids="+ids.join(',');
		parent.index = parent.$.layer({
			type : 2,
			border : [ 2, 0.3, '#000' ],
			title : [ '移动题库', 'font-size:14px;font-weight:bold;' ],
			closeBtn : [ 0, true ],
			iframe : {
				src : $.fn.getRootPath() + url
			},
			area : [ '700px', '400px' ]
		});
		return;
	} 
	else if (id == "ppQuestionDel") {
		var c = $('input:checkbox[name="ppQuestionId_list"]:checked');
		if(c.length <= 0){
			parent.layer.alert('请选择要删除的试题');
		}else{
			var msg = '确认删除这'+c.length+'条？';
			var ids = new Array();
			c.each(function(){
				ids.push($(this).val());
			});
			parent.layer.confirm(msg,function(index){
				$.ajax({
					url : $.fn.getRootPath()+ "/app/task/pp-question!del.htm",
					type : "post",
					data : {'ids':ids.join(',')},
					timeout : 5000,
					dataType : "json",
					success : function(data) {
						parent.layer.alert(data,-1);
						window.location.reload();
						c.removeAttr('checked');
					}
				});
				
				parent.layer.close(index);
			});
		}
		
		return;
	} else {
		return;
	}

	if (bl) {
		var ppQuestionId_list = $("input:checkbox[name='ppQuestionId_list']:checked")
		if (ppQuestionId_list.length == 0) {
			parent.layer.alert("请选择要操作的数据!", -1);
			return;
		} else if (ppQuestionId_list.length > 1) {
			parent.layer.alert("不能同时操作多条数据!", -1);
			return;
		} else {
			var ppQuestionId = $(ppQuestionId_list[0]).val();
			parameters.ppQuestionId = ppQuestionId;
		}
	}

	if(parameters.ppQuestionId)
		url = url+'?id='+parameters.ppQuestionId;
	var index = parent.$.layer({
		type : 2,
		border : [ 2, 0.3, '#000' ],
		title : [ title, 'font-size:14px;font-weight:bold;' ],
		closeBtn : [ 0, true ],
		iframe : {
			src : $.fn.getRootPath() + url
		},
		area : [ '700px', '400px' ]
	});
	$(window.parent.$("#addPpq")).remove();
	$(window.parent.$("body")).append('<input id="addPpq" type="hidden" value="'+index+'" />');
}

function noShade(id) {
	var url, bl = false, title, parameters = {};
	if (id == "ppQuestionDel") {// 删除问题
		url = "/app/back/pp-question!delUser.htm";
		title = "删除";
	} else {
		return;
	}

	var ppQuestionIds = "";
	$("input:checkbox[name='ppQuestionId_list']:checked").each(function() {
		bl = true;
		ppQuestionIds += $(this).val() + ",";
	});
	parameters.ppQuestionIds = ppQuestionIds;

	if (bl) {
		if (confirm("确定要" + title + "选中的数据吗？")) {
			$('body').ajaxLoading('正在保存数据...');
			$.post($.fn.getRootPath() + url, parameters, function(data) {
				if (data == "1") {
					parent.layer.alert("操作成功!", -1);
					pageClick('1');
				} else {
					parent.layer.alert("操作失败!", -1);
					$('body').ajaxLoadEnd();
				}
			}, "html");
		}
	} else {
		parent.layer.alert("请选择要" + title + "的数据!", -1);
	}
}





/**
 * @author nia.jiao
 * @author eko.zhan at 2015-07-27
 */
$(function(){
    /* 
     *  导入导出功能 <b> 
     */
	var intervalTitle = null;//提示标题
	var interval = null;//处理结果
	var prefix = "";//提示标题前缀
	var suffix = "";//提示标题后缀
	//提示标题
	function importWarningTitle(){
		if(suffix.length==4)suffix = "";
		$("#importWarning_title").html("<b>"+prefix+suffix+"<b/>");
		suffix += "*";
	}
	
	//导入导出结束初始化
	function importOrExport_init(){
		if(interval!=null)clearInterval(interval);
		if(intervalTitle!=null)clearInterval(intervalTitle);
		$("#ImportShadeClose").bind("click",function(){
			closeShade("ImportShade");
		});
	}
	$("#imgShadeClose").bind("click",function(){
		closeShade("ImportShade");
	});
	
    //导入、导出消息提示
	function importOrExport(type){
		if(type=='import'){
			prefix = "导入中"
		}else if(type=='export'){
			prefix = "导出中"
		}else{
			return false;
		}
		//显示消息提示区
		$('#scBefore').css("display","none");
		$('#importWarning').css("display","block");
		$('#scAfter').css("display","none");
		$('#importHandle').css("display","none");
		$("#rowTotal").html(0);
		$("#fuccessTotal").html(0);
		$("#failureTotal").html(0);
		$("#ImportShadeClose").unbind("click");//去掉关闭事件
		//提示标题
		intervalTitle = setInterval("importWarningTitle(prefix, suffix)",500);
		//提示结果
	   	interval = setInterval(function(){
	   		$.ajax({
				type : 'post',
				url : $.fn.getRootPath() + '/app/task/pp-question!importResult.htm', 
				data : {'type':type},
				async: true,
				dataType : 'json',
				success : function(data){
					$("#rowTotal").html(data.rowTotal);
					$("#fuccessTotal").html(data.fuccessTotal);
					$("#failureTotal").html(data.failureTotal);
					if(data.result==true){
						importOrExport_init();
					}
				},
				error : function(msg){
					importOrExport_init();
					parent.layer.alert("网络错误，请稍后再试!", -1);
				}
			});
	   	},500);
	}
	
	//弹出框打开
	function openToShade(shadeId){
		var w = ($('body').width() - $('#'+shadeId).width())/2;
		$('#'+shadeId).css('left',w+'px');
		$('#'+shadeId).css('top','0px');
		$('body').showShade();
		$('#'+shadeId).show();					
	}
	//弹出框关闭
	function closeShade(shadeId){
		$('#'+shadeId).css("display","none");
		$('body').hideShade();			
	}

    //数据导出
    /*
		$("#export").click( 
			function () { 
				//打开提示框
				$("#title_importOrExport").html("批量导出");
				openToShade("ImportShade");
				
				//导出消息提示
				importOrExport('export');
				$("#form0").attr("action", $.fn.getRootPath()+"/app/task/pp-question!exportQuestion.htm?status="+status);
				$("#form0").submit();
			
		});
		*/
	//modify by eko.zhan at 2015-07-27 伪遮罩层，5秒后自动消失，数据量如果太大需要调整，暂时纠正报错问题
	$('#export').click(function(){
		var _ind = layer.load('请稍候...', 5);
		$("#form0").attr("action", $.fn.getRootPath()+"/app/task/pp-question!exportQuestion.htm?status="+status);
		$("#form0").submit();
	});	
	
	  //导入
	 $("#importQuestion").click(function(){
		if($("#userFile").val()==null||$("#userFile").val()==''){
			parent.layer.alert("请选择需要上传的文件!", -1);
			return false;
		}
		
		//导入消息提示
		$("#title_importOrExport").html("批量导入");
		importOrExport('import');
		
		//数据导入
		$.ajaxFileUpload({
	        url: $.fn.getRootPath()+"/app/task/pp-question!importQuestion.htm",
	        secureuri: false,
	        fileElement: $("#userFile"),
	        uploadFileParamName : 'userFile',
	        success: function(data, status) {
	        	$('#scBefore').css("display","none");
				$('#importWarning').css("display","none");
				$('#scAfter').css("display","block");
				$('#importHandle').css("display","block");
				
				data = eval('(' + $(data).text() + ')');
			   	$('#scAfter p').html(data.returnResult);
	        	importOrExport_init();
	        },
	        error: function(obj, msg, e) {
	        	importOrExport_init();
	        	parent.layer.alert("网络错误，请稍后再试!", -1);
	        }
	    });
	
	 });
	 
	 //批量导入
	$("#import").click(function(){
	 	$('#scBefore').css("display","block");
		$('#scAfter').css("display","none");
		$('#importWarning').css("display","none");
		$('#importHandle').css("display","block");
		openToShade("ImportShade");
	});
	 
    	 //批量导入弹出窗关闭
	$('#ImportShadeClose').click(
		function(){
			closeShade("ImportShade");
	});
 	
		

});


