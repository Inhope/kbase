$(function() {
	$('div.zhishi_con_nr ul li[dataType!="attach"] h1 b').click(function() {
		var params = $(this).attr('categoryId').split(',');
		var id = params[0];/*当前单元格父id*/
		var categoryId = params[1];/*当前单元格id*/
		var name = $(this).attr('title');/*当前单元格内容*/
		var topObject = $(this).attr('_top');
		var hasObj = $(this).attr('_hasObj');
		if(params.length == 2){
			openOv(id, categoryId, null, name, topObject,hasObj);
			// add by baidengke 2016年3月27日 操作日志记录
			olog.category({
				categoryId: categoryId
			});
		}else{
			var objectId = params[2];
			openOv.call(this, id, categoryId, objectId, name, null, null);
			// add by baidengke 2016年3月27日 操作日志记录
			olog.obj({
				categoryId:categoryId,
				objectId:objectId,
				objectName: name,
				touchContext:olog.constants.browse
			});
		}
		
	});
	
	$('div.zhishi_titile p').click(function(){
		var flag = $(this).attr('after_endtime1');
		var url = window.location.href;
		if( url.indexOf('after_endtime1')>-1){
			if(flag=='0'){
				url=url.replace('after_endtime1=1', 'after_endtime1=0');
			}else if(flag=='1'){
				url=url.replace('after_endtime1=0', 'after_endtime1=1');
			}
		}else {
			url +="&after_endtime1="+flag;
		}
		window.location.href=url;
	});
});

function openOv(id,categoryId,objectId,name, topObject, hasObj){
	if(!objectId){
		//modify by eko.zhan at 2015-09-23 15:58 
		//采用jeasyui展开树，ztree在大数据量下有报错
		try{
			var node = window.parent.$('#categoryTree').tree('find', id);
			window.parent.$('#categoryTree').tree('expand', node.target);
		}catch(e){
			window.parent.selectNodeById(id,categoryId);
		}
		
		//modify by eko.zhan at 2016-06-17 12:25 使用 jeasyuiTab 后的bug，打开时直接 open
		if (parent.TABOBJECT.version==2){
			var isCate  = true;
			if(topObject && topObject == 'true'){
				isCate = false;
			}
			parent.TABOBJECT.open({
				id: name,
				name : name, 
				hasClose : true,
				url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId + '&isCate=' + isCate+'&t='+new Date().getTime(),
				isRefresh : true
			}, this);
		}else{
			var tab = parent.TABOBJECT.existTabs;
			var tabId = tab[tab.length-1];
			var isCate  = true;
			if(topObject && topObject == 'true'){
				tabId = categoryId;
				isCate = false;
			}
			if(hasObj && hasObj == 'true'){
				tabId = categoryId;
			}
			/*else {
				parent.TABOBJECT.tabTitle.find("span[class='xinjian_wz']").eq(0).attr('title',name);
				parent.TABOBJECT.tabTitle.find("span[class='xinjian_wz']").eq(0).text(name);
			}*/
			//window.location.href = $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId + '&categoryName=' + name;
			parent.TABOBJECT.open({
					id : tabId, 
					name : name, 
					hasClose : true,
					url : $.fn.getRootPath() + '/app/object/ontology-object.htm?id='+categoryId + '&isCate=' + isCate+'&t='+new Date().getTime(),
					isRefresh : true
				}, this);
		}
	}else{
	    name = (name==undefined||name==null)?'知识展示':name;
	    /*请求地址， 是否开启业务模板， 业务模板-文章id*/
	    var url = $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=1&objId='+objectId+'&categoryId_current='+categoryId_current, 
	    	bizTplEnable = $(this).attr('bizTplEnable'), bizTplArticleId = $(this).attr('bizTplArticleId');
	    if (bizTplEnable == 'true' && bizTplArticleId)  /*如果开启业务模板，来自业务模板的实例调整新的页面*/
	    	url =  $.fn.getRootPath() + '/app/biztpl/article-search.htm?&articleId=' + bizTplArticleId;
		parent.TABOBJECT.open({
			id : objectId, 
			name : name, 
			hasClose : true,
			url : url,
			isRefresh : true
		}, this);
	}
}

/************请求action获取category路径***************/
function navigate_init(robot_context_path,categoryId){
	var data = {
		'format':'json','async':true,'jsoncallback':'navigateCb','nodeid':categoryId,'nodetype':3
	};
	var url = robot_context_path+'p4pages/related-category.action'+'?format=json&async=true&nodeid='+categoryId+'&ts='
			+ new Date().getTime()+'&jsoncallback=navigateCb&nodetype=1';
	var script = document.createElement('script');  
	script.setAttribute('src', url);  
	document.getElementsByTagName('head')[0].appendChild(script);
}
window.navigateCb = function(data) {
var tempPathforinstance='';
	var navigateJObj = $('#categoryNv');
	if(data){
		for(var i=0;i<data.length;i++){
			var aJObj;
			aJObj = $('<a/>').attr('href','javascript:void(0);').attr('categoryId',data[i]['id'])
				.attr('title',data[i]['name']).html(data[i]['name']);
			navigateJObj.append(aJObj);
		}
		navigateJObj.children('a').click(function(){
			openOv('',$(this).attr('categoryId'),null,$(this).attr('title'));
		});
	}
	/*防止因为获取不到category路径造成样式干扰*/
	navigateJObj.append('&nbsp;&nbsp;');
}

