$(function() {
		//$('table.outTable tr td a').not('a.showAttachment').click(function() {
		$('a.categoryName').click(function() {
				var self = $(this);
				var tabName =self.attr('title');
				tabName = (tabName == undefined || tabName == '')	? '分类展示': tabName;
				url = $.fn.getRootPath()	+ '/app/category/category.htm?type=2&categoryId='+ self.attr("id") + '&t=' +new Date().getTime();
				parent.TABOBJECT.open({
							id : self.attr('id'),
							name : tabName,
							hasClose : true,
							url : url,
							isRefresh : true
						}, this);
		});
		$('a.showAttachment').each(function(){
			var self = $(this);
			$(this).click(function(){
				var _this = this;
				var _filename = $(_this).attr('_name');
	           	var _fileid = $(_this).attr('filename');
	           	var _filetype = '';
	           	if (_filename.lastIndexOf('.')!=-1){
	           		_filetype = _filename.substring(_filename.lastIndexOf('.'));
	           	}
	           	var fileName = _fileid + _filetype;
				_showModalDialog({url: swfAddress   + '?fileName='+fileName});
			});
		});
});
function _showModalDialog(opts){
	opts = $.extend(true, {
		width: 500,
		height: 400,
		args: window
	}, opts);
	var _diaWidth = opts.width;
	var _diaHeight = opts.height;
	var _scrWidth = screen.width;
	var _scrHegiht = screen.height;
	var _diaLeft = (_scrWidth-_diaWidth)/2;
	var _diaTop = (_scrHegiht-_diaHeight)/2;
	var params = 'height='+_diaHeight+'px,width='+_diaWidth+'px,left='+_diaLeft+'px,top='+_diaTop+'px'+',scrollbars=yes,resizable=yes,scrollbars=yes';
	window.open(opts.url, '', params);
}