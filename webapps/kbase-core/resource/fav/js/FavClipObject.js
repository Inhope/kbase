/**
 * @modify by eko.zhan at 2015-02-01
 * 修改视图样式
 */
$(function() {
	
	$('input[type=checkbox]').attr('checked', false);
	
	$('#selAll').click(function() {
		var isCheck = $(this).attr('checked');
		if(isCheck) {
			$('input[xname="childCheckBox"]').attr('checked', true);
		} else {
			$('input[xname="childCheckBox"]').attr('checked', false);
		}
	});
	
	$("#btnBack").click(function(){
		location.href = $.fn.getRootPath() + "/app/fav/fav-clip.htm";
	});
	
	$('#btnDel').click(function() {
		var checked = $('input[xname="childCheckBox"]:checked');
		if(checked.size() == 0) {
			parent.layer.alert('请选择要删除的对象!', -1);
		} else {
			parent.layer.confirm("确定删除吗？", function(){
				var ids = '';
				for(var i = 0; i < checked.size(); i ++) {
					ids += $(checked[i]).attr('id') + ',';
				}
				ids = ids.substring(0, ids.length - 1);
				$(document).ajaxLoading('正在删除数据...');
				$.ajax({
					url : $.fn.getRootPath() + '/app/fav/fav-clip-object!remove.htm',
					type : 'POST',
					timeout : 5000,
					dataType : 'json',
					data : {
						ids : ids
					},
					success : function(data, textStatus, jqXHR) {
						$(document).ajaxLoadEnd();
						parent.layer.alert(data.message, -1)
						if(data.success) {
							window.location.reload();
						}
					},
					error : function(data, textStatus, errorThrown) {
						$('body').ajaxLoadEnd();
					}
				});
			});
		}
	});
	
	$("b[xname='kbObject']").css("cursor", "pointer").click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=2&objId=' + encodeURIComponent($(this).attr('id')),
			isRefresh : true
		}, this);
	});
	
	
	$("b[xname='kbQuestion']").css("cursor", "pointer").click(function() {
		parent.TABOBJECT.open({
			id : $(this).attr("title"),
			name : $(this).attr("title"),
			title : $(this).attr("title"),
			hasClose : true,
			url : $.fn.getRootPath() + '/app/search/search.htm?searchMode=11&askContent=' + encodeURIComponent($(this).attr("title")),
			isRefresh : true
		}, this);
	});
	
});