/**
 * @modify by eko.zhan at 2015-02-01
 * 收藏夹视图界面兼容性调整
 */
$(function() {
	$('div.zhishi_con_nr ul li h2 b').attr('frameId', new Date().getTime() + '').click(function() {
		var fid = $(this).attr('id');
		window.location.href = $.fn.getRootPath() + '/app/fav/fav-clip-object.htm?fid=' + fid;
	});
	
	var option = {
		addBtn : $('#btnAdd'),
		editBtn : $('#btnEdit'),
		removeBtn : $('#btnDel'),
		formPanel : {		    
			panel : $('div.f-div'),
			closeBtn : $('div.f-div a'),
			commitBtn : $('div.f-div button'),
			favClipName : $('div.f-div input'),
			title:$('div.f-div-t b'),
			title1:$('div.f-div-t b')
		},
		checkBox : {
			rootCheckBoxEl : $('#selAll'),
			allChildCheckBoxEl : $("input[xname='childCheckBox']")
		},
		renderEvent : function() {
			var self = this;
			/*
			self.addBtn.click(function() {
				$('body').showShade();
				self.formPanel.panel.css({
					left : ($(document).width() - self.formPanel.panel.width()) / 2,
					top : 100
				}).show();
				self.formPanel.favClipName.focus();
			});
			*/
			self.formPanel.closeBtn.click(function() {
				$('body').hideShade();
				self.formPanel.panel.hide();
				self.formPanel.title1.text('增加收藏夹');
				self.formPanel.favClipName.val('');
			});
			
			self.formPanel.commitBtn.click(function() {
				var favClipName = self.formPanel.favClipName.val();
				if(!favClipName)
					return;
				self.formPanel.closeBtn.trigger('click');
				$('body').ajaxLoading('正在保存数据...');
				$.ajax({
					url : $.fn.getRootPath() + '/app/fav/fav-clip!addOrEditFavClip.htm',
					type : 'POST',
					timeout : 5000,
					dataType : 'json',
					data : {
						id : self.formPanel.commitBtn.attr('id'),
					    favClipName : $.trim(favClipName)
					},
					success : function(data, textStatus, jqXHR) {
						$('body').ajaxLoadEnd();
						if(data.success) {
							window.location.reload();
						} else {
							parent.layer.alert(data.message, -1)
						}
						self.formPanel.commitBtn.attr('id', '')
					},
					error : function(data, textStatus, errorThrown) {
						$('body').ajaxLoadEnd();
					}
				});
			});
			
			self.formPanel.favClipName.keydown(function(event) {
				if(event.keyCode == '13') {
					self.formPanel.commitBtn.trigger('click');
				}
			});
			
			self.checkBox.allChildCheckBoxEl.attr('checked', false);
			self.checkBox.rootCheckBoxEl.attr('checked', false).click(function(){
				var isCheck = $(this).attr('checked');
				if(isCheck) {
					self.checkBox.allChildCheckBoxEl.attr('checked', true);
				} else {
					self.checkBox.allChildCheckBoxEl.attr('checked', false);
				}
			});
			/*
			self.editBtn.click(function() {
				var checked = $('div.zhishi_con_nr :input[type=checkbox]:checked');
				if(checked.size() == 0) {
					parent.layer.alert('请选择一行进行修改', -1);
				} else if(checked.size() > 1) {
					parent.layer.alert('只能选择一行进行修改', -1);
				} else {
					self.addBtn.trigger('click');
					self.formPanel.favClipName.val(checked.parent().next().find('b').text());
					self.formPanel.title.text('修改收藏夹');
					self.formPanel.commitBtn.attr('id', checked.attr('id'));
				}
			});
			*/
			self.removeBtn.click(function() {
				var checked = $("input[xname='childCheckBox']:checked");
				if(checked.size()>0) {
					parent.layer.confirm('删除收藏夹同时会删除收藏夹下知识，是否继续?', function(index){
						parent.layer.close(index);
						var ids = '';
						for(var i = 0; i < checked.size(); i ++) {
							if(i == (checked.size() - 1)) 
								ids += $(checked[i]).attr('id');
							else 
								ids += $(checked[i]).attr('id') + ',';
						}
						$('body').ajaxLoading('正在删除数据...');
						$.ajax({
							url : $.fn.getRootPath() + '/app/fav/fav-clip!removeFavClip.htm',
							type : 'POST',
							timeout : 5000,
							dataType : 'json',
							data : {
								ids : ids
							},
							success : function(data, textStatus, jqXHR) {
								$('body').ajaxLoadEnd();
								if(data.success) {
									window.location.reload();
								} else {
									parent.layer.alert(data.message, -1)
								}
							},
							error : function(data, textStatus, errorThrown) {
								$('body').ajaxLoadEnd();
							}
						});
					});
				} else {
					parent.layer.alert('请选择要删除的收藏夹', -1);
				}
			});
		}
	}
	option.renderEvent();
	
	
	$("#btnAdd, #btnEdit").click(function(){
		var _elemId = $(this).attr("id");
		var _checkId = "";
		var _editFolderName = "";
		if (_elemId=="btnEdit"){
			var checked = $("input[xname='childCheckBox']:checked");
			if(checked.size()==0) {
				layer.alert('请选择一行进行修改', -1);
				return false;
			} else if(checked.size()>1) {
				layer.alert('只能选择一行进行修改', -1);
				return false;
			}
			_checkId = checked.attr("id");
			_editFolderName = checked.attr("xfoldername");
			$("#folderId").val(_checkId);
			$("#folderName").val(_editFolderName);
		}else{
			$("#folderId").val("");
			$("#folderName").val("");
		}
		
		var pageii = $.layer({
		    type: 1,
		    title: false,
		    area: ['auto', '100px'],
		    border: [10, 0.3, '#000'],
		    closeBtn: [0, true],
		    shift: 'left', //从左动画弹出
		    page: {
		        dom: $("table.kbs-dialog")
		    }
		});
		$("#btnSave").unbind("click");
		$("#btnSave").bind("click", function(){
			if ($("#folderName").val()==""){
				layer.alert("收藏夹名称不能为空", -1);
				return false;
			}
			$.ajax({
				url : $.fn.getRootPath() + '/app/fav/fav-clip!addOrEditFavClip.htm',
				type : 'POST',
				dataType : 'json',
				data : {
					id : _checkId,
				    favClipName : $.trim($("#folderName").val())
				},
				success : function(data, textStatus, jqXHR) {
					layer.close(pageii);
					if(data.success) {
						window.location.reload();
					} else {
						layer.alert(data.message, -1)
					}
				},
				error : function(data, textStatus, errorThrown) {
					$('body').ajaxLoadEnd();
				}
			}); //end ajax
		}); //end save
	}); //end edit or add
});