// Server Variable
var $CE = {
    $client: "wap",
    $ln: "cs",
    $serviceurl: "/data/",
    $localfilepath: "../",
    $picfiledomain: "../",
    $defaulcustomer: ""
};

// Error Page
function showerror(content) {
    var errormsg = $("#errormsg");
    errormsg.show();
    var errormsg_content = $("#errormsg_content");
    $("#errormsg_text").html(content);
    var top = $(window).scrollTop() + ($(window).height() / 2) - 90;
    errormsg_content.attr("style", "top:" + top.toString() + "px;");
}

// get Query Param
function getQueryParam() {
    if (window.location.href.indexOf('?') == -1)
        return null;
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        var key = hash[0].toLowerCase();
        var value = hash[1];

        if (value.indexOf('#') == value.length - 1) {
            value = value.substring(0, value.length - 1);
        }

        vars.push(key);
        vars[key] = value;
    }
    return vars;
}

// Like
function postlike(id, logid) {
    $("#btn_love").removeAttr("href");
    $.ajax({
        type: "get",
        dataType: "json",
        url: $CE.$serviceurl + "bizlike.aspx?id=" + id + "&LogId=" + logid,
        beforeSend: function(XMLHttpRequest) {
            //loadingObj.show();
        },
        success: function(data, textStatus) {
            if (data == null || data.length == 0) {
                showError();
                return;
            }
            if (data.errcode != "0") {
                return;
            }
            var retmsg = data.errmsg.toLowerCase();
            var liked = true;
            if (retmsg.indexOf("true") == -1) {
                liked = false;
            }
            if (liked) {
                $("#btn_love").removeClass("btn_love").removeClass("btn_loved").addClass("btn_loved").html("喜欢");
            }
            else {
                $("#btn_love").removeClass("btn_love").removeClass("btn_loved").addClass("btn_love").html("喜欢");
            }
            $("#btn_love").attr("href", "javascript:postlike('" + id + "', '" + logid + "');");
        },
        complete: function(XMLHttpRequest, textStatus) {
            //loadingObj.hide();
        },
        error: function() {
            showError();
            return;
        }
    });
}

Number.prototype.toFixed = function(num) {
    //重新构造toFixed方法,IE5.0+ 
    with (Math) this.NO = round(this.valueOf() * pow(10, num)) / pow(10, num);
    return String(/\./g.exec(this.NO) ? this.NO : this.NO + "." + String(Math.pow(10, num)).substr(1, num));
}

function tranSystemVersion(num) {
    var strV = "";
    if (num == "1")
        strV = "1";
    else if (num == "2")
        strV = "1.1";
    else if (num == "3")
        strV = "1.5";
    else if (num == "4")
        strV = "1.6";
    else if (num == "5")
        strV = "2";
    else if (num == "6")
        strV = "2.0.1";
    else if (num == "7")
        strV = "2.1";
    else if (num == "8")
        strV = "2.2";
    else if (num == "9")
        strV = "2.3";
    else if (num == "10")
        strV = "2.3.3";
    else if (num == "11")
        strV = "3";
    else if (num == "12")
        strV = "3.1";
    else if (num == "13")
        strV = "3.2";
    else if (num == "14")
        strV = "4.0.1";
    else if (num == "15")
        strV = "4.0.3";
    else if (num == "16")
        strV = "4.1";
    strV = strV + " 或更高";
    return strV;
}

// showtip
function showtip(content) {
    var showtip = $("#showtip");
    showtip.show();
    var showtip = $("#showtip_content");
    var top = $(window).scrollTop() + ($(window).height() / 2) - 90;
    showtip.attr("style", "top:" + top.toString() + "px;");
    showtip.html(content);
    setTimeout("hidetip()", 1500);
}
function hidetip() {
    var showtip = $("#showtip");
    showtip.hide();
}

// Loading
var loadingObj;
function loading(canvas, options) {
    this.canvas = canvas;
    if (options) {
        this.radius = options.radius || 12;
        this.circleLineWidth = options.circleLineWidth || 4;
        this.circleColor = options.circleColor || 'lightgray';
        this.dotColor = options.dotColor || 'gray';
    } else {
        this.radius = 12;
        this.circelLineWidth = 4;
        this.circleColor = 'lightgray';
        this.dotColor = 'gray';
    }
}
loading.prototype = {
    show: function() {
        var canvas = this.canvas;
        if (!canvas.getContext) return;
        if (canvas.__loading) return;
        canvas.__loading = this;
        var ctx = canvas.getContext('2d');
        var radius = this.radius;
        var rotators = [{ angle: 0, radius: 1.5 }, { angle: 3 / radius, radius: 2 }, { angle: 7 / radius, radius: 2.5 }, { angle: 12 / radius, radius: 3}];
        var me = this;
        canvas.loadingInterval = setInterval(function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var lineWidth = me.circleLineWidth;
            var center = { x: canvas.width / 2 - radius, y: canvas.height / 2 - radius };
            ctx.beginPath();
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = me.circleColor;
            ctx.arc(center.x, center.y, radius, 0, Math.PI * 2);
            ctx.closePath();
            ctx.stroke();
            for (var i = 0; i < rotators.length; i++) {
                var rotatorAngle = rotators[i].currentAngle || rotators[i].angle;
                var rotatorCenter = { x: center.x - (radius) * Math.cos(rotatorAngle), y: center.y - (radius) * Math.sin(rotatorAngle) };
                var rotatorRadius = rotators[i].radius;
                ctx.beginPath();
                ctx.fillStyle = me.dotColor;
                ctx.arc(rotatorCenter.x, rotatorCenter.y, rotatorRadius, 0, Math.PI * 2);
                ctx.closePath();
                ctx.fill();
                rotators[i].currentAngle = rotatorAngle + 4 / radius;
            }
        }, 50);
        var top = $(window).scrollTop() + ($(window).height() / 2) - 90;
        $("#loadingcanvas_container").show();
        $("#loadingcanvas").attr("style", "top:" + top.toString() + "px;");
        $("#loadingcanvas_text").attr("style", "top:" + (top + 70).toString() + "px;");
    },
    hide: function() {
        var canvas = this.canvas;
        canvas.__loading = false;
        if (canvas.loadingInterval) {
            window.clearInterval(canvas.loadingInterval);
        }
        var ctx = canvas.getContext('2d');
        if (ctx) ctx.clearRect(0, 0, canvas.width, canvas.height);
        $("#loadingcanvas_container").hide();
    }
};
(function($) {
    $.fn.extend({
        resizeImg: function(opt, callback) {
            var defaults = { w: 200, h: 150 };
            var opts = $.extend(defaults, opt);
            this.each(function() {
                $(this).load(function() {
                    var imgWidth = this.width, imgHeight = this.height;
                    if (imgWidth > opts["w"]) {
                        this.width = opts["w"];
                        this.height = imgHeight * (opts["w"] / imgWidth);
                        imgWidth = opts["w"];
                        imgHeight = this.height;
                    }
                    if (imgHeight > opts["h"]) {
                        this.height = opts["h"];
                        this.width = imgWidth * (opts["h"] / imgHeight);
                        imgHeight = opts["h"];
                        imgWidth = this.width;
                    }
                    //水平居中，垂直居中 
                    $(this).css({ "margin-top": (opts["h"] - imgHeight) / 2, "margin-left": (opts["w"] - imgWidth) / 2 });
                });
            });
        }
    });
})(jQuery);
