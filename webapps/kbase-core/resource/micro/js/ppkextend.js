//扩展静态方法
$.extend($, {
    //弹出消息
    ppkAlert: function (msg, flag) {
        $.ppkOverlay();
        $("body").append("<div class='ppkdialogCon'><div class='ppkalert'>" + msg + "</div></div>");
        if (!flag) {
            setTimeout(function () {
                $.ppkAlertRemove();
            }, 1500)
        }
    },
    //移除消息
    ppkAlertRemove: function () {
        $(".ppkdialogCon").remove();
        $(".ppkoverlay").remove();
    },
    ppkDialog: function (htm, flag) {
        $.ppkOverlay(flag);
        $("body").append("<div class='ppkdialogCon'>" + htm + "</div>");
    },
    //遮罩层  clickRemove 点击移除
    ppkOverlay: function (clickRemove) {
        $("body").append("<div class='ppkoverlay'></div>");
        if (clickRemove) {
            $(".ppkoverlay").bind("click", function () {
                $.ppkAlertRemove();
            });
        }
    },
    //ppkshare
    ppkShare: function () {
    },
	ppkShareGuide:function(act,keyword){
	},
    ppkWeiShare: function (options) {
        $.extend(dataForWeixin, options)
    },
    ppkBack: function () {
        $(".J_backsave").bind("click", function () {
            if (window.confirm("确定不保存离开吗？")) {
                return true;
            }
            return false;
        });
    },
    //获取url参数
    getQueryString: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(decodeURIComponent(r[2]));
        else
            return "";
    },
    disabled_A: function (tag) {
        $(tag).unbind("click").attr("href", "javascript:;").addClass("ppk-btn-hui");
    }
})

$.ppkBack();
var dataForWeixin = {
	path: location.href,
	title: document.title,
	desc: "",
	image:""
};
(function(){
   var onBridgeReady=function(){
	   WeixinJSBridge.on('menu:share:appmessage', function (argv) {
		   WeixinJSBridge.invoke('sendAppMessage', {
			   "link": dataForWeixin.path,
			   "desc": dataForWeixin.desc,
			   "title": dataForWeixin.title,
			   "img_url":dataForWeixin.image,
			   "img_width":"120",
			   "img_height":"120"
		   }, function (res) { });
	   });
	   WeixinJSBridge.on('menu:share:timeline', function(argv){
		  WeixinJSBridge.invoke('shareTimeline',{
			 "link": dataForWeixin.path,
			 "desc":dataForWeixin.desc,
			 "title":dataForWeixin.title
		  }, function(res){});
	   });
	   WeixinJSBridge.on('menu:share:weibo', function(argv){
		  WeixinJSBridge.invoke('shareWeibo',{
			 "content":dataForWeixin.title,
			 "url": dataForWeixin.path
		  }, function(res){});
	   });
	};
	if(document.addEventListener){
	   document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
	}else if(document.attachEvent){
	   document.attachEvent('WeixinJSBridgeReady'   , onBridgeReady);
	   document.attachEvent('onWeixinJSBridgeReady' , onBridgeReady);
	}
})();