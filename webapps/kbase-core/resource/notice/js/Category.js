var getCategory = (function(){
	var textbox = null;
	var requestKeyNew = "";
	var textboxCurrentValue;
	var requestKeyCurrent;
	var table = null;
	var tableStyle = null;
	var loadCssStyleFlg = false;
	var tableRows = null;
	var mouseoverflg = 0;
	selectIndex = -1, itemonmousedownFlg = 0;
	var selectRow = null;
	var keyDownCount = 0;
	var pressKeyUpOrKeyDownFlg = 0
	var ba = null;
	var loopHttpRequestAddress = null;
	var requestErrorCount = 0;
	var requestCount = 0;
	var responseKey = "";
	var timeoutVisibilityHidden = null;
	var callSuggestHandlerKey = "";
	var emptyText = '';
	var sItemMap = {}; //查询的条目
	var cItemMap = {}; //选中的条目
//	var textboxClass = 'key-word-filed';
	function dispose() {
		responseKey = "";
		textbox = null;
		requestKeyNew = "";
		table = null;
		tableStyle = null;
		loadCssStyleFlg = false;
		tableRows = null;
		mouseoverflg = 0;
		selectIndex = -1, itemonmousedownFlg = 0;
		selectRow = null;
		keyDownCount = 0;
		window.clearTimeout(loopHttpRequestAddress);
		window.clearInterval(ba);
		loopHttpRequestAddress = null;
		requestErrorCount = 0;
		requestCount = 0;
	}
	function init(vtextbox) {
		textbox = vtextbox;
		requestKeyNew = textboxCurrentValue = requestKeyCurrent = textbox.value;
	
		if (!vtextbox.inited) {
			textbox.autocomplete = "off";
			$(vtextbox).val('多个分类之间请用分号分格');
			$(vtextbox).css('color','#cccccc');
			
			bind(textbox, "blur", function() {
				pressKeyUpOrKeyDownFlg || visibilityHidden();
				pressKeyUpOrKeyDownFlg = 0;
				if($(vtextbox).val() == ''){
					$(vtextbox).val('多个分类之间请用分号分格');
					$(vtextbox).css('color','#cccccc');
				}
				return false;
			});
			
			bind(textbox, "focus", function() {
				if($(vtextbox).val() == '多个分类之间请用分号分格'){
					$(vtextbox).val('');
					$(vtextbox).css('color','');
				}
				return false;
			});
			
			bind(textbox, "beforedeactivate", function(e) {
				if (itemonmousedownFlg) {
					if (window.event) {
						window.event.cancelBubble = true;
						window.event.returnValue = false
					} else {
						e.stopPropagation();
						e.proventDefault();
					}
				}
				itemonmousedownFlg = 0
			});
			
//			$(textbox).keypress(function(keyArg) {
			$(textbox).keydown(function(keyArg) {
				window.__disableSuggest = false;
				var keyCode = keyArg.keyCode;
				if(keyCode) {
					if (keyCode == 27 && isVisibilityVisible()) {
						visibilityHidden();
//						setTextboxValue(requestKeyNew);
						keyArg.cancelBubble = true;
						return keyArg.returnValue = false
					}
					if (keyCode == 38 || keyCode == 40) {
						keyDownCount++;
						keyDownCount % 3 == 1 && keydownupHandler(keyCode);
						return false;
					}
					
					if (keyCode == 13) {
						if(textbox.value) {
//							visibilityHidden();
//							ASK(textbox.value);
//							normSearch(textbox.value);
						}
					}
				}
			});
			
			$(textbox).keyup(function(keyArg) {
				window.__disableSuggest = false;
				var keyCode = keyArg.keyCode;
				if(keyCode) {
					if (keyDownCount == 0 && !(keyCode == 38 || keyCode == 40))
						keydownupHandler(keyCode);
					keyDownCount = 0;
				}
				return false;
			});
			
			table = document.createElement("table");
			table.cellSpacing = table.cellPadding = "0";
			tableStyle = table.style;
			table.className = "gac_m";
			table.id ="suggestTbale";
			document.body.appendChild(table);
			vtextbox.inited = true;
		}
		
		//加载CSS
		if (!loadCssStyleFlg) {
			loadCssStyle();
			loadCssStyleFlg = true;
		}
		
		visibilityHidden();
		
		setPopupTablePosition();
		requestKeyNew = textboxCurrentValue = requestKeyCurrent = textbox.value;
		ba = window.setInterval(function() {
			var l = textbox.value;
			!(l == textboxCurrentValue || l == textboxCurrentValue+';') && keydownupHandler(0);
			textboxCurrentValue = l
		}, 2000);
		loopHttpRequest();
	}
	
	function keydownupHandler(keyCode) {
		if (keyCode == 38 || keyCode == 40) {
			pressKeyUpOrKeyDownFlg = 1;
			window.setTimeout(function() {
				textbox.focus()
			}, 10)
		}
		if (textbox.value != textboxCurrentValue) {
			requestKeyNew = textbox.value;
		}
		keyCode == 40 && showSelectItem(selectIndex + 1);
		keyCode == 38 && showSelectItem(selectIndex - 1);
		setPopupTablePosition();
		if (responseKey != requestKeyNew && !timeoutVisibilityHidden)
			timeoutVisibilityHidden = window.setTimeout(visibilityHidden, 100);
		textboxCurrentValue = textbox.value;
		textboxCurrentValue == "" && !loopHttpRequestAddress
				&& loopHttpRequest()
	}
	function showSelectItem(a) {
		if (!responseKey && requestKeyNew) {
			requestKeyCurrent = "";
			loopHttpRequest();
			return;
		}
		if (requestKeyNew != responseKey || !loopHttpRequestAddress)
			return;
		if (!tableRows || tableRows.length <= 0)
			return;
		if (!isVisibilityVisible()) {
			visibilityVisible();
			return;
		}
		var b = tableRows.length - 1;
		if (selectRow)
			selectRow.className = "gac_a";
		if (a == b || a == -1) {
			selectIndex = -1;
			setTextboxValue(requestKeyNew);
			textbox.focus();
			return;
		} else if (a > b)
			a = 0;
		else if (a < -1)
			a = b - 1;
		selectIndex = a;
		selectRow = tableRows.item(a);
		selectRow.className = "gac_b";
//		setTextboxValue(selectRow.completeString);
	}
	function loadCssStyle() {
		var a = [];
		function b(l, r) {
			a.push(l, "{", r, "}")
		}
		b(".gac_m","cursor:default;border:1px solid #90a8c1;z-index:10001;background:#EEEEEE;position:absolute;margin:7px 0 0 0;font-size:12px");
		b(".gac_m td", "line-height:15px");
		b(".gac_b", "background:#355fb1;color:#fff");
		var k = "padding-left:3px;white-space:nowrap;overflow:hidden;text-align:left;padding-bottom:1px";
		b(".gac_c", k);
		b(".gac_d","padding:0 3px; white-space:nowrap;overflow:hidden;text-align:right;color:green;font-size:0.77em");
		b(".gac_b td", "color:#fff");
		b(".gac_e","padding:0 3px 2px;text-decoration:underline;text-align:right;color:#00c;font-size:0.77em;line-height:0.88em");
		a = a.join("");
		var f = document.createElement("style");
		f.setAttribute("type", "text/css");
		document.getElementsByTagName("head")[0].appendChild(f);
		if (f.styleSheet) {
			f.styleSheet.cssText = a;
		} else {
			f.appendChild(document.createTextNode(a));
		}
	}
	function loopHttpRequest() {
		if (requestErrorCount >= 3)
			return;
		if (requestKeyCurrent != requestKeyNew && requestKeyNew) {
			var code13 = requestKeyNew.replace(new RegExp("\n", "g"), "");
			if (code13) {
				requestCount++;
				httpRequest(code13);
				textbox.focus();
			}
		}
		requestKeyCurrent = requestKeyNew;
		var a = 50;
		for ( var b = 1; b <= (requestCount - 2) / 2; ++b)
			a *= 2;
		a += 50;
		loopHttpRequestAddress = window.setTimeout(loopHttpRequest, a);
	}
	function httpRequest(a) {
		if (window.__disableSuggest)
			return false;
		callSuggestHandlerKey = a;
		
		var objects = a;
		if(a.indexOf(";")>-1){
			objects = a.substring(a.lastIndexOf(";")+1,a.length);
		}
		$.ajax({
			url :  $.fn.getRootPath()  + '/app/main/ontology-category!searchCategory.htm',
			type : 'post',
			timeout : 5000,
			dataType : 'json',
			data : {
				name : objects,
				type : "jeasyuitree"
			},
			success : function(data, textStatus, jqXHR) {
				createDataRowItems(data);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				return false;
			}
		});
	}
	function visibilityHidden() {
		if (timeoutVisibilityHidden) {
			window.clearTimeout(timeoutVisibilityHidden);
			timeoutVisibilityHidden = null
		}
		tableStyle && (tableStyle.visibility = "hidden");
	}
	function visibilityVisible() {
		tableStyle && (tableStyle.visibility = "visible");
		setPopupTablePosition();
		mouseoverflg = 1
	}
	function isVisibilityVisible() {
		return !!tableStyle && tableStyle.visibility == "visible"
	}
	function deleteAllTableRows() {
		if (table) {
			while (table.rows.length)
				table.deleteRow(-1);
		}
	}
	var getAbsoluteLeft = function(o) {
		oLeft = o.offsetLeft;
		while (o.offsetParent != null) {
			oParent = o.offsetParent;
			oLeft += oParent.offsetLeft;
			o = oParent;
		}
		return oLeft;
	};
	var getAbsoluteTop = function(o) {
		oTop = o.offsetTop;
		while (o.offsetParent != null) {
			oParent = o.offsetParent;
			oTop += oParent.offsetTop;
			o = oParent;
		}
		return oTop;
	};
	function setPopupTablePosition() {
		if (table) {
			var rawTxtBox = textbox;
			tableStyle.top = getAbsoluteTop(textbox)
					+ 20 + "px";
			tableStyle.left = getAbsoluteLeft(textbox)
					+ 'px';
			document.body.appendChild(table);
			tableStyle.width = rawTxtBox.offsetWidth + "px";
		}
	}
	function getOffsetFullValue(a, b) {
		var d = 0;
		while (a) {
			d += a[b];
			a = a.offsetParent
		}
		return d
	}
	function appendTextNode(a, b) {
		a.innerHTML = b;
	}
	function createDataRowItems(a) {
		requestCount > 0 && requestCount--;
		if (!table || callSuggestHandlerKey != requestKeyNew)
			return;
		if (timeoutVisibilityHidden) {
			window.clearTimeout(timeoutVisibilityHidden);
			timeoutVisibilityHidden = null;
		}
		responseKey = callSuggestHandlerKey;
		deleteAllTableRows();
		var showCloseFlg = false;
		//清空查询条目列表
		sItemMap = {};
		$(a).each(function(n,item){
		    //获取分类叶子节点
		    var bh = (item.attributes)?item.attributes.bh:"";
		    var _bhflag = (bh.length>0)?bh.substring(bh.length-1):"";
		    if(_bhflag == "L"){
				showCloseFlg = true;
				var drNew = table.insertRow(-1);
				drNew.onclick = function() {
					setTextboxValue(this.completeString);
					visibilityHidden();
				};
				drNew.onmousedown = itemonmousedown;
				drNew.onmouseover = itemonmouseover;
				drNew.onmousemove = function() {
					if (mouseoverflg) {
						mouseoverflg = 0;
						itemonmouseover.call(this)
					}
				};
				drNew.completeString = item.text;
				drNew.className = "gac_a";
				var tdKeyWord = document.createElement("td");
				appendTextNode(tdKeyWord, item.text);
				tdKeyWord.className = "gac_c";
				tdKeyWord.style.paddingTop = "2px";
				drNew.appendChild(tdKeyWord);
				var tdResult = document.createElement("td");
				appendTextNode(tdResult, "");
				tdResult.className = "gac_d";
				drNew.appendChild(tdResult);	
				//将查询的条目，写入查询条目列表
				sItemMap[item.text]	= item.id;    
		    }	 
		});
		if (showCloseFlg) {
			var drNew = table.insertRow(-1);
			drNew.onmousedown = itemonmousedown;
			var td = document.createElement("td");
			td.colSpan = 2;
			td.style.cursor = "pointer";
			td.align = 'right';
			td.style.fontSize = '12px';
			td.style.paddingRight = '2px';
			td.innerHTML = '关闭';
			drNew.className = "gac_e";
			drNew.appendChild(td);
			td.onclick = function() {
				visibilityHidden();
				responseKey = "";
				window.clearTimeout(loopHttpRequestAddress);
				loopHttpRequestAddress = null;
			}
		}
		selectIndex = -1;
		tableRows = table.rows;
		(tableRows && tableRows.length > 0 ? visibilityVisible
				: visibilityHidden)();
	}
	function setTextboxValue(newValue) {
		var divEle = document.createElement("div");
		divEle.innerHTML = newValue;
		if (textbox) {
			var val = divEle.innerText || divEle.textContent;
			textbox.value = textbox.value.substring(0,textbox.value.lastIndexOf(";")+1) + val+";";
			textboxCurrentValue = textbox.value;
			//将选中的条目，写入选中条目列表
		    if(sItemMap.hasOwnProperty(val)){
		        cItemMap[val] = sItemMap[val];
		    }
		}
	}
	function itemonmousedown(a) {
		itemonmousedownFlg = 1;
		return false
	}
	function itemonmouseover() {
		if (mouseoverflg)
			return;
		if (selectRow)
			selectRow.className = "gac_a";
		this.className = "gac_b";
		selectRow = this;
		for ( var a = 0, b; b = tableRows[a]; a++)
			b == selectRow && (selectIndex = a)
	}
	function bind(element, event, eventhandler) {// 给元素绑定事件
		// element.bind(event, eventhandler);
		element['on' + event] = eventhandler;
	}
	/**
	* 获取选中的分类ID
	*/
	function getCheckIds(){
	    var cates = $("#objects").val();
		var catelist = cates.split(";");
		var cateids = "";
		for(var index in catelist){
		    if(catelist.hasOwnProperty(index)){
		       var cate = catelist[index]
		       if(cItemMap.hasOwnProperty(cate)){
		           cateids += cItemMap[cate] + ";";
		       }
		    }
		}
		return cateids;
	}
	return {
		init : init,
		aiTipCallback : createDataRowItems,
		hide : visibilityHidden,
		checkIds : getCheckIds
	};
})();