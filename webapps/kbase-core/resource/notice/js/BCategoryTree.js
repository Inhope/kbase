$(function(){
	/////////////////////////////////////////////////	
	/////////////////地市树/////////////////////		
	blTreeInit();
	$('#blSelect').click(function(){
		var _offset = $(this).offset();
		var _scrollTop = $(document).scrollTop()
		$(this).next().css({width:$(this).outerWidth() + 'px', 
		                    left:_offset.left + 'px', 
		                    top:_offset.top - _scrollTop + $(this).outerHeight() + 'px'}).slideDown('fast');
		var _blval = $(this).val();
		if(_blval&&$.trim(_blval)!=''){
		   initNodeCheck('blTree', _blval);
		}	                    
	});
	$('#btnBLOk').click(function(){
	    var nodes = $.fn.zTree.getZTreeObj('blTree').getCheckedNodes(true);	
	    var _bls = '';
	    if(nodes!=null && nodes.length>0){
	       for(var i=0; i<nodes.length; i++){
	          var node = nodes[i];
	          if(node!=null && node.id!='root'){
	             _bls = (_bls=='')?node.name:_bls+','+node.name;
	          }
	       }
	    }
	    $('#blSelect').val(_bls);
	    $('#blSelect').next().hide();
	    return false;
	});
	$('#btnBLCancel').click(function(){
	    $('#blSelect').next().hide();
	});
	/////////////////////////////////////////////////
	/////////////////业务类型树//////////////////
	bcTreeInit();
	$('#bcSelect').click(function(){
		var _offset = $(this).offset();
		var _scrollTop = $(document).scrollTop()
		$(this).next().css({width:$(this).outerWidth() + 'px', 
		                    left:_offset.left + 'px', 
		                    top:_offset.top - _scrollTop + $(this).outerHeight() + 'px'}).slideDown('fast');
		var _bcval = $(this).val();
		if(_bcval&&$.trim(_bcval)!=''){
		   initNodeCheck('bcTree', _bcval);
		}                    
	});
	$('#btnBCOk').click(function(){
	    var nodes = $.fn.zTree.getZTreeObj('bcTree').getCheckedNodes(true);
	    var _bcs = '';
	    if(nodes!=null && nodes.length>0){
	       for(var i=0; i<nodes.length; i++){
	          var node = nodes[i];
	          if(node!=null && node.id!='root'){
	             _bcs = (_bcs=='')?node.name:_bcs+','+node.name;
	          }
	       }
	    }
	    $('#bcSelect').val(_bcs);
	    $('#bcSelect').next().hide();
	});
	$('#btnBCCancel').click(function(){
	    $('#bcSelect').next().hide();
	});
	/////////////////////////////////////////////
});
/** ***********初始化地市树************* */
function blTreeInit() {
	var setting = {
		edit : {
			enable : true,
			showRemoveBtn : false,
			showRenameBtn : false
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		async : {
			enable : true,
			url : $.fn.getRootPath() + '/app/notice/notice!busLocationTree.htm'
		},
		check: {
			enable: true,
			chkboxType : { "Y" : "s", "N" : "s" }
		},
		callback: {
			onAsyncSuccess: function(){return false;}
		}
	};
	$.fn.zTree.init($('#blTree'), setting);
}

/** ***********初始化业务类型树************* */
function bcTreeInit() {
	var setting = {
		edit : {
			enable : true,
			showRemoveBtn : false,
			showRenameBtn : false
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		async : {
			enable : true,
			url : $.fn.getRootPath() + '/app/notice/notice!busCategoryTree.htm'
		},
		check: {
			enable: true,
			chkboxType : { "Y" : "s", "N" : "s" }
		},
		callback: {
			onAsyncSuccess: function(){return false;}
		}
	};
	$.fn.zTree.init($('#bcTree'), setting);
}
/******************初始化Node选择******************************/
function initNodeCheck(_treeId, _selectId){
    var _treeObj = $.fn.zTree.getZTreeObj(_treeId);
	if(_treeObj&&_selectId&&$.trim(_selectId)!=''){
		var _selectId = ',' + $.trim(_selectId) + ',';
		var _nodes = _treeObj.getNodeByParam('id','root').children;
		if(_nodes && _nodes.length>0){
			for(var i=0; i<_nodes.length; i++){
			    var _node = _nodes[i];
			    if(_node!=null && _selectId.indexOf(_node.id)!=-1){
			       _treeObj.checkNode(_node,true);
			    }			      
			}
		}   
	}
}
