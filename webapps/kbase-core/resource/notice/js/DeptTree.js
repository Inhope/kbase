var setting = {
	view: {
			expandSpeed: "fast"
	},
	async : {
		enable : true,
		url : $.fn.getRootPath()+"/app/auther/dept!getDeptTree.htm",
		autoParam : ["id", "name=n","bh"],
		otherParam : {},
		dataFilter: ajaxDataFilter
	},
	check: {
		enable: true,
		chkboxType : { "Y" : "s", "N" : "s" }
	},
	callback : {
		onCheck : onCheck,
		//modify by eko.zhan at 2015-06-05 部门数据太多导致页面卡死
		//add by eko.zhan at 2015-06-16 屏蔽后出现的问题是选中部门并没有默认选中下级岗位，在发布时增加校验
		onAsyncSuccess: function(){return false;} //onAsyncSuccess
	}
	
};
function ajaxDataFilter(treeId, parentNode, responseData) {
    if (parentNode && parentNode.checked) {
      for(var i =0; i < responseData.length; i++) {
        //modify by heart.cao at 2015-09-06 如果节点为部门，默认不选中
        if(responseData[i].isParent=='false'
             || responseData[i].isParent==false){
           responseData[i].checked = true;
        }
      }
    }
    return responseData;
};



function onCheck(event, treeId, treeNode) {
	var treeObj = $.fn.zTree.getZTreeObj("noticeTree");
    if(treeNode.checked){
        //modify by heart.cao at 2015-09-06 点击选择框自动加载、展开子节点。
        if(treeNode.isParent=='true' 
           ||  treeNode.isParent==true){
	        treeObj.updateNode(treeNode);
		    treeObj.reAsyncChildNodes(treeNode, "refresh");
        }
    }else{
    	parentNodeUnChecked(treeNode);
    }
};

function parentNodeUnChecked(node){
	var treeObj = $.fn.zTree.getZTreeObj("noticeTree");
	if(node.getParentNode()){
		treeObj.checkNode(node.getParentNode(),false);
		parentNodeUnChecked(node.getParentNode());
	}
}


function asyncNodes(nodes) {
	var zTree = $.fn.zTree.getZTreeObj("noticeTree");
	if (nodes == null){
		zTree.reAsyncChildNodes(null, "refresh");
	}else{
		for (var i=0, l=nodes.length; i<l; i++) {
			zTree.reAsyncChildNodes(nodes[i], "refresh", true);
		}
	}
}


function onAsyncSuccess(event, treeId, treeNode, msg) {
	if(treeNode){
		asyncNodes(treeNode.children);
	}else{
		var treeObj = $.fn.zTree.getZTreeObj("noticeTree");
		asyncNodes(treeObj.getNodes());
	}
}


//function addStation(){
//	var nodesId= new Array();
//	var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
//	for(var i=0; i<nodes.length; i++){
//		if(!nodes[i].isParent){
//			nodesId.push(nodes[i].id);
//		}
//	}
//	$('#deptInput').val(nodesId);
//	$('#noticeAddStation').submit();
//}