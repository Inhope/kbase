$(function(){
	/***************加载树********************/
	$.fn.zTree.init($("#noticeTree"), setting);
	/**********************表单***************************/
	formInput();
	/**********************分页***************************/
	fenye();
	/************************删除置顶***************************/
	delAndTop();
	/************************新建*****************************/
	//addNotice();
	$('#btnNew').click(function(){
		var _title = '公告';
		if($.trim(_isNote)=='1')_title='便签';
		//modify by eko.zhan at 2016-01-08 在父页面中弹出公告
		parent.__kbs_layer_index = parent.$.layer({
			type: 2,
		    title: _title,
		    shade: [0.3, '#000'],
		    border: [5, 0.3, '#000'],
		    area: ['720px', '550px'],
		    iframe: {src: $.fn.getRootPath() + '/app/notice/notice!create.htm?isNote='+_isNote}
		});
	});
    var _tObj = parent.TABOBJECT;
    try{
       if(parent.parent.TABOBJECT!=undefined && parent.parent.TABOBJECT!=null)_tObj=parent.parent.TABOBJECT;
    }catch(e){}
	/*
	* 公告分类维护入口  add by heart.cao 20160104
	*/
	$('#btnCateWH').click(function(){
        var _url =  $.fn.getRootPath() + '/app/notice/notice-category!view.htm';
    	_tObj.open({
			id : 'notice-category',
			name : '分类维护',
			hasClose : true,
			url : _url,
			isRefresh : true
		}, this);	
	});
	
	/******************提示框*******************/
	if($('#objects')[0]){
		getObject.init($('#objects')[0]);
	}
	/*******************标题加载链接***********************/
	$('a.noticeTitle').each(function(){
		$(this).click(function(){
		    //公告内容在新页签中打开  modify by heart.cao 20160104
			var url =  $.fn.getRootPath()+'/app/notice/notice!notice.htm?id='+$(this).attr('noticeId')+'&hideReturn=1';
			var _title = '公告';
			var _tbId = 'notice-content';
			if($.trim(_isNote)=='1'){
			   _title='便签';
			   _tbId = 'note-content';
			}
			_title += '-' + $(this).attr('title');			
			_tObj.open({
				id : _tbId,
				name : _title,
				hasClose : true,
				url : url,
				isRefresh : true
			}, this);			 
		});
	});
	/*******************已读|未读关闭、居中**********************/
	$('div.index_dan_title a').click(function(){
		$('div.index_d').hide();
		$('div.shai_con table').html(' <tr class="shai_title">'+$('tr.shai_title').html()+'</tr>');
		$('body').hideShade();
	});
	var w = ($('body').width() - $('div.index_d').width())/2;
	$('div.index_d').css('left',w+'px');

});

function formInput(){
	$('body').ajaxLoadEnd();
	//选择 收件箱、发件箱 触发
	$('#select').change(function(){
		if($(this).val()==0 || $(this).val()=='收件箱'){
			$('body').ajaxLoading('正在提交数据...');
			window.location.href= $.fn.getRootPath()+'/app/notice/notice!list.htm?isNote='+_isNote;
		}
	});
	/*
	$('div.gonggao_titile_right a:eq(4)').click(function(){
		if($('table.shaixuan_overflow').is(':hidden')){
			$('table.shaixuan_overflow').show();
			$('#receiveForm input[name="filtrate"]').val('inline-block;');
		}else{
			$('table.shaixuan_overflow').hide();
			$('#receiveForm input[name="filtrate"]').val('none;');
		}
	});
	$('#startTimeForm').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#startTimeForm').next().css('position','relative').css('margin-top','9px');
	$('#startTimeForm').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	
	$('#endTimeForm').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#endTimeForm').next().css('position','relative').css('margin-top','9px');
	$('#endTimeForm').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	*/
	$('#receiveForm table a').each(function(index){
		if(index == 1){
			$(this).click(function(){
				$('#receiveForm input[name="title"]').val('');
				$('#receiveForm input[name="issueName"]').val('');
				$('#startTimeForm').val('');
				$('#endTimeForm').val('');
				$('#receiveForm select[name="pastEndTime"]').val('0');
			});
		}else if(index == 0){
			$(this).click(function(){
				/*
				var endTime = $('#endTimeForm').datebox('getValue');
				var startTime = $('#startTimeForm').datebox('getValue');
				if(startTime>endTime){
					parent.$.fn.hint('开始日期不能大于结束日期');
					return false;
				}
				*/
				$('#receiveForm input[name="start"]').val('0');
				$('#receiveForm').submit();
			});
		}
	});
}
function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
	var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
	var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
	var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
		
	});
	function fenyeSubmit(start){
		$('#receiveForm input[name="start"]').val(start);
		$('#receiveForm').submit();
	}
}

function delAndTop(){
	$('input[name="rowCheck"]').removeAttr('checked');
	$('#checkAll').click(function(){
		if($(this).attr('checked')){
			$('input[name="rowCheck"]').attr('checked','true');
		}else{
			$('input[name="rowCheck"]').removeAttr('checked');
		}
	});
	//删除
	$('#btnDelete').click(function(){
		var checkbox = $('input[name="rowCheck"]');
		var checkIds = new Array();
		checkbox.each(function(index){
			if($(this).attr('checked')){
				checkIds.push($(this).next().val());
			}
		});
		
		if(checkIds.length<1){
			parent.parent.$(document).hint("请选择要删除的行");
			return;
		}
		var r=confirm("确定删除 "+checkIds.length+" 条数据？");
	  	if (r==true){
	  		var idArray = '';
	  		for(var i = 0; i < checkIds.length; i++){
	  			if(i==0){
	  				idArray = checkIds[i];
	  			}else{
	  				idArray +="," + checkIds[i];
	  			}
	  		}
	    	$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/notice/notice!del.htm",
		   		data: {idArray:idArray},
		   		dataType:'json',
		   		async: true,
		   		success: function(msg){
		   			if(msg=='删除成功'){
		   				checkbox.each(function(index){
							if($(this).attr('checked')=='checked'){
								$(this).removeAttr('checked');
							}
						});
		   				window.location.reload();
		   			}else{
		   				parent.parent.$(document).hint("删除失败");
		   			}
		   		},
		   		error: function(){
		   			parent.parent.$(document).hint("删除失败");
		   		}
			});
	    }else{
	    	
	    }
	});
	
	//置顶
	$('#btnTop').click(function(){
		topNotice(1);	
	});
	//取消置顶
	$('#btnCancelTop').click(function(){
		topNotice(0);
	});
	function topNotice(top){
		var cancelTop = '';
		if(top == 0){
			cancelTop = '取消';
		}
		var checkbox = $('input[name="rowCheck"]');
		var checkIds = new Array();
		checkbox.each(function(index){
			if($(this).attr('checked')=='checked'){
				checkIds.push($(this).next().val());
			}
		});
		
		if(checkIds.length<1){
			parent.parent.$(document).hint("请选择要"+cancelTop+"置顶的行");
			return;
		}
		
		var r=confirm("确定"+cancelTop+"置顶 "+checkIds.length+" 条数据？");
	  	if (r==true){
	  		var idArray = '';
	  		for(var i = 0; i < checkIds.length; i++){
	  			if(i==0){
	  				idArray = checkIds[i];
	  			}else{
	  				idArray +="," + checkIds[i];
	  			}
	  		}
	    	$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/notice/notice!top.htm",
		   		data: {idArray:idArray,top:top},
		   		dataType:'json',
		   		async: true,
		   		success: function(msg){
		   			if(msg.indexOf('成功') > -1){
		   				checkbox.each(function(index){
							if($(this).attr('checked')=='checked'){
								$(this).removeAttr('checked');
							}
						});
		   				window.location.reload();
		   			}else if(msg.indexOf('置顶公告数量') > -1){
						parent.parent.$(document).hint(msg);
		   			}else{
		   				parent.parent.$(document).hint(msgcancelTop+"置顶失败");
		   			}
		   		},
		   		error: function(){
		   			parent.parent.$(document).hint(cancelTop+"置顶失败");
		   		}
			});
	    }else{
	    	
	    }
	}
	
	
}
function addNotice(){
	//新建
	
	$('div.gonggao_titile_right a:eq(3)').click(function(){
		var w = ($('body').width() - $('div.gonggao_d').width())/2;
		$('div.gonggao_d').css('left',w+'px');
		$('body').showShade();
		$('div.gonggao_d').show();
	});
	$('#closeNotice').click(function(){
		reNotice();
		$('#noticeTree').parent().hide();
		$('div.gonggao_d').hide();
		$('body').hideShade();
	});
	
	$('#depSelect').click(function(){
		if($('#noticeTree').parent().is(":hidden")){
//			var treeObj = $.fn.zTree.getZTreeObj("noticeTree");
//			asyncNodes(treeObj.getNodes());
			var h = 305;
			var w = $('body').width()/2-140;
			$('#noticeTree').parent().css('left',w+'px').css('top',h+'px');
			$('#noticeTree').parent().show();
		}
	});
	
	if($('#noticeTree').length > 0){
		$('*').bind('click', function(e){
			var self = $('#noticeTree').parent();
			if((self.offset().left <= e.pageX && e.pageX <= (self.offset().left + self.width()))) {
			
			} else {
				if(!self.is(':hidden'))
					self.hide();
					
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);		 
					//moidfy by eko.zhan at 2015-06-05 选择1个节点就为已选择
					if(nodes.length>0){
						$('#depSelect').val('已选择');
					}else{
						$('#depSelect').val('请选择');
					}
			}
		});
		$('*').bind('focus', function(e){
			var self = $('#noticeTree').parent();
			if((self.offset().left <= e.pageX && e.pageX <= (self.offset().left + self.width()))) {
			
			} else {
				if(!self.is(':hidden'))
					self.hide();
					var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
					//moidfy by eko.zhan at 2015-06-05 选择1个节点就为已选择
					if(nodes.length>0){
						$('#depSelect').val('已选择');
					}else{
						$('#depSelect').val('请选择');
					}
			}
		});
	}
	
	$('#noticeTree').prev().click(function(){
		var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
		//moidfy by eko.zhan at 2015-06-05 选择1个节点就为已选择
		if(nodes.length>0){
			$('#depSelect').val('已选择');
		}else{
			$('#depSelect').val('请选择');
		}
		$('#noticeTree').parent().hide();
	});
	
	$('#startTime').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#startTime').next().css('position','relative');
	$('#startTime').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	$('#startTime').next().children('span').css('float','right');
	
	$('#endTime').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#endTime').next().css('position','relative');
	$('#endTime').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	$('#endTime').next().children('span').css('float','right');
	
	
	$('#resetBtn').click(function(){
		reNotice();
	});
	$('#issueBtn').click(function(){
		var title = $('#title').val();
		var content = $('#content').val();
		var objects = $('#objects').val();
		if(objects == '多个实例之间请用分号分格')
			objects = '';
		var type1 = $('#type1');
		var type2 = $('#type2');
		var type3 = $('#type3');
		
		var type = 0;
		
		if(type1.attr('checked')=='checked'){
			type += 1;
		}
		if(type2.attr('checked')=='checked'){
			type += 2;
			
		}
		
		var startTime = $('#startTime').datebox('getValue');
		var endTime = $('#endTime').datebox('getValue');
		var tipNotice = $('#tipNotice');
		
		var statistics = 0;
		var top = 0;
		if($('#statistics').attr('checked')=='checked'){
			statistics = 1;
		}
		if($('#top').attr('checked')=='checked'){
			top = 1;
		}
		
		var nodes = $.fn.zTree.getZTreeObj("noticeTree").getCheckedNodes(true);
		var depStr="";
		
//		for(t=0;t<nodes.length;t++){
//		   depStr+=nodes[t].id+",";
//		}
		//modify by eko.zhan at 2015-06-16 部门岗位采用异步加载，如果未完成加载当管理员选择父级部门，不会默认选中岗位
		var stationsId = ''
		for(var i=0; i<nodes.length; i++){
			depStr+=nodes[i].id+",";
			if(!nodes[i].isParent){
				if(stationsId == ''){
					stationsId += nodes[i].id;
				}else{
					stationsId += ','+nodes[i].id;
				}
			}
		}
		//去掉逗号
		if (depStr.length>0){
			depStr = depStr.substring(0, depStr.length-1);
		}
		
		if( title==null || $.trim(title)=='' ){
			tipNotice.html('* 标题不能为空');
		}else if( content==null || $.trim(content)=='' ){
			tipNotice.html('* 内容不能为空');
		}else if(nodes.length < 1){
			tipNotice.html('* 发布范围不能为空');
		}else if (stationsId==''){
			tipNotice.html('* 发布范围请勾选岗位');
		}else if( startTime==null || $.trim(startTime)=='' || endTime==null || $.trim(endTime)==''){
			tipNotice.html('* 公告周期不能为空');
		}else if(testDate(startTime,endTime)){
			tipNotice.html('* 开始时间不能晚于结束时间');
		}else{
			$('body').hideShade();
			$('body').ajaxLoading('正在提交数据...');
			
			//验证实例
			var flag = false;
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/notice/notice!testObject.htm",
		   		data: {
		   			objects : objects
		   		},
		   		async: false,
		   		success: function(msg){
		   			if(msg=='"ok"'){
		   				flag = true;
		   			}else{
		   				tipNotice.html(msg.replace("\"","").replace("\"",""));
		   			}
		   		},
		   		error: function(){
		   		}
			});
			if(!flag){
				$('body').ajaxLoadEnd();
				$('body').showShade();
				return;
			}
			$('div.gonggao_d').hide();
			$('body').hideShade();
			
			tipNotice.html('');
			
			//提交数据
			$('body').ajaxLoading('正在提交数据...');
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/notice/notice!add.htm",
		   		data: {
		   			title : title,
		   			content : content,
		   			objects : objects,
		   			type : type,
		   			top : top,
		   			startTime : startTime,
		   			endTime : endTime,
		   			statistics : statistics,
		   			stationsId : stationsId,
		   			depScope:depStr
		   		},
		   		async: true,
		   		success: function(msg){
		   			parent.parent.$(document).hint(msg.replace("\"","").replace("\"",""));
		   			$('body').ajaxLoadEnd();
		   			reNotice();
		   			window.location.reload();
		   		},
		   		error: function(){
		   			parent.parent.$(document).hint("发布失败");
		   			$('body').ajaxLoadEnd();
		   		}
			});
			
		}
		function testDate(startTime,endTime){
			var start = startTime.split("-");
			var end = endTime.split("-");
			if(start[0] > end[0]){
				return true;
			}else if( start[0] == end[0] && start[1] > end[1]){
				return true;
			}else if( start[1] == end[1] && start[2] > end[2]){
				return true;
			}
			return false;
		}
		
	});
	
	function reNotice(){
		$('#title').val('');
		$('#content').val('');
		$('#objects').val('');
		$('#type2').removeAttr('checked');
		$('#type2').removeAttr('checked');
		$('#top').removeAttr('checked');
		$('#startTime').datebox('setValue','');
		$('#endTime').datebox('setValue','');
		$('#statistics').removeAttr('checked');
		$('#tipNotice').html('');
		
		$.fn.zTree.getZTreeObj("noticeTree").checkAllNodes(false);
		
		$('#depSelect').val('请选择');
	}
}


/*************************已阅|未阅**************************/
function readList(type,id){
		var _title = '未阅人员列表';
		if($.trim(type)=='1')_title='已阅人员列表';
		//modify by eko.zhan at 2016-01-08 在父页面中弹出公告
		parent.__kbs_layer_index = parent.$.layer({
			type: 2,
		    title: _title,
		    shade: [0.3, '#000'],
		    border: [5, 0.3, '#000'],
		    area: ['720px', '500px'],
		    iframe: {src: $.fn.getRootPath() + '/app/notice/notice!readers.htm?id='+id+"&type="+type+""}
		});
    /*
	$('body').ajaxLoading('正在提交数据...');
	$.ajax({
   		type: "POST",
   		url: $.fn.getRootPath()+"/app/notice/notice!readList.htm",
   		data: {
   			id : id,
   			type : type
   		},
   		async: false,
   		success: function(msg){
   			$('body').ajaxLoadEnd();
   			if(msg.indexOf('<tr>')>-1){

   				//$('div.shai_con table').html($('div.shai_con table').html()+msg);
   				//$('body').ajaxLoadEnd();
   				//$('body').showShade();
   				//$('div.index_d').show();

   				$('#readListDiv table').find('tr[id!="readListTR"]').remove();
   				$('#readListTR').after(msg);
   				var _readHtml = $('#readListDiv').html();
   				$.layer({
					type: 1,
				    shade: [0.3, '#000'],
				    area: ['500px', 'auto'],
				    title: false,
				    border: [5, 0.3, '#000'],
				    //page: {html : _readHtml}
				    page: {dom : '#readListDiv'}
   				});
   			}else{
   				parent.parent.$(document).hint("获取失败");
   			}
   		},
   		error: function(){
   			parent.parent.$(document).hint("获取失败");
   			$('body').ajaxLoadEnd();
   		}
	});*/
}