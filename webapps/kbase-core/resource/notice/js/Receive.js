$(function(){
	/**********************表单***************************/
	formInput();
	/**********************分页***************************/
	fenye();
	/*******************标题加载链接***********************/
	$('a.noticeTitle').each(function(){
		$(this).click(function(){
		    var count = parent.$(".individual_ico5").find(".individual_circle").html();
		    if(count!=null && count!="99+" && count>1){
		       parent.$(".individual_ico5").find(".individual_circle").html(count-1)
		    }
		    if(count!=null && count!="99+" && count==1){
		       parent.$(".individual_ico5").find(".individual_circle").remove();
		    }
		    //公告内容在新页签中打开  modify by heart.cao 20160104
		    var _tObj = parent.TABOBJECT;
		    try{
		       if(parent.parent.TABOBJECT!=undefined && parent.parent.TABOBJECT!=null)_tObj=parent.parent.TABOBJECT;
		    }catch(e){}			    
			var _url = $.fn.getRootPath()+'/app/notice/notice!notice.htm?id='+$(this).attr('noticeId')+'&isNote='+$.trim(_isNote)+'&hideReturn=1';
			var _title='公告';
			var _tbId = 'notice-content';
			if($.trim(_isNote)=='1'){
			   _title='便签';
			   _tbId = 'note-content';
			}
			_title += '-' + $(this).attr('title');					
			_tObj.open({
				id : _tbId,
				name : _title,
				hasClose : true,
				url : _url,
				isRefresh : true
			}, this);		    		
		});
	});
});
function formInput(){
	$('body').ajaxLoadEnd();

	//选择 收件箱、发件箱 触发
	$('#select').change(function(){
		if($(this).val()==1 || $(this).val()=='发件箱'){
			$('body').ajaxLoading('正在提交数据...');		
			window.location.href= $.fn.getRootPath()+'/app/notice/notice!send.htm?isNote='+_isNote;
		}
	});
	
	$('div #gonggao_titile_right').click(function(){
		if($('table.shaixuan_overflow').is(':hidden')){
			$('table.shaixuan_overflow').show();
			$('#receiveForm input[name="filtrate"]').val('inline-block;');
		}else{
			$('table.shaixuan_overflow').hide();
			$('#receiveForm input[name="filtrate"]').val('none;');
		}
	});
	/*
	$('#startTimeForm').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#startTimeForm').next().css('position','relative').css('margin-top','0px');
	$('#startTimeForm').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	
	$('#endTimeForm').datebox({
		required : false,
		closeText : '关闭',
		currentText : '今天',
		editable : false,
		formatter :function(e){
			var year = e.getFullYear();
			var month = e.getMonth()+1;
			if(month <= 9){
				month = '0' + month;
			}
			var date = e.getDate();
			if(date <= 9){
				date = '0' + date;
			}
			return year+'-'+month+'-'+date;
		}
	});
	$('#endTimeForm').next().css('position','relative').css('margin-top','0px');
	$('#endTimeForm').next().children().css('border','0').css('padding-left','0').css('posotion','absolute').css('float','left');
	*/
	/**
	* 提交
	*/
	$('#btnTJ').click(function(){
		$('#receiveForm input[name="start"]').val('0');
		$('#receiveForm input[name="busLocation"]').val($('#blSelect').val());
		$('#receiveForm input[name="busCategory"]').val($('#bcSelect').val());
		$('#receiveForm').submit();	
	});	
	/**
	* 重置
	*/
	$('#btnCZ').click(function(){
		$('#receiveForm input[name="title"]').val('');
		$('#receiveForm input[name="issueName"]').val('');
		$('#startTime').val('');
		$('#endTime').val('');	
		$('#receiveForm select[name="pastEndTime"]').val('0');
		$('#receiveForm select[name="pastReadTime"]').val('0'); 
		if($('#blSelect'))$('#blSelect').val(''); 
		if($('#bcSelect'))$('#bcSelect').val(''); 
		return false;
	});
	
	$('#receiveForm table a').each(function(index){
		if(index == 1){
			$(this).click(function(){
				$('#receiveForm input[name="title"]').val('');
				$('#receiveForm input[name="issueName"]').val('');
				$('#startTime').val('');
				$('#endTime').val('');
			});
		}else if(index == 0){
			$(this).click(function(){
				//var endTime = $('#endTimeForm').datebox('getValue');
				//var startTime = $('#startTimeForm').datebox('getValue');
				//if(startTime>endTime){
				//	parent.$.fn.hint('开始日期不能大于结束日期');
				//	return false;
				//}
				$('#receiveForm input[name="start"]').val('0');
				$('#receiveForm').submit();
			});
		}
	});
}
function fenye(){
	var btns = $('div.gonggao_con_nr_fenye a');
	var start = parseInt($('div.gonggao_con_nr_fenye').attr('start'));
	var limit = parseInt($('div.gonggao_con_nr_fenye').attr('limit'));
	var currentPage = parseInt($('div.gonggao_con_nr_fenye').attr('currentPage'));
	var totalPage = parseInt($('div.gonggao_con_nr_fenye').attr('totalPage'));
	btns.each(function(index){
		var btn= $(this);
		if(btn.html().indexOf('上一页') > -1 ){
			if( currentPage > 1){
				btn.click(function(){
					var prev = parseInt(start)-parseInt(limit);
					fenyeSubmit(prev);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('下一页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var next = parseInt(start)+parseInt(limit);
					fenyeSubmit(next);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('首页') > -1 ){
			if(currentPage > 1){
				btn.click(function(){
					fenyeSubmit(0);
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else if(btn.html().indexOf('尾页') > -1 ){
			if(currentPage < totalPage){
				btn.click(function(){
					var last = parseInt(limit)*(parseInt(totalPage)-1);
					fenyeSubmit(last);	
				});
			}else{
				btn.css('text-decoration','none').css('cursor','default');
			}
		}else {
			var text = $(this).html();
			if(text != currentPage){
				var click = parseInt(limit)*(parseInt(text)-1);
				btn.click(function(){
					fenyeSubmit(click);
				});
			}
		}
		
	});
	function fenyeSubmit(start){
		$('#receiveForm input[name="start"]').val(start);
		$('#receiveForm').submit();
	}
}