$(function(){
	/********************页面初始化*******************************/
	init();
	/********************点击事件*******************************/
	tagClick();
	/********************附件预览*******************************/
	openAttachment();
	openP4();
	/********************其他平台*******************************/
	otherPlatform();
	/*********************短信**********************************/
	//message();
	/*********************右键**********************************/
	//rightClick();
	/*********************答案反馈*********************************/
	//unsolved();
	/***********************指定业务*******************************/
	zdyw();
	/****************************全屏显示*********************************/
	doFullScreen();
	

	//选择更多实例
	moreObject();	
});
var tag1=0;
function init(){
	$('ul.gzul li:eq(0)').attr('class','active');
	/*$('div.gz-cn2').each(function(index){
		if(index!=0){
			$(this).css('display','none');
		}
	});
	*/
	//modify by eko.zhan at 2015-12-30 11:35 隐藏除了第一个元素分类展示面板以外的所有面板，作用与上面注释掉的代码一样
	$('div.gz-cn2').not($('div.gz-cn2:eq(0)')).hide();
	
	
	//modify by eko.zhan at 2015-12-30 11:40 
	//修改分类展示面板高度，适应屏幕，在IE7上获取parent耗时导致页面加载卡顿，这是一个很蛋疼的问题，所以获取screen高度
	var height = screen.height - 348;
	if (!window.__kbs_has_parent){
		//全屏显示
		height += 100;
	}
	$('.gzcontent .gz-cn2').height(height);
	
	
	
}

function tagClick(){
	$('ul.gzul li').each(function(index){
		$(this).click(function(){
			$('ul.gzul li').removeAttr('class','active');//去除所有tag1高亮
			$(this).attr('class','active');//点击的tag1高亮
			$('div.gz-cn2').css('display','none');//隐藏所有主div
				if(index!=$('ul.gzul li').length-1){
				$('div.gz-cn2').each(function(i){
					if($(this).attr('tag1') == index && "0" == $(this).attr('tag2')){//点击对应tag1的全文显示
						tag1 = $(this).attr('tag1');
						
						//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
						window.__kbs_global_finder_id = $(this).attr('id');
						window.__kbs_global_finder_index = 0;
						window.__kbs_global_finder_order = 1;
						reloadRng(window.__kbs_global_finder_id, $('.kbs-global-finder-keyword').val());
						
						$(this).css('display','block');
					}
				});
				$('ul.gzul02 li').removeAttr('class','active');//去除所有tag2高亮
				$('ul.gzul02 li').each(function(index){//点击的tag2高亮
					if($(this).attr('tag2') == "0"){
						$(this).attr('class','active');
					}
				});
			}else{
				$('div.gz-cn2').last().show();
				
				//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
				window.__kbs_global_finder_id = $('div.gz-cn2').last().attr('id');
				window.__kbs_global_finder_index = 0;
				window.__kbs_global_finder_order = 1;
				reloadRng(window.__kbs_global_finder_id, $('.kbs-global-finder-keyword').val());
			}
			
			_flushBodyHeight();
		});
	});
	
	
	$('ul.gzul02 li').each(function(index){
		$(this).click(function(){
			var tag2 = $(this).attr('tag2');
			$('ul.gzul02 li').removeAttr('class','active');//去除所有tag2高亮
			$('ul.gzul02 li').each(function(index){//点击的tag2高亮
				if($(this).attr('tag2') == tag2){
					$(this).attr('class','active');
				}
			});
			$('div.gz-cn2').css('display','none');//隐藏所有主div
			$('div.gz-cn2').each(function(i){
				if(tag1 == $(this).attr('tag1') && tag2 == $(this).attr('tag2')){
					
					//add by eko.zhan at 2015-10-28 15:15 页面关键字搜索功能
					window.__kbs_global_finder_id = $(this).attr('id');
					window.__kbs_global_finder_index = 0;
					window.__kbs_global_finder_order = 1;
					reloadRng(window.__kbs_global_finder_id, $('.kbs-global-finder-keyword').val());

					$(this).css('display','block');
				}
			});
			
			_flushBodyHeight();
		});
	});
	
}

/**
 * @author eko.zhan at 2015-10-12 14:30
 * 重新计算页面高度，修正滚动条高度
 */
function _flushBodyHeight(){
	//modify by eko.zhan at 2015-10-28 15:15 高度由内部页签决定不刷新父层的滚动条高度
	return false;
	//add by eko.zhan at 2015-10-09 20:05 
	$(parent.document).find('.content_content').find('iframe:visible').height($('body').get(0).scrollHeight);
	
	//
	//没有滚动条的页签切换时导致工具条不显示 modify by eko.zhan at 2015-10-10 13:27
	if (document.body.scrollHeight<$(parent).height()){
		if (window.__kbs_toolbar_top_height==undefined){
			window.__kbs_toolbar_top_height = $('.kbs-global-gztable-css').css('top');
		}
		$('.kbs-global-gztable-css').css('top', document.body.scrollHeight-50);
	}else if (window.__kbs_toolbar_top_height!=undefined){
		$('.kbs-global-gztable-css').css('top', window.__kbs_toolbar_top_height);
	}
}

/************请求action获取category路径***************/
function navigate_init(robot_context_path,categoryId){
/*
	var data = {
		'format':'json','async':true,'jsoncallback':'navigateCb','nodeid':categoryId,'nodetype':3
	};
	var url = robot_context_path+'p4pages/related-category.action'+'?format=json&async=true&nodeid='+categoryId+'&ts='
			+ new Date().getTime()+'&jsoncallback=navigateCb&nodetype=1';
	var script = document.createElement('script');  
	script.setAttribute('src', url);  
	document.getElementsByTagName('head')[0].appendChild(script);*/
	//modify by eko.zhan at 2015-09-22 16:15
	var data = {'format':'json', 'async':true, 'nodeid':categoryId, 'nodetype':1};
	$.getJSON(robot_context_path + 'p4pages/related-category.action?jsoncallback=?', data, function(arr){
		if (arr!=null){
			$(arr).each(function(i, item){
				//name id type bh
				$('#categoryNv').append('<a href="javascript:void(0)" categoryId="' + item.id + '"> > ' + item.name + '</a>');
			});
		}
	});
}
window.navigateCb = function(data) {
	if(data){
		var navigateJObj = $('#categoryNv');
		for(var i=0;i<data.length;i++){
			var aJObj;
			aJObj = $('<a/>').attr('href','javascript:void(0);').attr('categoryId',data[i]['id']).html(">"+data[i]['name']);
			navigateJObj.append(aJObj);
		}
//		navigateJObj.children('a').click(function(){
//			openOv('',$(this).attr('categoryId'));
//		});
	}
}

function openAttachment(){
	$('a.openAttachment').each(function(){
		$(this).click(function(){
			var _this = this;
			//var id = $(this).attr('filename');
			var _filename = $(_this).attr('_name');
           	var _fileid = $(_this).attr('filename');
           	var _filetype = '';
           	if (_filename.lastIndexOf('.')!=-1){
           		_filetype = _filename.substring(_filename.lastIndexOf('.'));
           	}
           	var fileName = _fileid + _filetype;
			/*$.getJSON(swfAddress+'?format=json&async=true&fileName='+fileName+'&callbackparam=?', function(data){
				if (data==null || data=="null") return false;
				var s = data;
				s = s.substring(s.indexOf('/')+1,s.length);
				s = swfAddress.substring(0,swfAddress.indexOf('attachment')) + s;
				window.parent.open(s);
			});*/
           	window.open(swfAddress + '?fileName='+fileName);
		});
		
	});
	$('a.showAttachment').each(function(){
		var self = $(this);
		$(this).click(function(){
			var _this = this;
			var _filename = $(_this).attr('_name');
           	var _fileid = $(_this).attr('filename');
           	var _filetype = '';
           	if (_filename.lastIndexOf('.')!=-1){
           		_filetype = _filename.substring(_filename.lastIndexOf('.'));
           	}
           	var fileName = _fileid + _filetype;
			if(self.attr('open') != 'open'){
				self.parent().find('a').removeAttr('open');
				self.parent().prev().find('a').removeAttr('open');
				self.attr('open','open');
				var id = self.attr('filename');
				self.parent().next().html('<iframe src="'+swfAddress   + '?fileName='+fileName+'"></iframe>');
			}else{
				self.removeAttr('open');
				self.parent().next().html('');
			}
		});
		
	});
}

function openP4(){
	$('a.showP4').each(function(){
		var self = $(this);
		$(this).click(function(){
			/* hidden by eko.zhan at 2015-10-28
			var _this = this;
			var _parent = $(_this).parents('div.yulan');
			if ($(_this).attr('open')!='open'){
				$(_this).attr('open', 'open');
				_parent.append('<div name="p4div"><iframe src="' + $(_this).attr('url') + '"></iframe></div>')
			}else{
				$(_this).removeAttr('open');
				_parent.find('div[name="p4div"]:last').remove();
			}*/
			if(self.attr('open') != 'open'){
				self.parent().find('a').removeAttr('open');
				self.parent().next().find('a').removeAttr('open');
				self.attr('open','open');
				var url = self.attr('url');
				self.parent().next().next().html('<iframe src="'+url+'"></iframe>');
			}else{
				self.removeAttr('open');
				self.parent().next().next().html('');
			}
		});
		
	});
}

function otherPlatform(){
	$('div.increase_dan_title').click(function(){
		$('div.increase_d').hide();
	});
	$('a.other_platform').click(function(){
		/*
		$('div.increase_d').hide();
		var valueId = $(this).attr('valueId');
		$.ajax({
	   		type: "POST",
	   		url: $.fn.getRootPath()+"/app/category/all-ans.htm",
	   		data: {valueId:valueId},
	   		dataType:'json',
	   		async: true,
	   		success: function(msg){
	   			if(msg && msg.length>0){
	   				var s = '';
	   				for(var i=0;i<msg.length;i++){
	   					s += '<tr>';
	   					s += '<td width="22%" height="0" align="left" valign="middle">';
	   					s += msg[i].platform;
	   					s += '</td>';
	   					s += '<td width="78%" height="0" align="left" valign="middle">';
	   					s += msg[i].ans;
	   					s +='</td></tr>';
	   				}
	   				$('div.increase_d tbody').html(s);
	   				var scrollTop = parseInt(window.parent.document.documentElement.scrollTop);
		   			$('div.increase_d').css('top',50+scrollTop+'px');
		   			$('div.increase_d').show();
	   			}else{
	   				parent.$(document).hint("没有其他平台答案");
	   			}
	   		},
	   		error :function(){
	   			parent.layer.alert('获取失败');
	   		}
		});
		*/
		var valueid = $(this).attr('valueId');
		_showModalDialog({url: $.fn.getRootPath() + '/app/category/all-ans!view.htm?valueId=' + valueid});
					
	});
}


function message(){
	$('input.check').click(function(){
		var valueId = $(this).attr('valueId');
		if($(this).attr('checked')){
			$('input.check').each(function(){
				if($(this).attr('valueId')==valueId){
					$(this).attr('checked',true);
				}
			});
		}else{
			$('input.check').each(function(){
				if($(this).attr('valueId')==valueId){
					$(this).removeAttr('checked');
				}
			});
		}
	});
	
}

function rightClick(){
	var _objid = '';			//实例id
	var _valueid = '';			//标准问id
	var _ques = '';				//标准问
	var _answer = '';			//标准答案
	var _answerHtml = '';
	
	var x;var y;
	/*********************右键菜单事件**********************************/
	menuClick();
	//阻止页面默认右键事件
	document.oncontextmenu=function(e){
		if(e && e.preventDefault){
			//阻止默认浏览器动作(W3C) 
			e.preventDefault(); 
		}else{
			//IE中阻止函数器默认动作的方式 
			window.event.returnValue = false; 
		}
	};
	
	//表格第二列（答案列）绑定右键事件
	$('div.gzform01 table tr td[class="answer"]').each(function(i, item){
		//alert($(item).text());
		
		$(this).mousedown(function(e){
			if(e.button == 2){	//右键
				_objid = $(item).attr('_oid');
				_ques = $.trim($(item).prev().find('span.kbs-question').text());
				_answer = $.trim($(item).find('div:first').text());
				_answerHtml = $.trim($(item).find('div:first').html());
				_valueid = $(item).prev().prev().text()	//标准问id
				
				showMenu(e);
				//设置答案反馈的值
				$('#corrform input[name="question"]').val($(this).prev().html());
				$('#corrform input[name="value_id"]').val($(this).prev().prev().html());
				askContent=$(this).prev().html();
				faqId=$(this).prev().prev().html();
				
				//针对IE浏览器增加 复制 功能
				if (!!window.ActiveXObject || "ActiveXObject" in window){
					if ($('#navCopy').length==0){
						$('#menu ul').append('<li><a href="javascript:void(0);" id="navCopy">复制</a></li>');
					}
				}
				
				//点击右键时当前标准问对象(右键发送短信用),Category.jsp右键事件赋值
				gzyd_sms_checkbox_obj = $(this).next().next().children("input[type='checkbox']");
				if(gzyd_sms_checkbox_obj){
					$('#menu ul').find("li[id='gzyd_sms_checkbox_now']").remove();
					var isableSMS = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isableSMS");
					if(isableSMS){
						$('#menu ul').append('<li id="gzyd_sms_checkbox_now" onclick="javascript:GzydMappings.rightClick_duanxin();"><a href="###">发送短信</a></li>');
					}
				}
			}
		});
	});
	
	/* hidden by eko.zhan at 2015-08-17 绑定事件查找td元素循环次数太多影响js性能
	$('tr').each(function(){
		$(this).find('td').each(function(index){
			if(index==2){
				$(this).mousedown(function(e){
					if(e.button == 2){
						showMenu(e);
						//设置答案反馈的值
						$('#corrform input[name="question"]').val($(this).prev().html());
						$('#corrform input[name="value_id"]').val($(this).prev().prev().html());
						askContent=$(this).prev().html();
						faqId=$(this).prev().prev().html();
						
						//点击右键时当前标准问对象(右键发送短信用),Category.jsp右键事件赋值
						gzyd_sms_checkbox_obj = $(this).next().next().children("input[type='checkbox']");
						if(gzyd_sms_checkbox_obj){
							$('#menu ul').find("li[id='gzyd_sms_checkbox_now']").remove();
							var isableSMS = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isableSMS");
							if(isableSMS){
								$('#menu ul').append('<li id="gzyd_sms_checkbox_now" onclick="javascript:GzydMappings.rightClick_duanxin();"><a href="###">发送短信</a></li>');
							}
						}
					}
				});
			}
		});
	});
		*/
	function showMenu(e){
		
		if(e.pageX || e.pageY){ 
			x=e.pageX;
			y=e.pageY;
		} else{
			x=e.clientX + document.body.scrollLeft - document.body.clientLeft;
			y=e.clientY + document.body.scrollTop - document.body.clientTop;
		}
		$('#menu').css({"top":y+"px", "left":x+"px", "visibility":"visible"});
		e.stopPropagation();
//		$("body").mousedown(onBodyMouseDown);
		$("body").bind("mousedown", onBodyMouseDown);
	}
	function hideMenu(){
		$('#menu').css({"visibility":"hidden"});
		$("body").unbind("mousedown", onBodyMouseDown);
	}
	/**************点击其他位置隐藏右键菜单************************/
	function onBodyMouseDown(event){
		if (!(event.target.id == "menu" || $(event.target).parents("#menu").length>0)) {
			$("#menu").css({"visibility" : "hidden"});
		}
	}
	
	function menuClick(){
		//评论反馈
		$('#menu li:eq(0)').click(function(){
			hideMenu();
			/*
			var w = ($('body').width() - $('#satisfaction').width())/2;
			$('#satisfaction').css({"top":y+"px", "left":w+"px"});
			$('body').showShade();
			$("#typelist").hide();
			$("#form1")[0].reset();
			$("#form2")[0].reset();
			$("#score").html($("input[name='satisfaction']:checked").val());
			$.ajax({
			   		type: "POST",
			   		url:  $.fn.getRootPath() + '/app/corrections/corrections!getsatisfaction.htm',
			   		data:{'value_id':faqId},
			   		async: true,
			   		dataType : 'html',
			   		success: function(data){
			   		    $("#fenye").remove();
			   		    $("#satisfact_content").remove();
			   			$("#satisfaction").append(data);
			   			$('#commcount').html($('#satisfactioncount').val());
			   		},
			   		error :function(){
			   			parent.layer.alert('获取失败');
			   		}
			});
			$('#satisfaction').show();
			*/
			/*
			var _data = {
				'objId': _objid,
				'faqId': _valueid,
				'cateId': categoryId,
				'question': _ques,
				'answer': _answer
			}
			var _params = $.param(_data);
			var _url = $.fn.getRootPath() + "/app/corrections/corrections!send.htm?" + _params;
			parent.__kbs_layer_index = parent.$.layer({
				type: 2,
				border: [10, 0.3, '#000'],
				title: false,
				closeBtn : [0, true],
				iframe: {src : _url, scrolling: 'no'},
				area: ['832px', '600px']
			});*/
			
			$.kbase.errcorrect({
				'objId': _objid,
				'faqId': _valueid,
				'cateId': categoryId/*,
				'question': _ques,
				'answer': _answer*/
			});
		});
		
		//收藏
		$('#navFav').click(function(e){
			//收藏标准问
			var faqId = _valueid;
			FavBallExt.f_openPanel(null, faqId, categoryId);
			/*
			parent.__kbs_layer_index = parent.$.layer({
				type : 2,
				border : [10, 0.3, '#000'],
				title : false,
				closeBtn : [0, true],
				iframe : {
					src : $.fn.getRootPath() + '/app/fav/fav-clip-object!pick.htm?objid=' + _objid + '&cataid=' + categoryId
				},
				area : ['500', '300']
			});
			*/
			
			hideMenu();
		});
		
		//复制
		$('#navCopy').live('click', function(){
			window.clipboardData.setData("Text", _ques + '\r\n' + _answer);
			//window.clipboardData.setData("Text", _ques + '\r\n' + _answerHtml);
			$(document).hint('复制成功');
			hideMenu();
		});
		
		//推荐
		$('#navRecommend').live('click', function(){
			parent.__kbs_layer_index = parent.$.layer({
				type : 2,
				border : [10, 0.3, '#000'],
				title : false,
				closeBtn : [0, true],
				iframe : {
					src : $.fn.getRootPath() + '/app/recommend/recommend!open.htm?cateid=' + categoryId + '&valueid=' + _valueid
				},
				area : ['540', '480']
			});
			hideMenu();
		});
		//上一版本
		$('#version').live('click',function(){
			_showModalDialog({url: $.fn.getRootPath() + '/app/category/category!versionAns.htm?valueId=' + _valueid+'&objId='+_objid});
			hideMenu();
		});
	}
} //end function rightClick


function zdyw(){
	$('ul.zdyw li a').each(function(index){
		var question = $.trim($(this).parent().attr('q'));
		var self = $(this);
		$(this).click(function(){
			if(self.next().next().attr('index')){
				self.css('color','#666666');
				if(self.next().next().attr('index')==index){
					self.next().next().remove();
					return false;
				}else{
					self.next().next().remove();
				}
			}
			$('body').showShade();
			$.ajax({
		   		type: "POST",
		   		url: $.fn.getRootPath()+"/app/category/related-ans!zdyw.htm",
		   		data: {question:question,type:index%2+1},
		   		dataType:'json',
		   		async: true,
		   		success: function(msg){
		   			if(msg == null){
		   				alert('当前地市未关联相应的知识');
		   			}else if(index%2 == 0){
		   				self.css('color','#0000FF');
		   				self.parent().append('<div index="'+index+'" style="border:1px solid;margin:1px;">'+msg+'</div>');
		   				openP4();
		   				
		   				_flushBodyHeight();
		   			}else if(index%2 == 1){
		   				var url = $.fn.getRootPath()+"/app/category/category.htm?type=3&categoryId="+msg.id+"&categoryName="+msg.name;
		   				parent.TABOBJECT.open({
		   					id : msg.id,
		   					name : msg.name,
		   					hasClose : true,
		   					url : url,
		   					isRefresh : true
		   				}, this);
		   			}
		   			$('body').hideShade();
		   		},
		   		error :function(){
		   			alert('当前地市未关联相应的知识');
		   			$('body').hideShade();
		   		}
			});
		});
	});
}

/**
 * @author eko.zhan 
 * @since 2015-10-10 12:05
 * @author eko.zhan at 2015-10-27 11:10 增加回到顶部功能
 * 增加全屏显示功能
 */
function doFullScreen(){
	$('#btnFullScreen').click(function(){
		//判断当前页面是否全屏，已全屏就不再全屏显示
		if (window.__kbs_has_parent){
			var _url = location.href;
			//var _params = 'fullscreen=yes, toolbar=yes, scrollbars=yes, location=no, status=no, menubar=no, resizable=no';
			var _params = 'height=' + screen.availHeight + ', width=' + screen.availWidth + ', top=0, left=0, toolbar=no, scrollbars=yes, location=no, status=no, menubar=no, resizable=yes';
			window.open(_url, '_blank', _params);
		}
	});
	
	//置顶
	$('#btnGoup').click(function(){
		//var tabBody = $('.gzcontent .gz-cn2:visible:last');
		var tabBody = $('#' + window.__kbs_global_finder_id);
		tabBody.scrollTop(0);
	});
}

/**
 * @author eko.zhan
 * url, width, height, args
 */
function _showModalDialog(opts){
	opts = $.extend(true, {
		width: 500,
		height: 400,
		args: window
	}, opts);
	var _diaWidth = opts.width;
	var _diaHeight = opts.height;
	var _scrWidth = screen.width;
	var _scrHegiht = screen.height;
	var _diaLeft = (_scrWidth-_diaWidth)/2;
	var _diaTop = (_scrHegiht-_diaHeight)/2;
	var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
	window.showModalDialog(opts.url, opts.args, params);
}

function moreObject(){
	$('#moreObject').click(function() {
				if ($('div.layer_tl').css('display') == 'none') {
					$('div.layer_tl').css('display', 'block')
				} else {
					$('div.layer_tl').css('display', 'none')
				}
	});
	
	$('input:checkbox').click(function() {
				$('div.checkboxs input:checkbox:checked').not($(this)).attr("checked", false);
	});

	$('div.btn_qr a').each(function(){
		var id = $(this).attr("id");
		if(id == 'btn_qr'){
				$(this).click(function(){
					 if($('div.checkboxs input:checkbox:checked').length > 0){
							var objId = $('div.checkboxs input:checkbox:checked')[0].value;
							var name =  $('div.checkboxs input:checkbox:checked')[0].name;
							url = $.fn.getRootPath() + '/app/template/template.htm?objId='+objId + '&tplId=' +tplId ;
							parent.TABOBJECT.open({
								id : objId+'_template',
								name : name,
								hasClose : true,
								url : url,
								isRefresh : true
							}, this);
		   			}
				});
		}else if(id == 'btn_qx'){
				$(this).click(function(){
					$('div.checkboxs input:checkbox:checked').attr("checked", false);
					$('div.layer_tl').css('display', 'none');
				});
		}
	});
	
	/*$('div.btn_qr a').click(function() {
		   if($('div.checkboxs input:checkbox:checked').length > 0){
					var objId = $('div.checkboxs input:checkbox:checked')[0].value;
					url = $.fn.getRootPath() + '/app/template/template.htm?objId='+objId + '&tplId=' +tplId ;
					parent.TABOBJECT.open({
						id : 'categoryTree',
						name : objName,
						hasClose : true,
						url : url,
						isRefresh : true
					}, this);
		   }
	});*/
	$('#goback').click(function() {
			parent.TABOBJECT.open({
					id : objId, 
					name : objName, 
					hasClose : true,
					url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=1&objId='+objId+'&categoryId_current='+categoryId,
					isRefresh : true
			}, this);
	});
	
}

function reloadRng(id, keyword){
	//仅支持IE浏览器
	if (!!window.ActiveXObject || "ActiveXObject" in window){
		window.__kbs_global_finder_id = id;
		window.__kbs_global_finder_keyword = keyword;
		var rng = document.body.createTextRange();
		rng.moveToElementText(document.getElementById(id));
		var _text = rng.text;
		//alert('reloadRng -> ' + _text);
		window.__kbs_global_finder_inds = [];
		var startPos = 0;
		if (_text.indexOf(window.__kbs_global_finder_keyword)>-1){
			var arr = _text.split(window.__kbs_global_finder_keyword);
			
			for (var i=1;i<arr.length;i++){
				var k = _text.indexOf(arr[i]);
				window.__kbs_global_finder_inds.push(k);
			}
			
		}	
	}
}