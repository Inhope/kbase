lastParent = parent;
parent = parent.parent;


$(function(){
	ObjectSearchForComparison();
});

var ObjectSearchForComparison = function() {
	var option = {
		panel : $('div.fdong'),
		queryBtn : $('input[value=查询]'),
		queryValue : $('input[value=查询]').parent().prev(),
		table : $('div.shili_dan_center1_top table tbody'),
		selectedDatePanel : $('div.shili_dan_right1_con #compareSortable'),
		selectedDateIdArr : new Array(),
		query : function(content, pageNo) {
			if(!content || $.trim(content) == ''){
				return;
			}
			
			pageNo = pageNo ? pageNo : 1;
			var self = this;
			
			$.ajax({
				url : $.fn.getRootPath() + '/app/comparison/business-contrast!expertSearch.htm',
				type : 'POST',
				dataType : 'json',
				data : {
				    page : pageNo,
				    rows : 13,
//				    objectId : (!content || $.trim(content) == '') ? window.objId : '',
				    objectName : content
				},
				success : function(data, textStatus, jqXHR) {
					$('span[id=totlePageSize]').text(data.totlePageSize);
					self.pager(content, parseInt(pageNo), data.pageTotalNo);
					self.table.html('');
					for(var i = 0; i < data.rows.length; i ++) {
						var d = data.rows[i];
						var tr = self.getTrHtml(d);
						self.table.append(tr);
					}
				},
				error : function(data, textStatus, errorThrown) {
					//modify by eko.zhan at 2016-01-03 12:23 去掉ajax timeout 5000，屏蔽请求失败提示
					//parent.layer.alert('请求数据失败!', -1)
				}
			});
			
		},

		/**
		 * 点击对比实例中的实例a标签
		 * @param e a标签
		 * @author baidengke
		 * @date 2016年3月8日
		 */
		onClickComparisonObject:function(e){
			var objId = $(e).attr('objId');
			parent.TABOBJECT.open({
				id : 'sldb',
				name : '实例对比',
				hasClose : true,
				url : $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=8&objId='+objId,
				isRefresh : true
			}, e);
		},
		/**
		 * 获取对比实例信息
		 * disposeDataFuns：数据处理函数数组
		 */
		getObjectComparison:function(){
			var disposeDataFuns = arguments;
			if(!disposeDataFuns || disposeDataFuns.length ==0){
				return;
			}
			
			$.ajax({
				url : $.fn.getRootPath() + "/app/search/object-search!getObjectComparison.htm",
				type : 'POST',
				data : {
				    objectId : window.objId
				},
				success : function(data) {
					if(!data){
						return;
					}
					
					var rows = $.parseJSON(data).data;
					
					//调用函数，处理数据
					for(var i in disposeDataFuns){
						disposeDataFuns[i](rows);
					}
				}
			});
		},
		/**
		 * 展示保存好的实例
		 */
		showObjectComparison:function(rows){
			if(!rows || rows.length == 0){
				return;
			}
			
			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];
				var rowLi = option.getSelectedLi(row.compareObjectId,row.compareObjectName,row.saveInManager);
				option.selectedDatePanel.append(rowLi);
				option.selectedDateIdArr.push(row.compareObjectId);
			}
		},
		render : function() {
			var self = this;
			//执行搜索
			self.queryBtn.click(function() {
				self.query(self.queryValue.val(), 1);
			});
			
			//回车搜索
			self.queryValue.keydown(function(e) {
				if(e.keyCode == 13){
					self.queryBtn.trigger('click');
				}
			});
			
			//将实例对比列表转为sortable，可拖动
			option.selectedDatePanel.sortable();
			//默认执行查询显示
//			this.query('', 1);
			//初始展示已保存的对比实例
			option.getObjectComparison(option.showObjectComparison);
			//默认添加当前实例
			self.selectedDateIdArr.push(window.objId);
			
			$('#sureBtn').click(function() {
				//将选择的对比实例保存到数据库中
				var dataLis = option.selectedDatePanel.find("li");
				if(!dataLis || dataLis.length ==0){
					return;
				}
				var objectComparisions = new Array(dataLis.length);
				$.each(dataLis, function(i, n){
				  	var objectComparision = {};
					objectComparision.objectId = window.objId;
					objectComparision.compareObjectId = $(this).attr("objectId");
					objectComparision.compareObjectName = $(this).find("p").text();
					objectComparision.saveInManager = 0;//来源非后台
					objectComparision.dataIndex = i + 1;//获取现有元素的个数 + 1
					objectComparisions[i] = objectComparision;
				});
				$.ajax({
					url : $.fn.getRootPath() + "/app/search/object-search!saveObjectComparison.htm",
					type : 'POST',
					data : {
						objectComparision:JSON.stringify(objectComparisions),
						objectId:window.objId
					},
					async:false,
					success : function(data) {
					}
				});
				option.closeComparePanel();
			});
		},
		/**
		 * 关闭实例对比面板
		 */
		closeComparePanel:function(){
			lastParent.refreshObjectComparison();
			//获取当前窗口索引
			var index = lastParent.layer.getFrameIndex(window.name);
			lastParent.layer.close(index);
		},
		//删除要对比的实力
		removeSelectedObject : function(parentLi){
			var id = parentLi.attr('objectId');
			this.selectedDateIdArr.splice($.inArray(id, this.selectedDateIdArr), 1);//在对比数组里面删除
			parentLi.remove();//在页面删除，如果不在selectedDateIdArr里面删除，只是表象的删除，实际还是存在的。
		},
		pager : function(value, currentPageNo, totlePageSize) { //分页
			var self = this;
			var pagerPanel = $('div.gonggao_con_nr_fenye').html('');
			var firstPageEl = $('<a href="javascript:void(0);" style="margin-right: 2px;" value="' + value +'" pageNo="1">首页</a>');
			firstPageEl.click(function() {
				var pageNo = $(this).attr('pageNo');
				if(parseInt(pageNo) > 0)
					self.query($(this).attr('value'), pageNo);
			});
			pagerPanel.append(firstPageEl);
			var prevPageEl = $('<a href="javascript:void(0);" style="margin-right: 2px;" value="' + value +'" pageNo="' + (parseInt(currentPageNo) - 1) + '">上一页</a>');
			prevPageEl.click(function() {
				var pageNo = $(this).attr('pageNo');
				if(parseInt(pageNo) > 0)
					self.query($(this).attr('value'), pageNo);
			});
			pagerPanel.append(prevPageEl);
			
			for(var i = 1; i <= totlePageSize; i ++) {
				if((i > (currentPageNo - 3)) && (i < currentPageNo + 3)) {
					var page = $('<a href="javascript:void(0);" style="margin-right: 2px;" value="' + value +'" pageNo="' + i + '">' + i +'</a>');
					if(i == parseInt(currentPageNo)) {
						page.addClass('dang');
					}
					page.click(function() {
						self.query($(this).attr('value'), $(this).attr('pageNo'));
					});
					pagerPanel.append(page);
				}
			}
			if(parseInt(totlePageSize) > currentPageNo + 2)
				pagerPanel.append('<span>...</span>');
			var nextPageEl = $('<a href="javascript:void(0);" style="margin-right: 2px;" value="' + value +'" pageNo="' + (parseInt(currentPageNo) + 1) + '">下一页</a>');
			nextPageEl.click(function() {
				var pageNo = $(this).attr('pageNo');
				if(parseInt(pageNo) <= parseInt(totlePageSize))
					self.query($(this).attr('value'), $(this).attr('pageNo'));
			});
			pagerPanel.append(nextPageEl);
			var lastPageEl = $('<a href="javascript:void(0);" style="margin-right: 2px;" value="' + value +'" pageNo="' + totlePageSize + '">尾页</a>');
			lastPageEl.click(function() {
				if(parseInt(currentPageNo) != parseInt(totlePageSize))
					self.query($(this).attr('value'), $(this).attr('pageNo'));
			});
			pagerPanel.append(lastPageEl);
		},
		getTrHtml : function(obj) {
			var saveInManager = 0;
			if(obj.saveInManager){
				saveInManager = obj.saveInManager;
			}
			
			var self = this;
			var tr = $('<tr id="' + obj.objectId + '"><td style="width:150px;" title="' + obj.objectName + '">' + (obj.objectName.length > 13 ? obj.objectName.substring(0, 13) : obj.objectName) +'</td><td style="width:130px;">' + obj.startTime +'</td><td style="width:130px;">' + obj.endTime +'</td><td style="width:130px;">' + obj.editTime + '</td></tr>');
			tr.dblclick(function() {
				var id = $(this).attr('id');
				var name = $('td:eq(0)', this).text();
				if(id == window.objId) {
					parent.layer.alert('不能与自己对比,对象:' + name, -1);
					return;
				} 
				/*if(self.selectedDateIdArr.length >= 5) {
					parent.layer.alert('最多选择4个实例与当前实例比对', -1)
					return;
				}*/
				for(var i = 0; i < self.selectedDateIdArr.length; i ++) {
					if(self.selectedDateIdArr[i] == id) {
						parent.layer.alert('请勿重复添加,对象:' + name, -1)
						return;
					}
				}
				self.selectedDatePanel.append(self.getSelectedLi(id, name));
				self.selectedDateIdArr.push(id);
			});
			return tr;
		},
		getSelectedLi : function(id,name,saveInManager) {
			if(saveInManager && saveInManager == 1){//如果来自后台则不生成li
				return;
			}
			var self = this;
			var li = $('<li></li>');//
			li.attr("objectId",id);
			li.append('<p>' + name +'</p>');
			li.append('<img src="'+$.fn.getRootPath()+'/theme/' + parent.USER_THEME + '/resource/search/images/xinxi_ico5.jpg"/>');
			$(li).children('img').click(function(){
				self.removeSelectedObject(li);
			});
			return li;
		}
	}
	option.render();
	
	//知识树
	var catalogTree = {
		zTree : null,
		setting : {
			view: {
				expandSpeed: "fast"//空不显示动画
			},
			async : {
				enable : true,
				url : $.fn.getRootPath() + "/app/search/object-search!getCatalogObject.htm",
				autoParam : ["id", "name=n", "level=lv","bh"],
				dataFilter : function(treeId, parentNode, responseData){
					for(var i =0; i < responseData.length; i++) {
						if(responseData[i].isParent == "true"){
				    		responseData[i].nocheck = true;
						}
				    }
				    return responseData;
			   }
			},
			check: {
				enable: true,
				chkboxType : { "Y" : "ps", "N" : "ps" }
			},
			callback: {
				onCheck : function(event, treeId, treeNode){
					if(treeNode.checked == true) {
						var id=treeNode.id;
					    var name=treeNode.name;
					    if(id == window.objId) {
							parent.layer.alert('不能与自己对比,对象:' + name, -1);
							treeNode.checked = false;
							catalogTree.zTree.updateNode(treeNode);
							return;
						}
						/*if(option.selectedDateIdArr.length >= 5) {
							parent.layer.alert('最多选择4个实例与当前实例比对', -1)
							treeNode.checked = false;
							catalogTree.zTree.updateNode(treeNode);
							return;
						}*/
						for(var i = 0; i < option.selectedDateIdArr.length; i ++) {
							if(option.selectedDateIdArr[i] == id) {
								parent.layer.alert('请勿重复添加,对象:' + name, -1)
								treeNode.checked = false;
								catalogTree.zTree.updateNode(treeNode);
								return;
							}
						}
						option.selectedDatePanel.append(option.getSelectedLi(id, name,null));
						option.selectedDateIdArr.push(id);
					}
				}
			}
		},
		setZtreeParam : function(params) {
			this.setting.async.otherParam = params;
		},
		refreshTree : function(params) {
			this.setZtreeParam(params);
			$.fn.zTree.init($("#catalogTree"), this.setting);
		},
		init : function(){
			$.fn.zTree.init($("#catalogTree"), this.setting);
			this.zTree=$.fn.zTree.getZTreeObj('catalogTree');
		}
	}
	catalogTree.init();
}



