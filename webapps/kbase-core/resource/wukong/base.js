/**
 * 基于jquery的插件扩展，使用方法详见 ../resource/wukong/search-plugin.js
 * @author eko.zhan
 * @since 2016-04-14 09:50
 * @version 1.0
 */
;(function($){
	$.extend(layer, {
		kbstips: function(info, elem){
			return layer.tips(info, elem, {style: ['background-color:#FFF; color:#000; border:1px solid #e8e8e8; padding: 10px 20px;']});
		}
	});
	
	$.extend({
		/* this function will do nothing */
		lucky: function(){}
	});
	
	$.fn.extend({
		/**
		 * 是否显示浮动框
		 * 使用方法：	$('#kbValAgingPanel').appear(this, function(){
		 *				$(_this).find('.kbs-icon-up').updown();
		 *			});
		 */
		appear: function(elem, callback, event){
			var _offset = $(elem).offset();
			var _width = $('body').width()-5;
			//$(this).css({position: 'absolute', left:_offset.left + 'px', top:_offset.top + $(elem).outerHeight() + 5 + 'px'}).show();
			$(this).css({position: 'absolute', width: _width + 'px', top:_offset.top + $(elem).outerHeight() + 5 + 'px', padding: '3px'}).show();
		
			event = event==undefined?'mouseleave':event;
			if (typeof(callback)=='function'){
				$(this).unbind(event);
				$(this).bind(event, function(e){
					callback();
					$(this).hide();
				});
			}
		}, 
		
		/**
		 * 上下图标切换
		 * 使用方法：$(_this).find('.kbs-icon-up').updown();
		 */
		updown: function(){
			if ($(this).hasClass('kbs-icon-up')){
				$(this).removeClass('kbs-icon-up').addClass('kbs-icon-down');
			}else if ($(this).hasClass('kbs-icon-down')){
				$(this).removeClass('kbs-icon-down').addClass('kbs-icon-up');
			}
		},
		
		/**
		 * 分页
		 * 使用方法：$('#pagePanel').pager({page: 1}, itemMergeHandler);
		 */
		pager: function(opts, callback){
			var _this = this;
			var url = $(_this).attr('_url');
			var _pager_ind = layer.load('加载中，请稍候...');
			$.getJSON(url, opts, function(res){
		    	layer.close(_pager_ind);
		        //执行回调
		        if (typeof(callback)=='function'){
		        	callback(res);
		        }
		        //显示分页
		        laypage({
		            cont: _this, //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
		            pages: res.pages, //通过后台拿到的总页数
  					skin: '#AF0000',
		            curr: opts.page || 1, //当前页
		            jump: function(obj, first){ //触发分页后的回调
		                if(!first){ //点击跳页触发函数自身，并传递当前页：obj.curr
		                	opts = $.extend(true, opts, {page: obj.curr})
		                    $(_this).pager(opts, callback);
		                }
		            }
		        });
		    });
		}
	});

})(jQuery);