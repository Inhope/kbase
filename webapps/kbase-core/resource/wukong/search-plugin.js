$(function(){
	window.__kbs_url = $('#pagePanel').attr('_url');
	window.__kbs_url_param_cate = '';
	window.__kbs_url_param_aging = '';
	window.__kbs_url_param_startdate = '';
	window.__kbs_url_param_enddate = '';
	window.__kbs_url_param_propid = ''; //知识属性id
	window.__kbs_url_param_location = pageContext.location;
	window.__kbs_url_param_brand = pageContext.brand;
	window.__kbs_url_param_rangetype = '0';	//搜索范围 默认为0，0-qa， 1-p4+atts
	window.__kbs_url_param_isfuzzy = '1';	//默认为模糊搜索
	//modify by eko.zhan at 2016-09-08 10:52 设置为动态,修改下面参数的值就能呈现想要的结果
	window.__kbs_url_param_ismerge = window.__kbs_enable_merge;	//目录合并-1 知识展示-0, 默认为知识展示
	window.__kbs_url_param_sort = '';	//为空表示默认排序，样例 dscount desc / dscount asc
	
	//设置搜索结果集的呈现列表样式
	if (window.__kbs_url_param_ismerge==1){
		$('.kbs-merge button').attr('_ismerge', '0');
		$('.kbs-merge button').text('取消合并');
	}else{
		$('.kbs-merge button').attr('_ismerge', '1');
		$('.kbs-merge button').text('目录合并');
	}
	
	//ie7调整body高度
	if (navigator.userAgent.indexOf('MSIE 7.0')!=-1){
		$('body').height('1000px');
	}
	
	//分页功能
	reload();
	
	//按知识目录筛选
	$('#kbValCatePanel').on('click', '.kbs-item', function(){
		var _id = $(this).attr('_id');
		var _name = $(this).text();

		$('.kbs-param-cateid').text(_name).show();
		
		window.__kbs_url_param_cate = _id;
		
		reload();
	})
	//目录取消
	$('.kbs-param-cateid').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_cate = '';
		
		reload();
	});
	
	//知识失效筛选
	$('#kbValAgingPanel').on('click', '.kbs-item', function(){
		$('.kbs-param-aging').text($(this).text()).show();
		window.__kbs_url_param_aging = $(this).attr('_type');
		
		reload();
	})
	//知识筛选取消
	$('.kbs-param-aging').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_aging = '';
		
		reload();
	});
	
	//创建日期范围
	$('#btnIntervalOk').click(function(){
		if ($('#startDate').val()!=''){
			window.__kbs_url_param_startdate = $('#startDate').val();
		}
		if ($('#endDate').val()!=''){
			window.__kbs_url_param_enddate = $('#endDate').val();
		}
		$('.kbs-param-interval').text($('#startDate').val() + '～' + $('#endDate').val()).show();
		
		reload();
	});
	$('.kbs-param-interval').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_startdate = '';
		window.__kbs_url_param_enddate = '';
		
		reload();
	});
	
	//知识属性筛选
	$('#kbValPropPanel').find('span').hover(function(){
		$('#kbValPropLevel1Panel').empty().hide();
		var tagid = $(this).attr('_id');
		var proptype = $(this).attr('_prop');
		var _url = pageContext.contextPath + '/app/wukong/search!getProp.htm?id=' + tagid + '&prop=' + proptype;
		
		$.post(_url, function(data){
			var _cells = '';
			$(data).each(function(i, item){
				_cells += '<span class="kbs-item" _id="' + item.id + '" _prop="1">' + item.name + '</span>';
			});
			
			$('#kbValPropLevel0Panel').empty().append(_cells).appear($('#kbValPropPanel'));
			$('#kbValPropLevel0Panel').find('span').hover(function(){
				var tagid = $(this).attr('_id');
				var proptype = $(this).attr('_prop');
				var _url = pageContext.contextPath + '/app/wukong/search!getProp.htm?id=' + tagid + '&prop=' + proptype;
				$.post(_url, function(data){
					var _cells = '';
					if (data.length==0) return false;
					$(data).each(function(i, item){
						_cells += '<span class="kbs-item" _id="' + item.id + '" _prop="1">' + item.name + '</span>';
					});
					$('#kbValPropLevel1Panel').empty().append(_cells).appear($('#kbValPropLevel0Panel'));
					$('#kbValPropLevel1Panel').mouseleave(function(e){
						var srcElem = e.srcElement || e.target;
						
						$('#kbValPropPanel').hide();
						$('#kbValPropLevel0Panel').hide();
						$('#kbValPropLevel1Panel').hide();
					});
				}, 'json');
			}, $.lucky);
			
		}, 'json');
	}, $.lucky);
	//知识属性筛选
	$('#kbValPropLevel0Panel, #kbValPropLevel1Panel').on('click', '.kbs-item', function(){
		$('.kbs-param-prop').text($(this).text()).show();
		window.__kbs_url_param_propid = $(this).attr('_id');
		
		reload();
		
	});
	//知识属性筛选取消
	$('.kbs-param-prop').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_propid = '';
		
		reload();
	});
	
	//归属地筛选
	$('#locationPanel').on('click', '.kbs-item', function(){
		$('.kbs-param-location').text($(this).text()).show();
		window.__kbs_url_param_location = $(this).attr('_id');
		
		reload();
	});
	//归属地取消
	$('.kbs-param-location').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_location = '';
		
		reload();
	});
	
	//品牌筛选
	$('#brandPanel').on('click', '.kbs-item', function(){
		$('.kbs-param-brand').text($(this).text()).show();
		window.__kbs_url_param_brand = $(this).attr('_id');
		
		reload();
	});
	//品牌取消
	$('.kbs-param-brand').click(function(){
		$(this).text('').hide();
		window.__kbs_url_param_brand = '';
		
		reload();
	});
	
	//预览所有
	$('#expandQa').click(function(){
		if ($('.kbs-row-answer:visible').length>0){
			$('.kbs-row-answer').hide();
		}else{
			$('.kbs-row-answer').show();
		}
		
		//刷新iframe高度
		parent.setCurrentIframeHeight($(parent.document).find('.content_content iframe:visible'));
		
	});
	
	/**
	 *  单个展开问答
	 */
	$('.kbs-nav-body').on('click', '.kbs-cell-question', function(){
		var $rowQuestion = $(this).parent('.kbs-row-question');
		var $rowAnswer = $rowQuestion.next('.kbs-row-answer:visible');
		if ($rowAnswer.length==0){
			$rowQuestion.next('.kbs-row-answer').show();
		}else{
			$rowQuestion.next('.kbs-row-answer').hide();
		}
	});
	//访问知识详情
	$('.kbs-info').parent('tbody').on('click', 'span[_name="detail"]', function(){
		var _id = $(this).attr('_id');
		var _title = $(this).attr('_title');
		/*针对不同类型的数据进行页面跳转*/
		var _url = '/app/wukong/search!detail.htm?id=' + _id;
		var currentTR = $(this).parent().parent(),
			bizTplEnable = $(currentTR).data('bizTplEnable'), /*是否开启文章功能*/
			bizTplArticleId = $(currentTR).data('bizTplArticleId'), /*文章id*/
			bizTplArticleName = $(currentTR).data('bizTplArticleName'),/*文章name(即实例名称)*/
			spotId = $(currentTR).data('spotId');/*锚点id*/
		if(bizTplEnable && bizTplArticleId) {/*如果开启业务模板，来自业务模板的实例调整新的页面*/
			//判断标准问是否在待匹配 modify by wilson.li at 20161010
			$.ajax({
				type: "POST",
				url: pageContext.contextPath + "/app/biztpl/article!isUnMatch.htm",
				data: "articleId="+bizTplArticleId+"&faqId="+_id,
				dataType:"json",
				async:false,//必须同步
				success: function(data) {
					if(data.status != "0"){
						_url =  '/app/biztpl/article-search.htm?&articleId=' + bizTplArticleId + '&spotId=' + spotId;
						_title = bizTplArticleName;
					}
				}
			});
		}
		/*打开tab*/
		parent.TABOBJECT.open({
			id : _id,
			name : _title,
			hasClose : true,
			url : pageContext.contextPath + _url,
			isRefresh : true
		}, this);
	});
	
	/**
	 *  附件下载
	 * @author anonymous 请程序员标记名称
	 */
//	$('.kbs-nav-body').on('click', '.kbs-text', function(){
//		window.open('http://172.16.9.54:7661/robot-search/attachmentDown?attachmentId=' + $(this).parent('td').attr('_fileid'), '_self');
//		var url = pageContext.contextPath + '/app/wukong/search!download.htm?fileId='+ $(this).parent('td').attr('_fileid') +'&fileName='+$(this).text();
//		var dl = '<iframe id="download" style="display:none;" src="' + url + '"></iframe>';
//		$('#download').remove();
//		$('body').append(dl);	
//	});
	
	/**
	 * 附件搜索下载
	 */
	$('.kbs-nav-body').on('click', '.kbs-embed-title .kbs-text .kbs-att-download', function(e){
		var _fileid = $(this).parent('.kbs-text').parent('td').attr('_fileid');
		var _filename = $.trim($(this).parent('.kbs-text').text());
		_filename = _filename.substring(0, _filename.lastIndexOf('(下载)'));
		_filename = $.trim(_filename);
		pageContext.download(_fileid, _filename);
		e.stopPropagation();
	});
	
	
	//附件预览
	$('.kbs-nav-body').on('click', '.kbs-embed-title .kbs-text', function(){
		if($(this).attr("class").indexOf("kbs-bhname") > 0){
			return;
		}
		var _fileid = $(this).parent('.kbs-embed-title').attr('_fileid');
		var _filetype = $(this).parent('.kbs-embed-title').attr('_filetype');
		
		pageContext.preview(_fileid + _filetype);
	});
	
	//附件关联的实例路径 
	$('.kbs-nav-body').on('click', '.kbs-embed-title .kbs-bhname', function(e){
		var _firstObjectId = $(this).attr('_firstObjectId');
		jumpobject(_firstObjectId);
		e.stopPropagation();
	});
	
	/**
	 *  附件引用
	 */
	$('.kbs-nav-body').on('click', '.kbs-embed-info', function(){
		var _this = this;
		var _fileId = $(_this).parent('td').attr('_fileid');
		var _fileType = $(_this).parent('td').attr('_filetype');
		
		//隐藏非当前所有的悬浮框
		$('.kbs-panel-embed[id!="embed' + _fileId + '"]').hide();
		
		var _param = {
			'fileId': _fileId,
			'fileType': _fileType
		}
		
		if ($('#embed'+_fileId).length>0){
			if ($('#embed' + _fileId + ':visible').length>0){
				$('#embed'+_fileId).hide();
			}else{
				$('#embed'+_fileId).show();
			}
		}else{
			var template = [
				'<div class="kbs-panel-embed" {0}>',
					'<table width="100%" cellspacing="1" cellpadding="1">',
						'{1}',
					'</table>',
				'</div>'
			].join('');
			$.getJSON(pageContext.contextPath + '/app/wukong/search!loadEmbed.htm', _param, function(data){
				//data.objRefs
				//data.qaRefs
				var objTemplate = '<tr><td width="20%"><b>所属实例</b></td><td width="80%">{0}</td></tr>';
				var qaTemplate = '<tr><td width="20%"><b>所属问答</b></td><td width="80%">{0}</td></tr>';
				if (data.objRefs && data.objRefs.length>0){
					var tmp = ''
					$(data.objRefs).each(function(i, item){
						tmp += '<span class="kbs-item" _embedtype="2" _id="' + item.objectId + '" _cateid="' + item.cat_id + '">' + item.objectName + '</span><br>';
					});
					objTemplate = objTemplate.fill(tmp);
				}else{
					objTemplate = '';
				}
				
				if (data.qaRefs && data.qaRefs.length>0){
					objTemplate = '<tr><td width="20%"><b>所属实例</b></td><td width="80%">{0}</td></tr>';
					var tmp0 = ''
					var tmp1 = ''
					$(data.qaRefs).each(function(i, item){
						tmp0 += '<span class="kbs-item" _embedtype="2" _id="' + item.objectId + '" _cateid="' + item.cat_id + '">' + item.objectName + '</span><br>';
						tmp1 += '<span class="kbs-item" _embedtype="1" _id="' + item.questionId + '">' + item.questionName + '</span><br>';
					});
					objTemplate = objTemplate.fill(tmp0);
					qaTemplate = qaTemplate.fill(tmp1);
				}else{
					qaTemplate = '';
				}
				template = template.fill('id="embed' + _fileId + '"', 
						qaTemplate == '' ? objTemplate + qaTemplate : qaTemplate);//这里修改答案附件 不显示对应实例 以示区别
				
				$('body').append(template);
				$('#embed' + _fileId).appear(_this);
				
			});
			
		}
	});
	
	/**
	 * add by eko.zhan at 2016-04-27 15:00
	 * 附件引用的实例和标准问打开
	 */
	$('body').on('click', '.kbs-panel-embed .kbs-item', function(){
		var _this = this;
		var _embedType = $(_this).attr('_embedtype');	//1-qa 2-obj 3-cate
		var _id = $(_this).attr('_id');
		var _cateId = $(_this).attr('_cateid');
		var _text = $(_this).text();
		
		var _url = pageContext.contextPath;
		if (_embedType==1){
			//qa
			_url += '/app/wukong/search!detail.htm?id=' + _id;
		}else if (_embedType==2){
			//obj
			_url += '/app/search/object-search.htm?searchMode=1&objId=' + _id + '&categoryId_current=' + _cateId;
		}
		
		parent.TABOBJECT.open({
			id : _id,
			name : _text,
			hasClose : true,
			url : _url,
			isRefresh : true
		}, this);
		
	})
	

	//单击body的一些隐藏功能
	$('body').on('click', function(e){
		
		var elem = e.srcElement || e.target;
		//隐藏所有的附件引用
		if(elem.className.indexOf('kbs-embed-info') == -1){
			$('.kbs-panel-embed').hide();
		}
		if (elem.className=='kbs-panel-tip'){
			return false;
		}
		$('.kbs-panel-tip').hide();
		$('#conditionOpts .kbs-icon-up').updown();
	});
	
	//收起筛选
	$('#btnSearchCondition').click(function(){
		$('#btnSearchCondition span:eq(1)').updown();
		if ($('#searchConditionPanel:visible').length==1){
			//执行隐藏
			$('#searchConditionPanel').hide();
			$(this).find('span:eq(0)').text('显示筛选');
		}else{
			//执行展开
			$('#searchConditionPanel').show();
			$(this).find('span:eq(0)').text('收起筛选');
		}
	});

	//搜索方式选中切换
	$('#modeOpts').on('click', 'span', function(){
		if ($(this).hasClass('kbs-item-checked')){
			return false;
		}
		$(this).siblings().removeClass('kbs-item-checked').addClass('kbs-item');
		$(this).addClass('kbs-item-checked');
		
		var _isFuzzy = $(this).attr('_isfuzzy');
		window.__kbs_url_param_isfuzzy = _isFuzzy;
		
		reload();
	})
	//搜索范围选中切换
	$('#rangeOpts').on('click', 'span[class^="kbs-item"]', function(){
		if ($(this).hasClass('kbs-item-checked')){
			return false;
		}
		$(this).siblings().removeClass('kbs-item-checked').addClass('kbs-item');
		$(this).addClass('kbs-item-checked');
		
		window.__kbs_url_param_rangetype = $(this).attr('_rangetype');
		
		//附件搜索隐藏table标题
		if (window.__kbs_url_param_rangetype==1){
			$('.kbs-nav-body thead').hide();
			$("#modeOpts span:eq(1)").hide();//精准搜索
			$("#kbValCate").hide();//知识目录
			$("#kbValAging").hide();//知识时效
//			$("#location").hide();//归属地
//			$("#brand").hide();//品牌
			$('.kbs-param-cateid').text("");
			$('.kbs-param-aging').text("");
			
			$('#kbValInterval').addClass("kbs-item-first");
		}else{
			$('.kbs-nav-body thead').show();
			$("#modeOpts span:eq(1)").show();//精准搜索
			$("#kbValCate").show();//知识目录
			$("#kbValAging").show();//知识时效
//			$("#location").show();//归属地
//			$("#brand").show();//品牌
			$('.kbs-param-cateid').text($('#kbValCatePanel span[_id="' + window.__kbs_url_param_cate + '"]').text());
			$('.kbs-param-aging').text($('#kbValAgingPanel span[_type="' + window.__kbs_url_param_aging + '"]').text());
			$('#kbValInterval').removeClass("kbs-item-first");
		}
		
		reload();
	})
	//筛选条件行切换上下图标
	$('#conditionOpts span[class^="kbs-item"]').hover(function(){
		$('.kbs-panel-tip:visible').hide();
		$(this).find('.kbs-icon-down').updown();
	}, $.lucky)
	//知识目录下拉
	$('#kbValCate').hover(function(){
		var _this = this;
		$('#kbValCatePanel').appear(this, function(){
			$(_this).find('.kbs-icon-up').updown();
		});
	}, $.lucky)
	//知识失效下拉
	$('#kbValAging').hover(function(){
		var _this = this;
		$('#kbValAgingPanel').appear(this, function(){
			$(_this).find('.kbs-icon-up').updown();
		});
	}, $.lucky)
	//知识创建日期下拉
	$('#kbValInterval').hover(function(){
		var _this = this;
		$('#kbValIntervalPanel').appear(this);
	}, $.lucky)
	//知识创建日期面板取消
	$('#btnCancel').click(function(){
		$('#kbValIntervalPanel').hide();
		$('#kbValInterval').find('.kbs-icon-up').updown();
	}, $.lucky);
	//知识属性面板下拉
	$('#kbValProp').hover(function(){
		var _this = this;
		$('#kbValPropPanel').appear(this, function(){
			$(_this).find('.kbs-icon-up').updown();
		});
	}, $.lucky)
	$('#kbValPropLevel0Panel').hover(function(){
		$('#kbValPropPanel').show();
	}, $.lucky)
	$('#kbValPropLevel1Panel').hover(function(){
		$('#kbValPropLevel0Panel').show();
		$('#kbValPropPanel').show();
	}, $.lucky)
	//归属地下拉
	$('#location').hover(function(){
		var _this = this;
		$('#locationPanel').appear(this, function(){
			$(_this).find('.kbs-icon-up').updown();
		});
	}, $.lucky)
	//品牌下拉
	$('#brand').hover(function(){
		var _this = this;
		$('#brandPanel').appear(this, function(){
			$(_this).find('.kbs-icon-up').updown();
		});
	}, $.lucky)
	
	//提示
	$('#memoQa').hover(function(){
		window._layer_tip_index = layer.kbstips('根据关键字搜索标准问和答案', this);
	}, function(){
		layer.close(window._layer_tip_index);
	});
	
	$('#memoObj').hover(function(){
		window._layer_tip_index = layer.kbstips('根据关键字搜索实例', this);
	}, function(){
		layer.close(window._layer_tip_index);
	});
	
	$('#memoAtt').hover(function(){
		window._layer_tip_index = layer.kbstips('根据关键字搜索P4和附件', this);
	}, function(){
		layer.close(window._layer_tip_index);
	});
	
	//合并目录 知识展示
	$('.kbs-merge').on('click', 'button', function(){
		/*
		if ($(this).hasClass('btn-opt-checked')) return false;
		$(this).siblings().removeClass('btn-opt-checked');
		$(this).removeClass('btn-opt').addClass('btn-opt-checked');
		*/
		
		window.__kbs_url_param_ismerge = $(this).attr('_ismerge');
		
		//modify by eko.zhan at 2016-06-06 13:40 目录合并
		if ($(this).attr('_ismerge')=='1'){
			$(this).attr('_ismerge', '0');
			$(this).text('取消合并');
		}else{
			$(this).attr('_ismerge', '1');
			$(this).text('目录合并');
		}
		
		//刷新iframe高度
		parent.setCurrentIframeHeight($(parent.document).find('.content_content iframe:visible'));
		
		reload();
	});
	
	//add by lwp 2016.05.13 相关问访问
	$('#related-question span.kbs-item').click(function(){
		window.location.href = pageContext.contextPath + "/app/wukong/search.htm?keyword="+encodeURIComponent($(this).html());
	});
	
	//点击量排序
	$('span[name="visitnum"]').click(function(){
		var _this = this;
		if ($(_this).hasClass('kbs-icon-down')){
			//当前是按点击量降序排列，需要升序
			$(_this).removeClass('kbs-icon-down').addClass('kbs-icon-up');
			window.__kbs_url_param_sort = 'dscount asc';
		}else{
			//
			$(_this).removeClass('kbs-icon-up').addClass('kbs-icon-down');
			window.__kbs_url_param_sort = 'dscount desc';
		}
		
		reload();
	});
	
	//显示隐藏更多 add by wilson.li at 20161012
	$("body").on("click","[name='showOrHideMore']",function(){
		$(this).parents("tr[name='showOrHideMoreTr']").nextUntil("tr[class='kbs-row-cate']").filter("tr[class='kbs-row-question']").each(function(){
			if($(this).is(":hidden")){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
		if($(this).text()=='显示全部↓'){
			$(this).text("折叠↑");
		}else{
			$(this).text("显示全部↓");
		}
	});
	
})

/**
 * 获取路径
	window.__kbs_url = $('#pagePanel').attr('_url');
	window.__kbs_url_param_cate = '';
	window.__kbs_url_param_aging = '';
	window.__kbs_url_param_startdate = '';
	window.__kbs_url_param_enddate = '';
	window.__kbs_url_param_location = '';
	window.__kbs_url_param_brand = '';
 */
var getLocation = function(){
	var _param = '';
	_param += "?keyword=" + encodeURIComponent($('#pagePanel').attr('_keyword'));
	_param += '&isFuzzy=' + window.__kbs_url_param_isfuzzy;
	_param += '&isMerge=' + window.__kbs_url_param_ismerge;
	_param += '&rangeType=' + window.__kbs_url_param_rangetype;
	_param += '&sortable=' + window.__kbs_url_param_sort;
	_param += '&location=' + window.__kbs_url_param_location;
	_param += '&brand=' + window.__kbs_url_param_brand;
	if (window.__kbs_url_param_cate!='' && window.__kbs_url_param_rangetype==0){
		_param += '&cateId=' + window.__kbs_url_param_cate;
	}
	if (window.__kbs_url_param_aging!='' && window.__kbs_url_param_rangetype==0){
		_param += '&agingType=' + window.__kbs_url_param_aging;
	}
	if (window.__kbs_url_param_startdate!=''){
		_param += '&startDate=' + window.__kbs_url_param_startdate;
	}
	if (window.__kbs_url_param_enddate!=''){
		_param += '&endDate=' + window.__kbs_url_param_enddate;
	}
	if (window.__kbs_url_param_propid!=''){
		_param += '&propid=' + window.__kbs_url_param_propid;
	}
	return window.__kbs_url + _param;
}

/**
 * 分页重新加载
 */
var reload = function(callback){
	$('#pagePanel').attr('_url', getLocation());
	//分页功能
	if (window.__kbs_url_param_ismerge==1 && window.__kbs_url_param_rangetype==0){
		$('#pagePanel').pager({page: 1}, itemMergeHandler);
	}else{
		$('#pagePanel').pager({page: 1}, itemSeparateHandler);
	}
}

/**
 * 合并目录回调方法
 */
var itemMergeHandler = function(data){
	$('.kbs-total-hit').text(data.totalHits);
	var totalElapsed = 0.01;
	try{
		totalElapsed = data.totalElapsed;
		totalElapsed = totalElapsed/1000;
		totalElapsed = Number(totalElapsed).toFixed(2);
	}catch(e){}
	$('.kbs-total-elapsed').text(totalElapsed);
	//知识预览：标准问，创建日期，修改时间，修改人，点击量，分类路径，单击详情
	var template0 = '<tr class="kbs-row-question"><td class="kbs-cell-question">{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>&nbsp;</td><td><span class="kbs-item" _name="detail" _id="{7}" _title="{8}">详情</span></td></tr><tr class="kbs-row-answer"><td colspan="7">{6}</td></tr>';
	var template1 = '';
	var showMore = '';
	$('#pageBar').siblings('[id!="pageBar"]').remove();
	var _tmpCateId = '';	//启用分类目录时，这里和cateid比较，不启用分类时，这里和objectid比较 add by eko.zhan at 2016-11-08 19:58
	var rowNum = 0;//自定义行数
	var showNum = 5;//控制显示个数，其他隐藏
	$(data.items).each(function(i, item){
		//当数据超过5条时显示“全部”按钮 modify by wilson.li at 20161012
		var cateId = pageContext.enableCateTag?item.category_id:item.object_id;
		if(_tmpCateId==cateId){
			if(rowNum == showNum){
				template1 = '<tr style="background-color: #fff;" name="showOrHideMoreTr"><td colspan="7" style="text-align:right;"><a href="javascript:void(0);" name="showOrHideMore" style="margin-right:10px;text-decoration:none;">显示全部↓</font></td></tr>' + template0;
				template1 = template1.replace(/class="kbs-row-question"/g,'class="kbs-row-question" style="display:none;"');
			}else if(rowNum > showNum){
				template1 = template0.replace(/class="kbs-row-question"/g,'class="kbs-row-question" style="display:none;"');
			}
		}else{
			rowNum = 0;
		}
		if(template1==''){
			template1 = template0;
		}
		if (_tmpCateId!=cateId){
			_tmpCateId = cateId;
			template1 = '<tr class="kbs-row-cate"><td colspan="7">{5}</td></tr>' + template1;
		}
		
		var question = item.question;
		var answer = item.answer
		//处理ie7下的高亮问题
		if (navigator.isLowerBrowser()){
			question = question.replace(/<highlight>/g, '<font color="red">');
			question = question.replace(/<\/highlight>/g, '</font>');
			
			answer = answer.replace(/<highlight>/g, '<font color="red">');
			answer = answer.replace(/<\/highlight>/g, '</font>');
		}
		
		//路径是否显示实例名称
		var path = item.bh_name;
		if (!pageContext.enableCateTag){
			path = item.bh_name + ' > ' + item.object_name;
		}
		//应东航要求，此处统一为“详情”，若其他客户需要区别，到时再加客户环境判断 modify by wilson.li at 20161017
		/*if (item.biztpl_type == 'article') {
			template1 = template1.replace(new RegExp('详情', 'g'), '文章');
		} else if (item.biztpl_type == 'group') {
			template1 = template1.replace(new RegExp('详情', 'g'), '标题');
		}*/
		$('#pageBar').before(template1.fill(question, item.create_time, item.edit_time, item.editor, item.visit_num, path, answer, item.question_id, item.question_w));

		var currentTR = $('#pageBar').prev().prev();
		$(currentTR).data('bizTplEnable', item.bizTplEnable);/*是否开启文章功能*/
		$(currentTR).data('bizTplArticleId', item.bizTplArticleId);/*文章id*/
		$(currentTR).data('bizTplArticleName', item.bizTplArticleName);/*文章名称*/
		$(currentTR).data('spotId', item.question_id);/*锚点id*/
		
		rowNum++;
		template1='';
	});
	
	//搜索结果页面下的a标签都应该用一个新的标签页打开
//	$('.kbs-nav-body a').attr('target', '_blank');
}
/**
 * 分条目显示回调方法
 */
var itemSeparateHandler = function(data){
	$('.kbs-total-hit').text(data.totalHits);
	var totalElapsed = 0.01;
	try{
		totalElapsed = data.totalElapsed;
		totalElapsed = totalElapsed/1000;
		totalElapsed = Number(totalElapsed).toFixed(2);
	}catch(e){}
	$('.kbs-total-elapsed').text(totalElapsed);
	if (window.__kbs_url_param_rangetype==1){	//查附件和P4
		var template0 = '<tr><td class="kbs-embed-title" width="100%" _fileid="{3}" _filetype="{4}"><span class="kbs-office {2}"></span><span class="kbs-text">{0}</span>';
		var template0_0 = '<tr><td class="kbs-embed-title" width="100%" _fileid="{3}" _filetype="{4}"><span class="kbs-office {2}"></span><span class="kbs-text">{0}&nbsp;&nbsp;<a class="kbs-att-download" style="color:red;" href="javascript:void(0);">(下载)</a></span>'
		var template1 = '<span class="kbs-text kbs-bhname" _firstObjectId="{6}">{5}</span></td></tr><tr><td class="kbs-embed-content">{1}</td></tr>';
		$('#pageBar').siblings('[id!="pageBar"]').remove();
		$(data.items).each(function(i, item){
			var template2 = "";
			//如果是p4则没有下载功能
			if(item.file_type && item.file_type == ".p4"){
				template2 = template0 + template1;
			}else{
				template2 = template0_0 + template1;
			}
			var text = item.file_name + item.file_type;
			var content = item.content;
			//text = text.replace(/&/g, '&amp').replace(/\"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
			//text = encodeURI(text);
			//处理ie7下的高亮问题
			if (navigator.userAgent.indexOf('MSIE 7.0')!=-1){
				text = text.replace(/<highlight>/g, '<font color="red">');
				text = text.replace(/<\/highlight>/g, '</font>');
				
				content = content.replace(/<highlight>/g, '<font color="red">');
				content = content.replace(/<\/highlight>/g, '</font>');
			}
			$('#pageBar').before(template2.fill(text, content, item.icon, item.file_id, item.file_type,item.bh_name,item.firstObjectId));
		});
	}else{
		//知识预览：标准问，创建日期，修改时间，修改人，点击量，分类路径，单击详情
		$('#pageBar').siblings('[id!="pageBar"]').remove();
		$(data.items).each(function(i, item){
			var question = item.question;
			var answer = item.answer;
			//处理ie7下的高亮问题
			if (navigator.userAgent.indexOf('MSIE 7.0')!=-1){
				question = question.replace(/<highlight>/g, '<font color="red">');
				question = question.replace(/<\/highlight>/g, '</font>');
				
				answer = answer.replace(/<highlight>/g, '<font color="red">');
				answer = answer.replace(/<\/highlight>/g, '</font>');
			}
			
			//路径是否显示实例名称
			var path = item.bh_name;
			if (!pageContext.enableCateTag){
				path = item.bh_name + ' > ' + item.object_name;
			}
			// 模板
			var template0 = '<tr class="kbs-row-question"><td class="kbs-cell-question">{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td><span class="kbs-item" _name="detail" _id="{7}" _title="{8}">详情</span></td></tr><tr class="kbs-row-answer"><td colspan="7">{6}</td></tr>';
			//应东航要求，此处统一为“详情”，若其他客户需要区别，到时再加客户环境判断 modify by wilson.li at 20161017
			/*if (item.biztpl_type == 'article') {
				template0 = template0.replace(new RegExp('详情', 'g'), '文章');
			} else if (item.biztpl_type == 'group') {
				template0 = template0.replace(new RegExp('详情', 'g'), '标题');
			}*/
			$('#pageBar').before(template0.fill(question, item.create_time, item.edit_time, item.editor, item.visit_num, path, answer, item.question_id, item.question_w));
			
			/*当前行 Gassol.Bi Jul 6, 2016 5:12:42 PM*/
			var currentTR = $('#pageBar').prev().prev();
			$(currentTR).data('bizTplEnable', item.bizTplEnable);/*是否开启文章功能*/
			$(currentTR).data('bizTplArticleId', item.bizTplArticleId);/*文章id*/
			$(currentTR).data('bizTplArticleName', item.bizTplArticleName);/*文章名称*/
			$(currentTR).data('spotId', item.question_id);/*锚点id*/
		});
	}
	
	//搜索结果页面下的a标签都应该用一个新的标签页打开
	//modify by eko.zhan at 2016-06-01 14:10 
	$('.kbs-nav-body a[href^="http"]').attr('target', '_blank');
}
