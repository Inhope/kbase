/**
 * 用于 view_list.jsp
 */
$(function(){
	$('.kbs-question').click(function(){
		var _id = $(this).attr('_id');
		var _title = $(this).attr('_title');
		var param = {
			'id': _id //,
			//'faqId': _id	//modify by eko.zhan at 2016-11-14 15:45 不重复传入faqId，直接在 SearchAction#detail方法中计算
		};
		parent.TABOBJECT.open({
			id : _id,
			name : _title,
			hasClose : true,
			url : pageContext.contextPath + '/app/wukong/search!detail.htm?' + $.param(param),
			isRefresh : true
		}, this);
	});
	
	//A标签在新页面中打开
	$('.kbs-answer a[href^="http"],.kbs-answer-helios a[href^="http"]').attr('target', '_blank');
	
	//新手引导功能，低版本浏览器不考虑使用该功能
	if (!navigator.isLowerBrowser()){
		var intro = introJs();
		intro._options.nextLabel = '下一步 &rarr;';
		intro._options.prevLabel = '&larr; 上一步';
		intro._options.skipLabel = '忽略';
		intro._options.doneLabel = '完成';
		
		var steps0 = [{
						element: '.kbs-question',
						intro: '命中的标准问，单击可以进入知识详情页面'
					},
					{
						element: '.kbs-answer',
						intro: '知识对应的答案'
					},
					{
						element: '.kbs-intro-att',
						intro: '知识相关的附件和P4文件'
					},
					{
						element: '.kbs-intro-relatefaq',
						intro: '与标准问相关的知识点'
					}];
		
		var steps1 = [{
						element: '.kbs-search-result',
						intro: '搜索条件列表，可单击条件取消搜索'
					},
					{
						element: '#searchConditionPanel',
						intro: '搜索条件筛选'
					},
					{
						element: '.kbs-intro-result',
						intro: '搜索结果列表'
					},
					{
						element: '.kbs-merge button',
						intro: '单击可合并目录展示搜索列表'
					},
					{
						element: '#expandQa',
						intro: '单击可在当前页面预览搜索列表中的知识答案'
					}];
		
		
		if ($('.kbs-question').length==0){
			intro.setOptions({
				steps: steps1,
				tooltipPosition: 'auto',
				positionPrecedence: ['left', 'right', 'bottom', 'top']
			});
		}else{
			intro.setOptions({
				steps: $.merge(steps0, steps1),
				tooltipPosition: 'auto',
				positionPrecedence: ['left', 'right', 'bottom', 'top']
			});
		}
		
		//新手指引click事件
		$('#kbsIntro').click(function(){
			intro.start();
		});
		//完成写cookie
		$(document).on('click', '.introjs-skipbutton', function(){
			$.cookie('enableIntro', false, {expires: 9527});
		});
		//首次进入
		if ($.cookie('enableIntro')==null){
			intro.start();
		}
	}else{
		$('#kbsIntro').hide();
	}
});