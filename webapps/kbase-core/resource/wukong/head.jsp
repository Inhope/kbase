<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<script type="text/javascript">
	var pageContext = {
		contextPath: '${pageContext.request.contextPath }',
		converterPath: '${requestScope.converterPath}',
		robotContextPath: '${robotContextPath}',
		enableCateTag: '${requestScope.enableCateTag}'=='true',
		location: '${requestScope.location}',
		brand: '${requestScope.brand}',
		
		object_id: '${faq.object_id}',
		cateid: '${faq.category_id}',
		question_id: '${faq.question_id}',
		catePath: '${faq.bh}',
		
		//打开页面
		open: function(opts){
			var param = {width: '500px', height: '400px'};
			param = $.extend(param, opts);
			parent.__kbs_layer_index = parent.$.layer({
				type: 2,
				border: [5, 0.3, '#000'],
				title: false,
				iframe: {src : param.url},
				area : [param.width, param.height]
			});
		},
		
		//预览
		preview: function(fileName){
			var fileType = fileName.right(".");
			var fileId = fileName.replace(".p4","");
			
			var url = "";
			if(fileType && fileType == "p4"){
				url = pageContext.contextPath + '/app/wukong/search!p4.htm?id=' + fileId;
			}else{
				url = pageContext.converterPath + '/attachment/preview.do?fileName=' + fileName;
			}
			
			/**
			 * 预览修改成window.open modify by eko.zhan at 2016-06-07 14:20
			 */
			window.open(url);
			return false;
			parent.$.layer({
				type: 2,
				title: false,
				shade: [0.3, '#000'],
				border: [0],
				area: ['1000px', '500px'],
				iframe: {src: url}
			});
		}, 
		
		//下载附件
		download: function(fileid, filename){
			try{ 
	            var elemIF = document.createElement("iframe");
	            elemIF.src = pageContext.contextPath + '/app/wukong/search!download.htm?fileId=' + fileid + '&fileName=' + filename;   
	            elemIF.style.display = "none";
	            document.body.appendChild(elemIF);   
	        }catch(e){ 
	        	alert('文件异常，下载失败' + e);
	        }
		}
		
	}
	
</script>