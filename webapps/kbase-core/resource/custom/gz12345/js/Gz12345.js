/**
 * 广州12345-短信、工单功能
 * @author Gassol.Bi
 * @modified 2016-9-8 14:39:55
 */
;$(function(){
	var that = window.Gz12345 = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		/*短信内容标签处理*/
		tagHandle: function(con){
			con = con.replace(/<\s?br\s?\/?\s?>/g, '\n');/*<br/>换成换行符*/
			con = con.replace(/<[^>]+>/g, '');/*html标签处理*/
			con = con.replace(/(^\s*)|(\s*$)/g, '');/*去除首尾空白*/
			con = con.replace(/\"/g, '“');/*转义双引号*/
			return con;
		},
	});
	
	/*基础方法*/
	that.extend({
		req:{
			relate: function(params){
				/**/
				PageUtils.ajax({
					url: '/app/custom/gz12345/gz12345!relateReq.htm',
					params: params,
					after: function(jsonResult){
						jsonResult = jsonResult
						$.messager.alert('信息', jsonResult.msg);
					}
				});
			}
		},
		sms:{
			answer: null,
			open: function(params){
				/*答案的内容过长，因此采用从子页面采用parent方式获取答案*/
				// this.answer = fn.tagHandle($('#gzyd_sms_answer').html());
				this.answer = $('#gzyd_sms_answer').html();
				PageUtils.dialog.open('#dg-dialog', {
		    		title: '短信', width: 600,
		    		url: '/app/custom/gz12345/gz12345!toSMS.htm',
		    		params: params,
		    		buttons:[{
						text:'发送',
						handler:function(){
							/*调用子页面方法*/
							window.frames['dg-dialog-iframe'].sendSMS();
						}
					},{
						text:'关闭',
						handler:function(){
							$('#dg-dialog').dialog('close');
						}
					}]
		    	});
			},
			send: function(){
				
			}
		}
	});
});
