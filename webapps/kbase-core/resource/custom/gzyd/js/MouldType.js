/**
 * 广州移动-信息模板分类
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.MouldType = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		/*获取当前选中分类*/
		getSelectedType: function(){
			return $('#type-tree').tree('getSelected');
		}
	});
	
	/*基础方法*/
	that.extend({
		init : function(){
			/*新增*/
			$('#btnAddType').linkbutton({
			    onClick: function(){
			    	PageUtils.dialog.open('#mouldType-dialog', {
			    		title: '新增', width:350, height: 300,
			    		url: '/app/custom/gzyd/mould-type!toEdit.htm'
			    	});
			    }
			});
			
			/*编辑*/
			$('#btnEditType').linkbutton({
			    onClick: function(){
			    	var type = fn.getSelectedType();
			    	if(type && type.id){
			    		PageUtils.dialog.open('#mouldType-dialog', {
				    		params: {'id': type.id},
				    		title: '编辑', width:350, height: 300,
				    		url: '/app/custom/gzyd/mould-type!toEdit.htm'
				    	});
			    	} else {
			    		$.messager.alert('警告', '请选择需要编辑的分类!');
			    	}
			    }
			});
			
			/*删除*/
			$('#btnDelType').linkbutton({
			    onClick: function(){
			    	var type = fn.getSelectedType();
			    	if(type && type.id){
			    		$.messager.confirm('确认','您确认想要删除记录吗？', function(r){    
						    if (r){
						        PageUtils.ajax({
						    		params: {'id': type.id},
						    		url: '/app/custom/gzyd/mould-type!del.htm',
									after: function(jsonResult) {
										$.messager.alert('确认', jsonResult.msg, 'info',
											function(){
												if(jsonResult.rst) {
													$('#type-tree').tree('reload');
												}
											});
									}
						    	}); 
						    }
						});
			    	} else {
			    		$.messager.alert('警告', '请选择需要删除的分类!');
			    	}
			    }
			});
		}
	});
	/*初始化*/
	MouldType.init();
});
