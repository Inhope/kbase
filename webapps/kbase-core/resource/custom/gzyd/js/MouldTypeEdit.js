/**
 * 广州移动-信息模板分类-编辑
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.MouldTypeEdit = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		
	});
	
	/*基础方法*/
	that.extend({
		submit:function(){
			PageUtils.submit('#form1', {
				url: '/app/custom/gzyd/mould-type!doSave.htm',
				after: function(jsonResult){
					$.messager.alert('确认', jsonResult.msg, 'info',
						function(){
							if(jsonResult.rst) {
								/*关闭弹窗*/
								parent.PageUtils.dialog.close('mouldType-dialog');
								/*刷新树数据*/
								parent.$('#type-tree').tree('reload');
							}
					});
				}
			});
		},
		init: function(){
			/*表单提交*/
			$('#btn-save').click(function(){
				that.submit();
			});
		}
	});
	/*初始化*/
	MouldTypeEdit.init();
});
