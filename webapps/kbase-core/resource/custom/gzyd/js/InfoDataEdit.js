/**
 * 广州移动-信息
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.InfoDataEdit = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	var Analyze = {
		/*单行文本*/
		String: function(id, data){
			$(id).textbox({
				width: '100%',
			    value: data
			});
		},
		/*多行文本*/
		Text: function(id, data){
			$(id).textbox({
				width: '100%',
				height: '100px',
			    value: data,
			    multiline: true
			});
		},
		/*日期*/
		Date: function(id, data){
			$(id).datebox({
				value: data
			});
		},
		/*附件*/
		File: function(id, data){
			var absPath, filename, 
				prop = id.replace('#', '');
			var td = $(id).parent();
			var html = "<input type='file' name=\'"+prop+"\' style='width:\'300px\''/>";
			if(data){
				/*附件的路径以|%|分开,如果是历史数据，里面会存在|&|,将其替换掉*/
				data = data.replace("\|\&\|", "\|\!\|");
				var strs = data.split('\|\%\|');
				/*已上传的附件下载*/
		    	html +='<a href="javascript:void(0);" style="display:none;">预览</a><br/>';
				$('#'+prop).prop('value',data);
				for(var i = 0; i<strs.length; i++){
					var str = strs[i].split('\|\!\|');
					if(str[0] && str[1]){
						html += '<a id ="'+str[0]+'_1" href="javascript:void(0);" onclick="window.open(\'' 
						+ $.fn.getRootPath() + str[0] + '\')">' + str[1] + '</a>&nbsp;<a id ="'+str[0]
						+ '_2"  href="javascript:void(0);" onclick="InfoDataEdit.fn.del(\''+prop+'\',\''+data+'\',\''+strs[i]+'\')">删除</a>&nbsp;&nbsp;';
					}
				}
			}
			$(td).append(html);
		},
		/*图片*/
		Img: function(id, data){
			var absPath, filename, 
				prop = id.replace('#', '');
			var td = $(id).parent();
			var html ="<input type='file' name=\'"+prop+"\' style='width:\'300px\''/>";
			if(data){
				/*图片的路径以|%|分开,如果是历史数据，里面会存在|&|,将其替换掉*/
				data = data.replace("\|\&\|", "\|\!\|");
				var strs = data.split('\|\%\|');
				/*预览按钮*/
				html +='<a href="javascript:void(0);" style="display:none;">预览</a><br/>';
				$('#'+prop).prop('value',data);
				for(var i = 0; i<strs.length; i++){
					var str = strs[i].split('\|\!\|');
					if(str[0] && str[1]){
						html += '<a id ="'+str[0]+'_1" href="javascript:void(0);" onclick="parent.InfoData.fn.showPicture(\'' 
						+ $.fn.getRootPath() + str[0] + '\')">' + str[1] + '</a>&nbsp;<a id="'+str[0]
						+ '_2" href="javascript:void(0);"onclick="InfoDataEdit.fn.del(\''+prop+'\',\''+data+'\',\''+strs[i]+'\')">删除</a>&nbsp;&nbsp;';
					}
				}
			}
			$(td).append(html);
		},
		/*地市*/
		Location: function(id, data){
			/*属性名称*/
			var prop = $(id).attr('name'), 
				td = $(id).parent();
			$(id).remove();
			/*格式渲染-数据*/
			var locals = parent.InfoData.fn.locals;
			var group = 1;/*分组*/
			$(locals).each(function(i, local){
				var htm = '';/*格式渲染*/
				if(local != '/'){
					htm = '<input type="checkbox" value="' + local + '"';
					if(local.indexOf('中心') == -1){/*非中心节点*/
						htm += 'name="' + prop + '" group="' + group + '" ';
					} else {/*中心节点*/
						htm += 'name="" group="all' + group + '" ';
					}
					if(data && (',' + data.replace(/\|/g, ',') + ',')
						.indexOf(',' + local + ',') > -1) {
						htm += 'checked="checked"';
					}
					htm += ' />' + local + '&nbsp;&nbsp;';
				} else {
					htm = '<br/>';
					group++;
				}
				$(td).append(htm);
			});
			
			/*地市中心全选操作*/
			$(td).find('input[type="checkbox"][group^="all"]').
				each(function(){/*点击某个中心，默认勾选其所包含的所有地市*/
					$(this).click(function(){
						var group = $(this).attr('group').replace(/all/g, ''), 
							checked = $(this).attr('checked');
						$(td).find('input[type="checkbox"][group="' + group + '"]').each(function(){
							if(checked == 'checked')
								$(this).attr('checked', checked);
							else $(this).removeAttr('checked', checked);
						});
					});
			});
		},
		/*业务*/
		Category: function(id, data){
			/*数据处理*/
			var vals, texts, id_ = id.replace('#', '');
			if(data && data.indexOf('\|\&\|')>-1){
				var str = data.split('\|\&\|');
				vals = str[0];
				texts = str[1];
			}
			
			/*编辑窗渲染*/
			$(id).textbox({
				editable: false,
				width: '100%',
				height: '50px',
				multiline: true,
			    value: texts,
			    buttonText: '选择业务',
			    buttonIcon: 'icon-tip',
			    onClickButton: function(){
			    	PageUtils.dialog.open('#choose_category-dialog', {
			    		params: {
			    			target: id_,
			    			initData: 'InfoDataEdit.fn.choose.category.initData',/*回显数据方法名*/
							callback: 'InfoDataEdit.fn.choose.category.callback'/*弹窗回掉方法名*/
						},
			    		title: '业务选择', 
			    		width:$(window).width()*0.98, 
			    		height: $(window).height()*0.98,
			    		url: '/app/custom/gzyd/info-data!category.htm'
			    	});
			    }
			});
			$(id).after('<input type="hidden" name="' + id_ + '_">');
			
			/*数据回显*/
			if(vals && texts){
				$(id).textbox('setValue', vals);
				$(id).textbox('setText', texts);
				/*业务名称回显*/
				$('input[name="' + id_ + '_"]:eq(0)').val(texts);
			}
		},
		execute: function(target){
			var prop = $(target).attr('prop'), 
				type = $(target).attr('type'), 
				data = $(target).text();
			/*渲染执行(td, td位置， 初始化数据)*/
			if(type != 'Img' && type != 'File'){
				$(target).after('<input id="' + prop + '" name="' + prop + '"/>');
			}else{
				$(target).after('<input type=\'hidden\' id="' + prop + '" name="' + prop + '"/>');
			}
			this[type]('#' + prop, data);
		}
	};
	
	/*功能方法*/
	fn.extend({
		choose: {
			/*业务选择*/
			category: {
				initData: function(target){
					/*返回结果*/
					var initData = new Array();
					/*业务id, 业务名称*/
					var vals = $('#' + target).textbox('getValue'),
						texts = $('#' + target).textbox('getText');
					if(vals && texts){
						vals = vals.split(',');
						texts = texts.split(',');
						$(vals).each(function(i, o){
							initData.push({
								id: o, name: texts[i]
							});
						});
					}
					return initData;
				},
				callback: function(target, data){
					if(data && data.length > 0){
						var vals = new Array(), /*业务id*/
							texts = new Array(); /*业务名称*/
						$(data).each(function(i, item){
							vals.push(item.id);
							texts.push(item.name);
						});
						/*业务id, 业务名称*/
						texts = texts.join(',');
						$('#' + target).textbox('setValue', vals.join(','));
						$('#' + target).textbox('setText', texts);
						/*业务名称回显*/
						$('input[name="' + target + '_"]:eq(0)').val(texts);
					} else {
						$('#' + target).textbox('clear');
						/*业务名称回显*/
						$('input[name="' + target + '_"]:eq(0)').val('');
					}
				}
			}
		},
		del:function(prop,data,str){
			/*删除图片或者附件*/
			if($('#mapKey').val()){
				$('#mapKey').prop('value',$('#mapKey').val()+'\|\%\|'+str);
			}else{
				$('#mapKey').prop('value',$('#mapKey').val()+str);
			}
			var oldValue = $('#'+prop).val();
			var newValue ='';
			if(oldValue.indexOf(str+'\|\%\|') >-1){
				newValue = oldValue.replace(str+'\|\%\|','');
			}else if(oldValue.indexOf('\|\%\|'+str) >-1){
				newValue = oldValue.replace('\|\%\|'+str,'');
			}else{
				newValue = oldValue.replace(str,'');
			}
			$('#'+prop).prop('value',newValue);
			var strs = str.split('\|\!\|');
			$('body').find('a[id="' + strs[0] + '_1"]').remove();
			$('body').find('a[id="' + strs[0] + '_2"]').remove();
		}
	});
	/*基础方法*/
	that.extend({
		submit: function(){
			PageUtils.submit('#form1', {
				url: '/app/custom/gzyd/info-data!doSave.htm',
				after: function(jsonResult){
					$.messager.alert('确认', jsonResult.msg, 'info',
						function(){
							if(jsonResult.rst) {
								parent.PageUtils.dialog.close('infoData-dialog');
								parent.PageUtils.datagrid.reload();
							}
					});
				}
			});
		},
		init: function(){
			/*表单渲染*/
			$('table[class="box"]').find('textarea[type][prop]').each(function(){
				try {
					Analyze.execute(this);
				} catch(e){ console.log(e); }
			});
			
			/*easyui拓展*/
			$.extend($.fn.validatebox.defaults.rules, {
				img: {
					validator: function(value, param){
						if(value) {
		        			var fileName = value;
							var suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
							if (suffix == ('.jpg') || suffix == ('.bmp') 
								|| suffix == ('.png') || suffix == ('.gif')) {
								return true;
							} else {
								$(param[0]).textbox('setText', '');
							}
		        		}
						return false;
					}, 
					message: '请选择图片格式(jpg|bmp|png|gif)' 
				} 
			});
		}
	});
	/*初始化*/
	InfoDataEdit.init();
});

