/**
 * 广州移动-信息模板-编辑
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.InfoMouldEdit = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		datas:{/*相关数据*/
			datas: {},
			set:function(key, data){ this.datas[key] = data; },
			get:function(key){ return this.datas[key]; }
		}
	});
	
	/*基础方法*/
	that.extend({
		submit:function(){/*表单提交*/
			PageUtils.submit('#form1', {
				url: '/app/custom/gzyd/info-mould!doSave.htm',
				before: function(options, params){
					/*列表数据*/
					DatagridEdit.accept();// 保存表格数据
					params.rows = JSON.stringify($('#now-grid').datagrid('getData').rows);
				},
				after: function(jsonResult){
					$.messager.alert('确认', jsonResult.msg, 'info',
						function(){
							if(jsonResult.rst) {
								parent.PageUtils.dialog.close('infoMould-dialog');
								parent.PageUtils.datagrid.reload();
							}
					});
				}
			});
		},
		init : function(){
			
		}
	});
	/*初始化*/
	that.init();
});

/*列表编辑*/
window.DatagridEdit = {
	/*获取当前选中行(不存在，默认选中最后一行) */
	getIndex: function(){
		var row = $('#now-grid').datagrid('getSelected');
		if(row) {
			return $('#now-grid').datagrid('getRowIndex', row); 
		} else {
			/*不存在，默认选中最后一行*/
			var index = $('#now-grid').datagrid('getRows').length - 1;
			$('#now-grid').datagrid('selectRow', index)
			return index;
		}
	},
	/*表格数据总数*/
	getTotal: function(){
		return $('#now-grid').datagrid('getRows').length;
	},
    append: function(){
    	var total  = this.getTotal();
    	if(total < 15) {
    		var index = this.getIndex();
	        if(index != null){
	        	$('#now-grid').datagrid('insertRow', {index: index + 1, 
	        		row: {name:'自定义字段' + (index + 2), type:'String', query:'是', show:'是', enDel:true }});
		        $('#now-grid').datagrid('selectRow', index + 1)
		                .datagrid('beginEdit', index + 1);
	        }
    	} else {
    		$.messager.alert('警告', '自定义列不应该超过15列!');
    		return false;
    	}
    },
    remove: function(){
        var index = this.getIndex();
        if(index != null){
        	/*获取当前行数据*/
        	var rows = $('#now-grid').datagrid('getRows'), 
        		row = rows[index];
        	/*行允许删除*/
        	if(row != null && row.enDel && index > -1){
        		$('#now-grid').datagrid('deleteRow', index);
        	}
        	/*移至下一行*/
        	if(index - 1 > -1)
	            $('#now-grid').datagrid('selectRow', index-1);
        }
    },
    up: function(){
    	var row = $('#now-grid').datagrid('getSelected');
    	if(row) {
    		this.accept();/*保存缓存数据*/
    		row = $('#now-grid').datagrid('getSelected');
    		var index = $('#now-grid').datagrid('getRowIndex', row);
    		if(index != null && index > 0){
    			/*删除当前行*/
    			$('#now-grid').datagrid('deleteRow', index);
    			/*指定位置添加行*/
    			index = index - 1;
    			$('#now-grid').datagrid('insertRow', { 
    				index: index, row: row
    			});
    			$('#now-grid').datagrid('selectRow', index);
        	}
    	} else {
    		$.messager.alert('警告', '请选择目标行!');
    	}
    },
    down: function(){
    	var row = $('#now-grid').datagrid('getSelected');
    	if(row) {
    		this.accept();/*保存缓存数据*/
    		row = $('#now-grid').datagrid('getSelected');
    		var index = $('#now-grid').datagrid('getRowIndex', row);
    		if(index != null && index < this.getTotal() - 1){
    			/*删除当前行*/
    			$('#now-grid').datagrid('deleteRow', index);
    			/*指定位置添加行*/
    			index = index + 1;
    			$('#now-grid').datagrid('insertRow', { 
    				index: index, row: row
    			});
    			$('#now-grid').datagrid('selectRow', index);
        	}
    	} else {
    		$.messager.alert('警告', '请选择目标行!');
    	}
    },
    accept: function(){
    	$('#now-grid').datagrid('acceptChanges');
    },
    reject: function(){
        $('#now-grid').datagrid('rejectChanges');
    }
};

