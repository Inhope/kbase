/**
 * 广州移动-信息
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.InfoData = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		showPicture: function(url){
			PageUtils.dialog.open('#picture-dialog', {
	    		title: '预览', content:'<img src="' + url + '">'
	    	});
		},
		locals: ['江门中心', '江门', '阳江', '茂名', '/',
				 '广州中心', '广州', '清远', '云浮', '韶关', '/',
				 '深圳中心', '深圳', '/',
				 '东莞中心', '东莞', '梅州', '河源', '惠州', '/',
				 '佛山中心', '佛山', '肇庆', '中山', '珠海', '湛江', '/',
				 '汕头中心', '汕头', '汕尾', '潮州', '揭阳'],
		/*分类展示*/
		openCate:function(id, name, url){
			PageUtils.tab.open({
				id: id, name: name, url: url
			})
		},
		/*格式化图片展示*/
		fmtImg: function (value, row, index){
			if(value){
				value = value.replace('\|\&\|', '\|\!\|');
				var strs = value.split('\|\%\|');
				var html ='';
				for(var i=0;i<strs.length;i++){
					var str = strs[i].split('\|\!\|');
					html+= '<a href="javascript:void(0);" onclick="InfoData.fn.showPicture(\'' 
							+ $.fn.getRootPath() + str[0] + '\')">' + str[1] + '</a><br/>';
				}
				return html;
			}
			return '';
		},
		/*格式化附件展示*/
		fmtFile: function(value, row, index){
			if(value){
				value = value.replace('\|\&\|', '\|\!\|');
				var strs = value.split('\|\%\|');
				var html ='';
				for(var i=0;i<strs.length;i++){
					var str = strs[i].split('\|\!\|');
					html+= '<a href="javascript:void(0);" onclick="window.open(\'' 
							+ $.fn.getRootPath() + str[0] + '\')">' + str[1] + '</a><br/>';
				}
				return html;
			}
			return '';
		}
	});
	
	/*基础方法*/
	that.extend({
		init: function(){
			/*查询*/
			$('#now-search').linkbutton({
			    onClick: function(){
			    	/*查询跳转到第一页*/
				    PageUtils.datagrid.reload(1);
			    }
			});
			
			/*重置*/
			$('#now-clear').linkbutton({
			    onClick: function(){
			    	/*时间段*/
			   	 	$('input[type="radio"][name="period"]:checked').each(function(){
			   	 		this.checked = false;
			    		$(this).removeAttr('checked');
			    	});
			    	/*是否有效*/
			    	$('input[type="radio"][name="valid"]:checked').each(function(){
			    		this.checked = false;
			    		$(this).removeAttr('checked');
			    	});
			    	/*地市*/
			    	$('input[type="checkbox"][group]:checked').each(function(){
			    		this.checked = false;
			    		$(this).removeAttr('checked');
			    	});
			    	$('#searCon').textbox('setText', '');
			    }
			});
			
			/*新增*/
			$('#btn-add').linkbutton({
			    onClick: function(dialog, options){
			    	PageUtils.dialog.open('#infoData-dialog', {
			    		title: '新增', width:600, height: $(window).height()*0.98,
			    		url: '/app/custom/gzyd/info-data!toEdit.htm',
			    		params: {'mouldId': _MouldId},
			    		buttons:[{
							text:'保存',
							iconCls:'icon-save',
							handler:function(){
								/*调用子页面方法*/
								this.style.display = 'none';
								var InfoDataEdit = window.frames['infoData-dialog-iframe'].InfoDataEdit;
								InfoDataEdit.submit();
							}
						},{
							text:'关闭',
							iconCls:'icon-no',
							handler:function(){
								$('#infoData-dialog').dialog('close');
							}
						}]
			    	});
			    }
			});
			
			/*编辑*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var id = PageUtils.datagrid.getChecked();
			    	if(id){/*获取当前选中行*/
				    	PageUtils.dialog.open('#infoData-dialog', {
				    		params: {'dataId': id},
				    		title: '编辑', width:600, height: $(window).height()*0.98,
				    		url: '/app/custom/gzyd/info-data!toEdit.htm',
				    		buttons:[{
								text:'保存',
								iconCls:'icon-save',
								handler:function(){
									/*调用子页面方法*/
									var InfoDataEdit = window.frames['infoData-dialog-iframe'].InfoDataEdit;
									InfoDataEdit.submit();
								}
							},{
								text:'关闭',
								iconCls:'icon-no',
								handler:function(){
									$('#infoData-dialog').dialog('close');
								}
							}]
				    	});
			    	}
			    }
			});
			
			/*删除*/
			$('#btn-del').linkbutton({
			    onClick: function(){
			    	var ids = PageUtils.datagrid.getCheckeds();
			    	if(ids){/*获取当前选中行*/
			    		$.messager.confirm('确认','您确认想要删除记录吗？', function(r){    
						    if (r){    
						        PageUtils.ajax({
						    		params: {'ids': ids.join(',')},
						    		url: '/app/custom/gzyd/info-data!del.htm',
									after: function(jsonResult) {
										$.messager.alert('确认', jsonResult.msg, 'info',
											function(){
												if(jsonResult.rst) {
													PageUtils.datagrid.reload(1);
												}
											});
									}
						    	});   
						    }
						});
			    	}
			    }
			});
			
			/*查询条件地市渲染*/
			var td = $('#locals').parent();
			if(td){
				var group = 1;
				$(fn.locals).each(function(i, local){
					var htm = '';/*格式渲染*/
					if(local != '/'){
						htm = '<input type="checkbox" value="' + local + '" '; 
						if(local.indexOf('中心') == -1){/*非中心节点*/
							htm += 'name="locals" group="' + group + '" ';
						} else {/*中心节点*/
							htm += 'name="" group="all' + group + '" ';
						}
						htm += ' />' + local + '&nbsp;&nbsp;';
					} else {
						htm = '<br/>';
						group++;
					}
					$(td).append(htm);
				});
				
				/*地市中心全选操作*/
				$(td).find('input[type="checkbox"][group^="all"]').
					each(function(){/*点击某个中心，默认勾选其所包含的所有地市*/
						$(this).click(function(){
							var group = $(this).attr('group').replace(/all/g, ''), 
								checked = $(this).attr('checked');
							$(td).find('input[type="checkbox"][group="' + group + '"]').each(function(){
								if(checked == 'checked')
									$(this).attr('checked', checked);
								else $(this).removeAttr('checked', checked);
							});
						});
				});
			}
			
			/*导入*/
			$('#btn-imp').linkbutton({
			    onClick: function(){
			    	PageUtils.dialog.open('#infoData-import', {
			    		params: {'mouldId': _MouldId},
			    		title: '导入', width:400, height: 250,
			    		url: '/app/custom/gzyd/info-data!importData.htm',
			    		buttons:[{
							text:'导入',
							iconCls:'icon-save',
							handler:function(){
								/*调用子页面方法*/
								var InfoDataImport = window.frames['infoData-import-iframe'].InfoDataImport;
								InfoDataImport.submit();
							}
						},{
							text:'关闭',
							iconCls:'icon-no',
							handler:function(){
								$('#infoData-import').dialog('close');
							}
						}]
			    	});
			    }
			});
			
			/*导出*/
			$('#btn-exp').linkbutton({
			    onClick: function(){
			    	var params = PageUtils.datagrid.getQueryParams();
			    	params.mouldId = _MouldId;/*指定模板*/
			    	ExportUtils.toExcel('/app/custom/gzyd/info-data!exportData.htm',
						'/app/custom/gzyd/info-data!exportResult.htm', params);
			    }
			});
		}
	});
	/*初始化*/
	InfoData.init();
});
