/**
 * 广州移动-信息模板
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.InfoMould = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		/*获取当前选中分类*/
		getSelectedType: function(){
			return $('#type-tree').tree('getSelected');
		},
		/*信息检索*/
		searchData: function(id, name, url){
			PageUtils.tab.open({
				id: id, name: name, url: url
			})
		}
	});
	
	/*基础方法*/
	that.extend({
		init : function(){
			/*查询*/
			$('#now-search').linkbutton({
			    onClick: function(){
			    	/*查询跳转到第一页*/
				    PageUtils.datagrid.reload(1);
			    }
			});
			/*重置*/
			$('#now-clear').linkbutton({
			    onClick: function(){
			    	$('#code').textbox('setText', '');
					$('#name').textbox('setText', '');
					$('#type-tree').tree('unselect');
			    }
			});
			
			/*新增*/
			$('#btn-add').linkbutton({
			    onClick: function(dialog, options){
			    	PageUtils.dialog.open('#infoMould-dialog', {
			    		title: '新增', width:600, height: $(window).height()*0.98,
			    		url: '/app/custom/gzyd/info-mould!toEdit.htm',
			    		buttons:[{
							text:'保存',
							iconCls:'icon-save',
							handler:function(){
								/*调用子页面方法*/
								this.style.display = 'none';
								var InfoMouldEdit = window.frames['infoMould-dialog-iframe'].InfoMouldEdit;
								InfoMouldEdit.submit();
							}
						},{
							text:'关闭',
							iconCls:'icon-no',
							handler:function(){
								$('#infoMould-dialog').dialog('close');
							}
						}]
			    	});
			    }
			});
			/*编辑*/
			$('#btn-edit').linkbutton({
			    onClick: function(){
			    	var id = PageUtils.datagrid.getChecked();
			    	if(id){/*获取当前选中行*/
				    	PageUtils.dialog.open('#infoMould-dialog', {
				    		params: {'id': id},
				    		title: '编辑', width:600, height: $(window).height()*0.98,
				    		url: '/app/custom/gzyd/info-mould!toEdit.htm',
				    		buttons:[{
								text:'保存',
								iconCls:'icon-save',
								handler:function(){
									/*调用子页面方法*/
									var InfoMouldEdit = window.frames['infoMould-dialog-iframe'].InfoMouldEdit;
									InfoMouldEdit.submit();
								}
							},{
								text:'关闭',
								iconCls:'icon-no',
								handler:function(){
									$('#infoMould-dialog').dialog('close');
								}
							}]
				    	});
			    	}
			    }
			});
			
			/*删除*/
			$('#btn-del').linkbutton({
			    onClick: function(){
			    	var id = PageUtils.datagrid.getChecked();
			    	if(id){/*获取当前选中行*/
			    		$.messager.confirm('确认','您确认想要删除记录吗？', function(r){    
						    if (r){    
						       	PageUtils.ajax({
						    		params: {'id': id},
						    		url: '/app/custom/gzyd/info-mould!del.htm',
									after: function(jsonResult) {
										$.messager.alert('确认', jsonResult.msg, 'info',
											function(){
												if(jsonResult.rst) {
													PageUtils.datagrid.reload(1);
												}
											});
									}
						    	}); 
						    }
						});
			    	}
			    }
			});
		}
	});
	/*初始化*/
	that.init();
});
