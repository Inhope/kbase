/**
 * 广州移动-短信、归档功能
 * @author Gassol.Bi
 * @modified 2016-4-22 22:39:13
 */
;$(function(){
	var that = window.GzydMappings = {}, 
		fn = that.fn = {};
	that.extend = fn.extend = $.extend;
	
	/*功能方法*/
	fn.extend({
		/*归档*/
		filing: {
			open: function(){
				/*业务映射数据*/
				var treeSrtype = null;
				if(categoryId != ""){
					$.ajax({
						type : 'post',
						url : $.fn.getRootPath()+"/app/custom/gzyd/gzyd-mappings!treeSrtype.htm", 
						data : {'categoryId': categoryId},
						async: false,
						dataType : 'json',
						success : function(data){
							treeSrtype = data;
						},
						error : function(msg){
							alert('网络错误，请稍后再试!');
						}
					});	
				}
				/*获取相关数据*/
				var ngccObj = SMSUtils.getData();
				if(!ngccObj) return false;
				/*打开窗体*/
				fn.shade.open({
					width: 350,
					height: 250,
					url: $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!guidangTo.htm',
					args: {'treeSrtype': treeSrtype, 'ngccObj': ngccObj}
				});
			}
		},
		
		/*短信*/
		sms: {
			/*短信行过滤*/
			rows: null,
			filter: function(obj){
				var rows = this.rows,
					rowsinit = $(obj).attr('rowsinit'),
					checked = $(obj).attr('checked');
				if(rowsinit != '1'){
					$(obj).attr('disabled', true);
					fn.sms.rows = rows = $('#all table').
						find('tr[gzyd_sms_isableSMS=false]');
					$(obj).attr('disabled', false);
					$(obj).attr('rowsinit', '1');
				}
				if(checked) checked = 'none';
				else checked = '';
				$(rows).each(function(){
					$(this).css({'display': checked});
				});
			},
			/*弹窗打开*/
			open: function(isRClick){
			    if(typeof(categoryId)==undefined)categoryId='';
				/*是否右键打开*/
				var qas = this.getQas(isRClick);
				if(!qas) return false;
				var ngccObj = SMSUtils.getData();
				if(!ngccObj) return false;
				/*打开短信弹窗*/
				fn.shade.open({
					width: 900,
					height: 600,
					url: $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!duanxinTo.htm',
					args: {'qas': qas, 'ngccObj': ngccObj, 'categoryId': categoryId}
				});
				/*打开归档弹窗*/
				//that.filing.guidang();
			},
			/*弹窗打开(不使用showModalDialog,移动二次需求2016-10-25 17:33:00)*/
			open1: function(isRClick){
				var _diaWidth = 900;
				var _diaHeight = 600;
				var _scrWidth = screen.width;
				var _scrHegiht = screen.height;
				var url = $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!duanxinTo.htm';
				if(url.indexOf('?') > -1)
					url += '&isRClick=' + isRClick;
				else url += '?isRClick=' + isRClick;
				var top = (_scrHegiht - _diaHeight)/2, 
					left = (_scrWidth - _diaWidth)/2;
				var win = window.open(url , '短信发送', 'left=0,top=0,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no,resizable=yes,width=' 
					+ _diaWidth + ',height=' + _diaHeight + ',top=' + top + ',left=' + left);
				win.focus();
			},
			/*弹窗打开,获取标准问数据(不使用showModalDialog,移动二次需求2016-10-25 17:33:00)*/
			getData1:function(isRClick){
				if(typeof(categoryId) == 'undefined')
					categoryId = '';
				/*是否右键打开*/
				var qas = this.getQas(isRClick);
				if(!qas) return false;
				var ngccObj = SMSUtils.getData();
				if(!ngccObj) return false;
				return {'qas': qas, 'ngccObj': ngccObj, 'categoryId': categoryId};
			},
			
			/*获取短信标题及短信内容*/
			getQas: function(isRClick){
				var qa = '';
				if(!isRClick){//不是右键发送短信
					if($("#gzyd_sms_question").length!=0 && $("#gzyd_sms_answer").length!=0){
						//来自知识详情展示
						var q = this.tagHandle($("#gzyd_sms_question").text());
						var a = this.tagHandle($("#gzyd_sms_answer").text());
						var qId = $("#gzyd_sms_question").attr("gzyd_sms_qId");
						var isableSMS = $("#gzyd_sms_question").attr("isableSMS");
						if(isableSMS == 'true'){
							qa += '{qid:"'+qId+'",q:"'+q+'",a:"'+a+'"},';
						}else{
							alert("知识点:'"+q+"',<br/>无权限发送短信!");
							return false;
						}
					} else if (window.__kbs_fav_sms==1){	//add by eko.zhan at 2016-07-30 13:00
						qa = window.__kbs_fav_sms_data;
					} else {//来自分类展示
						var gzyd_sms_checkbox = "gzyd_sms_checkbox", tabId = '';
						/*	:visible这种写法ie7下面太卡
							tabId = $('#contDetail').find("div[class='gz-cn2']:visible").attr("id");
						*/
						$($('div[class="gz-cn2"]')).each(function(){
							if($(this).css('display') != 'none') {
								tabId = $(this).attr('id');
								return false;
							}
						});
						//判断是否来自“显示所有”选择卡
						if(tabId != 'all') gzyd_sms_checkbox += "_" + tabId;
						//来自知识分类展示
						//alert(gzyd_sms_checkbox);
						var sms_list = $("input:checkbox[name="+gzyd_sms_checkbox+"]:checked");
						if(sms_list == null || sms_list.length == 0){
							alert("请选择知识点!");
							return false;
						}
						for(var i=0;i<sms_list.length;i++){
							var q = this.tagHandle($(sms_list[i]).attr("gzyd_sms_question"));
							var a = this.tagHandle($(sms_list[i]).attr("gzyd_sms_answer"));
							var qId = $(sms_list[i]).attr("valueId");
							var s = $(sms_list[i]).attr("gzyd_sms_isablesms");
							if(s == 'true'){
								qa += '{qid:"'+qId+'",q:"'+q+'",a:"'+a+'"},';
							}else{
								alert("知识点:'"+q+"',<br/>无权限发送短信!");
								return false;
							}
						}
					}
				} else {//右键发送短信
					var q = this.tagHandle($(gzyd_sms_checkbox_obj).attr("gzyd_sms_question"));
					var a = this.tagHandle($(gzyd_sms_checkbox_obj).attr("gzyd_sms_answer"));
					var qId = $(gzyd_sms_checkbox_obj).attr("valueId");
					var s = $(gzyd_sms_checkbox_obj).attr("gzyd_sms_isablesms");
					if(s == 'true'){
						qa += '{qid:"'+qId+'",q:"'+q+'",a:"'+a+'"},';
					}else{
						alert("知识点:'"+q+"',<br/>无权限发送短信!");
						return false;
					}
				}
				
				qa = qa.replace(/^,|,$/,'');
				return eval('['+qa+']');
			},
			/*
			 * 标准问、标准回复内容处理
			 */
			tagHandle: function(con){
				con = con.replace(/<[^>]+>/g, "");//html标签处理
				//con = con.replace(/</g, '&lt').replace(/>/g, '&gt');//html标签处理
				con = con.replace(/(^\s*)|(\s*$)/g, "");//去除首尾空白
				con = con.replace(/\"/g, "\\\"");//转义双引号
				return con;
			},
			add: function(question , answer){
			    var _question = this.tagHandle(question);
			    var _answer = this.tagHandle(answer);
			    $.ajax({
			        type: 'post',
			        url: $.fn.getRootPath() + '/app/custom/gzyd/gzyd-mappings!addDuanxin.htm',
			        data: {'question': _question, 'answer': _answer},
			        async: false,
			        dataType: 'json',
			        success: function(data){
			            alert(data);
			        },
			        error: function(msg){
			            alert('网络错误，请稍后再试!');
			        }
			    });
			} 
		},
		
		/*弹窗打开*/
		shade: {
			open: function(opts){
				var _diaWidth = opts.width;
				var _diaHeight = opts.height;
				var _scrWidth = screen.width;
				var _scrHegiht = screen.height;
				var _diaLeft = (_scrWidth-_diaWidth)/2;
				var _diaTop = (_scrHegiht-_diaHeight)/2;
				var params = 'dialogHeight:'+_diaHeight+'px;dialogWidth:'
					+_diaWidth+'px;dialogLeft:'+_diaLeft+'px;dialogTop:'+_diaTop+'px;center:1;';
				window.showModalDialog(opts.url, opts.args, params);
			}
		}
	});
	
	/*基础方法*/
	that.extend({
		/*短信相关*/
		sms:{
			/*短信显示*/
			filter: function(obj){
				fn.sms.filter(obj);
			},
			/*发短信*/
			send: function(isRClick){
				// fn.sms.open(isRClick);
				// 
				fn.sms.open1(isRClick);
			},
			/*添加短信*/
			add: function(question, answer){
				fn.sms.add(question, answer);
			}
		},
		
		/*归档相关*/
		filing:{
			guidang: function(){
				fn.filing.open();
			}
		}
	});
});

/*获取“C:/CSP_DATE/yyyyMMdd.txt”中相关参数*/
var SMSUtils = {
	_basePath: 'C:/CSP_DATE/',
	_obtainToday: function(){
		var date = new Date();
		date.setDate(date.getDate());
		var y = date.getFullYear() + '';
		var m = (date.getMonth() + 1) + '';
		var d = date.getDate() + '';
		if(m < 10) m = '0' + m;
		if(d < 10) d = '0' + d;
		return (y + m + d);
	},
	_obtainData: function(src){
		var ts;
		try {
			var fso = new ActiveXObject("Scripting.FileSystemObject");
			if(!src) src = this._basePath + this._obtainToday() + '.txt';
			ts = fso.OpenTextFile(src, 1);/*1表示只读*/
			var arr = ts.ReadAll();
			if(arr.indexOf('\r\n') > -1){
				arr = arr.split('\r\n');
				return arr[arr.length-2];/*获取最后一条数据*/
			} else {
				return arr;
			}
		} catch (err){
			alert('错误：'+err + '' 
				+ '\n原因：1.请确认今日已做过相关业务操作，以确保记录文件的生成' 
				+ '\n      2.当前功能仅支持IE浏览器');
		} finally {
			if(ts) ts.Close();
		}
		return null;
	},
	getData: function (){
		var data  = this._obtainData();/*获取处理数据*/
		if(data && data.indexOf('|') > -1){
			data = data.split('|');
			var ngccObj = {};/*返回结果*/
			ngccObj.SERIALNO = data[0];
			ngccObj.CONTACTID = data[1];
			ngccObj.SUBSNUMBER = data[2];
			ngccObj.SUBSCITYID = data[3];
			ngccObj.SUBSBRANDID = data[4];
			ngccObj.DATATIME = data[5];
			return ngccObj;
		}
		return false;
	}
}
