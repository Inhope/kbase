<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title>知识库公告</title>	
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/jquery/css/jquery-ex.css"/>
		<link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/css.css" rel="stylesheet" type="text/css" />
	    <link href="${pageContext.request.contextPath}/theme/${sessionScope.session_user_key.userInfo.userTheme}/resource/notice/css/gonggao.css" rel="stylesheet" type="text/css" />
	    <style type="text/css">
	        body{margin:0px 0px 0px 0px}		
	    </style>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery.js" ></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/library/jquery/js/Jquery-ex.js" ></script>
	    <script type="text/javascript">
	        var categoryTag = "${sessionScope.category_tag}";
	        function getNoticeContent(){
	            var _noticeId = '${param.id}';
	            var _url = $.fn.getRootPath() + '/app/notice/bulletin!read.htm?id='+_noticeId;
				try{
					/*
			            window.opener.TABOBJECT.open({
								id : 'notice',
								name : '公告管理',
								hasClose : true,
								url : _url,
								isRefresh : true
						}, this);*/
					opener.KbasePlugin.openNotice(_url);
				}catch(e){
					alert(e.message);
				}				
				window.close();
	        }
	    </script>
	</head>
	
	<body>
         <table class="popup-box" width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="middle" style="line-height: 30px;">
					<span class="popup-notice-title">${param.title}</span><br>
					<span class="popup-notice-subtitle">
						<b>创建人：</b><span>${param.username} </span><b>创建日期：</b><span>${param.edittime}</span>
					</span>
				</td>
			</tr>
			<tr>
				<td class="popup-notice-con" align="right">
					<a href="javascript:void(0);" onclick="getNoticeContent();"><b>查看详情</b></a>
				</td>
			</tr> 
		
		 </table>		
	</body>
</html>
