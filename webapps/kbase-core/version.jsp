<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<link rel="stylesheet" type="text/css" href="theme/red/resource/jquery/css/jquery-ex.css" />
		<script type="text/javascript" src="library/jquery/js/Jquery.js"></script>
		<script type="text/javascript" src="library/jquery/js/Jquery-ex.js"></script>
		<script type="text/javascript">
		<!---->
		//弹出新层
		function seemore(){
			parent.$.layer({
				type: 2,
				border: [10, 0.3, '#000'],
				title: "相关服务",
				closeBtn: [0, true],
				iframe: {src : 'versionother.jsp'},
				area: ['700px', '400px']
			});
		}
			
		</script>
		<!-- layer -->
		<script type="text/javascript" src="library/layer/layer.min.js"></script>
	</head>
	<body>
		<table width="900" cellspacing="0" cellpadding="0" border="0"
			style="margin: 0 auto;">
			<tbody>
				<tr>
					<td>
						<div
							style="width: 900px; text-align: left; font: 12px/ 15px simsun; color: #000; background: #eee;">
							<div style="padding: 72px 100px">
								<div style="height: 50px; background: #51a0e3;">
									<b style="font-size: 35px;color: white;position: absolute;padding: 20px 0 0 10px;">知识库KBS</b>
								</div>
								<div style="padding: 37px 0 81px 0; border: 1px solid #e7e7e7; font-size: 14px; color: #6e6e6e; background: #FFF;">
									<div style="padding: 0 0 26px 41px;">
										<table width="616" cellspacing="0" cellpadding="0" border="0"
											style="border-top: 1px solid #eee; border-left: 1px solid #eee;">
											<thead>
												<tr>
													<th
														style="padding: 10px 0; border-right: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center; background: #f8f8f8;"
														colspan="2">
														版本信息
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td
														style="padding: 10px 0; border-right: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;">
														发布时间
													</td>
													<td
														style="padding: 10px 0 10px 40px; border-right: 1px solid #eee; border-bottom: 1px solid #eee;">
														2015-03-16 10:00
													</td>
												</tr>
												<tr>
													<td
														style="padding: 10px 0; border-right: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;">
														当前版本
													</td>
													<td
														style="padding: 10px 0 10px 40px; border-right: 1px solid #eee; border-bottom: 1px solid #eee; color: #ff0000;">
														2.0.3
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div style="padding:0 0 26px 41px;color:#6e6e6e;font-size:12px;">
				                       	 相关服务版本信息详情，请访问 
				                       	 <a style="color:#3058a8;" href="javascript:seemore();">更多</a>。
				                    </div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>

