<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.eastrobot.util.file.PropertiesUtil"%>
<style>
table tr th{padding: 10px 0; border-right: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center; background: #f8f8f8;}
.td1{padding: 10px 0; border-right: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;font-size: 14px;}
.td2{padding: 10px 0 10px 40px; border-right: 1px solid #eee; border-bottom: 1px solid #eee;font-size: 14px;}
</style>
<script type="text/javascript">
</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0"
	style="border-top: 1px solid #eee; border-left: 1px solid #eee;">
	<thead>
		<tr>
			<th colspan="2">
				版本信息
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="td1">
				流程引擎服务(kbase-workflow)
			</td>
			<td class="td2">
				<c:catch var="catchException1">
					<c:import url="<%=PropertiesUtil.value("kbase.workflow.context_path")+"/version_v.jsp"%>"></c:import>
				</c:catch>
				<c:if test = "${catchException1 != null}">未知</c:if>
				<a target="_blank" href="<%=PropertiesUtil.value("kbase.workflow.context_path")+"/version.jsp"%>">更多</a>
			</td>
		</tr><tr>
			<td class="td1">
				流程引擎数据同步服务(kbase-sync)
			</td>
			<td class="td2">
				<c:catch var="catchException2">
					<c:import url="<%=PropertiesUtil.value("kbase.sync")+"/version_v.jsp"%>"></c:import>
				</c:catch>
				<c:if test = "${catchException2 != null}">未知</c:if>
				<a target="_blank" href="<%=PropertiesUtil.value("kbase.sync")+"/version.jsp"%>">更多</a>
			</td>
		</tr>
		<tr>
			<td class="td1">
				报表服务(kbase-report)
			</td>
			<td class="td2">
				<c:catch var="catchException3">
					<c:import url="<%=PropertiesUtil.value("kbase.report")+"/version_v.jsp"%>"></c:import>
				</c:catch>
				<c:if test = "${catchException3 != null}">未知</c:if>
				<a target="_blank" href="<%=PropertiesUtil.value("kbase.report")+"/version.jsp"%>">更多</a>
			</td>
		</tr>
		<tr>
			<td class="td1">
				搜索服务(kbase-search)
			</td>
			<td class="td2">
				<c:catch var="catchException4">
					<c:import url="<%=PropertiesUtil.value("robot.search.context_path")+"/version_v.jsp"%>"></c:import>
				</c:catch>
				<c:if test = "${catchException4 != null}">未知</c:if>
				<a target="_blank" href="<%=PropertiesUtil.value("robot.search.context_path")+"/version.jsp"%>">更多</a>
			</td>
		</tr>
	</tbody>
</table>
