<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isErrorPage="true" %>
<%@ page import="org.apache.commons.logging.LogFactory" %>
<% response.setStatus(200); %>
<% 
	try{
		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri"); 
		LogFactory.getLog(requestUri).error(exception.getMessage(), exception);
	}catch(Exception e){
		
	}
%>
<!DOCTYPE HTML>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>出错页面</title>
		<style type="text/css">
			body{
				font-size: 14px;
			}
			a{
				font-size: 12px;
			}
			pre{
				white-space: pre-wrap;       
				white-space: -moz-pre-wrap;
				white-space: -pre-wrap;      
				white-space: -o-pre-wrap;    
				word-wrap: break-word;       
			}
		</style>
	</head>
	
	<body>
		<h5>对不起！系统发生错误，请与系统管理员联系。</h5>
		<a id="detail" href="javascript:void(0);" onclick="showDetail();">查看详细信息</a>
		<hr size="1">
		<div id="detail_error_msg" style="display:none" align=left>
			<pre>
				<% 
					try{
						exception.printStackTrace(new java.io.PrintWriter(out));
					}catch(Exception e){
						
					}
				%>
			</pre>
		</div>
		<script type="text/javascript">
			function showDetail(){
				if(detail_error_msg.style.display==''){
					detail_error_msg.style.display = 'none';
					detail.innerHTML = "查看详细信息";
				}else{
					detail_error_msg.style.display = '';
					detail.innerHTML = "隐藏详细信息";
				}
			}
		</script>
	</body>
</html>