<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">

		<title>sqlrunner</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<!--
			<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<script type="text/javascript" src="${pageContext.request.contextPath }/library/jquery/js/Jquery.js"></script>
		<script type="text/javascript">
			$(function(){
				var time = new Date().getFullYear()  + '-' 
				 + (new Date().getMonth()+1) + '-'
				 + new Date().getDate() + '-'
				 + new Date().getHours() + ':'
				 + new Date().getMinutes();
				var urlTime = window.location.href.split('?time=')[1].split('&author=')[0];
				var author =  window.location.href.split('&author=')[1];
				if(urlTime != time){
					window.location.href = '${pageContext.request.contextPath }';
					return false;
				}
				$('#btn').click(function(){
					var sql = $.trim($('#in').val());
					if(sql == ''){
						alert('你倒是把sql写了啊!!!');
					} else {
						$.ajax({
	   						type: "POST",
		   					url: "${pageContext.request.contextPath }/app/sqlrun/sql-run!sqlrun.htm",
		   					data: {sql:sql,author:author},
		   					dataType:'json',
		   					async: true,
		   					success: function(msg){
		   						if(!msg){
		   							return;
		   						}
		   						if(msg.success == true){
		   							var data = msg.data;
		   							var txt = '';
		   							for(var i = 0; i < data.length; i++){
		   								txt += '<tr>'
		   								for(var j = 0; j < data[i].length; j++){
		   									txt += '<td style="border:1px solid;">';
		   									txt += data[i][j];
		   									txt += '</td>';
		   								}
		   								txt += '</tr>'
		   							}
		   							$('#table').html(txt);
		   							$('#div2').show();
		   							$('#div1').hide();
		   						}else{
		   							alert(msg.message);
		   						}
		   					},
		   					error :function(){
					   			alert('ajax访问失败!!!');
					   		}
						});
					}
				});
				$('#back').click(function(){
					$('#div1').show();
					$('#div2').hide();
				});
			});
		</script>
	</head>

	<body>
		<div id="div1">
			<div>
				<textarea id="in" style="resize:none;width:500px;height:500px;"></textarea>
			</div>
			<div>
				1.执行sql需要登录<br/>
				2.执行sql请自己写分页，超过100行的不执行<br/>
				<button id="btn">点击执行sql</button>
			</div>
  		</div>
  		<div  id="div2" style="display:none;">
  			<button id="back">返回输入界面</button>
  			<table id="table" style="border-collapse:collapse;"></table>
  		</div>
		
	</body>
</html>
