<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="org.springframework.web.context.ContextLoader"%>
<%@page import="org.springframework.jdbc.core.JdbcTemplate"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
Calendar cal1 = null;
Calendar cal0 = null;
cal0 = Calendar.getInstance();
out.println("开始载入页面 " + cal0.getTime());

JdbcTemplate jdbcTemplate = getBean(JdbcTemplate.class);

cal0 = Calendar.getInstance();
out.println("<br/>开始执行搜索 " + cal0.getTime());
List<Map<String, Object>> list = jdbcTemplate.queryForList("select v.value_id id,v.question q,v.create_time crt,v.edit_time edt,v.editor editor,v.enable_reverse_ask flag, "+
		 " dv.id dvid,dv.value ans,dv.start_time st,dv.end_time et,dv.end_time1 et1,dv.cmd cmd,dt.tag tag,kd.code code ,dsr.search_count dscount, "+
		 " o.object_id oid,o.name oname,c.bh bh,c.id cid,c.name cname,c.parent_id parent_category "+
		 " ,bta.tpl_id tpl_id,bta.attr_group1 attr_group1,bta.attr_group2 attr_group2 ,bta.priority priority "+ 
		 " from KB_VAL v inner join KB_DIM_VAL dv on v.value_id=dv.value_id "+
		 " inner join KB_OBJECT o on o.object_id=v.object_id "+
		 " inner join KB_CATEGORY c on c.id=o.cat_id "+
		 " left join KB_DIM_VAL_TAG dvt on dvt.dim_value_id=dv.id "+ 
		 " left join PUB_KB_DIM_TAG dt on dvt.dim_tag_id=dt.id "+
		 " left join PUB_KB_DIM kd on dt.dim_id=kd.id "+
		 " left join DEV_SEARCH_RECORD dsr on v.value_id=dsr.id "+ 
		 " left join KB_BIZ_TPL bt on  bt.id=o.biz_tpl_id "+
		 " left join KB_BIZ_TPL_ATTR bta on v.attribute_id=bta.attr_id and bt.id = bta.tpl_id "+ 
		 " where v.question is not null and v.disabled=0 and v.state=1 "+
		 " and (bt.id= bta.tpl_id or bta.tpl_id is null) "+
		 " and o.object_id ='0014448204343586397900215e28437e' "+
		 " order by c.id,o.object_id,v.value_id,dv.id ");
cal1 = Calendar.getInstance();
out.println("<br/>执行搜索完毕，耗时： " + (cal1.getTimeInMillis()-cal0.getTimeInMillis()));

out.println("<br/>共找到 " + list.size() + " 条结果");

cal0 = Calendar.getInstance();
out.println("<br/>开始遍历数据 " + cal0.getTime());

out.println("<br/>");
out.println("<table width=\"100%\">");
for (Map<String, Object> map : list){
	out.println("<tr>");
	Calendar cal00 = Calendar.getInstance();
	List<Map<String, Object>> list3 = jdbcTemplate.queryForList("select f.ID as fid,f.NAME as fname,f.CREATE_TIME as ft from RA_FILE f inner join RA_FILE__OBJECT o on f.ID = o.FILE_ID where o.OBJECT_ID='" + map.get("DVID") + "'");
	for (Map<String, Object> map4 : list3){
		List<Map<String, Object>> list4 = jdbcTemplate.queryForList("select PATH,NAME,ID,FILE_ID,CREATE_TIME,EDITOR,REMARK from RA_PHYSICAL_FILE where FILE_ID='" + map4.get("ID") + "'");
	}
	Calendar cal11 = Calendar.getInstance();
	out.println("<td>查询乱七八糟的耗时 " + (cal11.getTimeInMillis()-cal00.getTimeInMillis()) + " </td>");
	out.println("<td>" + map.get("ID") + "</td>");
	out.println("<td>" + map.get("Q") + "</td>");
	out.println("<td>" + map.get("CRT") + "</td>");
	out.println("<td>" + map.get("EDT") + "</td>");
	out.println("<td>" + map.get("EDITOR") + "</td>");
	out.println("<td>" + map.get("ANS") + "</td>");
	out.println("<td>" + map.get("DSCOUNT") + "</td>");
	out.println("<td>" + map.get("ONAME") + "</td>");
	out.println("<td>" + map.get("CNAME") + "</td>");
	out.println("<td>" + map.get("BH") + "</td>");
	out.println("</tr>");
	
}
out.println("</table>");
cal1 = Calendar.getInstance();
out.println("<br/>数据遍历完毕，耗时 " + (cal1.getTimeInMillis()-cal0.getTimeInMillis()));

out.println("<br/>=========================================================================");

cal0 = Calendar.getInstance();
List<Map<String, Object>> list1 = jdbcTemplate.queryForList("select id id,bh bh from kb_category");
cal1 = Calendar.getInstance();
out.println("<br/>执行搜索完毕，耗时： " + (cal1.getTimeInMillis()-cal0.getTimeInMillis()) + "，共找到 " + list1.size() + " 条数据");

out.println("<br/>=========================================================================");
cal0 = Calendar.getInstance();
List<Map<String, Object>> list2 = jdbcTemplate.queryForList("select d.code c,dt.tag t from pub_kb_dim d inner join pub_kb_dim_tag dt on d.id=dt.dim_id");
cal1 = Calendar.getInstance();
out.println("<br/>执行搜索完毕，耗时： " + (cal1.getTimeInMillis()-cal0.getTimeInMillis()) + "，共找到 " + list2.size() + " 条数据");

%>

<%!
	public static <T>T getBean(Class<T> clazz){
		return ContextLoader.getCurrentWebApplicationContext().getBean(clazz);
	}
%>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<base href="<%=basePath%>">
	</head>
	
</html>