/**
 * @author eko.zhan  
 * @version 2015-04-21 10:35
 * @requires
 		jQuery
 		layer 
 		SystemKeys
 * @description
 		流程相关的框架
 */
;(function($){
	$.workflow = function(opts){
		$.extend($.workflow.defaults, opts || {});
	};
	/* 初始值 */
	$.workflow.defaults = {
		loginUrlTemplate: '${workflowPath}j_acegi_security_check?j_username=${username}&j_password=${password}&targeturl=${targeturl}',
		diaWidth: 1200,
		diaHeight:  600
	};
	$.extend($.workflow, {
		/**
		 * 打开传入的url
		 * jQuery 用于layer，不同的iframe打开显示的样式也不一样，不传入默认为当前页面
		 */
		open: function(url, jQuery){
			//alert(url);
			var me = this;
			if (!!window.ActiveXObject || "ActiveXObject" in window){	   
				var _diaWidth = me.defaults.diaWidth;
				var _diaHeight = me.defaults.diaHeight;		
				var _diaLeft = (screen.width-_diaWidth)/2;
				var _diaTop = (screen.height-_diaHeight)/2;
				var params = 'height='+_diaHeight+', width='+_diaWidth+', left='+_diaLeft+', top='+_diaTop+', center=1, location=0, scrollbars=0, toolbar=0, resizable=1, status=0, fullscreen=0';
				window.open(url, '_blank', params);  
			}else{
				//自适应窗口高度
				var _wfwidth = me.defaults.diaWidth;
				var _wfheight = me.defaults.diaHeight;			
	            if (jQuery==undefined){
		 			try{
					    _wfwidth = $(document).width() * 0.8;
					    _wfheight = $(document).height() * 0.75;			
					}catch(e){}           
	            }
				if (jQuery==undefined) jQuery = $;
				jQuery.layer({
					type: 2,
					border: [10, 0.3, '#000'],
					title: false,
					//closeBtn: false,
					iframe: {src : url, scrolling: 'no'},
					area: [_wfwidth+'px', _wfheight+'px']
				});
			}
		},
		/**
		 * 获取登录地址
		 */
		format: function(targeturl){
			var me = this;
			var url = me.defaults.loginUrlTemplate;
			url = url.replace('${workflowPath}', me.defaults.contextPath);
			url = url.replace('${username}', SystemKeys.userName);
			url = url.replace('${password}', SystemKeys.userPwd);
			url = url.replace('${targeturl}', targeturl);
			return url;
		},
		/* 如果contextPath不为空，标识系统配置了流程地址，也就是启用流程功能 */
		isUsable: function(){
			if (this.defaults.contextPath!=undefined && this.defaults.contextPath!=null && this.defaults.contextPath!=''){
				return true;
			}
			return false;
		},
		/*
		 * 获取待办事宜数量
		 * @param moduleId 
		 * 如果不传值则表示获取所有待办数量，传入moduleId获取该moduleId对应的待办数量
		 * @modifier eko.zhan at 2016-01-11-10:44
		 * 当前ajax是同步执行，当数据量大或者接口无法访问时会导致卡卡卡卡卡
		 * @deprecated
		 */
		//TODO
		getTodoCount: function(moduleId){
			//没有传入 moduleId ，则获取所有的待办事宜数量，否则获取指定的 moduleId 的待办事宜数量
			var _count = 0;
			var _param = {};
			if (moduleId!=undefined){
				_param = {moduleId: moduleId};
			}
			$.ajax({
				async: false,
				type: "POST",
				dataType : 'text',
				timeout : 30*1000,
				data: _param,
				url: $.fn.getRootPath() + '/app/individual/individual!getindividualcount.htm',
				success: function(msg){
					if (!isNaN(msg)){
						_count = msg;
					}
				}
			});
			return _count;
		},
		/**
		 * 个人中心待办事宜(定制)
		 */
		getTodoCount4Personal: function(){
			if (this.isUsable){
				$.ajax({
					async: true,
					type: "POST",
					dataType : 'text',
					timeout : 30*1000,
					data: {},
					url: $.fn.getRootPath() + '/app/individual/individual!getindividualcount.htm',
					success: function(msg){
						if (!isNaN(msg)){
							var _count = msg;
							if (_count==0){
								$('#navIndividual').html('个人中心');
							}else{
								$('#navIndividual').html('个人中心(<b style="font-size:16px;color:yellow;">'+_count+'</b>)');
							}
						}
					}
				});
			}
		},
		/**
		 * 登出
		 */
		logout: function(){
			if (this.isUsable()){
				var _url = this.defaults.contextPath + 'main/logout_kbs.jsp';
				$.getJSON(_url+'?jsonpcallback=?');
			}
		},
		/**
		 * password方法是用于用户在kbase-core中修改密码后同步至kbase-workflow，
		 * 密码如果采用密文传输需加密后传入，当前方法不负责明文加密
		 * @pwd 已加密的密文
		 */
		password: function(username, pwd){
			if (this.isUsable()){
				var _url = this.format('/core/password.do');
				pwd = pwd==undefined?'202cb962ac59075b964b07152d234b70':pwd;
				$.ajax({
					url: _url+'?jsonpcallback=?',
					dataType: 'jsonp',
			        data: '',
			        jsonp: 'jsonpcallback',
			        jsonpCallback: 'f'+pwd,
			        success: function(data) {
			        	//alert(data);
			        	window.SystemKeys.userPwd = pwd;
			        }
				});
			}
		},
		/**
		 * 发起流程
		 */
		fire: function(){
			var url = this.format('/core/picker.do?method=forwardworkflow');
			this.open(url);
		},
		/**
		 * 打开审批文档
		 */
		editDocument: function(requestid, jQuery){
			//@fixedme 参数 &isclose=1 是什么用？暂时去掉，待查 modify by eko.zhan at 2015-05-08 14:30
			var url = this.format('/workflow/request/workflow.jsp?reqtype=kbase&requestid='+requestid);
			this.open(url, jQuery);
		}
	});
	
	//初始化$.workflow
	$(function(){
		try{
			$.workflow({contextPath: SystemKeys.workflowPath});
		}catch(e){
			$.workflow({contextPath: ''});
		}
	});
})(jQuery);