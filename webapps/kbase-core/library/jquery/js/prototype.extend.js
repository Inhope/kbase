/*
 * @author eko.zhan
 */
/* 不区分大小写 */
String.prototype.startWithIgnoreCase = function(start){
	try{
		if (this.toLowerCase().indexOf(start.toLowerCase())==0){
			return true;
		}else{
			return false;
		}
	}catch(e){return false;}
}
/* 不区分大小写 */
String.prototype.endWithIgnoreCase = function(end){
	try{
		if (this.toLowerCase().lastIndexOf(end.toLowerCase())==(this.length-end.length)){
			return true;
		}else{
			return false;
		}
	}catch(e){return false;}
}

/* 不区分大小写 */
String.prototype.indexOfIgnoreCase = function(find){
	return this.toLowerCase().indexOf(find.toLowerCase());
}
/**
 * 参考jQuery的$.trim()
 * @return
 */
String.prototype.trim = function(){
	rtrim = /\S/.test("\xA0") ? (/^[\s\xA0]+|[\s\xA0]+$/g) : /^\s+|\s+$/g;
	return this.replace(rtrim, "");
}
/* 
 * @param m ab{0}de{1}g 
 * @param i "c","d"
 * @return abcdefg
 */
String.prototype.fill = function(){
	var args = arguments;
	return this.replace(/\{(\d+)\}/g, function(m,i){
		return args[i];
	});
}
/**
 * @author zhanzhao
 * @since 2014-05-22
 * @description 从字符串中截取指定字符串的最右，从右边开始查找(lastIndexOf)
 * 例如："abcdefg".right("bcd") 最终返回 "efg"
 */
String.prototype.right = function(find){
	if (this.lastIndexOf(find)==-1) return this;
	return this.substring(this.lastIndexOf(find)+find.length, this.length);
}
/**
 * @author eko.zhan
 * @since 2016-07-27 15:21
 * 判断当前浏览器是否是低版本浏览器
 */
navigator.isLowerBrowser = function(){
	var agent = this.userAgent.toLowerCase();
	if (agent.indexOf('msie 6')!=-1 || agent.indexOf('msie 7')!=-1 || agent.indexOf('msie 8')!=-1){
		return true;
	}
	return false;
}
/**
 * @author eko.zhan
 * @since 2016-07-30 10:32
 * 封装window.open
 */
window._open = function(url, title, width, height){
	//获得窗口的垂直位置 
	var top = (screen.height - 30 - height) / 2; 
	//获得窗口的水平位置 
	var left = (screen.width - 10 - width) / 2; 
	if (navigator.isLowerBrowser()){
		title = title.replace(/-/g, '');
	}
	var win = window.open(url, title, 'height=' + height + ',width=' + width + ',top=' + top + ',left=' + left + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=no,titlebar=no');
	win.focus();
}