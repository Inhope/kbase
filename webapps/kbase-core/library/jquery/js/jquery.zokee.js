/**
 * 基于jquery的一些常用的方法
 * @author eko.zhan
 * @since 2014-08-26
 */ 
(function($){
	$.kbase = $.kbase || {};
	$.extend($.kbase, {
		/**
		 * 比较两个日期大小，当前仅支持中文日期，不比较时间，可根据需要扩展
		 * 传入参数样例为 2014-07-01 2014-07-06
		 */
		dateCompare: function(startDate, endDate){
			var e = new Date();
			var s = new Date();
			if (startDate.indexOf("-")!=-1){
				if (startDate.indexOf(":")!=-1){
					//有时分秒
					var sarr = startDate.split(" ");
					var slarr = sarr[0].split("-");
					var srarr = sarr[1].split(":");
					s = new Date(slarr[0], Number(slarr[1])-1, slarr[2], srarr[0], srarr[1], srarr[2]);
					var earr = endDate.split("-");
					var elarr = earr[0].split("-");
					var erarr = earr[1].split(":");
					e = new Date(elarr[0], Number(elarr[1])-1, elarr[2], erarr[0], erarr[1], erarr[2]);
				}else{
					var sarr = startDate.split("-");
					s = new Date(sarr[0], Number(sarr[1])-1, sarr[2]);
					var earr = endDate.split("-");
					e = new Date(earr[0], Number(earr[1])-1, earr[2]);
				}
				
				return e.getTime()-s.getTime();
			}
		}
	});
	
	//正则表达式工具
	/**
	 * @author eko.zhan
	 * 使用方法：
	 var $pwd = $.kbase.regex('thisismypassword01');
	
	alert('$pwd.matchLength() ' + $pwd.matchLength());
	alert('$pwd.matchNumeric() ' + $pwd.matchNumeric());
	alert('$pwd.matchLowercase() ' + $pwd.matchLowercase());
	alert('$pwd.matchUppercase() ' + $pwd.matchUppercase());
	alert('$pwd.matchSpecchar() ' + $pwd.matchSpecchar());
	 */
	$.kbase.regex = $.kbase.regex || function(str){
		var _this = $.kbase.regex;
		_this.target = str;
		return _this;
	};
	$.extend($.kbase.regex, {
		//判断是否满足长度
		matchLength: function(min, max){
			min = min==undefined?3:min;
			max = max==undefined?18:max;
			
			var len = this.target.length;
			if (len>=min && len<=max){
				return true;
			}else{
				return false;
			}
		}, //end function matchLength
		//判断是否包含数字
		matchNumeric: function(){
			var reg = /[0-9]+?/;
			return reg.test(this.target);
		}, //end function matchNumeric
		//判断是否包含小写字母
		matchLowercase: function(){
			var reg = /[a-z]+?/;
			return reg.test(this.target);
		}, //end function matchLowercase
		//判断是否包含大写字母
		matchUppercase: function(){
			var reg = /[A-Z]+?/;
			return reg.test(this.target);
		}, //end function matchUppercase
		//判断是否包含特殊符号
		matchSpecchar: function(){
			var reg = new RegExp("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]");
			return reg.test(this.target);
		} //end function matchSpecchar
		
	});
})(jQuery);