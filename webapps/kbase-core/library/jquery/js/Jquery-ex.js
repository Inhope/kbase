var forIEShade = '<iframe style="width:100%;height:100%;filter:alpha(opacity=0);-moz-opacity:0; position:absolute; z-index:-1;display: none;"></iframe>';
var COLOR = window.USER_THEME ? window.USER_THEME : parent.USER_THEME;

(function() {
	$.extend($.fn, {
		showShade : function() {
			var w = $(document).width();
			var h = $(document).height();
			var l = '0';
			var t = '0';
			var shadeDiv = $('<div id="_shadeDiv" style="filter: alpha(opacity=60);">' + forIEShade + '</div>');
			shadeDiv.css({
				'position' : 'absolute',
				'zIndex' : '10000',
				'width' : w + 'px',
				'height' : h + 'px',
				'top' : l,
				'left' : t,
				'backgroundColor' : 'gray',
				'opacity' : '0.6',
				'display' : 'block'
			});
			if(shadeDiv.is(':hidden'))
				$('body').append(shadeDiv);
		},
		hideShade : function(){
			$('body>#_shadeDiv').hide().remove();
		},
		ajaxLoading : function(msg) {
			$(this).showShade();
	    	var maskEl = $('<div id="__ajaxLoading"></div>').html(msg).appendTo("body").addClass('ajax-loading').css({
	    		'backgroundImage' : 'url("' + this.getRootPath() + '/theme/' + COLOR + '/resource/jquery/images/pagination_loading.gif")',
	    		display : 'block'
	    	});
	    	maskEl.css({
				left:($(document).width() - maskEl.outerWidth(true)) / 2 - 20,
//	    		top:(($(document).height() - maskEl.height()) / 2) - 40,
				top : 150,
	    		'zIndex' : '10001'
	    	});
		},
		ajaxLoadEnd : function() {
			this.hideShade();
    		$('#__ajaxLoading').remove();    
		},
		iframeLoading : function(message) {
			var l = $(this).offset().left;
			var t = $(this).offset().top;
			var w = $(this).width();
			var h = $(this).height();
			var maskEl = $("<div></div>").html(message).appendTo("body").addClass('ajax-loading').css({
	    		'backgroundImage' : 'url("' + this.getRootPath() + '/theme/' + COLOR + '/resource/jquery/images/pagination_loading.gif")',
	    		display : 'block'
	    	});
	    	maskEl.css({
				left: l + (w / 2) - maskEl.width() + 10,
//	    		top: t + (h / 2)  - maskEl.height() - 10,
				top: t + 150,
	    		'zIndex' : '10002'
	    	});
	    	
	    	return maskEl;
		},
		iframeLoadEnd : function() {
			$(this).remove();
		},
		hint : function(message) {
//			var pdivEl = $('#alert_message_div');
//			if(! pdivEl[0]) {
//				pdivEl = $('<div id="alert_message_div"></div>');
//				var pdivWidth = 160;
//				pdivEl.css({
//					width : pdivWidth + 'px',
//					fontSize : '13px',
//					color : '#F4F4F4',
//					textAlign: 'center',
//					position : 'absolute',
//					left : ($('body').width() - pdivWidth) / 2,
//					top : '-30px',
//					zIndex : 10002
//				});
//				pdivEl.insertBefore($('body'));
//			}
//			var cdivEl = $('<div>' + message + '</div>');
//			var hintCssItem = {
//				border : 'solid 1px green',
//				height : 'auto',
//				backgroundColor : 'green',
//				paddingTop : '5px',
//				margin : '3px 0 0 0',
//				whiteSpace : 'normal',
//				wordWrap: 'break-word'
//			}
//			pdivEl.append(cdivEl.css(hintCssItem));
//			pdivEl.show().animate({
//				top :  '5px'
//			},1000, function(){
//				pdivEl.fadeOut(5000, function(){
//					$(this).remove();
//				});
//			});
/*
			$.messager.show({
                title:'提示',
                msg:message,
                showType:'fade',
                style:{
                    right:'',
                    bottom:''
                }
            });
            */
            //modify by eko.zhan at 2015-01-26
            //modify by eko.zhan at 2015-06-24 嵌入iframe后，纠错评论提交点击报错，parent找到了上级，没有详细阅读纠错的代码，暂时修改这里
            try{
            	parent.layer.msg(message, 2, -1);
            }catch(e){
            	layer.msg(message, 2, -1);
            }
		},
		getRootPath : function(){
		    var curWwwPath = window.document.location.href;
		    var pathName = window.document.location.pathname;
		    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
		    if (projectName=='/app') projectName = '';
		    return (projectName);
		},
		createContextMenu : function() {
			var self = this;
			var ContextMenu = function() {
				this.menu = $('<div id="_contextMenu"></div>');
				this.menu.css({
					'textAlign' : 'center',
					'width' : '127px',
					'backgroundColor' : '#e49da8',
					'border' : '1px solid #e49da8'
				});
				
				this.addItem = function(itemText, icon, ev){
					this.menuItem = $('<div></div>');
					
					var innerHtml = '';
					if(icon)
						innerHtml =   '<img width="16" height="16" src="' + icon + '"/>';
					innerHtml += itemText;
					this.menuItem.html(innerHtml);
					
					this.menuItem.css({
						'fontSize' : '12px',
						'color' : 'white',
						'width' : '121px',
						'height' : '20px',
						'lineHeight' : '20px',
						'marginLeft' : '2px',
						'marginTop' : '2px',
						'border' : '1px solid #ba3f51',
						'backgroundColor' : '#ba3f51',
						'marginBottom' : '2px'
					});
					
			        this.menuItem.mouseover(function(){
			            $(this).css({
							'cursor' : 'pointer',
							'backgroundColor' : '#c1282b'
			            });
			        });
			        
			        this.menuItem.mouseout(function(){
			            $(this).css({
			            	cursor : 'default',
			            	'backgroundColor' : '#ba3f51'
			            });
			        });
			        
			        this.menuItem.click(function(){
			            $(this).css('cursor', 'default');
			            $(this).parent().css('display','none');
			            ev();
			            return false;
			        });
			        
			        this.menu.append(this.menuItem);
			    };
			    
			    this.render = function() {
			        var _menu = this.menu;
			        
			        self.mousedown(function(e) {
			        	
			        	
			        	if(e.which == 3) {
						   document.oncontextmenu = new Function("return false;");
			        		/*  if (document) {
						             document.oncopy=new Function('return false;');
						             document.onselectstart=new Function('return false;');
						             document.onmousedown=disableclick;
						         } ;*/
			        		$('div#_contextMenu').hide();
	            			_menu.css({
	            				'position' : 'absolute',
	            				'top' : e.pageY,
	            				'left' : e.pageX,
	            				'zIndex' : '9000',
	            				'display' : 'block'
	            			});
		            		$('*').bind('click', function(e1) {
		            			if(!_menu.is(':hidden')) {
			            			if(!((e1.pageY > e.pageY && e1.pageY < e.pageY + _menu.height()) && (e1.pageX > e.pageX && e1.pageX < e.pageX + _menu.width()))) {
			            				_menu.hide();
			            			}
		            			}
					        });
		            		$('body').append(_menu);
	            			return false;
			        	}
			        });
			    }
			}
			return new ContextMenu();
		}
	});
	
	/*$.fn.iframeLoading = function(message) {
		return $(this).html();
	}*/
	
	/* add by eko.zhan at 2016-11-01 16:05 针对$的扩展  */
	$.extend($, {
		/* 加载多语言资源 */
		loadBundles: function(lang) {
			lang = lang==null?'zh':lang;
			$.i18n.properties({
			    name:'resource', 
			    path:$.fn.getRootPath() + '/library/jquery-i18n-properties/bundle/', 
			    mode:'both',
			    language:lang, 
			    cache: true,
			    callback: function() {
			    	
			    }
			});
		},
		/* 获取cookie */
		getCookie: function(key){
			var result = null;
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			for (var i = 0, l = cookies.length; i < l; i++) {
				var parts = cookies[i].split('=');
				var name = parts[0];

				if (key && key === name) {
					result = parts[1];
					break;
				}
			}
			return result;
		},
		/* 切换语言类型 */
		changeLocale: function(key){
			if (key==1 || key=='en'){
				$.cookie('locale', 'en', {expires: 3000, path: '/'});
				$.loadBundles('en');
			}else{
				$.cookie('locale', 'zh', {expires: 3000, path: '/'});
				$.loadBundles('zh');
			}
		}
	})

	//init
	$(function(){
		$('head').append("<script src='" + $.fn.getRootPath() + "/library/jquery-i18n-properties/jquery.i18n.properties.min.js' type='text/javascript' charset='utf-8'></script>");
		$.loadBundles($.getCookie('locale'));
	});
})();