/*
 * Copyright (c) 2014-2016 www.xiaoi.com 
 * @author eko.zhan 
 * @version 0.2
 * @since 2014-11-18
 * @update 2015-12-13 16:00
 * @description 
 * 直接访问地址： http://localhost:8080/kbase-converter/attachment/preview.do?fileName=201509111605115010.xlsx
 */
;(function($){
	$.kbase = $.kbase || {};
	$.kbase.convert = $.kbase.convert || 
	{
		host: "http://" + location.host,
		contextName: "/kbase-converter",	//在其他应用中调用此js时，此处需要固定配置，配置样例：/kbase-converter
		contextPath: this.host + this.conextName,
		fileType: {					//所有文件格式可自行设定
			txt: ".txt",
			pdf: ".pdf",
			swf: ".swf",
			office: [".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx"],
			img: [".jpg", ".jpeg", ".gif", ".bmp", ".ico", ".png"],
			video: [".flv", ".avi", ".3gp"]
		}
	};
	$.extend($.kbase.convert, {
		//打开一些特殊格式的文件
		openSpecFile : function(id, srcSuffix, url){
			var _that = this;
			if (_that.fileType.swf==srcSuffix){
    			//swf文件
    			$("#"+id).flash({
					src : url,
					height: 580,
					width: 980,
					version: 8
				});
    		}else if ($.inArray(srcSuffix, _that.fileType.video)!=-1){
    			//视频文件
    			$("#"+id).append('<a id="FlowPlayer" href="'+url+'"></a>')
    			flowplayer("FlowPlayer", "resources/js/flowplayer.swf", {
				    clip:  {
				        autoPlay: false,		//是否自动播放，默认true
				        autoBuffering: true		//是否自动缓冲视频，默认true
				    }}
				);
    		}else if ($.inArray(srcSuffix, _that.fileType.img)!=-1){
    			//图片文件
    			$("#"+id).append("<img src=\"" + url + "\"></img>");
    		}else{
    			//未知
    			$("#"+id).html("<span style=\"font:italic normal bolder 12pt Arial;color:red;position:relative;top:20px;\">当前文件不支持预览功能，请联系管理员</span>");
    		}
		},	//end function openSpecFile
		//获取附件打开方式
		getAttachment : function(fileName, id){
			var _host = this.host;
			var _contextName = this.contextName;
			$.ajax({
				type: "get",
				url: _host + _contextName + '/attachment/find.do?fileName='+fileName,
				dataType: "jsonp",
				jsonp: "callback",
				jsonpCallback: "jsonpCallback",
				success: function(json){
				    var _mode = json.mode;	//转换的方式
				    var _url = json.url;
				    _url = _host + "/" + _url;
				    var _srcSuffix = fileName.substring(fileName.lastIndexOf("."));
				    var _destSuffix = _url.substring(_url.lastIndexOf("."));
				    var _$layer = parent.$.layer;
				    var _param = "fileName=" + fileName;
				    _param += "&srcSuffix=" + _srcSuffix;
				    _param += "&destSuffix=" + _destSuffix;
				    _param += "&mode=" + _mode;
				    _param += "&targetUrl=" + json.url;
				    
				    if (typeof(_$layer)=="function"){
				    	_$layer({
						    type: 2,
						    border: [10, 0.3, '#000'],
						    title: "",
						    closeBtn: [0, true],
						    iframe: {src : _host + _contextName + "/result.jsp?" + _param},
						    area: ['1000px', '600px']
						});
				    }else{
				    	window.open(_url);
				    }
				},
				error: function(){
				    alert('附件获取有误，请联系管理员');
				}
			});
			return false;
		}, //end function getAttachment
		//初始化，将所有kbsconvert=1的标签初始化
		init: function(opts){
			var _convert = this;
			if (opts){
				$.extend($.kbase.convert, opts);
				if (opts.contextPath){
					var contextPath = $.kbase.convert.contextPath;
					_convert.host = contextPath.substring(0, contextPath.lastIndexOf("/"));
					_convert.contextName = contextPath.substring(contextPath.lastIndexOf("/"));
				}else{
					_convert.contextPath = opts.host + opts.contextName;
				}
			} //end if opts is not undefined
			$("*[kbsconvert='1']").css("cursor", "pointer").click(function(){
				var _that = this;
				var _name = $(_that).attr("kbsname");
				var _id = $(_that).attr("kbsid");
				_convert.getAttachment(_id + _name.substring(_name.lastIndexOf(".")));
			});
		}
	});
})(jQuery);