/**
 * @author eko.zhan
 * @since 2015-07-30 16:20
 * @version 0.1 
 * @description IE Support Only
 * 使用方法：
 * if ($.browser.msie){
 * 		$.kbase.find();			//查找页面范围内的文本
 *		//or
 *		$.kbase.find('areaId');	//查找areaId范围内的文本
 * }
 */
(function($){
	var __kbs_global_finder_index = 0;
	var __kbs_global_finder_key = '';	//用于控制关键词修改
	var __kbs_global_finder_type = 1;	//用于控制上一个或下一个
	var __kbs_global_finder_div = '';	//区域id
	/**
	 * 页面载入初始化文字查找器
	 * @param div 查找区域
	 */
	function _init(div, createElement){
		__kbs_global_finder_div = div!=undefined?div:__kbs_global_finder_div;
		
		if (createElement){
			//插入css
			if ($('#kbs-global-finder-css').length==0){
				$(['<style type="text/css" id="kbs-global-finder-css">.kbs-global-finder-logo{position:fixed;width:30px;height:30px;line-height:30px;top:10px;right:10px;border:1px solid #2e538f;text-align:center;cursor:pointer;overflow:hidden;color:#fff;background:#fff url("'+ $.fn.getRootPath() +'/theme/object-flip-vertical.png");}.kbs-global-finder-input{position:fixed;width:345px;height:32px;line-height:32px;top:10px;right:10px;background:#6d91cf;border:0 solid #000;display:none;font-size:12px}.kbs-global-finder-keyword{margin-left:5px;border:0;height:22px}.kbs-global-finder-btn{margin-left:3px;background:#3661a7;border:1px solid #27467a;color:#fff}.kbs-global-finder-total{margin-left:3px}</style>'].join('')).appendTo('head');
			}
			//插入查询按钮
			if ($('#kbs-global-finder-logo').length==0){
				$(['<div class="kbs-global-finder-input">',
					'<input class="kbs-global-finder-keyword"/>',
					'<input class="kbs-global-finder-btn" type="button" value="上一个">',
					'<input class="kbs-global-finder-btn" type="button" value="下一个">',
					'<span class="kbs-global-finder-total">&nbsp;</span>',
					'</div>',
					'<div class="kbs-global-finder-logo"></div>'].join('')).appendTo('body');
			}
		}
		
		//单击 查
		$('.kbs-global-finder-logo').click(function(){
			var _this = this;
			if ($('.kbs-global-finder-input:hidden').length>0){
				$('.kbs-global-finder-input').show();	
			}else{
				$('.kbs-global-finder-input').hide();
			}
		});
		
		//TODO: 缺少自动隐藏功能
		$('.kbs-global-finder-keyword').keyup(function(e){
			//敲回车才执行搜索功能
			if (e.keyCode==13){
				$('.kbs-global-finder-btn:last').click();
			}
		});
		
		//上一个
		$('.kbs-global-finder-btn:first').click(function(){
			_find(0);
		});
		//下一个
		$('.kbs-global-finder-btn:last').click(function(){
			_find(1);
		});
	} //end function _init
	
	/**
	 * @param type 1-下一个；0-上一个，默认单击下一个
	 */
	function _find(type){
		type = type==undefined?1:type;
		
		var keyword = $('.kbs-global-finder-keyword').val();
		if (keyword==''){
			return false;
		}
		//keyword = '罗森';
		
		//if ($.browser.mozilla){
			//Firefox浏览器
			//window.find(keyword, false, true);
			//return false;
		//}
		var rng = document.body.createTextRange();
		if (__kbs_global_finder_div.length>0 && document.getElementById(__kbs_global_finder_div)){
			rng.moveToElementText(document.getElementById(__kbs_global_finder_div));//将光标定位到指定的div
		}
		var tmpArr = rng.text.split(keyword);
		var total = tmpArr.length-1;	//查找到关键词的总数
		
		//未匹配提示 0/0
		if (total==0){
			$('.kbs-global-finder-total').text('0/0');
			return false;
		}
		
		//关键词发生变化
		if(keyword!=__kbs_global_finder_key){
			if (type==1){
				__kbs_global_finder_index = 0;
			}else{
				__kbs_global_finder_index = total;
			}
			__kbs_global_finder_key = keyword;
	 	}
	 	//从上一个切换到下一个
		if (type!=__kbs_global_finder_type){
			if (type==1){
				__kbs_global_finder_index = __kbs_global_finder_index+2;
			}else{
				__kbs_global_finder_index = __kbs_global_finder_index-2;
			}
			__kbs_global_finder_type = type;
		}
	 	
		var found = false;
		
		for (var i=0; i <=__kbs_global_finder_index && (found=rng.findText(keyword))==true; i++) {
			rng.moveStart("character", 1);
			rng.moveEnd("textedit");
		}
		
		if (found) {
			rng.moveStart("character", -1);
			var b = rng.findText(keyword);
			try{
				rng.select();
			}catch(e){
				//TODO 搜索内容太短时会报错，原因未知 add by eko.zhan at 2015-08-13 17:09
			}
			rng.scrollIntoView();
			if (type==1){
				__kbs_global_finder_index++;
			}else{
				__kbs_global_finder_index--;
			}
		}else{
			if (type==1){
				if (__kbs_global_finder_index>0){
		    		__kbs_global_finder_index = 0;
   					$('.kbs-global-finder-btn:last').click();
		    	}else{
		    		//alert("未找到指定内容."); //弹出不友好
		    	}
			}else{
				if (__kbs_global_finder_index==total || __kbs_global_finder_index<0){
		    		__kbs_global_finder_index = total-1;
   					$('.kbs-global-finder-btn:first').click();
		    	}else{
		    		alert("未找到指定内容."); //弹出不友好
		    	}
			}
		}
		if (type==1){
			if (__kbs_global_finder_index>total){
				__kbs_global_finder_index = 1;
			}
			$('.kbs-global-finder-total').text(__kbs_global_finder_index + '/' + total);
		}else{
			if (__kbs_global_finder_index+2>total){
				__kbs_global_finder_index = -1;
			}
			$('.kbs-global-finder-total').text((__kbs_global_finder_index+2) + '/' + total);
		}
	} //end function _find
	
	
	$.kbase = $.kbase || {};
	
	$.extend($.kbase, {
		find: function(div, createElement){
			createElement = createElement==undefined?true:createElement;
			_init(div, createElement);
		}
	});
})(jQuery);