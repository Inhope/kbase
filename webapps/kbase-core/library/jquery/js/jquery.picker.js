/**
 * @auther eko.zhan
 * @since 2015-06-12 17:28
 * @required Jquery.js
 *			Jquery-ex.js
 * @description
 *	组织架构常用选择器
 *	使用方法：
 *		参数说明：
 *			传入一个对象
 *		属性说明：
 *			returnField	返回的字段，desc|id，两个字段采用竖线分割，第一个字段接收描述，第二个字段接收id
 *			diaWidth 对话框宽度
 *			diaHeight 对话框高度
 *			title 对话框标题，默认为false
 *		建议只传入returnField值，不传入title界面更好看
 *		$.kbase.picker.deptUser({returnField:"deptuserdesc|deptuserid"});
 */
;(function($){
	$.kbase = $.kbase || {};
	$.kbase.picker = $.kbase.picker || {};
	
	$.extend($.kbase.picker, {
		//部门多选(带id,name,bh)
		deptmultiselect:function(opts){
			opts.url = "/app/util/picker!multichoosedept.htm?";
			_open(opts);
		},
		
		//选择知识目录树
		categoryselect:function(opts){
			opts.url = "/app/util/kb-picker!chooseCategory.htm?";
			_open(opts);
		},
		
		
		//东航公告管理虚拟部门选择
		noticedeptsearch: function(opts){
			opts.url = "/app/custom/dh/dhpicker!choosedept.htm?scope=公告管理";
			_open(opts);
		},
		
		//东航推荐虚拟部门选择
		recommenddeptsearch: function(opts){
			opts.url = "/app/custom/dh/dhpicker!choosedept.htm?scope=推荐";
			_open(opts);
		},
		
		
		//东航任务考试中虚拟部门选择
		taskdeptsearch: function(opts){
			opts.url = "/app/custom/dh/dhpicker!choosedept.htm?scope=任务考试";
			_open(opts);
		},
		
		//部门用户带搜索（只选用户）
		deptselectuser: function(opts){
			opts.url = "/app/util/picker!deptselectuser.htm?";
			_open(opts);
		},
		
		//部门用户带搜索（多选）
		deptusersearch: function(opts){
			opts.url = "/app/util/picker!deptusersearch.htm?";
			_open(opts);
		},
		
		//选择部门（多选）
		multiDept: function(opts){
			opts.url = "/app/util/picker!multidept.htm?";
			_open(opts);
		},
		
		//选择部门-公告发布范围定制(多选)
		multiDeptForNotice: function(opts){
			opts.url = "/app/util/picker!multideptfornotice.htm?";
			_open(opts);		
		},
		
		//选择部门-群组成员范围定制(多选)
		multiDeptForGroup: function(opts){
			opts.url = "/app/util/picker!multideptforgroup.htm?";
			_open(opts);		
		},		

		/**
		 * 部门-用户（混合选择，单选）
		 */
		deptUser: function(opts){
			opts.url = "/app/util/picker!deptuser.htm?";
			_open(opts);
		},
		/**
		 * 部门-用户（混合选择，多选）
		 */
		multiDeptUser: function(opts){
			opts.url = "/app/util/picker!deptusermulti.htm?";
			_open(opts);
		},
		/**
		 * 按部门选择用户（只能选择用户，单选）
		 */
		userByDept: function(opts){
			opts.url = "/app/util/picker!userbydept.htm?";
			_open(opts);
		},
		/**
		 * 按部门选择用户（只能选择用户，单选）带搜索功能
		 */
		userByDeptS: function(opts){
			opts.url = "/app/util/picker!userbydepts.htm?";
			_open(opts);
		},
		/**
		 * 按部门选择用户（只能选择用户，多选）
		 */
		multiUserByDept: function(opts){
			opts.url = "/app/util/picker!multiuserbydept.htm?";
			_open(opts);
		},
		/**
		 * 选择用户-按部门 多选（附带更多参数）
		 * 	不允许勾选部门
		 */
		multiUserByDept2: function(opts){
			opts.url = "/app/util/picker!multiuserbydept2.htm";
			_open2(opts);
		},
		/**
		 * 选择用户-按部门 多选（附带更多参数）
		 * 	允许勾选部门，并且认为勾选部门的用户包含其所有子部门用户
		 * 	新增勾选排除
		 */
		multiUserByDept3: function(opts){
			opts.url = "/app/util/picker!multiuserbydept3.htm";
			_open2(opts);
		},
		/**
		 * 按部门选择用户（只能选择用户，多选）带搜索功能
		 */
		multiUserByDeptS: function(opts){
			opts.url = "/app/util/picker!multiuserbydepts.htm?";
			_open(opts);
		},
		
		/**
		 * 按部门选择岗位（只能选择岗位，单选）
		 */
		stationByDept: function(opts){
			opts.url = "/app/util/picker!stationbydept.htm?";
			_open(opts);
		},
		/**
		 * 按部门选择岗位（只能选择岗位，多选）
		 */
		multiStationByDept: function(opts){
			opts.url = "/app/util/picker!multistationbydept.htm?";
			_open(opts);
		},
		/**
		 * 选择知识
		 */
		kbval: function(opts){
			opts.url = "/app/util/kb-picker!kb.htm?";
			_open(opts);
		},
		/**
		 * 选择知识(含搜索)
		 */
		kbvalS: function(opts){
			opts.url = "/app/util/kb-picker!kb.htm?search=1";
			_open(opts);
		},
		/**
		 * @author eko.zhan at 2016-01-15 19:57
		 * 增加选择最小分类树
		 */
		ontoCate: function(opts){
			opts.url = "/app/util/kb-picker!ontoCate.htm?";
			_open(opts);
		},
		/**
		 * 试卷分类选择
		 * @author Gassol.Bi
		 * @modified 2016-1-8 11:41:21
		 */
		choosePaperCate: function(opts){
			opts.url = '/app/learning/paper-cate!choose.htm';
			_open2(opts);
		},
		/**
		 * 试题分类选择(单选)
		 */
		singleQuesCate: function(opts){
			opts.url = "/app/learning/ques!singleQuesCate.htm?";
			_open(opts);
		}
		
	});
	
	
	/**
	 * private function
	 */
	function _open(opts){
		var returnField = opts.returnField;
		var diaWidth = opts.diaWidth;
		var diaHeight = opts.diaHeight;
		var title = opts.title;
		var url = opts.url;

		returnField = returnField==undefined?"":returnField;
		diaWidth = diaWidth==undefined?540:diaWidth;
		diaHeight = diaHeight==undefined?480:diaHeight;
		title = title==undefined?false:title;
		if (url.length>0){
			window.__kbs_picker_index = $.layer({
				type:2, 
				border: [5, 0.3, '#000'], 
				title: title,
				closeBtn:[0, true],
				iframe:{src: (opts.rootPath || $.fn.getRootPath()) + url +"&returnField="+returnField}, 
				area:[diaWidth+"px", diaHeight+"px"]
			});
		}else{
			alert('[kbase] location error');
		}
		
	}
	
	/**
	 * 附加更多的参数
	 * @author Gassol.Bi
	  * @modified 2016-1-8 11:41:21
	 */
	function _open2(opts){
		var diaWidth = opts.diaWidth;
		var diaHeight = opts.diaHeight;
		var title = opts.title;
		var url = opts.url;
		diaWidth = diaWidth==undefined?540:diaWidth;
		diaHeight = diaHeight==undefined?480:diaHeight;
		title = title==undefined?false:title;
		if (url.length>0){
			/*绑定其他参数*/
			if(url.indexOf('?') == -1) url += '?'
			for(var key in opts){ 
				if(key != 'diaWidth' && key != 'diaHeight' 
					&& key != 'title' && key != 'url') {
						url += '&' + key + '=' + opts[key];
					}
			}
			/*打开弹窗*/
			window.__kbs_picker_index = $.layer({
				type:2, 
				border: [5, 0.3, '#000'], 
				title: title,
				closeBtn:[0, true],
				iframe:{src: (opts.rootPath || $.fn.getRootPath()) + url}, 
				area:[diaWidth+"px", diaHeight+"px"]
			});
		}else{
			alert('[kbase] location error');
		}
	}
})(jQuery);