/**
 * 基于jqueryeasyuitree 扩展的树，目前主要用于首页的知识分类
 * @author eko.zhan
 * @since 2015-09-24 10:55
 * @required 
 	jquery.js, 
 	jquery.easyui.js, 
 	Tab.js
 */
(function($){
	'use strict'
	
	//$.fn.tree
	$.extend($.fn, {
		kbsTree: function(opts){
			var _this = this;	//树
			var _timeout = null;
			/*
			 * opts.menuId			右键绑定菜单id
			 * opts.keywordsId		树快速搜索关键词输入框id
			 * opts.searchBtnId		树快速搜索buttonid
			 * opts.checkbox		是否显示checkbox	1-是 0-否
			 */
			var searchNodeId = 'kbs-search-node';
			var defaultOpts = {
				style: {
					height: '300px',
					width: '180px',
					overflow: 'auto'
				}
			};
			$.extend(true, defaultOpts, opts);
			
			//设置样式
			$(_this).css(defaultOpts.style);
			//初始化树
			$(_this).tree({
				onClick: function(node){
					if (_timeout!=null){
						window.clearTimeout(_timeout);
						_timeout = null;
					}
					
					_timeout = window.setTimeout(function(){
						if (node.id==searchNodeId) return false;
						var isCate = true;
						if (node.iconCls!=undefined && node.iconCls=='icon-onto-obj'){
							isCate = false;
						}
						//window.categoryTag 这个变量很不友好，后期想办法处理掉
						if (!isCate && window.categoryTag=='true'){
							//打开分类
							var url = $.fn.getRootPath() + '/app/category/category.htm?type=1&categoryId=' + node.id+'&categoryName='+node.text;
							TABOBJECT.open({
								id : node.id,
								name : node.text,
								hasClose : true,
								url : url,
								isRefresh : true
							}, this);
						}else{
							//打开实例
							var url = $.fn.getRootPath() + '/app/object/ontology-object.htm?searchMode=1&id=' + node.id+'&isCate='+isCate+'&t='+new Date().getTime();
							if (node.attributes.isObj!=null && node.attributes.isObj==1){
								url = $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=1&objId=' + node.id+'&categoryId_current='+node.attributes.pid;
							}
							TABOBJECT.open({
								id : node.id,
								name :  node.text,
								//name :  '分类展示',
								hasClose : true,
								url : url,
								isRefresh : true
							}, this);
						}
						
						//add by baidengke 2016年3月27日
						if (SystemKeys.enableDataEmbed=='true'){
							olog.category({
								categoryId:node.id,
								categoryName:node.text
							});
						}
						
						//显示选中节点 add by wilson.li at 2016-05-16
						if(isHuawei() == 'true' && $("#dirSelectedText")){
							var _selectedText = $("#dirSelectedText");
							var _text = node.text;
							if(_text.length > 10){
								_text = _text.substring(0,8)+"..."
							}
							_selectedText.find("font").html(_text);
							_selectedText.css("margin-right","-" + (_selectedText.width() + parseInt(_selectedText.css("margin-left"))) + "px");
							_selectedText.show();
						}
					}, 300);
				},
				onDblClick: function(node){
					if (_timeout!=null){
						window.clearTimeout(_timeout);
						_timeout = null;
					}
					
					if (node.state=='closed'){
						$(_this).tree('expand', node.target);
					}else{
						$(_this).tree('collapse', node.target);
					}
				},
				onContextMenu: function(e, node){
					if (defaultOpts.menuId){
						e.preventDefault();
						// select the node
						$(_this).tree('select', node.target);
						// display context menu
						$('#' + defaultOpts.menuId).menu('show', {
							left: e.pageX,
							top: e.pageY
						});
					}
				},
				//add by eko.zhan at 2016-10-13 09:20  增加最小分类节点展开功能，根据客户端配置决定是否展开最小分类节点
				onBeforeExpand: function(node){
					//add by eko.zhan at 2016-10-12 10:35 
					var _url = $(_this).tree('options').url;
					if (node.iconCls!=undefined && node.iconCls=='icon-onto-obj'){
						if (_url.indexOf('&queryObject=')!=-1){
							_url = _url.replace('&queryObject=0', '&queryObject=1');
						}else{
							_url += '&queryObject=1';
						}
					}else{
						if (_url.indexOf('&queryObject=')!=-1){
							_url = _url.replace('&queryObject=1', '&queryObject=0');
						}else{
							_url += '&queryObject=0';
						}
					}
					$(_this).tree('options').url = _url;
				},
				// add by baidengke 2016年3月27日  当点击知识分类节点时记录该操作日志
				onExpand: function(node){
					if (SystemKeys.enableDataEmbed=='true'){
						olog.category({
							categoryId:node.id,
							categoryName:node.text
						});
					}
				}
			}); //end jeasyui tree init
			
			//知识分类tree 右键在新窗口中打开
			if (defaultOpts.menuId){
				$('#' + defaultOpts.menuId).menu({
				    onClick:function(item){
				    	if (item.iconCls=='icon-add'){	//在新面板中打开
				    		var node = $(_this).tree('getSelected');
				    		var url = $.fn.getRootPath() + '/app/object/ontology-object.htm?rightClick=true&id=' + node.id;
				    		if (node.attributes.isObj!=null && node.attributes.isObj==1){
								url = $.fn.getRootPath() + '/app/search/object-search.htm?searchMode=1&objId=' + node.id+'&categoryId_current='+node.attributes.pid;
				    		}
							TABOBJECT.open({
								id : new Date().getTime(),
								//name : '分类展示',
								name : node.text,
								hasClose : true,
								url : url,
								isRefresh : true
							}, this);
							
							//add by baidengke 2016年3月27日
							olog.category({
								categoryId:node.id,
								categoryName:node.text
							});
				    	}
				    }
				});
			}
			
			//知识分类树搜索
			if (defaultOpts.searchBtnId && defaultOpts.keywordsId){
				$('#' + defaultOpts.keywordsId).keyup(function(e){
					if (e.keyCode==13){
						$('#' + defaultOpts.searchBtnId).click();
					}
				});
				$('#' + defaultOpts.searchBtnId).click(function(){
					var keywords = $.trim($('#' + defaultOpts.keywordsId).val());
					if (keywords.length==0) return false;
					
					
					//modify by eko.zhan at 2016-06-17 10:39 目录搜索直接调用目录模糊搜索，搜索触发的事件统一在 Header.js 中维护
					$('#dir_content').val(keywords);
					$('#dir_commit').click();
					
					//lvan.li 20160111 解决快速搜索不支持维度过滤的问题，将快速搜索请求定位到模糊搜索
					/*
					TABOBJECT.open({
							id : $(this).attr('frameId'),
							name : '目录搜索',
							title : keywords,
							hasClose : true,
							url : $.fn.getRootPath() + '/app/search/diretory-search.htm?quick=true&dirName=' + encodeURIComponent(keywords),
							isRefresh : true
					}, this);
					*/
					
					/*$.post($.fn.getRootPath() + '/app/main/ontology-category!search.htm', {type: 'jeasyuitree', name: keywords}, function(data){
						if (data!=null){
							var searchNode = $(_this).tree('find', searchNodeId);
							if (searchNode!=null){
								$(_this).tree('remove', searchNode.target);
							}
							$(_this).tree('append', {
								data: [{
									id: searchNodeId,
									text: '搜索结果',
									iconCls: 'icon-search',
									children: data
								}]
							});
							
							$(_this).scrollTop($(_this)[0].scrollHeight);
						}else{
							alert('没有查找到相关分类');
						}
					}, 'json');*/
				});
			}
		},
		smsTree: function(opts){
		    var _this = this;	//树
		    var _timeout = null;
		    var defaultOpts = {
				style: {
					height: '300px',
					width: '180px',
					overflow: 'auto'
				}
			};
			$.extend(true, defaultOpts, opts);
			//设置样式
			$(_this).css(defaultOpts.style);
			//初始化树
			var _url = $.fn.getRootPath() + '/app/sms/sms-ontology!list.htm?'+ new Date();
            $(_this).tree({
                 url: _url,
                 method: 'get',
                 onBeforeLoad:function(node,param){
                    if(node!=undefined && node.attributes!=undefined){
                          $(_this).tree('options').url = $.fn.getRootPath() + '/app/sms/sms-ontology!list.htm?bh=' + node.attributes.bh + '&' + new Date();
                    }
				 },
                 onCheck:function(node, checked){
					 if(checked){
						 if(TABOBJECT.exists('smsBox')==-1){
							 TABOBJECT.open({
								 id : 'smsBox',
								 name : '短信发送',
								 title : '短信发送',
								 hasClose : true,
								 url : $.fn.getRootPath() + '/app/sms/sms-ontology!smsBox.htm',
								 isRefresh : true
							 }, this);
						 }
						 var _smsBox = TABOBJECT.tabContent.children(':eq(' + TABOBJECT.exists('smsBox') + ')').contents();
						 var tab = $("#box_dx",_smsBox);
						 if($(tab).html()==undefined){
							 var _dxtimer = window.setInterval(function(){
								 _smsBox = TABOBJECT.tabContent.children(':eq(' + TABOBJECT.exists('smsBox') + ')').contents();
								 tab = $("#box_dx",_smsBox);
								 if($(tab).html()!=undefined){
									 window.clearInterval(_dxtimer);
									 appendDXQA(tab,node);//添加标准问答信息
								 }
							 }, 300);
						 }else{
							 appendDXQA(tab,node);//添加标准问答信息
						 }
					 }else{
						 var _smsBox = TABOBJECT.tabContent.children(':eq(' + TABOBJECT.exists('smsBox') + ')').contents();
						 var tab = $("#box_dx",_smsBox);
						 if($(tab).html()!=undefined){
							 $('#'+node.id,tab).remove();
						 }
					 }
					 //选中树目录下的复选框 直接跳转到已打开 “短信发送” tab    modified by barry.li at 20161028
					 if(TABOBJECT.exists('smsBox')==1) {
						 // $('span:contains("短信发送")').each(function () {
							//  $(this).click();
						 // });

						 //modified by Barry.li at 20161116
						 $('#tabs').tabs('select', '短信发送');
					 }
				 },
				onDblClick: function(node){
					if (_timeout!=null){
						window.clearTimeout(_timeout);
						_timeout = null;
					}

					if (node.state=='closed'){
						$(_this).tree('expand', node.target);
					}else{
						$(_this).tree('collapse', node.target);
					}
				},
				onClick:function(node){
					if (_timeout!=null){
						window.clearTimeout(_timeout);
						_timeout = null;
					}

					_timeout = window.setTimeout(function(){
						if(node.attributes.question!=undefined){
							if(node.checked){
								$('#smsCategoryTree').tree('uncheck',node.target)
							}else{
								$('#smsCategoryTree').tree('check',node.target)
							}
						}
					}, 300);

				}
            });
            /**
            * 添加短信问答信息
            * @param tab //短信问答表单对象
            * @param node
            */
            function appendDXQA(tab,node){
                 if($('#'+node.id,tab)==undefined || $('#'+node.id,tab).attr('id')==undefined){
                     $(tab).append('<tr id="'+node.id+'" categoryid="'+node.attributes.pid+'">' +
						'<td class="content_dx">'+node.attributes.question+'</td>' +
						'<td class="content_dx" colspan="2">'+node.attributes.answer+'</td>' +
						'<td class="checkbox_dx">' +
						'<a onclick="removeQa(\''+node.id+'\');" href="javascript:void(0);">移除</a>' +
						'|<a class="kbs-fav" href="javascript:void(0);">收藏</a>' +
						'</td>' +
					 '</tr>');
                      //记录日志  add by heart.cao 2016-06-06
                      SMSLog.log({
                           categoryid: node.attributes.categoryid,
                           category: node.attributes.category,
                           objectid: node.attributes.objectid,
                           object: node.attributes.object,
                           questionid: node.id,
                           question: node.attributes.question,
                           bh: node.attributes.bh
                      });
                 }
            }
		}
	});
})(jQuery);
/** 取消选中节点 add by wilson.li at 2016-05-16 */
$(function(){
	$("#dirSelectedClear").on("click",function(){
		$("#categoryTree").find("div.tree-node-selected").removeClass("tree-node-selected");
		$("#dirSelectedText").hide();
	});
});
//add by heart.cao at 2016-05-17 用于操作短信树
//调用方法详见 WEB-INF/app/sms/smsbox.jsp
var SMSTreePlugin = {
	unCheck: function(nodeId){//取消选中短信树
		var node = $('#smsCategoryTree').tree('find',nodeId);
		if(node!=undefined&&node!=null){
		   $('#smsCategoryTree').tree('uncheck',node.target)
		}
	},
	clearSMSBox: function(){//清空短信树关联的待选列表
	    var _smsBox = TABOBJECT.tabContent.children(':eq(' + TABOBJECT.exists('smsBox') + ')').contents();
        var tab = $("#box_dx",_smsBox);
        //console.log("TAB: " + tab.html())
        if($(tab).html()!=undefined){
            //console.log("DXCH: " + $('input[name="gzyd_dx_ck"]',tab).length)
	         var trObjs = $('tr',tab);
			 for(var i=0; i<trObjs.length; i++){
			    if(trObjs[i]!=undefined && $(trObjs[i]).attr('id')!=undefined
			             && $.trim($(trObjs[i]).attr('id'))!=''){
			        var _qaId = $(trObjs[i]).attr('id');
			        $('#'+_qaId,tab).remove();						             
			    }
			 }
        }
	},
	closeSMSBox: function(){//关闭短信树关联的待选列表
	   $('#tabs').tabs('close', '短信发送');
	}
}