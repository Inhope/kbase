/**
 * @author eko.zhan
 * @since 2015-01-30
 * @description 对layer框架的扩展和改写
 */
(function($, L){
	$.extend(L, {
		msg : function(msg, time, parme, end){
	        var conf = {
	            title: false, 
	            closeBtn: false,
	            shadeClose: true,	//增加msg层单击遮罩层关闭属性
	            time: time === undefined ? 2 : time,
	            dialog: {msg: (msg === '' || msg === undefined) ? '&nbsp;' : msg},
	            end: end
	        };
	        if(typeof parme === 'object'){
	            conf.dialog.type = parme.type;
	            conf.shade = parme.shade;
	            conf.shift = parme.rate;
	        } else if(typeof parme === 'function') {
	            conf.end = parme
	        } else {
	            conf.dialog.type = parme;
	            if (parme==16) conf.shadeClose = false; //add by eko.zhan at 2015-02-04 for loading
	        }
	        return $.layer(conf);    
	    } 
	});
})(jQuery, window.layer);