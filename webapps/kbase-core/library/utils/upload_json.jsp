<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper" %>
<%@ page import="com.eastrobot.util.file.PropertiesUtil" %>
<%@ page import="com.eastrobot.util.struts.HttpClientUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<div align="center"><%
    /**
    * Kindeditor图片上传工具 图片上传至 kbase-convert
    * modify by heart.cao 2016-06-15
    */
    //图片上传服务器
    String swfAddress = PropertiesUtil.value("swf_address");	
    
    //图片上传目录
	String imgDir = request.getParameter("img_dir");    
	if(StringUtils.isBlank(imgDir))imgDir = "images";            
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	imgDir += "/" + sdf.format(new Date());


	//定义允许上传的文件扩展名
	String fileExts = "gif,jpg,jpeg,png,bmp";
	
	//最大文件大小
	long maxSize = 2000000;
	response.setContentType("text/html; charset=UTF-8");
	if(!ServletFileUpload.isMultipartContent(request)){
		out.println(getError("请选择需要上传的图片"));
		return;
	}
	
	FileItemFactory factory = new DiskFileItemFactory();
	ServletFileUpload upload = new ServletFileUpload(factory);
	upload.setHeaderEncoding("UTF-8");
	
	
	MultiPartRequestWrapper wrapper = (MultiPartRequestWrapper) request;     
	//获得上传的文件名     
	String fileName = wrapper.getFileNames("imgFile")[0];  
	//获得文件过滤器     
	File file = wrapper.getFiles("imgFile")[0];    
	 
	//检查扩展名     
	String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();     
	if(!Arrays.<String>asList(fileExts.split(",")).contains(fileExt))  
	{  
	    out.println(getError("上传文件扩展名是不允许的扩展名。\n只允许" + fileExts + "格式。"));  
	    return;  
	}     	
	//检查文件大小     
	if (file.length() > maxSize)  {     
	    out.println(getError("上传文件大小超过限制。"));     
	    return;     
	}        
	
	//重构上传图片的名称      
	SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");     
	String newFileName = df.format(new Date()) + "_"    
	                + new Random().nextInt(1000) + "." + fileExt;     
	
	//图片上传路径                
	String uploadUrl = swfAddress + "/convert/upload.do?dir=" + imgDir + "&filename=" + newFileName;	
	//图片展示路径
	String showUrl =  swfAddress + "/DATAS/swf/" + imgDir + "/" + newFileName;               
	
	//图片推送开始.....                
	Map<String, Object> map = new HashMap<String, Object>(){};
	map.put("file", file);
	map.put("filename", newFileName);
	map.put("dir", imgDir);                
	
	String result = "";
	Calendar cal0 = Calendar.getInstance();
	System.out.println("开始传送文件流 " + cal0.getTimeInMillis());
	try {
		result = HttpClientUtils.doPost(uploadUrl, map);
		System.out.println("文件传送完毕：" + (Calendar.getInstance().getTimeInMillis()-cal0.getTimeInMillis()));
	} catch (IOException e) {
		e.printStackTrace();
	}
	if(StringUtils.isBlank(result)){
		out.println(getError("上传图片失败!"));
		return;
	}
	//图片推送结束..... 
	
	JSONObject obj = new JSONObject();  
	obj.put("error", 0);  
	obj.put("url", showUrl);  
	out.println(obj.toString()); 
	
	%></div>
	<%!
	private String getError(String message) {
		JSONObject obj = new JSONObject();
		obj.put("error", 1);
		obj.put("message", message);
		return obj.toString();
	}
%>