/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.uiColor = 'FAFAFA';
	//config.width = 1000;
	config.height = 500;
	config.toolbar = [ 
		['Source','-','NewPage','Preview'], 
		/*['Cut','Copy','Paste','PasteText','PasteFromWord','-','SpellChecker', 'Scayt'],*/ 
		['Undo','Redo','-','Find','Replace','-','SelectAll'], 
		/*['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],*/ 
		['TextColor','BGColor'],
		/*['Link','Unlink','Anchor'],*/ 
		//'/', 
		['Styles','Format','Font','FontSize'],
		['Level0', 'Level1'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript']
		/*['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],*/ 
		/*['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],*/ 
		/*['Table','HorizontalRule','Smiley','SpecialChar']*/
	];
	config.removePlugins = 'elementspath';	//去掉下方的 body div table 提示
	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	config.allowedContent = true;
	config.font_names='微软雅黑/微软雅黑;宋体/宋体;黑体/黑体;仿宋/仿宋_GB2312;楷体/楷体_GB2312;隶书/隶书;幼圆/幼圆;'+ config.font_names;
//	config.removePlugins = 'sourcearea';
//	config.extraPlugins = 'level0,level1'; 
};
