﻿﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.dialog.add( 'textfield', function( editor ) {

	var acceptedTypes = { email: 1, password: 1, search: 1, tel: 1, text: 1, url: 1 };

	function autoCommit( data ) {
		var element = data.element;
		var value = this.getValue();

		value ? element.setAttribute( this.id, value ) : element.removeAttribute( this.id );
	}

	function autoSetup( element ) {
		var value = element.hasAttribute( this.id ) && element.getAttribute( this.id );
		this.setValue( value || '' );
	}

	return {
		title: '属性',
		minWidth: 350,
		minHeight: 100,
		onShow: function() {
			delete this.textField;
			var element = this.getParentEditor().getSelection().getSelectedElement();
			if ( element && element.getName() == 'input' && ( acceptedTypes[ element.getAttribute( 'type' ) ] || !element.getAttribute( 'type' ) ) ) {
				this.textField = element;
				this.setupContent( element );
			}
		},
		onOk: function() {
			var editor = this.getParentEditor(),
				element = this.textField,
				isInsertMode = !element;

			if ( isInsertMode ) {
				element = editor.document.createElement( 'input' );
				element.setAttribute( 'type', 'text' );
			}

			var data = { element: element };

			if ( isInsertMode )
				editor.insertElement( data.element );

			this.commitContent( data );

			// Element might be replaced by commitment.
			if ( !isInsertMode )
				editor.getSelection().selectElement( data.element );
		},
		onLoad: function() {
			this.foreach( function( contentObj ) {
				if ( contentObj.getValue ) {
					if ( !contentObj.setup )
						contentObj.setup = autoSetup;
					if ( !contentObj.commit )
						contentObj.commit = autoCommit;
				}
			} );
		},
		contents: [ {
			id: 'info',
			label: editor.lang.forms.textfield.title1,
			title: editor.lang.forms.textfield.title2,
			elements: [ {
				type: 'hbox',
				widths: [ '50%'],
				children: [
				{
					id: 'value',
					type: 'text',
					label: '标题',
					'default': '',
					accessKey: 'V',
					commit: function( data ) {
						if ( CKEDITOR.env.ie && !this.getValue() ) {
							var element = data.element,
								fresh = new CKEDITOR.dom.element( 'input', editor.document );
							element.copyAttributes( fresh, { value: 1 } );
							fresh.replace( element );
							data.element = fresh;
						} else {
							autoCommit.call( this, data );
						}
					}
				} ]
			}/*, {
				id: 'btnDel',
				type: 'button',
				label: '标识为删除',
				'default': '删除',
				setup: function( element ) {
					var _isdel = element.getAttribute( 'xdelflag') || 0;
					_isdel==null?0:_isdel;
					this.isdel = _isdel;
				},
				commit: function( data ) {
					var element = data.element;

					if ( CKEDITOR.env.ie ) {
						
					} else {
						element.setAttribute( 'xdelflag', this.isdel );
						if(this.isdel == 1){
							element.setAttribute( 'style', 'text-decoration: line-through;' );
						}
					}
				},
				onClick: function(){
					if (window.confirm('确定标识为删除吗')){
						//TODO 发送ajax请求判断当前节点能否删除
						//需要约定删除的逻辑
						this.isdel = 1;
					}
				}
			}*/ ]
		} ]
	};
} );
