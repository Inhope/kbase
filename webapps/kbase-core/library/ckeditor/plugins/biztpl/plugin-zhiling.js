(function(){
	var pluginName = 'zhiling';
	var defaultLabel = '指令';
	function _command( editor, name, value ) {
		this.editor = editor;
		
		this.exec = function(){
			var ckId = editor.name;
			var cmds = $('textarea[id="'+ckId+'"]').siblings('div.wd_bj').find('ul.wz').find('li#cmds').find('em').text();
			var _url = pageContext.contextPath + '/app/biztpl/article-instruction!zhiling.htm?editid='+ckId+'&cmds=' + cmds;
				$("#openZhiLingIframe")[0].src = encodeURI(_url);
				$("#editzhiling").window("open");
				$("#editzhiling").parent().css('z-index',9999);
				$("#editzhiling").window("move",{top:$(window).scrollTop() + 20});
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new _command(editor) );
			editor.ui.addButton && editor.ui.addButton('zhiling', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();