(function(){
	var pluginName = 'level3';
	var defaultLabel = '四级标题';
	function levelCommand( editor, name, value ) {
		this.editor = editor;
		this.name = name;
		this.value = value;
		
		this.exec = function(){
			var selectObj = editor.getSelection().getSelectedElement();
			var xid = "";
			var label = editor.getSelection().getSelectedText();
			if(selectObj){
				xid = $(selectObj).attr("xid");
				label = $(selectObj).attr("value");
			}
			if($('#bizTplPanel').find("input[xlevel=0]").length > 0 && $($('#bizTplPanel').find("input")[0]).attr("xlevel") == 0){
				if(label==''){
					label = defaultLabel;
					editor.editable().insertHtml('<input type="text" xid="' + xid + '" xlevel="3" class="kbs-title-level3" value="' + label + '"><br/>');
				}else{
					editor.editable().insertHtml('<input type="text" xid="' + xid + '" xlevel="3" class="kbs-title-level3" value="' + label + '">');
				}
				$('#bizTplPanel').html(editor.getData());
			}else{
				alert("最顶级只能是一级标题");
			}
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new levelCommand(editor, defaultLabel, defaultLabel) );
			editor.ui.addButton && editor.ui.addButton('Level3', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();