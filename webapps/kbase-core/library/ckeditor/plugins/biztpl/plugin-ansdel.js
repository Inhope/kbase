(function(){
	var pluginName = 'ansdel';
	var defaultLabel = '删除';
	function _command( editor, name, value ) {
		this.editor = editor;
		
		this.exec = function(){
			if (window.confirm('确定删除吗')){
				var _id = editor.name;
				
				editor.destroy();
				try{
					delete map[_id];
				}catch(e){}
				
				$('textarea[id="' + _id + '"]').remove();
				$('span[_answer="'+_id+'"]').remove();
				//$('div[_name="'+_id+'"]').parent('td').remove();
				if(_id.indexOf('Answer') == 0){
					answerMap.push(_id.split('Answer')[1])
				}
			}
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new _command(editor) );
			editor.ui.addButton && editor.ui.addButton('Ansdel', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();