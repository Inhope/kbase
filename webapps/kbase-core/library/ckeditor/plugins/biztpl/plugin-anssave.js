(function(){
	var pluginName = 'anssave';
	var defaultLabel = '保存';
	function _Command( editor, name, value ) {
		this.editor = editor;
		this.name = name;
		this.value = value;
		
		this.exec = function(){
			//点击保存时内容不能为空
			if($.trim(this.editor.getData()) == ""){
				alert("内容不能为空!");
				return false;
			}
			var id = this.editor.name;
			var cke = $("div#cke_"+id);//富文本对象
			var _div = $("div[_name='"+id+"']");//文本div对象
			//没有div文本对象时创建div，有div时清空内容，赋予新值
			if(_div.length == 0){
				var type = '';
				if(id.indexOf('Answer') == 0){
					type = 'Answer';
				}else if(id.indexOf('group') == 0){
					type = 'group';
				}
				var _divhtml = "<div _name='"+id+"' class='textarea_div' _type='"+type+"'>"+this.editor.getData()+"</div>"; 
				cke.parent().append(_divhtml);
			}else{
				_div.text("");//清空数据
				_div.append(this.editor.getData());
			}
			_div.show();
			cke.hide();
			//div绑定双击时间-该双击事件也可在其他地方$(_div).dblclick()来触发
			$("div[_name='"+id+"']").on("dblclick",function(){
				editorToDiv();
				$("div#cke_"+id).show();
				$(this).hide();
			});
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new _Command(editor, defaultLabel, defaultLabel) );
			editor.ui.addButton && editor.ui.addButton('anssave', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();