(function(){
	var pluginName = 'level0';
	var defaultLabel = '一级标题';
	function levelCommand( editor, name, value ) {
		this.editor = editor;
		this.name = name;
		this.value = value;
		
		this.exec = function(){
			var selectObj = editor.getSelection().getSelectedElement();
			var xid = "";
			var label = editor.getSelection().getSelectedText();
			if(selectObj){
				xid = $(selectObj).attr("xid");
				label = $(selectObj).attr("value");
			}
			if (label==undefined) return false;
			if(label==''){
				label = defaultLabel;
				editor.editable().insertHtml('<input type="text" xid="' + xid + '" xlevel="0" class="kbs-title-level0" value="' + label + '"><br/>');
			}else{
				editor.editable().insertHtml('<input type="text" xid="' + xid + '" xlevel="0" class="kbs-title-level0" value="' + label + '">');
			}
			$('#bizTplPanel').html(editor.getData());
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new levelCommand(editor, defaultLabel, defaultLabel) );
			editor.ui.addButton && editor.ui.addButton('Level0', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();