(function(){
	var pluginName = 'ansattr';
	var defaultLabel = '答案维度';
	function _command( editor) {
		
		var mask=$('<div id="mask"></div>');
		var test=$('<div class="test"></div>')
		var window_w=$(window).width();
		var window_h=$(window).height();
		$('body').append(mask);
		$('body').append(test);
		this.editor = editor;
		
		this.exec = function(){
			var editId = editor.name;
			test.load( "app/biztpl/article!pickAnswerAttr.htm?op=edit&t="+new Date().getTime(), function( response, status, xhr ) {
				if(status=='success'){
					var shl_w=$('.wd_tab').width();
					var shl_h=$('.wd_tab').height();
					var offset_w=(window_w-shl_w)/2;
					var offset_h=(window_h-shl_h)/2;
					var scroll_L=$(document).scrollLeft();
					var scroll_T=$(document).scrollTop();
					$('body').css('overflow','hidden');
					mask.css({
						'width':(window_w+scroll_L)+'px',
						'height':$(document).height()+'px',
						'display':'block',
						'z-index':9999
					})
					$('.wd_tab').css({
						'position':'absolute',
						'top':(offset_h+scroll_T)+'px',
						'left':offset_w+'px'
					}).hide().fadeIn(300);
					$('.cancel').click(function(){
						$('.wd_tab').hide();
						$('#mask,.test').remove();
						$('body').css('overflow','auto');
					});
					
					var editDiv = $('div.q_ask_show div.q_ask_top').find('textarea[id="'+editId+'"]');
					var dimIds = $(editDiv).parents('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').attr('dimId');
					//勾选 checkbox
					if (dimIds!=undefined){
						var ids_name = [];
						if(dimIds && dimIds.length > 0){
							var _dimidsArr = dimIds.split(',');
							$(_dimidsArr).each(function(i, item){
								$('input[name="' + item + '"]').attr('checked', 'checked');
							});
						}
					}
					$('div.wd_tab input[_type="all"]').click(function(){
						var code = $(this).attr('code');
						$('div.wd_tab input[code="'+code+'"]').attr("checked", $(this).is(':checked'));
					});
					$('div.button button#ok').click(function(){
							
							var dimName = '';
							var dimId = '';
							var _dimname = '';
							$('div.wd_tab input[type="checkbox"]:checked').not('input[_type="all"]').each(function(i,item){
								dimName += $(item).attr('_name') +  ','
								dimId += $(item).attr('name') +  ','
							});
							if(dimId && dimId.length > 0){
								dimId = dimId.substring(0,dimId.length-1);
								dimName = dimName.substring(0,dimName.length-1);
							}
							_dimname = dimName;
							if(dimId.length == 0){
								dimName = '所有维度'
							}else{
								dimName = dimName.length > 40 ? dimName.substring(0,40) + '...' : dimName;
							}
							//维度校验
							var ans = $(editDiv).parents('div.q_ask_show').children('div.q_ans').children('div.wz_more[editid!="'+editId+'"]');
							var dimArray = new Array();//存储已经选择的维度组合
								ans.each(function(i, item){
									var dimIds = $(item).attr('dimid');
									dimArray.push(dimIds.split(',').sort().join(','));
							})
							var flg = checkDim(dimArray,dimId);
							if(!flg){
								return false;
							}
							$(editDiv).parents('div.q_ask_top').find('div.wd_bj h2 em').text(dimName);
							$(editDiv).parents('div.q_ask_top').find('div.wd_bj h2 em').attr('title',_dimname);
							$(editDiv).parents('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').find('em').text(dimName);
							$(editDiv).parents('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').find('em').attr('title',_dimname);
							$(editDiv).parents('div.q_ask_top').find('div.drop_down').find('ul li[id="dim"] div.wz_r').attr('dimId',dimId);
					
							$('div.button button#cancel').click();
					});
					
					$('div.button button#cancel').click(function(){
						$('.wd_tab').remove();
						mask.css('display','none');
						$('body').css('overflow','auto');
					})
					
					$('table tr td').not('td:first-child').click(function(e){
						var _this=$(e.target.tagName);
						_this.eq(_this.index(this)).addClass('on');
					});
				}
			})
			
			
			
			//window.open(pageContext.contextPath + '/app/biztpl/article!pickAnswerAttr.htm?targetPanel=' + undefined, '_blank', _getWindowParams());
			
			/*window.__kbs_editor_id = editor.name;
			
			window.__kbs_layer_index = layer.open({
				type: 2,
				area: ['auto', 'auto'],
				content: pageContext.contextPath + '/app/biztpl/article!pickAnswerAttr.htm?targetPanel=' + undefined
			});*/
			//return false;
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new _command(editor) );
			editor.ui.addButton && editor.ui.addButton('Ansattr', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();