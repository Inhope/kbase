(function(){
	var pluginName = 'embed';
	var defaultLabel = '附件';
	function _command( editor) {
		this.editor = editor;
		var mask=$('<div id="mask"></div>');
		var test=$('<div class="test"></div>')
		var window_w=$(window).width();
		var window_h=$(window).height();
		$('body').append(mask);
		$('body').append(test);
		
		this.exec = function(){
			var ckId = editor.name;
			var id = '';
			var div = $('div.wz_more[editid="'+ckId+'"]')
			if(div && div.length > 0){
				id=$('div.wz_more[editid="'+ckId+'"]').attr('id');
			}
			var url = encodeURI('app/biztpl/article!fuj.htm?type=1&categoryName='+$('[name="categoryName"]').val() + '&ckId='+ckId+'&id='+id);
			test.load(url , function( response, status, xhr ) {
				if(status=='success'){
					var shl_w=$('.fj').width();
					var shl_h=$('.fj').height();
					var offset_w=(window_w-shl_w)/2;
					var offset_h=(window_h-shl_h)/2;
					var scroll_L=$(document).scrollLeft();
					var scroll_T=$(document).scrollTop();
					$('body').css('overflow','hidden');
					mask.css({
						'width':(window_w+scroll_L)+'px',
						'height':$(document).height()+'px',
						'display':'block',
						'z-index':9999
					})
					$('.fj').css({
						'position':'absolute',
						'top':(offset_h+scroll_T)+'px',
						'left':offset_w+'px'
					}).hide().fadeIn(300);
					$('#cancel').click(function(){
						$('.fj').remove();
						mask.css('display','none');
						$('body').css('overflow','auto');
					});
				}
			})
		
			/*window.__kbs_editor_id = editor.name;
		
			window.__kbs_layer_index = layer.open({
				type: 2,
				area: ['500px', '320px'],
				content: pageContext.contextPath + '/app/biztpl/article!upload.htm'
			});
			
			return false;*/
		}
	};
	CKEDITOR.plugins.add(pluginName, {
		init: function( editor ) {
			editor.addCommand(pluginName, new _command(editor) );
			editor.ui.addButton && editor.ui.addButton('Embed', {
				label: defaultLabel,
				command: pluginName
			} );
		}
	});
})();